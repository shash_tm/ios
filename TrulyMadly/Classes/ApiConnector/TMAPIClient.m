//
//  TMAPIMannager.m
//  TrulyMadly
//
//  Created by Ankit Jain on 05/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMAPIClient.h"
#import "TMBuildConfig.h"
#import "AFURLRequestSerialization.h"
#import "TMNotificationHeaders.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "TMEnums.h"
#import "TMUserSession.h"

@interface TMAPIClient ()

@property(nonatomic,assign)BOOL isReachable;
@property(nonatomic,assign)BOOL isWifiActive;
@property (nonatomic, strong) CTTelephonyNetworkInfo *networkInfo;

@end


@implementation TMAPIClient

+ (TMAPIClient*)sharedAPIManager {
    static TMAPIClient *_sharedAPIMgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAPIMgr = [[TMAPIClient alloc] initWithBaseURL:[NSURL URLWithString:TM_BASE_URL
                                                              ]];
    });
    
    return _sharedAPIMgr;
}
-(instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if(self) {
        self.isReachable = TRUE;
        [self setupNetworkTypeNotification];
        [self setUpNetowrkMonitering];
        [self updateCurrentNetworkType];
    }
    return self;
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.networkInfo = nil;
}
-(void)setUpNetowrkMonitering {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [self setupRechabilityNotification];
}
-(void)setupNetworkTypeNotification {
    self.networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    //NSLog(@"New Radio Access Technology: %@", self.networkInfo.currentRadioAccessTechnology);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(radioAccessChanged) name:CTRadioAccessTechnologyDidChangeNotification object:nil];
}
-(void)setHttpHeadersFromParams:(NSDictionary*)headerParameters {

    //[self.requestSerializer setHTTPShouldHandleCookies:NO];
    
    [headerParameters enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
        //NSLog(@"key:%@ value:%@",key,value);
        [self.requestSerializer setValue:value forHTTPHeaderField:key];
    }];
    
}
-(void)setDefaultRequestHeadersso{
    //set default headers
}
-(void)updateRequestHeadersForCookie {
    
}
-(void)resetAllRequesttHeaders {
    NSDictionary *httpHeaderDict = self.requestSerializer.HTTPRequestHeaders;
    [httpHeaderDict enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
        //NSLog(@"Settig Header with key:%@ value:%@ to nil",key,value);
        [self.requestSerializer setValue:nil forHTTPHeaderField:key];
    }];
}
- (void)radioAccessChanged {
    //NSLog(@"New Radio Access Technology: %@", self.networkInfo.currentRadioAccessTechnology);
    [self updateCurrentNetworkType];
}

-(void)setupRechabilityNotification {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                //NSLog(@"No Internet Connection");
                self.isReachable = NO;
                self.isWifiActive = FALSE;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                //NSLog(@"WIFI");
                self.isReachable = YES;
                self.isWifiActive = TRUE;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                //NSLog(@"3G");
                self.isReachable = YES;
                self.isWifiActive = FALSE;
                break;
            default:
                //NSLog(@"Unkown network status");
                self.isReachable = YES;
                self.isWifiActive = FALSE;
                break;
        }
        [self updateCurrentNetworkType];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TMNETWORKCHANGE_NOTIFICATION
                                                            object:nil
                                                          userInfo:@{@"network_status":[NSNumber numberWithBool:self.isReachable]}];
    }];
}

-(void)updateCurrentNetworkType {
    TMNetworkType networkType;
    if(self.isWifiActive) {
        networkType = TMNetworkTypeWiFi;
    }
    else {
        if([self.networkInfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyEdge]){
            networkType = TMNetworkType2G;
        }
        else if([self.networkInfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyHSUPA]){
            networkType = TMNetworkType3G;
        }
        else if([self.networkInfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyLTE]){
            networkType = TMNetworkTypeLTE;
        }
        else {
            networkType = TMNetworkType3G;
        }
    }
    
    [[TMUserSession sharedInstance] setNetworkType:networkType];
}
-(BOOL)connected {
    //NSLog(@"isReachable:%d",self.isReachable);
    return self.isReachable;
}

@end

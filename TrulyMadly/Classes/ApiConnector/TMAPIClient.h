//
//  TMAPIMannager.h
//  TrulyMadly
//
//  Created by Ankit Jain on 05/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface TMAPIClient : AFHTTPSessionManager

+ (TMAPIClient*)sharedAPIManager;

-(void)setHttpHeadersFromParams:(NSDictionary*)headerParameters;
-(void)resetAllRequesttHeaders;
-(void)updateRequestHeadersForCookie;

-(BOOL)connected;

-(void)setUpNetowrkMonitering;

@end

//
//  TMCookie.m
//  TrulyMadly
//
//  Created by Ankit on 07/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCookie.h"
#import "TMDataStore.h"
#import "TMCommonUtil.h"

@implementation TMCookie

+(NSString*)cookieRawValue {
    NSString *cookieStr = [TMDataStore retrieveObjectforKey:[self cookieKey]];
    return cookieStr;
}
+(NSString*)cookieKey {
    return @"PHPSESSID";
}
+(NSString*)cookieWithKeyValueFormat {
    NSString *cookieStr = [self cookieRawValue];
    if(cookieStr != nil){
        cookieStr  = [NSString stringWithFormat:@"%@=%@",[self cookieKey],cookieStr];
    }else{
        cookieStr  = @"";
    }
    return cookieStr;
}
+(void)cacheTMSessionCookie {
    for (NSHTTPCookie *cookie in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies) {
        if( [cookie.name isEqualToString:@"PHPSESSID"] )  {
            [TMDataStore setObject:cookie.value forKey:@"PHPSESSID"];
            [TMDataStore synchronize];
        }
    }
}
+(NSDictionary*)socketConnectionCookieDictionary {
    NSArray *cookies = @[@{@"value":[self cookieRawValue],@"key":[self cookieKey]}];
    NSDictionary *cookieDict = @{@"cookie":cookies,
                                 @"app_version_code":[TMCommonUtil buildNumberString],
                                 @"os_version_code":[TMCommonUtil deviceOSVersion],
                                 @"device_id":[TMCommonUtil vendorId],
                                 @"source":@"ios_app"
                                 };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cookieDict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *dict = @{@"header":jsonStr};
    return dict;
}

@end

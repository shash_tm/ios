//
//  TMCookie.h
//  TrulyMadly
//
//  Created by Ankit on 07/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCookie : NSObject

+(NSString*)cookieRawValue;
+(NSString*)cookieKey;
+(NSString*)cookieWithKeyValueFormat;
+(void)cacheTMSessionCookie;
+(NSDictionary*)socketConnectionCookieDictionary;

@end

//
//  AppDelegate.h
//  TrulyMadly
//
//  Created by Ankit on 23/02/15.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//-(void)openActiveSessionWithPermissions:(NSArray *)permissions
//                             allowLoginUI:(BOOL)allowLoginUI
//                                     from:(NSString*)from;

-(void)openActiveSessionWithPermissions:(BOOL)allowLoginUI from:(NSString*)from;

-(void)requestBirthdayPermission:(NSString*)from;

-(void)requestPhotoPermission:(NSString*)from;

-(void)setCrashlyticsIdentifier:(NSString*)userId;

-(void)setUserAttributeForMoEngage;

@end


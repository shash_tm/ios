//
//  AppDelegate.m
//  TrulyMadly
//
//  Created by Ankit on 23/02/15.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "AppDelegate.h"
#import "TMDataStore.h"
#import "TMNotification.h"
#import "TMAnalytics.h"
#import "TMNotificationReciever.h"
#import "TWMessageBarManager.h"
#import "TMBuildConfig.h"
#import "TMLog.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <DigitsKit/DigitsKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "GAI.h"
#import "TMMessagingController.h"
#import "MoEngage.h"
#import "MoEHelperConstants.h"
//#import <NewRelicAgent/NewRelic.h>
#import "AppVirality.h"
#import "TMIAPManager.h"
#import "TMUserSession.h"
#import "TMNotificationHeaders.h"

@interface AppDelegate ()

@property(nonatomic, assign) BOOL islaunchFromUrl;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSURLCache *tmCache = [[NSURLCache alloc] initWithMemoryCapacity:25 * 1024 * 1024 diskCapacity:200 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:tmCache];
    
    TMLOG( @"### running FB sdk version: %@", [FBSettings sdkVersion] );
    TMLOG(@"launch options %@",launchOptions);
    
    if(launchOptions){
        if([launchOptions objectForKey:@"UIApplicationLaunchOptionsURLKey"]){
            self.islaunchFromUrl = true;
        }
    }
    
    [self configureCrashlytics];
    [self configureNotificationWithLaunchOptions:launchOptions];
    [self configureGA];
    [self configureMoengageWithAppInstance:application launchingWithOptions:launchOptions];
    [self configureAppViralitySdk];
    [self configureDigits];
    //[self configureNewRelicSdk];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[MoEngage sharedInstance] stop:application];
    
    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithName:@"MyTask" expirationHandler:^{
        TMLOG(@"Closing socket connection in background");
        NSString *userId = [TMUserSession sharedInstance].user.userId;
        if(userId) {
            TMMessagingController *messagingController = [TMMessagingController sharedController];
            [messagingController disconnectChat];
        }
        
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [FBSettings setDefaultAppID:FB_ID];
    [FBAppEvents activateApp];
    
    [[MoEngage sharedInstance]applicationBecameActiveinApplication:application];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[MoEngage sharedInstance]applicationTerminated:application];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    TMLOG(@"application: openURL:%@ sourceApplication:%@",url,sourceApplication);
    if([FBAppCall handleOpenURL:url sourceApplication:sourceApplication]) {
        return YES;
    }else {
        TMLOG(@"open events");
        //UIApplicationState appState = [UIApplication sharedApplication].applicationState;
        TMUserSession *userSession = [TMUserSession sharedInstance];
        BOOL isUserLoggedIn = [userSession isLogin];
        
        if([[url host] isEqualToString:@"events"]) {
            if([url path] && isUserLoggedIn) {
                NSString *eventId = [url path];
                eventId = [eventId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];
                
                if(self.islaunchFromUrl){
                    [TMDataStore setObject:eventId forKey:@"openEventForId"];
                    return YES;
                }
                
                NSDictionary *dict = @{@"event_id":eventId};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"openEvent" object:nil userInfo:dict];
                return YES;
            }
        }
        else if ([[url host] isEqualToString:@"launch"]) {
            if([url path] && isUserLoggedIn) {
                NSString *action = [url path];
                action = [action stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];
                if([action isEqualToString:@"refer"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"openRefer" object:nil userInfo:nil];
                    return YES;
                }
                
                BOOL sendDeepLinkNotification = FALSE;
                NSDictionary *userInfo = nil;
                if([action isEqualToString:@"buy_sparks"]) {
                    sendDeepLinkNotification = TRUE;
                    userInfo = @{@"target":@"buysparks"};
                }
                if([action isEqualToString:@"trustbuilder"]) {
                    sendDeepLinkNotification = TRUE;
                    userInfo = @{@"target":@"trustbuilder"};
                }
                if([action isEqualToString:@"match"]) {
                    sendDeepLinkNotification = TRUE;
                    userInfo = nil;
                }
                if([action isEqualToString:@"conversation"]) {
                    sendDeepLinkNotification = TRUE;
                    userInfo = @{@"target":@"conversation"};
                }
                if([action isEqualToString:@"myprofile"]) {
                    sendDeepLinkNotification = TRUE;
                    userInfo = @{@"target":@"myprofile"};
                }
                
                if(sendDeepLinkNotification) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:TMDEEPLINK_NOTIFICATION object:nil userInfo:userInfo];
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [self registerDeviceToken:deviceToken];
    [[MoEngage sharedInstance]registerForPush:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[MoEngage sharedInstance]didFailToRegisterForPush];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    TMLOG(@"application:didReceiveRemoteNotification:%@",userInfo);
    if(userInfo) {
        UIApplicationState appState = [UIApplication sharedApplication].applicationState;
        NSString *event_type = @"push_open";
        if(appState == UIApplicationStateActive) {
            event_type = @"push_received";
        }
        [self trackNotification:userInfo event_type:event_type];
        [self handleNotification:userInfo fromLaunch:false];
        
        [[MoEngage sharedInstance] didReceieveNotificationinApplication:application withInfo:userInfo];
    }
}

#pragma mark - Facebook Handling
#pragma mark -

-(void)openActiveSessionWithPermissions:(BOOL)allowLoginUI from:(NSString *)from {
    NSArray *fbPermissions = @[@"email",@"user_photos",@"user_relationships",
                               @"user_actions.music",@"user_actions.books",
                               @"user_actions.video",@"user_friends",@"user_birthday",
                               @"user_likes",@"user_work_history",@"user_education_history",
                               @"user_location"];
    
    [FBSession openActiveSessionWithReadPermissions:fbPermissions
                                       allowLoginUI:allowLoginUI
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      
                                      if(error) {
                                          
                                      }
                                      else {
                                          NSDictionary *dictionary = [NSDictionary dictionaryWithObject:from forKey:@"from"];
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionStateChangeNotification" object:nil userInfo:dictionary];
                                      }
                                  }];
    
}

-(void)requestBirthdayPermission:(NSString *)from {
    [[FBSession activeSession] requestNewReadPermissions:@[@"user_birthday"]
                                       completionHandler:^(FBSession *session, NSError *error) {
                                           NSDictionary *dictionary = [NSDictionary dictionaryWithObject:from forKey:@"from"];
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionStateChangeNotification" object:nil userInfo:dictionary];
                                       }];
}

-(void)requestPhotoPermission:(NSString *)from {
    [[FBSession activeSession] requestNewReadPermissions:@[@"user_photos"]
                                       completionHandler:^(FBSession *session, NSError *error) {
                                           NSDictionary *dictionary = [NSDictionary dictionaryWithObject:from forKey:@"from"];
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionStateChangeNotification" object:nil userInfo:dictionary];
                                       }];
}

#pragma mark - GA
#pragma mark -

-(void)configureGA {
    [GAI sharedInstance].trackUncaughtExceptions = true;
    [GAI sharedInstance].dispatchInterval = 10;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    [[GAI sharedInstance] trackerWithTrackingId:GA_SERVER_ID];
}

#pragma mark - Crashlytics
#pragma mark -

-(void)configureCrashlytics {
    [Fabric with:@[CrashlyticsKit]];
    [[Crashlytics sharedInstance] setObjectValue:CRASHLYTICS_ENVIRONMENT_VALUE forKey:@"environment"];
    NSString *userId =[[TMUserSession sharedInstance].user userId];
    if(userId) {
        [[Crashlytics sharedInstance] setObjectValue:userId forKey:@"userId"];
    }
}

-(void)setCrashlyticsIdentifier:(NSString *)userId {
    TMLOG(@"user id is %@",userId);
    [[Crashlytics sharedInstance] setUserIdentifier:userId];
    [[Crashlytics sharedInstance] setObjectValue:userId forKey:@"userId"];
}

-(void)configureDigits {
    [Fabric with:@[DigitsKit]];
}

#pragma mark - MoEngage
#pragma mark -

-(void)configureMoengageWithAppInstance:(UIApplication *)application launchingWithOptions:(NSDictionary *)launchOptions {
    [[MoEngage sharedInstance] initializeWithApiKey:MoENGAGE_APP_ID inApplication:application withLaunchOptions:launchOptions];
    
    [self setUserAttributeForMoEngage];
}
-(void)setUserAttributeForMoEngage {
    NSString *userId =[[TMUserSession sharedInstance].user userId];
    if(userId) {
        [[MoEngage sharedInstance]setUserAttribute:userId forKey:USER_ATTRIBUTE_UNIQUE_ID];
    }
    int age = [[TMUserSession sharedInstance].user age];
    if(age) {
        [[MoEngage sharedInstance]setUserAttribute:[NSNumber numberWithInt:age] forKey:@"Age"];
    }
    NSString *gender = [[TMUserSession sharedInstance].user gender];
    if(gender) {
        [[MoEngage sharedInstance]setUserAttribute:[[TMUserSession sharedInstance].user userGenderString] forKey:USER_ATTRIBUTE_USER_GENDER];
    }
    NSString* countryCode = [NSString stringWithFormat:@"%d",[[TMUserSession sharedInstance].user country]];
    [[MoEngage sharedInstance] setUserAttribute:countryCode forKey:@"Country"];
    NSString *city = [[TMUserSession sharedInstance].user city];
    if(city) {
        [[MoEngage sharedInstance]setUserAttribute:city forKey:@"City"];
    }
    NSString *fname = [[TMUserSession sharedInstance].user fName];
    if(fname) {
        [[MoEngage sharedInstance]setUserAttribute:fname forKey:USER_ATTRIBUTE_USER_NAME];
    }
    NSString *status = [[TMUserSession sharedInstance].user status];
    if(status) {
        [[MoEngage sharedInstance]setUserAttribute:status forKey:@"UserStatus"];
    }else {
        [[MoEngage sharedInstance]setUserAttribute:@"install" forKey:@"UserStatus"];
    }
}

#pragma mark - AppViraltity
#pragma mark -

-(void)configureAppViralitySdk {
    [AppVirality attributeUserBasedonCookie:@"58ec35f2941c42368195a5d100aab4a5" OnCompletion:^(BOOL success, NSError *error) {
        
        NSDictionary * userDetails = nil;
        //if your App has login/logout and would like to allow multiple users to use single device, uncomment below lines
        //[AppVirality enableInitWithEmail];
        //userDetails = @{@"EmailId":@"USER-EMAIL-ID",@"ReferrerCode":@"REFERRER-CODE",@"isExistingUser":@"false"};
        // Init AppVirality SDK
        [AppVirality initWithApiKey:@"58ec35f2941c42368195a5d100aab4a5" WithParams:userDetails OnCompletion:^(NSDictionary *referrerDetails,NSError*error) {
            
            //NSLog(@"user key %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userkey"]);
            //NSLog(@"User has Referrer %@", referrerDetails);
        }];
        
        //To Add Test device in AppVirality dashboard
        /*
         [AppVirality registerAsDebugDevice:^(BOOL success, NSError *error) {
         NSLog(@"Register Test Device Response: %d ", success);
         }];
         */
    }];
}

#pragma mark - NewReclic
#pragma mark -

-(void)configureNewRelicSdk {
   //[NewRelicAgent startWithApplicationToken:@"AA0ac48b9ebe15fdee28f48d3adcf4916cc63fc231"];
}

#pragma mark - Notification Handling
#pragma mark -

-(void)configureNotificationWithLaunchOptions:(NSDictionary*)launchOptions {
    UIApplication *application = [UIApplication sharedApplication];
    if([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        UIUserNotificationType types = (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert);
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [application registerUserNotificationSettings:notificationSettings];
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        UIRemoteNotificationType types = (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert);
        [application registerForRemoteNotificationTypes:types];
    }
    
    
    if(launchOptions != nil && [launchOptions isKindOfClass:[NSDictionary class]]) {
        NSDictionary *notificationDictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(notificationDictionary) {
            [self trackNotification:notificationDictionary event_type:@"push_open"];
            [self handleNotification:notificationDictionary fromLaunch:true];
        }
    }
}

-(void)registerDeviceToken:(NSData*)deviceToken {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    BOOL isUserLoggedIn = [userSession isLogin];
    
    NSString *newToken = deviceToken.description;
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    TMLOG("Push Notification Token is: %@", newToken);
    ///
    [AppVirality setUserDetails:@{@"pushDeviceToken":newToken} Oncompletion:^(BOOL success, NSError *error) {
        
    }];
    
    /////
    NSString *appInstall = [TMDataStore retrieveObjectforKey:@"appInstall"];
    
    if(appInstall == nil) {
        [TMNotification sendToken:@"install" token:newToken];
    }
    else {
        NSString *failure = [TMDataStore retrieveObjectforKey:@"failure"];
        if(failure != nil) {
            [TMNotification sendToken:@"install" token:newToken];
        }else {
            NSString *oldToken = [TMDataStore retrieveObjectforKey:@"deviceToken"];
            if(![oldToken isEqualToString:newToken]) {
                if(isUserLoggedIn) {
                    [TMNotification sendToken:@"login" token:newToken];
                }
                else {
                    [TMNotification sendToken:@"install" token:newToken];
                }
            }
        }
    }
}

-(void)handleNotification:(NSDictionary*)notificationDictionary fromLaunch:(BOOL)fromLaunch {
    TMLOG("handleNotification: %@ fromLaunch:%d", notificationDictionary,fromLaunch);
    
    TMUserSession *userSession = [TMUserSession sharedInstance];
    BOOL isUserLoggedIn = [userSession isLogin];
    if(isUserLoggedIn) {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        
        BOOL fromMoEngage = ([notificationDictionary objectForKey:@"moengage"]) ? TRUE : FALSE;
        TMLOG("handleNotification: From moengage:%d",fromMoEngage);
        
        TMNotificationReciever *notificationReceiver  = [[TMNotificationReciever alloc]
                                                         initWithDictionary:notificationDictionary fromMoEngage:fromMoEngage];
        NSDictionary *dictionary = @{@"notificationData":notificationReceiver};
        
        UIApplicationState appState = [UIApplication sharedApplication].applicationState;
        TMLOG(@"App State:%ld",(long)appState);
        
        if([notificationReceiver.pushType isEqualToString:@"WEB"]) {
            if(appState == UIApplicationStateActive) {
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                if([notificationReceiver canShowBanner]) {
                    TWMessageBarManager *notificationBar = [TWMessageBarManager sharedInstance];
                    [notificationBar showMessageWithTitle:@"TrulyMadly"
                                              description:notificationReceiver.alert
                                                     type:TWMessageBarMessageTypeSuccess
                                                 duration:4.0
                                                 callback:^{
                                                     
                                                     NSString *urlString = notificationReceiver.url;
                                                     NSURL *webURL = [NSURL URLWithString:urlString];
                                                     [self openExternalURL:webURL];
                                                     
                                                 }];
                }
            }
            else {
                NSString *urlString = notificationReceiver.url;
                NSURL *webURL = [NSURL URLWithString:urlString];
                [self openExternalURL:webURL];
            }
        }
        else {
            if(appState == UIApplicationStateActive) {
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                if([notificationReceiver canShowBanner]) {
                    [self showNotificationBarWithDescription:notificationReceiver.alert userInfoDictionary:dictionary];
                }
            }
            else if(appState == UIApplicationStateInactive) {
                if(fromLaunch) {
                    NSData *notifData = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
                    [TMDataStore setObject:notifData forKey:@"notificationReceived"];
                }
                else {
                    if([notificationReceiver canShowBanner]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"RecievedNotification" object:nil userInfo:dictionary];
                    }
                }
            }
            else {
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                if(fromLaunch) {
                    NSData *notifData = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
                    [TMDataStore setObject:notifData forKey:@"notificationReceived"];
                }
                else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"RecievedNotification" object:nil userInfo:dictionary];
                }
            }
        }
    }
}

-(void)openExternalURL:(NSURL*)openURL {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] openURL:openURL];
        exit(0);
    });
}
-(void)showNotificationBarWithDescription:(NSString*)description userInfoDictionary:(NSDictionary*)userInfo {
    TWMessageBarManager *notificationBar = [TWMessageBarManager sharedInstance];
    [notificationBar showMessageWithTitle:@"TrulyMadly"
                              description:description
                                     type:TWMessageBarMessageTypeSuccess
                                 duration:4.0
                                 callback:^{
                                     
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RecievedNotification" object:nil userInfo:userInfo];
                                     
    }];
}
-(void)trackNotification:(NSDictionary*)dict event_type:(NSString*)event_type {
    NSDictionary *notif = [dict objectForKey:@"aps"];
    NSString *type = [notif valueForKey:@"type"];
    if(notif[@"event_status"]) {
        type = [notif valueForKey:@"event_status"];
    }
    //if(![type isEqualToString:@"MESSAGE"]) {
    [[TMAnalytics sharedInstance] trackNotification:event_type eventStatus:type];
    //}
}

@end

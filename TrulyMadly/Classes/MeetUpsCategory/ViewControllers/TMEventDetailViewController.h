//
//  TMEventDetailViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@protocol TMEventDetailViewControllerDelegate;

@interface TMEventDetailViewController : TMBaseViewController

@property(nonatomic,weak)id<TMEventDetailViewControllerDelegate>eventDetailDelegate;
-(instancetype)initWithEventId:(NSString *)eventId;

@end

@protocol TMEventDetailViewControllerDelegate <NSObject>

-(void)didReloadData;

@end

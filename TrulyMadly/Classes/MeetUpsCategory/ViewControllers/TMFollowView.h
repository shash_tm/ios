//
//  TMFollowView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseView.h"

@class TMEventDetailViewController;

@protocol TMFollowViewDelegate;

@interface TMFollowView : TMBaseView

@property(nonatomic,weak)id<TMFollowViewDelegate> followViewDelegate;
-(instancetype)initWithFrameAndCategory:(CGRect)frame categoryId:(NSString *)categoryId;
-(void)makeEventListRequest;

@end

@protocol TMFollowViewDelegate <NSObject>

-(void)didSelectEvent:(TMEventDetailViewController *)eventDetailViewCon;
-(void)didFollowRequestCompleted:(NSDictionary *)eventListDic;

@end
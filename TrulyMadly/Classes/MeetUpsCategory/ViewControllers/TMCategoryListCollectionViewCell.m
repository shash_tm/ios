//
//  TMCategoryListCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCategoryListCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"
#import "TMCategoryList.h"

@interface TMCategoryListCollectionViewCell()

@property(nonatomic, strong) TMProfileImageView* profileImageView;
@property(nonatomic, strong) TMProfileImageView* iconImageView;
@property(nonatomic, strong) UILabel* nameLabel;
@property(nonatomic, strong) UILabel* countLabel;
@property(nonatomic, strong)UIView *containerView;

@end

@implementation TMCategoryListCollectionViewCell

#define ICON_HEIGHT 30

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 6;
        [self initializeUIComponents];
    }
    return self;
}

/**
 * Initializes the UI component
 * to be called only once in the cell's lifetime
 */
- (void) initializeUIComponents {
    //initialize all labels and UI properties
    self.profileImageView = [[TMProfileImageView alloc] initWithFrame:self.bounds];
    self.profileImageView.clipsToBounds = YES;
    [self.profileImageView setImageContentMode:UIViewContentModeScaleAspectFill];
    [self.contentView addSubview:self.profileImageView];
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.containerView];
    
    self.iconImageView = [[TMProfileImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-ICON_HEIGHT)/2, 0, ICON_HEIGHT, ICON_HEIGHT)];
    [self.iconImageView setImageContentMode:UIViewContentModeScaleAspectFit];
    //self.iconImageView.backgroundColor = [UIColor clearColor];
    [self.containerView addSubview:self.iconImageView];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.font = [UIFont boldSystemFontOfSize:16];
    [self.containerView addSubview:self.nameLabel];
    
    self.countLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.numberOfLines = 0;
    self.countLabel.textColor = [UIColor whiteColor];
    self.countLabel.font = [UIFont systemFontOfSize:14];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerView addSubview:self.countLabel];
    
}

-(void)configureListUI:(TMCategoryList *)categoryData {
    // background image
    NSString* imageURLString = categoryData.backgroungImageURL;
    NSString *lastPathComponent = [imageURLString lastPathComponent];
    [self.profileImageView setDefaultImage:[UIImage imageNamed:@"deal_image_placeholder"]];
    [self.profileImageView setImageFromURLStringWithBackgroundGradient:imageURLString isDarkCentered:YES];
    [self.profileImageView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(self.frame.size.width*0.75, NSUIntegerMax);
    
    CGFloat height = 0;
    CGFloat xPos = 0; CGFloat yPos = 0;
    CGFloat width = self.frame.size.width;
        
    imageURLString = categoryData.iconImageURL;
    lastPathComponent = [imageURLString lastPathComponent];
    [self.iconImageView setImageFromURLString:imageURLString];
    [self.iconImageView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    height = height+ICON_HEIGHT+5;
    yPos = yPos + ICON_HEIGHT + 5;
        
    NSString *text = categoryData.title;
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    xPos = (width-rect.size.width)/2;
    self.nameLabel.frame = CGRectMake(xPos, yPos, rect.size.width, rect.size.height);
    self.nameLabel.text = text;
    [self.nameLabel sizeToFit];
    height = height+rect.size.height+5;
    yPos = yPos + rect.size.height + 5;
        
    attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
    text = categoryData.eventCount;
    rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    xPos = (width-rect.size.width)/2;
    self.countLabel.frame = CGRectMake(xPos, yPos, rect.size.width, rect.size.height);
    self.countLabel.text = text;
    [self.countLabel sizeToFit];
    height = height+rect.size.height;
        
    self.containerView.frame = CGRectMake(0, (self.frame.size.height-height)/2, width, height);
}

@end

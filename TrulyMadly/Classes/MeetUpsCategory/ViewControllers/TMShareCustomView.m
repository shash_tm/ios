//
//  TMShareCustomView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMShareCustomView.h"
#import "UIImageView+AFNetworking.h"
#import "TMEventMatch.h"

#define VIEW_WIDTH  70
#define VIEW_HEIGHT 70

@class TMEventMatch;

@interface TMShareCustomView()

@property(nonatomic, strong)UIScrollView *horizontalScrollView;
@property(nonatomic, strong)NSArray *userList;

@end

@implementation TMShareCustomView

-(instancetype)initWithFrame:(CGRect)frame userList:(NSArray *)userList {
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        self.userList = [[NSArray alloc] initWithArray:userList];
        [self addScrollView];
        return self;
    }
    return nil;
}

-(void)addScrollView {
    self.horizontalScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self.horizontalScrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    self.horizontalScrollView.pagingEnabled = false;
    self.horizontalScrollView.showsHorizontalScrollIndicator = NO;
    self.horizontalScrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.horizontalScrollView];
    self.horizontalScrollView.contentSize = CGSizeMake((VIEW_WIDTH)*self.userList.count, self.frame.size.height);
    
    [self addProfiles];
}

-(void)addProfiles {
    
    CGFloat width = VIEW_WIDTH;   CGFloat xPos = 5; CGFloat height = VIEW_HEIGHT; CGFloat yPos = 7;
    
    CGFloat iconWidth = 50;
    
    for (int i=0; i<self.userList.count; i++) {
        
        TMEventMatch *userData = self.userList[i];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos,yPos, width, height)];
        view.tag = i;
        
        UIImageView *picView = [[UIImageView alloc] initWithFrame:CGRectMake((width-iconWidth)/2, yPos, iconWidth, iconWidth)];
        picView.backgroundColor = [UIColor clearColor];
        picView.layer.cornerRadius = iconWidth/2;
        picView.layer.masksToBounds = YES;
        [picView setImageWithURL:[[NSURL alloc] initWithString:userData.matchImageURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
        [view addSubview:picView];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(picView.frame)+5, width, 20)];
        lbl.font = [UIFont boldSystemFontOfSize:13];
        lbl.text = userData.matchName;
        lbl.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lbl];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [view addGestureRecognizer:tapGesture];
        
        [self.horizontalScrollView addSubview:view];
        
        xPos = xPos+width;
        
    }
}

-(void)tapAction:(UITapGestureRecognizer *)sender {
    if(self.delegate){
        TMEventMatch *matchData = self.userList[sender.view.tag];
        [self.delegate shareEventWithMatch:matchData];
    }
}

@end

//
//  TMShareNativeView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMShareNativeView.h"
#import "TMLog.h"

#define VIEW_WIDTH 70
#define VIEW_HEIGHT 80

@interface TMShareNativeView()

@property(nonatomic, strong)UIScrollView *horizontalScrollView;
@property(nonatomic, strong)NSArray *nativeApp;

@end

@implementation TMShareNativeView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        self.nativeApp = @[@"WhatsApp",@"Messenger",@"Message",@"Mail",@"More"];
        [self addScrollView];
        return self;
    }
    return nil;
}

-(void)addScrollView {
    self.horizontalScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self.horizontalScrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    self.horizontalScrollView.pagingEnabled = false;
    self.horizontalScrollView.showsHorizontalScrollIndicator = NO;
    self.horizontalScrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.horizontalScrollView];
    self.horizontalScrollView.contentSize = CGSizeMake((VIEW_WIDTH)*self.nativeApp.count, self.frame.size.height);
    [self addAppToView];
}

-(void)addAppToView {
    CGFloat xPos = 5;
    CGFloat yPos = 0;
    CGFloat width = VIEW_WIDTH;
    CGFloat height = VIEW_HEIGHT;
    
    CGFloat iconWidth = 50;
    
    for (int i=0; i<self.nativeApp.count; i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos,yPos, width, height)];
        view.tag = i;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((width-iconWidth)/2, 12, iconWidth, iconWidth)];
        imageView.image = [UIImage imageNamed:self.nativeApp[i]];
        [view addSubview:imageView];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame)+5, width, 20)];
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.text = self.nativeApp[i];
        lbl.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lbl];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [view addGestureRecognizer:tapGesture];
        
        [self.horizontalScrollView addSubview:view];
        
        xPos = xPos+width;
    }
}

-(void)tapAction:(UITapGestureRecognizer *)sender {
    switch(sender.view.tag) {
        case 0:
            TMLOG(@"whats app");
            if(self.delegate){
                [self.delegate removeNativeViewFromSuperView:@"whatsapp"];
            }
            break;
        case 1:
            TMLOG(@"fbMessenger app");
            if(self.delegate){
                [self.delegate removeNativeViewFromSuperView:@"messenger"];
            }
            break;
        case 2:
            TMLOG(@"sms app");
            if(self.delegate){
                [self.delegate removeNativeViewFromSuperView:@"sms"];
            }
            break;
        case 3:
            TMLOG(@"mail app");
            if(self.delegate){
                [self.delegate removeNativeViewFromSuperView:@"mail"];
            }
            break;
        case 4:
            TMLOG(@"more app");
            if(self.delegate){
                [self.delegate removeNativeViewFromSuperView:@"activity"];
            }
            break;
        default:
            TMLOG(@"default");
            break;
    }
}
@end

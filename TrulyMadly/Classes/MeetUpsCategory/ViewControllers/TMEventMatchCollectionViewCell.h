//
//  TMEventMatchCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEventMatchView.h"

@interface TMEventMatchCollectionViewCell : UICollectionViewCell

@property(nonatomic,weak)id<TMEventMatchViewDelegate>cellDelegate;
@property(nonatomic, assign) BOOL isCellConfigured;
-(void)configureUI:(NSArray *)eventMatchData forcefullyReload:(BOOL)forcefullyReload showBlur:(BOOL)showBlur;
-(void)showConnectTutorial;

@end

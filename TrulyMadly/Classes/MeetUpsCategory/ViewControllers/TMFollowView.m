//
//  TMFollowView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMFollowView.h"
#import "TMCategoryListManager.h"
#import "TMEventListCollectionViewCell.h"
#import "TMSwiftHeader.h"
#import "TMCategoryList.h"
#import "TMEventDetailViewController.h"
#import "TMEventList.h"
#import "TMLog.h"

#define EVENT_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"EventListCollectionViewCell"
#define DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG 1001
#define DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE 1002
#define DEAL_LIST_EMPTY_DEAL_LIST_IMAGE 1003

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@interface TMFollowView ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, strong) UICollectionView* eventListCollectionView;
@property(nonatomic, strong) NSArray* eventListArray;
@property(nonatomic, assign) BOOL isRequestInProgress;
@property(nonatomic, strong) NSString *categoryId;

//category list manager
@property(nonatomic, strong) TMCategoryListManager* categoryListManager;

@end

@implementation TMFollowView

-(instancetype)initWithFrameAndCategory:(CGRect)frame categoryId:(NSString *)categoryId {
    self = [super initWithFrame:frame];
    if(self) {
        self.categoryId = categoryId;
        [self createDealListCollectionView];
        [self makeEventListRequest];
    }
    return self;
    
}

-(TMCategoryListManager*)categoryListManager {
    if(_categoryListManager == nil) {
        _categoryListManager = [[TMCategoryListManager alloc] init];
    }
    return _categoryListManager;
}

//- (void) viewDidLayoutSubviews {
//    //update the frame for the deal list collection view
//    self.eventListCollectionView.frame = self.bounds;
//
//}

#pragma mark - Deal List UI creation methods

- (void) createDealListCollectionView {
    //add the collection view
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 10;//7.5;
    flowLayout.minimumInteritemSpacing = 0;
    
    self.eventListCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    self.eventListCollectionView.backgroundColor = [UIColor clearColor];
    self.eventListCollectionView.dataSource = self;
    self.eventListCollectionView.delegate = self;
    [self.eventListCollectionView registerClass:[TMEventListCollectionViewCell class] forCellWithReuseIdentifier:EVENT_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [self addSubview:self.eventListCollectionView];
    
    self.eventListCollectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,54.0,0.0);
}

-(void)makeEventListRequest {
    if([self.categoryListManager isNetworkReachable]) {
        
        NSDictionary *params = @{@"action":@"show_events",@"category_id":self.categoryId,@"follow_status":@"following"};
        
        [self configureViewForDataLoadingStart];
        [self.categoryListManager getEventListForCategory:params eventListBlock:^(NSDictionary *eventListDict, TMError *error) {
            [self configureViewForDataLoadingFinish];
            if (eventListDict) {
                
                NSArray *eventArray = [[NSArray alloc] initWithArray:[eventListDict objectForKey:@"dealList"]];
                self.eventListArray = [eventArray copy];
                
                if(self.followViewDelegate) {
                    [self.followViewDelegate didFollowRequestCompleted:eventListDict];
                }
                [self.eventListCollectionView reloadData];
                
                if (self.eventListArray.count == 0) {
                    //empty list
                    [self showEmptyDealListMessage];
                }
            }else {
                //error case
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    //[self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewAtError];
                }
                else {
                    //unknwon error
                    [self showRetryViewAtUnknownError];
                }
            }
        }];
    }else {
        [self showRetryViewAtError];
    }
}

#pragma mark - UICollectionView methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    int requiredHeight = 170;//200;
    
    int cellSideMargin = 8;
    
    int requredWidth = self.eventListCollectionView.bounds.size.width - 2*cellSideMargin;
    
    return CGSizeMake(requredWidth, requiredHeight);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.eventListArray.count;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 0, 8, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMEventListCollectionViewCell* eventListCell = [self.eventListCollectionView dequeueReusableCellWithReuseIdentifier:EVENT_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    TMEventList* eventObj = [self.eventListArray objectAtIndex:indexPath.item];
    [eventListCell configureListUI:eventObj];
    return eventListCell;
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TMEventList *eventObj = [self.eventListArray objectAtIndex:indexPath.item];
    TMEventDetailViewController *eventDetailViewCon = [[TMEventDetailViewController alloc] initWithEventId:eventObj.event_id];
    if(self.followViewDelegate) {
        [self.followViewDelegate didSelectEvent:eventDetailViewCon];
    }
}


/**
 * Shows the empty deal list content to the user
 */
- (void) showEmptyDealListMessage {
    
    [self hideEmptyDealListMessage];
    
    UILabel* emptyDealListMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (IS_IPHONE_6_PLUS)?(self.frame.size.width*4/5):(self.frame.size.width*4.5/5), 80)];
    emptyDealListMessageLabel.backgroundColor = [UIColor clearColor];
    emptyDealListMessageLabel.center = CGPointMake(self.frame.size.width/2, (self.frame.size.height-70)/2);
    emptyDealListMessageLabel.numberOfLines = 3;
    emptyDealListMessageLabel.tag = DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE;
    emptyDealListMessageLabel.textColor = [UIColor colorWithRed:(112.0/255.0) green:(112.0/255.0) blue:(112.0/255.0) alpha:1.0];
    emptyDealListMessageLabel.font = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?[UIFont systemFontOfSize:16]:[UIFont systemFontOfSize:15];
    emptyDealListMessageLabel.textAlignment = NSTextAlignmentCenter;
    emptyDealListMessageLabel.text = @"Check out experiences on TM Scenes and start following them!";//@"Check out events on TM Socials\nand start following them!";
    [self addSubview:emptyDealListMessageLabel];
    
    //coffee background image view
    
    CGFloat coffeeLogoSize = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?84:80;
    
    UIImageView* logoLoaderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, coffeeLogoSize, coffeeLogoSize)];
    logoLoaderImageView.image = [UIImage imageNamed:@"empty_list"];
    logoLoaderImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoLoaderImageView.backgroundColor = [UIColor clearColor];
    logoLoaderImageView.tag = DEAL_LIST_EMPTY_DEAL_LIST_IMAGE;
    logoLoaderImageView.center = CGPointMake(self.bounds.size.width/2, emptyDealListMessageLabel.frame.origin.y - 25);
    [self addSubview:logoLoaderImageView];
}

/**
 * Shows the empty deal list content to the user
 */
- (void) hideEmptyDealListMessage {
    if ([self viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE]) {
        [[self viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE] removeFromSuperview];
    }
    if ([self viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_IMAGE]) {
        [[self viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_IMAGE] removeFromSuperview];
    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(makeEventListRequest) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(makeEventListRequest) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)configureViewForDataLoadingStart {
    TMLOG(@"configure view for data loading start");
    self.isRequestInProgress = true;
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self hideEmptyDealListMessage];
    [self hideActivityIndicatorViewWithtranslucentView];
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading..."];
}

-(void)configureViewForDataLoadingFinish {
    TMLOG(@"configure view for data loading finish");
    self.isRequestInProgress = false;
    [self hideActivityIndicatorViewWithtranslucentView];
    if(self.retryView.superview) {
        [self removeRetryView];
    }
}


@end

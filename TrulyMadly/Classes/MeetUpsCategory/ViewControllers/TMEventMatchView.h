//
//  TMEventMatchView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 13/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMEventMatch;

@protocol TMEventMatchViewDelegate;

@interface TMEventMatchView : UIView

@property(nonatomic,weak)id<TMEventMatchViewDelegate>eventMatchDelegate;
-(instancetype)initWithFrame:(CGRect)frame matchData:(TMEventMatch *)matchData index:(NSInteger)index;

@end

@protocol TMEventMatchViewDelegate <NSObject>

-(void)didTapOnProfile:(TMEventMatch *)eventMatch;

@end

//
//  TMEventListCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 09/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventListCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"
#import "TMEventList.h"

@interface TMEventListCollectionViewCell()

@property(nonatomic, strong) TMProfileImageView* profileImageView;
@property(nonatomic, strong) UILabel* nameLabel;
@property(nonatomic, strong) UILabel* locationLabel;
@property(nonatomic, strong) UIView *mutualMatchView;
@property(nonatomic, strong) UIView *containerView;
@property(nonatomic, strong) UIView *distanceView;
@property(nonatomic, strong) UILabel *distanceLbl;
@property(nonatomic, strong) UIImageView *locImageView;
@property(nonatomic, strong) UIImageView *statusView;

@end

@implementation TMEventListCollectionViewCell

#define MUTUAL_MATCH_ICON_WIDTH 40
#define SPACE_BETWEEN_LABELS 5
#define MUTUAL_MATCH_SPACE 8

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 6;
        [self initializeUIComponents];
    }
    return self;
}

/**
 * Initializes the UI component
 * to be called only once in the cell's lifetime
 */
- (void) initializeUIComponents {
    //initialize all labels and UI properties
    self.profileImageView = [[TMProfileImageView alloc] initWithFrame:self.bounds];
    self.profileImageView.clipsToBounds = YES;
    [self.profileImageView setImageContentMode:UIViewContentModeScaleAspectFill];
    [self.contentView addSubview:self.profileImageView];
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.containerView];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor whiteColor];
    //self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.containerView addSubview:self.nameLabel];
    
    self.locationLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.numberOfLines = 0;
    self.locationLabel.textColor = [UIColor whiteColor];
    self.locationLabel.font = [UIFont systemFontOfSize:14];
    //self.locationLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerView addSubview:self.locationLabel];
    
    /*self.mutualMatchView = [[UIView alloc] initWithFrame:CGRectZero];
    self.mutualMatchView.clipsToBounds = YES;
    [self.containerView addSubview:self.mutualMatchView];
   
    self.distanceView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.distanceView];
    self.locImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.distanceView addSubview:self.locImageView];
    
    self.distanceLbl = [[UILabel alloc] initWithFrame:CGRectZero];
    self.distanceLbl.backgroundColor = [UIColor clearColor];
    self.distanceLbl.textColor = [UIColor whiteColor];
    self.distanceLbl.font = [UIFont systemFontOfSize:14];
    self.distanceLbl.textAlignment = NSTextAlignmentCenter;
    [self.distanceView addSubview:self.distanceLbl];
    
    self.statusView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.statusView];*/
}

- (void)configureEventCellUI:(TMEventList *)eventData {
    NSString *imageURLString, *lastPathComponent, *eventName, *locationText;
    NSDictionary *locationAttributes, *nameAttributes;
    CGRect textFrame;
    CGSize constraintSize;
    CGFloat yPos;
    
    imageURLString = eventData.backgroungImageURL;
    lastPathComponent = [imageURLString lastPathComponent];
    eventName = eventData.eventName;
    locationText = eventData.eventLocation;
    
    [self.profileImageView setDefaultImage:[UIImage imageNamed:@"deal_image_placeholder"]];
    [self.profileImageView setImageFromURLStringWithBackgroundGradient:imageURLString isDarkCentered:YES];
    [self.profileImageView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    
    locationAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14]};
    constraintSize = CGSizeMake(self.contentView.frame.size.width * 0.9, NSUIntegerMax);
    textFrame = [locationText boundingRectWithConstraintSize:constraintSize attributeDictionary:locationAttributes];
    yPos = CGRectGetMaxY(self.contentView.frame) - 12 - CGRectGetHeight(textFrame);
    self.locationLabel.frame = CGRectMake(11, yPos, CGRectGetWidth(textFrame), CGRectGetHeight(textFrame));
    self.locationLabel.text = locationText;
    
    nameAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:18]};
    constraintSize = CGSizeMake(self.contentView.frame.size.width * 0.9, NSUIntegerMax);
    textFrame = [eventName boundingRectWithConstraintSize:constraintSize attributeDictionary:nameAttributes];
    yPos = self.locationLabel.frame.origin.y - CGRectGetHeight(textFrame) - SPACE_BETWEEN_LABELS;
    self.nameLabel.frame = CGRectMake(11, yPos, CGRectGetWidth(textFrame), CGRectGetHeight(textFrame));
    self.nameLabel.text = eventName;

}

//Method currently not in use due to UI changes

/*-(void)configureListUI:(TMEventList *)eventData {
    // background image
    NSString* imageURLString = eventData.backgroungImageURL;
    NSString *lastPathComponent = [imageURLString lastPathComponent];
    [self.profileImageView setDefaultImage:[UIImage imageNamed:@"deal_image_placeholder"]];
    [self.profileImageView setImageFromURLStringWithBackgroundGradient:imageURLString isDarkCentered:YES];
    [self.profileImageView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(self.frame.size.width*0.65, NSUIntegerMax);
    
    CGFloat height = 0;
    CGFloat xPos = 0; CGFloat yPos = 0;
    CGFloat width = self.frame.size.width;
    
    NSString *text = eventData.eventName;
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    xPos = (width-rect.size.width)/2;
    self.nameLabel.frame = CGRectMake(xPos, yPos, rect.size.width, rect.size.height);
    self.nameLabel.text = text;
    height = height+rect.size.height+SPACE_BETWEEN_LABELS;
    yPos = yPos + rect.size.height + SPACE_BETWEEN_LABELS;
    
    attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
    text = eventData.eventLocation;
    rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    xPos = (width-rect.size.width)/2;
    self.locationLabel.frame = CGRectMake(xPos, yPos, rect.size.width, rect.size.height);
    self.locationLabel.text = text;
    height = height+rect.size.height;
    yPos = yPos + rect.size.height + SPACE_BETWEEN_LABELS + SPACE_BETWEEN_LABELS;
    
    if(eventData.isMutualMatchGoing) {
        
        NSArray *view = [self.mutualMatchView subviews];
        for (int i=0; i<view.count; i++) {
            [view[i] removeFromSuperview];
        }
        
        attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13]};
        text = eventData.eventFollowingUsersCount;
        rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        CGFloat offset = 10;
        CGFloat viewWidth;
        if(eventData.eventFollowingUsers.count > 0 && eventData.userCountPresent){
            viewWidth = MUTUAL_MATCH_ICON_WIDTH*eventData.eventFollowingUsers.count - (eventData.eventFollowingUsers.count-1)*offset + rect.size.width + 30;
        }else if(eventData.eventFollowingUsers.count > 0) {
            viewWidth = MUTUAL_MATCH_ICON_WIDTH*eventData.eventFollowingUsers.count - (eventData.eventFollowingUsers.count-1)*offset;
        }
        else {
            viewWidth = rect.size.width + 20;
        }
        
        if(eventData.eventFollowingUsers.count > 3) {
            viewWidth = MUTUAL_MATCH_ICON_WIDTH*3 - (2)*offset + rect.size.width + 30;
        }
        
        xPos = (width-viewWidth)/2;
        self.mutualMatchView.frame = CGRectMake(xPos, yPos, viewWidth, MUTUAL_MATCH_ICON_WIDTH);
        
        CGFloat xPosition = 0; CGFloat yPos = 0;
        for (int i=0; i<eventData.eventFollowingUsers.count; i++) {
            if(i<3){
                TMProfileImageView *mutualMatchImgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPosition, yPos, MUTUAL_MATCH_ICON_WIDTH, MUTUAL_MATCH_ICON_WIDTH)];
                [self.mutualMatchView addSubview:mutualMatchImgView];
                mutualMatchImgView.layer.cornerRadius = MUTUAL_MATCH_ICON_WIDTH/2;
                mutualMatchImgView.layer.masksToBounds = YES;
                mutualMatchImgView.layer.borderColor = [UIColor whiteColor].CGColor;
                mutualMatchImgView.layer.borderWidth = 1.0;

                NSDictionary *mutualMatchData = [eventData.eventFollowingUsers objectAtIndex:i];
                NSString *imageURLString = [mutualMatchData objectForKey:@"image_url"];
                NSString *lastPathComponent = [imageURLString lastPathComponent];
                [mutualMatchImgView setImageFromURLString:imageURLString];
                [mutualMatchImgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:TRUE];
                mutualMatchImgView.retryImageDownloadOnFailure = TRUE;
                mutualMatchImgView.clearCacheAtPath = false;
                
                xPosition = xPosition + MUTUAL_MATCH_ICON_WIDTH - offset;
                if(i==1){
                    [self.mutualMatchView bringSubviewToFront:mutualMatchImgView];
                }
                if(i==2){
                    [self.mutualMatchView sendSubviewToBack:mutualMatchImgView];
                }
            }
            else {
                break;
            }
        }
        
        if(eventData.eventFollowingUsers.count > 0){
            xPosition = xPosition + offset + offset;
        }else {
            xPosition = 0;
        }
        if(eventData.userCountPresent) {
            UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, yPos+2.5, rect.size.width+20, MUTUAL_MATCH_ICON_WIDTH-5)];
            lblCount.layer.borderWidth = 1.0;
            lblCount.layer.borderColor = [UIColor whiteColor].CGColor;
            lblCount.layer.cornerRadius = 18;
            lblCount.font = [UIFont systemFontOfSize:13];
            lblCount.textColor = [UIColor whiteColor];
            lblCount.text = eventData.eventFollowingUsersCount;
            lblCount.textAlignment = NSTextAlignmentCenter;
            [self.mutualMatchView addSubview:lblCount];
        }
        self.mutualMatchView.backgroundColor = [UIColor clearColor];
        height = height + MUTUAL_MATCH_ICON_WIDTH + SPACE_BETWEEN_LABELS + SPACE_BETWEEN_LABELS;
    }else {
        self.mutualMatchView.frame = CGRectZero;
        NSArray *view = [self.mutualMatchView subviews];
        for (int i=0; i<view.count; i++) {
            [view[i] removeFromSuperview];
        }
    }
    
    self.containerView.frame = CGRectMake(0, (self.frame.size.height-height)/2, width, height);
    
    // for geo location
    if(eventData.isGeoAvailable){
        attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        text = eventData.distanceFromCurrentLocation;
        rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        CGFloat locImageHeight = rect.size.height-2;
        CGFloat locImageWidth = (locImageHeight*3)/4;
        CGFloat viewWidth = rect.size.width + 3 + locImageWidth + 5;
        xPos = width-viewWidth-5;
        self.distanceView.frame = CGRectMake(xPos, 8, viewWidth, rect.size.height);
        
        self.locImageView.frame = CGRectMake(0, 1, locImageWidth, locImageHeight);
        self.locImageView.image = [UIImage imageNamed:@"location_white"];
        
        self.distanceLbl.frame = CGRectMake(locImageWidth+5 , 0, rect.size.width, rect.size.height);
        self.distanceLbl.text = text;
    }else {
        self.distanceView.frame = CGRectZero;
        self.locImageView.frame = CGRectZero;
        self.distanceLbl.frame = CGRectZero;
    }
    
    // to show going status
    if(eventData.myStatusForEvent) {
        self.statusView.frame = CGRectMake(0, 8, 85.4, 28);
        self.statusView.image = [UIImage imageNamed:@"going_status"];
    }else {
        self.statusView.frame = CGRectZero;
    }
        
}*/

@end

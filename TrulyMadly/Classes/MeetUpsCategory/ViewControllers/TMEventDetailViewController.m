//
//  TMEventDetailViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventDetailViewController.h"
#import "TMDealContactCollectionViewCell.h"
#import "TMDealTermsNConditionCollectionViewCell.h"
#import "TMProfilePhotoHeaderView.h"
#import "TMEventDealObject.h"
#import "TMEventMatchCollectionViewCell.h"
#import "TMEventMatchView.h"
#import "TMLog.h"
#import "TMCategoryListManager.h"
#import "TMSwiftHeader.h"
#import "TMAboutEventCollectionViewCell.h"
#import "TMAboutEvent.h"

#import "TMNativeAdWebViewController.h"
#import "TMDealLocationViewController.h"

#import "TMToastView.h"
#import "TMEventMatch.h"

#import "TMMessageProfileViewController.h"
#import "TMUserSession.h"

#import "TMDataStore+TMAdAddtions.h"

#import "TMFullScreenPhotoViewController.h"

#import "TMMessageConfiguration.h"
#import "TMMessageConversationViewController.h"
#import "TMShareEventContext.h"

#import "TMShareView.h"
#import "TMMessagingController.h"
#import "TMMessage.h"
#import "TMBuySelectViewController.h"

#import <MessageUI/MessageUI.h>

#import <FacebookSDK/FacebookSDK.h>

#define FOLLOW_TUTORIAL @"follow_coach_mark_count"
#define CONNECT_TUTORIAL @"connect_follow_mark"

#define DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG 1001
#define DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE 1002
#define DEAL_LIST_EMPTY_DEAL_LIST_IMAGE 1003

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@interface TMEventDetailViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, TMDealContactCollectionViewCellDelegate, TMProfilePhotoHeaderViewDelegate, TMEventMatchViewDelegate, TMAboutEventCollectionViewCellDelegate, UIAlertViewDelegate, TMMessageProfileViewDelegate, TMShareViewDelegate, MFMessageComposeViewControllerDelegate>

@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) TMEventDealObject *eventDealObj;
@property(nonatomic, strong) NSString *eventId;
@property(nonatomic, strong) TMCategoryListManager *categoryListManager;
@property(nonatomic, strong) NSString *phoneNumber;
@property(nonatomic, assign) BOOL isEventFollowingByMe;
@property(nonatomic, assign) NSInteger index;
@property(nonatomic, assign) BOOL isForceFullyReload;
@property(nonatomic, strong) UIView *followTutorial;
@property(nonatomic, strong) UIView *matchActionView;
@property(nonatomic, strong) TMEventMatchCollectionViewCell *followCell;
@property(nonatomic, assign) BOOL oldStatus;
@property(nonatomic, strong) TMEventMatch *matchData;
@property(nonatomic, strong) UIView *followView;
@property(nonatomic, strong) TMAboutEvent *aboutEventData;
@end

@implementation TMEventDetailViewController

-(instancetype)initWithEventId:(NSString *)eventId {
    self = [super init];
    if(self) {
        self.eventId = eventId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Details";
    [self.navigationController setNavigationBarHidden:false animated: false];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self configureUIState];
    [self loadDealDetailData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = @"Details";
    TMUserSession *userSession = [TMUserSession sharedInstance];
    if(userSession.isUserProfileUpdated) {
        [self loadDealDetailData];
        userSession.isUserProfileUpdated = FALSE;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(TMCategoryListManager*)categoryListManager {
    if(_categoryListManager == nil) {
        _categoryListManager = [[TMCategoryListManager alloc] init];
    }
    return _categoryListManager;
}

-(UIView*)matchActionView {
    if(!_matchActionView) {
        CGRect frame = self.view.frame;
        CGFloat height = 80;
        _matchActionView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(frame),
                                                                    CGRectGetHeight(frame) - height,
                                                                    CGRectGetWidth(frame),
                                                                    height)];
        _matchActionView.layer.backgroundColor = [UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:1].CGColor;
        
//        CAGradientLayer *gradient = [CAGradientLayer layer];
//        gradient.frame = _matchActionView.bounds;//self.view.bounds;
//        gradient.colors = @[(id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0] CGColor],
//                            (id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8] CGColor]];
//        gradient.locations = @[@0.65f, @1.0f];
        //[_matchActionView.layer insertSublayer:gradient atIndex:0];
        
    }
    return _matchActionView;
}

-(void)dealloc
{
    //NSLog(@"deal detail dealloc called");
    if ([[UIApplication sharedApplication].keyWindow viewWithTag:110390]) {
        [[[UIApplication sharedApplication].keyWindow viewWithTag:110390] removeFromSuperview];
    }
}

-(void)configureUIState {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self addLeftBarButton];
    [self addRightBarButton];
}

-(void)addLeftBarButton {
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer , leftButton] animated:true];
    
}

- (void)addRightBarButton {
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shareIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(didShareViewTap)];
    
    [self.navigationItem setRightBarButtonItems:@[negativeSpacer , rightButton] animated:true];
}

-(void)addActionButton {
    
    [self resetMatchActionView];
    if(!self.matchActionView.superview) {
        [self.view addSubview:self.matchActionView];
        
        NSString *actionTitle, *eventPrice, *statusText;
        UILabel *eventPriceLabel, *eventStatusLabel, *actionLabel;
        UIView *actionView;
        CGFloat contentWidth, yPos;
        CGSize constraintSize;
        CGRect priceTextFrame, statusTextFrame, textFrame;
        BOOL isSelectOnly;
        
        self.aboutEventData = self.eventDealObj.aboutUsObj;
        isSelectOnly = self.eventDealObj.isSelectOnly;
        if(![self.aboutEventData.ticketPrice intValue]) {
            eventPrice = @"Free";
        }else {
            eventPrice = [NSString stringWithFormat:@"₹ %@",self.aboutEventData.ticketPrice];
        }
        actionTitle = self.aboutEventData.ticketTextString;
        
        contentWidth = self.view.bounds.size.width * 0.5;
        constraintSize = CGSizeMake(contentWidth - 12, NSUIntegerMax);
        
        priceTextFrame = [eventPrice boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:26]}];
        eventPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 10, contentWidth - 12, CGRectGetHeight(priceTextFrame))];
        eventPriceLabel.numberOfLines = 0;
        eventPriceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:26];
        eventPriceLabel.text = eventPrice;
        
        
        eventStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        eventStatusLabel.numberOfLines = 0;
        eventStatusLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
        if(isSelectOnly) {
            NSMutableAttributedString *attributedString;
            NSString *statusString;
            NSRange range;
            statusString = @"Exclusively for TM Select Members";
            attributedString = [[NSMutableAttributedString alloc] initWithString:statusString];
            range = [statusString rangeOfString:@"TM Select"];
            [attributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12] range:range];
                statusText = attributedString.string;
                eventStatusLabel.attributedText = attributedString;
        }else {
            statusText = self.eventDealObj.statusText;
            eventStatusLabel.text = statusText;
        }
        
        statusTextFrame = [statusText boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]}];
        yPos = CGRectGetMaxY(eventPriceLabel.frame);
        eventStatusLabel.frame = CGRectMake(12, yPos, contentWidth - 12, CGRectGetHeight(statusTextFrame));
        
        actionView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(eventStatusLabel.frame), 10, contentWidth - 10, 60)];
        actionView.backgroundColor = [UIColor likeColor];
        actionView.layer.cornerRadius = 5.0;
        
        constraintSize = CGSizeMake(actionView.frame.size.width, NSUIntegerMax);
        textFrame = [actionTitle boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]}];
        yPos = (actionView.bounds.size.height - CGRectGetHeight(textFrame))/2;
        actionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, yPos, CGRectGetWidth(actionView.frame), CGRectGetHeight(textFrame))];
        actionLabel.numberOfLines = 0;
        actionLabel.textAlignment = NSTextAlignmentCenter;
        actionLabel.textColor = [UIColor whiteColor];
        actionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18];
        actionLabel.text = actionTitle;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapActionView)];
        [actionView addGestureRecognizer:tapGesture];
        
        [self.matchActionView addSubview:eventPriceLabel];
        [self.matchActionView addSubview:eventStatusLabel];
        [actionView addSubview:actionLabel];
        [self.matchActionView addSubview:actionView];

    }
}

- (void)didTapActionView {
    
    BOOL isSelectMember, isSelectOnly;
    NSString *eventUrlString;
    
    isSelectMember = [TMUserSession sharedInstance].user.isSelectUser;
    isSelectOnly = self.eventDealObj.isSelectOnly;
    eventUrlString = self.aboutEventData.ticketUrlString;
    
    if(isSelectOnly && !isSelectMember) {
        TMBuySelectViewController *buyViewController = [[TMBuySelectViewController alloc] initWithSource:@"event_details"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:buyViewController];
        [self presentViewController:navigationController animated:TRUE completion:nil];
    }else {
        NSURL *url = [[NSURL alloc] initWithString:eventUrlString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}


//Method currently not in use due to UI Changes
/*-(void)addButtons {
    
    if(!self.matchActionView.superview) {
        [self.view addSubview:self.matchActionView];
        
        NSString *followText = @"Follow";
        NSString *imageName = @"follow";
        UIColor *viewColor = [UIColor likeColor];
        if(self.isEventFollowingByMe) {
            followText = @"Following";
            imageName = @"like";
            viewColor = [UIColor colorWithRed:0.867 green:0.867 blue:0.867 alpha:1];
        }
        // share view
        CGFloat xPos = 25+8;
        CGFloat width = (self.view.bounds.size.width-50)*0.5-12;
        CGFloat height = 44; CGFloat yPos = 10.5;
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(width, NSUIntegerMax);
        CGRect rect = [@"Share" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        UIView *shareView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
        shareView.layer.cornerRadius = 10;
        shareView.backgroundColor = [UIColor hideColor];
        [self.matchActionView addSubview:shareView];
        
        xPos = (width-(15+rect.size.width+5))/2;
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, (44-15)/2, 15, 17.3)];
        imgView.image = [UIImage imageNamed:@"share_event"];
        [shareView addSubview:imgView];
        
        UILabel *shareLbl = [[UILabel alloc] initWithFrame:CGRectMake(imgView.frame.origin.x+15+5, (44-rect.size.height)/2, rect.size.width, rect.size.height)];
        shareLbl.text = @"Share";
        shareLbl.font = [UIFont systemFontOfSize:15];
        shareLbl.textColor = [UIColor whiteColor];
        [shareView addSubview:shareLbl];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didShareViewTap)];
        [shareView addGestureRecognizer:tapGesture];
        
        rect = [followText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        xPos = CGRectGetMaxX(shareView.frame)+8;
        
        if(!self.followView) {
            self.followView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
            self.followView.layer.cornerRadius = 10;
            self.followView.backgroundColor = viewColor;
            [self.matchActionView addSubview:self.followView];
        }
        
        
        
        CGFloat size = 12;
        CGFloat height1 = 12;
        if(self.isEventFollowingByMe) {
            size = 17; height1 = 12;
        }
        xPos = (width-(size+rect.size.width+5))/2;
        UIImageView *followImgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, (44-height1)/2, size, height1)];
        followImgView.image = [UIImage imageNamed:imageName];
        [self.followView addSubview:followImgView];
        
        UILabel *followLbl = [[UILabel alloc] initWithFrame:CGRectMake(followImgView.frame.origin.x+size+5, (44-rect.size.height)/2, rect.size.width, rect.size.height)];
        followLbl.text = followText;
        followLbl.font = [UIFont systemFontOfSize:15];
        followLbl.textColor = [UIColor whiteColor];
        [self.followView addSubview:followLbl];
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didFollowViewTap)];
        [self.followView addGestureRecognizer:tapGesture1];
    }
    
}*/

-(void)backButtonPressed {
    self.categoryListManager = nil;
    self.collectionView = nil;
    if((self.oldStatus && !self.isEventFollowingByMe) || (!self.oldStatus && self.isEventFollowingByMe)) {
        if(self.eventDetailDelegate && [self.eventDetailDelegate respondsToSelector:@selector(didReloadData)]) {
            [self.eventDetailDelegate didReloadData];
        }
    }
    [self.navigationController popViewControllerAnimated:true];
}

-(void)loadDealDetailData {
    
    if([self.categoryListManager isNetworkReachable]) {
        
        NSDate *startDate = [NSDate date];
        NSMutableDictionary *eventParams = [NSMutableDictionary dictionaryWithCapacity:16];
        eventParams[@"screenName"] = @"TMEventDetailViewController";
        eventParams[@"eventCategory"] = @"scenes";
        eventParams[@"status"] = self.eventId;
        eventParams[@"startDate"] = startDate;
        eventParams[@"label"] = self.eventId;
        NSDictionary *params = @{@"datespot_id":self.eventId, @"action":@"event_details"};
        
        [self configureViewForDataLoadingStart];
        [self.categoryListManager getEventDetailData:params eventDetailBlock:^(TMEventDealObject *eventObj, TMError *error) {
            NSDate *endDate = [NSDate date];
            eventParams[@"endDate"] = endDate;

            [self configureViewForDataLoadingFinish];
            if(eventObj != nil) {
                self.eventDealObj = eventObj;
                //Changed to prevent the rendering of mutual view
                self.eventDealObj.isMutualViewAvail = NO;
                if(self.eventDealObj.isEventExpired) {
                    // show event expired message
                    [self showEmptyDealListMessage];
                    eventParams[@"eventAction"] = @"details_event_expired";
                }else {
                    self.isEventFollowingByMe = self.eventDealObj.followStatus;
                    self.oldStatus = self.isEventFollowingByMe;
                    [self configureCollectionViewWithDealData];
                    [self addActionButton];
                    //[self addButtons];
                    /*if (!([self didShowFollowTutorial] || self.isEventFollowingByMe)) {
                        [self showFollowTutorial];
                    }*/
                    eventParams[@"eventAction"] = (self.isEventFollowingByMe) ? @"details_event_followed" : @"details_event";
                    if(self.eventDealObj.matchData.count > 0) {
                        int mutualCount = 0; int profiles_liked = 0; int attendee_list = 0;
                        for (int i=0; i<self.eventDealObj.matchData.count; i++) {
                            TMEventMatch *eventMatch = self.eventDealObj.matchData[i];
                            if(eventMatch.isMutualMatch) {
                                mutualCount++;
                            }else if(eventMatch.likedByMe) {
                                profiles_liked++;
                            }else {
                                attendee_list++;
                            }
                        }
                        NSDictionary *eventInfoDict = @{@"attendee_list":@(attendee_list),@"mutual_matches":@(mutualCount),@"profiles_liked":@(profiles_liked)};
                        eventParams[@"event_info"] = eventInfoDict;
                    }
                }
            }else {
                //error case
                eventParams[@"status"] = @"error";
                eventParams[@"event_info"] = @{@"event_id":self.eventId};
                
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewAtError];
                }
                else {
                    //unknwon error
                    [self showRetryViewAtUnknownError];
                }
            }
            
            [[TMAnalytics sharedInstance] trackNetworkEvent:eventParams];
        }];
    }
    else {
        [self showRetryViewAtError];
    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadDealDetailData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadDealDetailData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)configureViewForDataLoadingStart {
    
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading..."];
}

-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
    TMLOG(@"configureViewForDataLoadingFinish");
    if(self.retryView.superview) {
        TMLOG(@"removing retry view");
        [self removeRetryView];
    }
}


-(void)configureCollectionViewWithDealData
{
    [self resetCollectionView];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(2.0f, 10.0f, 2.0f, 10.0f);
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height) collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,54.0,0.0);
    self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
    
    [self.collectionView registerClass:[TMDealTermsNConditionCollectionViewCell class] forCellWithReuseIdentifier:@"conditions"];
    [self.collectionView registerClass:[TMDealContactCollectionViewCell class] forCellWithReuseIdentifier:@"contacts"];
    [self.collectionView registerClass:[TMEventMatchCollectionViewCell class] forCellWithReuseIdentifier:@"matchData"];
    [self.collectionView registerClass:[TMAboutEventCollectionViewCell class] forCellWithReuseIdentifier:@"aboutEvent"];
    
    [self.collectionView registerClass:[TMProfilePhotoHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:[TMProfilePhotoHeaderView headerReuseIdentifier]];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];

}

-(void)resetCollectionView {
    [self.collectionView removeFromSuperview];
    self.collectionView = nil;
}

-(void)resetMatchActionView {
    if([self.matchActionView superview]) {
        [self.matchActionView removeFromSuperview];
        self.matchActionView = nil;
    }
}

#pragma mark - Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.eventDealObj sectionCount];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.eventDealObj.isMutualViewAvail) {
        if(indexPath.section == 0){
            // mutual match
            TMEventMatchCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"matchData" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            cell.cellDelegate = self;
            [cell configureUI:self.eventDealObj.matchData forcefullyReload:self.isForceFullyReload showBlur:self.isEventFollowingByMe];
            self.isForceFullyReload = false;
            self.followCell = cell;
            return cell;
        }
        else if(indexPath.section == 1) {
            // about the event view
            TMAboutEventCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"aboutEvent" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell configureUI:self.eventDealObj.aboutUsObj];
            cell.delegate = self;
            return cell;
        }
        else if (indexPath.section == 2){
            // contacts details
            TMDealContactCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"contacts" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell configureDealContact:self.eventDealObj.contacts];
            cell.delegate = self;
            return cell;
        }
        else if(indexPath.section == 3) {
            // terms and conditions view
            TMDealTermsNConditionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"conditions" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell configureDealConditions:self.eventDealObj.conditions];
            return cell;
        }
    }
    else{
        if(indexPath.section == 0) {
            // about the event view
            TMAboutEventCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"aboutEvent" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell configureUI:self.eventDealObj.aboutUsObj];
            cell.delegate = self;
            return cell;
        }
        else if (indexPath.section == 1){
            // contacts details
            TMDealContactCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"contacts" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell configureDealContact:self.eventDealObj.contacts];
            cell.delegate = self;
            return cell;
        }
        else if(indexPath.section == 2) {
            // terms and conditions view
            TMDealTermsNConditionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"conditions" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell configureDealConditions:self.eventDealObj.conditions];
            return cell;
        }
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        TMProfilePhotoHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                  withReuseIdentifier:[TMProfilePhotoHeaderView headerReuseIdentifier]                                                                            forIndexPath:indexPath];
        headerView.backgroundColor = [UIColor clearColor];
        headerView.delegate = self;
        [headerView configureHeader:self.eventDealObj.header isDealProfile:true];
        return headerView;
    }
    return nil;
}

# pragma mark - collection view flow layout delegate methods

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewFlowLayout*)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section == 0) {
        return CGSizeMake([self cellWidth:collectionViewLayout section:section], [UIScreen mainScreen].bounds.size.width);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewFlowLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIEdgeInsets contentInset = UIEdgeInsetsMake(18, 0, 13, 0);
    CGFloat height = [self contentHeightForItemAtIndexPath:indexPath layout:collectionViewLayout];
    height = height + contentInset.top;
    height = height + contentInset.bottom;
    
    return CGSizeMake([self cellWidth:collectionViewLayout section:indexPath.section], ceilf(height));
}

- (CGFloat)contentHeightForItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewFlowLayout *)collectionViewLayout
{
    CGFloat height = 0;
    height = [self.eventDealObj contentLayoutHeight:indexPath.section row:indexPath.row maxWidth:[self cellWidth:collectionViewLayout section:indexPath.section]];
    return height;
}

-(CGFloat)cellWidth:(UICollectionViewFlowLayout *)collectionViewLayout section:(NSInteger)section
{
    if(self.eventDealObj.isMutualViewAvail){
        if(section == 0){
            return self.view.bounds.size.width;
        }
    }
    return self.view.bounds.size.width-collectionViewLayout.sectionInset.left-collectionViewLayout.sectionInset.right;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == 0) {
        return UIEdgeInsetsMake(0, 0, 13, 0);
    }
    else if (section == 1) {
        return UIEdgeInsetsMake(0, 0, 15, 0);
    }
    return UIEdgeInsetsMake(0, 0, 11, 0);
}


#pragma mark TMEventMatchDelegate Method

-(void)didTapOnProfile:(TMEventMatch *)eventMatch {
    self.index = eventMatch.index;
    if(eventMatch.isMutualMatch) {
        TMLOG(@"open one on one coversation");
        [self pushConversationForMatch:eventMatch isShare:false];
    }else if(self.isEventFollowingByMe) {
        self.title = nil;
        // tracking function called
        NSString *eventType = (eventMatch.likedByMe) ? @"await_response" : @"view_match";
        NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id,@"attendee_list":@(self.eventDealObj.matchData.count)};
        [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
        
        TMMessageProfileViewController *profileViewCon = [[TMMessageProfileViewController alloc] initWithEventMatchData:eventMatch];
        profileViewCon.profileDelegate = self;
        profileViewCon.actionDelegate = self.delegate;
        [self.navigationController pushViewController:profileViewCon animated:true];
    }else {
        // tracking function called
        NSString *eventType = @"disabled_match";
        NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id};
        [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Hey! You have to follow this experience before you can connect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        });
    }
}


// function to call one to one conversation
-(void)pushConversationForMatch:(TMEventMatch *)eventMatch isShare:(BOOL)isShare{
    self.title = nil;
    
    TMMessageConfiguration *messageConfg = [[TMMessageConfiguration alloc] init];
    messageConfg.messageConversationFullLink = eventMatch.messageConvFullURL;
    messageConfg.matchUserId = [eventMatch.user_id integerValue];
    messageConfg.fName = eventMatch.matchName;
    messageConfg.profileImageURLString = eventMatch.matchImageURL;
    messageConfg.profileURLString = eventMatch.profileUrlString;
    
    //create share event context
    if(isShare) {
        TMShareEventContext *context = [[TMShareEventContext alloc] init];
        context.eventId = self.eventDealObj.eventId;
        [context setShareEventMessage:self.eventDealObj.header.combinedName];
        context.eventImgURL = self.eventDealObj.header.imageURL;
        TMMessage *message = [[TMMessage alloc] initWithShareEventContext:context
                                                          withMessageType:MESSAGETTYPE_SHARE_EVENT
                                                           withReceiverId:messageConfg.matchUserId];
        message.fullConversationLink = messageConfg.messageConversationFullLink;
        [[TMMessagingController sharedController] sendMessage:message];
        
        // tracking function called
        NSString *eventType = (self.isEventFollowingByMe) ? @"share_match_followed" : @"share_match";
        NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id};
        [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
    }
    else {
        // tracking function called
        NSString *eventType = (self.isEventFollowingByMe) ? @"chat_match_followed" : @"chat_match";
        NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id};
        [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
    }
    
    TMMessageConversationViewController *msgConvViewCon = [[TMMessageConversationViewController alloc] initWithMessageConversation:messageConfg];
    
    NSArray *viewCon = self.navigationController.viewControllers;
    NSMutableArray *newVC = [[NSMutableArray alloc] init];
    if(viewCon.count>2) {
        if([viewCon[viewCon.count-2] isKindOfClass:[TMMessageConversationViewController class]]) {
            for (int i=0; i<viewCon.count-2; i++) {
                [newVC addObject:[viewCon objectAtIndex:i]];
            }
            [newVC addObject:msgConvViewCon];
            [self.navigationController setViewControllers:newVC animated:true];
        }
        else {
            [self.navigationController pushViewController:msgConvViewCon animated:true];
        }
    }else {
        [self.navigationController pushViewController:msgConvViewCon animated:true];
    }
}


#pragma mark TMAboutEventCollectionViewCellDelegate Method

-(void)didClickOnTicket:(NSString *)ticketUrlString {
    NSString *eventType = (self.isEventFollowingByMe) ? @"get_tickets_followed" : @"get_tickets";
    [self trackExperienceDetailEvent:eventType eventInfo:nil];
    
    NSURL *url = [NSURL URLWithString:ticketUrlString];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] openURL:url];
    });
}

#pragma mark TMDealLocationViewControllerDelegate Method

-(void)didPhoneClick:(NSString *)phoneNumber
{
    NSString *eventType = (self.isEventFollowingByMe) ? @"phone_called_followed" : @"phone_called";
    [self trackExperienceDetailEvent:eventType eventInfo:nil];

    self.phoneNumber = phoneNumber;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:phoneNumber message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
    
    [alertView addButtonWithTitle:@"Call"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

-(void)didViewMoreClick
{
    // Push TMLocationViewController
    TMDealLocationViewController *locViewCon = [[TMDealLocationViewController alloc] initWithDateContact:self.eventDealObj.contacts headerText:self.eventDealObj.header.name];
    locViewCon.cellDelegate = self;
    [self.navigationController pushViewController:locViewCon animated:YES];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([buttonTitle isEqualToString:@"Call"]){
        NSString *callString = [NSString stringWithFormat:@"%@%@",@"tel://",self.phoneNumber];
        NSString* webStrURL = [callString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:webStrURL];
        [[UIApplication sharedApplication] openURL:url];
    }
    else if ([buttonTitle isEqualToString:@"Yes"]){
        [self pushConversationForMatch:self.matchData isShare:true];
    }else if([buttonTitle isEqualToString:@"Unfollow"]){
        [self sendUserAction];
    }
}

#pragma mark floating button action method

-(void)didShareViewTap {
    TMLOG(@"share view tap");
    
    NSString *eventType = (self.isEventFollowingByMe) ? @"share_event_followed" : @"share_event";
    [self trackExperienceDetailEvent:eventType eventInfo:nil];
    
    TMShareView *shareView = [[TMShareView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height+64.0)];
    shareView.shareDelegate = self;
    shareView.tag = 110390;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:shareView];
}

-(void)didFollowViewTap {
    TMLOG(@"follow view tap");
    // show alert in case of unfollow the event
    if(self.isEventFollowingByMe){
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to Unfollow this experience?" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unfollow", nil];
            [alertView show];
        });
    }else {
        [self sendUserAction];
    }
}

-(void)sendUserAction {
    if([self.categoryListManager isNetworkReachable]) {
        
        NSDictionary *params = @{@"datespot_id":self.eventId, @"action":@"follow_event"};
        if(self.isEventFollowingByMe){
            params = @{@"datespot_id":self.eventId, @"action":@"unfollow_event"};
        }
        
        self.isEventFollowingByMe = !self.isEventFollowingByMe;
        
        // animate the follow button
        [self animateFollowView];
        
        // show connect tutorial
        if(![self didShowConnectTutorial] && self.isEventFollowingByMe) {
            if(self.followCell){
                [TMDataStore setObjectWithUserIDForObject:@"true" key:CONNECT_TUTORIAL];
                [self.followCell showConnectTutorial];
                [self.collectionView bringSubviewToFront:self.followCell];
            }
        }
        self.isForceFullyReload = true;
        [self.collectionView reloadData];
        
        // tracking function called
        NSString *eventType = (self.isEventFollowingByMe) ? @"follow_event" : @"unfollow_event";
        [self trackExperienceDetailEvent:eventType eventInfo:nil];
        
        [self.categoryListManager sendUserAction:params eventBlock:^(BOOL status) {
            if(!status) {
                // show Toast
                self.isEventFollowingByMe = !self.isEventFollowingByMe;
                self.isForceFullyReload = true;
                [self.collectionView reloadData];
                [self showToastForFailureAction];
                
                // animate the follow button
                [self animateFollowView];
            }else {

            }
        }];
    }
    else{
        // show toast
        [self showToastForNoNetwork];
    }
}

-(void)animateFollowView {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat: ( 90 * M_PI / 180 )];
    animation.duration = 0.5f;
    animation.delegate = self;
    [self.followView.layer addAnimation:animation forKey:@"SpinAnimation"];
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if(flag) {
        NSArray *viewsToRemove = [self.followView subviews];
        for (UIView *v in viewsToRemove) {
            [v removeFromSuperview];
        }
    
        CGFloat width = (self.view.bounds.size.width-50)*0.5-12;
        NSString *followText = @"Follow";
        NSString *imageName = @"follow";
        UIColor *viewColor = [UIColor likeColor];
        if(self.isEventFollowingByMe) {
            followText = @"Following";
            imageName = @"like";
            viewColor = [UIColor colorWithRed:0.867 green:0.867 blue:0.867 alpha:1];
        }
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(width, NSUIntegerMax);
        CGRect rect = [followText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
        CGFloat size = 12;
        CGFloat height = 12;
        if(self.isEventFollowingByMe) {
            size = 17; height = 12;
        }
        CGFloat xPos = (width-(size+rect.size.width+5))/2;
        UIImageView *followImgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, (44-height)/2, size, height)];
        followImgView.image = [UIImage imageNamed:imageName];
        [self.followView addSubview:followImgView];
    
        UILabel *followLbl = [[UILabel alloc] initWithFrame:CGRectMake(followImgView.frame.origin.x+size+5, (44-rect.size.height)/2, rect.size.width, rect.size.height)];
        followLbl.text = followText;
        followLbl.font = [UIFont systemFontOfSize:15];
        followLbl.textColor = [UIColor whiteColor];
        [self.followView addSubview:followLbl];
        
        self.followView.backgroundColor = viewColor;
    }
}

-(void)showToastForNoNetwork {
    [TMToastView showToastInParentView:self.view withText:TM_INTERNET_NOTAVAILABLE_MSG withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
}

-(void)showToastForFailureAction {
    if(!self.isEventFollowingByMe) {
        // unfollow message
        [TMToastView showToastInParentView:self.view withText:@"We couldn\'t process your Follow request. Please try again!" withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
    }
    
}

-(void)showToastForLikeaProfile {
    [TMToastView showToastInParentView:self.view withText:@"You have \'Liked\' XXXXX. Wait for a \'Like\' back to start a conversation." withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
}

-(void)showToastForHideaProfile {
    [TMToastView showToastInParentView:self.view withText:@"This profile has been permanently removed from your matches." withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
}

-(void)showToastForMutualLike {
    [TMToastView showToastInParentView:self.view withText:@"Yay! Looks like you both ‘Liked’ each other!" withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
}

#pragma mark TMProfileViewDelegate method
-(void)didLikeProfile {
    // change the like status for profile at index
    self.isForceFullyReload = true;
    TMEventMatch *eventMatch =  self.eventDealObj.matchData[self.index];
    eventMatch.likedByMe = true;
    [self.eventDealObj.matchData replaceObjectAtIndex:self.index withObject:eventMatch];
    [self.collectionView reloadData];
    if(eventMatch.hasLiked){
        [self showToastForMutualLike];
    }else {
        [self showToastForLikeaProfile];
    }
    
    // tracking function called
    NSString *eventType = @"like_match";
    NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id,@"attendee_list":@(self.eventDealObj.matchData.count)};
    [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
}

-(void)didHideProfile {
    
    // tracking function called
    NSString *eventType = @"hide_match";
    TMEventMatch *eventMatch = [self.eventDealObj.matchData objectAtIndex:self.index];
    NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id,@"attendee_list":@(self.eventDealObj.matchData.count)};
    [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
    
    // remove this profile from list
    self.isForceFullyReload = true;
    [self.eventDealObj.matchData removeObjectAtIndex:self.index];
    if(self.eventDealObj.matchData.count <=0) {
        self.eventDealObj.isMutualViewAvail = false;
    }
    [self.collectionView reloadData];
    [self showToastForHideaProfile];
}

-(void)isMutualMatch:(TMEventMatch *)eventMatch mutualMatchDict:(NSDictionary *)mutualMatchDict{
    NSUInteger index = [self.eventDealObj.matchData indexOfObject:eventMatch];
    if(index != NSNotFound) {
        self.isForceFullyReload = true;
        eventMatch.isMutualMatch = true;
        eventMatch.matchName = mutualMatchDict[@"name"];
        eventMatch.messageConvFullURL = mutualMatchDict[@"msg_url"];
        [self.eventDealObj.matchData replaceObjectAtIndex:index withObject:eventMatch];
        [self.collectionView reloadData];
        
        // tracking function called
        NSString *eventType = @"likeback_event";
        NSDictionary *eventInfoDict = @{@"match_id":eventMatch.user_id};
        [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
    }
}

#pragma mark tutorial methods
-(BOOL)didShowFollowTutorial {
    if([TMDataStore containsObjectWithUserIDForKey:FOLLOW_TUTORIAL]) {
        int count = [[TMDataStore retrieveObjectWithUserIDForKey:FOLLOW_TUTORIAL] intValue];
        if(count >= 2) {
            return true;
        }else {
            count = count+1;
            [TMDataStore setObjectWithUserIDForObject:@(count) key:FOLLOW_TUTORIAL];
            return false;
        }
    }else {
        [TMDataStore setObjectWithUserIDForObject:@"1" key:FOLLOW_TUTORIAL];
        return false;
    }
    return false;
}
-(BOOL)didShowConnectTutorial {
    if([TMDataStore containsObjectWithUserIDForKey:CONNECT_TUTORIAL]) {
        BOOL status = [[TMDataStore retrieveObjectWithUserIDForKey:CONNECT_TUTORIAL] boolValue];
        return status;
    }
    return false;
}
-(void)showFollowTutorial {
    CGFloat tutWidth = 220;//180;
    CGFloat tutHeight = 140;//107;
    CGFloat xPos = self.view.bounds.size.width-tutWidth-20;
    CGFloat yPos = self.view.bounds.size.height-54-tutHeight-60;
    
    if(!self.followTutorial) {
        self.followTutorial = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, tutWidth, tutHeight)];
        [self.view addSubview:self.followTutorial];
        
        NSString *text = @"Follow events to discover\npeeps and stay updated.";
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.view.bounds), tutHeight);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        UIView *curveView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width+50, rect.size.height+30)];
        curveView.backgroundColor = [UIColor blackColor];
        curveView.layer.cornerRadius = 30;
        [self.followTutorial addSubview:curveView];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake((curveView.frame.size.width-rect.size.width)/2, (curveView.frame.size.height-rect.size.height)/2, rect.size.width, rect.size.height)];
        textLabel.numberOfLines = 0;
        textLabel.textColor = [UIColor whiteColor];
        textLabel.text = text;
        textLabel.font = [UIFont systemFontOfSize:16];
        textLabel.textAlignment = NSTextAlignmentCenter;
        [self.followTutorial addSubview:textLabel];
        
        CGFloat verticalLineWidth = 3;
        UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake((curveView.frame.size.width-verticalLineWidth)/2+50, CGRectGetMaxY(curveView.frame)-2, verticalLineWidth, 50)];
        verticalLine.backgroundColor = [UIColor blackColor];
        [self.followTutorial addSubview:verticalLine];
        
        CGFloat circleWidth = 15;
        UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake((CGRectGetMaxX(verticalLine.frame)-(circleWidth-verticalLineWidth)/2-3), CGRectGetMaxY(verticalLine.frame)-2, circleWidth, circleWidth)];
        circleView.layer.cornerRadius = circleWidth/2;
        circleView.backgroundColor = [UIColor blackColor];
        [self.followTutorial addSubview:circleView];
        
        [self.view bringSubviewToFront:self.followTutorial];
    }

    //[TMDataStore setObjectWithUserIDForObject:@"true" key:FOLLOW_TUTORIAL];

    [UIView animateWithDuration:0.5 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame = self.followTutorial.frame;
                         frame.origin.y = frame.origin.y+78;
                         self.followTutorial.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.75
                                               delay: 3.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.followTutorial.alpha = 0;
                                          }
                                          completion:^(BOOL finished){
                                              [self.followTutorial removeFromSuperview];
                                              self.followTutorial = nil;
                                          }];
                         
                     }];
}

#pragma mark TMProfilePhotoHeader delegate method

-(void)didTapPhotoHeader:(NSArray *)images withCurrentIndex:(NSInteger)currentIndex
{
    TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] init];
    fullScreenViewCon.currentIndex = currentIndex;
    [self.navigationController pushViewController:fullScreenViewCon animated:YES];
    [fullScreenViewCon loadDateSpotImagesFromArray:images];
}

#pragma mark TMShareViewDelegate Method
-(void)didRemoveFromSuperView:(NSString *)appName {
    if ([[UIApplication sharedApplication].keyWindow viewWithTag:110390]) {
        [[[UIApplication sharedApplication].keyWindow viewWithTag:110390] removeFromSuperview];
    }
    
    if([appName isEqualToString:@"activity"]) {
        NSURL *URL = [[NSURL alloc] initWithString:self.eventDealObj.shareURL];
        
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[self.eventDealObj.shareText, URL]
                                                                                                applicationActivities:nil];
        activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop, UIActivityTypeAddToReadingList,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,UIActivityTypePostToFacebook,UIActivityTypePostToFlickr,UIActivityTypePostToTwitter,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypePrint];
        NSString *subject = [NSString stringWithFormat:@"I\'ll be at %@ on %@",self.eventDealObj.header.name,self.eventDealObj.header.date];
        [activityViewController setValue:subject forKey:@"Subject"];
        [self.navigationController presentViewController:activityViewController
                                                    animated:YES
                                                  completion:nil];
    }
    else if([appName isEqualToString:@"whatsapp"]) {
        
        NSString *msg = [self.eventDealObj.shareText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *msg1 = [msg stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        NSString *link = [self.eventDealObj.shareURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        NSString *replacedStr = [link stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
        NSString *replacedStr1 = [replacedStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        
        NSString *text = [NSString stringWithFormat:@"whatsapp://send?text=%@%@",msg1,replacedStr1];
        NSURL *url = [[NSURL alloc] initWithString:text];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"There was an error connecting to WhatsApp. Please make sure that it is installed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            });
        }
    }
    else if([appName isEqualToString:@"messenger"]) {
        [self shareViaMessenger];
    }
    else if([appName isEqualToString:@"sms"]) {
        [self showSMS];
    }
    else if([appName isEqualToString:@"mail"]) {
        
        NSString *msg = [self.eventDealObj.shareText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *msg1 = [msg stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        NSString *link = [self.eventDealObj.shareURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        NSString *replacedStr = [link stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
        NSString *replacedStr1 = [replacedStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        
        NSString *body = [NSString stringWithFormat:@"%@%@", msg1, replacedStr1];
        NSString *subject = [NSString stringWithFormat:@"I\'ll be at %@ on %@",self.eventDealObj.header.name,self.eventDealObj.header.date];
        NSString *encodeSub = [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodeSub1 = [encodeSub stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        NSString *text =  [NSString stringWithFormat:@"mailto:?subject=%@%@%@",encodeSub1,@"&body=",body];
        NSURL *url = [[NSURL alloc] initWithString:text];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    
    // tracking function called
    NSString *eventType = (self.isEventFollowingByMe) ? @"share_outside_followed" : @"share_outside";
    NSDictionary *eventInfoDict = @{@"app_name":appName};
    [self trackExperienceDetailEvent:eventType eventInfo:eventInfoDict];
}

-(void)launchOnetoOneConv:(TMEventMatch *)matchData {
    self.matchData = matchData;
    if ([[UIApplication sharedApplication].keyWindow viewWithTag:110390]) {
        [[[UIApplication sharedApplication].keyWindow viewWithTag:110390] removeFromSuperview];
    }
    NSString *msg = [NSString stringWithFormat:@"Share this experience with %@?",self.matchData.matchName];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView addButtonWithTitle:@"Yes"];
        [alertView show];
    });
}

- (void)showSMS {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[];
    NSString *message = [NSString stringWithFormat:@"%@ %@ ", self.eventDealObj.shareText, self.eventDealObj.shareURL];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
            break;
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

// share via messenger
-(void)shareViaMessenger {
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [[NSURL alloc] initWithString:self.eventDealObj.shareURL];
    params.name = self.eventDealObj.shareText;
    if ([FBDialogs canPresentMessageDialogWithParams:params]) {
        [FBDialogs presentMessageDialogWithParams:params clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            if(error != nil){
                // show error
            }else{
                
            }
        }];
    }else {
        TMLOG(@"messenger is not present");
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"There was an error connecting to Messenger. Please make sure that it is installed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        });
    }
}


/**
 * Shows the empty deal list content to the user
 */
- (void) showEmptyDealListMessage {
    
    [self hideEmptyDealListMessage];
    
    UILabel* expiredMessageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (IS_IPHONE_6_PLUS)?(self.view.frame.size.width*4/5):(self.view.frame.size.width*4.5/5), 80)];
    expiredMessageLbl.backgroundColor = [UIColor clearColor];
    expiredMessageLbl.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    expiredMessageLbl.numberOfLines = 3;
    expiredMessageLbl.tag = DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE;
    expiredMessageLbl.textColor = [UIColor colorWithRed:(112.0/255.0) green:(112.0/255.0) blue:(112.0/255.0) alpha:1.0];
    expiredMessageLbl.font = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?[UIFont systemFontOfSize:16]:[UIFont systemFontOfSize:15];
    expiredMessageLbl.textAlignment = NSTextAlignmentCenter;
    expiredMessageLbl.text = @"Oops! This experience has expired.\nCheck out other experiences on TM Scenes!";
    [self.view addSubview:expiredMessageLbl];
    
    //expired event image view
    
    CGFloat coffeeLogoSize = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?84:80;
    
    UIImageView* logoLoaderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, coffeeLogoSize, coffeeLogoSize)];
    logoLoaderImageView.image = [UIImage imageNamed:@"expired"];
    logoLoaderImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoLoaderImageView.backgroundColor = [UIColor clearColor];
    logoLoaderImageView.tag = DEAL_LIST_EMPTY_DEAL_LIST_IMAGE;
    logoLoaderImageView.center = CGPointMake(self.view.bounds.size.width/2, expiredMessageLbl.frame.origin.y - 25);
    [self.view addSubview:logoLoaderImageView];

}


/**
 * Shows the empty deal list content to the user
 */
- (void) hideEmptyDealListMessage {
    if ([self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE]) {
        [[self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE] removeFromSuperview];
    }
    if ([self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_IMAGE]) {
        [[self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_IMAGE] removeFromSuperview];
    }
}

// pragma mark Tracking Function
-(void)trackExperienceDetailEvent:(NSString *)eventType eventInfo:(NSDictionary *)eventInfo
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMEventListViewController" forKey:@"screenName"];
    [eventDict setObject:@"scenes" forKey:@"eventCategory"];
    [eventDict setObject:eventType forKey:@"eventAction"];
    [eventDict setObject:self.eventId forKey:@"status"];
    [eventDict setObject:self.eventId forKey:@"label"];
    if(eventInfo) {
        [eventDict setObject:eventInfo forKey:@"event_info"];
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}

@end

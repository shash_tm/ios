//
//  TMShareNativeView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMShareNativeViewDelegate;

@interface TMShareNativeView : UIView

@property(nonatomic,weak)id<TMShareNativeViewDelegate>delegate;

@end

@protocol TMShareNativeViewDelegate <NSObject>

-(void)removeNativeViewFromSuperView:(NSString*)appName;

@end

//
//  TMCategoryListCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMCategoryList;

@interface TMCategoryListCollectionViewCell : UICollectionViewCell

-(void)configureListUI:(TMCategoryList *)categoryListData;

@end

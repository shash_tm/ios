//
//  TMEventCategoryListViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventCategoryListViewController.h"
#import "TMCategoryListManager.h"
#import "TMCategoryListCollectionViewCell.h"
#import "TMCategoryList.h"
#import "TMEventListViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMLocationPreferenceViewController.h"
#import "TMSwiftHeader.h"
#import "TMAnalytics.h"
#import "TMUserSession.h"

#define CATEGORY_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"CategoryListCollectionViewCell"
#define DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG 1001
#define DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE 1002
#define DEAL_LIST_EMPTY_DEAL_LIST_IMAGE 1003

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@interface TMEventCategoryListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, strong) UICollectionView* eventListCollectionView;
@property(nonatomic, strong) NSArray* eventListArray;
@property(nonatomic, strong) UIImageView *musicView;
@property(nonatomic, strong) UIImageView *faceView;
//category list manager
@property(nonatomic, strong) TMCategoryListManager* categoryListManager;

@property(nonatomic, assign) BOOL fromNotification;
@property(nonatomic, strong) NSString* categoryId;
@property(nonatomic, strong) NSString* titleText;

@end

@implementation TMEventCategoryListViewController

- (instancetype) init {
    self = [super init];
    if(self) {
        return self;
    }
    return nil;
}

- (instancetype)initWithCategoryId:(NSString *)categoryId withTitleText:(NSString *)titleText {
    self = [super init];
    if(self) {
        self.fromNotification = true;
        self.categoryId = categoryId;
        self.titleText = titleText;
        return self;
    }
    return nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self addLeftNavigationButtonItem];
    self.title = @"TM Scenes";
    
    //add the collection view
    [self createDealListCollectionView];
    
    [self makeCategoryListRequest];
    
    //track the event
    [self trackCategoryEvent];
}

-(void)addLeftNavigationButtonItem {
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer , leftBarButton] animated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews {
    //update the frame for the deal list collection view
    self.eventListCollectionView.frame = self.view.bounds;
}

-(TMCategoryListManager *)categoryListManager {
    if(_categoryListManager == nil){
        _categoryListManager = [[TMCategoryListManager alloc] init];
    }
    return _categoryListManager;
}

#pragma mark - Deal List UI creation methods

- (void) createDealListCollectionView {
    //add the collection view
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 10;//7.5;
    flowLayout.minimumInteritemSpacing = 0;
    
    self.eventListCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.eventListCollectionView.backgroundColor = [UIColor whiteColor];
    self.eventListCollectionView.dataSource = self;
    self.eventListCollectionView.delegate = self;
    [self.eventListCollectionView registerClass:[TMCategoryListCollectionViewCell class] forCellWithReuseIdentifier:CATEGORY_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [self.view addSubview:self.eventListCollectionView];
}

#pragma mark - Cancel button callback method

/**
 * Callback method when cancel button is pressed
 */
- (void) cancelButtonPressed {
    //dismiss the deal list view controller
    self.eventListCollectionView = nil;
    [self.navigationController popViewControllerAnimated:true];
}

-(void)makeCategoryListRequest {
    
    if([self.categoryListManager isNetworkReachable]) {
        [self configureViewForDataLoadingStart];
        [self.categoryListManager getCategoryListData:^(NSArray *categoryListArr, TMError *error) {
            [self configureViewForDataLoadingFinish];
            if(categoryListArr != nil) {
                self.eventListArray = [categoryListArr copy];
                [self.eventListCollectionView reloadData];
                if (self.eventListArray.count == 0) {
                    //empty list
                    [self showEmptyDealListMessage];
                }
            }else {
                //error case
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewAtError];
                }
                else {
                    //unknwon error
                    [self showRetryViewAtUnknownError];
                }
            }
            
        }];
    }
    else {
        [self showRetryViewAtError];
    }
}

#pragma mark - Empty Deal List Message methods

/**
 * Shows the empty deal list content to the user
 */
- (void) showEmptyDealListMessage {
    
    [self hideEmptyDealListMessage];
    
    UILabel* emptyDealListMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (self.view.bounds.size.width*4.5/5), 80)];
    emptyDealListMessageLabel.backgroundColor = [UIColor clearColor];
    emptyDealListMessageLabel.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    emptyDealListMessageLabel.numberOfLines = 3;
    emptyDealListMessageLabel.tag = DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE;
    emptyDealListMessageLabel.textColor = [UIColor colorWithRed:(112.0/255.0) green:(112.0/255.0) blue:(112.0/255.0) alpha:1.0];
    emptyDealListMessageLabel.font = [UIFont systemFontOfSize:15];
    emptyDealListMessageLabel.textAlignment = NSTextAlignmentCenter;
    emptyDealListMessageLabel.text = @"There\'re no experiences in your area yet!";//@"Oops! There\'re no events in your area yet!";
    [self.view addSubview:emptyDealListMessageLabel];
    
    //coffee background image view
    
//    CGFloat coffeeLogoSize = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?72:54;
    
//    UIImageView* logoLoaderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, coffeeLogoSize, coffeeLogoSize)];
//    logoLoaderImageView.image = [UIImage imageNamed:@"date_list_loader_gray_icon"];
//    logoLoaderImageView.contentMode = UIViewContentModeScaleAspectFit;
//    logoLoaderImageView.backgroundColor = [UIColor clearColor];
//    logoLoaderImageView.tag = DEAL_LIST_EMPTY_DEAL_LIST_IMAGE;
//    logoLoaderImageView.center = CGPointMake(self.view.bounds.size.width/2, emptyDealListMessageLabel.frame.origin.y - 18);
//    [self.view addSubview:logoLoaderImageView];
}


/**
 * Shows the empty deal list content to the user
 */
- (void) hideEmptyDealListMessage {
    if ([self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE]) {
        [[self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE] removeFromSuperview];
    }
}

#pragma mark - Deal List Loader View methods

/**
 * Shows the deal list loader view
 */
- (void) showDealListLoaderView {
    
    //hide any deal list loader view if present
    [self hideDealListLoaderView];
    
    UIImageView* dealListLoaderBackgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    dealListLoaderBackgroundImageView.image = [UIImage imageNamed:@"date_list_loader_background"];
    dealListLoaderBackgroundImageView.tag = DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG;
    
    
    self.musicView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 42)];
    self.musicView.image = [UIImage imageNamed:@"music_loader"];
    self.musicView.contentMode = UIViewContentModeScaleAspectFit;
    self.musicView.backgroundColor = [UIColor clearColor];
    self.musicView.center = CGPointMake(self.view.bounds.size.width/2-25, (self.view.bounds.size.height-(108+56))/2);
    [dealListLoaderBackgroundImageView addSubview:self.musicView];
    
    self.faceView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 42)];
    self.faceView.image = [UIImage imageNamed:@"face_loader"];
    self.faceView.contentMode = UIViewContentModeScaleAspectFit;
    self.faceView.backgroundColor = [UIColor clearColor];
    self.faceView.center = CGPointMake(self.view.bounds.size.width/2+25, (self.view.bounds.size.height-(108+56))/2);
    [dealListLoaderBackgroundImageView addSubview:self.faceView];
    
    //loader message label
    UILabel* loaderMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x - self.view.bounds.size.width*7/20, self.faceView.frame.origin.y + self.faceView.bounds.size.height - 16, self.view.bounds.size.width*7/10, 108)];
    loaderMessageLabel.backgroundColor = [UIColor clearColor];
    loaderMessageLabel.textColor = [UIColor whiteColor];
    loaderMessageLabel.textAlignment = NSTextAlignmentCenter;
    loaderMessageLabel.numberOfLines = 3;
    loaderMessageLabel.font = [UIFont systemFontOfSize:16];//(IS_IPHONE_6_PLUS)?([UIFont systemFontOfSize:16]):(IS_IPHONE_6?([UIFont systemFontOfSize:15]):(([UIScreen mainScreen].bounds.size.height > 500)?([UIFont systemFontOfSize:14]):([UIFont systemFontOfSize:13])));
    loaderMessageLabel.text = @"Looking for experiences\nyou will love...";
    [dealListLoaderBackgroundImageView addSubview:loaderMessageLabel];
    
    [self.view addSubview:dealListLoaderBackgroundImageView];
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat:( -45 * M_PI  / 180)];
    animation.toValue = [NSNumber numberWithFloat: ( 45 * M_PI  / 180)];
    animation.duration = 0.7f;
    animation.repeatCount = INFINITY;
    animation.autoreverses = true;
    [self.musicView.layer addAnimation:animation forKey:@"SpinAnimation"];
    
    CABasicAnimation* animation1 = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation1.fromValue = [NSNumber numberWithFloat:( 45 * M_PI  / 180)];
    animation1.toValue = [NSNumber numberWithFloat: ( -45 * M_PI  / 180)];
    animation1.duration = 0.7f;
    animation1.repeatCount = INFINITY;
    animation1.autoreverses = true;
    [self.faceView.layer addAnimation:animation1 forKey:@"SpinAnimation"];
    
}

/**
 * Hides the deal list loader view
 */
- (void) hideDealListLoaderView {
    if ([self.view viewWithTag:DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG]) {
        [[self.view viewWithTag:DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG] removeFromSuperview];
    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(makeCategoryListRequest) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(makeCategoryListRequest) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)configureViewForDataLoadingStart {
    
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self hideEmptyDealListMessage];
    [self showDealListLoaderView];
}

-(void)configureViewForDataLoadingFinish {
    [self hideDealListLoaderView];
    if(self.retryView.superview) {
        [self removeRetryView];
    }
}

#pragma mark - UICollectionView methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    int requiredHeight = 170;//200;
    
    int cellSideMargin = 8;
    
    int requredWidth = self.eventListCollectionView.bounds.size.width - 2*cellSideMargin;
    
    return CGSizeMake(requredWidth, requiredHeight);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.eventListArray.count;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 0, 8, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMCategoryListCollectionViewCell* categoryListCell = [self.eventListCollectionView dequeueReusableCellWithReuseIdentifier:CATEGORY_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    TMCategoryList* categoryObj = [self.eventListArray objectAtIndex:indexPath.item];
    [categoryListCell configureListUI:categoryObj];
    return categoryListCell;
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TMCategoryList* categoryObj = [self.eventListArray objectAtIndex:indexPath.item];
    self.categoryId = categoryObj.category_id;
    self.titleText = categoryObj.title;
    [self launchEventList];
}

-(void)launchEventList {
    TMEventListViewController *listViewCon = [[TMEventListViewController alloc] initWithCategoryId:self.categoryId title:self.titleText];
    TMLocationPreferenceViewController *locView = [[TMLocationPreferenceViewController alloc] initWithGeo:true];
    
    UINavigationController *navViewCon = [[UINavigationController alloc] initWithRootViewController:listViewCon];
    ECSlidingViewController *viewCon = [[ECSlidingViewController alloc] initWithTopViewController:navViewCon];
    viewCon.underRightViewController = locView;
    viewCon.anchorLeftRevealAmount = self.view.bounds.size.width-60;
    self.navigationController.navigationBarHidden = true;
    [self.navigationController pushViewController:viewCon animated:true];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    if(self.fromNotification){
        [self launchEventList];
        self.fromNotification = false;
    }
}

// pragma mark Tracking Function
-(void)trackCategoryEvent
{
    TMUserSession *userSession = [TMUserSession sharedInstance];
    NSString * city =  [userSession.user city];
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMEventCategoryListViewController" forKey:@"screenName"];
    [eventDict setObject:@"scenes" forKey:@"eventCategory"];
    [eventDict setObject:@"list_category" forKey:@"eventAction"];
    if(city){
        [eventDict setObject:city forKey:@"status"];
        [eventDict setObject:city forKey:@"label"];
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}



@end

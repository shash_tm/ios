//
//  TMShareView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMEventMatch;
@protocol TMShareViewDelegate;

@interface TMShareView : UIView

@property(nonatomic,weak)id<TMShareViewDelegate>shareDelegate;

@end

@protocol TMShareViewDelegate <NSObject>

-(void)didRemoveFromSuperView:(NSString *)appName;
-(void)launchOnetoOneConv:(TMEventMatch *)matchData;

@end

//
//  TMEventCategoryListViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMEventCategoryListViewController : TMBaseViewController

-(instancetype)initWithCategoryId:(NSString *)categoryId withTitleText:(NSString *)titleText;

@end

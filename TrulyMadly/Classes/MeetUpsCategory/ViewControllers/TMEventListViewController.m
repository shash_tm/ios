//
//  TMEventListViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 09/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventListViewController.h"
#import "TMLocationPreferenceViewController.h"
#import "TMBadgeView.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMEventList.h"
#import "TMCategoryList.h"
#import "TMEventListCollectionViewCell.h"
#import "TMSwiftHeader.h"
#import "TMEventDetailViewController.h"
#import "TMLog.h"

#import "TMRecommendView.h"
#import "TMFollowView.h"

#import "TMAnalytics.h"
#import "UIColor+TMColorAdditions.h"

#define EVENT_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"EventListCollectionViewCell"
#define DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG 1001
#define DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE 1002
#define DEAL_LIST_EMPTY_DEAL_LIST_IMAGE 1003
#define RECOMMEND_LABEL_TAG 1004
#define FOLLOW_LABEL_TAG 1005

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@interface TMEventListViewController ()<TMLocationPreferenceViewControllerDelegate, UIScrollViewDelegate, TMRecommendViewDelegate, TMFollowViewDelegate, TMEventDetailViewControllerDelegate>

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) TMBadgeView* locationBadgeView;
@property(nonatomic, strong) UIView *headerView;
@property(nonatomic, strong) UIView *headerView1;
@property(nonatomic, strong) UIView *recommendedLineView;
@property(nonatomic, strong) UIView *followLineView;
@property(nonatomic, strong) TMRecommendView *recommendedViewCon;
@property(nonatomic, strong) TMFollowView *followViewCon;
@property(nonatomic, strong) UIView *overlayView;
@property(nonatomic, strong) NSMutableArray *zoneArray;

@property(nonatomic, strong) NSArray* zoneIds;
@property(nonatomic, strong) NSDictionary *geoLoc;
@property(nonatomic, strong) NSString *cityName;
@property(nonatomic, strong) NSString *categoryId;
@property(nonatomic, strong) NSString *titleText;

@property(nonatomic, assign) NSInteger currentViewIndex;
@property(nonatomic, assign) BOOL saveZones;
@property(nonatomic, assign) BOOL saveGeo;
@property(nonatomic, assign) BOOL isRequestInProgress;
@property(nonatomic, assign) BOOL showBlooper;
@property(nonatomic, assign) BOOL reloadRightMenu;

@end

@implementation TMEventListViewController

-(instancetype)initWithCategoryId:(NSString *)categoryId title:(NSString *)title {
    self = [super init];
    if(self) {
        self.categoryId = categoryId;
        self.titleText = title;
        self.zoneArray = [[NSMutableArray alloc] init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer , leftButton] animated:true];

    self.title = self.titleText;
        
    [self addScrollView];
    [self loadAllViews];
    
    [self trackExperienceListEvent];
}

-(void)addScrollView {
    
    /*self.headerView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+64.0, self.view.bounds.size.width/2, 44)];
    self.headerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.headerView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToRecommendedView)];
    [self.headerView addGestureRecognizer:tapGesture];
    
    self.headerView1 = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2, self.view.frame.origin.y+64.0, self.view.bounds.size.width/2, 44)];
    self.headerView1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.headerView1];
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToFollowView)];
    [self.headerView1 addGestureRecognizer:tapGesture1];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.headerView.frame.size.width, 40)];
    lbl.text = @"Recommended";
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor lightGrayColor];
    lbl.tag = RECOMMEND_LABEL_TAG;
    [self.headerView addSubview:lbl];
    
    self.recommendedLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 42, self.headerView.frame.size.width, 2)];
    self.recommendedLineView.backgroundColor = [UIColor likeColor];//[UIColor colorWithRed:72.0/255.0 green:122.0/255.0 blue:251.0/255.0 alpha:1.0];
    [self.headerView addSubview:self.recommendedLineView];
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.headerView.frame.size.width, 40)];
    lbl1.text = @"Following";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.textColor = [UIColor lightGrayColor];
    lbl1.tag = FOLLOW_LABEL_TAG;
    [self.headerView1 addSubview:lbl1];
    
    self.followLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 42, self.headerView.frame.size.width, 2)];
    self.followLineView.backgroundColor = [UIColor clearColor];
    [self.headerView1 addSubview:self.followLineView];*/
    
    //self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,self.headerView.frame.origin.y+self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height-44)];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    //self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.scrollView];
    
    CGSize size = CGSizeMake((self.view.bounds.size.width),
                             self.view.bounds.size.height-30);
    self.scrollView.contentSize = size;
}

-(void)loadAllViews {
    CGFloat xPos = 0;
    self.recommendedViewCon = [[TMRecommendView alloc] initWithFrameAndCategory:CGRectMake(xPos, 12, self.view.bounds.size.width, self.view.bounds.size.height-30) categoryId:self.categoryId];
    self.recommendedViewCon.recommendViewDelegate = self;
    [self.scrollView addSubview:self.recommendedViewCon];
    
    /*if(!self.recommendedViewCon) {
        self.recommendedViewCon = [[TMRecommendView alloc] initWithFrameAndCategory:CGRectMake(xPos, 0, self.view.bounds.size.width, self.view.bounds.size.height-54) categoryId:self.categoryId];
        self.recommendedViewCon.recommendViewDelegate = self;
        [self.scrollView addSubview:self.recommendedViewCon];
        xPos = xPos + self.recommendedViewCon.frame.size.width;
    }
    if(!self.followViewCon) {
        self.followViewCon = [[TMFollowView alloc] initWithFrameAndCategory:CGRectMake(xPos, 0, self.view.bounds.size.width, self.view.bounds.size.height-54) categoryId:self.categoryId];
        self.followViewCon.followViewDelegate = self;
        [self.scrollView addSubview:self.followViewCon];
    }*/
}

#pragma mark UIScorllViewDelegate Method
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    self.currentViewIndex = lround(fractionalPage);
    TMLOG(@" current page is %ld",(long)self.currentViewIndex);
    if(self.currentViewIndex == 0) {
        self.recommendedLineView.backgroundColor = [UIColor likeColor];//[UIColor colorWithRed:72.0/255.0 green:122.0/255.0 blue:251.0/255.0 alpha:1.0];
        self.followLineView.backgroundColor = [UIColor clearColor];
        if(self.cityName) {
            [self addRightNavigationItem];
        }
    }else {
        self.followLineView.backgroundColor = [UIColor likeColor];//[UIColor colorWithRed:72.0/255.0 green:122.0/255.0 blue:251.0/255.0 alpha:1.0];
        self.recommendedLineView.backgroundColor = [UIColor clearColor];
        self.navigationItem.rightBarButtonItem = nil;
        self.locationBadgeView = nil;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // tracking call
    [self trackExperienceListEvent];
}

-(UIView*)overlayView {
    if(!_overlayView) {
        _overlayView = [[UIView alloc] initWithFrame:self.view.bounds];
        _overlayView.backgroundColor = [UIColor blackColor];
        _overlayView.alpha = 0.5;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [_overlayView addGestureRecognizer:tapGesture];
    }
    return _overlayView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addRightNavigationItem
{
    if(!self.locationBadgeView) {
        self.locationBadgeView = [[TMBadgeView alloc] initWithFrame:CGRectMake(0, 0, 54, 44)
                                                          withImage:[UIImage imageNamed:@"location_blue"]
                                                     withImageFrame:CGRectMake(22, 12, 18, 24)];
        [self.locationBadgeView.badgeButton addTarget:self action:@selector(locationPref) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *rightMenuButton1 = [[UIBarButtonItem alloc]
                                             initWithCustomView:self.locationBadgeView];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -16;
        
        NSArray *itemarrs = [NSArray arrayWithObjects:negativeSpacer,rightMenuButton1, nil];
        [self.navigationItem setRightBarButtonItems:itemarrs];
    }
    [self.locationBadgeView setBadgeNotificationStatusForLocation:self.showBlooper];
}

-(void)locationPref
{
    if(!self.isRequestInProgress) {
        if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered) {
            [self.slidingViewController anchorTopViewToLeftAnimated:YES];
            [self.view addSubview:self.overlayView];
            [self.view bringSubviewToFront:self.overlayView];
            TMLocationPreferenceViewController *locPrefVCont = (TMLocationPreferenceViewController *)self.slidingViewController.underRightViewController;
            locPrefVCont.locationPreferenceDelegate = self;
            [locPrefVCont initWithZoneArray:self.zoneArray cityName:self.cityName];
            if(!self.showBlooper){
                [locPrefVCont defaultState];
            }
            
            // track location function called
            [self trackLocation:@"filter_clicked" eventInfo:nil];
        }
        else {
            [self resetLocationView];
        }
    }
}

-(void)tapAction
{
    [self resetLocationView];
}

-(void)resetLocationView
{
    [self.slidingViewController resetTopViewAnimated:YES];
    self.view.userInteractionEnabled = true;
    [self.overlayView removeFromSuperview];
}

#pragma mark - Cancel button callback method

/**
 * Callback method when cancel button is pressed
 */
- (void) cancelButtonPressed {
    //dismiss the deal list view controller
   // self.eventListCollectionView = nil;
   // [self.navigationController dismissViewControllerAnimated:true completion:nil];
    [self.slidingViewController.navigationController popViewControllerAnimated:true];
}

-(int)processZoneList:(NSArray *)zoneList selectedZones:(NSArray *)selectedZones
{
    self.showBlooper = false;
    [self.zoneArray removeAllObjects];
    int counter = 0;
    BOOL isZoneSelected = false;
   
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"-1",@"zone_id",@"Doesn\'t Matter",@"name",[NSNumber numberWithBool:false],@"selected",nil];
    [self.zoneArray addObject:dict];
    for (int i=0; i<zoneList.count; i++) {
        NSMutableDictionary *zoneDict = [[NSMutableDictionary alloc] initWithDictionary:zoneList[i]];
        if([selectedZones containsObject:[zoneDict objectForKey:@"zone_id"]]) {
            zoneDict[@"selected"] = [NSNumber numberWithBool:true];
            isZoneSelected = true;
            self.showBlooper = true;
            counter++;
        }
        [self.zoneArray addObject:zoneDict];
    }
    
    if(!isZoneSelected){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.zoneArray[0]];
        dict[@"selected"] = [NSNumber numberWithBool:true];
        self.zoneArray[0] = dict;
    }
    return counter;
}

#pragma mark TMLocationPreferenceViewControllerDelegate method

-(void)didClickForApply:(NSArray *)zoneIdArr {
    self.reloadRightMenu = false;
    [self resetLocationView];
    if(self.recommendedViewCon) {
        [self.recommendedViewCon saveZoneIds:zoneIdArr];
    }
    if(self.locationBadgeView) {
        self.showBlooper = false;
        [self.locationBadgeView setBadgeNotificationStatusForLocation:self.showBlooper];
        
        if(zoneIdArr.count>0) {
            if(!(zoneIdArr.count == 1 && [zoneIdArr[0] intValue] == -1)) {
                self.showBlooper = true;
                [self.locationBadgeView setBadgeNotificationStatusForLocation:self.showBlooper];
            }
        }
    }
    // track location applied
    NSString * result = [[zoneIdArr valueForKey:@"description"] componentsJoinedByString:@" "];
    NSDictionary *eventInfoDict = @{@"location":result};
    [self trackLocation:@"filter_applied" eventInfo:eventInfoDict];
}

-(void)didClickForApplyWithGeo:(NSDictionary *)geoData {
    self.reloadRightMenu = false;
    [self resetLocationView];
    if(self.recommendedViewCon) {
        [self.recommendedViewCon saveGeoLoc:geoData];
    }
    if(self.locationBadgeView) {
        self.showBlooper = true;
        [self.locationBadgeView setBadgeNotificationStatusForLocation:self.showBlooper];
    }
    
    // track location applied
    NSDictionary *eventInfoDict = @{@"location":@"nearby"};
    [self trackLocation:@"filter_applied" eventInfo:eventInfoDict];
}

#pragma mark TMRecommendViewDelegate TMFollowViewDelegate method

-(void)didRequestCompleted:(NSDictionary *)eventListDic selectedZones:(NSArray *)selectedZones saveGeo:(BOOL)saveGeo {
    NSArray *zoneList = [eventListDic objectForKey:@"zoneList"];
    [self processZoneList:zoneList selectedZones:selectedZones];
    self.cityName = [eventListDic objectForKey:@"cityName"];
    
    if(saveGeo){
        self.showBlooper = saveGeo;
    }
//    if(self.showBlooper) {
//        [self.navigationItem setRightBarButtonItems:nil];
//        self.locationBadgeView = nil;
//    }
    if(self.currentViewIndex == 0){
        [self addRightNavigationItem];
    }
    // recommended count
    
    /*NSArray *eventArray = [[NSArray alloc] initWithArray:[eventListDic objectForKey:@"dealList"]];
    UILabel *lbl = [self.headerView viewWithTag:RECOMMEND_LABEL_TAG];
    if(eventArray.count > 0){
        lbl.text = [NSString stringWithFormat:@"Recommended (%lu)",(unsigned long)eventArray.count];
    }else{
        lbl.text = [NSString stringWithFormat:@"Recommended"];
    }*/
}

-(void)didFollowRequestCompleted:(NSDictionary *)eventListDic {
    // follow count
    NSArray *eventArray = [[NSArray alloc] initWithArray:[eventListDic objectForKey:@"dealList"]];
    UILabel *lbl = [self.headerView1 viewWithTag:FOLLOW_LABEL_TAG];
    if(eventArray.count > 0){
        lbl.text = [NSString stringWithFormat:@"Following (%lu)",(unsigned long)eventArray.count];
    }else{
        lbl.text = [NSString stringWithFormat:@"Following"];
    }
}

-(void)didSelectEvent:(TMEventDetailViewController *)eventDetailViewCon {
    eventDetailViewCon.eventDetailDelegate = self;
    [self.navigationController pushViewController:eventDetailViewCon animated:true];
}

#pragma mark TMEventDetailViewDelegate Methods
-(void)didReloadData {
    if(self.recommendedViewCon) {
        [self.recommendedViewCon makeEventListRequest];
    }
    if(self.followViewCon) {
        [self.followViewCon makeEventListRequest];
    }
}

-(void)moveToRecommendedView {
    if(self.currentViewIndex != 0) {
        self.currentViewIndex = 0;
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

-(void)moveToFollowView {
    if(self.currentViewIndex != 1) {
        self.currentViewIndex = 1;
        [self.scrollView setContentOffset:CGPointMake(self.view.bounds.size.width, 0) animated:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.headerView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width/2, 44);
    self.headerView1.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.origin.y, self.view.bounds.size.width/2, 44);
    self.scrollView.frame = CGRectMake(self.view.bounds.origin.x,
                                       CGRectGetMinY(self.view.bounds)+self.headerView.frame.size.height,
                                       self.view.bounds.size.width,
                                       self.view.bounds.size.height-self.headerView.frame.size.height);
    self.scrollView.contentSize = CGSizeMake((self.view.bounds.size.width*2),
                                             self.view.bounds.size.height-self.headerView.frame.size.height);
}

// pragma mark Tracking Function
-(void)trackExperienceListEvent
{
    NSString *eventType = (self.currentViewIndex == 0) ? @"list_event" : @"list_following";
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMEventListViewController" forKey:@"screenName"];
    [eventDict setObject:@"scenes" forKey:@"eventCategory"];
    [eventDict setObject:eventType forKey:@"eventAction"];
    [eventDict setObject:self.categoryId forKey:@"status"];
    [eventDict setObject:self.categoryId forKey:@"label"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}

// pragma mark Tracking Function
-(void)trackLocation:(NSString *)eventType eventInfo:(NSDictionary *)eventInfo
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMEventListViewController" forKey:@"screenName"];
    [eventDict setObject:@"scenes" forKey:@"eventCategory"];
    [eventDict setObject:eventType forKey:@"eventAction"];
    [eventDict setObject:self.categoryId forKey:@"status"];
    [eventDict setObject:self.categoryId forKey:@"label"];
    if(eventInfo) {
        [eventDict setObject:eventInfo forKey:@"event_info"];
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}


@end

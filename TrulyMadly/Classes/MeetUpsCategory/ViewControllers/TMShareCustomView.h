//
//  TMShareCustomView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMEventMatch;

@protocol TMShareCustomViewDelegate;

@interface TMShareCustomView : UIView

@property(nonatomic,weak)id<TMShareCustomViewDelegate>delegate;

-(instancetype)initWithFrame:(CGRect)frame userList:(NSArray *)userList;

@end

@protocol TMShareCustomViewDelegate <NSObject>

-(void)shareEventWithMatch:(TMEventMatch *)matchData;

@end
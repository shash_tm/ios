//
//  TMEventListCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 09/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TMEventList;

@interface TMEventListCollectionViewCell : UICollectionViewCell

- (void)configureEventCellUI:(TMEventList *)eventData;

//Method currently not in use due to UI changes.
-(void)configureListUI:(TMEventList *)eventData;

@end

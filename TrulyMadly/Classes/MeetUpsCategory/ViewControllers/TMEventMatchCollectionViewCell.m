//
//  TMEventMatchCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventMatchCollectionViewCell.h"
#import "TMEventMatchView.h"
#import "TMEventMatch.h"
#import "NSString+TMAdditions.h"

#define VIEW_WIDTH  70
#define VIEW_HEIGHT 75

@interface TMEventMatchCollectionViewCell ()<UIScrollViewDelegate>

@property(nonatomic, strong) UIScrollView *horizontalScrollView;
@property(nonatomic, strong) NSArray *eventMatchData;
@property(nonatomic, strong) UIView *connectTutorial;
@property(nonatomic, assign)BOOL showBlur;

@end

@implementation TMEventMatchCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}

/**
 * Initializes the UI component
 * to be called only once in the cell's lifetime
 */
- (void) initializeUIComponents {
    //initialize all labels and UI properties
    
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)self.eventMatchData.count];
    NSString *headerText = [[self.eventMatchData[0] getTermsHeaderText] stringByReplacingOccurrencesOfString:@"LIKE_COUNT"
                                                                                                  withString:count];
    CGFloat yPos = 15;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGSize constrintSize = CGSizeMake(self.frame.size.width-20, NSUIntegerMax);
    CGRect rect = [headerText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,
                                                                 yPos + ([self.eventMatchData[0] iconWidth]-20)/2,
                                                                 rect.size.width,
                                                                 rect.size.height)];
    headerTitle.font = [UIFont systemFontOfSize:15];
    headerTitle.textColor = [UIColor blackColor];
    headerTitle.backgroundColor = [UIColor clearColor];
    headerTitle.text = headerText;//[self.eventMatchData[0] getTermsHeaderText];
    headerTitle.numberOfLines = 0;
    [headerTitle sizeToFit];
    [self.contentView addSubview:headerTitle];
    
    self.horizontalScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yPos+12+7+rect.size.height, self.frame.size.width+10, VIEW_HEIGHT)];
    [self.horizontalScrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    self.horizontalScrollView.pagingEnabled = false;
    self.horizontalScrollView.delegate = self;
    self.horizontalScrollView.showsHorizontalScrollIndicator = NO;
    self.horizontalScrollView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.horizontalScrollView];
}

-(void)configureUI:(NSArray *)eventMatchData forcefullyReload:(BOOL)forcefullyReload showBlur:(BOOL)showBlur{
    self.showBlur = !showBlur;
    
    if(forcefullyReload) {
        NSArray *view = [self.contentView subviews];
        for (int i=0; i<view.count; i++) {
            [view[i] removeFromSuperview];
        }
        
        self.eventMatchData = eventMatchData;
        [self initializeUIComponents];
        NSInteger noOfProfiles = eventMatchData.count;
        self.horizontalScrollView.contentSize = CGSizeMake((VIEW_WIDTH)*noOfProfiles, VIEW_HEIGHT);
        [self addProfilesToScrollView];
    }else {
        if(!self.isCellConfigured) {
            self.isCellConfigured = true;
            self.eventMatchData = eventMatchData;
            
            [self initializeUIComponents];
            
            NSInteger noOfProfiles = eventMatchData.count;
            self.horizontalScrollView.contentSize = CGSizeMake((VIEW_WIDTH)*noOfProfiles, VIEW_HEIGHT);
            [self addProfilesToScrollView];
        }
    }
}

-(void)addProfilesToScrollView {
    CGFloat xPos = 0;
    CGFloat yPos= 0;
    for (int i=0; i<self.eventMatchData.count; i++) {
        TMEventMatch *data = self.eventMatchData[i];
        TMEventMatchView *eventMatchView = [[TMEventMatchView alloc] initWithFrame:CGRectMake(xPos, yPos,
                                                                                              VIEW_WIDTH,
                                                                                              VIEW_HEIGHT)
                                                                         matchData:self.eventMatchData[i]
                                                                             index:i];
        eventMatchView.eventMatchDelegate = self.cellDelegate;
        [self.horizontalScrollView addSubview:eventMatchView];
        xPos += VIEW_WIDTH;
        if(self.showBlur && !data.isMutualMatch) {
            eventMatchView.alpha = 0.30;
        }
    }
}

-(void)showConnectTutorial {
    CGFloat tutWidth = 220; //180
    CGFloat tutHeight = 140;//126
    CGFloat xPos = 5;//-5;//self.frame.size.width-tutWidth;
    CGFloat yPos = -110;//self.frame.size.width-tutHeight+20;
    if(!self.connectTutorial) {
        self.connectTutorial = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, tutWidth, tutHeight)];
        [self addSubview:self.connectTutorial];
    
        NSString *text = @"You can now connect\nwith people following\nthis event. Tap to view!";
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), tutHeight);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        UIView *curveView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width+50, rect.size.height+30)];
        curveView.backgroundColor = [UIColor blackColor];
        curveView.layer.cornerRadius = 30;
        [self.connectTutorial addSubview:curveView];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake((curveView.frame.size.width-rect.size.width)/2, (curveView.frame.size.height-rect.size.height)/2, rect.size.width, rect.size.height)];
        textLabel.numberOfLines = 0;
        textLabel.textColor = [UIColor whiteColor];
        textLabel.text = text;
        textLabel.font = [UIFont systemFontOfSize:16];
        textLabel.textAlignment = NSTextAlignmentCenter;
        [self.connectTutorial addSubview:textLabel];
        
        CGFloat verticalLineWidth = 3;
        
        UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(curveView.frame)-2, verticalLineWidth, 50)];
        verticalLine.backgroundColor = [UIColor blackColor];
        [self.connectTutorial addSubview:verticalLine];
        
        CGFloat circleWidth = 15;
        UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake((CGRectGetMaxX(verticalLine.frame)-(circleWidth-verticalLineWidth)/2-3), CGRectGetMaxY(verticalLine.frame)-2, circleWidth, circleWidth)];
        circleView.layer.cornerRadius = circleWidth/2;
        circleView.backgroundColor = [UIColor blackColor];
        [self.connectTutorial addSubview:circleView];
        
        [self bringSubviewToFront:self.connectTutorial];
    }
    
    [UIView animateWithDuration:0.5 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame = self.connectTutorial.frame;
                         frame.origin.y = -80;
                         self.connectTutorial.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.75
                                               delay: 3.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.connectTutorial.alpha = 0;
                                          }
                                          completion:^(BOOL finished){
                                              [self.connectTutorial removeFromSuperview];
                                              self.connectTutorial = nil;
                                          }];
                         
                     }];
}

@end

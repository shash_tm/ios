//
//  TMEventMatchView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 13/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventMatchView.h"
#import "TMEventMatch.h"
#import "UIImageView+AFNetworking.h"

@interface TMEventMatchView()

@property(nonatomic, strong)TMEventMatch *eventMatchData;

@end

@implementation TMEventMatchView

-(instancetype)initWithFrame:(CGRect)frame matchData:(TMEventMatch *)matchData index:(NSInteger)index {
    self = [super initWithFrame:frame];
    if(self) {
        
        self.eventMatchData = matchData;
        self.eventMatchData.index = index;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapGesture];
        
        [self configureView];
        
        return self;
    }
    return nil;
}

-(void)configureView {
    
    CGFloat width = self.frame.size.width*0.7;   CGFloat xPos = (self.frame.size.width*0.3)/2;
    UIImageView *picView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, 5, width, width)];
    picView.backgroundColor = [UIColor clearColor];
    picView.layer.cornerRadius = width/2;
    picView.layer.masksToBounds = YES;
    [picView setImageWithURL:[[NSURL alloc] initWithString:self.eventMatchData.matchImageURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    [self addSubview:picView];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, picView.frame.origin.y+picView.frame.size.width+3, self.frame.size.width, 16)];
    lb.font = [UIFont systemFontOfSize:12.0];
    lb.textColor = [UIColor blackColor];
    lb.backgroundColor = [UIColor clearColor];
    lb.text = self.eventMatchData.matchName;
    lb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lb];
    
    if( !self.eventMatchData.isMutualMatch && self.eventMatchData.likedByMe) {
        UIImageView *tickView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos-5, 5, 20, 20)];
        tickView.image = [UIImage imageNamed:@"event_like"];
        [self addSubview:tickView];
        [self bringSubviewToFront:tickView];
    }

}

-(void)tapAction {
    if(self.eventMatchDelegate) {
        [self.eventMatchDelegate didTapOnProfile:self.eventMatchData];
    }
}
@end

//
//  TMAboutEventCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAboutEventCollectionViewCell.h"
#import "TMAboutEvent.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"
#import "UIColor+TMColorAdditions.h"

@interface TMAboutEventCollectionViewCell()

@property(nonatomic, strong)TMAboutEvent *aboutEvent;
@property(nonatomic, strong)UILabel *headerTitle;

@end

@implementation TMAboutEventCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}

-(void)addHeader
{
    CGRect frame = self.frame;
    CGFloat yPos = 22.0;
    
    UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                  0,
                                                                                  frame.size.width,
                                                                                  1.0)];
    seperatorImgView.backgroundColor = [UIColor separatorColor];
    [self.contentView addSubview:seperatorImgView];
    
    UIImageView *imgIconView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                             yPos,
                                                                             [self.aboutEvent getIconWidth],
                                                                             [self.aboutEvent getIconWidth])];
    imgIconView.image = [UIImage imageNamed:@"details"];
    [self.contentView addSubview:imgIconView];
    
    self.headerTitle = [[UILabel alloc] initWithFrame:CGRectMake([self.aboutEvent getIconWidth]+self.aboutEvent.spaceBetweenIconAndText,
                                                                 yPos,
                                                                 frame.size.width,
                                                                 20)];
    self.headerTitle.font = [UIFont systemFontOfSize:15];
    self.headerTitle.textColor = [UIColor blackColor];
    self.headerTitle.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.headerTitle];
    
    NSString *text = [self.aboutEvent getTermsHeaderText];
    self.headerTitle.text = text;
}


-(void)configureUI:(TMAboutEvent *)aboutEvent {
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        self.aboutEvent = aboutEvent;
        [self addHeader];
        
        CGRect frame = self.frame;
        CGFloat xPos = [self.aboutEvent getIconWidth]+self.aboutEvent.spaceBetweenIconAndText;
        CGFloat yPos = self.headerTitle.frame.origin.y+self.headerTitle.frame.size.height+5;
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        CGSize constrintSize = CGSizeMake(frame.size.width-xPos, NSUIntegerMax);
        CGRect rect = [self.aboutEvent.aboutTheEventText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        UILabel *aboutUs = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
        aboutUs.text = self.aboutEvent.aboutTheEventText;
        aboutUs.font = [UIFont systemFontOfSize:14];
        aboutUs.textAlignment = NSTextAlignmentLeft;
        aboutUs.textColor = [UIColor colorWithRed:0.498 green:0.498 blue:0.498 alpha:1];
        aboutUs.numberOfLines = 0;
        [aboutUs sizeToFit];
        [self.contentView addSubview:aboutUs];
        
        // ticket button
        /*if(![self.aboutEvent.ticketUrlString isEqualToString:@""]) {
            
            CGFloat iconWidth = 20;
            attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
            constrintSize = CGSizeMake(self.frame.size.width*0.5, NSUIntegerMax);
            rect = [self.aboutEvent.ticketTextString boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            
            CGFloat width = rect.size.width + iconWidth + 60;
            
            yPos = aboutUs.frame.origin.y+aboutUs.frame.size.height+15;
            UIView *ticketView = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-width)/2, yPos, width, 44)];
            ticketView.layer.cornerRadius = 5;
            ticketView.layer.borderWidth = 2.0;
            ticketView.layer.borderColor = [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:1.0].CGColor;
            [self.contentView addSubview:ticketView];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
            [ticketView addGestureRecognizer:tapGesture];
            
            xPos = (width-(iconWidth+rect.size.width+5))/2;
            
            TMProfileImageView *imgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,1+(44-12)/2,iconWidth, 12)];
            [imgView setImageContentMode:UIViewContentModeScaleAspectFit];
            [ticketView addSubview:imgView];
            
            NSString* imageURLString = self.aboutEvent.ticketImageUrlString;
            NSString *lastPathComponent = [imageURLString lastPathComponent];
            [imgView setImageFromURLString:imageURLString];
            [imgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
            
            UILabel *ticketLbl = [[UILabel alloc] initWithFrame:CGRectMake(imgView.frame.origin.x+20+5, (44-rect.size.height)/2, rect.size.width, rect.size.height)];
            ticketLbl.text = self.aboutEvent.ticketTextString;
            ticketLbl.font = [UIFont systemFontOfSize:14];
            ticketLbl.textColor = [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:1.0];
            [ticketView addSubview:ticketLbl];
        }*/
    }
}

-(void)tapAction {
    if(self.delegate) {
        [self.delegate didClickOnTicket:self.aboutEvent.ticketUrlString];
    }
}
@end

//
//  TMAboutEventCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMAboutEvent;

@protocol TMAboutEventCollectionViewCellDelegate;

@interface TMAboutEventCollectionViewCell : UICollectionViewCell

@property(nonatomic,weak)id<TMAboutEventCollectionViewCellDelegate>delegate;
@property(nonatomic, assign) BOOL isCellConfigured;
-(void)configureUI:(TMAboutEvent *)aboutEvent;

@end

@protocol TMAboutEventCollectionViewCellDelegate <NSObject>

@optional
-(void)didClickOnTicket:(NSString *)ticketUrlString;

@end
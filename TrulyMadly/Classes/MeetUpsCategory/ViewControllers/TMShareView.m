//
//  TMShareView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMShareView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMShareNativeView.h"
#import "TMShareCustomView.h"

#import "TMMessagingController.h"

@interface TMShareView()<TMShareNativeViewDelegate, TMShareCustomViewDelegate, UIAlertViewDelegate>

@property(nonatomic, strong)TMEventMatch *matchData;

@end

@implementation TMShareView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        [[TMMessagingController sharedController] getConversationDataForShare:^(NSDictionary *conversationData) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSArray *activeConversationList = conversationData[@"convlist"];
                
                self.backgroundColor = [UIColor alertBackgroundColor];
                
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
                [self addGestureRecognizer:tapGesture];
                
                [self addBackgroundView:activeConversationList];
            });

        }];
        return self;
    }
    return nil;
}

-(void)addBackgroundView:(NSArray *)convList {
    CGFloat height = 155;
    if(convList.count > 0) {
        height = 260;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-height, self.frame.size.width, height)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bgView.frame.size.width, 50)];
    lbl.text = @"Share";
    lbl.font = [UIFont boldSystemFontOfSize:18];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.backgroundColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.90];
    [bgView addSubview:lbl];
    
    UIImage *btnImage = [UIImage imageNamed:@"remove"];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(0,5,40,40);
    [cancelButton addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:btnImage forState:UIControlStateNormal];
    [bgView addSubview:cancelButton];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ignoreThisTouch)];
    [bgView addGestureRecognizer:tapGesture];
    
    CGFloat yPos = 50;
    if(convList.count > 0) {
        TMShareCustomView *customView = [[TMShareCustomView alloc] initWithFrame:CGRectMake(0, 50, self.frame.size.width, 100) userList:convList];
        customView.delegate = self;
        [bgView addSubview:customView];
        yPos = CGRectGetMaxY(customView.frame)+5;
    }
    
    TMShareNativeView *nativeView = [[TMShareNativeView alloc] initWithFrame:CGRectMake(0, yPos, self.frame.size.width, 105)];
    nativeView.delegate = self;
    nativeView.backgroundColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.90];
    [bgView addSubview:nativeView];
}

-(void)tapAction {
    if(self.shareDelegate){
        [self.shareDelegate didRemoveFromSuperView:@""];
    }
}

-(void)ignoreThisTouch {
    
}

#pragma mark TMShareNativeViewDelegate method
-(void)removeNativeViewFromSuperView:(NSString *)appName {
    if(self.shareDelegate){
        [self.shareDelegate didRemoveFromSuperView:appName];
    }
}

#pragma mark TMShareCustomViewDelegate method
-(void)shareEventWithMatch:(TMEventMatch *)matchData {
    self.matchData = matchData;
    if(self.shareDelegate){
        [self.shareDelegate launchOnetoOneConv:self.matchData];
    }
}

@end

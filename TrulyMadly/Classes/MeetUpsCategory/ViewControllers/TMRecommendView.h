//
//  TMRecommendView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseView.h"

@class TMEventDetailViewController;

@protocol TMRecommendViewDelegate;

@interface TMRecommendView : TMBaseView

@property(nonatomic,weak)id<TMRecommendViewDelegate> recommendViewDelegate;
-(instancetype)initWithFrameAndCategory:(CGRect)frame categoryId:(NSString *)categoryId;
-(void)saveZoneIds:(NSArray *)zoneIds;
-(void)saveGeoLoc:(NSDictionary *)geoData;
-(void)makeEventListRequest;

@end

@protocol TMRecommendViewDelegate <NSObject>

-(void)didSelectEvent:(TMEventDetailViewController *)eventDetailViewCon;
-(void)didRequestCompleted:(NSDictionary *)eventListDic selectedZones:(NSArray *)selectedZones saveGeo:(BOOL)saveGeo;

@end
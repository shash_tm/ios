//
//  TMEventListViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 09/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMEventListViewController : TMBaseViewController

-(instancetype)initWithCategoryId:(NSString *)categoryId title:(NSString *)title;

@end

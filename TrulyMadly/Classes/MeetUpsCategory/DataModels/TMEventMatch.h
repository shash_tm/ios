//
//  TMEventMatch.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 13/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMEventMatch : NSObject

@property(nonatomic, strong)NSString *user_id;
@property(nonatomic, strong)NSString *matchName;
@property(nonatomic, strong)NSString *matchImageURL;
@property(nonatomic, strong)NSString *profileUrlString;
@property(nonatomic, strong)NSString *likeUrl;
@property(nonatomic, strong)NSString *hideUrl;
@property(nonatomic, strong)NSString *messageConvFullURL;

@property(nonatomic, strong)NSString *headerText;

@property(nonatomic, assign)BOOL likedByMe;
@property(nonatomic, assign)BOOL isMutualMatch;
@property(nonatomic, assign)BOOL hasLiked;
@property(nonatomic, assign)NSInteger index;
@property(nonatomic, assign)int totalCount;

@property(nonatomic,assign)CGFloat iconWidth;
@property(nonatomic, assign)CGFloat spaceBetweenIconAndText;
@property(nonatomic,assign)CGFloat contentMaxWidth;
-(CGFloat)conditionContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width;
-(NSString *)getTermsHeaderText;
-(CGFloat)getIconWidth;



@end

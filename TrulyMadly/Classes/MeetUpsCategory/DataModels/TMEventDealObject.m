//
//  TMEventDealObject.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventDealObject.h"
#import "TMDateCondition.h"
#import "TMDateContact.h"
#import "TMEventMatch.h"
#import "TMDateHeader.h"
#import "NSObject+TMAdditions.h"
#import "TMAboutEvent.h"

@implementation TMEventDealObject

-(instancetype)initWithEventData:(NSDictionary *)eventData {
    self = [super init];
    if(self) {
        [self configureEventData:eventData];
        return self;
    }
    return nil;
}

-(void)configureEventData:(NSDictionary *)eventData {
    // Header object
    self.isEventExpired = (eventData[@"event_expired"] && ![eventData[@"event_expired"] isKindOfClass:[NSNull class]]) ? [eventData[@"event_expired"] boolValue] : false;
    
    self.eventId = eventData[@"datespot_id"];
    self.header = [[TMDateHeader alloc] initWithData:eventData];
    self.statusText = (eventData[@"tm_status"] == [NSNull null])? @"" : eventData[@"tm_status"];
    self.isSelectOnly = [eventData[@"is_select_only"] boolValue];
    
    if(!self.isEventExpired) {
        self.contacts = (eventData[@"locations"] && [eventData[@"locations"] isKindOfClass:[NSArray class]])?[[TMDateContact alloc] initWithData:eventData[@"locations"]]:nil;
        if([eventData[@"terms_and_conditions"] isValidObject]) {
            self.conditions = ([eventData[@"terms_and_conditions"] count]>0) ? [[TMDateCondition alloc] initWithCondition:eventData[@"terms_and_conditions"]] : nil;
        }
        self.aboutUsObj = [[TMAboutEvent alloc] initWithDetails:eventData];
    
        self.matchData = [[NSMutableArray alloc] init];
        if(eventData[@"following_users"] && [eventData[@"following_users"] isKindOfClass:[NSArray class]]) {
            NSArray *tmp = eventData[@"following_users"];
            for (int i=0; i<[eventData[@"following_users"] count]; i++) {
                NSDictionary *data = eventData[@"following_users"][i];
                TMEventMatch *eventMatch = [[TMEventMatch alloc] init];
                eventMatch.user_id = data[@"user_id"];
                eventMatch.matchName = data[@"fname"];
                eventMatch.matchImageURL = data[@"thumbnail"];
                eventMatch.profileUrlString = data[@"profile_url"];
                eventMatch.likedByMe = (data[@"likedByMe"]) ? [data[@"likedByMe"] boolValue] : false;
                eventMatch.isMutualMatch = [data[@"isMutualMatch"] boolValue];
                eventMatch.likeUrl = (data[@"like_url"] && [data[@"like_url"] isKindOfClass:[NSString class]]) ? data[@"like_url"] : nil;
                eventMatch.hideUrl = (data[@"hide_url"] && [data[@"hide_url"] isKindOfClass:[NSString class]]) ? data[@"hide_url"] : nil;
                eventMatch.messageConvFullURL = (data[@"message_url"] && [data[@"message_url"] isKindOfClass:[NSString class]]) ? data[@"message_url"] : @"";
                eventMatch.index = i;
                eventMatch.headerText = (eventData[@"like_minded_text"] && [eventData[@"like_minded_text"] isKindOfClass:[NSString class]]) ? eventData[@"like_minded_text"] : @"";
                eventMatch.totalCount = (int)tmp.count;
                eventMatch.hasLiked = (data[@"hasLiked"]) ? [data[@"hasLiked"] boolValue] : false;
                [self.matchData addObject:eventMatch];
            }
        }
        if(self.matchData.count > 0){
            self.isMutualViewAvail = true;
        }else {
            self.isMutualViewAvail = false;
        }
        self.followStatus = (eventData[@"follow_status"] && !([eventData[@"follow_status"] isKindOfClass:[NSNull class]])) ? [eventData[@"follow_status"] boolValue] : false;
        if(eventData[@"share_text"] && [eventData[@"share_text"] isKindOfClass:[NSString class]] && eventData[@"share_url"] && [eventData[@"share_url"] isKindOfClass:[NSString class]]) {
            self.shareText = eventData[@"share_text"];
            self.shareURL = eventData[@"share_url"];
        }
    }
}

-(NSInteger)sectionCount
{
    if(self.isMutualViewAvail) {
        if(!self.conditions) {
            return 3;
        }
        return 4;
    }else {
        if(!self.conditions) {
            return 2;
        }
    }
    return 3;
}

-(CGFloat)contentLayoutHeight:(NSInteger)section row:(NSInteger)row maxWidth:(CGFloat)maxWidth   {
    CGFloat height = 0;
    if(self.isMutualViewAvail) {
        switch (section) {
            case 0: // mutual view
                height = [self.matchData[0] conditionContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
                break;
            case 1:
                // about the event height
                height = [self.aboutUsObj conditionContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];//200;
                break;
            case 2://Contact Details
                height = [self.contacts contactContentHeightForDetailPage:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
                break;
            case 3://Conditions
                height = [self.conditions conditionContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
                break;
            default:
                break;
        }
    }
    else{
        switch (section) {
            case 0: // about the event height
                height = [self.aboutUsObj conditionContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];//200;
                break;
            case 1://Contact Details
                height = [self.contacts contactContentHeightForDetailPage:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
                break;
            case 2://Conditions
                height = [self.conditions conditionContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
                break;
            default:
                break;
        }
    }    
    return height;
}

-(CGFloat)baseIconWidth:(CGFloat)maxWidth {
    CGFloat iconWidth = (maxWidth - (15*4))/5;
    return iconWidth;
}

@end

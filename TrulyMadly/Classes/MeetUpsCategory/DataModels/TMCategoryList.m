//
//  TMCategoryList.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCategoryList.h"

@implementation TMCategoryList

-(instancetype)initWithCategoryListData:(NSDictionary *)categoryData {
    self = [super init];
    if(self) {
        [self configureCategoryListData:categoryData];
    }
    return self;
}

-(void)configureCategoryListData:(NSDictionary *)categoryData {
    self.category_id = (categoryData[@"category_id"]) ? categoryData[@"category_id"] : nil;
    
    if(categoryData[@"background_image"] && !([categoryData[@"background_image"] isKindOfClass:[NSNull class]])) {
        self.backgroungImageURL = categoryData[@"background_image"];
    }
    if(categoryData[@"icon_image"] && !([categoryData[@"icon_image"] isKindOfClass:[NSNull class]])) {
        self.iconImageURL = categoryData[@"icon_image"];
    }
    if(categoryData[@"title"] && !([categoryData[@"title"] isKindOfClass:[NSNull class]])) {
        self.title = categoryData[@"title"];
    }
    if(categoryData[@"event_count"] && !([categoryData[@"event_count"] isKindOfClass:[NSNull class]])) {
        self.eventCount = categoryData[@"event_count"];
    }
}

@end

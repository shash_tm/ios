//
//  TMShareEventContext.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMShareEventContext.h"

@implementation TMShareEventContext

-(void)setShareEventMessage:(NSString *)eventName {
    NSString *text;
    text = [NSString stringWithFormat:@"Hey, I\'ll be at %@. Are you interested?",eventName];
    self.eventName = text;
}


@end

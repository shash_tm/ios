//
//  TMShareEventContext.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMShareEventContext : NSObject

@property(nonatomic,strong)NSString *eventId;
@property(nonatomic,strong)NSString *eventName;
@property(nonatomic,strong)NSString *eventImgURL;

-(void)setShareEventMessage:(NSString *)eventName;


@end

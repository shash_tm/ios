//
//  TMAboutEvent.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMAboutEvent : NSObject

@property(nonatomic, strong)NSString *aboutTheEventText;
@property(nonatomic, strong)NSString *ticketUrlString;
@property(nonatomic, strong)NSString *ticketTextString;
@property(nonatomic, strong)NSString *ticketImageUrlString;
@property(nonatomic, strong)NSString *ticketPrice;

@property(nonatomic,assign)CGFloat iconWidth;
@property(nonatomic, assign)CGFloat spaceBetweenIconAndText;

-(instancetype)initWithDetails:(NSDictionary *)aboutObj;
-(CGFloat)conditionContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width;
-(NSString *)getTermsHeaderText;
-(CGFloat)getIconWidth;

@end

//
//  TMEventList.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 09/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMEventList : NSObject

@property(nonatomic, strong)NSString *event_id;
@property(nonatomic, strong)NSString *eventName;
@property(nonatomic, strong)NSString *backgroungImageURL;
@property(nonatomic, strong)NSString *eventLocation;

@property(nonatomic, assign)BOOL isMutualMatchGoing;
@property(nonatomic, assign)BOOL userCountPresent;
@property(nonatomic, strong)NSArray *eventFollowingUsers;
@property(nonatomic, strong)NSString *eventFollowingUsersCount;

@property(nonatomic, assign)BOOL isGeoAvailable;
@property(nonatomic, strong)NSString *distanceFromCurrentLocation;

@property(nonatomic, assign)BOOL myStatusForEvent;

-(instancetype)initWithEventData:(NSDictionary *)eventData;

@end

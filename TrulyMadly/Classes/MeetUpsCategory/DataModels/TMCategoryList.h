//
//  TMCategoryList.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCategoryList : NSObject

@property(nonatomic, strong)NSString *category_id;
@property(nonatomic, strong)NSString *backgroungImageURL;
@property(nonatomic, strong)NSString *iconImageURL;
@property(nonatomic, strong)NSString *title;
@property(nonatomic, strong)NSString *eventCount;

-(instancetype)initWithCategoryListData:(NSDictionary *)categoryData;

@end

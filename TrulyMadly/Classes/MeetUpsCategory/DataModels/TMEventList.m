//
//  TMEventList.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 09/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventList.h"

@implementation TMEventList

-(instancetype)initWithEventData:(NSDictionary *)eventData {
    self = [super init];
    if(self) {
        [self configureEvent:eventData];
    }
    return self;
}

-(void)configureEvent:(NSDictionary *)eventData {
    self.event_id = eventData[@"datespot_id"];
    
    if(eventData[@"image"] && !([eventData[@"image"] isKindOfClass:[NSNull class]])) {
        self.backgroungImageURL = eventData[@"image"];
    }
    self.eventName = (eventData[@"name"] && !([eventData[@"name"] isKindOfClass:[NSNull class]])) ? eventData[@"name"] : @"";
    NSString *location = (eventData[@"location"] && !([eventData[@"location"] isKindOfClass:[NSNull class]])) ? eventData[@"location"] :@"";
    NSString *pretty_date = (eventData[@"pretty_date"] && !([eventData[@"pretty_date"] isKindOfClass:[NSNull class]])) ? eventData[@"pretty_date"] :@"";
    
    self.eventLocation = [NSString stringWithFormat:@"%@, %@",pretty_date,location];

    if((eventData[@"mutual_matches"] && [eventData[@"mutual_matches"] isKindOfClass:[NSArray class]]) || (eventData[@"user_count"] &&  ![eventData[@"user_count"] isKindOfClass:[NSNull class]])) {
        self.isMutualMatchGoing = true;
        //BOOL userCountPresent = false;
        self.eventFollowingUsers = ([eventData[@"mutual_matches"] isKindOfClass:[NSArray class]]) ? eventData[@"mutual_matches"] : [[NSArray alloc] init];
        
        id userCount = eventData[@"user_count"];
        if(userCount && [userCount isKindOfClass:[NSNumber class]]) {
            int count = [eventData[@"user_count"] intValue];
            if(count > 0) {
                self.eventFollowingUsersCount = [NSString stringWithFormat:@"%d following",count];
                self.userCountPresent = true;
            }
        }else if(userCount && [userCount isKindOfClass:[NSString class]]) {
            if(![eventData[@"user_count"] isEqualToString:@"0"]) {
                self.eventFollowingUsersCount = [NSString stringWithFormat:@"%@ following",eventData[@"user_count"]];
                self.userCountPresent = true;
            }
        }
        
        if(self.eventFollowingUsers.count > 0 || self.userCountPresent) {
            self.isMutualMatchGoing = true;
        }else{
            self.isMutualMatchGoing = false;
        }
    }else {
        self.isMutualMatchGoing = false;
    }
    
    if(eventData[@"geo_location_distance"] && !([eventData[@"geo_location_distance"] isKindOfClass:[NSNull class]])) {
        self.isGeoAvailable = true;
        NSString *tmp = [eventData[@"geo_location_distance"] stringByAppendingString:@" "];
        self.distanceFromCurrentLocation = [tmp stringByAppendingString:eventData[@"distance_unit"]];
    }else {
        self.isGeoAvailable = false;
    }
    
    self.myStatusForEvent = (eventData[@"going_status"] && !([eventData[@"going_status"] isKindOfClass:[NSNull class]])) ? [eventData[@"going_status"] boolValue] : false;
    
}

@end

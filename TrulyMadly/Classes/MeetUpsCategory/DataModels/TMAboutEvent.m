//
//  TMAboutEvent.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAboutEvent.h"
#import "NSString+TMAdditions.h"

@interface TMAboutEvent()

@property(nonatomic, assign)CGFloat contentMaxWidth;

@end

@implementation TMAboutEvent

-(instancetype)initWithDetails:(NSDictionary *)aboutObj
{
    self = [super init];
    if(self) {
        self.aboutTheEventText = ([aboutObj objectForKey:@"details"] && [aboutObj[@"details"] isKindOfClass:[NSString class]]) ? aboutObj[@"details"] : @"";
        self.ticketUrlString = ([aboutObj objectForKey:@"event_ticket_url"] && [aboutObj[@"event_ticket_url"] isKindOfClass:[NSString class]]) ? aboutObj[@"event_ticket_url"] : @"";
        self.ticketTextString = ([aboutObj objectForKey:@"event_ticket_text"] && [aboutObj[@"event_ticket_text"] isKindOfClass:[NSString class]]) ? aboutObj[@"event_ticket_text"] : @"";
        self.ticketImageUrlString = ([aboutObj objectForKey:@"event_ticket_image"] && [aboutObj[@"event_ticket_image"] isKindOfClass:[NSString class]]) ? aboutObj[@"event_ticket_image"] : @"";
        self.ticketPrice = ([aboutObj objectForKey:@"ticket_price"] && [aboutObj[@"ticket_price"] isKindOfClass:[NSString class]]) ? aboutObj[@"ticket_price"] : @"";
        [self setDefaults];
    }
    return self;
}

-(NSString *)getTermsHeaderText
{
    return @"Details";
}

-(void)setDefaults
{
    self.spaceBetweenIconAndText = 10.0;
    
}

-(CGFloat)getIconWidth
{
    return self.iconWidth;
}

-(CGFloat)conditionContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width
{
    CGFloat conditionsTmpHeight = 0;
    
    self.iconWidth = ((width*40)/100);
    self.contentMaxWidth = maxWidth - (self.iconWidth + self.spaceBetweenIconAndText);
    
    conditionsTmpHeight = conditionsTmpHeight + [self sizeForContent:self.aboutTheEventText].height;
    
    conditionsTmpHeight = conditionsTmpHeight+self.iconWidth+10;
    /*if(![self.ticketUrlString isEqualToString:@""]) {
        conditionsTmpHeight = conditionsTmpHeight+44;
    }*/
    
    return conditionsTmpHeight;
}

-(CGSize)sizeForContent:(NSString*)content {
    NSDictionary *attributes = @{NSFontAttributeName:[self contentDescriptionFont]};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth, NSUIntegerMax);
    CGRect rect = [content boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}

-(UIFont*)contentDescriptionFont {
    return [UIFont systemFontOfSize:14];
}

@end


//
//  TMEventMatch.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 13/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEventMatch.h"
#import "NSString+TMAdditions.h"

@implementation TMEventMatch

-(instancetype)init {
    self = [super init];
    if(self) {
        [self setDefaults];
    }
    return self;
}

-(NSString *)getTermsHeaderText
{
    return self.headerText;
}

-(void)setDefaults
{
    self.spaceBetweenIconAndText = 10.0;
    
}

-(CGFloat)getIconWidth
{
    return self.iconWidth;
}

-(CGFloat)conditionContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width
{
    self.contentMaxWidth = maxWidth;
    CGFloat conditionsTmpHeight = 75+[self sizeForContent:self.headerText].height+8;
    
    self.iconWidth = ((width*60)/100);
    
    return conditionsTmpHeight;
}

-(CGSize)sizeForContent:(NSString*)content {
    NSString *headerText = [content stringByReplacingOccurrencesOfString:@"LIKE_COUNT" withString:[NSString stringWithFormat:@"%d",self.totalCount]];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth-20, NSUIntegerMax);
    CGRect rect = [headerText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}

@end

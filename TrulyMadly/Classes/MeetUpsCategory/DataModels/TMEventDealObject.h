//
//  TMEventDealObject.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMDateHeader.h"

@class TMDateCondition;
@class TMDateContact;
@class TMAboutEvent;

@interface TMEventDealObject : NSObject

@property(nonatomic, assign)BOOL isMutualViewAvail;

@property(nonatomic, strong)TMDateHeader *header;
@property(nonatomic, strong)TMDateContact *contacts;
@property(nonatomic, strong)TMDateCondition *conditions;
@property(nonatomic, strong)TMAboutEvent *aboutUsObj;
@property(nonatomic, strong)NSMutableArray *matchData;
@property(nonatomic, assign)BOOL followStatus;
@property(nonatomic, strong)NSString *shareText;
@property(nonatomic, strong)NSString *shareURL;
@property(nonatomic, strong)NSString *eventId;
@property(nonatomic, strong)NSString *statusText;
@property(nonatomic, assign)BOOL isEventExpired;
@property(nonatomic, assign)BOOL isSelectOnly;

-(instancetype)initWithEventData:(NSDictionary *)eventData;
-(NSInteger)sectionCount;
-(CGFloat)contentLayoutHeight:(NSInteger)section row:(NSInteger)row maxWidth:(CGFloat)maxWidth;

@end

//
//  TMCategoryListManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
@class TMError;
@class TMEventDealObject;

typedef void (^CategoryListBlock)(NSMutableArray* categoryListArr, TMError *error);
typedef void (^EventListBlock)(NSDictionary *eventListDict, TMError *error);
typedef void (^EventDetailBlock)(TMEventDealObject *eventObj, TMError *error);

@interface TMCategoryListManager : TMBaseManager

-(void)getCategoryListData:(CategoryListBlock)categoryBlock;
-(void)getEventListForCategory:(NSDictionary *)queryString eventListBlock:(EventListBlock)eventListBlock;
-(void)getEventDetailData:(NSDictionary *)queryString eventDetailBlock:(EventDetailBlock)eventDetailBlock;
-(void)sendUserAction:(NSDictionary *)queryString eventBlock:(void(^)(BOOL status))eventBlock;

@end

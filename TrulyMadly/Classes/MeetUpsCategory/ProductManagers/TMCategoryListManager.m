//
//  TMCategoryListManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCategoryListManager.h"
#import "TrulyMadly-Swift.h"
#import "TMLog.h"
#import "TMCategoryList.h"
#import "TMEventList.h"
#import "TMEventDealObject.h"

@implementation TMCategoryListManager

#define KEY_DATES_LIST @"events"
#define KEY_ZONES_LIST @"zones"
#define KEY_EVENT_DETAILS @"event_details"

-(void)getCategoryListData:(CategoryListBlock)categoryBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EVENTS_PATH];
    
    NSDictionary *queryString = @{@"action":@"show_categories"};
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *responseData = [response response];
                NSArray *categoryListArr = (responseData[@"categories"] && [responseData[@"categories"] isKindOfClass:[NSArray class]]) ? [responseData objectForKey:@"categories"] : [[NSArray alloc] init];
                NSMutableArray *categoryList = [[NSMutableArray alloc] init];
                for (int i=0 ; i<categoryListArr.count; i++) {
                    TMCategoryList *categoryObj = [[TMCategoryList alloc] initWithCategoryListData:categoryListArr[i]];
                    [categoryList addObject:categoryObj];
                }
                categoryBlock(categoryList,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                categoryBlock(nil,tmError);
            }
        }
        else {
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            categoryBlock(nil,tmError);
        }
    }];
    
}

-(void)getEventListForCategory:(NSDictionary *)queryString eventListBlock:(EventListBlock)eventListBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EVENTS_PATH];
    
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                NSDictionary *eventListDictionary = [response response];
                NSMutableArray *eventList = [[NSMutableArray alloc] init];
                if (eventListDictionary && [eventListDictionary isKindOfClass:[NSDictionary class]]) {
                    NSArray* events = ([eventListDictionary valueForKey:KEY_DATES_LIST] && [[eventListDictionary valueForKey:KEY_DATES_LIST] isKindOfClass:[NSArray class]])?[eventListDictionary valueForKey:KEY_DATES_LIST]:nil;
                    for (int index = 0; index < events.count; index++) {
                        if ([[events objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                            NSDictionary* eventInfoDictionary = [events objectAtIndex:index];
                            TMEventList *eventObj = [[TMEventList alloc] initWithEventData:eventInfoDictionary];
                            [eventList addObject:eventObj];
                        }
                    }
                }
                
                NSArray *zoneList = ([eventListDictionary valueForKey:KEY_ZONES_LIST] && [[eventListDictionary valueForKey:KEY_ZONES_LIST] isKindOfClass:[NSArray class]])?[eventListDictionary valueForKey:KEY_ZONES_LIST]:[[NSArray alloc] init];
                
                NSString *cityName = ([eventListDictionary valueForKey:@"city_name"] && [[eventListDictionary valueForKey:@"city_name"] isKindOfClass:[NSString class]])?[eventListDictionary valueForKey:@"city_name"]:@"";
                
                NSDictionary* responseDict = [[NSDictionary alloc] initWithObjectsAndKeys:eventList,@"dealList",zoneList,@"zoneList",cityName,@"cityName", nil];
                
                eventListBlock(responseDict,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                eventListBlock(nil,tmError);
            }
        }
        else {
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            eventListBlock(nil,tmError);
        }
    }];
}

-(void)getEventDetailData:(NSDictionary *)queryString eventDetailBlock:(EventDetailBlock)eventDetailBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EVENTS_PATH];
    
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *responseData = [response response];
                NSDictionary *eventDetail = [responseData objectForKey:KEY_EVENT_DETAILS];
                TMEventDealObject *eventDealObj = [[TMEventDealObject alloc] initWithEventData:eventDetail];
                eventDetailBlock(eventDealObj,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                eventDetailBlock(nil,tmError);
            }
        }
        else {
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            eventDetailBlock(nil,tmError);
        }
    }];
}

-(void)sendUserAction:(NSDictionary *)queryString eventBlock:(void (^)(BOOL))eventBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EVENTS_PATH];
    
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                eventBlock(true);
            }
            else {
                eventBlock(false);
            }
        }
        else {
            eventBlock(false);
        }
    }];
}

@end

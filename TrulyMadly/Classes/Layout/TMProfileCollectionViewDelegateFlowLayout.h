//
//  TMProfileCollectionViewDelegateFlowLayout.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMEnums.h"

@class TMProfileCollectionView;
@class TMProfileCollectionViewFlowLayout;

@protocol TMProfileCollectionViewDelegateFlowLayout <UICollectionViewDelegateFlowLayout>

- (CGFloat)collectionView:(TMProfileCollectionView *)collectionView
                   layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
 heightContentAtIndexPath:(NSIndexPath *)indexPath;

- (UIEdgeInsets)collectionView:(TMProfileCollectionView *)collectionView
                   layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
 cellContentInsetAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(TMProfileCollectionView *)collectionView
                        layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
            didSelectFavouriteAtIndex:(NSInteger)favIndex;

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
     didHideCommonLike:(BOOL)status;

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
        takeQuizAction:(TMQuizScoreType)quizType;

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
        fullScreenPhotoViewAction:(NSArray*)images
          currentIndex:(NSInteger)index;

@optional
- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
     editProfileActionWithModule:(TMEditProfileModule)editProfileModule;

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
commonFacebookFriendAction:(UIView*)actionView
     mutualConnections:(NSArray*)connections;

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
          didTapFBSync:(BOOL)tapFB;

-(void)collectionView:(TMProfileCollectionView *)collectionView
               layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
   didTrustScoreClick:(BOOL)isOpen;

-(void)didClickSelectQuizCell;

@end

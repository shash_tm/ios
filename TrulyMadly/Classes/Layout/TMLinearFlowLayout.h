//
//  TMLinearFlowLayout.h
//  TrulyMadly
//
//  Created by Ankit Jain on 30/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TMLinearFlowLayoutDataSource;


@interface TMLinearFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, weak) id<TMLinearFlowLayoutDataSource> dataSource;
@property (nonatomic, assign) UIOffset itemOffset;
@property (nonatomic, assign) UIOffset initOffset;

@end


@protocol TMLinearFlowLayoutDataSource <NSObject>

-(CGSize)contentSizeForIndex:(NSInteger)index;

@optional
-(CGSize)headerSizeForIndex:(NSInteger)index;

@end
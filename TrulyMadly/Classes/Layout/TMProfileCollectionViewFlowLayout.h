//
//  TMProfileCollectionViewFlowLayout.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMProfileCollectionView;

@interface TMProfileCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (readonly, nonatomic) TMProfileCollectionView *collectionView;

/**
 *  Returns the width of items in the layout.
 */
@property (readonly, nonatomic) CGFloat itemWidth;

@property (readonly, nonatomic) CGFloat headerHeight;

- (CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
@end

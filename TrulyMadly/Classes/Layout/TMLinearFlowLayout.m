//
//  TMLinearFlowLayout.m
//  TrulyMadly
//
//  Created by Ankit Jain on 30/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMLinearFlowLayout.h"

@interface TMLinearFlowLayout ()

@property (strong, nonatomic) NSMutableArray *itemAttributes;
@property (nonatomic, assign) CGSize contentSize;

@end


@implementation TMLinearFlowLayout

-(id)init
{
    self = [super init];
    if (self)
    {
        [self setItemOffset:UIOffsetMake(0.0, 0.0)];
        [self setInitOffset:UIOffsetMake(0.0, 0.0)];
    }
    return self;
}

- (void)prepareLayout {
    
    self.itemAttributes = [[NSMutableArray alloc] init];
    
    CGFloat xOffset = 0;
    CGFloat yOffset = self.itemOffset.vertical;
    
    CGFloat contentWidth = 0.0;         // Used to determine the contentSize
    CGFloat contentHeight = 0.0;        // Used to determine the contentSize
    
        // Loop through all items and calculate the UICollectionViewLayoutAttributes for each on
    NSUInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    for (NSUInteger index = 0; index < numberOfItems; index++)
    {
        CGSize itemSize = [self.dataSource contentSizeForIndex:index];
        
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = CGRectIntegral(CGRectMake(xOffset, yOffset, itemSize.width, itemSize.height));
        [_itemAttributes addObject:attributes];
        
        xOffset = xOffset+itemSize.width+self.itemOffset.horizontal;
    }
    
        // Get the last item to calculate the total height of the content
    UICollectionViewLayoutAttributes *attributes = [_itemAttributes lastObject];
    contentHeight = attributes.frame.origin.y+attributes.frame.size.height;
    contentWidth = attributes.frame.origin.x + attributes.frame.size.width;
        // Return this in collectionViewContentSize
    _contentSize = CGSizeMake(contentWidth, contentHeight);
    
    
}

-(CGSize)collectionViewContentSize
{
    return self.contentSize;
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_itemAttributes objectAtIndex:indexPath.row];
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    return [_itemAttributes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [evaluatedObject frame]);
    }]];
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return NO;
}

@end

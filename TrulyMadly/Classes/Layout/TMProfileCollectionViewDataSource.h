//
//  TMProfileCollectionViewDataSource.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TMProfileCollectionViewDataSource <UICollectionViewDataSource>

@end

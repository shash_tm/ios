//
//  TMProfileCollectionViewFlowLayout.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewFlowLayout.h"
#import "TMProfileCollectionViewDelegateFlowLayout.h"
#import "TMProfileCollectionView.h"

@implementation TMProfileCollectionViewFlowLayout

- (void)configureFlowLayout
{
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.sectionInset = UIEdgeInsetsMake(2.0f, 10.0f, 2.0f, 10.0f);
    self.minimumLineSpacing = 0.0f;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureFlowLayout];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureFlowLayout];
}

#pragma mark - Setters

#pragma mark - Getters

- (CGFloat)itemWidth
{
    return CGRectGetWidth(self.collectionView.frame) - self.sectionInset.left - self.sectionInset.right;
}
-(CGFloat)headerHeight {
    CGRect screenBound = [UIScreen mainScreen].bounds;
    CGFloat screenWidth = screenBound.size.width;
    CGFloat introFooterHeight = 0;//25;
    CGFloat finalHeaderHeight = screenWidth+introFooterHeight;
    return finalHeaderHeight;
}

#pragma mark - Collection view flow layout

- (void)prepareLayout
{
    [super prepareLayout];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *attributesInRect = [super layoutAttributesForElementsInRect:rect];
    
    [attributesInRect enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes *attributes, NSUInteger idx, BOOL *stop) {
        if (attributes.representedElementCategory == UICollectionElementCategoryCell) {
           
        }
        else if ([attributes representedElementKind] == UICollectionElementKindSectionHeader) {
            
        }
    }];
    
    return attributesInRect;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = (UICollectionViewLayoutAttributes *)[super layoutAttributesForItemAtIndexPath:indexPath];
    
    if (attributes.representedElementCategory == UICollectionElementCategoryCell) {
        
    }
    else if ([attributes representedElementKind] == UICollectionElementKindSectionHeader) {
        
    }
    
    return attributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    
    return YES;
}

#pragma mark - Message cell layout utilities

- (CGFloat)contentHeightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat finalHeight = [self.collectionView.delegate
                           collectionView:self.collectionView
                           layout:self
                           heightContentAtIndexPath:indexPath];
    return finalHeight;
}

- (CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat finalHeight = [self contentHeightForItemAtIndexPath:indexPath];
    
    UIEdgeInsets contentInset = [self.collectionView.delegate
                                 collectionView:self.collectionView
                                 layout:self
            cellContentInsetAtIndexPath:indexPath];
    
    finalHeight += contentInset.top;
    finalHeight += contentInset.bottom;
  
    return CGSizeMake(self.itemWidth, ceilf(finalHeight));
}

@end

//
//  TMAdTrackingModel.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 26/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMAdTrackingModel : NSObject

@property(nonatomic, strong)NSArray *impressionTracker;
@property(nonatomic, strong)NSArray *clickTracker;
@property(nonatomic, strong)NSString *landingUrl;
@property(nonatomic, assign)BOOL isLandingUrlAvailable;

-(instancetype)initWithCampaignDict:(NSDictionary *)campaignDict;

@end

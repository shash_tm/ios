//
//  TMAdTrackingModel.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 26/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAdTrackingModel.h"

@implementation TMAdTrackingModel

-(instancetype)initWithCampaignDict:(NSDictionary *)campaignDict {
    self = [super init];
    if(self) {
        [self configureTrackers:campaignDict];
    }
    return self;
}

-(void)configureTrackers:(NSDictionary *)campaignDict {
    
    if(campaignDict[@"landing_url"] && ![campaignDict[@"landing_url"] isKindOfClass:[NSNull class]]) {
        self.isLandingUrlAvailable = true;
        self.landingUrl = campaignDict[@"landing_url"];
    }else {
        self.isLandingUrlAvailable = false;
    }
    
    // impression tracker
    if(campaignDict[@"impression_urls"] && [campaignDict[@"impression_urls"] isKindOfClass:[NSArray class]]) {
        self.impressionTracker = [[NSArray alloc] initWithArray:campaignDict[@"impression_urls"]];
    }else {
        self.impressionTracker = [[NSArray alloc] init];
    }
    
    // click tracker
    if(campaignDict[@"click_urls"] && [campaignDict[@"click_urls"] isKindOfClass:[NSArray class]]) {
        self.clickTracker = [[NSArray alloc] initWithArray:campaignDict[@"click_urls"]];
    }else {
        self.clickTracker = [[NSArray alloc] init];
    }
    
}

@end

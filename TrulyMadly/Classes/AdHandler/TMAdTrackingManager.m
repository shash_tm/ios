//
//  TMAdTrackingManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 26/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAdTrackingManager.h"
#import "TrulyMadly-Swift.h"
#import "TMAnalytics.h"

@implementation TMAdTrackingManager

-(void)trackImpressionUrls:(NSArray *)impUrls eventInfo:(NSDictionary*)eventInfo {
    [self trackADEvent:@"impression" eventData:eventInfo];
    for (int i=0; i<impUrls.count; i++) {
        TMRequest *request = [[TMRequest alloc] initWithUrlString:impUrls[i]];
        [self executeGETAPI:request
              success:^(NSURLSessionDataTask *task, id responseObject) {
                        
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                        
              }];
    }
}

-(void)trackClickUrls:(NSArray *)clickUrls eventInfo:(NSDictionary*)eventInfo{
    [self trackADEvent:@"click" eventData:eventInfo];
    for (int i=0; i<clickUrls.count; i++) {
        TMRequest *request = [[TMRequest alloc] initWithUrlString:clickUrls[i]];
        [self executeGETAPI:request
                    success:^(NSURLSessionDataTask *task, id responseObject) {
                        
                    }
                    failure:^(NSURLSessionDataTask *task, NSError *error) {
                        
                    }];
    }
}

-(void)trackADEvent:(NSString *)event_type eventData:(NSDictionary *)eventData
{
    NSDictionary *eventInfoDict = @{@"source":eventData[@"source"]};
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMMatchesViewController" forKey:@"screenName"];
    [eventDict setObject:@"campaigns" forKey:@"eventCategory"];
    [eventDict setObject:event_type forKey:@"eventAction"];
    [eventDict setObject:eventInfoDict forKey:@"event_info"];
    if(eventData[@"match_id"]) {
        [eventDict setObject:eventData[@"match_id"] forKey:@"status"];
        [eventDict setObject:eventData[@"match_id"] forKey:@"label"];
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}

@end

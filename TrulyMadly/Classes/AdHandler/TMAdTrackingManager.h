//
//  TMAdTrackingManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 26/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@interface TMAdTrackingManager : TMBaseManager

-(void)trackImpressionUrls:(NSArray *)impUrls eventInfo:(NSDictionary*)eventInfo;
-(void)trackClickUrls:(NSArray *)clickUrls eventInfo:(NSDictionary*)eventInfo;

@end

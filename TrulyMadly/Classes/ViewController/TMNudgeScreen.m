//
//  TMNudgeScreen.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNudgeScreen.h"
#import "TMSwiftHeader.h"
#import "TMLog.h"
#import "TMCircleView.h"
#import "TMActivityDashboard.h"
//#import "NSString+TMAdditions.h"


@interface TMNudgeScreen()

@property(nonatomic,strong)UIImageView *gradientImageView;
@property(nonatomic,strong)NSString *gender;
@property(nonatomic,assign)TMNudgeView nudgeView;
@property(nonatomic, strong)UIView *popOverView;

@end

@implementation TMNudgeScreen

float const xPos = 15.0f;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        [self addGradientImageViewAsBackground:frame];
    }
    return self;
}

-(void)addGradientImageViewAsBackground:(CGRect)frame {
    self.gradientImageView = [[UIImageView alloc] initWithFrame:frame];
    self.gradientImageView.userInteractionEnabled = true;
    self.gradientImageView.image = [UIImage imageNamed:@"BG"];
    [self addSubview:self.gradientImageView];
}

-(void)addEmptyViewWithFrame:(CGRect)frame {
    UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:frame];
    seperatorImgView.backgroundColor = [UIColor whiteColor];
    [self.gradientImageView addSubview:seperatorImgView];
}

-(CGRect)addNextSetOfMatchesText:(NSString*)text {
    UIFont* font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    //NSLog(@"height of screen is = %f",[[UIScreen mainScreen] bounds].size.height);
    
    CGFloat yPos = 35;
    
    if(screenHeight == 480) {
        yPos = 15;
        font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
    }
    if([text isEqualToString:@""]) {
        yPos = 65;
        if (screenHeight == 480) {
            yPos = 45;
        }
        else if(screenHeight == 568) {
            yPos = 85;
        }
        else if(screenHeight == 667) {
            yPos = 135;
        }
        else if(screenHeight == 736) {
            yPos = 160;
        }
        text = @"Check back later for more matches.";
        font = [UIFont fontWithName:@"HelveticaNeue" size:20.0];
    }
    
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:self.bounds.size.width-30], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
    lb.font = font;
    lb.textColor = [UIColor colorWithRed:0.361 green:0.357 blue:0.357 alpha:1.0];
    lb.numberOfLines = 0;
    lb.textAlignment = NSTextAlignmentLeft;
    lb.backgroundColor = [UIColor clearColor];
    lb.text = text;
    [lb sizeToFit];
    [self.gradientImageView addSubview:lb];
    
    return lb.frame;
}

-(void)addTrustScore:(NSString*)trustScore frame:(CGRect)frame {
        
    TMCircleView* innerCircle = [[TMCircleView alloc] initWithFrame:frame];
    innerCircle.backgroundColor = [UIColor clearColor];
    [self.gradientImageView addSubview:innerCircle];
    
//    UIImageView* trustImage = [[UIImageView alloc] initWithFrame:frame];
//    trustImage.userInteractionEnabled = false;
//    trustImage.image = [UIImage imageNamed:@"score_active"];
//    [self.gradientImageView addSubview:trustImage];
    
    UIFont* font = [UIFont fontWithName:@"HelveticaNeue" size:24.0];
    
    NSString *text = [trustScore stringByAppendingString:@"%"];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:frame.size.width], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x+(frame.size.width-rect.size.width)/2, frame.origin.y+(frame.size.height-20)/2, rect.size.width, 20)];
    lb.font = font;
    lb.textColor = [UIColor whiteColor];
    lb.numberOfLines = 0;
    lb.textAlignment = NSTextAlignmentCenter;
    lb.backgroundColor = [UIColor clearColor];
    lb.text = text;
    [self.gradientImageView addSubview:lb];
    
}

-(void)addTrustScoreNew:(NSString*)trustScore frame:(CGRect)frame {
    
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    UIFont* font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    UIFont* font = [UIFont fontWithName:@"HelveticaNeue" size:46.0];

    CGFloat extraHeight = 4;
    
    if(screenHeight == 667) {
        font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0];
        font = [UIFont fontWithName:@"HelveticaNeue" size:55.0];
        extraHeight = 6;
    }
    else if(screenHeight == 736) {
        font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        font = [UIFont fontWithName:@"HelveticaNeue" size:60.0];
        extraHeight = 6;
    }

//    else {
//        font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0];
//        font = [UIFont fontWithName:@"HelveticaNeue" size:30.0];
//    }
    
    
    
    NSString *text1 = @"Your Trust Score";
    
    NSDictionary *dict1 = [NSDictionary dictionaryWithObjectsAndKeys:font1, @"kfont", [NSNumber numberWithFloat:frame.size.width], @"kmaxwidth", text1, @"ktext", nil];
    CGRect rect1 = [TMStringUtil textRectForString:dict1];
    
    NSString *text = [trustScore stringByAppendingString:@"%"];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:frame.size.width], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    
    
    UILabel *lb1 = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y+(frame.size.height-(rect1.size.height+rect.size.height))/2+extraHeight, frame.size.width, rect1.size.height)];
    lb1.font = font1;
    lb1.textColor = [UIColor whiteColor];
    lb1.numberOfLines = 1;
    lb1.textAlignment = NSTextAlignmentLeft;
    lb1.backgroundColor = [UIColor clearColor];
    lb1.text = text1;
    [self.gradientImageView addSubview:lb1];
    
    
//    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y+(frame.size.height-20)/2, rect.size.width, 20)];
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x, lb1.frame.origin.y+lb1.frame.size.height+1, frame.size.width, rect.size.height)];
    lb.font = font;
    lb.textColor = [UIColor whiteColor];
    lb.numberOfLines = 1;
    lb.textAlignment = NSTextAlignmentLeft;
    lb.backgroundColor = [UIColor clearColor];
    lb.text = text;
    [self.gradientImageView addSubview:lb];
    
    UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x+frame.size.width, lb1.frame.origin.y-3, 0.75, rect1.size.height+rect.size.height-2)];
    seperatorImgView.backgroundColor = [UIColor whiteColor];
    [self.gradientImageView addSubview:seperatorImgView];
    
    
}

-(void)addPic:(CGRect)frame {
    UIImageView* picImageView = [[UIImageView alloc] initWithFrame:frame];
    picImageView.userInteractionEnabled = false;
    [self.gradientImageView addSubview:picImageView];
    if([self.gender isEqualToString:@"female"]) {
        picImageView.image = [UIImage imageNamed:@"CustomAlert_Profile"];
    }else {
        picImageView.image = [UIImage imageNamed:@"ManProfileIcon"];
    }
}

-(void)addTextMessage:(CGRect)frame msg:(NSString*)msg {
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    UIFont* font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:22.0];
    
    if([self.gender isEqualToString:@"female"]) {
        if(screenHeight > 480) {
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:30.0];
        }else {
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0];
        }
    }
    
    NSString *text = msg;
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:frame.size.width-20], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, rect.size.width, rect.size.height)];
    lb.font = font;
    lb.textColor = [UIColor whiteColor];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.backgroundColor = [UIColor clearColor];
    lb.text = text;
    lb.numberOfLines = 0;
    [lb sizeToFit];
    
    CGRect newFrame = lb.frame;
    if([self.gender isEqualToString:@"female"]) {
        newFrame.origin.x = (self.bounds.size.width-lb.frame.size.width)/2;
    }else {
        newFrame.origin.y = frame.origin.y+(frame.size.height-lb.frame.size.height)/2;
    }
    lb.frame = newFrame;
    
    [self.gradientImageView addSubview:lb];
}

-(void)addButtonForMales:(CGRect)frame titleText:(NSString*)titleText photoButton:(BOOL)photoButton{
    
    UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    continueButton.frame = frame;
    continueButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0 ];
    [continueButton setTitle:titleText forState:UIControlStateNormal];
    continueButton.layer.cornerRadius = 6;
    [continueButton setTitleColor:[UIColor colorWithRed:0.9 green:0.58 blue:0.61 alpha:1.0] forState:UIControlStateNormal];
    continueButton.backgroundColor = [UIColor whiteColor];
    if(photoButton) {
        [continueButton addTarget:self action:@selector(didPhotoNextClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [continueButton addTarget:self action:@selector(didNextClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.gradientImageView addSubview:continueButton];
    
}

-(void)addButtonForFemales:(NSString*)titleText{
    
    UIButton *mayBeLater = [UIButton buttonWithType:UIButtonTypeSystem];
    mayBeLater.frame = CGRectMake(0, self.bounds.size.height-65, (self.bounds.size.width/2)-1, 65);
    mayBeLater.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0 ];
    [mayBeLater setTitle:@"Maybe Later" forState:UIControlStateNormal];
    [mayBeLater setTitleColor:[UIColor colorWithRed:0.29 green:0.27 blue:0.4 alpha:1.0] forState:UIControlStateNormal];
    mayBeLater.backgroundColor = [UIColor whiteColor];
    mayBeLater.alpha = 0.7;
    [mayBeLater addTarget:self action:@selector(didClickLater:) forControlEvents:UIControlEventTouchUpInside];
    [self.gradientImageView addSubview:mayBeLater];
    
    UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    continueButton.frame = CGRectMake((self.bounds.size.width/2)+1, self.bounds.size.height-65, (self.bounds.size.width/2)-1,65);
    continueButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0 ];
    [continueButton setTitle:titleText forState:UIControlStateNormal];
    [continueButton setTitleColor:[UIColor colorWithRed:0.29 green:0.27 blue:0.4 alpha:1.0] forState:UIControlStateNormal];
    continueButton.backgroundColor = [UIColor whiteColor];
    continueButton.alpha = 0.7;
    [continueButton addTarget:self action:@selector(didNextClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.gradientImageView addSubview:continueButton];
}


-(void)showNudgeScreen:(NSString *)gender nudgeView:(TMNudgeView)nudgeView
           textMessage:(NSString *)textMessage trustScore:(NSString *)trustScore {
    self.gender = gender;
    self.nudgeView = nudgeView;
    
    if([gender isEqualToString:@"male"]) {
        CGRect frame = [self addNextSetOfMatchesText:@""];
        CGRect newFrame = frame;
        newFrame.origin.x = xPos;
        newFrame.origin.y = frame.origin.y+frame.size.height+30;
        newFrame.size.height = 0.75;
        newFrame.size.width = self.bounds.size.width-30;
        [self addEmptyViewWithFrame:newFrame];
        
        newFrame.origin.y = newFrame.origin.y+newFrame.size.height+30;
        newFrame.size.width = self.bounds.size.width*0.35;
        newFrame.size.height = self.bounds.size.width*0.35;
        
        NSString* str = @"";
        if(nudgeView == TMTrustBuilderView) {
            str = @"Build Now";
            //[self addTrustScore:trustScore frame:newFrame];
            [self addTrustScoreNew:trustScore frame:newFrame];
        }else if(nudgeView == TMProfileView) {
            str = @"Complete Now";
            [self addPic:newFrame];
        }
        
        newFrame.origin.x = newFrame.origin.x+newFrame.size.width+10;
        newFrame.size.width = (self.bounds.size.width)*0.7-30;
        [self addTextMessage:newFrame msg:textMessage];
        
        // add button
        newFrame.origin.x = xPos;
        newFrame.origin.y = newFrame.origin.y+newFrame.size.height+30;
        newFrame.size.width = self.bounds.size.width-30;
        newFrame.size.height = 44;
        [self addButtonForMales:newFrame titleText:str photoButton:false];
        
        newFrame.origin.x = xPos;
        newFrame.origin.y = newFrame.origin.y+newFrame.size.height+34;
        newFrame.size.height = 0.75;
        newFrame.size.width = self.bounds.size.width-30;
        [self addEmptyViewWithFrame:newFrame];
    }
    else {
        NSString* str = @"";
        CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        
        CGRect frame = CGRectMake((self.bounds.size.width*0.6)/2,self.bounds.size.width*0.3, self.bounds.size.width*0.4, self.bounds.size.width*0.4);
        
        if(screenHeight == 480) {
            frame = CGRectMake((self.bounds.size.width*0.7)/2,self.bounds.size.width*0.3, self.bounds.size.width*0.3, self.bounds.size.width*0.3);
        }
        if(nudgeView == TMTrustBuilderView) {
            str = @"Build Now";
            [self addTrustScore:trustScore frame:frame];
        }else if(nudgeView == TMProfileView) {
            str = @"Complete Now";
            [self addPic:frame];
        }
        
        CGRect newFrame = frame;
        newFrame.origin.x = xPos;
        
        if(screenHeight == 480) {
            newFrame.origin.y = frame.origin.y+frame.size.height+10;
        }else {
            newFrame.origin.y = frame.origin.y+frame.size.height+30;
        }
        newFrame.size.width = self.bounds.size.width-60;
        
        [self addTextMessage:newFrame msg:textMessage];
        [self addButtonForFemales:str];
        
    }
}


-(void)addContainer:(CGRect)frame activityDashboard:(TMActivityDashboard*)activityDashboard screenHeight:(CGFloat)screenHeight {
    
    UIColor *blueColor = [UIColor colorWithRed:0.635 green:0.694 blue:0.804 alpha:1.0];
    UIColor *textColor = [UIColor colorWithRed:0.325 green:0.451 blue:0.635 alpha:1.0];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:13.0 ];
    UIFont *viewFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30.0 ];
    CGFloat verticalSpace = 5.0;
    CGFloat verticalPhotoSpace = 22.0;
    CGFloat textWidth = frame.size.width;
    CGFloat yPos = 15;  CGFloat Ypos = 15;
    CGFloat lowerPartSpace = 5;
    BOOL isBiggerScreen = false;
    CGFloat shutterSpace = 0;
    
    if(screenHeight == 480){
        yPos = 5;  Ypos = 5; lowerPartSpace = 5; //verticalPhotoSpace = 18.0;
        font = [UIFont fontWithName:@"HelveticaNeue" size:12.0 ];
        viewFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24.0 ];
    }
    else if(screenHeight > 568) {
        verticalPhotoSpace = 28.0;
        verticalSpace = 15.0;
        textWidth = (frame.size.width/3)-15;
        lowerPartSpace = 10;
        isBiggerScreen = true;
        shutterSpace = 5;
    }
    
    UIImageView *dummy = [[UIImageView alloc] initWithFrame:frame];
    dummy.backgroundColor = [UIColor whiteColor];
    dummy.alpha = 0.5;
    dummy.layer.cornerRadius = 8.0;
    dummy.layer.borderWidth = 0.75;
    dummy.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self.gradientImageView addSubview:dummy];
    
    UIImageView *container = [[UIImageView alloc] initWithFrame:frame];
    container.backgroundColor = [UIColor clearColor];
    container.layer.cornerRadius = 8.0;
    container.layer.borderWidth = 0.75;
    container.layer.borderColor = [[UIColor whiteColor] CGColor];
    [container setUserInteractionEnabled:true];
    [self.gradientImageView addSubview:container];
    
    // nof of views
    CGFloat xPos = 20;
    CGFloat width = frame.size.width*0.3-10; CGFloat height = 30;
    UILabel *noOfViews = [[UILabel alloc] initWithFrame:CGRectMake(10,yPos,width,height)];
    noOfViews.font = viewFont;
    noOfViews.textColor = textColor;
    noOfViews.textAlignment = NSTextAlignmentCenter;
    noOfViews.backgroundColor = [UIColor clearColor];
    noOfViews.text = [NSString stringWithFormat: @"%ld", (long)activityDashboard.noOfView];
    
    [container addSubview:noOfViews];
    
    
    if(screenHeight == 480){
        yPos = yPos+height+2;
    }
    else if(screenHeight > 568) {
        xPos = xPos+5;
        yPos = yPos+height+4;
        width = frame.size.width*0.3-30;
    }
    else {
        xPos = xPos - 4;
        yPos = yPos+height+4;
    }
    
    UILabel *viewsText = [[UILabel alloc] initWithFrame:CGRectMake(xPos,yPos,width,height)];
    viewsText.font = font;
    viewsText.textColor = textColor;
    viewsText.textAlignment = NSTextAlignmentCenter;
    viewsText.backgroundColor = [UIColor clearColor];
    viewsText.text = activityDashboard.activity_text;
    viewsText.numberOfLines = 0;
    [viewsText sizeToFit];
    
    [container addSubview:viewsText];
    
    Ypos = yPos + height + shutterSpace;
    // popularity text
    
    if(screenHeight > 568) {
        xPos = xPos+width+20;
    }else {
        xPos = xPos+width+12;
    }
    width = frame.size.width*0.7-45;

//    UILabel *popularityText = [[UILabel alloc] initWithFrame:CGRectMake(xPos,yPos,width,height)];
//    popularityText.font = font;
//    popularityText.textColor = textColor;
//    popularityText.textAlignment = NSTextAlignmentLeft;
//    popularityText.backgroundColor = [UIColor clearColor];
//    popularityText.text = activityDashboard.popularity_text;
//    popularityText.numberOfLines = 0;
//    [popularityText sizeToFit];
//    
//    [container addSubview:popularityText];
    
    height = 10;
    yPos = noOfViews.frame.origin.y + noOfViews.frame.size.height + 5; //noOfViews.frame.origin.y+10;
    
    // add bigger bar
    UIImageView *biggerBar = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    biggerBar.backgroundColor = blueColor;
    biggerBar.layer.cornerRadius = 4.0;
    [container addSubview:biggerBar];
    
    width = (width/5)*activityDashboard.popularity;
    // add smaller bar
    UIImageView *smallerBar = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    smallerBar.backgroundColor = [UIColor whiteColor];
    smallerBar.layer.cornerRadius = 4.0;
    [container addSubview:smallerBar];
    
    
    // tip image
    xPos = xPos+width-7;
    width = 14;//20;//30;
    height = (14*13)/20;//13;//20;
     yPos = yPos-height;
    UIImageView *tipImage = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    tipImage.backgroundColor = [UIColor clearColor];
    tipImage.image = [UIImage imageNamed:@"Indicator"];
    [container addSubview:tipImage];
    
    
    UILabel *LikesScore = [[UILabel alloc] initWithFrame:CGRectMake(biggerBar.frame.origin.x+(biggerBar.frame.size.width-128)/2
                                                                    ,biggerBar.frame.origin.y-30,110,20)];
    LikesScore.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0 ];
    LikesScore.textColor = textColor;
    LikesScore.textAlignment = NSTextAlignmentLeft;
    LikesScore.backgroundColor = [UIColor clearColor];
    LikesScore.text = @"Popularity score";
    [container addSubview:LikesScore];
    
    
    // question mark button with image
    UIImageView *infoImage = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(LikesScore.frame)+3, LikesScore.frame.origin.y+2.5, 15, 15)];
    infoImage.backgroundColor = [UIColor clearColor];
    infoImage.image = [UIImage imageNamed:@"question"];
    [container addSubview:infoImage];
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeSystem];
    infoButton.frame = CGRectMake(CGRectGetMaxX(LikesScore.frame), LikesScore.frame.origin.y-14, 44, 44);
    infoButton.backgroundColor = [UIColor clearColor];
    [infoButton addTarget:self action:@selector(didInfoButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:infoButton];
    
    NSString *popText = @"It shows how much more your profile is \'liked\' as compared to other male users.";
    NSDictionary *dict_pop = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13.0], @"kfont", [NSNumber numberWithFloat:biggerBar.frame.size.width], @"kmaxwidth", popText, @"ktext", nil];
    CGRect rect_pop = [TMStringUtil textRectForString:dict_pop];
    
    CGFloat popxPos = biggerBar.frame.origin.x;//frame.size.width-rect_pop.size.width-30;
    CGFloat popyPos = infoImage.frame.origin.y + infoImage.frame.size.height;
    self.popOverView = [[UIView alloc] initWithFrame:CGRectMake(popxPos, popyPos, biggerBar.frame.size.width, rect_pop.size.height+20)];
    [self.popOverView setUserInteractionEnabled:false];
    self.popOverView.backgroundColor = [UIColor clearColor];
    
    UIImageView *popImage = [[UIImageView alloc] initWithFrame:CGRectMake((infoImage.frame.origin.x-self.popOverView.frame.origin.x)-1, 0, 20, 20)];
    popImage.backgroundColor = [UIColor clearColor];
    popImage.image = [UIImage imageNamed:@"pop_tip"];
    [self.popOverView addSubview:popImage];

    UIView *popLBLView = [[UIView alloc] initWithFrame:CGRectMake(0, 12, self.popOverView.frame.size.width+5, rect_pop.size.height+10)];
    popLBLView.backgroundColor = [UIColor whiteColor];
    popLBLView.layer.cornerRadius = 6;
    [self.popOverView addSubview:popLBLView];
    
    UILabel *popLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, popLBLView.frame.size.width, rect_pop.size.height)];
    popLbl.text = popText;
    popLbl.font = [UIFont systemFontOfSize:13.0];
    popLbl.textColor = [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1.0];
    popLbl.numberOfLines = 0;
    [popLbl sizeToFit];
    [popLBLView addSubview:popLbl];
    [container addSubview:self.popOverView];
    
    // bigger circle image
    width = container.frame.size.width-2*container.frame.origin.x; //frame.size.width*0.12;
    height = frame.size.width*0.14+2;//width;
    xPos = (frame.size.width-width)/2;
    yPos = Ypos;
    UIImageView *circleImage = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    circleImage.backgroundColor = [UIColor clearColor];
    circleImage.alpha = 0.75;
    circleImage.image = [UIImage imageNamed:@"Shutter-Icon"];
    [container addSubview:circleImage];
    
    //add popularity text
    UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0 ];
    if(screenHeight == 480){
        yPos = yPos + height;
        boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
    }
    else {
        yPos = yPos + height + 8;
    }
    xPos = 18 ; width = frame.size.width-36; height = 20;
    NSString * popularityText = @"Upload more pics to see your popularity soar!";
    NSDictionary *dict_tmp = [NSDictionary dictionaryWithObjectsAndKeys:boldFont, @"kfont", [NSNumber numberWithFloat:width], @"kmaxwidth", popularityText, @"ktext", nil];
    CGRect rect_t = [TMStringUtil textRectForString:dict_tmp];
    xPos = (frame.size.width-rect_t.size.width)/2;
    UILabel *popularityTextnew = [[UILabel alloc] initWithFrame:CGRectMake(xPos,yPos,rect_t.size.width,height)];
    popularityTextnew.font = boldFont;
    popularityTextnew.textColor = [UIColor colorWithRed:0.361 green:0.357 blue:0.357 alpha:1.0];
    popularityTextnew.textAlignment = NSTextAlignmentCenter;
    popularityTextnew.backgroundColor = [UIColor clearColor];
    popularityTextnew.text = popularityText;
    popularityTextnew.numberOfLines = 0;
    [popularityTextnew sizeToFit];
    
    [container addSubview:popularityTextnew];
    
    // add show pieces
    width = frame.size.width/3;
    width = width-15;
    xPos = 12; yPos = yPos+height+verticalPhotoSpace;
    height = (49*width)/36;
    UIImageView *pic1 = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    pic1.backgroundColor = [UIColor redColor];
    pic1.image = [UIImage imageNamed:@"passion"];
    [container addSubview:pic1];
    
    NSString *text = @"Express Your Passion";
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:textWidth], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    CGFloat xPoint = xPos;
    CGFloat nameWidth = width;
    if(isBiggerScreen) {
        xPoint = xPos+ABS((width-rect.size.width)/2);
        nameWidth = width-20;
    }
    UILabel *name1 = [[UILabel alloc] initWithFrame:CGRectMake(xPoint,yPos+height+verticalSpace,nameWidth,30)];
    name1.font = font;
    name1.textColor = textColor;
    name1.textAlignment = NSTextAlignmentCenter;
    name1.backgroundColor = [UIColor clearColor];
    name1.text = text;
    name1.numberOfLines = 0;
    [name1 sizeToFit];
    
    [container addSubview:name1];
    
    xPos = xPos+width+10;
    UIImageView *pic2 = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    pic2.backgroundColor = [UIColor redColor];
    pic2.image = [UIImage imageNamed:@"Candid-Photo"];
    [container addSubview:pic2];
    
    text = @"Use Candid Photos";
    NSDictionary *dict1 = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:textWidth], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect1 = [TMStringUtil textRectForString:dict1];
    xPoint = xPos;
    if(isBiggerScreen) {
        xPoint = xPos+ABS((width-rect1.size.width)/2);
    }
    UILabel *name2 = [[UILabel alloc] initWithFrame:CGRectMake(xPoint,yPos+height+verticalSpace,nameWidth,30)];
    name2.font = font;
    name2.textColor = textColor;
    name2.textAlignment = NSTextAlignmentCenter;
    name2.backgroundColor = [UIColor clearColor];
    name2.text = text;
    name2.numberOfLines = 0;
    [name2 sizeToFit];
    
    [container addSubview:name2];
    
    xPos = xPos+width+10;
    UIImageView *pic3 = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    pic3.backgroundColor = [UIColor redColor];
    pic3.image = [UIImage imageNamed:@"Natural"];
    [container addSubview:pic3];
    
    text = @"Natural Background";
    NSDictionary *dict2 = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:textWidth], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect2 = [TMStringUtil textRectForString:dict2];
    xPoint = xPos;
    if(isBiggerScreen) {
        xPoint = xPos+ABS((width-rect2.size.width)/2);
    }
    UILabel *name3 = [[UILabel alloc] initWithFrame:CGRectMake(xPoint,yPos+height+verticalSpace,nameWidth,30)];
    name3.font = font;
    name3.textColor = textColor;
    name3.textAlignment = NSTextAlignmentCenter;
    name3.backgroundColor = [UIColor clearColor];
    name3.text = text;
    name3.numberOfLines = 0;
    [name3 sizeToFit];
    
    [container addSubview:name3];
    
    self.popOverView.hidden = true;
    [container bringSubviewToFront:self.popOverView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopOverView)];
    [container addGestureRecognizer:tapGesture];
    
    [container bringSubviewToFront:infoButton];
    
}

-(void)hidePopOverView
{
    if(self.popOverView)
    {
        self.popOverView.hidden = true;
    }
}

-(void)didInfoButtonClick
{
    if(self.popOverView)
    {
        self.popOverView.hidden = false;
    }
}

-(void)showActivityDashboard:(TMActivityDashboard *)activityDashboard {
    
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
   // NSLog(@"height of screen is = %f",[[UIScreen mainScreen] bounds].size.height);
    
    CGRect frame = [self addNextSetOfMatchesText:activityDashboard.intro_text];
    CGRect newFrame = frame;
    
    newFrame.origin.y = frame.origin.y+frame.size.height+20;
    newFrame.size.height = (self.bounds.size.height*0.6)+20;
    if(screenHeight == 480) {
         newFrame.origin.y = frame.origin.y+frame.size.height+10;
         newFrame.size.height = (self.bounds.size.height*0.6)+50;
    }
    newFrame.size.width = self.bounds.size.width-30;
    [self addContainer:newFrame activityDashboard:activityDashboard screenHeight:screenHeight];
        
    newFrame.origin.y = newFrame.origin.y+newFrame.size.height+20;
    newFrame.size.width = self.bounds.size.width-60;
    newFrame.origin.x = 30;
    newFrame.size.height = 44;
    
//    if(screenHeight == 480) {
//        newFrame.origin.y = newFrame.origin.y+frame.size.height+15;
//    }
    
    [self addButtonForMales:newFrame titleText:@"Upload Pictures" photoButton:true];
    
}

-(void)didClickLater:(UIButton*)sender {
    TMLOG(@"may be later click");
    if(self.delegate) {
        [self.delegate didMaybeLaterClick:self.nudgeView];
    }
}

-(void)didNextClick:(UIButton*)sender {
    TMLOG(@"continue click");
    if(self.delegate) {
        [self.delegate didContinueClick:self.nudgeView];
    }
}

-(void)didPhotoNextClick:(UIButton*)sender {
    TMLOG(@"continue click");
    if(self.delegate) {
        [self.delegate didPhotoContinueClick];
    }
}


@end

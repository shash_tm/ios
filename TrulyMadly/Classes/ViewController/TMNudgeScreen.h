//
//  TMNudgeScreen.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TMTrustBuilderView,
    TMProfileView
} TMNudgeView;

@protocol TMNudgeScreenDelegate;

@class TMActivityDashboard;

@interface TMNudgeScreen : UIView

@property(nonatomic,weak)id<TMNudgeScreenDelegate> delegate;
@property(nonatomic,strong)NSString* nudgeMessage;
@property(nonatomic,strong)NSString* trustScore;

-(void)showNudgeScreen:(NSString*)gender nudgeView:(TMNudgeView)nudgeView
           textMessage:(NSString*)textMessage trustScore:(NSString*)trustScore;

-(void)showActivityDashboard:(TMActivityDashboard*)activityDashboard;

@end

@protocol TMNudgeScreenDelegate <NSObject>

-(void)didContinueClick:(TMNudgeView)nudgeView;

-(void)didMaybeLaterClick:(TMNudgeView)nudgeView;

-(void)didPhotoContinueClick;

@end
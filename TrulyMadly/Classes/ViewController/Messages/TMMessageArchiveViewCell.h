//
//  TMMessageArchiveViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 27/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMMessageArchiveViewCell : UITableViewCell

@property(nonatomic,strong)UILabel *messageTextLabel;

@end

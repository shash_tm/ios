//
//  TMMessagesInputToolbar.m
//  TrulyMadly
//
//  Created by Ankit on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessagesInputToolbar.h"
#import "TMMessagesToolbarContentView.h"
#import "UIImage+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"
#import "TMNotificationHeaders.h"
#import "TMStickerButton.h"
#import "TMUserSession.h"

const CGFloat KTMMessagesInputToolbarHeightDefault = 44.0f;

static void * KTMMessagesInputToolbarKeyValueObservingContext = &KTMMessagesInputToolbarKeyValueObservingContext;


@interface TMMessagesInputToolbar ()

@property (nonatomic, weak) TMMessagesToolbarContentView *contentView;
@property (nonatomic, assign) BOOL isObserving;

- (void)leftBarButtonPressed:(UIButton *)sender;
- (void)rightBarButtonPressed:(UIButton *)sender;
- (void)leftQuizBarButtonPressed:(UIButton *)sender;
- (void)rightBarShareButtonPressed:(UIButton*)sender;

- (void)addObservers;
- (void)removeObservers;
-(void)addNotification;
- (UIButton*)shareButton;
-(UIButton*)sendButtonForCommonAnswer;

@end


@implementation TMMessagesInputToolbar

#pragma mark - Initialization

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.isSendOptionEnabled = TRUE;
        self.layer.borderWidth = 0.50f;
        self.layer.borderColor = [UIColor messageInputToolbarTintColor].CGColor;
        
        self.isObserving = NO;
        self.sendButtonOnRight = YES;
        
        TMMessagesToolbarContentView *toolbarContentView = [[TMMessagesToolbarContentView alloc] initWithFrame:self.bounds];
        [self addSubview:toolbarContentView];
        self.contentView = toolbarContentView;
    
        [self addObservers];

        [self addNotification];
        [self toggleSendButtonEnabled];
    }
    return self;
}

- (void)setUpToolBarForCommonAnswersWithFrame:(CGRect)superViewFrame {
    self.isObserving = NO;
    self.sendButtonOnRight = YES;
    
    TMMessagesToolbarContentView *toolbarContentView = [[TMMessagesToolbarContentView alloc] initWithCommonAnswerScreenFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y, superViewFrame.size.width, 56)];
    self.contentView.rightBarButtonItem = [self sendButtonForCommonAnswer];
    self.contentView.rightShareBarButtonItem = [self shareButton];
    
    [self addSubview:toolbarContentView];
    self.contentView = toolbarContentView;
    
    [self toggleSendButtonEnabled];
    [self setupCommonAnswerActionButtonWithMediaButtonStatus:YES];
    
    [self addObservers];
}

-(void)setupCommonAnswerActionButtonWithMediaButtonStatus:(BOOL)addMediaButtons {
    self.contentView.rightShareBarButtonItem = [self shareButton];
    self.contentView.rightBarButtonItem = [self sendButtonForCommonAnswer];
    if(addMediaButtons) {
//        self.contentView.leftBarButtonItem = [self stickerSendButton];
//        self.contentView.leftQuizBarButtonItem = [self quizButton];
       
    }
    else {
        self.contentView.textView.frame = CGRectMake(10,
                                                     CGRectGetMinY(self.contentView.textView.frame),
                                                     CGRectGetWidth(self.contentView.textView.frame)+85,
                                                     CGRectGetHeight(self.contentView.textView.frame));
    }
}

//-(void)setupActionButtonWithMediaButtonStatus:(BOOL)addMediaButtons
//                          unreadQuizzesStatus:(BOOL)unreadQuizStatus
//                                    buttonTag:(int)buttonTag {
    //self.contentView.rightBarButtonItem = (showDealButton) ? [self curatedDealsOptionButtonWithAplpha:1] : [self sendButton];
-(void)setupActionButtonWithMediaButtonStatus:(BOOL)addMediaButtons unreadQuizzesStatus:(BOOL) unreadQuizStatus stickerBlooperStatus:(BOOL) stickerBlooperStatus buttonTag:(int) buttonTag {
    self.contentView.rightBarButtonItem = [self sendButton];
    if(addMediaButtons) {
        self.contentView.leftQuizBarButtonItem = [self quizButtonWithUnreadQuizStatus:unreadQuizStatus buttonTag:buttonTag];
        self.contentView.leftBarButtonItem = [self stickerSendButtonWithBlooperStatus:stickerBlooperStatus];
    }
    else {
        self.contentView.textView.frame = CGRectMake(10,
                                                     CGRectGetMinY(self.contentView.textView.frame),
                                                     CGRectGetWidth(self.contentView.textView.frame)+85,
                                                     CGRectGetHeight(self.contentView.textView.frame));
    }
}
-(void)setupActionButtonForMsTmWithBlooperStatus:(BOOL) stickerBlooperStatus {
    self.contentView.rightBarButtonItem = [self sendButton];
    self.contentView.leftBarButtonItem = [self stickerSendButtonWithBlooperStatus:stickerBlooperStatus];
    self.contentView.textView.frame = CGRectMake(43,
                                                 CGRectGetMinY(self.contentView.textView.frame),
                                                 CGRectGetWidth(self.contentView.textView.frame)+52,
                                                 CGRectGetHeight(self.contentView.textView.frame));
}
-(void)setupSendButtonOnly {
    self.contentView.rightBarButtonItem = [self sendButton];
}
-(void)setupSendButtonWithBlooperStatus:(BOOL) blooperStatus {
    self.contentView.rightBarButtonItem = [self sendButton];
    self.contentView.leftBarButtonItem = [self stickerSendButtonWithBlooperStatus:blooperStatus];
}
-(void)setupPhotoShareButton {
    self.contentView.rightBarButtonItem = [self photoShareButton];
    //[self curatedDealsOptionButtonWithAplpha:alpha];
}
-(UIButton*)sendButton
{
    NSString *sendTitle = @"Send";
    
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectZero];
    sendButton.tag = 45002;
    [sendButton setTitle:sendTitle forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor sendButtonColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor messageInputToolbarTintColor] forState:UIControlStateDisabled];
    
    sendButton.titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    sendButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    sendButton.titleLabel.minimumScaleFactor = 0.85f;
    sendButton.contentMode = UIViewContentModeCenter;
    sendButton.backgroundColor = [UIColor clearColor];
    sendButton.tintColor = [UIColor whiteColor];
    
    return sendButton;
}
-(UIButton*)sendButtonForCommonAnswer
{
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake(4, 4, 44, 44)];
    sendButton.tag = 45002;
    [sendButton setTitleColor:[UIColor sendButtonColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor messageInputToolbarTintColor] forState:UIControlStateDisabled];
    
    sendButton.titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    sendButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    sendButton.titleLabel.minimumScaleFactor = 0.85f;
    sendButton.contentMode = UIViewContentModeScaleAspectFit;
    sendButton.backgroundColor = [UIColor clearColor];
    sendButton.tintColor = [UIColor whiteColor];
    
    UIImageView* sendButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 10, 28, 28)];
    [sendButton addSubview:sendButtonImageView];
    sendButtonImageView.image = [UIImage imageNamed:@"send_white"];
    sendButtonImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    return sendButton;
}

-(UIButton*)photoShareButton
{
    UIButton *dealButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 4, 44, 44)];
    dealButton.tag = 45003;
    dealButton.backgroundColor = [UIColor clearColor];
    
    UIImageView* sendButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 8, 40, 31)];
    [dealButton addSubview:sendButtonImageView];
    sendButtonImageView.image = [UIImage imageNamed:@"photosharing"];
    
    return dealButton;
}
-(UIButton*)stickerSendButtonWithBlooperStatus:(BOOL) isBlooperToBeShown
{
    TMStickerButton *stickerSendingButton = [[TMStickerButton alloc] initWithFrame:CGRectZero];
    [stickerSendingButton setBlooperStatus:isBlooperToBeShown];
    //[stickerSendingButton setImage:[UIImage imageNamed:@"stickericon"] forState:UIControlStateNormal];
    stickerSendingButton.contentMode = UIViewContentModeCenter;
    return stickerSendingButton;
}
-(UIButton*)curatedDealsOptionButtonWithAplpha:(CGFloat)alpha
{
    UIButton *dealButton = [[UIButton alloc] initWithFrame:CGRectMake(4, 4, 40, 40)];
    dealButton.tag = 45001;
    dealButton.backgroundColor = [UIColor clearColor];
    dealButton.alpha = alpha;
    
    UIImageView* sendButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 8, 40, 31)];
    [dealButton addSubview:sendButtonImageView];
    sendButtonImageView.image = [UIImage imageNamed:@"dateliciousicon"];
    
    return dealButton;
}
-(UIButton<TMAnimatableButtonProtocol>*)quizButtonWithUnreadQuizStatus:(BOOL) unreadQuizStatus buttonTag:(int) buttonTag{
    TMQuizAnimatableButton* quizAnimatableButton = [[TMQuizAnimatableButton alloc] initWithFrame:CGRectZero];
    quizAnimatableButton.contentMode = UIViewContentModeScaleAspectFit;
    [quizAnimatableButton setUnreadQuizStatus:unreadQuizStatus];
    quizAnimatableButton.tag = buttonTag;
    return quizAnimatableButton;
}
- (UIButton*)shareButton {
    UIButton* shareButton = [[UIButton alloc] initWithFrame:CGRectMake(4, 4, 44, 44)];
    [shareButton setBackgroundColor:[UIColor clearColor]];
    [shareButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
    shareButton.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView* shareButtonImageView= [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 28, 28)];
    [shareButton addSubview:shareButtonImageView];
    shareButtonImageView.image = [UIImage imageNamed:@"sharescore"];
    shareButtonImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    return shareButton;
}


#pragma mark - Actions

- (void)leftBarButtonPressed:(UIButton *)sender
{
    [self removeAnimationFromPhotoButton];
    [self.delegate messagesInputToolbar:self didPressLeftBarButton:sender];
}

- (void)rightBarButtonPressed:(UIButton *)sender
{
    [self removeAnimationFromPhotoButton];
    
    if(sender.tag == 45001) {
        [self.delegate messagesInputToolbar:self didPressCuratedDealBarButton:sender];
    }
    else if(sender.tag == 45002) {
        [self.delegate messagesInputToolbar:self didPressRightBarButton:sender];
    }
    else if(sender.tag == 45003) {
        [sender.layer removeAnimationForKey:@"SpinAnimation"];
        [self.delegate messagesInputToolbar:self didPressPhotoSharingButton:sender];
    }
}

-(void)leftQuizBarButtonPressed:(UIButton *)sender{
    [self removeAnimationFromPhotoButton];
    [self.delegate messagesInputToolbar:self didPressLeftQuizBarButton:sender];
}

- (void)rightBarShareButtonPressed:(UIButton*)sender {
    [self removeAnimationFromPhotoButton];
    if ([self.delegate respondsToSelector:@selector(messagesInputToolbar:didPressRightShareBarButton:)]) {
         [self.delegate messagesInputToolbar:self didPressRightShareBarButton:sender];
    }
}

#pragma mark - Input toolbar

- (void)toggleSendButtonEnabled
{
    BOOL hasText = [self.contentView.textView hasText];
    
    if (self.sendButtonOnRight && self.contentView.rightBarButtonItem.tag == 45002) {
        self.contentView.rightBarButtonItem.enabled = (hasText && self.isSendOptionEnabled);
    }
}

#pragma mark - Key-value observing

-(void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disableSendActionHandler:) name:TMDISABLESENDACTION_NOTIFICATION object:nil];
}
-(void)disableSendActionHandler:(NSNotification*)notification {
    //NSLog(@"disableSendActionHandler");
    NSDictionary *userInfo = [notification userInfo];
    BOOL status = [[userInfo objectForKey:@"connectionstatus"] boolValue];
    if(status) {
        //NSLog(@"disableSendActionHandler disabled:%d",status);
        self.isSendOptionEnabled = FALSE;
        [self toggleSendButtonEnabled];
        self.contentView.leftBarButtonContainerView.userInteractionEnabled = FALSE;
        self.contentView.leftBarButtonContainerView.alpha = 0.5;
        self.contentView.leftBarQuizContainerView.userInteractionEnabled = FALSE;
        self.contentView.leftBarQuizContainerView.alpha = 0.5;
        self.contentView.rightBarButtonContainerView.userInteractionEnabled = FALSE;
        self.contentView.rightBarButtonContainerView.alpha = 0.5;
    }
    else {
        //NSLog(@"disableSendActionHandler enabled:%d",status);
        self.isSendOptionEnabled = TRUE;
        [self toggleSendButtonEnabled];
        self.contentView.leftBarButtonContainerView.userInteractionEnabled = TRUE;
        self.contentView.leftBarButtonContainerView.alpha = 1.0;
        self.contentView.leftBarQuizContainerView.userInteractionEnabled = TRUE;
        self.contentView.leftBarQuizContainerView.alpha = 1.0;
        self.contentView.rightBarButtonContainerView.userInteractionEnabled = TRUE;
        self.contentView.rightBarButtonContainerView.alpha = 1.0;
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == KTMMessagesInputToolbarKeyValueObservingContext) {
        if (object == self.contentView) {
            
            if ([keyPath isEqualToString:NSStringFromSelector(@selector(leftBarButtonItem))]) {
                
                [self.contentView.leftBarButtonItem removeTarget:self
                                                          action:NULL
                                                forControlEvents:UIControlEventTouchUpInside];
                
                [self.contentView.leftBarButtonItem addTarget:self
                                                       action:@selector(leftBarButtonPressed:)
                                             forControlEvents:UIControlEventTouchUpInside];
                
            }
            else if ([keyPath isEqualToString:NSStringFromSelector(@selector(rightBarButtonItem))]) {
                
                [self.contentView.rightBarButtonItem removeTarget:self
                                                           action:NULL
                                                 forControlEvents:UIControlEventTouchUpInside];
                
                [self.contentView.rightBarButtonItem addTarget:self
                                                        action:@selector(rightBarButtonPressed:)
                                              forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([keyPath isEqualToString:NSStringFromSelector(@selector(leftQuizBarButtonItem))]) {
                
                [self.contentView.leftQuizBarButtonItem removeTarget:self
                                                           action:NULL
                                                 forControlEvents:UIControlEventTouchUpInside];
                
                [self.contentView.leftQuizBarButtonItem addTarget:self
                                                        action:@selector(leftQuizBarButtonPressed:)
                                              forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([keyPath isEqualToString:NSStringFromSelector(@selector(rightShareBarButtonItem))]) {
                
                [self.contentView.rightShareBarButtonItem removeTarget:self
                                                              action:NULL
                                                    forControlEvents:UIControlEventTouchUpInside];
                
                [self.contentView.rightShareBarButtonItem addTarget:self
                                                           action:@selector(rightBarShareButtonPressed:)
                                                 forControlEvents:UIControlEventTouchUpInside];
            }

            
            [self toggleSendButtonEnabled];
        }
    }
}

- (void)addObservers
{
    if (self.isObserving) {
        return;
    }
    
    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(leftBarButtonItem))
                          options:0
                          context:KTMMessagesInputToolbarKeyValueObservingContext];
    
    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(leftQuizBarButtonItem))
                          options:0
                          context:KTMMessagesInputToolbarKeyValueObservingContext];
    
    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(rightBarButtonItem))
                          options:0
                          context:KTMMessagesInputToolbarKeyValueObservingContext];
    
    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(rightShareBarButtonItem))
                          options:0
                          context:KTMMessagesInputToolbarKeyValueObservingContext];

    
    self.isObserving = YES;
}

- (void)removeObservers
{
    if (!_isObserving) {
        return;
    }
    
    @try {
        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(leftBarButtonItem))
                             context:KTMMessagesInputToolbarKeyValueObservingContext];
        
        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(leftQuizBarButtonItem))
                             context:KTMMessagesInputToolbarKeyValueObservingContext];
        
        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(rightBarButtonItem))
                             context:KTMMessagesInputToolbarKeyValueObservingContext];
        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(rightShareBarButtonItem))
                             context:KTMMessagesInputToolbarKeyValueObservingContext];

    }
    @catch (NSException *__unused exception) { }
    
    _isObserving = NO;
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObservers];
    _contentView = nil;
}

-(void)animatePhotoShareButtonForMatchId:(NSString *)matchId {
    UIButton *photoShareButton = [(UIButton *)self.contentView.rightBarButtonItem viewWithTag:45003];
    if(photoShareButton){
        //[photoShareButton.layer removeAnimationForKey:@"SpinAnimation"];
        TMUserSession *userSession = [TMUserSession sharedInstance];
        if([userSession canShakePhotoIconOnChatWithMatchId:matchId]){
            CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            animation.fromValue = [NSNumber numberWithFloat:( -20* M_PI  / 180)];
            animation.toValue = [NSNumber numberWithFloat: ( 20 * M_PI  / 180)];
            animation.duration = 0.2f;
            animation.repeatCount = INFINITY;
            animation.autoreverses = true;
            [photoShareButton.layer addAnimation:animation forKey:@"SpinAnimation"];
        }
    }
}

-(void)removeAnimationFromPhotoButton {
    UIButton *photoShareButton = [(UIButton *)self.contentView.rightBarButtonItem viewWithTag:45003];
    if(photoShareButton){
        [photoShareButton.layer removeAnimationForKey:@"SpinAnimation"];
    }
}

@end


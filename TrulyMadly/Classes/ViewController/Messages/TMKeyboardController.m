//
//  TMKeyboardController.m
//  TrulyMadly
//
//  Created by Ankit on 16/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMKeyboardController.h"


NSString * const TMMessagesKeyboardControllerNotificationKeyboardDidChangeFrame = @"TMMessagesKeyboardControllerNotificationKeyboardDidChangeFrame";
NSString * const TMMessagesKeyboardControllerUserInfoKeyKeyboardDidChangeFrame = @"TMMessagesKeyboardControllerUserInfoKeyKeyboardDidChangeFrame";

static void * KTMMessagesKeyboardControllerKeyValueObservingContext = &KTMMessagesKeyboardControllerKeyValueObservingContext;

typedef void (^TMAnimationCompletionBlock)(BOOL finished);


@interface TMKeyboardController () <UIGestureRecognizerDelegate>

@property (assign, nonatomic) BOOL isObserving;

@property (weak, nonatomic) UIView *keyboardView;

@property (assign, nonatomic) BOOL isTapGestureInAction;

- (void)registerForNotifications;
- (void)unregisterForNotifications;

- (void)didReceiveKeyboardDidShowNotification:(NSNotification *)notification;
- (void)didReceiveKeyboardWillChangeFrameNotification:(NSNotification *)notification;
- (void)didReceiveKeyboardDidChangeFrameNotification:(NSNotification *)notification;
- (void)didReceiveKeyboardDidHideNotification:(NSNotification *)notification;
- (void)handleKeyboardNotification:(NSNotification *)notification completion:(TMAnimationCompletionBlock)completion;

- (void)setKeyboardViewHidden:(BOOL)hidden;

- (void)notifyKeyboardFrameNotificationForFrame:(CGRect)frame;

- (void)removeKeyboardFrameObserver;

- (void)handlePanGestureRecognizer:(UIPanGestureRecognizer *)pan;

@end


@implementation TMKeyboardController

#pragma mark - Initialization

- (instancetype)initWithTextView:(UITextView *)textView
                     contextView:(UIView *)contextView
            panGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer
                        delegate:(id<TMKeyboardControllerDelegate>)delegate

{
    NSParameterAssert(textView != nil);
    NSParameterAssert(contextView != nil);
    NSParameterAssert(panGestureRecognizer != nil);
    
    self = [super init];
    if (self) {
        _textView = textView;
        _contextView = contextView;
        _panGestureRecognizer = panGestureRecognizer;
        _delegate = delegate;
        _isObserving = NO;
    }
    return self;
}

- (instancetype)initWithTextView:(UITextView *)textView
                     contextView:(UIView *)contextView
            panGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer
            tapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer
                        delegate:(id<TMKeyboardControllerDelegate>)delegate

{
    NSParameterAssert(textView != nil);
    NSParameterAssert(contextView != nil);
    NSParameterAssert(panGestureRecognizer != nil);
    
    self = [super init];
    if (self) {
        _textView = textView;
        _contextView = contextView;
        _panGestureRecognizer = panGestureRecognizer;
        _tapGestureRecognizer = tapGestureRecognizer;
        _delegate = delegate;
        _isObserving = NO;
    }
    return self;
}

- (void)dealloc
{
    [self removeKeyboardFrameObserver];
    [self unregisterForNotifications];
    _textView = nil;
    _contextView = nil;
    _panGestureRecognizer = nil;
    _delegate = nil;
    _keyboardView = nil;
}

#pragma mark - Setters

- (void)setKeyboardView:(UIView *)keyboardView
{
    if (_keyboardView) {
        [self removeKeyboardFrameObserver];
    }
    
    _keyboardView = keyboardView;
    
    if (keyboardView && !_isObserving) {
        [_keyboardView addObserver:self
                        forKeyPath:NSStringFromSelector(@selector(frame))
                           options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew)
                           context:KTMMessagesKeyboardControllerKeyValueObservingContext];
        
        _isObserving = YES;
    }
}

#pragma mark - Getters

- (BOOL)keyboardIsVisible
{
    return self.keyboardView != nil;
}

- (CGRect)currentKeyboardFrame
{
    if (!self.keyboardIsVisible) {
        return CGRectNull;
    }
    
    return self.keyboardView.frame;
}

#pragma mark - Keyboard controller

- (void)beginListeningForKeyboard
{
    if (self.textView.inputAccessoryView == nil) {
        self.textView.inputAccessoryView = [[UIView alloc] init];
    }
    
    [self registerForNotifications];
}

- (void)endListeningForKeyboard
{
    [self unregisterForNotifications];
    
    [self setKeyboardViewHidden:NO];
    self.keyboardView = nil;
}

#pragma mark - Notifications

-(void)hideKeyboard {
    self.keyboardView.userInteractionEnabled = true;
    [self setKeyboardViewHidden:YES];
    self.isTapGestureInAction = false;
    [self removeKeyboardFrameObserver];
    [self.textView resignFirstResponder];
}

- (void)registerForNotifications
{
    [self unregisterForNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardDidShowNotification:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillChangeFrameNotification:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardDidChangeFrameNotification:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardDidHideNotification:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)unregisterForNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveKeyboardDidShowNotification:(NSNotification *)notification
{
    self.keyboardView = self.textView.inputAccessoryView.superview;
    [self setKeyboardViewHidden:NO];
    
    [self handleKeyboardNotification:notification completion:^(BOOL finished) {
        [self.panGestureRecognizer addTarget:self action:@selector(handlePanGestureRecognizer:)];
        [self.tapGestureRecognizer addTarget:self action:@selector(tapAction)];
    }];
}

- (void)didReceiveKeyboardWillChangeFrameNotification:(NSNotification *)notification
{
    [self handleKeyboardNotification:notification completion:nil];
}

- (void)didReceiveKeyboardDidChangeFrameNotification:(NSNotification *)notification
{
    [self setKeyboardViewHidden:NO];
    
    [self handleKeyboardNotification:notification completion:nil];
}

- (void)didReceiveKeyboardDidHideNotification:(NSNotification *)notification
{
    self.keyboardView = nil;
    
    [self handleKeyboardNotification:notification completion:^(BOOL finished) {
        [self.panGestureRecognizer removeTarget:self action:NULL];
        
        [self.delegate keyboardControllerKeyboardDidHide:self];
    }];
}

- (void)handleKeyboardNotification:(NSNotification*)notification completion:(TMAnimationCompletionBlock)completion
{
    NSDictionary *userInfo = [notification userInfo];
    
    CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //NSLog(@"keyboardEndFrame:%@",NSStringFromCGRect(keyboardEndFrame));
    
    if (CGRectIsNull(keyboardEndFrame)) {
        return;
    }
    
    UIViewAnimationCurve animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSInteger animationCurveOption = (animationCurve << 16);
    
    double animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGRect keyboardEndFrameConverted = [self.contextView convertRect:keyboardEndFrame fromView:nil];
    //NSLog(@"keyboardEndFrameConverted:%@",NSStringFromCGRect(keyboardEndFrameConverted));
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurveOption
                     animations:^{
                         [self notifyKeyboardFrameNotificationForFrame:keyboardEndFrameConverted];
                     }
                     completion:^(BOOL finished) {
                         if (completion) {
                             completion(finished);
                         }
                     }];
}

#pragma mark - Utilities

- (void)setKeyboardViewHidden:(BOOL)hidden
{
    self.keyboardView.hidden = hidden;
    self.keyboardView.userInteractionEnabled = !hidden;
}

- (void)notifyKeyboardFrameNotificationForFrame:(CGRect)frame
{
    [self.delegate keyboardController:self keyboardDidChangeFrame:frame];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TMMessagesKeyboardControllerNotificationKeyboardDidChangeFrame
                                                        object:self
                                                      userInfo:@{ TMMessagesKeyboardControllerUserInfoKeyKeyboardDidChangeFrame : [NSValue valueWithCGRect:frame] }];
}

#pragma mark - Key-value observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == KTMMessagesKeyboardControllerKeyValueObservingContext) {
        
        if (object == self.keyboardView && [keyPath isEqualToString:NSStringFromSelector(@selector(frame))]) {
            
            CGRect oldKeyboardFrame = [[change objectForKey:NSKeyValueChangeOldKey] CGRectValue];
            CGRect newKeyboardFrame = [[change objectForKey:NSKeyValueChangeNewKey] CGRectValue];
            
            if (CGRectEqualToRect(newKeyboardFrame, oldKeyboardFrame) || CGRectIsNull(newKeyboardFrame)) {
                return;
            }
            
            //  do not convert frame to contextView coordinates here
            //  KVO is triggered during panning (see below)
            //  panning occurs in contextView coordinates already
            [self notifyKeyboardFrameNotificationForFrame:newKeyboardFrame];
        }
    }
}

- (void)removeKeyboardFrameObserver
{
    if (!_isObserving) {
        return;
    }
    
    @try {
        [_keyboardView removeObserver:self
                           forKeyPath:NSStringFromSelector(@selector(frame))
                              context:KTMMessagesKeyboardControllerKeyValueObservingContext];
    }
    @catch (NSException * __unused exception) { }
    
    _isObserving = NO;
}

#pragma mark - Pan gesture recognizer

-(void)tapAction {
    self.isTapGestureInAction = true;
    
    CGFloat contextViewWindowHeight = CGRectGetHeight(self.contextView.window.frame);
    CGRect newKeyboardViewFrame = self.keyboardView.frame;
    
    newKeyboardViewFrame.origin.y = contextViewWindowHeight;
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationCurveEaseOut
                     animations:^{
                         self.keyboardView.frame = newKeyboardViewFrame;
                     }
                     completion:^(BOOL finished) {
                         self.keyboardView.userInteractionEnabled = true;
                         self.isTapGestureInAction = false;
                         [self setKeyboardViewHidden:YES];
                         [self removeKeyboardFrameObserver];
                         [self.textView resignFirstResponder];
                         
                     }];
}

- (void)handlePanGestureRecognizer:(UIPanGestureRecognizer *)pan
{
    if(!self.isTapGestureInAction) {
        CGPoint touch = [pan locationInView:self.contextView];
        
        //  system keyboard is added to a new UIWindow, need to operate in window coordinates
        //  also, keyboard always slides from bottom of screen, not the bottom of a view
        CGFloat contextViewWindowHeight = CGRectGetHeight(self.contextView.window.frame);
        
        CGFloat keyboardViewHeight = CGRectGetHeight(self.keyboardView.frame);
        
        CGFloat dragThresholdY = (contextViewWindowHeight - keyboardViewHeight - self.keyboardTriggerPoint.y);
        
        CGRect newKeyboardViewFrame = self.keyboardView.frame;
        
        BOOL userIsDraggingNearThresholdForDismissing = (touch.y > dragThresholdY);
        
        self.keyboardView.userInteractionEnabled = !userIsDraggingNearThresholdForDismissing;
        
        switch (pan.state) {
            case UIGestureRecognizerStateChanged:
            {
                self.isKeyboardScrolling = true;
                newKeyboardViewFrame.origin.y = touch.y + self.keyboardTriggerPoint.y;
                
                //  bound frame between bottom of view and height of keyboard
                newKeyboardViewFrame.origin.y = MIN(newKeyboardViewFrame.origin.y, contextViewWindowHeight);
                newKeyboardViewFrame.origin.y = MAX(newKeyboardViewFrame.origin.y, contextViewWindowHeight - keyboardViewHeight);
                
                if (CGRectGetMinY(newKeyboardViewFrame) == CGRectGetMinY(self.keyboardView.frame)) {
                    return;
                }
                
                [UIView animateWithDuration:0.0
                                      delay:0.0
                                    options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionTransitionNone
                                 animations:^{
                                     self.keyboardView.frame = newKeyboardViewFrame;
                                 }
                                 completion:nil];
            }
                break;
                
            case UIGestureRecognizerStateEnded:
            case UIGestureRecognizerStateCancelled:
            case UIGestureRecognizerStateFailed:
            {
                if(self.isKeyboardScrolling) {
                    self.isKeyboardScrolling = false;
                    [self.delegate keyboardControllerKeyboardWillStopPanning:self];
                }
                
                BOOL keyboardViewIsHidden = (CGRectGetMinY(self.keyboardView.frame) >= contextViewWindowHeight);
                if (keyboardViewIsHidden) {
                    return;
                }
                
                CGPoint velocity = [pan velocityInView:self.contextView];
                BOOL userIsScrollingDown = (velocity.y > 0.0f);
                BOOL shouldHide = (userIsScrollingDown && userIsDraggingNearThresholdForDismissing);
                
                newKeyboardViewFrame.origin.y = shouldHide ? contextViewWindowHeight : (contextViewWindowHeight - keyboardViewHeight);
                
                [UIView animateWithDuration:0.25
                                      delay:0.0
                                    options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationCurveEaseOut
                                 animations:^{
                                     
                                     self.keyboardView.frame = newKeyboardViewFrame;
                                 }
                                 completion:^(BOOL finished) {
                                     self.keyboardView.userInteractionEnabled = !shouldHide;
                                     
                                     if (shouldHide) {
                                         [self setKeyboardViewHidden:YES];
                                         [self removeKeyboardFrameObserver];
                                         [self.textView resignFirstResponder];
                                     }
                                 }];
            }
                break;
                
            default:
                break;
        }
        
    }
}

@end

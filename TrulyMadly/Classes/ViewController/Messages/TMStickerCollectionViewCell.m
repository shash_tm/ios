//
//  TMStickerCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 12/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMStickerManager.h"

@interface TMStickerCollectionViewCell ()

@property(nonatomic,strong)UIActivityIndicatorView* activityIndicatorView;


@end


@implementation TMStickerCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        //self
        CGRect bounds = self.bounds;
        CGFloat offset = 12;
        self.stickerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.origin.x+offset,
                                                                              bounds.origin.y+offset,
                                                                              bounds.size.width-2*offset,
                                                                              bounds.size.height-2*offset)];
        self.stickerImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.stickerImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.stickerImageView];
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:self.activityIndicatorView];
        self.activityIndicatorView.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        self.activityIndicatorView.hidden = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloaded:) name:@"imageDownloaded" object:nil];
    }
    return self;
}

+(NSString*)reusableIdentifier {
    return @"stickercell";
}


- (void) setStickerImage:(UIImage*) stickerImage {
    if (stickerImage) {
        [self.stickerImageView setImage:stickerImage];
        self.stickerImageView.hidden = NO;
        self.activityIndicatorView.hidden = YES;
        [self.activityIndicatorView stopAnimating];
        [self sendSubviewToBack:self.activityIndicatorView];
    }
    else {
        [self.stickerImageView setImage:nil];
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        [self bringSubviewToFront:self.activityIndicatorView];
    }
}

- (void) imageDownloaded:(NSNotification*) notification {
    NSString* downloadedURL = ([notification.object isKindOfClass:[NSString class]])?notification.object:nil;
    if ([downloadedURL isEqualToString:self.sticker.thumbnailQualityImageURLString]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.stickerImageView.image = [[TMStickerManager sharedStickerManager] getImageForSticker:self.sticker];
            self.activityIndicatorView.hidden = YES;
            [self.activityIndicatorView stopAnimating];
        });
    }
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
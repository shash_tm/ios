//
//  TMStickerCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 12/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSticker.h"


@interface TMStickerCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong)UIImageView *stickerImageView;
@property(nonatomic,strong)TMSticker* sticker;

+(NSString*)reusableIdentifier;

- (void) setStickerImage:(UIImage*) stickerImage;

@end

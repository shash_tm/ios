//
//  TMMessagesInputToolbar.h
//  TrulyMadly
//
//  Created by Ankit on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMMessagesToolbarContentView.h"

FOUNDATION_EXPORT const CGFloat kTMMessagesInputToolbarHeightDefault;

@class TMMessagesInputToolbar;

@protocol TMMessagesInputToolbarDelegate <UIToolbarDelegate>

@required

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
      didPressRightBarButton:(UIButton *)sender;

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
      didPressCuratedDealBarButton:(UIButton *)sender;

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
      didPressPhotoSharingButton:(UIButton *)sender;

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
       didPressLeftBarButton:(UIButton *)sender;

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
       didPressLeftQuizBarButton:(UIButton *)sender;

@optional
- (void)messagesInputToolbar:(TMMessagesInputToolbar*) toolbar
 didPressRightShareBarButton:(UIButton*) sender;

@end


@interface TMMessagesInputToolbar : UIToolbar

@property (weak, nonatomic) id<TMMessagesInputToolbarDelegate> delegate;

@property (weak, nonatomic, readonly) TMMessagesToolbarContentView *contentView;

@property (assign, nonatomic) BOOL sendButtonOnRight;

@property (nonatomic, assign) BOOL isSendOptionEnabled;

//-(void)setupActionButtonWithMediaButtonStatus:(BOOL)addMediaButtons
//                          unreadQuizzesStatus:(BOOL)unreadQuizStatus
//                                    buttonTag:(int)buttonTag;
-(void)setupActionButtonWithMediaButtonStatus:(BOOL)addMediaButtons unreadQuizzesStatus:(BOOL) unreadQuizStatus stickerBlooperStatus:(BOOL) stickerBlooperStatus buttonTag:(int) buttonTag;

-(void)setUpToolBarForCommonAnswersWithFrame:(CGRect)superViewFrame;
-(void)setupCommonAnswerActionButtonWithMediaButtonStatus:(BOOL)addMediaButtons;
-(void)setupSendButtonWithBlooperStatus:(BOOL)blooperStatus;
-(void)setupPhotoShareButton;
//-(void)setupDealsButtonWithAlpha:(CGFloat)alpha;
-(void)toggleSendButtonEnabled;
-(void)setupSendButtonOnly;

-(void)setupActionButtonForMsTmWithBlooperStatus:(BOOL) stickerBlooperStatus;

-(void)animatePhotoShareButtonForMatchId:(NSString *)matchId;

@end

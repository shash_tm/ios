//
//  TMBlockUserViewController.h
//  TrulyMadly
//
//  Created by Ankit on 09/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMBlockUserViewControllerDelegate;

@interface TMBlockUserViewController : UIViewController

@property(nonatomic,weak)id<TMBlockUserViewControllerDelegate> delegate;
@property(nonatomic,strong)NSString* loadingText;

-(instancetype)initWithBlockReason:(NSArray*)blockReasons blockFlags:(NSArray*)blockFlags;
-(void)setHeaderTitle:(NSString*)headerTitle;

@end

@protocol TMBlockUserViewControllerDelegate <NSObject>

-(void)blockUserWithReason:(NSString*)reasonText;

@end


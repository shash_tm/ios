//
//  TMArchiveConversationViewController.m
//  TrulyMadly
//
//  Created by Ankit on 12/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMArchiveConversationViewController.h"
#import "TMMessageListViewCell.h"
#import "TMConversation.h"
#import "TMErrorMessages.h"
#import "TMMessagingController.h"
#import "TMMessageConversationViewController.h"
#import "TMNotificationHeaders.h"
#import "TMTimestampFormatter.h"


@interface TMArchiveConversationViewController ()<UITableViewDataSource,UITableViewDelegate,TMMessageConversationViewControllerDelegate,UIAlertViewDelegate>

@property(nonatomic,strong)NSMutableArray *conversationList;
@property(nonatomic,strong)UITableView *messageConvTableView;
@property(nonatomic,strong)UILabel *msgLabel;
@property(nonatomic,strong)TMConversation *activeConversation;
@property(nonatomic,assign)NSInteger activeConversationIndex;
@property(nonatomic,assign)NSInteger blockUserIndex;

@end

@implementation TMArchiveConversationViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.conversationList = [NSMutableArray array];
        self.activeConversationIndex = -1;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addTableview];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = @"Archived Chats";
//    self.activeConversation = nil;
    self.activeConversationIndex = -1;
    [self getCachedMessageConversation];
}
-(void)viewDidLayoutSubviews {
    self.messageConvTableView.frame = self.view.bounds;
    self.msgLabel.frame = CGRectMake(0,
                                     (self.view.frame.size.height-30)/2,
                                     self.view.frame.size.width,
                                     30);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    self.delegate = nil;
}
-(UILabel*)msgLabel {
    if(!_msgLabel) {
        _msgLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.msgLabel.text = @"No Archived Conversations";
        self.msgLabel.textAlignment = NSTextAlignmentCenter;
        self.msgLabel.font = [UIFont systemFontOfSize:20];
        self.msgLabel.textColor = [UIColor lightGrayColor];
        self.msgLabel.frame = CGRectMake(0,
                                         (self.view.frame.size.height-30)/2,
                                         self.view.frame.size.width,
                                         30);
    }
    return _msgLabel;
}
-(void)addTableview {
    self.messageConvTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.messageConvTableView.dataSource = self;
    self.messageConvTableView.delegate = self;
    self.messageConvTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 12)];
    self.messageConvTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)getCachedMessageConversation {
    [[TMMessagingController sharedController] getConversationData:^(NSDictionary *conversationData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *archivedConversationList = conversationData[@"archiveconvlist"];
            [self.conversationList removeAllObjects];
            if(archivedConversationList.count) {
                [self.msgLabel removeFromSuperview];
                self.msgLabel = nil;
                [self.view addSubview:self.messageConvTableView];
                [self.view bringSubviewToFront:self.messageConvTableView];
                [self.conversationList addObjectsFromArray:archivedConversationList];
                [self.messageConvTableView reloadData];
            }
            else {
                [self.view addSubview:self.msgLabel];
                [self.messageConvTableView removeFromSuperview];
            }
        });
    }];
}

#pragma mark tableview datasource / delegate methods -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.conversationList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reusableIdentifier = @"mlcell";
    TMMessageListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if(!cell) {
        cell = [[TMMessageListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TMConversation *conversation = [self.conversationList objectAtIndex:indexPath.row];
    [cell setMessageListData:conversation];
    
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TMConversation *messageConversation = [self.conversationList objectAtIndex:indexPath.row];
    
    if([messageConversation isBlocked]) {
        self.blockUserIndex = indexPath.row;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:EM_BLOCK_ALERT_MSG message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alertView.tag = 100;
            [alertView show];
        });
    }
    else {
        self.title = nil;
        if(messageConversation.conversationMessageType == CONVERSATIONMESSAGE_MUTUALLIKE) {
            [self updateConversationAsRead:messageConversation];
        }
        self.activeConversation = messageConversation;
        self.activeConversationIndex = indexPath.row;
        
        //update cell
        TMMessageListViewCell *cell = (TMMessageListViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        [cell setCellStateForReadConversation];
        
        ////move to oto viewcontroller
        dispatch_async(dispatch_get_main_queue(), ^{
            TMMessageConversationViewController *msgConvViewCon = [self messageConvViewControllerForConversation:messageConversation];
            msgConvViewCon.actionDelegate = self.actionDelegate;
            [self.navigationController pushViewController:msgConvViewCon animated:YES];
        });
    }
}
-(void)updateConversationAsBlocked:(TMConversation*)conversationMessage {
    [[TMMessagingController sharedController] markConversationAsBlockedAndShown:[conversationMessage getChatContext]];
}
-(void)updateConversationAsRead:(TMConversation*)conversationMessage {
    [[TMMessagingController sharedController] markConversationAsRead:[conversationMessage getChatContext]];
}

-(TMMessageConversationViewController*)messageConvViewControllerForConversation:(TMConversation*)messageConversation {
    TMMessageConfiguration *messageConfg = [[TMMessageConfiguration alloc] init];
    messageConfg.messageConversationFullLink = messageConversation.fullConvLink;
    messageConfg.matchUserId = messageConversation.matchId;
    messageConfg.fName = messageConversation.fName;
    messageConfg.profileImageURLString = messageConversation.profileImageURL;
    messageConfg.profileURLString = messageConversation.profileURLString;
    messageConfg.clearChatTs = messageConversation.clearChatTs;
    messageConfg.isMsTm = messageConversation.isMsTm;
    messageConfg.dealState = messageConversation.dealState;
    messageConfg.isConversationHasUnreadMessage = [messageConversation isUnreadMessageConversation];
    TMMessageConversationViewController *msgConvViewCon = [[TMMessageConversationViewController alloc] initWithMessageConversation:messageConfg];
    msgConvViewCon.messageDelegate = self;
    return msgConvViewCon;
}

-(void)blockCurrentConversation {
    [self.conversationList removeObjectAtIndex:self.activeConversationIndex];
    [self.messageConvTableView reloadData];
}
-(void)willUnmatchCurrentConversation {
    [self.conversationList removeObjectAtIndex:self.activeConversationIndex];
    [self.messageConvTableView reloadData];
    [self postSocketConnectionNotificationWithStatus];
}
-(void)reloadCachedConversation {
    [self postSocketConnectionNotificationWithStatus];
}
-(void)postSocketConnectionNotificationWithStatus {
    [[NSNotificationCenter defaultCenter] postNotificationName:TMNEWMESSAGE_NOTIFICATION
                                                        object:nil
                                                      userInfo:nil];
}


#pragma mark - Alert Delegate Handler
#pragma mark -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 100) {
        TMConversation *conversationMessage = [self.conversationList objectAtIndex:self.blockUserIndex];
        [[TMMessagingController sharedController] executeBlockUserCall:[NSString stringWithFormat:@"%ld",(long)conversationMessage.matchId]];
        [self.conversationList removeObjectAtIndex:self.blockUserIndex];
        [self.messageConvTableView reloadData];
        //mark as block shown
        [self updateConversationAsBlocked:conversationMessage];
        
        [self postSocketConnectionNotificationWithStatus];
    }
}

@end

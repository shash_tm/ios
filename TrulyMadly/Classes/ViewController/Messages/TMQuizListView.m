//
//  TMQuizListView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizListView.h"
#import "TMQuizCollectionViewCell.h"
#import "TMQuizListCollectionViewCell.h"
#import "UIColor+TMColorAdditions.h"
#import "TMMessagingController.h"
#import "TMQuizController.h"
#import "TMAnalytics.h"
#import "TMLog.h"
#import "TMUserSession.h"

@interface TMQuizListView ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>

@property(nonatomic,strong)NSArray *quizList;
@property(nonatomic,strong)NSArray *quizStatus;
@property(nonatomic,strong)UICollectionView *quizCollectionView;
@property(nonatomic,assign)CGSize cellSize;
@property(nonatomic,weak) TMQuizController* quizController;
@property(nonatomic,strong)NSMutableDictionary* eventDict;
@property(nonatomic,strong)NSArray* unreadQuizzesArray;
@property(nonatomic,strong)NSArray* flareArray;
@property(nonatomic,strong)NSArray* userPlayedQuizIDArray;
@property(nonatomic,strong)NSArray* matchPlayedQuizIDArray;
@property(nonatomic,strong)NSMutableArray* animatedQuizzesArray;
@property(nonatomic,assign)BOOL hasQuizGalleryBeenShown;

@end

#define QUIZ_COLLECTION_VIEW_LIST_CELL_REUSE_IDENTIFIER @"QuizListCollectionViewCell"

@implementation TMQuizListView

-(instancetype)initWithFrame:(CGRect)frame withCellSize:(CGSize)cellSize quizContoller:(TMQuizController*) quizController {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor greenColor];
        
        self.quizList = [quizController getAllQuizzesFromDBForLatestQuizVersion];
    
        self.quizController = quizController;
        
        [self configureCollectionView];
        
        [self addHiddenScrollView];
        
        //calculating the width and height of cell
        int cellWidth = (frame.size.width - 2*((UICollectionViewFlowLayout*)self.quizCollectionView.collectionViewLayout).minimumLineSpacing - self.quizCollectionView.contentInset.left - self.quizCollectionView.contentInset.right)/3.07;
        
        int cellHeight = (frame.size.height - ((UICollectionViewFlowLayout*)self.quizCollectionView.collectionViewLayout).minimumInteritemSpacing - self.quizCollectionView.contentInset.top - self.quizCollectionView.contentInset.bottom)/2.1;
        
        self.cellSize = CGSizeMake(cellWidth, cellHeight);

        //initialize the quiz info arrays
        [self updateQuizInfo];
        
        //adding observer for the quiz status update only when the UI is available
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchUpdatedQuizStatus:) name:@"playedQuizzesStatusUpdate" object:nil];
        
        //adding observer for the updated quiz data availability
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedQuizDataAvailable) name:@"updateQuizDataFetched" object:nil];
    }
    return self;
}


#define add scroll view methods
- (void) addHiddenScrollView
{
    UIScrollView* hiddenScrollView = [[UIScrollView alloc] initWithFrame:self.quizCollectionView.bounds];
   
    int noOfPages = (int)self.quizList.count/8;
    
    hiddenScrollView.contentSize = CGSizeMake(self.quizCollectionView.bounds.size.width*noOfPages, self.quizCollectionView.bounds.size.height);
    hiddenScrollView.delegate = self;
    hiddenScrollView.tag = 1234;
    hiddenScrollView.pagingEnabled = YES;
//    [self addSubview:hiddenScrollView];
//   // [self sendSubviewToBack:hiddenScrollView];
//    
//    //configuring the gesture
//    [self addGestureRecognizer:hiddenScrollView.panGestureRecognizer];
//    self.quizCollectionView.panGestureRecognizer.enabled = NO;
}

#define UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView*)scrollView {
    if (scrollView.tag == 1234) { //ignore collection view scrolling callbacks
        CGPoint contentOffset = scrollView.contentOffset;
        contentOffset.x = contentOffset.x - self.quizCollectionView.contentInset.left;
        self.quizCollectionView.contentOffset = contentOffset;
    }
}


#pragma updated quiz data callback method

/**
 * callback method when updated quiz list is available
 * used to relaod the quiz list
 */
- (void) updatedQuizDataAvailable
{
    self.quizList = [self.quizController getAllQuizzesFromDBForLatestQuizVersion];
  
    [self reloadQuizList];
    
    //fetch the updated quiz status
    [self.quizController makeQuizRequestForStatusAndFlare];
    // [self.quizController fetchPlayedQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
}

#define quiz status fetching methods

- (void) fetchUpdatedQuizStatus:(NSNotification*) notification
{
    [self reloadQuizList];
}


-(void)configureCollectionView {
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(self.cellSize.width, self.cellSize.height)];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 4;
    flowLayout.minimumInteritemSpacing = 4;
    flowLayout.sectionInset = UIEdgeInsetsMake(4, 4, 4, 4);
    
    self.quizCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    self.quizCollectionView.backgroundColor = self.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
    self.quizCollectionView.dataSource = self;
    self.quizCollectionView.delegate = self;
    
    [self addSubview:self.quizCollectionView];
    
    [self.quizCollectionView registerClass:[TMQuizCollectionViewCell class] forCellWithReuseIdentifier:[TMQuizCollectionViewCell reusableIdentifier]];
    
    [self.quizCollectionView registerClass:[TMQuizListCollectionViewCell class] forCellWithReuseIdentifier:QUIZ_COLLECTION_VIEW_LIST_CELL_REUSE_IDENTIFIER];
    //registering for custom nibs
    [self.quizCollectionView registerNib:[UINib nibWithNibName:@"TMQuizListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:QUIZ_COLLECTION_VIEW_LIST_CELL_REUSE_IDENTIFIER];
}


#pragma mark - UICollectionview datasource/delegate methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.quizList.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    TMQuizListCollectionViewCell* listCell = [collectionView dequeueReusableCellWithReuseIdentifier:QUIZ_COLLECTION_VIEW_LIST_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    
    [listCell resetQuizState];
    
    NSDictionary *quizData = [[NSDictionary alloc] init];
    quizData = self.quizList[indexPath.row];
    NSURL *imageURL = [NSURL URLWithString:[quizData valueForKey:@"image"]];
    
    BOOL isQuizNew = ([[quizData valueForKey:@"is_new"] intValue] == 1) ? YES:NO;
    
    [listCell setQuizName:([quizData valueForKey:@"display_name"] && ![[quizData valueForKey:@"display_name"] isKindOfClass:[NSNull class]])?[quizData valueForKey:@"display_name"]:@""];
    [listCell setQuizIconFromURL:imageURL];
    [listCell setQuizNewStatus:isQuizNew];
    
   quizStatus currentQuizStatus = [self getQuizStatusForQuizID:[quizData valueForKey:@"quiz_id"]];
    
    BOOL isFlare = [self.flareArray containsObject:[quizData valueForKey:@"quiz_id"]];
   
    BOOL isMatchStatusUpdated = [self.unreadQuizzesArray containsObject:[quizData valueForKey:@"quiz_id"]];
    
    BOOL hasMatchStatusBeenUpdated = NO;
    
    if (![self.animatedQuizzesArray containsObject:[quizData valueForKey:@"quiz_id"]]) {
        [listCell setQuizUnreadStatus:isMatchStatusUpdated];
        if (isMatchStatusUpdated) {
            //add the object to the property array
            [self.animatedQuizzesArray addObject:[quizData valueForKey:@"quiz_id"]];
            
            hasMatchStatusBeenUpdated = YES;
        }
    }
    [listCell setFlareStatus:isFlare];
    
    switch (currentQuizStatus) {
            
            case quizPlayedByBoth:
        {
            NSString* userProfileImageURLString = [self.quizController getUserProfileImageURL];
            NSString* matchProfileImageURLString = [self.delegate getCurrentMatchProfilePic];
            
            if (userProfileImageURLString && matchProfileImageURLString) {
                [listCell setQuizBothPlayedStatusFromUserImageURLString:[self.quizController getUserProfileImageURL] matchImageURLString:[self.delegate getCurrentMatchProfilePic]];
            }
            else if (userProfileImageURLString) {
                [listCell setBothImageWithMatchProfilePlaceholderAndUserImageURLString:userProfileImageURLString];
            }
            else if (matchProfileImageURLString) {
                [listCell setBothImageWithUserProfilePlaceholderAndMatchImageURLString:matchProfileImageURLString];
            }
            else {
                //show placeholders on both of them
                [listCell setBothImagePlaceholder];
            }
              break;
        }
            
            case quizPlayedByNeither:
            [listCell resetQuizUserImages];
            break;
            
            case quizPlayedByUser:
            ([self.quizController getUserProfileImageURL]?([listCell setQuizCommonStatusFromImageURLString:[self.quizController getUserProfileImageURL]]):([listCell setCommonImagePlaceholder]));
            break;
            
            case quizPlayedByMatch:
            ([self.delegate getCurrentMatchProfilePic])?([listCell setQuizCommonStatusFromImageURLString:[self.delegate getCurrentMatchProfilePic]]):([listCell setCommonImagePlaceholder]);
            break;
            
        default:
            break;
    }
    
    if (isQuizNew) {
        //mark the quiz as old
        [self.quizController updateQuizNewStatus:false forQuizID:[quizData valueForKey:@"quiz_id"]];
    }
    
    //update the match status only when quiz gallery is shown
    if (self.hasQuizGalleryBeenShown && isMatchStatusUpdated && hasMatchStatusBeenUpdated) {
        //update the cache for the updated match quiz state
      //not updating the read status here
        //updating the read status of all quizzes altogether upon quiz drawer opening
        //  [self.quizController removeQuizzesStatusForMatchID:[[TMMessagingController sharedController] getCurrentMatchId] userID:[TMUserSession sharedInstance].user.userId quizID:[quizData valueForKey:@"quiz_id"]];
    }
    
    return listCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.cellSize.width, self.cellSize.height);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString* quiz_id = ([(NSDictionary*)self.quizList[indexPath.row] valueForKey:@"quiz_id"]);
    [self.delegate didClickOnQuiz:[self.quizStatus valueForKey:quiz_id]];
    
    //TODO::Abhijeet::need to provied a cleaner delegate check than introspection
    if (self.delegate && [self.delegate isKindOfClass:[UIViewController class]]) {
        
        self.eventDict = [[NSMutableDictionary alloc] init];
        
        BOOL isFlare = [self.flareArray containsObject:quiz_id];
        NSString *key = (isFlare) ? @"flare_quiz_item_in_drawer_clicked" : @"quiz_item_in_drawer_clicked";
        if (quiz_id) {
            //adding event tracking for save quiz action
            [self.eventDict setObject:@"TMQuizGalleryView" forKey:@"screenName"];
            [self.eventDict setObject:quiz_id forKey:@"label"];
            [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
            //[self.eventDict setObject:@"true" forKey:@"GA"];
            [self.eventDict setObject:quiz_id forKey:@"status"];
            [self.eventDict setObject:key forKey:@"eventAction"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
        }
        //make request for decide quiz action
        [self.quizController decideQuizActionForQuizID:quiz_id withBaseViewController:(UIViewController*)self.delegate];
        
        //inform message conversation screen to show activity indicator view
        if ([self.quizController getQuizStatusFromDBForQuizID:quiz_id] == quizPlayedByUser) {
            if ([self.delegate respondsToSelector:@selector(didClickOnNudgeQuiz)]) {
                [self.delegate didClickOnNudgeQuiz];
            }
        }
    }
}


- (quizStatus) getQuizStatusForQuizID:(NSString*) quizID
{
    BOOL isQuizPlayedByUser = [self.userPlayedQuizIDArray containsObject:quizID];
    BOOL isQuizPlayedByMatch = [self.matchPlayedQuizIDArray containsObject:quizID];
    
    quizStatus currentQuizStatus = quizPlayedByNeither;
    
    if (isQuizPlayedByUser) {
        if (isQuizPlayedByMatch) {
            currentQuizStatus = quizPlayedByBoth;
        }
        else {
            currentQuizStatus = quizPlayedByUser;
        }
    }
    else {
        if (isQuizPlayedByMatch) {
            currentQuizStatus = quizPlayedByMatch;
        }
    }
    return currentQuizStatus;
}

- (void) updateQuizInfo {
    
    //update the quiz ids played by user
    self.userPlayedQuizIDArray = [self.quizController getAllQuizzesPlayedByUserID:[TMUserSession sharedInstance].user.userId];
    
    //update the quiz ids played by match
    self.matchPlayedQuizIDArray = [self.quizController getAllQuizzesPlayedByUserID:[[TMMessagingController sharedController] getCurrentMatchId]];
    
    //update the flare array
    self.flareArray = [self.quizController getAllFlareQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
    
    //update the unread quizzes array
    self.unreadQuizzesArray = [self.quizController getUnreadQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId] userID:[TMUserSession sharedInstance].user.userId];
}


- (NSMutableArray*) animatedQuizzesArray {
    if (!_animatedQuizzesArray) {
        _animatedQuizzesArray = [[NSMutableArray alloc] init];
    }
    return _animatedQuizzesArray;
}

- (void) reloadQuizList {
    
    [self updateQuizInfo];
    
    //reload the UI
    [self.quizCollectionView reloadData];
}



- (void) showQuizGallery
{
    self.hasQuizGalleryBeenShown = YES;
    //update all the read quizzes
    [self.quizController removeAllQuizzesStatusForMatchID:[[TMMessagingController sharedController] getCurrentMatchId] userID:[TMUserSession sharedInstance].user.userId];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

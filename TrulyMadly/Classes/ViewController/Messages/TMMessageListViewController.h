//
//  TMMessageViewController.h
//  TrulyMadly
//
//  Created by Ankit on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"

@class TMConversation;


@interface TMMessageListViewController : TMBaseViewController 

@property (nonatomic, strong) NSString* nativeContent;
@property (nonatomic, assign) NSTimeInterval delayTime;

-(instancetype)initWithNavigationToMessgaeConversation:(TMConversation*)conversation;

-(void)showMessageConversation:(TMConversation*)messageConversation;

@end

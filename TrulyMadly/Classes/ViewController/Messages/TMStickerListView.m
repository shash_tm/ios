//
//  TMStickerListView.m
//  TrulyMadly
//
//  Created by Ankit on 11/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerListView.h"
#import "TMStickerCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMUserSession.h"
#import "TMAPIConstants.h"
#import "UIColor+TMColorAdditions.h"
#import "TMBuildConfig.h"
#import "TMStickerView.h"
#import "TMStickerCollectionView.h"
#import "TMSticker.h"
#import "TMStickerManager.h"
#import "TMStickerFileManager.h"
#import "TMAnalytics.h"

@interface TMStickerListView ()<UIScrollViewDelegate, TMStickerViewDelegate, TMStickerViewDataSource>

@property(nonatomic,strong) NSMutableArray *galleryList;
@property(nonatomic,strong) UIScrollView *masterStickerScrollView;
@property(nonatomic,strong) UIScrollView* slidingScrollView;
@property(nonatomic,assign) CGSize cellSize;
@property(nonatomic,assign) int selectedGalleryID;
@property(nonatomic,assign) CGFloat lastContentOffset;
@property(nonatomic,assign) BOOL isStickerGalleryHidden;
@property(nonatomic,strong) NSMutableDictionary* eventDict;

@end


#define TIMESTAMP_GALLERY_ID -121

#define PLACEHOLDER_IMAGE_VIEW_TAG 1234321

#define MASTER_COLLECTION_VIEW_TAG 1098901

#define DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT 123

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)

#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

#define SLIDING_MENU_HEIGHT (IS_IPHONE_6?35:(IS_IPHONE_6_PLUS?42:33))

#define GALLERY_BACKGROUND_COLOR [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0]

@implementation TMStickerListView

/**
 * Designated intializer
 */
-(instancetype)initWithFrame:(CGRect)frame withCellSize:(CGSize)cellSize delegate:(id<TMStickerListViewDelegate>) delegate dataSource:(id<TMStickerListViewDataSource>) dataSource {
    self = [super initWithFrame:frame];
    if(self) {
        self.delegate = delegate;
        self.dataSource = dataSource;
        self.cellSize = cellSize;
        self.selectedGalleryID = -1;
        
        //add ga tracking
        //gallery_shown
        self.eventDict = [[NSMutableDictionary alloc] init];
        
        [self.eventDict setObject:@"sticker" forKey:@"eventCategory"];
        [self.eventDict setObject:@"true" forKey:@"GA"];
        [self.eventDict setObject:@"gallery_shown" forKey:@"eventAction"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    }
    
    //update the UI components
    [self createSlidingAndStickerScrollingViews];
    
    //update the view with first gallery
    self.galleryList = [[self.dataSource getFirstStickerGallery] mutableCopy];
   
    //fetch the first gallery before inserting timestamp object
    self.selectedGalleryID = [self getFirstGalleryID];
    [self updateSlidingAndStickerScrollingViews];
    [self reloadAllStickers];
    
    return self;
}

- (void) createSlidingAndStickerScrollingViews {
   
    //create the sliding scrollable view
    [self createSlidingScrollableMenu];
    
    //create the detail collection view
    [self createStickerCollectionView];
}


/**
 * creates the sliding gallery menu at the bottom
 * @param gallery item array
 */
- (void) createSlidingScrollableMenu
{
    UIColor* galleryBackgroundColor = GALLERY_BACKGROUND_COLOR;
    
    CGRect requiredFrame = CGRectZero;
    
    self.slidingScrollView = [[UIScrollView alloc] initWithFrame:requiredFrame];
    self.slidingScrollView.delegate = self;
    self.slidingScrollView.showsHorizontalScrollIndicator = NO;
    self.slidingScrollView.backgroundColor = galleryBackgroundColor;
    
    //adding tap gesture
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped:)];
    [self.slidingScrollView addGestureRecognizer:tapGestureRecognizer];
    
    self.slidingScrollView.contentSize = CGSizeZero;
    [self addSubview:self.slidingScrollView];
}


/**
 * Updates the sticker collection views
 * @param: sticker gallery array
 */
- (void) createStickerCollectionView
{
    [self configureMasterScrollView];
    
    self.masterStickerScrollView.contentSize = CGSizeZero;
}


/**
 * Configures the master scroll view
 */
-(void)configureMasterScrollView {
    self.masterStickerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height - SLIDING_MENU_HEIGHT)];
    self.masterStickerScrollView.pagingEnabled = YES;
    self.masterStickerScrollView.backgroundColor = self.backgroundColor = [UIColor whiteColor];
    self.masterStickerScrollView.delegate = self;
    self.masterStickerScrollView.showsHorizontalScrollIndicator = NO;
    self.masterStickerScrollView.showsVerticalScrollIndicator = NO;
    self.masterStickerScrollView.tag = MASTER_COLLECTION_VIEW_TAG;
    [self addSubview:self.masterStickerScrollView];
}


/**
 * Updates the UI components - both the sliding gallery bar and the sticker collection views
 */
- (void) updateSlidingAndStickerScrollingViews
{
    //inserting just a dummy object here
    TMSticker* timestampDummySticker = [[TMSticker alloc] init];
    timestampDummySticker.galleryID = [NSString stringWithFormat:@"%d",TIMESTAMP_GALLERY_ID];
    [self.galleryList insertObject:timestampDummySticker atIndex:0];
    
    //update the sliding views
    [self updateSlidingScrollableMenuWithItemArray:self.galleryList];
  
    //update the sticker collection views
    [self updateStickerCollectionViewWithStickerGalleryArray:self.galleryList];
}


/**
 * Updates the sticker collection views
 * @param: sticker gallery array
 */
- (void) updateStickerCollectionViewWithStickerGalleryArray:(NSArray*) stickerGalleryArray
{
    self.masterStickerScrollView.contentSize = CGSizeMake(self.masterStickerScrollView.bounds.size.width*stickerGalleryArray.count, self.masterStickerScrollView.bounds.size.height);
    
    for (int index = 0; index < stickerGalleryArray.count; index++) {
        if ([[stickerGalleryArray objectAtIndex:index] isKindOfClass:[TMSticker class]]) {
            TMSticker* currentGallery = [stickerGalleryArray objectAtIndex:index];
            int currentGalleryID = [currentGallery.galleryID intValue];
            
            if (![self viewWithTag:currentGalleryID*DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT]) {
               
                CGRect collectionViewFrame = CGRectMake(index*self.bounds.size.width,0, self.bounds.size.width, self.bounds.size.height - SLIDING_MENU_HEIGHT);
                
                TMStickerView* stickerCollectionBaseView = [[TMStickerView alloc] initWithFrame:collectionViewFrame cellSize:self.cellSize delegate:self dataSource:self];
                stickerCollectionBaseView.tag = [currentGallery.galleryID intValue]*DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT;
                [self.masterStickerScrollView addSubview:stickerCollectionBaseView];
            }
        }
    }
}


/**
 * creates the sliding gallery menu at the bottom
 * @param gallery item array
 */
- (void) updateSlidingScrollableMenuWithItemArray:(NSArray*) itemArray
{
    UIColor* galleryBackgroundColor = GALLERY_BACKGROUND_COLOR;
    
    CGRect requiredFrame = CGRectMake(0, self.bounds.size.height - SLIDING_MENU_HEIGHT, self.bounds.size.width, SLIDING_MENU_HEIGHT);
    
    CGFloat noOfItemsPerPage = 6.5;
    
    self.slidingScrollView.frame = requiredFrame;
    self.slidingScrollView.delegate = self;
    self.slidingScrollView.showsHorizontalScrollIndicator = NO;
    self.slidingScrollView.backgroundColor = galleryBackgroundColor;
    

    CGFloat cellWidth = (requiredFrame.size.width)/(noOfItemsPerPage);
    CGFloat cellHeight = requiredFrame.size.height;
    
    int xPosition = 0;
    
    int yPosition = cellHeight/2;
    
    xPosition += cellWidth/2;
    
    self.slidingScrollView.contentSize = CGSizeMake(itemArray.count*cellWidth, requiredFrame.size.height);
    
    UIImageView* previousImageView;
    
    for (int index = 0; index < itemArray.count; index++) {
        UIImageView* galleryImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        galleryImageView.contentMode = UIViewContentModeScaleAspectFit;
        if ([galleryImageView viewWithTag:PLACEHOLDER_IMAGE_VIEW_TAG]) {
            [[galleryImageView viewWithTag:PLACEHOLDER_IMAGE_VIEW_TAG] removeFromSuperview];
        }
        
        CGFloat cellPlaceholderImageSize = MIN(cellWidth, cellHeight);
        
        UIImageView* placeholderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellPlaceholderImageSize/2, cellPlaceholderImageSize/2)];
        placeholderImageView.center = galleryImageView.center;
        placeholderImageView.tag = PLACEHOLDER_IMAGE_VIEW_TAG;
        [galleryImageView addSubview:placeholderImageView];
        
        if ([((TMSticker*)[itemArray objectAtIndex:index]).galleryID intValue] == TIMESTAMP_GALLERY_ID) {
            //timestamp gallery
            galleryImageView.image = nil;
            placeholderImageView.image = [UIImage imageNamed:@"sticker_recently_used"];
            galleryImageView.tag = TIMESTAMP_GALLERY_ID;
        }
        else {
            placeholderImageView.frame = CGRectMake(0, 0, 2*cellPlaceholderImageSize/3, 2*cellPlaceholderImageSize/3);
            placeholderImageView.center = galleryImageView.center;
            placeholderImageView.image = [self.dataSource getImageForSticker:((TMSticker*)[itemArray objectAtIndex:index])];
            
            if (!galleryImageView.image) {
                //download it
                [placeholderImageView setImageWithURL:[NSURL URLWithString:((TMSticker*)[itemArray objectAtIndex:index]).thumbnailQualityImageURLString]];
            }
            
            galleryImageView.tag = [((TMSticker*)[itemArray objectAtIndex:index]).galleryID intValue];
        }
        
        if (self.selectedGalleryID == galleryImageView.tag) {
            galleryImageView.backgroundColor = [UIColor whiteColor];
        }
        else {
            galleryImageView.backgroundColor = GALLERY_BACKGROUND_COLOR;
        }
        
        if (index == 0) {
            //first object in the slider
            xPosition = cellWidth/2;
        }
        else {
            xPosition = previousImageView.frame.origin.x + previousImageView.frame.size.width + cellWidth/2;
        }
        galleryImageView.center = CGPointMake(xPosition, yPosition);
        galleryImageView.contentMode = UIViewContentModeScaleAspectFit;
        if (galleryImageView.tag != 0 && [self.slidingScrollView viewWithTag:galleryImageView.tag]) {
            [[self.slidingScrollView viewWithTag:galleryImageView.tag] removeFromSuperview];
        }
        
        [self.slidingScrollView addSubview:galleryImageView];
        
        previousImageView = galleryImageView;
    }
}

#pragma mark - Sticker reload methods

/**
 * Method called by the invoking class to initialize all stickers
 */
- (void) reloadStickerGallery {
    //add ga tracking
    //gallery_shown
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    
    [self.eventDict setObject:@"sticker" forKey:@"eventCategory"];
    [self.eventDict setObject:@"true" forKey:@"GA"];
    [self.eventDict setObject:@"gallery_shown" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    
    BOOL isGalleryUpdated = NO;
    
    int previousCount = (int)self.galleryList.count;
    
    self.galleryList = [[self.dataSource getAllStickerGalleries] mutableCopy];
    
    isGalleryUpdated = (self.galleryList.count > previousCount)?YES:NO;
    
    if (isGalleryUpdated) {
        //fetch the first gallery before inserting timestamp object
        self.selectedGalleryID = [self getFirstGalleryID];
    }
    
    [self updateSlidingAndStickerScrollingViews];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadAllStickers];
    });
}


/**
 * reloads all the sticker galleries
 */
- (void) reloadAllStickers {
    
    //switch to the required detailed collection view
    for (int index = 0; index < self.masterStickerScrollView.subviews.count; index++) {
        if ([self.masterStickerScrollView.subviews objectAtIndex:index].tag%DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT == 0 && [[self.masterStickerScrollView.subviews objectAtIndex:index] isKindOfClass:[TMStickerView class]]) {
          
            TMStickerView* stickerView = [self.masterStickerScrollView.subviews objectAtIndex:index];
            
            int currentGalleryID = (int)stickerView.tag/DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT;
            
            stickerView.stickerArray = (currentGalleryID == TIMESTAMP_GALLERY_ID)?[self.dataSource stickersForTimestampGallery]:[self.dataSource stickersForGalleryID:currentGalleryID];
            
            [stickerView reloadData];
        }
    }
    
    //reload the currently selected gallery to display it afront
    [self reloadCurrentGalleryStickers];
}


/**
 * reloads sticker of current gallery
 */
- (void) reloadCurrentGalleryStickers
{
    //switch to the required detailed collection view
    for (int index = 0; index < self.masterStickerScrollView.subviews.count; index++) {
        if (((UIView*)[self.masterStickerScrollView.subviews objectAtIndex:index]).tag/DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT == self.selectedGalleryID && [[self.masterStickerScrollView.subviews objectAtIndex:index] isKindOfClass:[TMStickerView class]]) {
            
            TMStickerView* stickerView = (TMStickerView*)[self.masterStickerScrollView.subviews objectAtIndex:index];
            
            //get timestamp gallery or the sticker gallery
            stickerView.stickerArray = (self.selectedGalleryID == TIMESTAMP_GALLERY_ID)?[self.dataSource stickersForTimestampGallery]:[self.dataSource stickersForGalleryID:self.selectedGalleryID];
            
            [self.masterStickerScrollView scrollRectToVisible:stickerView.frame animated:NO];
            [stickerView reloadData];
        }
    }
}


/**
 * reloads the timestamp gallery
 */
- (void) reloadTimeStampGalleryStickers
{
    TMStickerView* timeStampStickerView = ([self.masterStickerScrollView viewWithTag:TIMESTAMP_GALLERY_ID*DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT] && [[self.masterStickerScrollView viewWithTag:TIMESTAMP_GALLERY_ID*DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT] isKindOfClass:[TMStickerView class]])?([self.masterStickerScrollView viewWithTag:TIMESTAMP_GALLERY_ID*DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT]):nil;
    if (timeStampStickerView) {
        timeStampStickerView.stickerArray = [self.dataSource stickersForTimestampGallery];
        [timeStampStickerView reloadData];
    }
}


#pragma mark - Sliding scroll view tap callback method

/**
 * Callback method when the sliding gallery is tapped
 * @param tap gesture
 */
- (void) scrollViewTapped:(UITapGestureRecognizer*) tapGestureRecognizer
{
    CGPoint touchLocation = [tapGestureRecognizer locationOfTouch:0 inView:self.slidingScrollView];
    
    BOOL hasStickerGalleryBeenTapped = NO;
    
    for (UIView* subView in self.slidingScrollView.subviews) {
        if ([subView isKindOfClass:[UIImageView class]] && CGRectContainsPoint(subView.frame, touchLocation)) {
            //this image view has been tapped
            UIImageView* tappedGallery = (UIImageView*)subView;
            if (self.selectedGalleryID != tappedGallery.tag) {
                //reload only when a different gallery is chosen
                self.selectedGalleryID = (int)tappedGallery.tag;
                
                [self reloadCurrentGalleryStickers];
                
                tappedGallery.backgroundColor = [UIColor whiteColor];
                hasStickerGalleryBeenTapped = YES;
            }
        }
    }
    if (hasStickerGalleryBeenTapped) {
        for (UIView* subView in self.slidingScrollView.subviews) {
            if ([subView isKindOfClass:[UIImageView class]] && subView.tag != 0 && subView.tag != self.selectedGalleryID) {
                subView.backgroundColor = GALLERY_BACKGROUND_COLOR;
            }
        }
        //track gallery_shown
        [self trackGalleryEvent];
    }
}


#pragma mark - UIScrollView delegate methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.masterStickerScrollView) {
        //reload the sliding menu here
        for (int index = 0;index < self.masterStickerScrollView.subviews.count; index++) {
            if (((UIView*)[self.masterStickerScrollView.subviews objectAtIndex:index]).tag%DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT == 0 && [[self.masterStickerScrollView.subviews objectAtIndex:index] isKindOfClass:[TMStickerView class]]) {
                UIView* requiredSlidingView = [self.slidingScrollView viewWithTag:((UIView*)[self.masterStickerScrollView.subviews objectAtIndex:index]).tag/DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT];
                
                TMStickerView* stickerView = [self.masterStickerScrollView.subviews objectAtIndex:index];
                
                if (CGRectIntersectsRect(self.masterStickerScrollView.bounds, stickerView.frame)) {
                    requiredSlidingView.backgroundColor = [UIColor whiteColor];
                    int selectedGalleryID = (int)([self.masterStickerScrollView.subviews objectAtIndex:index]).tag/DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT;
                    self.selectedGalleryID = selectedGalleryID;
                    
                    stickerView.stickerArray = (self.selectedGalleryID == TIMESTAMP_GALLERY_ID)?[self.dataSource stickersForTimestampGallery]:[self.dataSource stickersForGalleryID:self.selectedGalleryID];
                    [stickerView reloadData];
                }
                else {
                    requiredSlidingView.backgroundColor = GALLERY_BACKGROUND_COLOR;
                }
            }
        }
        //track gallery_shown
        [self trackGalleryEvent];
    }
}

#pragma mark - TMStickerViewDataSource methods

- (UIImage*) getImageForSticker:(TMSticker*) sticker {
    return [self.dataSource getImageForSticker:sticker];
}

#pragma mark - TMStickerViewDelegate methods

- (void) didSendSticker:(TMSticker*) sticker {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSendSticker:)]) {
        [self.delegate didSendSticker:sticker];
    }
}

- (void) storeHDImageForSticker:(TMSticker*) sticker {
    [self.delegate storeHDImageForSticker:sticker stickerRefreshBlock:^(int galleryID, int stickerID){
        //do nothing
    }];
}
- (void) updateTimeStampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID
{
    if ([self.delegate updateTimestampForStickerWithStickerID:stickerID galleryID:galleryID]) {
        //update the timestamp gallery
        [self reloadTimeStampGalleryStickers];
    }
}

#pragma mark - Utility methods

/**
 * gets the first gallery ID
 * @return gallery ID
 */
- (int) getFirstGalleryID {
    if (self.galleryList && self.galleryList.count) {
        if ([[self.galleryList firstObject] isKindOfClass:[TMSticker class]]) {
            TMSticker* firstSticker = [self.galleryList firstObject];
            return [firstSticker.galleryID intValue];
        }
    }
    return 1;
}


- (BOOL) isIndexPathPresentInCollectionView:(UICollectionView*) collectionView WithSection:(int) section item:(int) item {
    if (section < [collectionView numberOfSections]) {
        if (item < [collectionView numberOfItemsInSection:section]) {
            return YES;
        }
    }
    return NO;
}

// pragma mark Tracking Function
-(void)trackGalleryEvent
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMStickerListView" forKey:@"screenName"];
    [eventDict setObject:@"sticker" forKey:@"eventCategory"];
    [eventDict setObject:@"gallery_shown" forKey:@"eventAction"];
    [eventDict setObject:@(self.selectedGalleryID) forKey:@"status"];
    [eventDict setObject:@(self.selectedGalleryID) forKey:@"label"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}
@end

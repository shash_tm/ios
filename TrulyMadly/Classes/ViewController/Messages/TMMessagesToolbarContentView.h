//
//  TMMessagesToolbarContentView.h
//  TrulyMadly
//
//  Created by Ankit on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMMessagesComposerTextView.h"
#import "TMQuizAnimatableButton.h"


@interface TMMessagesToolbarContentView : UIView

@property (strong, nonatomic, readonly) TMMessagesComposerTextView *textView;
@property (strong, nonatomic) UIButton *rightBarButtonItem;
@property (strong, nonatomic, readonly) UIView *rightBarButtonContainerView;
@property (strong, nonatomic, readonly) UIView *rightBarShareButtonContainerView;
@property (strong, nonatomic, readonly) UIView *curatedDealBarButotnContainerView;
@property (strong, nonatomic) UIButton *leftBarButtonItem;
@property (strong, nonatomic) UIButton<TMAnimatableButtonProtocol> *leftQuizBarButtonItem;
@property (strong, nonatomic) UIButton* rightShareBarButtonItem;
@property (strong, nonatomic, readonly) UIView *leftBarButtonContainerView;
@property (strong, nonatomic, readonly) UIView *leftBarQuizContainerView;


//custom constructor for common answer screen
- (instancetype) initWithCommonAnswerScreenFrame:(CGRect) frame;

@end

//
//  TMBlockUserTableViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 16/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMBlockUserTableViewCell : UITableViewCell

@property(nonatomic,strong)UILabel *blockLabel;
@property(nonatomic,strong)UITextField *blockReasonTf;

-(void)configureCellForTextMode:(BOOL)textMode;

@end

//
//  TMFeedbackViewController.m
//  TrulyMadly
//
//  Created by Ankit on 09/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMFeedbackViewController.h"
#import "TMMessagingController.h"
#import "TMMessageConfiguration.h"
#import "TMAPIConstants.h"
#import "TMBuildConfig.h"
#import "TMLog.h"
#import "TMMessage.h"
#import "TMMessageReceiver.h"
#import "NSObject+TMAdditions.h"
#import "TMChatContext.h"


@class TMChatUser;
@class TMChatSeenContext;


static void * kJSQMessagesKeyValueObservingContext = &kJSQMessagesKeyValueObservingContext;

@interface TMFeedbackViewController ()<TMMessagingControllerDelegate>

@property(nonatomic,strong)TMMessageConfiguration* messageConfiguration;

@end

@implementation TMFeedbackViewController

-(instancetype)init {
    self = [super init];
    if(self) {
       
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Feedback";
    
    self.showDeliveryStatus = FALSE;
    self.messageConfiguration = [[TMMessageConfiguration alloc] init];
    self.messageConfiguration.messageConversationFullLink = KAPI_FEEDBACK;
    self.messageConfiguration.matchUserId = ADMIN_USERID;

    TMChatContext *context = [[TMChatContext alloc] init];
    context.matchId = self.messageConfiguration.matchUserId;
    context.loggedInUserId = self.messageConfiguration.hostUserId;
    
    TMMessagingController *msgingClinet = [self messagingController];
    msgingClinet.delegate = self;
    [msgingClinet setupChatWithContext:context conversationURLString:self.messageConfiguration.messageConversationFullLink];
    
    //[self.inputToolbar setupActionButtonWithMediaButtonStatus:FALSE unreadQuizzesStatus:NO buttonTag:0];
    [self.inputToolbar setupSendButtonOnly];
    [self.inputToolbar setupActionButtonWithMediaButtonStatus:FALSE unreadQuizzesStatus:NO stickerBlooperStatus:NO buttonTag:0];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    self.title = @"Feedback";
    [super viewWillAppear:animated];
    TMMessagingController *msgingClinet = [self messagingController];
    [msgingClinet setChatSessionActive:TRUE];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self.keyboardController beginListeningForKeyboard];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self setEditing:false];
    [self.keyboardController endListeningForKeyboard];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    TMMessagingController *msgingClinet = [self messagingController];
    msgingClinet.delegate = nil;
    [msgingClinet resetChatContext];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //NSLog(@"MEMORY WARNING: %s", __PRETTY_FUNCTION__);
}

#pragma mark - MessageController Delegate
#pragma mark -

//-(void)didSendSticker:(NSString*)stickerUrl {
//    TMMessagingController *msgingClinet = [self messagingController];
//    TMMessage *message = [[TMMessage alloc] initWithText:stickerUrl
//                                                    type:MESSAGETTYPE_STICKER
//                                              receiverId:self.messageConfiguration.matchUserId];
//    message.isAdmin = TRUE;
//    [msgingClinet sendMessage:message];
//}

- (void) didSendSticker:(TMSticker*) sticker {
//        TMMessagingController *msgingClinet = [self messagingController];
//    TMMessage *message = [[TMMessage alloc] initWithSticker:sticker  text:sticker.highQualityImageURLString type:MESSAGETTYPE_STICKER receiverId:self.messageConfiguration.matchUserId];
//    message.isAdmin = TRUE;
//    [msgingClinet sendMessage:message];
}

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                      date:(NSDate *)date
{
    TMMessagingController *msgingClinet = [self messagingController];
    TMMessage *message = [[TMMessage alloc] initWithText:text
                                                    type:MESSAGETTYPE_TEXT
                                              receiverId:self.messageConfiguration.matchUserId
                                               sparkHash:nil];
    message.isAdmin = TRUE;
    [msgingClinet sendMessage:message];
}


-(void)didReceiveCachedMessages:(NSArray*)messages {
    if(messages.count) {
        @synchronized(self) {
            [self.conversations removeAllObjects];
            [self.shownDateIndexes removeAllObjects];
            self.lastShownDate = nil;
            [self.conversations addObjectsFromArray:messages];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            [self scrollToBottomAnimated:FALSE hideCollectionVewWhileScrolling:FALSE];
        });
    }
}
-(void)invalidateAndReloadCachedMessages:(NSArray*)messages {
    if(messages.count) {
        [self.conversations removeAllObjects];
        [self.shownDateIndexes removeAllObjects];
        self.lastShownDate = nil;
        [self.conversations addObjectsFromArray:messages];
        [self.collectionView reloadData];
        [self scrollToBottomAnimated:FALSE hideCollectionVewWhileScrolling:FALSE];
    }
}
-(void)didReceiveNewMesages:(NSArray*)messages {
    [self.conversations addObjectsFromArray:messages];
    [self inserMessage:messages];
}
-(void)willSendMessage:(TMMessage*)message {
    if(!message.isFailedMessageRetry) {
        [self finishSendingMessageAnimated:FALSE];
        [self.conversations addObject:message];
    }
    else if(message.isFailedMessageRetry) {
        message.deliveryStatus = SENDING;
    }
    [self inserMessage:@[(message)]];
}
-(void)willNotSendMessage:(TMMessage*)message withError:(TMError*)error {
    if(message.isFailedMessageRetry) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Can not send message.\nCheck your phone internet connection and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

-(void)didSendMessage:(TMMessage*)message {
    
}
-(void)message:(TMMessage*)message sendingFailedWithError:(TMError*)error {
    
}
-(void)didReceiveChatUserData:(TMChatUser*)chatUser {
    
}
-(void)didReceiveChatSeenData:(TMChatSeenContext*)chatSeenContext {
    
}
-(void)userBlockedWithStatus:(BOOL)status {
    
}
-(void)networkChangedWithStatus:(BOOL)status {

}

-(void)inserMessage:(NSArray*)messages {
    [self.collectionView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self scrollToBottomAnimated:TRUE hideCollectionVewWhileScrolling:FALSE];
    });
}
@end

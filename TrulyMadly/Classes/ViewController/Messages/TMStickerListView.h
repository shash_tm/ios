//
//  TMStickerListView.h
//  TrulyMadly
//
//  Created by Ankit on 11/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSticker.h"

@protocol TMStickerListViewDataSource <NSObject>

- (NSArray*) getFirstStickerGallery;

- (NSArray*) getAllStickerGalleries;

- (NSArray*) stickersForGalleryID:(NSUInteger) galleryID;

- (NSArray*) stickersForTimestampGallery;

- (UIImage*) getImageForSticker:(TMSticker*) sticker;

@end

@protocol TMStickerListViewDelegate <NSObject>

- (void) didSendSticker:(TMSticker*) sticker;
- (void) storeHDImageForSticker:(TMSticker* ) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock;
- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID;


@end

@interface TMStickerListView : UIView

@property(nonatomic,weak)id<TMStickerListViewDelegate> delegate;
@property(nonatomic,weak)id<TMStickerListViewDataSource> dataSource;

-(instancetype)initWithFrame:(CGRect)frame withCellSize:(CGSize)cellSize delegate:(id<TMStickerListViewDelegate>) delegate dataSource:(id<TMStickerListViewDataSource>) dataSource ;

- (void) reloadStickerGallery;

@end


//
//  TMMessageConversationViewController.m
//  TrulyMadly
//
//  Created by Ankit on 12/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageConversationViewController.h"
#import "TMMessagingController.h"
#import "TMMessageProfileViewController.h"
#import "TMBlockUserViewController.h"
#import "TMNavigationImageView.h"
#import "TMMessageReceiver.h"
#import "TMMessageConfiguration.h"
#import "TMMessage.h"
#import "TMLog.h"
#import "NSObject+TMAdditions.h"
#import "TMChatUser.h"
#import "TMChatContext.h"
#import "TMChatSeenContext.h"
#import "TMNoNetworkBar.h"
#import "TMUserSession.h"
#import "TMMessagingController.h"
#import "TMMessagesCollectionViewFlowLayoutInvalidationContex.h"
#import "TMNotificationHeaders.h"
#import "TMErrorMessages.h"
#import "NSString+TMAdditions.h"
#import "TMTimestampFormatter.h"
#import "TMMessageManager.h"
#import "TMCuratedDealsManager.h"
#import "TMDealListViewController.h"
#import "TMDealDetailViewController.h"
#import "TMCuratedDealContext.h"
#import "TMDealMessageContext.h"
#import "TMDataStore.h"
#import "TMDealTutorialViewController.h"
#import "TMToastView.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMLocationPreferenceViewController.h"
#import "TMImageDownloader.h"
#import "TMPhotoShareContext.h"
#import "TMFullImageViewController.h"
#import "TMSwiftHeader.h"
#import "UIImage+TMAdditions.h"
#import "TMShareEventContext.h"
#import "TMEventDetailViewController.h"
#import "TMSparkTrackInfo.h"


#define CLEARCHAT_ALERT_MESSAGE_TAG     101
#define UNMATCH_ALERT_MESSAGE_TAG       102
#define REPORTUSER_ALERT_MESSAGE_TAG    103
#define BLOCK_ALERT_MESSAGE_TAG         104
#define DELETESPARK_ALERT_TAG           105

@interface TMMessageConversationViewController ()<TMMessagingControllerDelegate,TMNavigationImageViewDeleagte,UIAlertViewDelegate,
                                                  TMBlockUserViewControllerDelegate,TMQuizControllerDelegateProtocol,
                                                  UIActionSheetDelegate,TMDealListViewControllerDelegate,
                                                  UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) TMNoNetworkBar *noNetworkBar;
@property(nonatomic,strong) TMNavigationImageView *navigationView;
@property(nonatomic,strong) TMChatUser *currentChatuser;
@property(nonatomic,strong) TMMessageProfileViewController *profileViewController;
@property(nonatomic,strong) TMMessageManager *messageManager;
@property(nonatomic,strong) TMDealTutorialViewController *dealTutorialViewCon;
@property(nonatomic,strong) UIImageView *doodleImageView;
@property(nonatomic,assign) BOOL isCollectionViewReloaded;
@property(nonatomic,assign) BOOL isDealButtonVisible;
@property(nonatomic,assign) BOOL isPhotShareButtonEnabled;
@property(nonatomic,assign) BOOL isPhotShareButtonEnabledToMsTm;
@property(nonatomic,assign) BOOL didViewDetailProfile;

@end

@implementation TMMessageConversationViewController


-(instancetype)initWithMessageConversation:(TMMessageConfiguration*)messageConfiuration {
    self = [super init];
    if(self) {
        
        self.showDeliveryStatus = TRUE;
        self.messageConfiuration = messageConfiuration;
        self.isSparkConvsersation = messageConfiuration.isSparkConversation;
        [self creareNavigationItem];
        
        TMChatContext *context = [[TMChatContext alloc] init];
        context.matchId = messageConfiuration.matchUserId;
        context.loggedInUserId = messageConfiuration.hostUserId;
        context.clearChatTs = messageConfiuration.clearChatTs;
        
        TMLOG(@"1.################");
        TMMessagingController *msgingClinet = [self messagingController];
        msgingClinet.delegate = self;
        [msgingClinet setupChatWithContext:context conversationURLString:messageConfiuration.messageConversationFullLink];
        
        self.quizController = [[TMQuizController alloc] initWithMatchName:messageConfiuration.fName matchID:[NSString stringWithFormat:@"%ld",(long)messageConfiuration.matchUserId]];
        self.quizController.delegate = self;
    }
    
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    TMLOG(@"2.1################");
    
    BOOL isNewStickerAvailable = ([TMDataStore containsObjectForKey:STICKER_BLOOPER_TO_BE_SHOWN])?([[TMDataStore retrieveObjectforKey:STICKER_BLOOPER_TO_BE_SHOWN] boolValue]):(NO);
    
    if(self.messageConfiuration.isMsTm) {
        self.isDealButtonVisible = FALSE;
        self.isPhotShareButtonEnabledToMsTm = [[self messagingController] canPhotoShareButtonToMsTm];
        [self.inputToolbar setupActionButtonForMsTmWithBlooperStatus:isNewStickerAvailable];
        if(self.isPhotShareButtonEnabledToMsTm) {
            [self.inputToolbar setupPhotoShareButton];
        }
    }
    else if(self.isSparkConvsersation) {
        self.isDealButtonVisible = FALSE;
        self.isPhotShareButtonEnabledToMsTm = FALSE;
        [self.inputToolbar setupActionButtonForMsTmWithBlooperStatus:isNewStickerAvailable];
        //[self addTopNavigationBarButtons];
    }
    else {
        
        [self.quizController makeQuizRequestForStatusAndFlare];
        
        self.isDealButtonVisible = [[self messagingController] canShowDealButton];
        self.isPhotShareButtonEnabled = [[self messagingController] canPhotoShareButton];
        BOOL isNewQuizAvailable = [self.quizController isNewQuizAvailable];
        BOOL isUpdatedQuizAvailable = [self.quizController isQuizStatusUpdatedForOppositeMatch];
        
        [self.inputToolbar setupActionButtonWithMediaButtonStatus:YES
                                              unreadQuizzesStatus:isNewQuizAvailable||isUpdatedQuizAvailable
                                             stickerBlooperStatus:isNewStickerAvailable
                                                        buttonTag:(int)self.messageConfiuration.matchUserId];
        if(self.isPhotShareButtonEnabled) {
            [self.inputToolbar setupPhotoShareButton];
        }
        
        [self addTopNavigationBarButtons];
        
        [self.quizController getAllFlareQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
    }
  
    [self scrollToBottomAnimated:false hideCollectionVewWhileScrolling:false];
    
    BOOL status = [[self messagingController] currentNetworkConnectionStatus];
    [self updateNetworkStatus:status];
    //
    BOOL showConnectingIndicator = [[self messagingController] showConnectingIndicator];
    if(showConnectingIndicator) {
        [self updateNavigationViewForConnectionStatus:showConnectingIndicator showConnectionText:TRUE];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(socketConnectiionInProgress:)
                                                 name:TMSOCKETCONNECTION_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(photoshareImagedownloadFailNotification:)
                                                 name:TMJPEGIMAGEDOWNLOADFAIL_NOTIFICATION
                                               object:nil];
    
    if(![[self messagingController] canConnect]) {
        [[self messagingController] postDisableSendActiontNotificationWithStatus:TRUE];
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.isDealButtonVisible) {
        BOOL isDealTutorialShown = [TMDataStore retrieveBoolforKey:@"com.deal.tuturialstatus"];
        if(!isDealTutorialShown) {
            [TMDataStore setBool:TRUE forKey:@"com.deal.tuturialstatus"];
            self.dealTutorialViewCon = [[TMDealTutorialViewController alloc] init];
            [self presentViewController:self.dealTutorialViewCon animated:NO completion:^{
                
            }];
        }
    }
    
    self.isBack = TRUE;
    TMMessagingController *msgingClinet = [self messagingController];
    [msgingClinet setChatSessionActive:TRUE];
    if(self.profileViewController) {
        self.profileViewController = nil;
    }
    if ([self.inputToolbar.contentView.leftQuizBarButtonItem respondsToSelector:@selector(animateButton)]) {
        [self.inputToolbar.contentView.leftQuizBarButtonItem animateButton];
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.keyboardController hideKeyboard];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (!self.presentedViewController) {
       //do it only when quiz is not presented
        if(self.isBack) {
            //[self setEditing:false];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            TMMessagingController *msgingClinet = [self messagingController];
            msgingClinet.delegate = nil;
            [msgingClinet resetChatContext];
            self.navigationItem.titleView = nil;
            [self.quizController setIsNudgeActionCancelled:YES];
            self.quizController = nil;
            self.delegate = nil;
        }
        else {
            TMMessagingController *msgingClinet = [self messagingController];
            [msgingClinet setChatSessionActive:FALSE];
        }
    }
    [self.keyboardController endListeningForKeyboard];
}
-(void)dealloc {
    TMLOG(@"TMMessageConversationViewController---dealloc:%@",self);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.delegate = nil;
    
    [self.conversations removeAllObjects];
    self.conversations = nil;
    
    if (self.presentedViewController) {
        //overlay screen is present
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    if(self.dealTutorialViewCon) {
        self.dealTutorialViewCon = nil;
    }
    if(self.quizController) {
        [self.quizController setIsNudgeActionCancelled:YES];
        self.quizController = nil;
    }
    
    self.profileViewController = nil;
}

#pragma mark - Getter
#pragma mark -
-(TMMessageManager*)messageManager {
    if(!_messageManager) {
        _messageManager = [[TMMessageManager alloc] init];
    }
    return _messageManager;
}
-(UIButton*)curatedDealsOptionButtonWithAplpha:(CGFloat)alpha
{
    UIButton *dealButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    //dealButton.tag = 45001;
    [dealButton addTarget:self action:@selector(showCuratedDeal) forControlEvents:UIControlEventTouchUpInside];
    dealButton.backgroundColor = [UIColor clearColor];
    dealButton.alpha = alpha;
    
    UIImageView* sendButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 4, 40, 31)];
    [dealButton addSubview:sendButtonImageView];
    sendButtonImageView.image = [UIImage imageNamed:@"dateliciousicon"];
    
    return dealButton;
}

-(void)setupDoodleImageView {
    if(!self.doodleImageView) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat yPos = (CGRectGetMaxY(self.view.bounds) - CGRectGetHeight(self.inputToolbar.frame) - width)/2;
        self.doodleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,yPos,width, width)];
        self.doodleImageView.clipsToBounds = TRUE;
        self.doodleImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.collectionView addSubview:_doodleImageView];
        self.doodleImageView.backgroundColor = [UIColor clearColor];
    }
}

#pragma mark - View Handlers
#pragma mark -

-(void)reloadMessageCollectionView {
    if(self.doodleImageView) {
        [self.doodleImageView removeFromSuperview];
        self.doodleImageView.image = nil;
        self.doodleImageView = nil;
    }
    [self.collectionView reloadData];
}
#pragma mark - Navigation Button Handler
#pragma mark -
-(void)creareNavigationItem {
    self.navigationView = [[TMNavigationImageView alloc] initWithFrame:CGRectZero];
    self.navigationView.delegate = self;
    self.navigationItem.titleView = self.navigationView;
    if(self.messageConfiuration.fName) {
        [self.navigationView setTitleText:self.messageConfiuration.fName withImageUrl:self.messageConfiuration.profileImageURLString];
    }
}
-(void)addNavigationTitleView {
    if(self.currentChatuser.fName) {
        [self.navigationView setTitleText:self.currentChatuser.fName withImageUrl:self.currentChatuser.profileImageURLString];
    }
}
-(void)addTopNavigationBarButtons {
    NSArray *barButtons= nil;
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]
                                       initWithImage:[UIImage imageNamed:@"3dots"] style:UIBarButtonItemStylePlain target:self action:@selector(blockProfileAction)];
    if(self.isDealButtonVisible) {
        CGFloat dealButtonAlpha = (self.messageConfiuration.dealState == DealStateDisabled) ? 0.4 : 1;
        UIButton *dealButton = [self curatedDealsOptionButtonWithAplpha:dealButtonAlpha];
        UIBarButtonItem *dealBarButton = [[UIBarButtonItem alloc] initWithCustomView:dealButton];
        barButtons = @[rightBarButton,dealBarButton];
    }
    else {
        barButtons = @[rightBarButton];
    }
    
    self.navigationItem.rightBarButtonItems = barButtons;
}

#pragma mark - MessageController Delegate
#pragma mark -
-(void)willMakeFirstPollingRequest {
    if(self.messageConfiuration.isConversationHasUnreadMessage) {
        [self updateNavigationViewForConnectionStatus:TRUE showConnectionText:FALSE];
    }
}
-(void)didCompleteFirstPollingRequest {
    if(self.messageConfiuration.isConversationHasUnreadMessage) {
        self.messageConfiuration.isConversationHasUnreadMessage = FALSE;
        [self updateNavigationViewForConnectionStatus:FALSE showConnectionText:FALSE];
    }
}
-(void)didReceiveCachedMessages:(NSArray*)messages {
    if(messages.count) {
        [self.conversations removeAllObjects];
        [self.shownDateIndexes removeAllObjects];
        self.lastShownDate = nil;
        [self.trackDict removeAllObjects];
        [self.conversations addObjectsFromArray:messages];
        [self reloadMessageCollectionView];
        [self scrollToBottomAnimated:FALSE hideCollectionVewWhileScrolling:FALSE];
    }
}
-(void)invalidateAndReloadCachedMessages:(NSArray*)messages {
    if(messages.count) {
        [self.conversations removeAllObjects];
        [self.shownDateIndexes removeAllObjects];
        self.lastShownDate = nil;
        [self.trackDict removeAllObjects];
        [self.conversations addObjectsFromArray:messages];
        [self reloadMessageCollectionView];
        [self scrollToBottomAnimated:FALSE hideCollectionVewWhileScrolling:FALSE];
    }
}
-(void)didReceiveNewMesages:(NSArray*)messages {
    [self.conversations addObjectsFromArray:messages];
    [self inserMessage:messages];
}
-(void)willSendMessage:(TMMessage*)message {
    TMLOG(@"willSendMessage");
    if(!message.isFailedMessageRetry) {
        [self finishSendingMessageAnimated:FALSE];
        [self.conversations addObject:message];
        [self.trackDict setObject:message forKey:message.randomId];
        [self inserMessage:@[(message)]];
    }
    else if(message.isFailedMessageRetry) {
        message.deliveryStatus = SENDING;
        [self.trackDict setObject:message forKey:message.randomId];
        [self reloadMessageCollectionView];
    }
}
-(void)willNotSendMessage:(TMMessage*)message withError:(TMError*)error {
    if(message.isFailedMessageRetry) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Can not send message.\nCheck your phone internet connection and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
-(void)didSendMessage:(TMMessage*)message {
    if(message.isSparkAcceptanceMessage) {
        [self tapAction];
        self.isSparkConvsersation = FALSE;
        self.messageConfiuration.isSparkConversation = FALSE;
        self.messageConfiuration.sparkHash = nil;
        [self.collectionView setContentOffset:CGPointZero animated:NO];
        BOOL isNewQuizAvailable = [self.quizController isNewQuizAvailable];
        BOOL isUpdatedQuizAvailable = [self.quizController isQuizStatusUpdatedForOppositeMatch];
        BOOL isNewStickerAvailable = ([TMDataStore containsObjectForKey:STICKER_BLOOPER_TO_BE_SHOWN])?([[TMDataStore retrieveObjectforKey:STICKER_BLOOPER_TO_BE_SHOWN] boolValue]):(NO);
        self.isPhotShareButtonEnabled = [[self messagingController] canPhotoShareButton];
        [self.inputToolbar removeFromSuperview];
        self.inputToolbar = nil;
        [self setupInputToolbar];
        [self.inputToolbar setupActionButtonWithMediaButtonStatus:YES
                                              unreadQuizzesStatus:isNewQuizAvailable||isUpdatedQuizAvailable
                                             stickerBlooperStatus:isNewStickerAvailable
                                                        buttonTag:(int)self.messageConfiuration.matchUserId];
        if(self.isPhotShareButtonEnabled) {
            [self.inputToolbar setupPhotoShareButton];
        }
        [self addTopNavigationBarButtons];
    }
    
    [self updateMessage:message];
    
    if(message.type == MESSAGETTYPE_CD_VOUCHER) {
        NSString *message = ([[self messagingController] didNumberSave]) ? @"Voucher code has been sent to you via SMS.\nEnjoy your date!"
                                                                         : @"Tap 'View ​Voucher​'​ to enter your phone number to receive the voucher via SMS";
        [TMToastView showToastInParentView:self.view withText:message withDuaration:2.5 presentationDirection:TMToastPresentationFromBottom];
    }
}
-(void)message:(TMMessage*)message sendingFailedWithError:(TMError*)error {
    [self updateMessage:message];
}
-(void)didReceiveChatUserData:(TMChatUser*)chatUser {
    if(self.currentChatuser.doodleLink) {
        chatUser.doodleLink = self.currentChatuser.doodleLink;
    }
    self.currentChatuser = chatUser;
    [self addNavigationTitleView];
    if(self.profileViewController && (!self.profileViewController.profileURLString)) {
        [self.profileViewController setProfileURLString:self.currentChatuser.profileLinkURLString];
    }
    if((!self.conversations.count) && (!self.doodleImageView) && (!self.isSparkConvsersation)) {
        //show doodle
        [[self messagingController] fetchAndCacheDoodleLinkForUser:chatUser];
    }
}
-(void)didReceiveChatSeenData:(TMChatSeenContext*)chatSeenContext {
    TMLOG(@"Last MsgTs:%@ chatseencontext:%@",self.lastSeenMsgTs,chatSeenContext.lastSeenMessageTimestamp);
    if(self.lastSeenMsgTs && (![self.lastSeenMsgTs isEqualToString:chatSeenContext.lastSeenMessageTimestamp])) {
        self.lastSeenMsgTs = chatSeenContext.lastSeenMessageTimestamp;
        TMLOG(@"1. Read Time:%@",self.lastSeenMsgTs);
        [self reloadMessageCollectionView];
    }
    else if(self.lastSeenMsgTs == nil && [chatSeenContext.lastSeenMessageId isValidObject]){
        BOOL status = TRUE;
        if([chatSeenContext.lastSeenMessageId isNStringObject] &&
           ([chatSeenContext.lastSeenMessageId isEqualToString:@"null"] || [chatSeenContext.lastSeenMessageId isEqualToString:@"<null>"])) {
            status = FALSE;
        }

        if(status) {
            self.lastSeenMsgTs = chatSeenContext.lastSeenMessageTimestamp;
            TMLOG(@"2. Read Time:%@",self.lastSeenMsgTs);
            [self reloadMessageCollectionView];
        }
    }
}
-(void)messageSendingFailedWithBadWordsResponse:(TMMessage*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message.text delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    });
}

//called when user sends message and server retruns response as blocked user
-(void)didReceiveBlockUserResponse {
    /////
    TMChatContext *context = [[TMChatContext alloc] init];
    context.matchId = self.messageConfiuration.matchUserId;
    context.loggedInUserId = self.messageConfiuration.hostUserId;
    [[TMMessagingController sharedController] markConversationAsBlockedAndShown:context];
    
    //////
    [self.messageDelegate blockCurrentConversation];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:EM_BLOCK_ALERT_MSG message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        alertView.tag = BLOCK_ALERT_MESSAGE_TAG;
        [alertView show];
    });
}

//called when user report match
-(void)userBlockedWithStatus:(BOOL)status {
    UINavigationController *presentedViewCon = (UINavigationController*)self.presentedViewController;
    if(status) {
        ///////
        TMChatContext *context = [[TMChatContext alloc] init];
        context.matchId = self.messageConfiuration.matchUserId;
        context.loggedInUserId = self.messageConfiuration.hostUserId;
        [[TMMessagingController sharedController] markConversationAsBlockedAndShown:context];
        
        ////////////
        [self.messageDelegate blockCurrentConversation];
        dispatch_async(dispatch_get_main_queue(), ^{
            [presentedViewCon dismissViewControllerAnimated:false completion:^{
                [self.navigationController popViewControllerAnimated:false];
            }];
        });
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [presentedViewCon dismissViewControllerAnimated:false completion:^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Problem in blocking user" message:@"Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }];
        });
    }
}
-(void)didReceiveDoodleLink:(NSString*)doodleLink {
    if(self.conversations.count == 0) {
        [self setupDoodleImageView];
        [self.doodleImageView setImageWithURL:[NSURL URLWithString:doodleLink]];
        self.currentChatuser.doodleLink = doodleLink;
    }
}
-(void)networkChangedWithStatus:(BOOL)status {
    [self updateNetworkStatus:status];
    if (!status) {
        //hide the typing indicator
        [self hideTypingIndicatorDueToIncomingMessageOrNoConnectionStatus];
    }
}
-(void)inserMessage:(NSArray*)messages {
    //remove typing indicator only if the sender of any message is the match
    BOOL isTypingIndicatorRemovalRequired = NO;
    
    for (int index = 0; index < messages.count && !isTypingIndicatorRemovalRequired; index++) {
        if ([[messages objectAtIndex:index] isKindOfClass:[TMMessage class]]) {
            TMMessage* message = [messages objectAtIndex:index];
            if (message.senderType == MESSAGESENDER_MATCH) {
                isTypingIndicatorRemovalRequired = YES;
            }
        }
    }
    
    if (isTypingIndicatorRemovalRequired) {
        [self hideTypingIndicatorDueToIncomingMessageOrNoConnectionStatus];
    }
    [self reloadMessageCollectionView];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self scrollToBottomAnimated:TRUE hideCollectionVewWhileScrolling:FALSE];
    });
}
-(void)updateMessage:(TMMessage*)message {
    TMMessage *trackedMessage = self.trackDict[message.randomId];
    if((trackedMessage != nil) && (message.randomId != nil)) {
        TMLOG(@"TrckDict:%@ Not Avaialble ReceivedMessage:%@",self.trackDict,message);
        [trackedMessage updateStateFromSentMessageResponse:message];
        if((message.type == MESSAGETTYPE_PHOTO) && (message.deliveryStatus != SENDING)) {
            [trackedMessage removePhotoUploadProgress];
        }
        [self.trackDict removeObjectForKey:message.randomId];
        [self reloadMessageCollectionView];
    }
    else {
        TMLOG(@"Updated Message Not Available");
        TMLOG(@"TrckDict:%@ Not Avaialble ReceivedMessage:%@",self.trackDict,message);
        NSDictionary *userInfo = @{ @"msg": message };
        [[NSNotificationCenter defaultCenter] postNotificationName:message.uid object:nil userInfo:userInfo];
        
        [self reloadMessageCollectionView];
    }
}
-(void)updateNetworkStatus:(BOOL)status {
    if(status == FALSE) {
        if(!self.noNetworkBar) {
            TMLOG(@"No Network....");
            self.noNetworkBar = [[TMNoNetworkBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 0)];
            [self.view addSubview:self.noNetworkBar];
        }
        [self.noNetworkBar showWithWidth:self.view.frame.size.width];
        
        //hide connecting
        [self updateNavigationViewForConnectionStatus:FALSE showConnectionText:TRUE];
        [[self messagingController] setShowConnectingIndicator:FALSE];
    }
    else {
        [self.noNetworkBar hide];
        [self.noNetworkBar removeFromSuperview];
        self.noNetworkBar = nil;
    }
}
-(void)didReceiveQuizStatusUpdate:(NSString*)matchId {
    [self.quizController makeQuizRequestForStatusAndFlare];
}
-(void)didReceiveDealStateUpdate:(TMDealState)state {
    if(self.isDealButtonVisible) {
        if(self.messageConfiuration.dealState == DealStateDisabled) {
            if([self.messageDelegate respondsToSelector:@selector(reloadCachedConversation)]) {
                [self.messageDelegate reloadCachedConversation];
            }
        }
        
        self.messageConfiuration.dealState = state;
        [self addTopNavigationBarButtons];
    }
}
-(void)willStartPhotoUploadForMessage:(TMMessage*)message {
    TMMessage *trackedMessage = self.trackDict[message.randomId];
    if((trackedMessage != nil) && (message.randomId != nil)) {
        [message showPhotoUploadProgress];
    }
}
-(void)didCompletePhotoUploadForMessage:(TMMessage*)message {
    TMMessage *trackedMessage = self.trackDict[message.randomId];
    if((trackedMessage != nil) && (message.randomId != nil)) {
        TMLOG(@"TrckDict:%@ Not Avaialble ReceivedMessage:%@",self.trackDict,message);
        [trackedMessage updateMetadataFromDictioanary:message.metadataDictionary];
        //trackedMessage.metadataDictionary = message.metadataDictionary;
        [trackedMessage removePhotoUploadProgress];
        //[self.trackDict removeObjectForKey:message.randomId];
    }
//    else {
//        TMLOG(@"Updated Message Not Available");
//        TMLOG(@"TrckDict:%@ Not Avaialble ReceivedMessage:%@",self.trackDict,message);
//        NSDictionary *userInfo = @{ @"msg": message };
//        [[NSNotificationCenter defaultCenter] postNotificationName:message.uid object:nil userInfo:userInfo];
//        
//        [self.collectionView reloadData];
//    }
//
    //[message removePhotoUploadProgress];
}
-(void)didFailPhotoUploadForMessage:(TMMessage*)message {
    TMMessage *trackedMessage = self.trackDict[message.randomId];
    TMLOG(@"TrckDict:%@ Not Avaialble ReceivedMessage:%@",self.trackDict,message);
    if((trackedMessage != nil) && (message.randomId != nil)) {
        [trackedMessage removePhotoUploadProgress];
    }
}

#pragma mark - TMMessageViewController Overridden Methods
#pragma mark -
- (void) didSendSticker:(TMSticker*) sticker {
    TMMessagingController *msgingClinet = [self messagingController];
    TMMessage *message = [[TMMessage alloc] initWithSticker:sticker
                                                       text:sticker.highQualityImageURLString
                                                 receiverId:self.messageConfiuration.matchUserId
                                                  sparkHash:self.messageConfiuration.sparkHash];
    message.fullConversationLink = self.messageConfiuration.messageConversationFullLink;
    [msgingClinet sendMessage:message];
    if(self.messageConfiuration.isSparkConversation) {
        [self trackSparkAcceptanceMessageStatus:message];
    }
}
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                      date:(NSDate *)date
{
    TMMessagingController *msgingClinet = [self messagingController];
    TMMessage *message = [[TMMessage alloc] initWithText:text
                                                    type:MESSAGETTYPE_TEXT
                                              receiverId:self.messageConfiuration.matchUserId
                                               sparkHash:self.messageConfiuration.sparkHash];
    message.fullConversationLink = self.messageConfiuration.messageConversationFullLink;
    [msgingClinet sendMessage:message];
    
    if(!self.messageConfiuration.isMsTm && self.isPhotShareButtonEnabled){
        [self.inputToolbar animatePhotoShareButtonForMatchId:[NSString stringWithFormat:@"%ld",(long)self.messageConfiuration.matchUserId]];
    }
    if(self.messageConfiuration.isSparkConversation) {
        [self trackSparkAcceptanceMessageStatus:message];
    }
}
-(void)showCuratedDeal {
    if(self.messageConfiuration.dealState != DealStateDisabled) {
        if(self.messageConfiuration.dealState == DealStateEnabled) {
            TMDealListViewController* dealListViewController = [[TMDealListViewController alloc] initWithMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
            dealListViewController.dealListViewControllerDelegate = self;
            UINavigationController* dealNavigationController = [[UINavigationController alloc] initWithRootViewController:dealListViewController];
            TMLocationPreferenceViewController *locView = [[TMLocationPreferenceViewController alloc] initWithGeo:false];
            ECSlidingViewController *viewCon = [[ECSlidingViewController alloc] initWithTopViewController:dealNavigationController];
            viewCon.underRightViewController = locView;
            viewCon.anchorLeftRevealAmount = self.view.bounds.size.width-60;
            [self presentViewController:viewCon animated:YES completion:nil];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Datelicious - Not Available" message:@"Oops! Datelicious is not available in your match’s city yet. Check back later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}
-(void)showPhotoSharingOptions {
    [self tapAction];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library",@"Camera", nil];
    actionSheet.tag = PHOTOSHARING_ACTIONSHEET_TAG;
    [actionSheet showInView:self.view];
    ///track icon click
    [self trackPhotoShareButtonAction];
}
-(void)handleTapActionOnCuratedDealMessage:(TMMessage*)messageItem {
    TMCuratedDealContext *context = [[TMCuratedDealContext alloc] init];
    context.dateSpotId = messageItem.datespotId;
    context.dealId = messageItem.dealId;
    context.fromChat = true;
    context.matchId = [NSString stringWithFormat:@"%ld",(long)self.messageConfiuration.matchUserId];
    TMDealDetailViewController *dealDetailViewCon = [[TMDealDetailViewController alloc] initWithDealContext:context];
    dealDetailViewCon.dealDetailViewControllerDelegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:dealDetailViewCon];
    [self presentViewController:navController animated:YES completion:nil];
}
-(void)handleTapActionOnPhotoShareMessage:(TMMessage*)messageItem {
    TMPhotoShareContext *photoShareContext = messageItem.photoshareContext;
    if(messageItem.senderType == MESSAGESENDER_ME) {
        UIImage *image = [photoShareContext getCachedSentImage];
        if(image) {
            if(messageItem.deliveryStatus == SENDING) {
                //mark message as sent failed and cancel image upload
                TMLOG(@"Cancel Photo Upload");
                [messageItem removePhotoUploadProgress];
                TMMessagingController *msgingClinet = [self messagingController];
                [msgingClinet cancelPhotoUploadMessage:messageItem];
            }
            else if((messageItem.deliveryStatus == SENT) || messageItem.deliveryStatus == SENDING_FAILED) {
                self.isBack = FALSE;
                TMFullImageViewController *vc = [[TMFullImageViewController alloc] init];
                [self.navigationController pushViewController:vc animated:TRUE];
                [vc setFullSharedImage:image];
            }
        }
    }
    else { //incoming photo message
        UIImage *image = [photoShareContext getCachedJpegImage];
        if(image) {
            self.isBack = FALSE;
            TMFullImageViewController *vc = [[TMFullImageViewController alloc] init];
            [self.navigationController pushViewController:vc animated:TRUE];
            [vc setFullSharedImage:image];
        }
        else { //if thumbnail image is shown
            if(photoShareContext.downloadInProgress) { //check if image is downloading then cancel image download
                
            }
            else { //start image download
                if([self.messagingController canConnect]) {
                    [messageItem downloadFullImage];
                }
            }
        }
    }
}
-(void)handleTapActionOnImageLinkMessage:(TMMessage*)messageItem {
    self.isBack = FALSE;
    TMWebViewController *webviewController = [[TMWebViewController alloc] init];
    [self.navigationController pushViewController:webviewController animated:TRUE];
    [webviewController loadURL:[messageItem imageLinkLandingURLString]];
}
-(void)handleTapActionOnShareEventMessage:(TMMessage*)messageItem {
    NSString *event_id = messageItem.shareEventContext.eventId;
    if(event_id) {
        self.isBack = FALSE;
        TMEventDetailViewController *detailCon = [[TMEventDetailViewController alloc] initWithEventId:event_id];
        NSArray *viewCon = self.navigationController.viewControllers;
        NSMutableArray *newVC = [[NSMutableArray alloc] init];
        if(viewCon.count>2) {
            if([viewCon[viewCon.count-2] isKindOfClass:[TMEventDetailViewController class]]) {
                for (int i=0; i<viewCon.count-2; i++) {
                    [newVC addObject:[viewCon objectAtIndex:i]];
                }
                [newVC addObject:detailCon];
                [self.navigationController setViewControllers:newVC animated:true];
            }
            else {
                [self.navigationController pushViewController:detailCon animated:true];
            }
        }else {
            [self.navigationController pushViewController:detailCon animated:true];
        }
    }
}

-(void)setupSendActionButtonForTextLength:(NSInteger)textLength {
    BOOL showPhotoShareButton = FALSE;
    if(self.messageConfiuration.isMsTm && self.isPhotShareButtonEnabledToMsTm) {
        showPhotoShareButton = TRUE;
    }
    else if(self.isPhotShareButtonEnabled) {
        showPhotoShareButton = TRUE;
    }
    
    if(textLength > 0) {
        
        BOOL isNewStickerAvailable = ([TMDataStore containsObjectForKey:STICKER_BLOOPER_TO_BE_SHOWN])?([[TMDataStore retrieveObjectforKey:STICKER_BLOOPER_TO_BE_SHOWN] boolValue]):(NO);
        
        [self.inputToolbar setupSendButtonWithBlooperStatus:isNewStickerAvailable];
    }
    else {
        if(showPhotoShareButton) {
            [self.inputToolbar setupPhotoShareButton];
        }
    }
}

#pragma mark - Other Delegate Handler
#pragma mark -
-(void)didTapToNavigationView {
    self.isBack = FALSE;
    self.profileViewController = [[TMMessageProfileViewController alloc] init];
    self.profileViewController.matchId = self.messageConfiuration.matchUserId;
    self.profileViewController.actionDelegate = self.actionDelegate;
    [self.navigationController pushViewController:self.profileViewController animated:NO];
    if(![self.currentChatuser.profileLinkURLString isNULLString]) {
        self.profileViewController.profileURLString = self.currentChatuser.profileLinkURLString;
    }
    if(self.messageConfiuration.isSparkConversation) {
        self.didViewDetailProfile = TRUE;
        [self trackSparkProfileViewStatusFromSource:@"bar"];
    }
}
-(NSString*)getUserName {
    return [TMUserSession sharedInstance].user.fName;
}

- (NSString*)getCurrentMatchName {
    return self.messageConfiuration.fName;
}

-(NSString*)getUserProfilePic {
    if ([TMUserSession sharedInstance].user) {
        return [TMUserSession sharedInstance].user.profilePicURL;
    }
    return nil;
}
-(NSString*)getCurrentMatchProfilePic {
    return self.messageConfiuration.profileImageURLString;
}

#pragma mark - Block User Action Handlers
#pragma mark -
-(void)blockProfileAction {
    [self tapAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Clear Chat",@"Unmatch",@"Report User", nil];
        actionSheet.tag = MORE_OPTION_ACTIONSHEET_TAG;
        [actionSheet showInView:self.view];

    });
}

#pragma mark - BlockViewController Delegate
#pragma mark -
-(void)blockUserWithReason:(NSString*)reasonText {
    TMMessagingController *msgingClinet = [self messagingController];
    [msgingClinet blockUserWithReason:reasonText];
}

#pragma mark - TMMessageCollectionViewHeaderDelegate Delegate
#pragma mark -

-(void)didTapSparkUserProfile {
    self.isBack = FALSE;
    self.self.didViewDetailProfile = TRUE;
    self.profileViewController = [[TMMessageProfileViewController alloc] init];
    self.profileViewController.matchId = self.messageConfiuration.matchUserId;
    self.profileViewController.actionDelegate = self.actionDelegate;
    [self.navigationController pushViewController:self.profileViewController animated:YES];
    if(![self.currentChatuser.profileLinkURLString isNULLString]) {
        self.profileViewController.profileURLString = self.currentChatuser.profileLinkURLString;
    }
    ////
    [self trackSparkProfileViewStatusFromSource:@"card"];
}
-(void)deleteSparkAction {
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to delete this spark?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertview.tag = DELETESPARK_ALERT_TAG;
    [alertview show];
    [self trackSparkProfileDeleteStatus:@"delete_clicked"];
}

#pragma mark - TMQuizControllerDelegateProtocol methods
#pragma mark -
- (void)handleLogoutFromQuiz {
    if (self.presentedViewController) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
           
            [NSThread sleepForTimeInterval:1.5];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:nil];
            });
        });
    }
    
    [self.actionDelegate setNavigationFlowForLogoutAction:false];
}

#pragma mark - TMDealListViewControllerDelegate handler
#pragma mark -
- (void)didClickForRequestForDateSpot:(TMDealMessageContext*)dealMessageContext {
    TMMessagingController *msgingClinet = [self messagingController];
    TMMessage *message = [[TMMessage alloc] initWithDealMessageContext:dealMessageContext
                                                       withMessageType:MESSAGETTYPE_CD_ASK
                                                        withReceiverId:self.messageConfiuration.matchUserId];
    message.fullConversationLink = self.messageConfiuration.messageConversationFullLink;
    [msgingClinet sendMessage:message];
    
    UIViewController *presentedViewController = [self presentedViewController];
    [presentedViewController dismissViewControllerAnimated:FALSE completion:^{
        
    }];
}
- (void)didClickLetsGoForDateSpot:(TMDealMessageContext*)dealMessageContext {
    TMMessagingController *msgingClinet = [self messagingController];
    TMMessage *message = [[TMMessage alloc] initWithDealMessageContext:dealMessageContext
                                             withMessageType:MESSAGETTYPE_CD_VOUCHER
                                              withReceiverId:self.messageConfiuration.matchUserId];
    
    message.fullConversationLink = self.messageConfiuration.messageConversationFullLink;
    [msgingClinet sendMessage:message];
    
    UIViewController *presentedViewController = [self presentedViewController];
    [presentedViewController dismissViewControllerAnimated:FALSE completion:^{
        
    }];
}

#pragma mark - UIAlertView Delegate
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == CLEARCHAT_ALERT_MESSAGE_TAG) {
        if(buttonIndex == 1) {
            [self clearChatConversation];
            [self trackClearChatButtonAction];
        }
    }
    else if(alertView.tag == UNMATCH_ALERT_MESSAGE_TAG) {
        if(buttonIndex == 1) {
            [self unmatchConversation];
        }
    }
    else if(alertView.tag == REPORTUSER_ALERT_MESSAGE_TAG) {
        if(buttonIndex == 1) {
            self.isBack = FALSE;
            TMBlockUserViewController *vc = [[TMBlockUserViewController alloc] initWithBlockReason:nil blockFlags:nil];
            vc.loadingText = @"Blocking User...";
            vc.delegate = self;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
            [self presentViewController:navController animated:YES completion:nil];
        }
    }
    else if(alertView.tag == BLOCK_ALERT_MESSAGE_TAG) {
        [self.navigationController popViewControllerAnimated:false];
    }
    else if(alertView.tag == DELETESPARK_ALERT_TAG) {
        if(buttonIndex==1) {
            [self trackSparkProfileDeleteStatus:@"delete_confirmed"];
            [self.messageDelegate deleteSparkWithMatch:self.messageConfiuration];
        }
    }
}

#pragma mark - ActionSheet Delegate
#pragma mark -
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex  {
    if(actionSheet.tag == MORE_OPTION_ACTIONSHEET_TAG) {
        if(buttonIndex == 0) { //clear chat
            if(self.conversations.count) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Clear Chat" message:@"Are you sure you want to clear the chat?"
                                                                       delegate:self
                                                              cancelButtonTitle:@"Cancel"
                                                              otherButtonTitles:@"Ok", nil];
                    alertView.tag = CLEARCHAT_ALERT_MESSAGE_TAG;
                    [alertView show];
                });
            }
        }
        else if(buttonIndex == 1) { //unmatch
            NSString *genderText = ([[TMUserSession sharedInstance].user isUserFemale]) ? @"him" : @"her";
            NSString *message = [NSString stringWithFormat:@"This will clear all chat history and remove %@ permanently from your matches",genderText];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Unmatch %@",self.messageConfiuration.fName] message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            alertView.tag = UNMATCH_ALERT_MESSAGE_TAG;
            [alertView show];
        }
        else if(buttonIndex == 2) { //report user
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to block the user" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            alertView.tag = REPORTUSER_ALERT_MESSAGE_TAG;
            [alertView show];
        }
    }
    else if(actionSheet.tag == FAILED_MESSAGE_ACTIONSHEET_TAG) {
        [self handleFailedMessageActionSheetActionForButtonIndex:buttonIndex];
    }
    else if(actionSheet.tag == PHOTOSHARING_ACTIONSHEET_TAG) {
        [self handlePhotoSharingActionSheetActionForButtonIndex:buttonIndex];
    }

}
-(void)handlePhotoSharingActionSheetActionForButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) { //Gallery
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    else if(buttonIndex == 1) { //camera
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    }
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = sourceType;
        imagePickerController.delegate = self;
        
        if (sourceType == UIImagePickerControllerSourceTypeCamera) {
            imagePickerController.showsCameraControls = YES;
        }
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    });
}

#pragma mark - UIImagePickerControllerDelegate
#pragma mark -

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *imageSource = nil;
    if(UIImagePickerControllerSourceTypeCamera == picker.sourceType) {
        imageSource = @"Camera";
    }
    else {
        imageSource = @"Gallery";
    }
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    UIImage *image1 = [image fixOrientation];
    //NSLog(@"Image Orientation:%ld",(long)image.imageOrientation);
    NSData *tmpImg = UIImageJPEGRepresentation(image1, 0.4);
    //NSLog(@"Original File size is : %.2f KB",(float)tmpImg.length/1024.0f);
    //NSLog(@"Original File Width is : %f",image1.size.width);
    //NSLog(@"Original File Height is : %f",image1.size.height);
    
    ////////////////
    TMMessage *message = [[TMMessage alloc] initPhotoMessageWithImageData:tmpImg
                                                               receiverId:self.messageConfiuration.matchUserId
                                                              imageSource:imageSource];
    
    TMMessagingController *msgingClinet = [self messagingController];
    message.fullConversationLink = self.messageConfiuration.messageConversationFullLink;
    [msgingClinet sendMessage:message];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - ClearChat Handlers
#pragma mark -
-(void)configureViewForDataLoadingStartWithText:(NSString*)text {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:text];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
}

-(void)unmatchConversation {
    if([self.messageManager isNetworkReachable]) {
        [self.messageManager blockUserWithReason:@"Not interested anymore" withURL:self.messageConfiuration.messageConversationFullLink
                                        response:^(BOOL status) {
                                            
                                            if(status) {
                                                TMChatContext *context = [[TMChatContext alloc] init];
                                                context.matchId = self.messageConfiuration.matchUserId;
                                                context.loggedInUserId = self.messageConfiuration.hostUserId;
                                                [[TMMessagingController sharedController] markConversationAsBlockedAndShown:context];
                                            }
        }];
        
        [self.messageDelegate willUnmatchCurrentConversation];
        [self.navigationController popViewControllerAnimated:false];
        
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Cannot Unmatch %@",self.messageConfiuration.fName] message:TM_INTERNET_NOTAVAILABLE_MSG delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        });
    }
}

-(void)clearChatConversation {
    if([self.messageManager isNetworkReachable]) {
        [self configureViewForDataLoadingStartWithText:@"Clearing Chat..."];
        TMMessage *lastMessageInConversation = [self.conversations objectAtIndex:self.conversations.count-1];
        [[self messagingController] clearChatTillMessage:lastMessageInConversation
                                             withURLString:self.messageConfiuration.messageConversationFullLink
                                                  response:^(BOOL status) {
                                                      
            [self configureViewForDataLoadingFinish];
            if(status) {
                [self.conversations removeAllObjects];
                [self reloadMessageCollectionView];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:CLEARCHAT_ERROR_TITLE]
                                                                        message:PLEASETRYLATER_ERROR_MSG
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertView show];
                });
            }
        }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Cannot Clear Chat"]
                                                                message:TM_INTERNET_NOTAVAILABLE_MSG
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        });
    }
}

#pragma mark - Notification Handlers
#pragma mark -
-(void)socketConnectiionInProgress:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    BOOL status = [[userInfo objectForKey:@"connectionstatus"] boolValue];
    [self updateNavigationViewForConnectionStatus:status showConnectionText:TRUE];
}
-(void)photoshareImagedownloadFailNotification:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSInteger matchId = [userInfo[@"matchid"] integerValue];
    if(self.messageConfiuration.matchUserId == matchId) {
        NSString *textMessage = userInfo[@"msg"];
        [TMToastView showToastInParentView:self.view withText:textMessage withDuaration:2.5 presentationDirection:TMToastPresentationFromBottom];
    }
}

-(void)updateNavigationViewForConnectionStatus:(BOOL)connectionStatus showConnectionText:(BOOL)showTextStatus {
    if(connectionStatus) {
        [self.navigationView setViewForConnectionModeWithText:showTextStatus];
    }
    else {
        [self.navigationView resetViewForConnectionMode];
    }
}

#pragma mark - Tracking Handlers
#pragma mark -

-(void)trackPhotoShareButtonAction {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"photo_share";
    eventDictionary[@"eventAction"] = @"icon_clicked";
    eventDictionary[@"label"] = @(self.messageConfiuration.matchUserId);
    eventDictionary[@"status"] = @(self.messageConfiuration.matchUserId);
    
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}

-(void)trackClearChatButtonAction {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"messages";
    eventDictionary[@"eventAction"] = @"clear_chat";
    eventDictionary[@"label"] = @(self.messageConfiuration.hostUserId);
    eventDictionary[@"status"] = @(self.messageConfiuration.hostUserId);
    
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}
-(void)trackSparkProfileViewStatusFromSource:(NSString*)viewSource {
    BOOL messageReceived = (self.conversations.count) ? TRUE : FALSE;
    NSInteger sparkExpiryRemaininTime = self.messageConfiuration.sparkExpiryTimeInSecs;
    NSDictionary *eventInfo = @{@"match_id":@(self.messageConfiuration.matchUserId),@"source":viewSource,
                                @"msg_rcd":@(messageReceived),@"time_left":@(sparkExpiryRemaininTime)};
    [[TMAnalytics sharedInstance] trackSparkEvent:@"chat" status:@"view_profile" eventInfo:eventInfo];
}
-(void)trackSparkProfileDeleteStatus:(NSString*)status {
    BOOL messageReceived = (self.conversations.count) ? TRUE : FALSE;
    NSInteger sparkExpiryRemaininTime = self.messageConfiuration.sparkExpiryTimeInSecs;
    NSDictionary *eventInfo = @{@"match_id":@(self.messageConfiuration.matchUserId),@"msg_rcd":@(messageReceived),
                                @"time_left":@(sparkExpiryRemaininTime),@"profile_view":@(self.didViewDetailProfile)};
    [[TMAnalytics sharedInstance] trackSparkEvent:@"chat" status:status eventInfo:eventInfo];
}
-(void)trackSparkAcceptanceMessageStatus:(TMMessage*)message {
    BOOL isStickerMessage = (message.type == MESSAGETTYPE_STICKER) ? TRUE : FALSE;
    BOOL messageReceived = (self.conversations.count) ? TRUE : FALSE;
    NSString *text = (message.type == MESSAGETTYPE_TEXT) ? message.text : @"";
    NSInteger sparkExpiryRemaininTime = self.messageConfiuration.sparkExpiryTimeInSecs;
    NSDictionary *eventInfo = @{@"match_id":@(self.messageConfiuration.matchUserId),@"sticker":@(isStickerMessage),@"def_msg":@(FALSE),
                                @"msg_rcd":@(messageReceived),@"msg_sent":text,@"time_left":@(sparkExpiryRemaininTime),
                                @"profile_view":@(self.didViewDetailProfile)};
    [[TMAnalytics sharedInstance] trackSparkEvent:@"chat" status:@"message_sent" eventInfo:eventInfo];
}

@end


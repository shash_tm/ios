//
//  TMFullImageViewController.m
//  TrulyMadly
//
//  Created by Ankit on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMFullImageViewController.h"

@interface TMFullImageViewController ()

@end

@implementation TMFullImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
}
-(void)setFullSharedImage:(UIImage*)image {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                           30,
                                                                           self.view.frame.size.width,
                                                                           self.view.frame.size.height-130)];
    imageView.image = image;
    imageView.backgroundColor = [UIColor clearColor];
    imageView.clipsToBounds = TRUE;
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:imageView];
}

@end

//
//  TMMessageProfileViewController.m
//  TrulyMadly
//
//  Created by Ankit on 23/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageProfileViewController.h"
#import "TMFullScreenPhotoViewController.h"
#import "TMSelectQuizResultViewController.h"
#import "TMBuySelectViewController.h"

#import "TMMatchManager.h"
#import "TMSwiftHeader.h"
#import "TMPopOverView.h"
#import "TMLog.h"
#import "TMNativeAdWebViewController.h"
#import "TMAdTrackingModel.h"
#import "TMAdTrackingManager.h"
#import "TMProfile.h"
#import "TMProfileSelectInfo.h"
#import "TMEventMatch.h"
#import "TMToastView.h"
#import "TMNotificationHeaders.h"
#import "TMUserSession.h"



@interface TMMessageProfileViewController ()<TMPopOverViewDelegate, UIAlertViewDelegate, TMSparkPackageBuyViewDelegate>

@property(nonatomic,strong)TMPopOverView *popoverView;
@property(nonatomic,strong)TMMatchManager *matchManager;
@property(nonatomic,assign)BOOL isProfileRequestInProgress;
@property(nonatomic,strong)TMAdTrackingManager *adTrackerManager;
@property(nonatomic,strong)TMEventMatch *eventMatch;
@property(nonatomic,strong)UIView *matchActionView;
@property(nonatomic,strong)TMNativeAdWebViewController *webViewController;
@property(nonatomic,strong)TMSparkPackageBuyView *sparkPackageView;

@end

@implementation TMMessageProfileViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.matchManager = [[TMMatchManager alloc] init];
        self.isProfileRequestInProgress = FALSE;
    }
    return self;
}

-(instancetype)initWithEventMatchData:(TMEventMatch *)eventMatch {
    self = [super init];
    if(self) {
        self.eventMatch = eventMatch;
        self.matchManager = [[TMMatchManager alloc] init];
        self.isProfileRequestInProgress = FALSE;
        self.profileURLString = self.eventMatch.profileUrlString;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Profile Details";
    
    [[TMAnalytics sharedInstance] trackView:@"TMMessageProfileViewController"];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    TMLOG(@"TMMessageProfileViewController ---- dealloc");
    self.matchManager = nil;
    self.profileURLString = nil;
    self.adTrackerManager = nil;
    if(self.webViewController){
        [self.webViewController dismissViewControllerAnimated:false completion:nil];
    }
}

-(TMAdTrackingManager*)adTrackerManager {
    if(_adTrackerManager == nil) {
        _adTrackerManager = [[TMAdTrackingManager alloc] init];
    }
    return _adTrackerManager;
}

-(UIView*)matchActionView {
    if(!_matchActionView) {
        CGRect frame = self.view.frame;
        CGFloat height = 65;
        _matchActionView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(frame),
                                                                               CGRectGetHeight(frame) - height,
                                                                               CGRectGetWidth(frame),
                                                                               height)];
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = _matchActionView.bounds;//self.view.bounds;
        gradient.colors = @[(id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0] CGColor],
                                 (id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8] CGColor]];
        gradient.locations = @[@0.65f, @1.0f];
        [_matchActionView.layer insertSublayer:gradient atIndex:0];
        
        [self.view bringSubviewToFront:_matchActionView];

    }
    return _matchActionView;
}

-(void)removeSubViewsForDataReloading {
    NSArray *subviews = [self.view subviews];
    for (int i=0; i<subviews.count; i++) {
        UIView *subview = [subviews objectAtIndex:i];
        [subview removeFromSuperview];
    }
}
-(void)configureViewForDataLoadingStart {
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading Profile..."];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
    if(self.retryView.superview) {
        [self removeRetryView];
    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadProfileDataFromUrl) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadProfileDataFromUrl) forControlEvents:UIControlEventTouchUpInside];
    }
}

#pragma mark - Getter
-(void)setProfileURLString:(NSString *)profileURLString {
    _profileURLString = profileURLString;
    if((!self.isProfileRequestInProgress) && (profileURLString != nil)) {
        self.isProfileRequestInProgress = TRUE;
        [self loadProfileDataFromUrl];
    }
}

#pragma mark - Data Handling
-(void)loadProfileDataFromUrl {
    if(self.matchManager.isNetworkReachable) {
        NSString *URLString = _profileURLString;
        if(URLString) {
            NSDictionary *eventInfoDict = @{@"profile_url":URLString,@"enable_profile_cache":[NSNumber numberWithBool:true]};
            if(self.eventMatch) {
                eventInfoDict = @{@"profile_url":URLString,@"enable_profile_cache":[NSNumber numberWithBool:false]};
            }
            NSMutableDictionary *eventParams = [NSMutableDictionary dictionaryWithCapacity:16];
            eventParams[@"screenName"] = @"TMMessageProfileViewController";
            eventParams[@"eventCategory"] = @"profile";
            eventParams[@"eventAction"] = @"page_load";
            eventParams[@"event_info"] = eventInfoDict;
            self.matchManager.trackEventDictionary = eventParams;
            
            [self removeSubViewsForDataReloading];
            [self configureViewForDataLoadingStart];
            [self.matchManager userProfileFromUrl:eventInfoDict response:^(TMProfileResponse *match, TMError *error) {
                [self configureViewForDataLoadingFinish];
                ////process response
                if(match) {
                    self.isMutualMatch = true;
                    TMProfile *profile = [match profileForIndex:0];
                    self.profile = profile;
                    [self configureProfileCollectionViewWithProfileData];
                    [self.view addSubview:self.collectionView];
                    
                    // call the impression tracker if profile is an ad campaign
                    if(self.profile.isProfileAdCampaign && self.profile.matchId != nil) {
                        NSDictionary* matchDetails = @{@"source":@"profile"};
                        if(self.profile.matchId){
                            matchDetails = @{@"source":@"profile",@"match_id":self.profile.matchId};
                        }
                        [self.adTrackerManager trackImpressionUrls:self.profile.adObj.impressionTracker eventInfo:matchDetails];
                    }
                    
                    // for profile open from event details
                    if(self.eventMatch) {
                        [self addUserActionButtons];
                    }else {
                        self.collectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
                        self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
                    }
                    
                    [self showMutualEventNudge];
                }
                else {
                    if(error.errorCode == TMERRORCODE_LOGOUT) {
                        [self.actionDelegate setNavigationFlowForLogoutAction:false];
                    }
                    else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                        if (![self.matchManager isCachedResponsePResentForMatchProfile:_profileURLString]) {
                            [self showRetryViewAtError];
                            [self.retryView.retryButton addTarget:self action:@selector(loadProfileDataFromUrl) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                    else {
                        [self showRetryViewAtUnknownError];
                    }
                }
            }];
        }
    }
    else {
        [self showRetryViewAtError];
    }
}

-(void)addUserActionButtons {
    
    if(self.eventMatch.likedByMe) {
        if(!self.matchActionView.superview) {
            [self.view addSubview:self.matchActionView];
            CGFloat xPos = 8; CGFloat width = self.view.bounds.size.width-16; CGFloat height = 44; CGFloat yPos = 10.5;
           
            UIView *inviteView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
            inviteView.layer.cornerRadius = 5;
            inviteView.backgroundColor = [UIColor colorWithRed:0.804 green:0.804 blue:0.804 alpha:1.0];
            [self.matchActionView addSubview:inviteView];
            
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
            CGSize constrintSize = CGSizeMake(width, NSUIntegerMax);
            CGRect rect = [@"Like Sent" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            
            xPos = (width-rect.size.width)/2; yPos = (44-rect.size.height)/2;
            
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
            lbl.text = @"Like Sent";
            lbl.font = [UIFont systemFontOfSize:15];
            lbl.textColor = [UIColor whiteColor];
            [inviteView addSubview:lbl];
        }
    }
    else {
        if(!self.matchActionView.superview) {
            [self.view addSubview:self.matchActionView];
            CGFloat xPos = 8; CGFloat width = self.view.bounds.size.width*0.5-12; CGFloat height = 44; CGFloat yPos = 10.5;
            
            UIView *hideView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
            hideView.layer.cornerRadius = 5;
            hideView.backgroundColor = [UIColor hideColor];
            [self.matchActionView addSubview:hideView];
            
            xPos = (width-(20))/2;
            
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, (44-15)/2, 15, 15)];
            imgView.image = [UIImage imageNamed:@"nope"];
            [hideView addSubview:imgView];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didHideViewTap)];
            [hideView addGestureRecognizer:tapGesture];
            
            xPos = CGRectGetMaxX(hideView.frame)+8;
            UIView *likeView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
            likeView.layer.cornerRadius = 5;
            likeView.backgroundColor = [UIColor likeColor];
            [self.matchActionView addSubview:likeView];
            
            xPos = (width-(20))/2;
            
            UIImageView *followImgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, (44-15)/2, 21, 15)];
            followImgView.image = [UIImage imageNamed:@"like"];
            [likeView addSubview:followImgView];
            
            UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didLikeViewTap)];
            [likeView addGestureRecognizer:tapGesture1];
        }
    }
}

#pragma mark - uicollectionview datasource / delegate

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
fullScreenPhotoViewAction:(NSArray *)images
          currentIndex:(NSInteger)index {
    
    if(self.profile.isProfileAdCampaign && self.profile.matchId != nil) {
        // call click trackers
        NSDictionary* matchDetails = @{@"source":@"profile"};
        if(self.profile.matchId){
            matchDetails = @{@"source":@"profile",@"match_id":self.profile.matchId};
        }
        [self.adTrackerManager trackClickUrls:self.profile.adObj.clickTracker eventInfo:matchDetails];
        
        if(self.profile.adObj && self.profile.adObj.isLandingUrlAvailable) {
            //open web view with landing url
            [self showAdWebView];
        }else {
            TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] initWithReportUserOption:true];
            fullScreenViewCon.currentIndex = index;
            [self.navigationController pushViewController:fullScreenViewCon animated:YES];
            [fullScreenViewCon loadImgArray:images];
        }
    }else {
        TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] init];
        fullScreenViewCon.currentIndex = index;
        [self.navigationController pushViewController:fullScreenViewCon animated:YES];
        [fullScreenViewCon loadImgArray:images];
    }
}
-(void)collectionView:(TMProfileCollectionView *)collectionView
               layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
   didTrustScoreClick:(BOOL)isOpen
{
    if(isOpen) {
        [self trackActivityDashboard:@"trustscore" status:@"open"];
    }else {
        [self trackActivityDashboard:@"trustscore" status:@"close"];
    }
}
- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
commonFacebookFriendAction:(UIView*)actionView
     mutualConnections:(NSArray *)connections{
     
    CGRect convertedRect = [self.collectionView convertRect:actionView.frame toView:self.view];
    
    CGFloat xPos = CGRectGetMaxX(convertedRect) - (CGRectGetWidth(convertedRect)/2) - 120;
    CGFloat yPos = CGRectGetMinY(convertedRect) + CGRectGetHeight(convertedRect)/2;
    CGRect rect = CGRectMake(xPos,
                             yPos,
                             120,
                             44*3);
    
    self.popoverView = [[TMPopOverView alloc] initWithFrame:rect];
    self.popoverView.delegate = self;
    [self.view addSubview:self.popoverView];
    [self.popoverView loadData:connections withActionViewFrame:actionView.frame];
    
    self.popoverView.baseView.frame = self.view.bounds;
    [self.view addSubview:self.popoverView.baseView];
    [self.view bringSubviewToFront:self.popoverView];
}
-(void)handleSelectQuizCellClick {
    if([[TMUserSession sharedInstance].user isSelectUser]) {
        NSURL *matchImageURL = [NSURL URLWithString:self.profile.basicInfo.imageUrlString];
        NSInteger matchId = self.profile.matchId.integerValue;
        NSString *headerTitle = [self.profile.selectInfo getCommonText];
        NSString *headerSubtitle = self.profile.selectInfo.quotesText;
        
        TMSelectQuizResultViewController *selectQuizResultViewController = [[TMSelectQuizResultViewController alloc] initWithMatchId:matchId matchImageURL:matchImageURL headerTitle:headerTitle headerSubTitle:headerSubtitle];
        
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        selectQuizResultViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self presentViewController:selectQuizResultViewController animated:TRUE completion:nil];
    }
    else {
        NSString *source = (self.eventMatch) ? @"select_detail_card_profile:event_user_profile"
                                             : @"select_detail_card_profile:profile";
        [self showBuySelectViewControllerFromSource:source];
    }
    
    NSString *source = (self.eventMatch) ? @"event_user_profile" : @"profile";
    NSDictionary *trackInfo = @{@"source":source};
    [self trackSelectEvent:@"select_detail_card_profile" withStatus:@"clicked" userInfo:trackInfo];
}

#pragma mark - Show Web view on ad click
- (void)showAdWebView {
    //add the web view
    NSString* adLandringURLString = self.profile.adObj.landingUrl;
    
    NSString* webViewTitle = @"";
    
    self.webViewController = [[TMNativeAdWebViewController alloc] initWithNibName:@"TMNativeAdWebViewController" bundle:nil urlString:adLandringURLString webViewTitle:webViewTitle];
    [self presentViewController:self.webViewController animated:YES completion:nil];
}

-(void)didTapPopOverView {
    [self.popoverView removeFromSuperview];
    self.popoverView = nil;
}


#pragma mark - Like/hide action button method

-(void)didHideViewTap{
    BOOL isUnlikeActionRegistered = [[TMUserSession sharedInstance].user isUserUnlikeActionRegisteredFromEvent];
    
    if(isUnlikeActionRegistered) {
        [self performHideAction];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Hey! Taking this action will permanently remove this profile from your matches." message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
        [alertView show];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1) {
        TMUser *user = [TMUserSession sharedInstance].user;
        [user registerUserUnlikeActionFromEvent];
        [self performHideAction];
    }
}

-(void)performHideAction {
    if([self.matchManager isNetworkReachable]) {
        [self sendUserActionWithUrlString:self.eventMatch.hideUrl];
        if (self.profileDelegate) {
            [self.profileDelegate didHideProfile];
            [self.navigationController popViewControllerAnimated:true];
        }
    }else {
        // show toast
        [TMToastView showToastInParentView:self.view withText:TM_INTERNET_NOTAVAILABLE_MSG withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
    }
}

-(void)didLikeViewTap {
    if([self.profile showSelectNudgeOnLikeAction]) {
        [self showSelectJoinNudgeFromAction:@"like"];
    }
    else {
        if([self.matchManager isNetworkReachable]) {
            [self sendUserActionWithUrlString:self.eventMatch.likeUrl];
            if (self.profileDelegate) {
                [self.profileDelegate didLikeProfile];
                [self.navigationController popViewControllerAnimated:true];
            }
        }else {
            // show toast
            [TMToastView showToastInParentView:self.view withText:TM_INTERNET_NOTAVAILABLE_MSG withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
        }
    }
}

-(void)sendUserActionWithUrlString:(NSString *)actionURL{
    ////post notification to remove cached match
    if([[TMUserSession sharedInstance].user isUserFemale]) {
        NSDictionary *userInfo = @{@"matchid":self.eventMatch.user_id};
        [[NSNotificationCenter defaultCenter] postNotificationName:TMEVENTMATCH_LIKEHIDEACTION_NOTIFICATION
                                                            object:nil
                                                          userInfo:userInfo];
    }
    
    [self.matchManager sendMatchWithUrl:actionURL completionBlock:^(NSDictionary *mutualData, TMError *error) {
        BOOL isMutual = [[mutualData objectForKey:@"isMutualMatch"] boolValue];
        if(isMutual) {
            if(self.profileDelegate && self.eventMatch) {
                [self.profileDelegate isMutualMatch:self.eventMatch mutualMatchDict:mutualData];
            }
        }
    }];
}
-(void)showSelectJoinNudgeFromAction:(NSString*)action {
    if(!self.sparkPackageView) {
        self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
        self.sparkPackageView.delegate = self;
        self.sparkPackageView.action = action;
        self.sparkPackageView.source = @"event_user_profile";
    }
    NSString *userProfileImageURL = [TMUserSession sharedInstance].user.profilePicURL;
    NSString *matchImageURL = self.profile.basicInfo.imageUrlString;
    NSArray *imageURLStrings = @[matchImageURL,userProfileImageURL];
    NSString *selectProfileCTA = [[TMUserSession sharedInstance].user selectProfileCta];
    NSString *actionButtonString = [NSString stringWithFormat:@"%@!",selectProfileCTA];
    [self.sparkPackageView showSelectActionNudgeAlertWithActionText:[actionButtonString uppercaseString]
                                                withImageURLStrings:imageURLStrings];
    NSDictionary *trackingInfo = @{@"source":@"event_user_profile",@"on_action":action};
    [self trackSelectEvent:@"select_on_action_nudge" withStatus:@"view" userInfo:trackingInfo];
}
-(void)showBuySelectViewControllerFromSource:(NSString*)source {
    TMBuySelectViewController *buyViewController = [[TMBuySelectViewController alloc] initWithSource:source];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:buyViewController];
    [self presentViewController:navigationController animated:TRUE completion:nil];
}
-(void)showMutualEventNudge {
    for (UIView *subView in [self.view subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            [subView removeFromSuperview];
            break;
        }
    }
    if(self.profile.isMutualEventAvail) {
        TMLOG(@"mutual event available");
        [TMToastView showToastInParentViewForMutualEvent:self.view withMutualEventData:self.profile.mutualEventArr withDuaration:4.0 presentationDirection:TMToastPresentationFromLeft];
    }
}

#pragma mark - TMSparkPackageBuyView Delegate handler
-(void)didCancelJoinSelectNudge {
    NSDictionary *trackingInfo = [self.sparkPackageView getTrackingInfoDictionary];
    [self trackSelectEvent:self.sparkPackageView.eventName withStatus:@"clicked_no" userInfo:trackingInfo];
    
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}
-(void)didClickJoinSelectButton {
    NSDictionary *trackingInfo = [self.sparkPackageView getTrackingInfoDictionary];
    [self trackSelectEvent:self.sparkPackageView.eventName withStatus:@"clicked_yes" userInfo:trackingInfo];
    
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    
    [self showBuySelectViewControllerFromSource:@"select_on_action_nudge:event_user_profile"];
}
-(void)didRemoveSparkView {
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}

#pragma mark - Tracking
-(void)trackSelectEvent:(NSString*)eventName withStatus:(NSString*)status userInfo:(NSDictionary*)trackingInfo {
    NSString *isSelectMember = ([[TMUserSession sharedInstance].user isSelectUser]) ? @"true" : @"false";
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    if(trackingInfo) {
        [eventInfo addEntriesFromDictionary:trackingInfo];
    }
    eventInfo[@"is_select_member"] = isSelectMember;
    [[TMAnalytics sharedInstance] trackSelectEvent:eventName status:status eventInfo:eventInfo];
}
-(void)trackActivityDashboard:(NSString*)eventAction status:(NSString*)status {
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMMessageProfileViewController" forKey:@"screenName"];
    [eventDict setObject:@"profile" forKey:@"eventCategory"];
    [eventDict setObject:eventAction forKey:@"eventAction"];
    [eventDict setObject:status forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}

@end

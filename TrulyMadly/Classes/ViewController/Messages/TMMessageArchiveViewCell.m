//
//  TMMessageArchiveViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 27/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMMessageArchiveViewCell.h"
#import "UIColor+TMColorAdditions.h"


@interface TMMessageArchiveViewCell ()

@property(nonatomic,strong)UIView *seperatorView;

@end

@implementation TMMessageArchiveViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        self.seperatorView = [[UIView alloc] initWithFrame:CGRectZero];
        self.seperatorView.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:self.seperatorView];
        
        self.messageTextLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.messageTextLabel.textColor = [UIColor archiveMessageCellTextColor];
        self.messageTextLabel.font = [UIFont systemFontOfSize:16];
        self.messageTextLabel.backgroundColor = [UIColor clearColor];
        self.messageTextLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.messageTextLabel];
    }
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat height = 30;
    self.messageTextLabel.frame = CGRectMake(CGRectGetMinX(self.bounds),
                                             (CGRectGetHeight(self.bounds)-height)/2,
                                             CGRectGetWidth(self.bounds),
                                             height);
    
    self.seperatorView.frame = CGRectMake(0,
                                          79.3,
                                          self.frame.size.width, 0.7);
}

@end

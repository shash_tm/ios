//
//  TMBlockUserViewController.m
//  TrulyMadly
//
//  Created by Ankit on 09/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBlockUserViewController.h"
#import "TMActivityIndicatorView.h"
#import "NSString+TMAdditions.h"
#import "TMBlockUserMessageInfo.h"
#import "TMBlockUserTableViewCell.h"
#import "TMMessagingController.h"
#import "TMErrorMessages.h"


@interface TMBlockUserViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>


@property(nonatomic,strong)UITableView *deactivateReasonTableView;
@property(nonatomic,strong)NSMutableArray *blockMessageList;
@property(nonatomic,strong)NSString *deactivateHeading;
@property(nonatomic,strong)NSIndexPath *currentIndex;
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)UITextField *textField;
@property(nonatomic,strong)NSString *headerText;

@property(nonatomic,assign)BOOL isBlockUserCallInProgress;

@end

@implementation TMBlockUserViewController


-(instancetype)initWithBlockReason:(NSArray*)blockReasons blockFlags:(NSArray*)blockFlags {
    self = [super init];
    if(self) {
        self.currentIndex = nil;
        self.blockMessageList = [NSMutableArray arrayWithCapacity:4];
        
        NSArray *blockFlags = [NSArray arrayWithObjects:@"0", @"1", @"1", nil];
        NSArray *blockReasons = [NSArray arrayWithObjects:@"Inappropriate or offensive content", @"Misleading profile information", @"Others", nil];
        for (int i=0; i<blockFlags.count; i++) {
            TMBlockUserMessageInfo *info = [[TMBlockUserMessageInfo alloc] init];
            info.blockReason = [blockReasons objectAtIndex:i];
            info.blockType = [[blockFlags objectAtIndex:i] integerValue];
            [self.blockMessageList addObject:info];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:FALSE];
    
    [self initViewComponents];
    [self initNavigationItemWithActiveStatus:false];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initViewComponents {
    self.deactivateReasonTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.deactivateReasonTableView.delegate = self;
    self.deactivateReasonTableView.dataSource = self;
    self.deactivateReasonTableView.sectionHeaderHeight = 0;
    self.deactivateReasonTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.deactivateReasonTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.deactivateReasonTableView.scrollEnabled = false;
    if(self.headerText) {
         self.deactivateReasonTableView.sectionHeaderHeight = 40;
    }
    [self.view addSubview:self.deactivateReasonTableView];
}

-(void)initNavigationItemWithActiveStatus:(BOOL)activeStatus {
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(0, 0, 60, 44);
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [cancelBtn setEnabled:activeStatus];
    [cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]
                                          initWithCustomView:cancelBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    ////////
    UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame = CGRectMake(0, 0, 60, 44);
    [confirmBtn setTitle:@"Confirm" forState:UIControlStateNormal];
    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [confirmBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
                                           initWithCustomView:confirmBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

-(void)cancelAction {
    if(!self.isBlockUserCallInProgress) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

-(void)setHeaderTitle:(NSString*)headerTitle {
    self.headerText = headerTitle;
}

-(void)confirmAction {
    if(!self.isBlockUserCallInProgress) {
        if([self hasUserSelectedBlockReason]) {
            if([[TMMessagingController sharedController] currentNetworkConnectionStatus]) {
                self.isBlockUserCallInProgress = true;
                [self showActivityIndicatorView];
                TMBlockUserMessageInfo *info = [self.blockMessageList objectAtIndex:self.currentIndex.row];
                [self.delegate blockUserWithReason:info.blockReason];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TM_INTERNET_NOTAVAILABLE_MSG message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
        else {
            //show alert
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report User" message:@"Please select reason to block user" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}
-(BOOL)hasUserSelectedBlockReason {
    if(self.currentIndex != nil) {
        TMBlockUserMessageInfo *info = [self.blockMessageList objectAtIndex:self.currentIndex.row];
        if(info.blockType == 1) {
            TMBlockUserTableViewCell *cell = (TMBlockUserTableViewCell*) [self.deactivateReasonTableView cellForRowAtIndexPath:self.currentIndex];
            NSString *text = cell.blockReasonTf.text;
            if(text && ![[text stringByTrimingWhitespace] isEqualToString:@""]) {
                return true;
            }
            else  {
                return false;
            }
        }
        return true;
    }
    return false;
}
-(void)showActivityIndicatorView {
    self.view.userInteractionEnabled = false;
    self.deactivateReasonTableView.alpha = 0.1;
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    [self.view addSubview:self.activityIndicatorView];
    self.activityIndicatorView.titleText = self.loadingText;
    self.activityIndicatorView.titleLable.textColor = [UIColor darkGrayColor];
    [self.activityIndicatorView startAnimating];
}


#pragma mark UITableview datasource/delegate
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.self.blockMessageList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.currentIndex && self.currentIndex.row == indexPath.row) {
        TMBlockUserMessageInfo *info = [self.blockMessageList objectAtIndex:indexPath.row];
        if(info.blockType == 1) {
            return 80;
        }
        else {
            return 60;
        }
    }
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *bgView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    bgView.text = self.headerText;
    return bgView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"cell";
    TMBlockUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if(!cell) {
        cell = [[TMBlockUserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.blockReasonTf.delegate = self;
        cell.backgroundColor =[UIColor clearColor];
    }
    TMBlockUserMessageInfo *info = [self.blockMessageList objectAtIndex:indexPath.row];
    BOOL isTextMode = false;
    if((self.currentIndex) && (indexPath.row == self.currentIndex.row && info.blockType == 1)) {
        isTextMode = true;
    }
    
    if((self.currentIndex) && (indexPath.row == self.currentIndex.row)) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.blockLabel.text = info.blockReason;
    [cell configureCellForTextMode:isTextMode];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if((self.currentIndex != nil) && (self.currentIndex.row != indexPath.row) ) {
        UITableViewCell *currentCell = [tableView cellForRowAtIndexPath:self.currentIndex];
        currentCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    self.currentIndex = indexPath;
    self.textField.text = @"";
    [self.textField resignFirstResponder];
    
    [tableView reloadData];
}

#pragma mark UItextfield delegate
#pragma mark -

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
}


@end

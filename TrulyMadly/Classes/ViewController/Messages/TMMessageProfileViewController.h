//
//  TMMessageProfileViewController.h
//  TrulyMadly
//
//  Created by Ankit on 23/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileViewController.h"

@class TMEventMatch;

@protocol TMMessageProfileViewDelegate <NSObject>

@optional
-(void)didHideProfile;
-(void)didLikeProfile;
-(void)isMutualMatch:(TMEventMatch *)eventMatch mutualMatchDict:(NSDictionary *)mutualMatchDict;

@end

@interface TMMessageProfileViewController : TMProfileViewController

@property(nonatomic,weak)id<TMMessageProfileViewDelegate>profileDelegate;
@property(nonatomic,strong)NSString *profileURLString;
@property(nonatomic,assign)NSInteger matchId;

-(instancetype)initWithEventMatchData:(TMEventMatch *)eventMatch;

@end

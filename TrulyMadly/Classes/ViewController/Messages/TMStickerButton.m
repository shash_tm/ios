//
//  TMStickerButton.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 18/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMStickerButton.h"
#import "TMDataStore.h"

@interface TMStickerButton ()

@property (nonatomic, strong) UIImageView* blooperImageView;
@property (nonatomic, strong) UIImageView* bgImageView;

@end

@implementation TMStickerButton

- (instancetype) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.bgImageView = [[UIImageView alloc]initWithFrame:frame];
        [self.bgImageView setImage:[UIImage imageNamed:@"stickericon"]];
        self.bgImageView.center = self.center;
        self.bgImageView.clipsToBounds = TRUE;
        [self addSubview:self.bgImageView];
        
        [self createNotificationView];
    }
    return self;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.blooperImageView.frame = CGRectMake(0, 0, self.bounds.size.width/4, self.bounds.size.width/4);
    self.blooperImageView.center = CGPointMake((self.bounds.size.width - self.bounds.size.width/6)-3, (self.bounds.size.height/8)+2);
    self.bgImageView.frame = self.bounds;
}

- (void) createNotificationView {
    self.blooperImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.blooperImageView.image = [UIImage imageNamed:@"notification"];
    self.blooperImageView.backgroundColor = [UIColor clearColor];
    self.blooperImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.blooperImageView.hidden = YES;
    [self addSubview:self.blooperImageView];
    
    [self addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    //add observer for the new quiz notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newStickerReceivedNotification:) name:@"newStickerAvailable" object:nil];
}

- (void) newStickerReceivedNotification:(NSNotification*) newStickerNotification {

    [UIView animateWithDuration:0.3 animations:^{
       self.blooperImageView.hidden = NO;
    }];
}

- (void) setBlooperStatus:(BOOL) blooperStatus {
    [UIView animateWithDuration:0.3 animations:^{
        self.blooperImageView.hidden = !blooperStatus;
    }];
}

- (void) buttonPressed:(UIButton*) button {
    [UIView animateWithDuration:0.3 animations:^{
         self.blooperImageView.hidden = YES;
    } completion:^(BOOL finished) {
        [TMDataStore setObject:@NO forKey:STICKER_BLOOPER_TO_BE_SHOWN];
    }];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

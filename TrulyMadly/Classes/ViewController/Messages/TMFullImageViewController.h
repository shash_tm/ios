//
//  TMFullImageViewController.h
//  TrulyMadly
//
//  Created by Ankit on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMFullImageViewController : UIViewController

-(void)setFullSharedImage:(UIImage*)image;

@end

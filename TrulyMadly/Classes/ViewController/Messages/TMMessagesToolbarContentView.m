//
//  TMMessagesToolbarContentView.m
//  TrulyMadly
//
//  Created by Ankit on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessagesToolbarContentView.h"
#import "TMMessagesComposerTextView.h"


const CGFloat kJSQMessagesToolbarContentViewHorizontalSpacingDefault = 8.0f;


@interface TMMessagesToolbarContentView ()

@property (strong, nonatomic) TMMessagesComposerTextView *textView;
@property (strong, nonatomic) UIView *leftBarButtonContainerView;
@property (strong, nonatomic) UIView *rightBarButtonContainerView;
@property (strong, nonatomic) UIView *leftBarQuizContainerView;
@property (strong, nonatomic) UIView *rightBarShareButtonContainerView;


@end


@implementation TMMessagesToolbarContentView

#pragma mark - Initialization

-(instancetype)init {
    self = [super init];
    if(self) {
        

    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        
        //left container view
        CGFloat horizontalOffset = 9;
        //textfield
        CGRect rect = CGRectMake(103, 8, frame.size.width-162, 38);
        self.textView = [[TMMessagesComposerTextView alloc] initWithFrame:rect];
        self.textView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.textView];
        
        ////quiz
        self.leftBarQuizContainerView = [[UIView alloc] initWithFrame:CGRectZero];
        self.leftBarQuizContainerView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.leftBarQuizContainerView];
        
        //sticker button
        self.leftBarButtonContainerView = [[UIView alloc] initWithFrame:CGRectZero];
        self.leftBarButtonContainerView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.leftBarButtonContainerView];
        
        //right container view
        CGFloat rightButtonXPos = CGRectGetMaxX(rect)+horizontalOffset;
        CGFloat rightButtonWidth = 44;
        CGFloat rightButtonYPos = 0;
        CGFloat rightButtonHeight = CGRectGetHeight(self.frame);
        self.rightBarButtonContainerView = [[UIView alloc] initWithFrame:CGRectMake(rightButtonXPos,
                                                                                    rightButtonYPos,
                                                                                    rightButtonWidth,
                                                                                    rightButtonHeight)];
        self.rightBarButtonContainerView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.rightBarButtonContainerView];
    }
    return self;
}


- (instancetype) initWithCommonAnswerScreenFrame:(CGRect) frame
{
    if (self = [super initWithFrame:frame]) {
      
        UIColor* mainContentBackgroundColor = [UIColor colorWithRed:172.0/255.0 green:199.0/255.0 blue:229.0/255.0 alpha:1.0];
        
        self.backgroundColor = mainContentBackgroundColor;
        
        //left container view
        CGFloat horizontalOffset = 9;
        //textfield
     
        CGRect rect = CGRectMake(12, 10, frame.size.width-124, 38);
        self.textView = [[TMMessagesComposerTextView alloc] initWithFrame:rect];
        self.textView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.textView];
        
        //right container view
        CGFloat rightButtonXPos = CGRectGetMaxX(rect)+horizontalOffset;
        CGFloat rightButtonWidth = 44;
        CGFloat rightButtonYPos = 0;
        CGFloat rightButtonHeight = CGRectGetHeight(self.frame);
        self.rightBarButtonContainerView = [[UIView alloc] initWithFrame:CGRectMake(rightButtonXPos,
                                                                                    rightButtonYPos,
                                                                                    rightButtonWidth,
                                                                                    rightButtonHeight)];
        self.rightBarButtonContainerView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.rightBarButtonContainerView];
        
        CGFloat rightShareButtonXPos = CGRectGetMaxX(self.rightBarButtonContainerView.frame) + horizontalOffset;
        CGFloat rightShareButtonWidth = 52;
        CGFloat rightShareButtonYPos = 0;
        CGFloat rightShareButtonHeight = CGRectGetHeight(self.frame);
        
        self.rightBarShareButtonContainerView = [[UIView alloc] initWithFrame:CGRectMake(rightShareButtonXPos, rightShareButtonYPos, rightShareButtonWidth, rightShareButtonHeight)];
        self.rightBarShareButtonContainerView.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:179.0/255.0 blue:184.0/255.0 alpha:1.0];
        [self addSubview:self.rightBarShareButtonContainerView];
        
        return self;
    }
    
    return nil;
}
#pragma mark - Setters

-(void)setLeftBarButtonItem:(UIButton *)leftBarButtonItem {
    if(_leftBarButtonItem) {
        [_leftBarButtonItem removeFromSuperview];
    }
    
    if(!leftBarButtonItem) {
        _leftBarButtonItem = nil;
        _leftBarButtonContainerView.hidden = YES;
        return;
    }
    
    if(CGRectEqualToRect(leftBarButtonItem.frame, CGRectZero)) {
        CGFloat horizontalOffset = (CGRectEqualToRect(self.leftBarQuizContainerView.frame, CGRectZero)) ? 4 : 4;
        CGFloat leftButtonWidth = 40;
        CGFloat leftButtonHeight = 40;
        CGFloat leftButtonXPos = CGRectGetMaxX(self.leftBarQuizContainerView.frame)+horizontalOffset;
        CGFloat leftButtonYPos = (CGRectGetHeight(self.frame) - leftButtonHeight)/2;
        
        //sticker button
        self.leftBarButtonContainerView.frame = CGRectMake(leftButtonXPos, leftButtonYPos, leftButtonWidth, leftButtonHeight);
        leftBarButtonItem.frame = self.leftBarButtonContainerView.bounds;
    }
    
    self.leftBarButtonContainerView.hidden = NO;
    [self.leftBarButtonContainerView addSubview:leftBarButtonItem];
    
    _leftBarButtonItem = leftBarButtonItem;
}

-(void)setRightBarButtonItem:(UIButton *)rightBarButtonItem {
    if (_rightBarButtonItem) {
        [_rightBarButtonItem removeFromSuperview];
    }
    
    if (!rightBarButtonItem) {
        _rightBarButtonItem = nil;
        self.rightBarButtonContainerView.hidden = YES;
        return;
    }
    
    if (CGRectEqualToRect(rightBarButtonItem.frame, CGRectZero)) {
        rightBarButtonItem.frame = self.rightBarButtonContainerView.bounds;
    }
    
    self.rightBarButtonContainerView.hidden = NO;
    [self.rightBarButtonContainerView addSubview:rightBarButtonItem];
  
    _rightBarButtonItem = rightBarButtonItem;
}

-(void)setLeftQuizBarButtonItem:(UIButton *)leftQuizBarButtonItem {
    if (_leftQuizBarButtonItem) {
        [_leftQuizBarButtonItem removeFromSuperview];
    }
    
    if (!leftQuizBarButtonItem) {
        _leftQuizBarButtonItem = nil;
        self.leftBarQuizContainerView.hidden = YES;
        return;
    }
    
    //if (CGRectEqualToRect(leftQuizBarButtonItem.frame, CGRectZero)) {
        CGFloat leftButtonXPos = 8;//(CGRectGetMinX(self.textView.frame) - (2*horizontalOffset))/2;
        CGFloat leftButtonWidth = 44;
        CGFloat leftButtonHeight = 30;
        
        CGFloat leftButtonYPos = ((CGRectGetHeight(self.frame) - leftButtonHeight)/2)+2;
        
        //Quiz Button
        self.leftBarQuizContainerView.frame = CGRectMake(leftButtonXPos, leftButtonYPos, leftButtonWidth, leftButtonHeight);
        leftQuizBarButtonItem.frame = self.leftBarQuizContainerView.bounds;
    //}
    
    self.leftBarQuizContainerView.hidden = NO;
    [self.leftBarQuizContainerView addSubview:leftQuizBarButtonItem];
    
    _leftQuizBarButtonItem = leftQuizBarButtonItem;
}

- (void) setRightShareBarButtonItem:(UIButton *)rightShareBarButtonItem
{
    if (_rightShareBarButtonItem) {
        [_rightShareBarButtonItem removeFromSuperview];
    }
    if (!rightShareBarButtonItem) {
        _rightShareBarButtonItem = nil;
        self.rightBarShareButtonContainerView.hidden = YES;
        return;
    }
    if (CGRectEqualToRect(rightShareBarButtonItem.frame, CGRectZero)) {
        rightShareBarButtonItem.frame = self.rightShareBarButtonItem.bounds;
    }
    self.rightShareBarButtonItem.hidden = NO;
    [self.rightBarShareButtonContainerView addSubview:rightShareBarButtonItem];
    
    _rightShareBarButtonItem = rightShareBarButtonItem;
}

@end

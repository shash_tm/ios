//
//  UINativeAdTableViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 31/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMMessageListViewCell.h"

@interface TMNativeAdTableViewCell : TMMessageListViewCell

-(void)setName:(NSString*) nameText andDescription:(NSString*) descriptionText andIconURL:(NSString*) iconURL;

@end

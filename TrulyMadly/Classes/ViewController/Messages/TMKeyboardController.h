//
//  TMKeyboardController.h
//  TrulyMadly
//
//  Created by Ankit on 16/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString * const TMMessagesKeyboardControllerNotificationKeyboardDidChangeFrame;

FOUNDATION_EXPORT NSString * const TMMessagesKeyboardControllerUserInfoKeyKeyboardDidChangeFrame;

@class TMKeyboardController;

@protocol TMKeyboardControllerDelegate <NSObject>

@required

- (void)keyboardController:(TMKeyboardController*)keyboardController keyboardDidChangeFrame:(CGRect)keyboardFrame;

- (void)keyboardControllerKeyboardWillStopPanning:(TMKeyboardController*)keyboardController;

- (void)keyboardControllerKeyboardDidHide:(TMKeyboardController*)keyboardController;

@end


@interface TMKeyboardController : NSObject

@property (weak, nonatomic) id<TMKeyboardControllerDelegate> delegate;

@property (weak, nonatomic, readonly) UITextView *textView;

@property (weak, nonatomic, readonly) UIView *contextView;

@property (weak, nonatomic, readonly) UIPanGestureRecognizer *panGestureRecognizer;

@property (weak, nonatomic, readonly) UITapGestureRecognizer *tapGestureRecognizer;

@property (assign, nonatomic) CGPoint keyboardTriggerPoint;

@property (assign, nonatomic, readonly) BOOL keyboardIsVisible;

@property (assign, nonatomic, readonly) CGRect currentKeyboardFrame;

@property (assign, nonatomic) BOOL isKeyboardScrolling;

- (instancetype)initWithTextView:(UITextView *)textView
                     contextView:(UIView *)contextView
            panGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer
            tapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer
                        delegate:(id<TMKeyboardControllerDelegate>)delegate;

- (void)beginListeningForKeyboard;

- (void)endListeningForKeyboard;

-(void)hideKeyboard;

@end

//
//  TMQuizCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMLog.h"
#import "NSString+TMAdditions.h"

@interface TMQuizCollectionViewCell ()

@property(nonatomic,strong)UIImageView *quizImageView;
@property(nonatomic,strong)UIImageView *quizStatusImageView;
@property(nonatomic,strong)UILabel* labelName;

@end

@implementation TMQuizCollectionViewCell


-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        //self
        CGRect bounds = self.bounds;
        CGFloat offset = 8;
        
        self.labelName = [[UILabel alloc] initWithFrame:CGRectMake(bounds.origin.x+offset, bounds.size.height-4*offset, bounds.size.width-2*offset, 4*offset)];
        //self.labelName.backgroundColor = [UIColor blackColor];
        self.labelName.textColor = [UIColor blackColor];
        self.labelName.textAlignment = NSTextAlignmentCenter;
        self.labelName.numberOfLines = 0;
        self.labelName.text = @"test test test tettstetete et ete te te";
        self.labelName.font = [UIFont systemFontOfSize:13];
       // [self.labelName sizeToFit];
        [self.labelName adjustsFontSizeToFitWidth];
        
        //check for excessive label height
        if (self.labelName.frame.size.height > self.frame.size.height/2) {
            self.labelName.frame = CGRectMake(self.labelName.frame.origin.x, self.labelName.frame.origin.y, self.frame.size.width, self.frame.size.height/2 + 5);
            self.labelName.center = CGPointMake(self.frame.size.width/2, self.labelName.center.y);
            [self.labelName adjustsFontSizeToFitWidth];
        }
        self.labelName.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.labelName];

        //obtain the remaining height
        int remainingHeight = self.bounds.size.height - self.labelName.bounds.size.height;
        
        offset = 2;
        
        self.quizImageView = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.origin.x+offset,
                                                                              bounds.origin.y+offset,
                                                                              remainingHeight,
                                                                              remainingHeight)];
        self.quizImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.quizStatusImageView.autoresizingMask = ( UIViewAutoresizingFlexibleBottomMargin
                                                     | UIViewAutoresizingFlexibleHeight
                                                     | UIViewAutoresizingFlexibleLeftMargin
                                                     | UIViewAutoresizingFlexibleRightMargin
                                                     | UIViewAutoresizingFlexibleTopMargin
                                                     | UIViewAutoresizingFlexibleWidth );
        
        self.quizImageView.layer.cornerRadius = self.quizImageView.frame.size.width/2;
        self.quizImageView.clipsToBounds = YES;
        //self.quizImageView.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:self.quizImageView];
        
        self.quizStatusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.origin.x+bounds.size.width- bounds.size.width/2.5, bounds.origin.y ,(bounds.size.width/3),bounds.size.height/3)];
        self.quizStatusImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.quizStatusImageView.layer.cornerRadius = self.quizStatusImageView.frame.size.width/2;
        self.quizStatusImageView.clipsToBounds = YES;
        self.quizStatusImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.quizStatusImageView.layer.borderWidth = 1.5f;
        self.quizStatusImageView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:233.0/255.0 blue:236.0/255.0 alpha:1.0];
        
        //readjusting the label frame
        self.labelName.center = CGPointMake(self.labelName.center.x, self.quizImageView.frame.origin.y + self.quizImageView.frame.size.height + self.labelName.frame.size.height/2);
        
        //putting the image frame at center of cell
        self.quizImageView.center = CGPointMake(self.bounds.size.width/2, self.quizImageView.center.y);
        
        [self.contentView addSubview:self.quizStatusImageView];
        
        [self.contentView bringSubviewToFront:self.quizStatusImageView];
        
        self.clipsToBounds = YES;
        
        //setting the shadow attributes
//        self.layer.masksToBounds = NO;
//        self.layer.shadowOffset = CGSizeMake(-15, 20);
//        self.layer.shadowRadius = 5;
//        self.layer.shadowOpacity = 0.5;
        
        TMLOG(@"quiz image view frame %@",NSStringFromCGRect(self.quizImageView.frame));
        TMLOG(@"quiz label name frame %@",NSStringFromCGRect(self.labelName.frame));
        TMLOG(@"quiz cell frame %@",NSStringFromCGRect(self.bounds));
    }
    return self;
}

+(NSString*)reusableIdentifier {
    return @"quizCell";
}

-(void)setQuizIconFromURL:(NSURL *)imageURL{
    [self.quizImageView setImageWithURL:imageURL placeholderImage:nil];
}

-(void)setQuizStatusFromImage:(UIImage*) image
{
    if (image) {
        self.quizStatusImageView.hidden = NO;
        [self.quizStatusImageView setImage:image];
    }
    else {
        self.quizStatusImageView.hidden = YES;
    }
}


-(void)setQuizName:(NSString *)name{
    self.labelName.text = name;
}

@end

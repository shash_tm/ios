//
//  TMQuizListView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMQuizOverlayViewController.h"
#import "TMQuizController.h"


@protocol TMQuizListViewDelegate;

@interface TMQuizListView : UIView

@property(nonatomic,weak)id<TMQuizListViewDelegate> delegate;

-(instancetype)initWithFrame:(CGRect)frame withCellSize:(CGSize)cellSize quizContoller:(TMQuizController*) quizController;

- (void) showQuizGallery;

@end

@protocol TMQuizListViewDelegate <TMQuizOverlayViewControllerDelegate>

-(void)didClickOnQuiz:(NSArray*)quizData;

- (void) didClickOnNudgeQuiz;

@end
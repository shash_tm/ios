//
//  UINativeAdTableViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 31/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMNativeAdTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMProfileImageView.h"

@implementation TMNativeAdTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
      
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    //add the sponsered label here
    int width = self.bounds.size.width/4.2;
    int height = self.bounds.size.height*3/10;
    UILabel* sponseredLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - width, self.bounds.origin.y + self.layer.borderWidth + 2, width, height)];
    sponseredLabel.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:238.0/255.0 alpha:1.0];
    sponseredLabel.text = @"Sponsored";
    sponseredLabel.font = [UIFont systemFontOfSize:13];
    sponseredLabel.textColor = [UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0];
    sponseredLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:sponseredLabel];
    
    //set the detail indicator view image
     self.detailIndicatorImageView.image = [UIImage imageNamed:@"detailindicator_grey"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setName:(NSString*) nameText andDescription:(NSString*) descriptionText andIconURL:(NSString*) iconURL {
    [self setLabelFontAndColorForUnReadMessage];
    self.nameLabel.text = nameText;
    self.messageTextLabel.text = descriptionText;
    [self.profileImgView downloadAndSetImageFromURL:[NSURL URLWithString:iconURL]];
}

@end

//
//  TMStickerView.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 12/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMStickerView.h"
#import "TMStickerCollectionView.h"
#import "TMStickerCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMStickerFileManager.h"

@interface TMStickerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, weak) id<TMStickerViewDelegate> delegate;
@property(nonatomic, weak) id<TMStickerViewDataSource> dataSource;
@property(nonatomic, strong) TMStickerCollectionView* collectionView;
@property(nonatomic, assign) CGSize cellSize;

@end

#define DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT 123

@implementation TMStickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype) initWithFrame:(CGRect)frame cellSize:(CGSize) cellSize delegate:(id<TMStickerViewDelegate>) delegate dataSource:(id<TMStickerViewDataSource>) dataSource {
    if (self = [super initWithFrame:frame]) {
        self.delegate = delegate;
        self.dataSource = dataSource;
        self.cellSize = cellSize;
        [self configureUIComponents];
    }
    return self;
}

- (void) configureUIComponents {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    
    
    
    self.collectionView = [[TMStickerCollectionView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = self.backgroundColor = [UIColor whiteColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    [self.collectionView registerClass:[TMStickerCollectionViewCell class] forCellWithReuseIdentifier:[TMStickerCollectionViewCell reusableIdentifier]];
    [self addSubview:self.collectionView];
}

- (void) reloadData {
    [self.collectionView reloadData];
}

#pragma mark - UICollectionview datasource/delegate methods

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
   // NSUInteger tag = collectionView.superview.tag/DETAIL_COLLECTION_VIEW_MULTIPLICATION_CONSTANT;
    
  //  self.stickerArray = ((tag == TIMESTAMP_GALLERY_ID)?[self.dataSource stickersForTimestampGallery]:[self.dataSource stickersForGalleryID:tag]) ;
    
    return self.stickerArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMStickerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMStickerCollectionViewCell reusableIdentifier] forIndexPath:indexPath];
    
    NSArray* stickerArray = self.stickerArray;
    
    if (stickerArray.count > indexPath.row) {
        TMSticker* sticker = [stickerArray objectAtIndex:indexPath.row];
        UIImage* fileImage = [self.dataSource getImageForSticker:sticker];
        if (fileImage) {
            //sticker exists in the disk
            //load it
            [cell setStickerImage:fileImage];
        }
        else {
            [cell setStickerImage:nil];
        }
        cell.sticker = sticker;
        cell.tag = ([sticker.galleryID intValue] + 1)*([sticker.stickerID intValue] + 1);
    }
    else {
        [cell setStickerImage:nil];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.cellSize.width, self.cellSize.height);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[self.stickerArray objectAtIndex:indexPath.row] isKindOfClass:[TMSticker class]]) {
        TMSticker* sticker = [self.stickerArray objectAtIndex:indexPath.row];
        if (sticker.highQualityImageURLString) {
            //TODO:: decide for the tracking
            [self.delegate didSendSticker:sticker];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                //store the hd image in directory
                [self.delegate storeHDImageForSticker:sticker];
            });
        }
    }
    if ([[self.stickerArray objectAtIndex:indexPath.row] isKindOfClass:[TMSticker class]]) {
        TMSticker* sticker = [self.stickerArray objectAtIndex:indexPath.row];
        [self.delegate updateTimeStampForStickerWithStickerID:sticker.stickerID galleryID:sticker.galleryID];
    }
}

@end

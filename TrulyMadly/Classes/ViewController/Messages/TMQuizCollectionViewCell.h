//
//  TMQuizCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMQuizCollectionViewCell : UICollectionViewCell

+(NSString*)reusableIdentifier;

-(void)setQuizIconFromURL:(NSURL*)imageURL;
-(void)setQuizStatusFromImage:(UIImage*) image;
-(void)setQuizName:(NSString*)name;

@end

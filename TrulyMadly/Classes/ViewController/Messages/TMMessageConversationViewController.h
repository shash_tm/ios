//
//  TMMessageConversationViewController.h
//  TrulyMadly
//
//  Created by Ankit on 12/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMMessageViewController.h"

@class TMMessageConfiguration;
@class TMChatContext;
@class TMShareEventContext;

@protocol TMMessageConversationViewControllerDelegate;

@interface TMMessageConversationViewController : TMMessageViewController

@property (nonatomic, weak) id<TMMessageConversationViewControllerDelegate> messageDelegate;

-(instancetype)initWithMessageConversation:(TMMessageConfiguration*)messageConfiuration;

@end


@protocol TMMessageConversationViewControllerDelegate <NSObject>

-(void)reloadCachedConversation;
-(void)blockCurrentConversation;
-(void)willUnmatchCurrentConversation;

@optional
-(void)deleteSparkWithMatch:(TMMessageConfiguration*)configuration;

@end

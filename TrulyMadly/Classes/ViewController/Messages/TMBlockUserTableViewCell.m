//
//  TMBlockUserTableViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 16/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBlockUserTableViewCell.h"

@interface TMBlockUserTableViewCell ()


@end

@implementation TMBlockUserTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        self.blockLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.blockLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.blockLabel];
        
        self.blockReasonTf = [[UITextField alloc] initWithFrame:CGRectZero];
        self.blockReasonTf.backgroundColor = [UIColor clearColor];
        self.blockReasonTf.borderStyle = UITextBorderStyleRoundedRect;
        self.blockReasonTf.placeholder = @"Please specify";
        [self.contentView addSubview:self.blockReasonTf];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellForTextMode:(BOOL)textMode {
    if(textMode) {
        [self enableTextMode];
    }
    else {
        [self disableTextMode];
    }
}
-(void)enableTextMode {
    self.blockLabel.frame = CGRectMake(15, 5, self.frame.size.width, 20);
    self.blockReasonTf.frame = CGRectMake(15, 30, 250, 40);
    [self.blockReasonTf becomeFirstResponder];
}

-(void)disableTextMode {
    self.blockLabel.frame = CGRectMake(15, 20, self.frame.size.width, 20);
    self.blockReasonTf.frame = CGRectZero;
}

@end

//
//  TMStickerButton.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 18/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMStickerButton : UIButton

- (void) setBlooperStatus:(BOOL) blooperStatus;

@end

//
//  TMMessageViewController.m
//  TrulyMadly
//
//  Created by Ankit on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMessageListViewController.h"
#import "TMMessageManager.h"
#import "TMMessageConversationViewController.h"
#import "TMMessageListViewCell.h"
#import "TMNativeAdTableViewCell.h"
#import "TMConversation.h"
#import "TMActivityIndicatorView.h"
#import "TMErrorMessages.h"
#import "TMSwiftHeader.h"
#import "TMMessageConfiguration.h"
#import "TMMessagingController.h"
#import "UIColor+TMColorAdditions.h"
#import "TMDataStore.h"
#import "TMNotificationHeaders.h"
#import "TMArchiveConversationViewController.h"
#import "TMNativeAdWebViewController.h"
#import "TMMessageArchiveViewCell.h"
#import "TMToastView.h"
#import "TMSparkConversationView.h"
#import "TMTimestampFormatter.h"
#import "TMAnalytics.h"
#import "TMAdManager.h"
#import "TMAdConversation.h"
#import "TMArchiveConversation.h"
#import "TMSpark.h"
#import "MoEngage.h"
#import "TMStickerView.h"
#import "TMUserSession.h"
#import "TMLog.h"

#define BLOCKUSER_ALERTVIEW_TAG 100
#define UNMATCH_ALERTVIEW_TAG 101
#define SHARE_ALERTVIEW_TAG 102
#define SPARKVIEW_HEIGHT 205

typedef enum {
    UnMatch,
    Archive,
    Share
}RightOptionType;

@interface TMConversationListRightOptionView : UIView

@property(nonatomic,assign)RightOptionType rightOptionType;

+(UIView*)viewForRightOptionType:(RightOptionType)rightOptionType;

@end

@implementation TMConversationListRightOptionView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}
-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = CGRectGetWidth(self.frame);
    
    //////
    UILabel *label = (UILabel*)[self viewWithTag:1500];
    CGFloat labelXPos = 0;
    CGFloat labelYPos = 50;
    CGFloat labelHeight = 20;
    label.frame = CGRectMake(labelXPos, labelYPos, width, labelHeight);
    
    /////
    UIImageView *imageView = (UIImageView*)[self viewWithTag:1501];
    CGFloat imageWidth;
    CGFloat imageHeight;
    CGFloat imageYPos = 18;
    if(self.rightOptionType == UnMatch) {
        imageWidth = 30;
        imageHeight = 26;
        imageView.frame = CGRectMake((width-imageWidth)/2, imageYPos, imageWidth, imageHeight);
    }
    else if (self.rightOptionType == Archive) {
        imageWidth = 28;
        imageHeight = 24;
        imageView.frame = CGRectMake((width-imageWidth)/2, imageYPos, imageWidth, imageHeight);
    }
    else if (self.rightOptionType == Share) {
        imageWidth = 20;
        imageHeight = 24;
        imageView.frame = CGRectMake((width-imageWidth)/2, imageYPos, imageWidth, imageHeight);
    }
}
+(UIView*)viewForRightOptionType:(RightOptionType)rightOptionType {
    TMConversationListRightOptionView *view = [[TMConversationListRightOptionView alloc] initWithFrame:CGRectZero];
    view.rightOptionType = rightOptionType;
    view.backgroundColor = [UIColor likeselectedColor];
    view.layer.borderColor = [UIColor whiteColor].CGColor;
    view.layer.borderWidth = 0.5;
    
    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectZero];
    imgview.backgroundColor = [UIColor clearColor];
    imgview.tag = 1501;
    [view addSubview:imgview];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 1500;
    [view addSubview:label];
    
    if(rightOptionType == UnMatch) {
        imgview.image = [UIImage imageNamed:@"Unmatch"];
        label.text = @"Unmatch";
    }
    else if (rightOptionType == Archive) {
        imgview.image = [UIImage imageNamed:@"Archive"];
        label.text = @"Archive";
    }
    else if (rightOptionType == Share) {
        imgview.image = [UIImage imageNamed:@"ShareProfile"];
        label.text = @"Share";
    }
    return view;
}

@end

@interface TMMessageListViewController ()<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate,UIAlertViewDelegate,
                                          TMMessageConversationViewControllerDelegate,MOInAppDelegate,TMSparkConversationViewDelegate,TMSparkConversationViewDataSource, TMConversationControllerDelegate, TMNativeDelegate>

@property(nonatomic,strong)TMMessageManager *msgManager;
@property(nonatomic,strong)TMConversation *activeConversation;
@property(nonatomic,strong)TMConversation *messageConversation;
@property(nonatomic,strong)TMAdManager* adManager;
@property(nonatomic,strong)TMSpark* spark;

@property(nonatomic,strong)UITableView *messageConvTableView;
@property(nonatomic,strong)UIImageView *tutorialImageView;
@property(nonatomic,strong)UILabel *msgLabel;
@property(nonatomic,strong)UIView *conversationHeaderView;
@property(nonatomic,strong)TMSparkConversationView *sparkConversationView;

@property(nonatomic,strong)NSMutableArray *conversationList;
@property(nonatomic,strong)NSArray *archiveConversationList;
@property(nonatomic,strong)NSArray *sparkList;
@property(nonatomic,strong)NSDictionary* chatListAdDictionary;
@property(nonatomic,strong)NSString* shareText;
@property(nonatomic,strong)NSString* shareLink;

@property(nonatomic,assign)NSInteger blockUserIndex;
@property(nonatomic,assign)NSInteger unmatchIndex;
@property(nonatomic,assign)NSInteger archiveChatCount;
@property(nonatomic,assign)NSInteger chatListAdCount;
@property(nonatomic,assign)NSInteger activeConversationIndex;
@property(nonatomic,assign)NSInteger totalCachedSparkCount;

@property(nonatomic,assign)BOOL navigateToChat;
@property(nonatomic,assign)BOOL isFirstRequestInProgress;
@property(nonatomic,assign)BOOL scrollToTop;
@property(nonatomic,assign)BOOL hasInterstitialAdBeenShown;
@property(nonatomic,assign)BOOL willScreenDisappear;
@property(nonatomic,assign)BOOL isNativeAdShownTracked;
@end

@implementation TMMessageListViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        [self initDefaults];
    }
    return self;
}
-(instancetype)initWithNavigationToMessgaeConversation:(TMConversation*)conversation {
    self = [super init];
    if(self) {
        self.messageConversation = conversation;
        self.navigateToChat = (conversation) ? TRUE : FALSE;
        BOOL firstRequestStatus = [TMDataStore retrieveBoolforKey:@"firstconvlistreq_status"];
        self.isFirstRequestInProgress = !firstRequestStatus;
        self.delayTime = 0;
        [self initDefaults];
        [TMMessagingController sharedController].conversationDelegate = self;
        [self getActiveSpark];
    }
    return self;
}

-(void)initDefaults {
    self.scrollToTop = FALSE;
    self.conversationList = [NSMutableArray array];
    self.archiveConversationList = [NSArray array];
    self.activeConversationIndex = -1;
    self.adManager = [[TMAdManager alloc] init];
    self.adManager.adDelegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMessageList:) name:TMNEWMESSAGE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getActiveSpark) name:TMSPARK_REFRESH_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshConversationForSparkAcceptance) name:TMSPARKACCEPTANCE_REFRESH_NOTIFICATION object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sparkProgressTimer) name:TMSPARKTIMER_NOTIFICATION object:nil];
    
    // moengage in app messaging
    [[MoEngage sharedInstance]handleInAppMessage];
    [MoEngage sharedInstance].delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[TMAnalytics sharedInstance] trackView:@"TMMessageListViewController"];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setTitleText];
    [self addTableview];
    
    if(!self.isFirstRequestInProgress) {
        [self getCachedMessageConversation];
    }
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [NSTimer scheduledTimerWithTimeInterval:self.delayTime target:self selector:@selector(fetchConversationData) userInfo:nil repeats:NO];
    self.activeConversation = nil;
    self.activeConversationIndex = -1;
    if(self.navigateToChat) {
        self.navigateToChat = FALSE;
        TMMessageConversationViewController *msgConvViewCon = [self messageConvViewControllerForConversation:self.messageConversation];
        msgConvViewCon.messageDelegate = self;
        [self.navigationController pushViewController:msgConvViewCon animated:false];
    }
    self.willScreenDisappear = NO;
    //make updated ad call
    [self addNativeAdsForMessageListScreen];
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setTitleText];
}
-(void)viewDidLayoutSubviews {
    CGFloat yOffset = (self.totalCachedSparkCount) ? SPARKVIEW_HEIGHT : 0;
    CGFloat yPos = CGRectGetMinY(self.view.bounds)+yOffset;
    self.messageConvTableView.frame = CGRectMake(CGRectGetMinX(self.view.bounds),
                                                 yPos,
                                                 CGRectGetWidth(self.view.bounds),
                                                 CGRectGetHeight(self.view.bounds) - yPos);
    self.msgLabel.frame = CGRectMake(0,
                                     (self.view.frame.size.height-30)/2,
                                     self.view.frame.size.width,
                                     30);
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if([[self.chatListAdDictionary objectForKey:@"mediationAd"] boolValue]) {
        [self.adManager unregisterNativeAd:self.chatListAdDictionary];
    }
    self.willScreenDisappear = YES;
}
- (void)didReceiveMemoryWarning {
    
    [[TMMessagingController sharedController] stopSparkTimer];
    //nullify the ad manager in case of low memory
    if(self.adManager != nil) {
        [self.adManager deinit];
        self.adManager = nil;
    }
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    TMLOG(@"TMMessageListViewController --- dealloc");
    [TMMessagingController sharedController].conversationDelegate = nil;
    [[TMMessagingController sharedController] stopSparkTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if(self.adManager != nil) {
        [self.adManager deinit];
        self.adManager = nil;
    }
}

#pragma mark getter
#pragma mark -

-(TMMessageManager*)msgManager {
    if(!_msgManager) {
        _msgManager = [[TMMessageManager alloc] init];
    }
    return _msgManager;
}
//-(TMAdManager*)adManager {
//    if(!_adManager) {
//        _adManager = [[TMAdManager alloc] init];
//        _adManager.adDelegate = self;
//    }
//    return _adManager;
//}
-(NSArray*)archiveConversationList {
    if(!_archiveConversationList) {
        _archiveConversationList = [NSArray array];
    }
    return _archiveConversationList;
}
-(NSArray*)sparkList {
    if(!_sparkList) {
        _sparkList = [NSArray array];
    }
    return _sparkList;
}
-(TMSparkConversationView*)sparkConversationView {
    if(!_sparkConversationView) {
        _sparkConversationView = [[TMSparkConversationView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                           CGRectGetWidth(self.view.frame),
                                                                                           SPARKVIEW_HEIGHT)];
        _sparkConversationView.delegate = self;
        _sparkConversationView.datasource = self;
        [self.view addSubview:_sparkConversationView];
    }
    return _sparkConversationView;
}
-(UIView*)conversationHeaderView {
    if(!_conversationHeaderView) {
        _conversationHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.messageConvTableView.frame), 36)];
        [_conversationHeaderView setBackgroundColor:[UIColor conversationHeaderColor]];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20,
                                                                   0,
                                                                   CGRectGetWidth(_conversationHeaderView.frame)-40,
                                                                   CGRectGetHeight(_conversationHeaderView.frame))];
        [label setFont:[UIFont systemFontOfSize:16]];
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor conversationHeaderTitleColor];
        label.tag = 7100;
        [_conversationHeaderView addSubview:label];
        
        UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(_conversationHeaderView.frame) - 0.7,
                                                                         CGRectGetWidth(self.messageConvTableView.frame),
                                                                         0.7)];
        [seperatorView setBackgroundColor:[UIColor conversationListSeperatorColor]];
        [_conversationHeaderView addSubview:seperatorView];
    }
    UILabel *label = [_conversationHeaderView viewWithTag:7100];
    label.text = [NSString stringWithFormat:@"%@ (%ld)",@"Mutual Matches",self.conversationList.count];
    return _conversationHeaderView;
}

#pragma mark View Components Creation Methods
#pragma mark -

-(void)setTitleText {
    if(!self.title) {
        self.title = @"Conversations";
    }
}
-(void)showTutorial {
    if(!self.tutorialImageView) {
        self.tutorialImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        self.tutorialImageView.userInteractionEnabled = TRUE;
        self.tutorialImageView.backgroundColor = [UIColor clearColor];
        NSString *imageName = [[TMUserSession sharedInstance].user isUserFemale] ? @"tut_female" : @"tut_male";
        self.tutorialImageView.image = [UIImage imageNamed:imageName];
        self.tutorialImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:self.tutorialImageView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tutorialAction:)];
        [self.tutorialImageView addGestureRecognizer:tapGesture];
    }
    [self.view bringSubviewToFront:self.tutorialImageView];
}
-(void)addTableview {
    self.messageConvTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.messageConvTableView.dataSource = self;
    self.messageConvTableView.delegate = self;
    self.messageConvTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.messageConvTableView];
}
-(void)addMsgLabel {
    self.msgLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.msgLabel.text = @"No Message Conversations";
    self.msgLabel.textAlignment = NSTextAlignmentCenter;
    self.msgLabel.font = [UIFont systemFontOfSize:20];
    self.msgLabel.textColor = [UIColor lightGrayColor];
    [self.view addSubview:self.msgLabel];
}
-(void)configureUIForMessageConversationData:(NSDictionary*)conversationData {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.conversationList removeAllObjects];
        self.archiveConversationList = nil;
        
        NSArray *activeConversationList = conversationData[@"activeconvlist"];
        NSArray *archivedConversationList = conversationData[@"archiveconvlist"];
        
        if(activeConversationList.count) {
            [self.conversationList addObjectsFromArray:activeConversationList];
        }
        if(self.chatListAdCount) {
            NSInteger adConversationIndex = -1;
            if(self.conversationList.count >= 4) {
                adConversationIndex = 3;
            }
            if(self.conversationList.count > 0 && self.conversationList.count < 4) {
                adConversationIndex = self.conversationList.count;
            }
            if(adConversationIndex != -1) {
                TMAdConversation *adConversation = [[TMAdConversation alloc] init];
                [self.conversationList insertObject:adConversation atIndex:adConversationIndex];
            }
        }
        if(archivedConversationList.count) {
            self.archiveChatCount = archivedConversationList.count;
            self.archiveConversationList = archivedConversationList;
            TMArchiveConversation *archiveConversation = [[TMArchiveConversation alloc] init];
            [self.conversationList addObject:archiveConversation];
        }
        
        ///
        [self reloadMessageListTable];
        [self.view bringSubviewToFront:self.messageConvTableView];
        
        ///
        if(self.conversationList.count) {
            if(![self isTuturialShown]) {
                [self showTutorial];
            }
            else if(self.tutorialImageView) {
                [self.view bringSubviewToFront:self.tutorialImageView];
            }
        }
    });
}
-(void)configureUIForErrorMessageConversationResponse:(TMError*)error {
    //error case
    if(error.errorCode == TMERRORCODE_LOGOUT) {
        [self.actionDelegate setNavigationFlowForLogoutAction:false];
    }
    else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showRetryViewAtError];
    }
    else {
        //unknwon error
        [self showRetryViewAtUnknownError];
    }
}
-(void)configureViewForDataLoadingStartWithText:(NSString*)text {
    if(self.msgLabel.superview) {
        [self.msgLabel removeFromSuperview];
        self.msgLabel = nil;
    }
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:text];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
}
-(void)showRetryViewAtError {
    if(!self.isRetryViewShown) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(fetchConversationData) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(fetchConversationData) forControlEvents:UIControlEventTouchUpInside];
    }
}
- (void)showNativeAdWebView {
    //add the web view
   // NSString* nativeAdLandringURLString = [[self.chatListAdDictionary objectForKey:@"clickUrl"]];
    NSString* nativeAdLandringURLString = ([self.chatListAdDictionary valueForKey:@"landingURL"] && [[self.chatListAdDictionary valueForKey:@"landingURL"] isKindOfClass:[NSString class]])?[self.chatListAdDictionary valueForKey:@"landingURL"]:nil;
    
    NSString* webViewTitle = ([[self.chatListAdDictionary valueForKey:@"title"] isValidObject])?[self.chatListAdDictionary valueForKey:@"title"]:@"";
    
    TMNativeAdWebViewController* webViewController = [[TMNativeAdWebViewController alloc] initWithNibName:@"TMNativeAdWebViewController" bundle:nil urlString:nativeAdLandringURLString webViewTitle:webViewTitle];
    [self presentViewController:webViewController animated:YES completion:nil];
}
- (void)reloadMessageListTable {
    [self.messageConvTableView reloadData];
//    if (!self.hasInterstitialAdBeenShown) {
//        //make call for interstitial ads
//        [self loadInterstitialAd];
//    }
}
-(NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    if([[TMUserSession sharedInstance].user isUserFemale]) {
        [rightUtilityButtons sw_addUtilityButton:[TMConversationListRightOptionView viewForRightOptionType:Share]];
    }
    [rightUtilityButtons sw_addUtilityButton:[TMConversationListRightOptionView viewForRightOptionType:Archive]];
    [rightUtilityButtons sw_addUtilityButton:[TMConversationListRightOptionView viewForRightOptionType:UnMatch]];
    
    return rightUtilityButtons;
}
-(NSArray *)rightButtonsForMsTm
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButton:[TMConversationListRightOptionView viewForRightOptionType:Archive]];
    return rightUtilityButtons;
}


#pragma mark Msgs Conversations Data Handling
#pragma mark -

-(void)fetchConversationData {
    if(self.msgManager.isNetworkReachable) {
        self.delayTime = 0;
        [self trackGetConversationListNetworkRequest];
        if(self.isFirstRequestInProgress) {
            [self configureViewForDataLoadingStartWithText:@"Loading Message Conversations..."];
        }
        [self.msgManager fetchConversationList:^(NSArray *conversationList, TMError *error) {
            if(self.isFirstRequestInProgress) {
                [self configureViewForDataLoadingFinish];
            }
            if(!error) {
                if(conversationList.count) {
                    if(self.isFirstRequestInProgress) {
                        self.isFirstRequestInProgress = FALSE;
                        [TMDataStore setObject:[NSNumber numberWithBool:TRUE] forKey:@"firstconvlistreq_status"];
                    }
                    [self updateConversationList:conversationList];
                    
                    //MoEngage event
                    if(conversationList.count > 1) {
                        [[MoEngage sharedInstance]trackEvent:@"Conversation List Launched" andPayload:nil];
                    }
                }
                else {
                    if(self.isFirstRequestInProgress) {
                        [self addMsgLabel];
                    }
                }
            }
            else {
                if(self.isFirstRequestInProgress) {
                    [self configureUIForErrorMessageConversationResponse:error];
                }
            }
        }];
    }
    else {
        if(self.isFirstRequestInProgress) {
            TMLOG(@"No Network While Fetching Conversation List");
            [self showRetryViewAtError];
        }
    }
}
-(void)getActiveSpark {
    [[TMMessagingController sharedController] getActiveSpark:^(TMSpark *spark,NSInteger totalCachedSpark) {
        if(spark) {
            self.spark = spark;
            self.totalCachedSparkCount = totalCachedSpark;
            [[TMUserSession sharedInstance] updateSparkConversationTutorialStatusAsShown];
            TMLOG(@"cuurent sparkid:%ld totalspark:%ld",(long)spark.matchId,(long)totalCachedSpark);
            [self reloadConversationViewForSpark];
            if(!self.spark.isSparkSeen) {
                TMLOG(@"spark not seen:sending spark seen call");
                [self updateSparkAsSeen];
            }
        }
        else {
            if([[TMUserSession sharedInstance] canShowSparkConversationTutorial]) {
                self.totalCachedSparkCount = 1;
                [self reloadConversationViewForSparkTutorial];
                [[TMUserSession sharedInstance] updateSparkConversationTutorialCounter];
            }
            else {
                self.spark = nil;
                self.totalCachedSparkCount = 0;
                [self reloadConversationViewForNoSpark];
            }
        }
    }];
}
-(void)reloadConversationViewForSpark {
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat yPos = CGRectGetMinY(self.view.bounds) + SPARKVIEW_HEIGHT;
        self.messageConvTableView.frame = CGRectMake(CGRectGetMinX(self.view.bounds),
                                                     yPos,
                                                     CGRectGetWidth(self.view.bounds),
                                                     CGRectGetHeight(self.view.bounds) - yPos);
        
        [self.sparkConversationView reloadSparkData];
        
        if(self.spark.isSparkSeen) {
            TMLOG(@"spark already seen:starting timer");
            [[TMMessagingController sharedController] startSparkTimer];
        }
    });
}
-(void)reloadConversationViewForSparkTutorial {
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat yPos = CGRectGetMinY(self.view.bounds) + SPARKVIEW_HEIGHT;
        self.messageConvTableView.frame = CGRectMake(CGRectGetMinX(self.view.bounds),
                                                     yPos,
                                                     CGRectGetWidth(self.view.bounds),
                                                     CGRectGetHeight(self.view.bounds) - yPos);
        
        [self.sparkConversationView reloadSparkDataForTutorialView];
    });
}
-(void)reloadConversationViewForNoSpark {
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat yPos = CGRectGetMinY(self.view.bounds);
        self.messageConvTableView.frame = CGRectMake(CGRectGetMinX(self.view.bounds),
                                                     yPos,
                                                     CGRectGetWidth(self.view.bounds),
                                                     CGRectGetHeight(self.view.bounds) - yPos);
        
        [self.sparkConversationView removeFromSuperview];
        self.sparkConversationView = nil;
    });
}
-(void)updateSparkAsSeen {
    NSInteger matchId = self.spark.matchId;
    NSString *hash = self.spark.sparkHash;
    [self trackSparkStatus:@"seen"];
    [[TMMessagingController sharedController] updateSparkStatusAsSeen:matchId
                                                                 hash:hash
                                                 didUpdateSparkStatus:^(BOOL status) {
         if(status) {
             TMLOG(@"spark seen starting timer");
             dispatch_async(dispatch_get_main_queue(), ^{
                 self.spark.isSparkSeen = TRUE;
                 self.spark.sparkStartTime = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
                 
                 [[TMMessagingController sharedController] startSparkTimer];
             });
         }
         else {
             [self trackSparkStatus:@"error"];
         }
    }];
}
-(void)getCachedMessageConversation {
    [[TMMessagingController sharedController] getConversationData:^(NSDictionary *conversationData) {
        [self configureUIForMessageConversationData:conversationData];
    }];
}
-(void)updateConversationList:(NSArray*)data {
    [[TMMessagingController sharedController] saveConversationList:data newConversationData:^(NSDictionary *conversationData) {
        [self configureUIForMessageConversationData:conversationData];
    }];
}
-(BOOL)isChatConversation:(id)conversation {
    if([conversation isKindOfClass:[TMConversation class]]) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isAdConversation:(id)conversation {
    if([conversation isKindOfClass:[TMAdConversation class]]) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isArchiveConversation:(id)conversation {
    if([conversation isKindOfClass:[TMArchiveConversation class]]) {
        return TRUE;
    }
    return FALSE;
}
-(TMMessageConversationViewController*)messageConvViewControllerForConversation:(TMConversation*)conversation {
    TMMessageConfiguration *messageConfg = [[TMMessageConfiguration alloc] init];
    messageConfg.messageConversationFullLink = conversation.fullConvLink;
    messageConfg.matchUserId = conversation.matchId;
    messageConfg.fName = conversation.fName;
    messageConfg.profileImageURLString = conversation.profileImageURL;
    messageConfg.profileURLString = conversation.profileURLString;
    messageConfg.clearChatTs = conversation.clearChatTs;
    messageConfg.isMsTm = conversation.isMsTm;
    messageConfg.dealState = conversation.dealState;
    messageConfg.isConversationHasUnreadMessage = [conversation isUnreadMessageConversation];
    TMMessageConversationViewController *msgConvViewCon = [[TMMessageConversationViewController alloc] initWithMessageConversation:messageConfg];
    msgConvViewCon.messageDelegate = self;
    return msgConvViewCon;
}
-(TMMessageConversationViewController*)conversationViewControllerForCurrentSpark {
    TMMessageConfiguration *messageConfg = [[TMMessageConfiguration alloc] init];
    messageConfg.sparkRemainingTime = [self.spark sparkExpiryRemainingTime];
    messageConfg.sparkExpiryTimeInSecs = self.spark.sparkExpiryTime;
    messageConfg.sparkHash = self.spark.sparkHash;
    messageConfg.sparkUser = self.spark.matchData;
    messageConfg.messageConversationFullLink = self.spark.fullConversationLink;
    messageConfg.matchUserId = [self.spark matchId];
    messageConfg.fName = [self.spark fName];
    messageConfg.profileImageURLString = [self.spark profileImageURLString];
    messageConfg.profileURLString = self.spark.profileURLString;
    messageConfg.isSparkConversation = TRUE;
    messageConfg.isMsTm = FALSE;
    messageConfg.dealState = DealStateDisabled;
    messageConfg.isConversationHasUnreadMessage = FALSE;
    messageConfg.clearChatTs = nil;
    TMMessageConversationViewController *msgConvViewCon = [[TMMessageConversationViewController alloc] initWithMessageConversation:messageConfg];
    msgConvViewCon.messageDelegate = self;
    return msgConvViewCon;
}
-(void)updateConversationAsBlocked:(TMChatContext*)chatContext {
    [[TMMessagingController sharedController] markConversationAsBlockedAndShown:chatContext];
}
-(void)updateConversationAsRead:(TMConversation*)conversationMessage {
    [[TMMessagingController sharedController] markConversationAsRead:[conversationMessage getChatContext]];
}

#pragma mark - tableview datasource / delegate methods
#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.conversationList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 36;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.conversationHeaderView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id conversation = [self.conversationList objectAtIndex:indexPath.row];
    
    if([self isAdConversation:conversation]) {
        NSString *reusableIdentifier, *name, *description, *iconURL;
        BOOL isSeventyNineAd = YES;
        reusableIdentifier = @"chatListNativeAdCell";
        TMNativeAdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
        if (!cell) {
            cell = [[TMNativeAdTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        name = 	([[self.chatListAdDictionary valueForKey:@"title"] isValidObject])?[self.chatListAdDictionary valueForKey:@"title"]:@"";
        description = ([[self.chatListAdDictionary valueForKey:@"description"] isValidObject]?[self.chatListAdDictionary valueForKey:@"description"]:@"");
        iconURL = [self.chatListAdDictionary valueForKey:@"iconUrl"];
        [cell setName:name andDescription:description andIconURL:iconURL];
        if([[self.chatListAdDictionary objectForKey:@"mediationAd"] boolValue]) {
            isSeventyNineAd = NO;
            [self.adManager registerNativeAdWithViewObject:self.chatListAdDictionary view:cell viewController:self];
        }
        if(!self.isNativeAdShownTracked) {
            self.isNativeAdShownTracked = YES;
            if(isSeventyNineAd) {
                [self.adManager registerNativeAdForImpressions:self.chatListAdDictionary];
            }
            [self.adManager trackNativeAdShownEvent:isSeventyNineAd];
        }
        
        return cell;
    }
    else if([self isArchiveConversation:conversation]) {
        NSString *reusableIdentifier = @"archivedcell";
        TMMessageArchiveViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
        
        if(!cell) {
            cell = [[TMMessageArchiveViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.messageTextLabel.text = [NSString stringWithFormat:@"Archived chats (%ld)",(long)self.archiveConversationList.count];
        return  cell;
    }
    else {
        NSString *reusableIdentifier = @"mlcell";
        TMMessageListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
        if(!cell) {
            cell = [[TMMessageListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        
        TMConversation *chatConversation = (TMConversation*)conversation;
        [cell setMessageListData:conversation];
        
        if(chatConversation.isMsTm) {
            [cell setRightUtilityButtons:[self rightButtonsForMsTm] WithButtonWidth:100];
        }
        else if((!chatConversation.isMsTm) && (!chatConversation.isBlocked)) {
            [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:100];
        }
        else {
            cell.rightUtilityButtons = nil;
        }
        return  cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    id conversation = [self.conversationList objectAtIndex:indexPath.row];
    
    if([self isAdConversation:conversation]) {
        //SeventyNine Native Ad changes
        if(![[self.chatListAdDictionary objectForKey:@"mediationAd"] boolValue]) {
            [self.adManager nativeAdClickedWithParam:self.chatListAdDictionary];
        }
    }
    else if([self isArchiveConversation:conversation]) {
        TMArchiveConversationViewController *archiveConvListViewCon = [[TMArchiveConversationViewController alloc] init];
        [self.navigationController pushViewController:archiveConvListViewCon animated:TRUE];
    }
    else if([self isChatConversation:conversation]) {
        TMConversation *messageConversation = [self.conversationList objectAtIndex:indexPath.row];
        if([messageConversation isBlocked]) {
            self.blockUserIndex = indexPath.row;
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Match Inactive" message:EM_BLOCK_ALERT_MSG delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                alertView.tag = BLOCKUSER_ALERTVIEW_TAG;
                [alertView show];
            });
        }
        else {
            if(messageConversation.conversationMessageType == CONVERSATIONMESSAGE_MUTUALLIKE) {
                [[TMMessagingController sharedController] fireMutualMatchChatRead:messageConversation.matchId];
            }
            self.activeConversation = messageConversation;
            self.activeConversationIndex = indexPath.row;
            
            ////move to oto viewcontroller
            dispatch_async(dispatch_get_main_queue(), ^{
                TMMessageConversationViewController *msgConvViewCon = [self messageConvViewControllerForConversation:messageConversation];
                msgConvViewCon.actionDelegate = self.actionDelegate;
                [self.navigationController pushViewController:msgConvViewCon animated:YES];
                messageConversation.seen  = TRUE;
                self.title = nil;
            });
        }
    }
 }

#pragma mark - TMSparkConversationView datasource / delegate / Notification methods
#pragma mark -
-(NSInteger)sparkRowCount {
    return self.totalCachedSparkCount;
}
-(NSString*)sparkProfileName {
    return self.spark.fName;
}
-(NSString*)sparkProfileAge {
    return self.spark.age;
}
-(NSString*)sparkProfileDesignation {
    return self.spark.designation;
}
-(NSString*)sparkMessage {
    return self.spark.message;
}
-(NSString*)sparkProfileImageURLString {
    return self.spark.profileImageURLString;
}
-(NSString*)sparkProgressPercent {
    return [self.spark sparkRemainingTimeInShortFormat];
}
-(BOOL)isSelectUser {
    return self.spark.isSelectMember;
}
-(void)didSelectSpark {
    if(self.spark) {
        TMMessageConversationViewController *msgConvViewCon = [self conversationViewControllerForCurrentSpark];
        msgConvViewCon.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:msgConvViewCon animated:YES];
        self.title = nil;
        [self trackSparkStatus:@"clicked"];
    }
}

-(void)refreshConversationForSparkAcceptance {
    [[TMMessagingController sharedController] stopSparkTimer];
    [self fetchConversationData];
    [self getActiveSpark];
}
-(void)didSparkProgressTimerComplete {
    if([self.spark isSparkExpired]) {
        //check if oto viewcon is pushed then pop
        [[TMMessagingController sharedController] stopSparkTimer];
        [self getActiveSpark];
    }
    else {
        NSString *sparkRemaingTime = [self.spark sparkExpiryRemainingTime];
        [[NSNotificationCenter defaultCenter] postNotificationName:TMSPARKTIMER_NOTIFICATION object:sparkRemaingTime];
    }
}

#pragma mark - Alert Delegate Handler
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == BLOCKUSER_ALERTVIEW_TAG) {
        if ([[self.conversationList objectAtIndex:self.blockUserIndex] isKindOfClass:[TMConversation class]]) {
            TMConversation *conversationMessage = [self.conversationList objectAtIndex:self.blockUserIndex];
            [self.msgManager sendBlockUserCall:[NSString stringWithFormat:@"%ld",(long)conversationMessage.matchId]];
            [self.conversationList removeObjectAtIndex:self.blockUserIndex];
            [self reloadMessageListTable];
            //mark as block shown
            [self updateConversationAsBlocked:[conversationMessage getChatContext]];
        }
    }
    else if(alertView.tag == UNMATCH_ALERTVIEW_TAG) {
        if(buttonIndex == 1) {
            //execute unmatch call
            TMConversation *msgConversation = [self.conversationList objectAtIndex:self.unmatchIndex];
            [self unmatchConversation:msgConversation];
        }
    }
    else if(alertView.tag == SHARE_ALERTVIEW_TAG) {
        if(buttonIndex == 1) {
            [self trackShareProfileWithEventAction:@"ask_friend_confirm"];
            NSArray *objectsToShare = @[self.shareText,self.shareLink];
            TMCustomActivity* shareActivity = [[TMCustomActivity alloc] initWithText:self.shareText link:self.shareLink];
            UIActivityViewController* activityViewCon = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:@[shareActivity]];
            activityViewCon.excludedActivityTypes = @[UIActivityTypeAirDrop,UIActivityTypeAddToReadingList,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];
            
            [self presentViewController:activityViewCon animated:TRUE completion:^{
                
            }];
        }
        else {
            [self trackShareProfileWithEventAction:@"ask_friend_cancel"];
        }
    }
}

#pragma mark - SWTableViewDelegate Handlers
#pragma mark -
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.messageConvTableView indexPathForCell:cell];
    TMConversation *conversationMessage = [self.conversationList objectAtIndex:indexPath.row];
    switch (index) {
        case 0:
            if(conversationMessage.isMsTm) {
                [self archiveChatAtIndex:indexPath.row];
            }
            else if([[TMUserSession sharedInstance].user isUserFemale]) {
                //NSLog(@"share button was pressed");
                [self shareConversationProfileAtIndex:indexPath.row];
            }
            else {
                //NSLog(@"archive button was pressed");
                [self archiveChatAtIndex:indexPath.row];
            }
            break;
            
        case 1:
            if([[TMUserSession sharedInstance].user isUserFemale]) {
                //NSLog(@"archive button was pressed");
                [self archiveChatAtIndex:indexPath.row];
            }
            else {
                //NSLog(@"unmatvh button was pressed");
                [self showUnMatchAlertForIndex:indexPath.row];
            }
            break;
            
        case 2:
           // NSLog(@"unmatvh button was pressed");
            [self showUnMatchAlertForIndex:indexPath.row];
            break;
            
        default:
            break;
    }
}

#pragma mark - Action Handlers
#pragma mark -
-(void)unmatchConversation:(TMConversation*)conversationMessage {
    if([self.msgManager isNetworkReachable]) {
        [self.conversationList removeObjectAtIndex:self.unmatchIndex];
        [self reloadMessageListTable];
        
        [self.msgManager blockUserWithReason:@"Not interested anymore" withURL:conversationMessage.fullConvLink response:^(BOOL status) {
            //mark as block shown
            if(status) {
                [self updateConversationAsBlocked:[conversationMessage getChatContext]];
            }
        }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Cannot Unmatch %@",conversationMessage.fName] message:TM_INTERNET_NOTAVAILABLE_MSG delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        });
    }
}
-(void)showMessageConversation:(TMConversation*)messageConversation {
    [self.navigationController popToViewController:self animated:false];
    
    //show new conv
    dispatch_async(dispatch_get_main_queue(), ^{
        TMMessageConversationViewController *msgConvViewCon = [self messageConvViewControllerForConversation:messageConversation];
        msgConvViewCon.messageDelegate = self;
        [self.navigationController pushViewController:msgConvViewCon animated:false];
    });
}
-(void)showUnMatchAlertForIndex:(NSInteger)index {
    self.unmatchIndex = index;
    TMConversation *msgConversation = [self.conversationList objectAtIndex:self.unmatchIndex];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *genderText = ([[TMUserSession sharedInstance].user isUserFemale]) ? @"him" : @"her";
        NSString *message = [NSString stringWithFormat:@"This will clear all chat history and remove %@ permanently from your matches",genderText];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Unmatch %@",msgConversation.fName] message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        alertView.tag = UNMATCH_ALERTVIEW_TAG;
        [alertView show];
    });
}
-(void)archiveChatAtIndex:(NSInteger)index {
    TMConversation *msgConversation = [self.conversationList objectAtIndex:index];
    msgConversation.isArchived = TRUE;
    [[TMMessagingController sharedController] archiveConversation:msgConversation];
    [self getCachedMessageConversation];
    
    if(![TMDataStore retrieveBoolforKey:@"com.convlist.atoast"]) {
        [TMDataStore setBool:TRUE forKey:@"com.convlist.atoast"];
        [TMToastView showToastInParentView:self.messageConvTableView withText:@"Conversation archived, scroll down to view." withDuaration:1.5 presentationDirection:TMToastPresentationFromBottom];
    }
    
    [self trackArchiveChatAction];
}
-(void)shareConversationProfileAtIndex:(NSInteger)index {
    [self trackShareProfileWithEventAction:@"ask_friend_click"];
    TMConversation *msgConversation = [self.conversationList objectAtIndex:index];
    NSString *matchId = [NSString stringWithFormat:@"%ld",(long)msgConversation.matchId];
    [self configureViewForDataLoadingStartWithText:@"Sharing Profile..."];
    [self.msgManager getShareProfileURLForMatch:matchId responseBlock:^(NSDictionary *data) {
        [self configureViewForDataLoadingFinish];
        
        if(data) {
            NSString *alertMessage = data[@"alert_message"];
            self.shareText = data[@"share_message"];
            self.shareLink = data[@"link"];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Share Profile" message:alertMessage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
                alertView.tag = SHARE_ALERTVIEW_TAG;
                [alertView show];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:PROFILE_SHARE_ERROR_TITLE message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            });
        }
    }];
}

#pragma mark - TMMessageConversationViewControllerDelegate / Notification Handlers
#pragma mark -
-(void)reloadCachedConversation {
    [self getCachedMessageConversation];
}
-(void)refreshMessageList:(NSNotification*)notif {
    [self getCachedMessageConversation];
}
-(void)blockCurrentConversation {
    //[self updateConversationAsBlocked:[self.activeConversation getChatContext]];
    [self getCachedMessageConversation];
}
-(void)willUnmatchCurrentConversation {
    self.delayTime = 2;
    [self.conversationList removeObjectAtIndex:self.activeConversationIndex];
    [self reloadMessageListTable];
}
-(void)deleteSparkWithMatch:(TMMessageConfiguration*)configuration; {
    [[TMMessagingController sharedController] stopSparkTimer];
    [[TMMessagingController sharedController] deleteSpark:configuration.matchUserId hash:configuration.sparkHash];
    [self.navigationController popViewControllerAnimated:FALSE];
}

#pragma mark - Native ad View methods
#pragma mark -
- (void)addNativeAdsForMessageListScreen {
    [self.adManager getNativeAd];
}

#pragma mark - Conversation List Tutorial Handlers
#pragma mark -
-(void)tutorialAction:(UITapGestureRecognizer*)tapGesture {
    [TMDataStore setObject:[NSNumber numberWithBool:TRUE] forKey:@"istutorialshown"];
    [self.tutorialImageView removeFromSuperview];
    self.tutorialImageView = nil;
    [self.view bringSubviewToFront:self.messageConvTableView];
}
-(BOOL)isTuturialShown {
    BOOL status = [TMDataStore retrieveBoolforKey:@"istutorialshown"];
    return status;
}

#pragma mark - App Background Handler
#pragma mark -
-(void)appWillEnterForeground:(NSNotification*)notification {
    if(self.isTopMostViewController) {
        [self fetchConversationData];
    }
    
    //make call for native ads
    [self addNativeAdsForMessageListScreen];
}

#pragma mark - Tracking Handler
#pragma mark -
-(void)trackGetConversationListNetworkRequest {
    NSMutableDictionary *trackEventDict = [[NSMutableDictionary alloc] init];
    trackEventDict[@"screenName"] = @"TMMessageListViewController";
    trackEventDict[@"eventCategory"] = @"messages";
    trackEventDict[@"eventAction"] = @"page_load";
    self.msgManager.trackEventDictionary = trackEventDict;
}
-(void)trackArchiveChatAction {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"conversation_list";
    eventDictionary[@"eventAction"] = @"archive_chat";
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}
-(void)trackShareProfileWithEventAction:(NSString*)eventAction {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"conversation_list";
    eventDictionary[@"eventAction"] = eventAction;
    eventDictionary[@"status"] = @"matched";
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}

-(void)trackSparkStatus:(NSString*)status {
    NSDictionary *eventInfo = nil;
    if([status isEqualToString:@"seen"]) {
        eventInfo = @{@"match_id":@(self.spark.matchId),@"sparks_left":@(self.totalCachedSparkCount)};
    }
    else if([status isEqualToString:@"error"]) {
        eventInfo = @{@"match_id":@(self.spark.matchId),@"error":@"timeout",@"action":@"seen"};
    }
    else if([status isEqualToString:@"clicked"]) {
        NSInteger sparkRemainingTimeInSecs = self.spark.sparkExpiryTime;
        eventInfo = @{@"match_id":@(self.spark.matchId),@"sparks_left":@(self.totalCachedSparkCount),@"time_left":@(sparkRemainingTimeInSecs)};
    }
    
    [[TMAnalytics sharedInstance] trackSparkEvent:@"conv_list" status:status eventInfo:eventInfo];
}

//SeventyNine Native Ad Changes
- (void) showNativeAd:(NSDictionary *)adDictionary{
    
    NSMutableDictionary* nativeJsonDict = [NSMutableDictionary dictionaryWithDictionary:adDictionary];
    self.chatListAdDictionary = [nativeJsonDict mutableCopy];
    self.chatListAdCount = 1;
    self.isNativeAdShownTracked = NO;
    if(self.isTopMostViewController) {
        [self getCachedMessageConversation];
    }
    
}

@end

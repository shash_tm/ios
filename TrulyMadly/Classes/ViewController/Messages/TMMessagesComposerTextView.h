//
//  TMMessagesComposerTextView.h
//  TrulyMadly
//
//  Created by Ankit on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMMessagesComposerTextView : UITextView

@property (copy, nonatomic) NSString *placeHolder;

@property (strong, nonatomic) UIColor *placeHolderTextColor;

- (BOOL)hasText;

@end

//
//  TMStickerView.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 12/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSticker.h"

@protocol TMStickerViewDataSource <NSObject>

- (UIImage*) getImageForSticker:(TMSticker*) sticker;

@end

@protocol TMStickerViewDelegate <NSObject>

- (void) didSendSticker:(TMSticker*) sticker;
- (void) storeHDImageForSticker:(TMSticker*) sticker;
- (void) updateTimeStampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID;

@end

@interface TMStickerView : UIView

@property (nonatomic, strong) NSArray* stickerArray;

- (instancetype) initWithFrame:(CGRect)frame cellSize:(CGSize) cellSize delegate:(id<TMStickerViewDelegate>) delegate dataSource:(id<TMStickerViewDataSource>) dataSource;
- (void) reloadData;
@end

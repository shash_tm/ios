//
//  TMMessageListViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 20/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageListViewCell.h"
#import "UIColor+TMColorAdditions.h"
#import "TMConversation.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"
#import "TMUserSession.h"


@interface TMMessageListViewCell ()

@property(nonatomic,strong)UIView *seperatorView;
@property(nonatomic,strong)UIImageView *sparkIndicator;
@property(nonatomic,strong)UIImageView *selectTag;

@end


@implementation TMMessageListViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        
        CGRect rect = CGRectMake(0, 0, 60, 60);
        self.profileImgView = [[TMProfileImageView alloc] initWithFrame:rect];
        self.profileImgView.layer.cornerRadius = 60/2;
        self.profileImgView.clipsToBounds = TRUE;
        self.profileImgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.profileImgView];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.font = [UIFont boldSystemFontOfSize:16];
        self.nameLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.nameLabel];
        
        self.dateTextLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.dateTextLabel.textAlignment = NSTextAlignmentRight;
        //self.dateTextLabel.backgroundColor = [UIColor greenColor];
        [self.contentView addSubview:self.dateTextLabel];
        
        self.messageTextLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.messageTextLabel.textColor = [UIColor messgaeListSubTitleTextColor];
        self.messageTextLabel.numberOfLines = 0;
        //self.messageTextLabel.backgroundColor = [UIColor orangeColor];
        [self.contentView addSubview:self.messageTextLabel];
        
        
        self.seperatorView = [[UIView alloc] initWithFrame:CGRectZero];
        self.seperatorView.backgroundColor = [UIColor conversationListSeperatorColor];
        [self.contentView addSubview:self.seperatorView];
    }
    
    return self;
}
-(UIImageView*)selectTag {
    if(!_selectTag) {
        _selectTag = [[UIImageView alloc] initWithFrame:CGRectZero];
        _selectTag.image = [UIImage imageNamed:@"tmselectlabel"];
    }
    return _selectTag;
}
-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat imageViewHeight = 60;
    CGFloat totalHeight = CGRectGetHeight(self.contentView.bounds);
    CGFloat imageViewYPos = (totalHeight - imageViewHeight)/2;
    
    self.profileImgView.frame = CGRectMake(12, imageViewYPos, imageViewHeight, imageViewHeight);
    
    CGRect rect = self.profileImgView.frame;
    CGFloat xPosOffset = 15;
    CGFloat maxNameLabelWidth = CGRectGetWidth(self.frame) - (CGRectGetMaxX(rect)+130);
    NSDictionary *attributes = @{NSFontAttributeName:self.nameLabel.font};
    CGSize labelConstraintSize = CGSizeMake(maxNameLabelWidth, NSUIntegerMax);
    CGRect labelRect = [self.nameLabel.text boundingRectWithConstraintSize:labelConstraintSize attributeDictionary:attributes];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(rect)+xPosOffset,
                                      12,
                                      CGRectGetWidth(labelRect),
                                      28);
    
    self.dateTextLabel.frame = CGRectMake((CGRectGetMinX(self.nameLabel.frame) + maxNameLabelWidth), 5, 105, 16);
    
    CGFloat messageWidth = CGRectGetWidth(self.frame) - (CGRectGetMaxX(rect)+50);
    CGSize constraintSize = CGSizeMake(messageWidth, 40);
    NSDictionary *attDict = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGRect textRect = [self.messageTextLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:attDict];
    self.messageTextLabel.frame = CGRectMake(CGRectGetMaxX(rect)+xPosOffset,
                                             CGRectGetMaxY(self.nameLabel.frame),
                                             messageWidth,
                                             textRect.size.height);
    
    self.seperatorView.frame = CGRectMake(0,
                                          totalHeight - 0.7,
                                          self.frame.size.width, 0.7);
    
    if(self.sparkIndicator) {
        CGFloat width = 22;
        self.sparkIndicator.frame = CGRectMake(CGRectGetMaxX(self.profileImgView.frame) - (width),
                                               (CGRectGetMaxY(self.profileImgView.frame) - (width)),
                                               width, width);
    }
    
    if(self.selectTag.superview) {
        CGFloat selectTagWidth = 48;
        CGFloat selectTagHeight = 16;
        self.selectTag.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame)+10,
                                          CGRectGetMidY(self.nameLabel.frame) - (selectTagHeight/2),
                                          selectTagWidth, selectTagHeight);
    }
}

-(void)setMessageListData:(TMConversation*)msgConversation {
    NSString *lastPathComponent = [msgConversation.profileImageURL lastPathComponent];
    [self.profileImgView setDefaultImage];
    [self.profileImgView setImageFromURLString:msgConversation.profileImageURL];
    [self.profileImgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    
    self.nameLabel.text = msgConversation.fName;
    
    if(msgConversation.message && [msgConversation.message isKindOfClass:[NSString class]]) {
        self.messageTextLabel.text = [msgConversation.message trimBrTag];
    }
    else {
        self.messageTextLabel.text = @"";
    }
    
    self.dateTextLabel.text = [msgConversation messageFormattedTimestamp];
    
    if([msgConversation isUnreadMessageConversation]) {
        [self setCellStateForUnReadConversation];
    }
    else {
        [self setCellStateForReadConversation];
    }
    
    if(msgConversation.isSparkMatch) {
        [self addSparkIndicator];
    }
    else {
        [self removeSparkIndicator];
    }
    
    if(msgConversation.isSelectMember && [[TMUserSession sharedInstance] isSelectEnabled]) {
        [self.contentView addSubview:self.selectTag];
    }
    else {
        [self.selectTag removeFromSuperview];
        self.selectTag = nil;
    }
}


-(void)setCellStateForReadConversation {
    [self setLabelFontAndColorForReadMessage];
}

-(void)setCellStateForUnReadConversation {
    [self setLabelFontAndColorForUnReadMessage];
}
-(void)addSparkIndicator {
    if(!self.sparkIndicator) {
        self.sparkIndicator = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.sparkIndicator.image = [UIImage imageNamed:@"sparkconversation"];
        [self.contentView addSubview:self.sparkIndicator];
    }
}
-(void)removeSparkIndicator {
    [self.sparkIndicator removeFromSuperview];
    self.sparkIndicator.image = nil;
    self.sparkIndicator = nil;
}

-(void)setLabelFontAndColorForReadMessage {
    self.nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
    self.messageTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    self.messageTextLabel.textColor = [UIColor messgaeListSubTitleTextColor];
    self.dateTextLabel.textColor = [UIColor readConversationDateColor];
    self.dateTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
}

-(void)setLabelFontAndColorForUnReadMessage {
    self.nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
    self.messageTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f];
    self.messageTextLabel.textColor = [UIColor unreadConversationDateColor];
    self.dateTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    self.dateTextLabel.textColor = [UIColor unreadConversationDateColor];
}

@end

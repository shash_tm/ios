//
//  TMMessageListViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 20/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@class TMConversation;
@class TMProfileImageView;

@interface TMMessageListViewCell : SWTableViewCell

@property(nonatomic,strong)TMProfileImageView *profileImgView;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UIImageView *detailIndicatorImageView;
@property(nonatomic,strong)UILabel *messageTextLabel;
@property(nonatomic,strong)UILabel *dateTextLabel;

-(void)setMessageListData:(TMConversation*)msgConversation;
-(void)setCellStateForReadConversation;

-(void)setLabelFontAndColorForReadMessage;
-(void)setLabelFontAndColorForUnReadMessage;

@end

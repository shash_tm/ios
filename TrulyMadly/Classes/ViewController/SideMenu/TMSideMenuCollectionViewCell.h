//
//  TMSideMenuCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 08/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMSideMenuItem;

@interface TMSideMenuCollectionViewCell : UICollectionViewCell

-(void)setMenuData:(TMSideMenuItem*)menuItem;

@end

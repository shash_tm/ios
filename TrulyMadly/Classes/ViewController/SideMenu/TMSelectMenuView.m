//
//  TMSelectMenuView.m
//  TrulyMadly
//
//  Created by Ankit on 07/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectMenuView.h"

@interface TMSelectMenuView ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *actionLabel;
@property(nonatomic,strong)UILabel *descriptionLabel;

@end


@implementation TMSelectMenuView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        CGFloat width = [[UIScreen mainScreen] bounds].size.width*0.8;
        
        UIView *seperatorview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 0.5)];
        seperatorview.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:seperatorview];
        
        CGFloat imageViewWidth = 138;
        CGFloat imageViewHeight = 70;
        CGFloat imageViewYPos = 25;
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake((width - imageViewWidth)/2,
                                                                       imageViewYPos,
                                                                       imageViewWidth,
                                                                       imageViewHeight)];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.image = [UIImage imageNamed:@"selectunit"];
        [self addSubview:self.imageView];
        
        CGFloat actionLabelWidth = 200;
        self.actionLabel = [[UILabel alloc] initWithFrame:CGRectMake((width - actionLabelWidth)/2,
                                                                     CGRectGetMaxY(self.imageView.frame)+12,
                                                                     actionLabelWidth,
                                                                     44)];
        self.actionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        self.actionLabel.backgroundColor = [UIColor clearColor];
        self.actionLabel.textAlignment = NSTextAlignmentCenter;
        self.actionLabel.layer.borderWidth = 1.5;
        self.actionLabel.layer.borderColor = [UIColor colorWithRed:126.0f/255.0f green:38.0f/255.0f blue:135.0f/255.0f alpha:1].CGColor;
        self.actionLabel.layer.cornerRadius = 5;
        self.actionLabel.textColor = [UIColor colorWithRed:126.0f/255.0f green:38.0f/255.0f blue:135.0f/255.0f alpha:1];
        [self addSubview:self.actionLabel];
        
        
        CGFloat descriptionLabelXPos = 20;
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(descriptionLabelXPos,
                                                                          CGRectGetMaxY(self.actionLabel.frame)+5,
                                                                          width - (2*descriptionLabelXPos),
                                                                          20)];
        self.descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        self.descriptionLabel.textColor = [UIColor colorWithRed:255.0f/255.0f green:0 blue:0 alpha:1];
        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.descriptionLabel];
    }
    return self;
}

-(void)setCTALabelText:(NSString*)ctaText {
    self.actionLabel.text = ctaText;
}
-(void)setExpiryLabelText:(NSString*)expiryText {
    self.descriptionLabel.text = expiryText;
}

@end

//
//  TMSideMenuCollectionViewHeader.h
//  TrulyMadly
//
//  Created by Ankit on 17/07/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSideMenuCollectionViewHeader : UICollectionReusableView

-(void)setImageFromURLString:(NSString*)URLString;
-(void)setUserName:(NSString*)userName;
-(void)showSelectTag;
-(void)removeSelecTag;

@end

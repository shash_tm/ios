//
//  TMSideMenuCollectionViewHeader.m
//  TrulyMadly
//
//  Created by Ankit on 17/07/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSideMenuCollectionViewHeader.h"
#import "UIImageView+AFNetworking.h"


@interface TMSideMenuCollectionViewHeader ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)CAGradientLayer *gradient;
@property(nonatomic,strong)UILabel *textLabel;
@property(nonatomic,strong)UIImageView *editIcon;
@property(nonatomic,strong)UIImageView *selectTagImageView;

@end

@implementation TMSideMenuCollectionViewHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat bottomWhiteSpaceWidth = 24;
        CGFloat width = [[UIScreen mainScreen] bounds].size.width*0.8;
        CGFloat imageViewHeight = (CGRectGetHeight(frame) - bottomWhiteSpaceWidth);
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, imageViewHeight)];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.layer.masksToBounds = YES;
        self.imageView.image = [UIImage imageNamed:@"SidemenuPlaceholder"];
        [self addSubview:self.imageView];
        
        
        if (!self.gradient.superlayer) {
            self.gradient = [CAGradientLayer layer];
            self.gradient.contents = (id)[UIImage imageNamed:@"gradient_white"].CGImage;
            CGFloat height = 151;
            self.gradient.frame = CGRectMake(0,
                                             CGRectGetHeight(self.imageView.frame)-height,
                                             CGRectGetWidth(self.imageView.frame),
                                             height);
           [self.imageView.layer insertSublayer:self.gradient atIndex:0];
        }
        
        CGFloat labelHeight = 20;
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,
                                                                   CGRectGetMaxY(self.imageView.frame) - (labelHeight+18),
                                                                   width,
                                                                   labelHeight)];
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.textAlignment = NSTextAlignmentLeft;
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:20.0];
        [self addSubview:self.textLabel];
        
        CGFloat iconWidth = 16;
        self.editIcon = [[UIImageView alloc] initWithFrame:CGRectMake(width-30,
                                                                      CGRectGetMaxY(self.imageView.frame) - (iconWidth+20),
                                                                      iconWidth, iconWidth)];
        self.editIcon.backgroundColor = [UIColor clearColor];
        [self addSubview:self.editIcon];
        self.editIcon.image = [UIImage imageNamed:@"edit_without_circle"];
    }
    return self;
}

-(UIImageView*)selectTagImageView {
    if(!_selectTagImageView) {
        _selectTagImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.imageView.frame) - 74, 20, 74, 28)];
        _selectTagImageView.image = [UIImage imageNamed:@"selectlabel"];
    }
    return _selectTagImageView;
}
-(void)setUserName:(NSString*)userName {
    self.textLabel.text = userName;
}
-(void)setImageFromURLString:(NSString*)URLString {
    [self.imageView setImageWithURL:[NSURL URLWithString:URLString]];
}
-(void)showSelectTag {
    [self addSubview:self.selectTagImageView];
}
-(void)removeSelecTag {
    [self.selectTagImageView removeFromSuperview];
    self.selectTagImageView = nil;
}

@end

//
//  TMSideMenuViewController.m
//  TrulyMadly
//
//  Created by Ankit on 08/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMSideMenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMSideMenuCollectionViewCell.h"
#import "TMSideMenuCollectionViewHeader.h"
#import "TMMyProfileViewController.h"
#import "TMSlidingViewController.h"
#import "TMMatchesViewController.h"
#import "TMFeedbackViewController.h"
#import "TMFaqViewController.h"
#import "TMAppViralityController.h"
#import "TMMatchesViewController.h"
#import "TMUserSession.h"
#import "TMSideMenuItem.h"
#import "TMSwiftHeader.h"
#import "TMSelectQuizViewController.h"
#import "TMSelectMenuView.h"
#import "TMBuySelectViewController.h"
#import "TMEventCategoryListViewController.h"
#import "TMEventListViewController.h"
#import "TMLocationPreferenceViewController.h"
#import "TMEditPreferenceViewController.h"


@interface TMSideMenuViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,TMMyProfileViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *content;
@property (nonatomic, strong) UICollectionView *sideMenuCollectionView;
@property (nonatomic, strong) TMSelectMenuView *selectMenuView;
@property (nonatomic, weak) UINavigationController *mainNavigationController;

@end

@implementation TMSideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.content = [NSMutableArray array];
  
    self.mainNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
    [self setupMenuView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupSelectView];
    [self setupSelectData];
    [self setupSideMenuContent];
    [self.sideMenuCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupSelectData {
    [self.selectMenuView setCTALabelText:[[TMUserSession sharedInstance].user selectSideMenuCta]];
    [self.selectMenuView setExpiryLabelText:[[TMUserSession sharedInstance].user selectExpiryDaysLeftText]];
}
-(void)setupSelectView {
    CGRect currentViewBound = self.view.bounds;
    if([[TMUserSession sharedInstance] isSelectEnabled]) {
        CGFloat height = 180;
        if(!self.selectMenuView.superview) {
            self.selectMenuView = [[TMSelectMenuView alloc] initWithFrame:CGRectZero];
            [self.view addSubview:self.selectMenuView];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAction:)];
            [self.selectMenuView addGestureRecognizer:tapGesture];
        }
        self.selectMenuView.frame = CGRectMake(0,
                                               CGRectGetHeight(currentViewBound) - height,
                                               CGRectGetWidth(currentViewBound),
                                               height);
        CGFloat yPos = 20;
        CGRect rect = CGRectMake(currentViewBound.origin.x,
                                 yPos,
                                 currentViewBound.size.width,
                                 currentViewBound.size.height - (yPos + CGRectGetHeight(self.selectMenuView.frame)));
        self.sideMenuCollectionView.frame = rect;
    }
    else {
        [self.selectMenuView removeFromSuperview];
        self.selectMenuView = nil;
        
        CGFloat yPos = 20;
        CGRect rect = CGRectMake(currentViewBound.origin.x,
                                 yPos,
                                 currentViewBound.size.width,
                                 currentViewBound.size.height - (yPos + CGRectGetHeight(self.selectMenuView.frame)));
        
        self.sideMenuCollectionView.frame = rect;
    }
}
-(void)setupMenuView {
    CGRect currentViewBound = self.view.bounds;

    CGFloat yPos = 20;
    CGRect rect = CGRectMake(currentViewBound.origin.x,
                             yPos,
                             currentViewBound.size.width,
                             currentViewBound.size.height - (yPos + CGRectGetHeight(self.selectMenuView.frame)));
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 1;
    flowLayout.minimumInteritemSpacing = 1;
    
    self.sideMenuCollectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.sideMenuCollectionView.backgroundColor = [UIColor clearColor];
    self.sideMenuCollectionView.dataSource = self;
    self.sideMenuCollectionView.delegate = self;
    [self.view addSubview:self.sideMenuCollectionView];
    
    [self.sideMenuCollectionView registerClass:[TMSideMenuCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [self.sideMenuCollectionView registerClass:[TMSideMenuCollectionViewHeader class]
                    forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerview"];
}
-(void)setupSideMenuContent {
    [self.content removeAllObjects];
    
    NSArray *sideMenuOptions = @[@(SPARK),@(SCENES),@(PREFERENCES),@(REFER_AND_EARN),@(SETTINGS)];
    for (int menuCount=0; menuCount<sideMenuOptions.count; menuCount++) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        TMSideMenuType menuType = [sideMenuOptions[menuCount] integerValue];
        if(menuType == REFER_AND_EARN) {
            BOOL status = FALSE;//[[TMUserSession sharedInstance] isAppViralityEnabled];
            if(status == FALSE)
                continue;
        }
        else if(menuType == SPARK) {
            BOOL status = [[TMUserSession sharedInstance] isSparkEnabled];
            if(status == FALSE)
                continue;
        }
        else if(menuType == SCENES) {
            BOOL status = [userSession isMeetUpIconShown];
            if(status == FALSE)
                continue;
        }
        
        TMSideMenuItem *menuItem = [[TMSideMenuItem alloc] initWithSideMenuType:menuType];
        [self.content addObject:menuItem];
    }
}


#pragma mark - uicollectionview datasource / delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.content.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.view.frame.size.width, 60);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeMake(self.view.frame.size.width, 204);
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusableIdentifier = @"cell";
    
    TMSideMenuCollectionViewCell *cell = [collectionView
                                         dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                         forIndexPath:indexPath];

    TMSideMenuItem *menuItem = self.content[indexPath.row];
    [cell setMenuData:menuItem];
    return  cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    TMSideMenuCollectionViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerview" forIndexPath:indexPath];
    TMUser *user = [TMUserSession sharedInstance].user;
    [headerView setUserName:user.fName];
    [headerView setImageFromURLString:user.profilePicURL];
    if([[TMUserSession sharedInstance].user isSelectUser] && [[TMUserSession sharedInstance] isSelectEnabled]) {
        [headerView showSelectTag];
    }
    else {
        [headerView removeSelecTag];
    }
    if(!headerView.gestureRecognizers) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerSelected:)];
        [headerView addGestureRecognizer:tapGesture];
    }
    return headerView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.slidingViewController resetTopViewAnimated:NO];
    
    TMSideMenuItem *menuItem = self.content[indexPath.row];
    TMSideMenuType menuType =  menuItem.sideMenuType;
    if(menuType == PREFERENCES) {
        TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
        TMEditPreferenceViewController *editPreferenceViewCon = [[TMEditPreferenceViewController alloc] init];
        editPreferenceViewCon.actionDelegate = slidingVC.actionDelegate;
        [self.mainNavigationController pushViewController:editPreferenceViewCon animated:FALSE];
    }
    if(menuType == REFER_AND_EARN) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        [userSession registerReferBlooperShown];
        [TMAppViralityController showGrowthHackScreenFromController:self.mainNavigationController];
    }
    else if(menuType == SPARK) {
        [self.delegate didClickTMFrills];
    }
    else if(menuType == HELPDESK) {
        TMFaqViewController *faqVC = [[TMFaqViewController alloc] init];
        [self.mainNavigationController pushViewController:faqVC animated:FALSE];
    }
    else if(menuType == SETTINGS) {
        TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
        TMSettingsViewController *settingsViewCon = [[TMSettingsViewController alloc] init];
        settingsViewCon.actionDelegate = slidingVC.actionDelegate;
        [self.mainNavigationController pushViewController:settingsViewCon animated:FALSE];
    }
    else if(menuType == SELECT) {
        TMSelectQuizViewController *settingsViewCon = [[TMSelectQuizViewController alloc] init];
        [self.mainNavigationController presentViewController:settingsViewCon animated:TRUE completion:nil];
    }
    else if(menuType == SCENES) {
        NSString *sceneCategoryIds = [[TMUserSession sharedInstance] getSceneCategoryIds];
        if(sceneCategoryIds) {
            [self launchEventListWithCategoryId:sceneCategoryIds];
        }
        else {
            TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
            TMEventCategoryListViewController *eventListViewCon = [[TMEventCategoryListViewController alloc] init];
            //eventListViewCon.delegate = self;
            eventListViewCon.actionDelegate = slidingVC.actionDelegate;
            [self.mainNavigationController pushViewController:eventListViewCon animated:FALSE];
        }
    }
}
-(void)headerSelected:(UITapGestureRecognizer*)tapGestureRecognizer
{
    TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
    TMMyProfileViewController *myProfileViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"myprofilevc"];
    myProfileViewCon.actionDelegate = slidingVC.actionDelegate;
    [self.mainNavigationController pushViewController:myProfileViewCon animated:NO];
}
-(void)selectAction:(UITapGestureRecognizer*)tapGestureRecognizer {
    //TMSelectQuizViewController *quizViewCon = [[TMSelectQuizViewController alloc] init];
    //[self.mainNavigationController presentViewController:quizViewCon animated:TRUE completion:nil];
    ///////////
    [self launchBuySelectViewControllerFromSource:@"menu"];
    [self trackSelectMenuClick];
}
-(void)launchMyProfileViewController:(BOOL)fromNudge {
    TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
    TMMyProfileViewController *myProfileViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"myprofilevc"];
    myProfileViewCon.actionDelegate = slidingVC.actionDelegate;
    myProfileViewCon.from_nudge = fromNudge;
    [self.mainNavigationController pushViewController:myProfileViewCon animated:NO];
    
    [self.slidingViewController resetTopViewAnimated:NO];
}

-(void)launchEditPreferencesViewController {
    TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
    TMEditPreferenceViewController *editPreferenceViewCon = [[TMEditPreferenceViewController alloc] init];
    editPreferenceViewCon.actionDelegate = slidingVC.actionDelegate;
    [self.mainNavigationController pushViewController:editPreferenceViewCon animated:FALSE];
}

-(void)launchBuySelectViewControllerFromSource:(NSString*)source {
    TMBuySelectViewController *buyViewController = [[TMBuySelectViewController alloc] initWithSource:source];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:buyViewController];
    [self.mainNavigationController presentViewController:navigationController animated:TRUE completion:nil];
}
-(void)launchEventListWithCategoryId:(NSString*)categoryId {
    TMEventListViewController *listViewCon = [[TMEventListViewController alloc] initWithCategoryId:categoryId
                                                                                             title:@"TM Mixers"];
    TMLocationPreferenceViewController *locView = [[TMLocationPreferenceViewController alloc] initWithGeo:true];
    
    UINavigationController *navViewCon = [[UINavigationController alloc] initWithRootViewController:listViewCon];
    ECSlidingViewController *viewCon = [[ECSlidingViewController alloc] initWithTopViewController:navViewCon];
    viewCon.underRightViewController = locView;
    viewCon.anchorLeftRevealAmount = self.view.bounds.size.width-60;
    self.mainNavigationController.navigationBarHidden = TRUE;
    [self.mainNavigationController pushViewController:viewCon animated:TRUE];
}
-(void)launchFeedbackViewController {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        TMFaqViewController *faqVC = [[TMFaqViewController alloc] init];
        [self.mainNavigationController pushViewController:faqVC animated:NO];
        
        TMFeedbackViewController *feedbackVC = [[TMFeedbackViewController alloc] init];
        [faqVC.navigationController pushViewController:feedbackVC animated:NO];
        
        [self.slidingViewController resetTopViewAnimated:NO];
    });
}

-(BOOL)isFeedbackViewControllerActive {
    UIViewController *topVC = [self.mainNavigationController topViewController];
    if( (topVC) && ([topVC isKindOfClass:[TMFeedbackViewController class]]) ) {
        return TRUE;
    }
    return FALSE;
}

////
-(void)trackSelectMenuClick {
    BOOL isSelectMember = [[TMUserSession sharedInstance].user isSelectUser];
    NSString *membershipStatus = (isSelectMember) ? @"true" : @"false";
    NSString *expiryStatus = nil;
    if(isSelectMember) {
        NSInteger expiryDaysLeft = [[TMUserSession sharedInstance].user selectExpiryDaysLeft];
        expiryStatus = [NSString stringWithFormat:@"%ld",(long)expiryDaysLeft];
    }
    
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    eventInfo[@"is_select_member"] = membershipStatus;
    if(expiryStatus) {
        eventInfo[@"expiry_in_days"] = expiryStatus;
    }
    
    [[TMAnalytics sharedInstance] trackSelectEvent:@"tm_select_menu_clicked" status:@"view" eventInfo:eventInfo];
}


@end

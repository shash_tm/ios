//
//  TMSideMenuCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 08/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMSideMenuCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMSideMenuItem.h"
#import "TMUserSession.h"

#define SIDEMENUWIDTH [[UIScreen mainScreen] bounds].size.width*0.8

@interface TMSideMenuCollectionViewCell ()

@property(nonatomic,strong)UIImageView *menuIcon;
@property(nonatomic,strong)UILabel *menuTitle;
@property(nonatomic,strong)UIImageView *blooperImage;

@end

@implementation TMSideMenuCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        self.menuIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.menuIcon];
        
        CGFloat labelHeight  = 30;
        self.menuTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                   (CGRectGetHeight(frame) - labelHeight)/2,
                                                                   200,
                                                                   labelHeight)];
        self.menuTitle.backgroundColor = [UIColor clearColor];
        self.menuTitle.textColor = [UIColor blackColor];//[UIColor colorWithRed:92/255.0f green:92/255.0f blue:92/255.0f alpha:1.0];
        self.menuTitle.textAlignment = NSTextAlignmentLeft;
        self.menuTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
        [self.contentView addSubview:self.menuTitle];
        
        /*
        self.blooperImage = [[UIImageView alloc] initWithFrame:CGRectMake(33, 14, 10, 10)];
        self.blooperImage.image = [UIImage imageNamed:@"notification"];
        self.blooperImage.hidden = TRUE;
        [self.contentView addSubview:self.blooperImage];
         */
    }
    
    return self;
}

-(void)setMenuData:(TMSideMenuItem*)menuItem {
    self.menuTitle.text = menuItem.titleText;
    self.menuIcon.image = [UIImage imageNamed:menuItem.imageName];
    self.menuIcon.frame = CGRectMake(20,
                                     (CGRectGetHeight(self.frame) - menuItem.imageViewSize.height)/2,
                                     menuItem.imageViewSize.width,
                                     menuItem.imageViewSize.height);
    self.menuTitle.frame = CGRectMake(CGRectGetMaxX(self.menuIcon.frame)+12,
                                      CGRectGetMinY(self.menuTitle.frame),
                                      CGRectGetWidth(self.menuTitle.frame),
                                      CGRectGetHeight(self.menuTitle.frame));
    /*
    BOOL showBlooper = FALSE;
    if(menuItem.sideMenuType == REFER_AND_EARN) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        if(![userSession isReferBlooperShown]){
            showBlooper = TRUE;
        }else {
            showBlooper = FALSE;
        }
    }
    self.blooperImage.hidden = (showBlooper) ? FALSE : TRUE;
    */
}

@end

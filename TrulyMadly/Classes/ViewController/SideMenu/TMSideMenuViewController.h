//
//  TMSideMenuViewController.h
//  TrulyMadly
//
//  Created by Ankit on 08/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSideMenuViewControllerDelegate;

@interface TMSideMenuViewController : UIViewController

@property(nonatomic,weak)id<TMSideMenuViewControllerDelegate> delegate;

-(void)launchFeedbackViewController;
-(BOOL)isFeedbackViewControllerActive;
-(void)launchMyProfileViewController:(BOOL)fromNudge;
-(void)launchEventListWithCategoryId:(NSString*)categoryId;
-(void)launchBuySelectViewControllerFromSource:(NSString*)source;
-(void)launchEditPreferencesViewController;
@end

@protocol TMSideMenuViewControllerDelegate <NSObject>

-(void)didClickTMFrills;

@end

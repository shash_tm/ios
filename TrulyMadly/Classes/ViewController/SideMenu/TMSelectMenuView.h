//
//  TMSelectMenuView.h
//  TrulyMadly
//
//  Created by Ankit on 07/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSelectMenuView : UIView

-(void)setCTALabelText:(NSString*)ctaText;
-(void)setExpiryLabelText:(NSString*)expiryText;

@end

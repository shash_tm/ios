//
//  TMDismissAnimator.m
//  TrulyMadly
//
//  Created by Ankit on 11/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMDismissAnimator.h"

@implementation TMDismissAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.6;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *containerView = [transitionContext containerView];
    
    [containerView insertSubview:toVC.view belowSubview:fromVC.view];
    
    CGRect screenBounds =  UIScreen.mainScreen.bounds;
    CGPoint bottomLeftCorner = CGPointMake(0, screenBounds.size.height);
    CGRect finalFrame = CGRectMake(bottomLeftCorner.x, bottomLeftCorner.y, screenBounds.size.width, screenBounds.size.height);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromVC.view.frame = finalFrame;
        
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}

@end

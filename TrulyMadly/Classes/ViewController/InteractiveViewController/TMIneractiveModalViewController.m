//
//  TMIneractiveModalViewController.m
//  TrulyMadly
//
//  Created by Ankit on 11/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMIneractiveModalViewController.h"
#import "TMInteractor.h"

@interface TMIneractiveModalViewController ()

@end

@implementation TMIneractiveModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupGestureRecognizer {
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.view addGestureRecognizer:panGesture];
}

-(void)handleGesture:(UIPanGestureRecognizer*) pangesture {
    CGFloat percentThreshold = 0.3;
    // convert y-position to downward pull progress (percentage)
    CGPoint translation = [pangesture translationInView:self.view];
    CGFloat verticalMovement = translation.y / CGRectGetHeight(self.view.frame);
    CGFloat downwardMovement = fmaxf((verticalMovement), 0.0);
    CGFloat downwardMovementPercent = fminf(downwardMovement, 1.0);
    CGFloat progress = (CGFloat)downwardMovementPercent;
    
    switch (pangesture.state) {
        case UIGestureRecognizerStateBegan:
            self.interactor.hasStarted = TRUE;
            [self dismissViewControllerAnimated:TRUE completion:nil];
            break;
            
        case UIGestureRecognizerStateChanged:
            self.interactor.shouldFinish = (progress > percentThreshold);
            [self.interactor updateInteractiveTransition:progress];
            break;
        case UIGestureRecognizerStateCancelled:
            self.interactor.hasStarted = FALSE;
            [self.interactor cancelInteractiveTransition];
            break;
            
        case UIGestureRecognizerStateEnded:
            self.interactor.hasStarted = FALSE;
            if(self.interactor.shouldFinish) {
                [self.interactor finishInteractiveTransition];
            }
            else {
                [self.interactor cancelInteractiveTransition];
            }
            break;
            
        default:
            break;
    }
}

@end

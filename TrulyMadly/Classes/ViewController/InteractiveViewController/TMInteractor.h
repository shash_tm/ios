//
//  TMInteractor.h
//  TrulyMadly
//
//  Created by Ankit on 11/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMInteractor : UIPercentDrivenInteractiveTransition

@property(nonatomic,assign)BOOL hasStarted;
@property(nonatomic,assign)BOOL shouldFinish;

@end

//
//  TMIneractiveModalViewController.h
//  TrulyMadly
//
//  Created by Ankit on 11/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMInteractor;

@interface TMIneractiveModalViewController : UIViewController

@property(nonatomic,strong)TMInteractor *interactor;

@end

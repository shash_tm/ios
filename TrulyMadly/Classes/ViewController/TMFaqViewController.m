//
//  TMFaqViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMFaqViewController.h"
#import "TMBuildConfig.h"
#import "TMFeedbackViewController.h"
#import "TrulyMadly-Swift.h"
#import "TMAnalytics.h"
#import "TMErrorMessages.h"

@interface TMFaqViewController()<UIWebViewDelegate>

@property(nonatomic,strong)UIWebView* webView;
@property(nonatomic,strong)UIButton* contactButton;
@property(nonatomic,assign) BOOL canShowLoader;

@end

@implementation TMFaqViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"FAQs";
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-140)];
    _webView.delegate = self;
    _webView.scrollView.showsHorizontalScrollIndicator = FALSE;
    _webView.hidden = true;
    [self.view addSubview:_webView];
    
    self.canShowLoader = true;
    
    NSString *faqUrl = [TM_BASE_URL stringByAppendingPathComponent:KAPI_FAQ_URL];
    [self addWebViewWithUrl:faqUrl];
    
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    
    //adding tracking for the fb button click
    [eventDict setObject:@"TMFaqViewController" forKey:@"screenName"];
    [eventDict setObject:@"faq" forKey:@"eventCategory"];
    [eventDict setObject:@"page_load" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
    //[self addWebViewWithUrl:@"http://trulymadly.clicksbazaar.com"];
    
}

-(void)addWebViewWithUrl:(NSString*)faqUrl
{
    NSURL* url = [[NSURL alloc] initWithString:faqUrl];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    //NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    [_webView loadRequest:request];
}

//web view delegates
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(!webView.isLoading) {
        [self removeLoader];
        self.webView.hidden = false;
        [self addBottomView];
    }
}

-(void)webViewDidStartLoad:(UIWebView*)webView
{
    [self addLoader];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self removeLoader];
    self.webView.hidden = false;
    [self showAlertWithTitle:@"" msg:TM_INTERNET_NOTAVAILABLE_MSG withDelegate:nil];
}

-(void)addLoader
{
    if(self.canShowLoader) {
        [self showActivityIndicatorViewWithMessage:@"Loading..."];
        self.canShowLoader = false;
    }
}

-(void)removeLoader
{
    [self hideActivityIndicatorView];
}

-(void)addBottomView
{
    if(_contactButton == nil) {
        _contactButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _contactButton.frame = CGRectMake(10, self.view.frame.size.height-55, self.view.bounds.size.width-20, 44);
        _contactButton.titleLabel.font = [UIFont systemFontOfSize:15.0 ];
        [_contactButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
        [_contactButton setTitle:@"Contact Us" forState:UIControlStateNormal];
        _contactButton.layer.cornerRadius = 10;
        _contactButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
        [_contactButton addTarget:self action:@selector(didContactClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_contactButton];
    }
}

-(void)didContactClick {
    TMFeedbackViewController *feedbackVC = [[TMFeedbackViewController alloc] init];
    [self.navigationController pushViewController:feedbackVC animated:FALSE];
}

@end

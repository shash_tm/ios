//
//  TMQuizOverlayQuizDescCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMOverlayBaseCollectionViewCell.h"

@interface TMQuizOverlayQuizDescCollectionViewCell : TMOverlayBaseCollectionViewCell

@end

//
//  TMFavQuizOptionListView.m
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFavQuizOptionListView.h"
#import "TMPersonality.h"
#import "TMAnalytics.h"
#import "TMLog.h"

@interface TMFavQuizOptionListView ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)UITableView *favOptionsView;
@property(nonatomic,strong)NSMutableArray *contents;
@property(nonatomic,strong)TMPersonality *personalityMgr;
@property(nonatomic,assign)BOOL requestInProgress;
@property(nonatomic,assign)NSString* eventCategory;


@end


@implementation TMFavQuizOptionListView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.contents = [NSMutableArray arrayWithCapacity:16];
        self.favOptionsView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        self.favOptionsView.dataSource = self;
        self.favOptionsView.delegate = self;
        [self addSubview:self.favOptionsView];
        
        UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-0, 1)];
        seperatorImgView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:seperatorImgView];
        
        self.eventCategory = @"dictionary";

    }
    
    return self;
}
-(void)cleanup {
    self.personalityMgr = nil;
    [self.contents removeAllObjects];
}
-(void)layoutSubviews {
    self.favOptionsView.frame = self.bounds;
}
-(void)emptyFavAutoCopmpleteList {
    [self.contents removeAllObjects];
    [self.favOptionsView reloadData];
}

-(void)fetchFavOptionListForText:(NSString*)text withCategory:(NSString*)category{
    //if(!self.requestInProgress) {
        self.requestInProgress = true;
        
        if(!self.personalityMgr) {
            self.personalityMgr = [[TMPersonality alloc] init];
        }
        [self.favOptionsView reloadData];
    
        self.personalityMgr.trackEventDictionary = [self dictForTracking];
        [self.personalityMgr getFavQuizListForKey:text withCategory:category
                                     withResponse:^(NSDictionary *response, TMError *error) {
                                         
                                         TMLOG(@"favlist:%@",response);
                                         self.requestInProgress = false;
                                         if(response) {
                                             [self setSearchValuesFromRespose:response withCategory:category];
                                             [self.favOptionsView reloadData];
                                             [self.delegate didReceiveResponse];
                                         }
                                         else {
                                             //NSLog(@"Error in fetching fav auto complete list:%@",error);
                                         }
                                     }];
   // }
}

- (void)setSearchValuesFromRespose:(NSDictionary *)response withCategory:(NSString *)category {
    
    NSDictionary *responseDict;
    NSArray *searchData;
    responseDict = response[@"pages"];
    searchData = responseDict[category];
    [self.contents removeAllObjects];
    [self.contents addObjectsFromArray:searchData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(self.requestInProgress) {
        return @"Searching...";
    }
    return @"";
}

-(void)reloadTableViewWithData:(NSArray*)data {
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contents.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reusableIdentifier = @"123";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reusableIdentifier];
    }
    NSDictionary *dict = [self.contents objectAtIndex:indexPath.row];

    cell.textLabel.text = [dict objectForKey:@"name"];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    //cell.detailTextLabel.text = [dict objectForKey:@"notable_name"];
    //cell.detailTextLabel.textColor = [UIColor grayColor];
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [self.contents objectAtIndex:indexPath.row];
    [self.delegate didSelectFavOption:dict];
}

- (NSMutableDictionary*)dictForTracking {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc ] init];
    [dict setObject:@"TMFavQuizOptionListView" forKey:@"screenName"];
    [dict setObject:self.eventCategory forKey:@"eventCategory"];
    [dict setObject:@"server_call" forKey:@"eventAction"];
    
    return dict;
}

@end

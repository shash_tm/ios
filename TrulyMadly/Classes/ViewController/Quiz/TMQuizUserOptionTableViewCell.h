//
//  TMQuizUserOptionTableViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizOptionDomainObject.h"

@interface TMQuizUserOptionTableViewCell : UITableViewCell

- (void) fillUserOptionDetails:(QuizOptionDomainObject*) optionDomainObject profilePicURL:(NSString*) profilePicURL isAnswerCommon:(BOOL) isAnswerCommon isUserAnswer:(BOOL) isUserAnswer isFemaleGender:(BOOL) isFemaleGender;

@end

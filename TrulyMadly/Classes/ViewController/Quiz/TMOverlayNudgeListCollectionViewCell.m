//
//  TMOverlayNudgeListCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 16/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMOverlayNudgeListCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

typedef enum {
    nudgeSelected,
    nudgeNotSelected
} nudgeSelectionState;

@interface TMOverlayNudgeListCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *userSelectionButton;
@property (assign, nonatomic) nudgeSelectionState currentSelectionState;
@property (strong, nonatomic) NSIndexPath* indexPath;
@property (strong, nonatomic) NSString* matchID;

//state capturing
@property (nonatomic, assign) BOOL isPlayed;
@property (nonatomic, strong) NSString* nudgeUserName;
@property (nonatomic, strong) NSString* profilePicImageURL;

@end

#define KEY_IS_PLAYED @"is_played"
#define KEY_MATCH_ID @"match_id"
#define KEY_USER_NAME @"name"
#define KEY_PROFILE_PIC_IMAGE_URL @"profile_pic"

@implementation TMOverlayNudgeListCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    
    //set up the profile image view
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.bounds.size.width/2;
    self.clipsToBounds = YES;
    self.userProfileImageView.layer.borderColor = [UIColor blackColor].CGColor;
    self.userProfileImageView.clipsToBounds = YES;
}

- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary
{
    //point of customization
    
    //info required
    //is_played, match_id, name, profile_pic
    self.isPlayed = ([questionOptionsDictionary valueForKey:KEY_IS_PLAYED] && ![[questionOptionsDictionary valueForKey:KEY_IS_PLAYED] isKindOfClass:[NSNull class]])?[[questionOptionsDictionary valueForKey:KEY_IS_PLAYED] boolValue]:NO;
    
    self.matchID = ([questionOptionsDictionary valueForKey:KEY_MATCH_ID] && ![[questionOptionsDictionary valueForKey:KEY_MATCH_ID] isKindOfClass:[NSNull class]])?[questionOptionsDictionary valueForKey:KEY_MATCH_ID]:nil;
    
    self.nudgeUserName = ([questionOptionsDictionary valueForKey:KEY_USER_NAME] && ![[questionOptionsDictionary valueForKey:KEY_USER_NAME] isKindOfClass:[NSNull class]])?[questionOptionsDictionary valueForKey:KEY_USER_NAME]:nil;
    
    self.profilePicImageURL = ([questionOptionsDictionary valueForKey:KEY_PROFILE_PIC_IMAGE_URL] && ![[questionOptionsDictionary valueForKey:KEY_PROFILE_PIC_IMAGE_URL] isKindOfClass:[NSNull class]])?[questionOptionsDictionary valueForKey:KEY_PROFILE_PIC_IMAGE_URL]:nil;
    
    self.userProfileImageView.layer.borderColor = [UIColor colorWithRed:122.0/255.0 green:136.0/255.0 blue:166.0/255.0 alpha:1.0].CGColor;
    
 //   if (self.indexPath.row == 0) {
      
        //highlight the first user
        UIColor* highlighedUserColor = [UIColor colorWithRed:102.0/255.0 green:125.0/255.0 blue:195.0/255.0 alpha:1.0];
        self.userNameLabel.textColor = highlighedUserColor;
        self.userNameLabel.font = [UIFont boldSystemFontOfSize:16];
        self.userProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.userProfileImageView.layer.borderWidth = 0.8f;
    
 CAShapeLayer* borderLayer = [CAShapeLayer layer];
    borderLayer.strokeColor = [UIColor colorWithRed:211.0/255.0 green:221.0/255.0 blue:242.0/255.0 alpha:1.0].CGColor;
    borderLayer.fillColor = nil;
 //   borderLayer.lineDashPattern = @[@4, @2];
    [self.layer addSublayer:borderLayer];
    
    borderLayer.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    borderLayer.frame = self.bounds;
//    }
//    else {
//        
//        //show normal UI for other users
//        self.userNameLabel.textColor = [UIColor blackColor];
//        self.userNameLabel.font = [UIFont systemFontOfSize:16];
//        self.userProfileImageView.layer.borderWidth = 0;
//    }
    
    self.userNameLabel.text = self.nudgeUserName;

    [self.userProfileImageView setImageWithURL:[NSURL URLWithString:self.profilePicImageURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
}

- (void) setUpCellWithIndexPath:(NSIndexPath*) indexPath
{
    self.indexPath = indexPath;
    
    //set default selected state
    [self.userSelectionButton setImage:[UIImage imageNamed:@"check on"] forState:UIControlStateNormal];
    self.currentSelectionState = nudgeSelected;
    self.userProfileImageView.image = nil;
}

- (void) setNudgeSelectionState:(BOOL) isSelected
{
    if (isSelected) {
        //set selected state
        [self.userSelectionButton setImage:[UIImage imageNamed:@"check on"] forState:UIControlStateNormal];
        self.currentSelectionState = nudgeSelected;
    }
    else {
        //set non selected state
        [self.userSelectionButton setImage:[UIImage imageNamed:@"check off"] forState:UIControlStateNormal];
        self.currentSelectionState = nudgeNotSelected;
    }
}

- (IBAction)nudgeButtonPressed:(UIButton *)sender {
    if (self.currentSelectionState == nudgeNotSelected) {
        [self.userSelectionButton setImage:[UIImage imageNamed:@"check on"] forState:UIControlStateNormal];
        self.currentSelectionState = nudgeSelected;
    }
    else {
        [self.userSelectionButton setImage:[UIImage imageNamed:@"check off"] forState:UIControlStateNormal];
        self.currentSelectionState = nudgeNotSelected;
    }
    [self.delegate nudgePressedForMatchID:self.matchID];
}

@end

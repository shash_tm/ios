//
//  TMOverlayBaseCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMOverlayBaseCollectionViewCell : UICollectionViewCell

/**
 * fill details for the question text
 * @param question info dictionary
 */
- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary;

@end

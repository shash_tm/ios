//
//  TMQuizOverlayOptionCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizOverlayOptionCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

#define OPTION_IMAGE_VIEW_TAG 12324

typedef enum {
    optionSelected,
    optionNotSelected
} optionButtonState;

typedef enum {
    optionTypeImage,
    optionTypeText
} optionButtonType;

@interface TMQuizOverlayOptionCollectionViewCell ()

@property (strong, nonatomic) UIImageView *optionImageView;
@property (weak, nonatomic) IBOutlet UILabel *optionTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *optionSelectionButton;
@property (strong, nonatomic) NSIndexPath* indexPath;
@property (assign, nonatomic) optionButtonState currentOptionState;
@property (assign, nonatomic) optionButtonType currentOptionType;
@property (weak, nonatomic) IBOutlet UIView *optionContainerView;

@end

#define CUSTOM_BLUE_COLOR  [UIColor colorWithRed:86.0/255.0 green:113.0/255.0 blue:173.0/255.0 alpha:1.0]

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@implementation TMQuizOverlayOptionCollectionViewCell

- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary
{
    // Initialization code
    self.optionContainerView.layer.borderColor = CUSTOM_BLUE_COLOR.CGColor;
   // self.optionContainerView.layer.borderWidth = 1.0f;
   // self.optionContainerView.layer.cornerRadius = 8;
    
    NSDictionary* optionsInfoDictionary = [questionOptionsDictionary objectForKey:@"options"];
    
    self.optionTextLabel.text = ([[optionsInfoDictionary objectForKey:@"option_text"] isKindOfClass:[NSString class]])?[optionsInfoDictionary objectForKey:@"option_text"]:@"";//@"My name isMy name isMy name isMy name is";
    
    //removing the previous image view
    while ([self viewWithTag:OPTION_IMAGE_VIEW_TAG]) {
        [[self viewWithTag:OPTION_IMAGE_VIEW_TAG] removeFromSuperview];
    }
    
    if ([optionsInfoDictionary valueForKey:@"option_image"]) {
        [self createAndresizeOptionImage:[optionsInfoDictionary valueForKey:@"option_image"]];
        self.optionSelectionButton.hidden = YES;
        self.currentOptionType = optionTypeImage;
     }
    else {
        self.optionImageView.hidden = YES;
        self.optionSelectionButton.hidden = NO;
        self.optionContainerView.hidden = NO;
        self.currentOptionType = optionTypeText;
    }
    
    [self.optionTextLabel adjustsFontSizeToFitWidth];
    
    if (self.currentOptionType == optionTypeImage) {
        self.optionContainerView.layer.cornerRadius = 8;
    }
    else {
        self.optionContainerView.layer.cornerRadius = 0;
    }
    
    self.clipsToBounds = YES;
  
    //reset the selection button state
    [self.optionSelectionButton setImage:[UIImage imageNamed:@"radio off"] forState:UIControlStateNormal];
    
    [self bringSubviewToFront:self.optionSelectionButton];
}

- (void) createAndresizeOptionImage:(NSString*) imageURL
{
    //things to be kept in mind
    //atleast 22 pixels from the left of the collectionview cell
    //vertical centre same as vertical centre of the cell
    //horizontal centre 22 pixels + width of image from x of the cell
    //width = height = minimum of width, height of image
    
    int requiredHeight = ([UIScreen mainScreen].bounds.size.height < 500)?self.bounds.size.height:self.bounds.size.width - 30;//MIN(imageWidth, imageHeight);
    
    BOOL isEvenPositionImage = (self.indexPath.row%2 == 0)?YES:NO;
    
    BOOL isEvenNoRowImage = ((self.indexPath.row/2)%2 == 0)?YES:NO;
    
    self.optionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.origin.x + (isEvenPositionImage?20:10), 0, requiredHeight , requiredHeight)];
    self.optionImageView.tag = OPTION_IMAGE_VIEW_TAG;
    //self.optionImageView.center = CGPointMake(self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y + self.bounds.size.height/2);
    [self addSubview:self.optionImageView];
    self.optionImageView.layer.borderWidth = 0.5f;
    self.optionImageView.layer.borderColor = [UIColor colorWithRed:88.0/255.0 green:113.0/255.0 blue:163.0/255.0 alpha:1.0].CGColor;
    self.optionImageView.layer.cornerRadius = 6;
    self.optionImageView.hidden = NO;
    self.optionContainerView.hidden = YES;
    [self.optionImageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
    self.optionImageView.clipsToBounds = YES;
    self.optionImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    if (([UIScreen mainScreen].bounds.size.height < 500)) {
        if (isEvenPositionImage) {
            self.optionImageView.center = CGPointMake(self.bounds.size.width/2 + 14, self.bounds.size.height/2);
        }
        else {
            self.optionImageView.center = CGPointMake(self.bounds.size.width/2 - 14, self.bounds.size.height/2);
        }
    }
    else {
        if (IS_IPHONE_6) {
            if (isEvenPositionImage) {
                self.optionImageView.center = CGPointMake(self.bounds.size.width/2 + 6, self.bounds.size.height/2 + (isEvenNoRowImage?6:-6));
            }
            else {
                self.optionImageView.center = CGPointMake(self.bounds.size.width/2 - 6, self.bounds.size.height/2 + (isEvenNoRowImage?6:-6));
            }
        }
        else if (IS_IPHONE_6_PLUS) {
            if (isEvenPositionImage) {
                self.optionImageView.center = CGPointMake(self.bounds.size.width/2 + 5, self.bounds.size.height/2 + (isEvenNoRowImage?6:-6));
            }
            else {
                self.optionImageView.center = CGPointMake(self.bounds.size.width/2 - 5, self.bounds.size.height/2 + (isEvenNoRowImage?6:-6));
            }
        }
        else {
            if (isEvenPositionImage) {
                self.optionImageView.center = CGPointMake(self.bounds.size.width/2 + 5, self.bounds.size.height/2);
            }
            else {
                self.optionImageView.center = CGPointMake(self.bounds.size.width/2 - 5, self.bounds.size.height/2);
            }
        }
    }
    
    self.optionImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                             | UIViewAutoresizingFlexibleHeight
                                             | UIViewAutoresizingFlexibleLeftMargin
                                             | UIViewAutoresizingFlexibleRightMargin
                                             | UIViewAutoresizingFlexibleTopMargin
                                             | UIViewAutoresizingFlexibleWidth);
}

- (void) setUpCellWithIndexPath:(NSIndexPath*) indexPath withCellSelectionState:(OptionSelectionState) currentSelectionState
{
    self.indexPath = indexPath;
    [self makeChangesForCellDeSelectionWithOptionSelectionState:currentSelectionState];
    
    //reset the image view content
    self.optionImageView.image = nil;
}


- (void) setImageForButtonOFF
{
    [self.optionSelectionButton setImage:[UIImage imageNamed:@"radio off"] forState:UIControlStateNormal];
    self.currentOptionState = optionNotSelected;
}

- (void) setImageForButtonON
{
    [self.optionSelectionButton setImage:[UIImage imageNamed:@"radio on"] forState:UIControlStateNormal];
    self.currentOptionState = optionSelected;
}

- (void) makeChangesForCellSelection
{
    self.optionTextLabel.textColor = [UIColor whiteColor];
    self.optionContainerView.backgroundColor = CUSTOM_BLUE_COLOR;
    if(self.currentOptionType == optionTypeImage) {
        self.optionImageView.layer.borderWidth = 2.0f;
        self.optionContainerView.layer.borderWidth = 2.0f;
    }
    else {
        self.optionImageView.layer.borderWidth = 0;
        self.optionContainerView.layer.borderWidth = 0;
    }
}


- (void) makeChangesForCellDeSelectionWithOptionSelectionState:(OptionSelectionState) currentOptionSelectionState
{
    self.optionTextLabel.textColor = CUSTOM_BLUE_COLOR;
    self.optionContainerView.backgroundColor = [UIColor whiteColor];
    
//    switch (currentOptionSelectionState) {
//        case someOptionSeleted:
//             self.optionTextLabel.textColor = [UIColor lightGrayColor];
//            break;
//            
//        case noOptionSelected:
//             self.optionTextLabel.textColor = [UIColor blackColor];
//            break;
//        default:
//            break;
//    }
    
    if(self.currentOptionType == optionTypeImage) {
        self.optionImageView.layer.borderWidth = 0.5f;
    }
    else {
        self.optionImageView.layer.borderWidth = 0;
    }
        
   // [self setImageForButtonOFF];
}


/**
 * Callback method when option button is pressed
 * @param button object
 */
- (IBAction)optionButtonPressed:(id)sender {
    
    [self setImageForButtonON];
    
    [self.delegate optionSelectedWithIndexPath:self.indexPath];
}

@end

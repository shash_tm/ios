//
//  TMFavQuizOptionListView.h
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMFavQuizOptionListViewDelegate;

@interface TMFavQuizOptionListView : UIView

@property(nonatomic,weak)id<TMFavQuizOptionListViewDelegate> delegate;

-(void)reloadTableViewWithData:(NSArray*)data;

-(void)fetchFavOptionListForText:(NSString*)text withCategory:(NSString*)category;

-(void)emptyFavAutoCopmpleteList;
-(void)cleanup;

@end

@protocol TMFavQuizOptionListViewDelegate <NSObject>

-(void)didSelectFavOption:(NSDictionary*)text;
-(void)didReceiveResponse;

@end

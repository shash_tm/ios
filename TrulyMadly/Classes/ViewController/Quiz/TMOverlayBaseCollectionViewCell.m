//
//  TMOverlayBaseCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMOverlayBaseCollectionViewCell.h"

@implementation TMOverlayBaseCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary
{
    //method meant to be overriden
}

@end

//
//  UserAnswerQuestionView.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "UserAnswerQuestionView.h"
#import "UIImageView+AFNetworking.h"


@interface UserAnswerQuestionView ()

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *questionImageView;


@end

@implementation UserAnswerQuestionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    self.questionImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.questionImageView.autoresizingMask = ( UIViewAutoresizingFlexibleBottomMargin
                                                 | UIViewAutoresizingFlexibleHeight
                                                 | UIViewAutoresizingFlexibleLeftMargin
                                                 | UIViewAutoresizingFlexibleRightMargin
                                                 | UIViewAutoresizingFlexibleTopMargin
                                                 | UIViewAutoresizingFlexibleWidth );
    self.questionImageView.layer.cornerRadius = self.questionImageView.bounds.size.width/2;
    self.questionImageView.clipsToBounds = YES;
}

- (void) fillQuestionDetailsForQuestionObject:(QuizQuestionDomainObject*) questionDomainObject andQuestionNumber:(int) questionNo
{
    //setting the label
    self.questionLabel.text = [NSString stringWithFormat:@"%d. %@",questionNo,questionDomainObject.questionText];
    
    //settting the image view
    [self.questionImageView setImageWithURL:[NSURL URLWithString:questionDomainObject.questionImageURL]];
   
}

@end

//
//  TMQuizOverlayViewController.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 09/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMQuizController.h"


@protocol TMQuizOverlayViewControllerDelegate <NSObject>

@optional
- (NSString*) getCurrentMatchProfilePic;

- (NSString*) getUserProfilePic;

- (NSString*) getUserName;

- (NSString*) getCurrentMatchName;

@end

@interface TMQuizOverlayViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) id<TMQuizOverlayViewControllerDelegate> delegate;
@property (nonatomic, weak) TMQuizController* quizController;

- (instancetype) initWithNibName:(NSString*) nibNameOrNil bundle:(NSBundle*) nibBundleOrNil quizID:(NSString*)quizID blurredImage:(UIImage*) blurredImage loaderMessage:(NSString*) loaderMessage welcomeScreenToBeShown:(BOOL) isWelcomeScreeenToBeShown quizController:(TMQuizController*) quizController;

@end

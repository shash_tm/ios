//
//  TMCollectionViewCellFavQuizList.m
//  TrulyMadly
//
//  Created by Ankit on 24/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMCollectionViewCellFavQuizList.h"
#import "TMFavListView.h"
#import "UIColor+TMColorAdditions.h"

@interface TMCollectionViewCellFavQuizList ()<TMFavListViewDelegate>
@property(nonatomic,strong)TMFavListView *favListView;

@end

@implementation TMCollectionViewCellFavQuizList

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.favListView = [[TMFavListView alloc] initWithFrame:self.bounds withType:FavListType_Edit];
        [self.favListView enableUserInteraction];
        self.favListView.color = [UIColor likeunSelectedColor];
        self.favListView.delegate = self;
        self.favListView.favCollectionView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
        [self.contentView addSubview:self.favListView];
        
        UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, frame.size.height-2, frame.size.width, 2)];
        seperatorImgView.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:230.0f/255.0f blue:243.0f/255.0f alpha:1];
        [self.contentView addSubview:seperatorImgView];

    }
    return self;
}

-(void)setDataWithArray:(NSArray*)list {
    [self.favListView loadDataWithContent:list];
}

-(void)didSelectItemAtIndex:(NSString *)str {
    [self.delegate didSelectItem:str withKey:self.key];
}

-(void)didSelecFavAtIndex:(TMFavTag*)tag {
    [self.delegate didSelecFavTag:tag withKeyIdentifier:self.key];
}

@end

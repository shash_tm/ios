//
//  TMQuizCommonAnswerTableViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 27/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizCommonAnswerTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMUserSession.h"
#import "TMUser.h"

#define FEMALE_TEXT_COLOR  [UIColor colorWithRed:146.0/255.0 green:103.0/255.0 blue:123.0/255.0 alpha:1.0]
#define MALE_TEXT_COLOR [UIColor colorWithRed:86.0/255.0 green:113.0/255.0 blue:173.0/255.0 alpha:1.0]

@interface TMQuizCommonAnswerTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userImageVIew;
@property (weak, nonatomic) IBOutlet UIImageView *userOptionImageView;
@property (weak, nonatomic) IBOutlet UILabel *userOptionLabel;
@property (weak, nonatomic) IBOutlet UIView *userContainerView;

@property (weak, nonatomic) IBOutlet UIImageView *matchImageView;
@property (weak, nonatomic) IBOutlet UIImageView *matchOptionImageView;
@property (weak, nonatomic) IBOutlet UILabel *matchOptionLabel;
@property (weak, nonatomic) IBOutlet UIView *matchContainerView;

@property (nonatomic, assign) BOOL isFemaleUser;

@end

@implementation TMQuizCommonAnswerTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    //make the user/match image views rounded
    self.userImageVIew.layer.cornerRadius = self.userImageVIew.bounds.size.width/2;
    self.matchImageView.layer.cornerRadius = self.matchImageView.bounds.size.width/2;
    
    self.userImageVIew.clipsToBounds = YES;
    self.matchImageView.clipsToBounds = YES;
    
    self.userContainerView.layer.borderWidth = 0.5f;
    self.matchContainerView.layer.borderWidth = 0.5f;
    
    self.userContainerView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userContainerView.layer.cornerRadius = 5.0f;
    self.matchContainerView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.matchContainerView.layer.cornerRadius = 5.0f;
    self.userOptionImageView.layer.cornerRadius = 5;
    self.matchOptionImageView.layer.cornerRadius = 5;
    self.userOptionImageView.clipsToBounds = YES;
    self.matchOptionImageView.clipsToBounds = YES;
    
    self.isFemaleUser = [[TMUserSession sharedInstance].user isUserFemale];
    
    if (self.isFemaleUser) {
        self.userOptionLabel.textColor = FEMALE_TEXT_COLOR;
        self.matchOptionLabel.textColor = MALE_TEXT_COLOR;
    }
    else {
        self.userOptionLabel.textColor = MALE_TEXT_COLOR;
        self.matchOptionLabel.textColor = FEMALE_TEXT_COLOR;
    }
}

- (void) updateCommonAnswerInfoForUserAnswer:(QuizOptionDomainObject*) userAnswer matchAnswer:(QuizOptionDomainObject*) matchAnswer userProfilePicURL:(NSString*) userProfilePicURL matchProfilePicURL:(NSString*) matchProfilePicURL
{
    //update the user image
    [self.userImageVIew setImageWithURL:[NSURL URLWithString:userProfilePicURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    
    //update the match image
    [self.matchImageView setImageWithURL:[NSURL URLWithString:matchProfilePicURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    
    if (!userAnswer) {
        //hide the image option
        self.userOptionImageView.hidden = YES;
        
        //hide the label option
        self.userOptionLabel.hidden = YES;
    }
    
    else if (userAnswer.optionText && userAnswer.optionText.length) {
        //hide the image option
        self.userOptionImageView.hidden = YES;
        
        //show the label option
        self.userOptionLabel.hidden = NO;
        
        //update the label
        self.userOptionLabel.text = userAnswer.optionText;
    }
    else {
        //hide the text label
        self.userOptionLabel.hidden = YES;
        
        //show the image option
        self.userOptionImageView.hidden = NO;
        
        //update the image
        [self.userOptionImageView setImageWithURL:[NSURL URLWithString:userAnswer.optionImageURL] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
    }
    
    if (!matchAnswer) {
        //hide the image option
        self.matchOptionImageView.hidden = YES;
        
        //hide the label option
        self.matchOptionLabel.hidden = YES;
    }
    
    else if (matchAnswer.optionText && matchAnswer.optionText.length) {
        //hide the image option
        self.matchOptionImageView.hidden = YES;
        
        self.matchOptionLabel.hidden = NO;
        
        //update the label
        self.matchOptionLabel.text = matchAnswer.optionText;
    }
    else {
        //hide the text label
        self.matchOptionLabel.hidden = YES;
        
        self.matchOptionImageView.hidden = NO;
        
        //update the image
        [self.matchOptionImageView setImageWithURL:[NSURL URLWithString:matchAnswer.optionImageURL] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
    }
    
    if ((userAnswer.optionText.length && matchAnswer.optionText.length && [userAnswer.optionText isEqualToString:matchAnswer.optionText]) || (userAnswer.optionImageURL.length && matchAnswer.optionImageURL.length && [userAnswer.optionImageURL isEqualToString:matchAnswer.optionImageURL])) {
   
        self.userContainerView.layer.borderWidth = 1.5;
        self.matchContainerView.layer.borderWidth = 1.5;
        
        self.userContainerView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.matchContainerView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else {
      
        self.userContainerView.layer.borderWidth = 0.5f;
        self.matchContainerView.layer.borderWidth = 0.5f;
        
        if (self.isFemaleUser) {
            self.userContainerView.layer.borderColor = FEMALE_TEXT_COLOR.CGColor;
            self.matchContainerView.layer.borderColor = MALE_TEXT_COLOR.CGColor;
        }
        else {
            self.userContainerView.layer.borderColor = MALE_TEXT_COLOR.CGColor;
            self.matchContainerView.layer.borderColor = FEMALE_TEXT_COLOR.CGColor;
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

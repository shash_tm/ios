//
//  TMQuizUserOptionImageTableViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 18/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizUserOptionImageTableViewCell.h"
#import "UIImageView+AFNetworking.h"


#define FEMALE_TEXT_COLOR [UIColor colorWithRed:209.0/255.0 green:12.0/255.0 blue:108.0/255.0 alpha:1.0]
#define MALE_TEXT_COLOR [UIColor colorWithRed:124.0/255.0 green:160.0/255.0 blue:209.0/255.0 alpha:1.0]

@interface TMQuizUserOptionImageTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userOptionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *matchOptionImageView;

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UIImageView *matchProfileImageView;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;

@end

@implementation TMQuizUserOptionImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.userOptionImageView.layer.cornerRadius = self.userOptionImageView.bounds.size.width/2;
    self.userOptionImageView.clipsToBounds = YES;
    self.userOptionImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userOptionImageView.layer.borderWidth = 1.0f;
    self.userOptionImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userOptionImageView.autoresizingMask = ( UIViewAutoresizingFlexibleBottomMargin
                                                 | UIViewAutoresizingFlexibleHeight
                                                 | UIViewAutoresizingFlexibleLeftMargin
                                                 | UIViewAutoresizingFlexibleRightMargin
                                                 | UIViewAutoresizingFlexibleTopMargin
                                                 | UIViewAutoresizingFlexibleWidth );
    
    self.matchOptionImageView.layer.cornerRadius = self.matchOptionImageView.bounds.size.width/2;
    self.matchOptionImageView.clipsToBounds = YES;
    self.matchOptionImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.matchOptionImageView.layer.borderWidth = 1.0f;
    self.matchOptionImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.matchOptionImageView.autoresizingMask = ( UIViewAutoresizingFlexibleBottomMargin
                                                 | UIViewAutoresizingFlexibleHeight
                                                 | UIViewAutoresizingFlexibleLeftMargin
                                                 | UIViewAutoresizingFlexibleRightMargin
                                                 | UIViewAutoresizingFlexibleTopMargin
                                                 | UIViewAutoresizingFlexibleWidth );
    
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.bounds.size.width/2;
    self.userProfileImageView.clipsToBounds = YES;
    self.userProfileImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userProfileImageView.autoresizingMask = ( UIViewAutoresizingFlexibleBottomMargin
                                                 | UIViewAutoresizingFlexibleHeight
                                                 | UIViewAutoresizingFlexibleLeftMargin
                                                 | UIViewAutoresizingFlexibleRightMargin
                                                 | UIViewAutoresizingFlexibleTopMargin
                                                 | UIViewAutoresizingFlexibleWidth );
    
    self.matchProfileImageView.layer.cornerRadius = self.matchProfileImageView.bounds.size.width/2;
    self.matchProfileImageView.clipsToBounds = YES;
    self.matchProfileImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.matchProfileImageView.autoresizingMask = ( UIViewAutoresizingFlexibleBottomMargin
                                                 | UIViewAutoresizingFlexibleHeight
                                                 | UIViewAutoresizingFlexibleLeftMargin
                                                 | UIViewAutoresizingFlexibleRightMargin
                                                 | UIViewAutoresizingFlexibleTopMargin
                                                 | UIViewAutoresizingFlexibleWidth );
}

- (void) fillDetailsWithQuestionDomainObject:(QuizQuestionDomainObject*) quizQuestionDomainObject userProfileImageURL:(NSString*) userProfileImageURL matchProfileImageURL:(NSString*) matchProfileImageURL userName:(NSString*) userName matchName:(NSString*) matchName isFemale:(BOOL) isFemale
{
    if (quizQuestionDomainObject.userAnswer && quizQuestionDomainObject.matchAnswer) {
        
        //fetching the gender info
        BOOL isUserFemale = isFemale;
        
        //setting the user option image
        [self.userOptionImageView setImageWithURL:[NSURL URLWithString:quizQuestionDomainObject.userAnswer.optionImageURL]];
        
        //setting the match option image
        [self.matchOptionImageView setImageWithURL:[NSURL URLWithString:quizQuestionDomainObject.matchAnswer.optionImageURL]];
        
        //setting the user profile image
        [self.userProfileImageView setImageWithURL:[NSURL URLWithString:userProfileImageURL]];
        
        //setting the match profile image
        [self.matchProfileImageView setImageWithURL:[NSURL URLWithString:matchProfileImageURL]];
        
        //setting the user name
        self.userNameLabel.text = userName;
        self.userNameLabel.textColor = isUserFemale?FEMALE_TEXT_COLOR:MALE_TEXT_COLOR;
        
        //setting the match name
        self.matchNameLabel.text = matchName;
        self.matchNameLabel.textColor = isUserFemale?MALE_TEXT_COLOR:FEMALE_TEXT_COLOR;
      }
}

@end

//
//  TMQuizOverlayOptionCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMOverlayBaseCollectionViewCell.h"

typedef enum {
    someOptionSeleted,
    noOptionSelected
} OptionSelectionState;

@protocol TMQuizOverlayOptionDelegate <NSObject>

/**
 * Callback method for option selected at a given indexpath
 * @param the selected indexpath
 */
- (void) optionSelectedWithIndexPath:(NSIndexPath*) indexPath;

@end

@interface TMQuizOverlayOptionCollectionViewCell : TMOverlayBaseCollectionViewCell

@property (nonatomic, weak) id <TMQuizOverlayOptionDelegate> delegate;

- (void) setUpCellWithIndexPath:(NSIndexPath*) indexPath withCellSelectionState:(OptionSelectionState) currentSelectionState;
- (void) makeChangesForCellSelection;
- (void) makeChangesForCellDeSelectionWithOptionSelectionState:(OptionSelectionState) currentOptionSelectionState;

@end

//
//  TMFavouritesViewController.m
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFavouritesViewController.h"
#import "TMCollectionViewCellFavQuiz.h"
#import "TMFavQuizCollectionFooterView.h"
#import "TMPersonality.h"
#import "TMFavQuizOptionListView.h"
#import "TMCollectionViewCellFavQuizList.h"
#import "TMFavTag.h"
#import "TMSwiftHeader.h"

//TODO
//check to stop empty and unedited save call

@interface TMFavouritesViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,TMCollectionViewCellFavQuizDelegate,UITextFieldDelegate,TMFavQuizOptionListViewDelegate,TMFavQuizCollectionFooterViewDelegate,TMCollectionViewCellFavQuizListDelegate>

@property(nonatomic,assign)TMNavigationFlow navigationFlow;

@property(nonatomic,strong)TMPersonality *personalityMgr;
@property(nonatomic,strong)UICollectionView *favQuizCollectionView;
@property(nonatomic,strong)NSString *text;
@property(nonatomic,weak)UITextField *activeTf;
@property(nonatomic,weak)NSIndexPath *indexPath;
@property(nonatomic,weak)NSTimer *autoCompleteTimer;
@property(nonatomic,strong)NSMutableDictionary *favDict;
@property(nonatomic,strong)NSArray *favSections;
@property(nonatomic,assign)CGFloat currentKeyboardHeight;
@property(nonatomic,strong)NSString *currentKey;
@property(nonatomic,strong)NSString *currentCategory;
@property(nonatomic,strong)NSMutableDictionary *placeholderTextDict;
@property(nonatomic,assign)NSString* eventCategory;
@property(nonatomic,strong)NSMutableDictionary* eventDict;
@property(nonatomic,strong)NSString *searchText;
@property(nonatomic,strong)NSArray *icon_images;
@end

@implementation TMFavouritesViewController

#pragma mark Intialization
#pragma mark -

-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow {
    self = [self init];
    if(self) {
        self.navigationFlow = navigationFlow;
        self.personalityMgr = [[TMPersonality alloc] init];
        self.eventCategory = @"hobby";
        self.eventDict = [[NSMutableDictionary alloc] init];
        [self.eventDict setObject:@"TMFavouritesViewController" forKey:@"screenName"];
        [self.eventDict setObject:self.eventCategory forKey:@"eventCategory"];
        [self.eventDict setObject:@"page_load" forKey:@"eventAction"];
        
        [self initDefaults];
        [self loadFavData];
    }
    
    return self;
}
-(void)initDefaults {
    self.currentKeyboardHeight = 0;
    self.favDict = [NSMutableDictionary dictionaryWithCapacity:16];
    self.placeholderTextDict = [NSMutableDictionary dictionaryWithCapacity:16];
    self.favSections = [NSArray arrayWithObjects:@"movies", @"music", @"books", @"travel", @"others", nil];
    
    NSArray *placeholders = [NSArray arrayWithObjects:@"Favourite movies, actors, TV shows, channels...",@"Favourite music, bands, songs, artist, genres...",@"Favourite books, authors, magazines, genres...",@"Favourite cuisines, places, restaurants...",@"Favourite sports and hobbies...", nil];
    
    self.icon_images = [NSArray arrayWithObjects:@"Movies_Fav", @"Music_Fav", @"Books_Fav", @"Food_Fav", @"Misc_Fav", nil];
    
    //UIFont *font = (self.view.frame.size.width > 320) ? [UIFont systemFontOfSize:14] : [UIFont systemFontOfSize:12];
    
    for (int i=0; i<self.favSections.count; i++) {
        
        /*NSAttributedString *attString = [[NSAttributedString alloc] initWithString:[placeholders objectAtIndex:i]
                                                                        attributes:@{
                                                                                     NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                                                                     NSFontAttributeName:font
                                                                                     }
                                         ];*/
        [self.placeholderTextDict setObject:[placeholders objectAtIndex:i] forKey:[self.favSections objectAtIndex:i]];
        [self.favDict setObject:[NSMutableArray arrayWithCapacity:16] forKey:[self.favSections objectAtIndex:i]];
    }
}
-(void)registerNotifcations {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Favourites";
    
    [[TMAnalytics sharedInstance] trackView:@"TMFavouritesViewController"];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self cleanup];
}

-(void)dealloc {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Configiuration Methods
#pragma mark -

-(void)configureUI {
    [self configureCollectionView];
    [self configureSaveBarItem];
}
-(void)configureCollectionView {
    
    if([self.favQuizCollectionView superview]) {
        [self.favQuizCollectionView removeFromSuperview];
        self.favQuizCollectionView = nil;
    }
    CGFloat collectionViewHeight = self.favQuizCollectionView.frame.size.height;

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.minimumLineSpacing = 5;
    flowLayout.headerReferenceSize = CGSizeMake(self.view.bounds.size.width, 18);
    [flowLayout setSectionInset:UIEdgeInsetsMake(collectionViewHeight / 2, 0, collectionViewHeight / 2, 0)];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.favQuizCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.favQuizCollectionView.backgroundColor = [UIColor clearColor];
    self.favQuizCollectionView.dataSource = self;
    self.favQuizCollectionView.delegate = self;
    self.favQuizCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:self.favQuizCollectionView];
    
    [self.favQuizCollectionView registerClass:[TMCollectionViewCellFavQuiz class] forCellWithReuseIdentifier:@"quiztf"];
    [self.favQuizCollectionView registerClass:[TMCollectionViewCellFavQuizList class] forCellWithReuseIdentifier:@"facquizlist"];
    [self.favQuizCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect frame = self.view.frame;
    CGFloat yPos = (self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) ? 64 : 0;
    self.favQuizCollectionView.frame = CGRectMake(0, yPos, frame.size.width, frame.size.height - yPos);
}

-(void)configureSaveBarItem {
   UIBarButtonItem *righBarButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveAction)];
    self.navigationItem.rightBarButtonItem = righBarButton;
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadFavData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.isRetryViewShown) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadFavData) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)configureViewForDataLoadingStartWithMessage:(NSString*)message {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:message];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
}


#pragma mark View Handing  Methods
#pragma mark -

-(void)showFavOptionListView {
    CGFloat yPos= 50;
    if(floor(self.favQuizCollectionView.frame.origin.y) != 0) {
        yPos = 100;
    }
    TMFavQuizOptionListView *listview = [[TMFavQuizOptionListView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height-260)];
    listview.tag = 500001;
    listview.delegate = self;
    [self.view addSubview:listview];
}

-(void)setFavQuizOptionListViewHeightForKeyboardHeight {
    
    TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
    CGRect rect = listview.frame;
    CGFloat height = self.view.frame.size.height - (rect.origin.y+self.currentKeyboardHeight);
    listview.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, height);
}

-(void)removeFavOptionListView {
    TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
    [listview removeFromSuperview];
}

#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.favSections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSString *key = [self.favSections objectAtIndex:section];
    NSArray *innerArray = [self.favDict objectForKey:key];
    NSInteger numOfItems = (innerArray.count) ? 2 : 1;
    return numOfItems;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {
        NSString *reusableIdentifier = @"quiztf";
        
        TMCollectionViewCellFavQuiz *cell = [collectionView
                                             dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                             forIndexPath:indexPath];
        NSString *key = [self.favSections objectAtIndex:indexPath.section];
        cell.delegate = self;
        cell.textFieldDelegate = self;
        [cell setActiveIndexPath:indexPath];
        cell.tf.delegate = self;
        cell.tf.tag = 71000+indexPath.section;
        //cell.tf.attributedPlaceholder = [self.placeholderTextDict objectForKey:key];
        cell.tf.placeholder = [self.placeholderTextDict objectForKey:key];
        cell.backgroundColor = [UIColor clearColor];
        if(indexPath.section == 0) {
            CGRect rect = cell.icon.frame;
            cell.icon.frame = CGRectMake(rect.origin.x, cell.tf.frame.origin.y+14.5, 14, 15);
        }else if(indexPath.section == 1) {
            CGRect rect = cell.icon.frame;
            cell.icon.frame = CGRectMake(rect.origin.x, cell.tf.frame.origin.y+14.5, 13, 15);
        }else if(indexPath.section == 2) {
            CGRect rect = cell.icon.frame;
            cell.icon.frame = CGRectMake(rect.origin.x, cell.tf.frame.origin.y+14.5, 18, 15);
        }else if(indexPath.section == 3) {
            CGRect rect = cell.icon.frame;
            cell.icon.frame = CGRectMake(rect.origin.x, cell.tf.frame.origin.y+14.5, 12, 15);
        }else if(indexPath.section == 4) {
            CGRect rect = cell.icon.frame;
            cell.icon.frame = CGRectMake(rect.origin.x, cell.tf.frame.origin.y+20, 15, 4);
        }
        [cell.icon setImage:[UIImage imageNamed:[self.icon_images objectAtIndex:indexPath.section]]];
        return  cell;
    }
    else {
        NSString *reusableIdentifier = @"facquizlist";
        
        TMCollectionViewCellFavQuizList *cell = [collectionView
                                             dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                             forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        NSString *key = [self.favSections objectAtIndex:indexPath.section];
        NSArray *favList = [self.favDict objectForKey:key];
        [cell setDataWithArray:favList];
        cell.key = key;
        cell.delegate = self;
        return  cell;
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerview.backgroundColor = [UIColor clearColor];
    return headerview;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (self.view.frame.size.width - 20);
    CGSize size = CGSizeMake(width, 44);
    
    return size;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
   return CGSizeMake(0, 0);
}

-(void)didBeginEditingTextInTextFieldWithIndexPath:(NSIndexPath*)indexPath {
    self.indexPath = indexPath;
    UICollectionViewCell *containingCell = [self.favQuizCollectionView cellForItemAtIndexPath:self.indexPath];
    //NSLog(@"Frame:%@",NSStringFromCGRect(containingCell.frame));
    
    //[self.favQuizCollectionView scrollRectToVisible:containingCell.frame animated:YES];
    
    
    if((containingCell.frame.origin.y>50) && (self.view.frame.size.width<=320) ) {
       // CGFloat collectionViewHeight = self.favQuizCollectionView.frame.size.height;
//        [self.favQuizCollectionView setContentInset:UIEdgeInsetsMake(collectionViewHeight / 2, 0, collectionViewHeight / 2, 0)];
        
        CGPoint offset = CGPointMake(0, (44*indexPath.row) + (20*indexPath.row) );
        
        [self.favQuizCollectionView setContentOffset:offset];
    }
    
    
}

-(void)didFinishEditingTextInTextFieldWithIndexPath:(NSIndexPath*)indexPath {
    CGPoint offset = CGPointMake(0, 0);
     [self.favQuizCollectionView setContentInset:UIEdgeInsetsZero];
    [self.favQuizCollectionView setContentOffset:offset];
}

-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    //CGFloat deltaHeight = kbSize.height - _currentKeyboardHeight;
    // Write code to adjust views accordingly using deltaHeight
    self.currentKeyboardHeight = kbSize.height;
    [self setFavQuizOptionListViewHeightForKeyboardHeight];
}

-(void)keyboardDidHide:(NSNotification*)notification {
    self.currentKeyboardHeight = 0;
}


-(NSString*)categoryForTextFieldTag:(NSUInteger)tag {
    NSInteger index = tag - 71000;
    NSString *category = [self.favSections objectAtIndex:index];
    return category;
}

#pragma mark UITextfield delegate methods -

-(void)textFieldDidBeginEditing:(UITextField *)textField {
//    NSLog(@"textFieldDidBeginEditing");
//    NSLog(@"TF SuperView Frame:%@",NSStringFromCGRect(textField.superview.superview.frame));
//    NSLog(@"CollectionviewOffset:%@",NSStringFromCGPoint(self.favQuizCollectionView.contentOffset));
//    CGPoint point = self.favQuizCollectionView.contentOffset;
//    point.y = point.y + textField.superview.superview.frame.origin.y;
//    CGPoint currentOffset = self.favQuizCollectionView.contentOffset;
//    NSLog(@"_____:%f  :%f  contentS:%f",currentOffset.y,point.y,self.favQuizCollectionView.contentSize.height);
//    self.favQuizCollectionView.contentOffset = CGPointMake(point.x, point.y);
//    NSLog(@"CollectionviewOffset:%@",NSStringFromCGPoint(self.favQuizCollectionView.contentOffset));
//    NSLog(@"Converted:%@",NSStringFromCGRect([self.favQuizCollectionView convertRect:textField.frame fromView:textField]));
//    [self.favQuizCollectionView convertRect:textField.frame fromView:textField];
   // [self showFavOptionListView];
    
    /////////
    CGRect rect = [self.favQuizCollectionView convertRect:textField.frame fromView:textField];
    CGFloat ht = self.favQuizCollectionView.contentSize.height;
    CGPoint point = self.favQuizCollectionView.contentOffset;
    point.y = rect.origin.y + (ht-rect.origin.y) + 15;
    //NSLog(@"Y:%f",point.y);
    self.favQuizCollectionView.contentOffset = CGPointMake(point.x, point.y);
    [self showFavOptionListView];
    //////////
    self.currentCategory = [self categoryForTextFieldTag:textField.tag];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //NSLog(@"textFieldDidEndEditing");
    if(self.autoCompleteTimer) {
      //  NSLog(@"Invalidating Timer");
        [self.autoCompleteTimer invalidate];
        self.autoCompleteTimer = nil;
    }
    textField.text = @"";
    
    self.favQuizCollectionView.contentOffset = CGPointMake(0, 0);
    [self removeFavOptionListView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //NSLog(@"textField:%@---shouldChangeCharactersInRange---replacementString:%@ Range:Loc:%d Length:%d",textField.text,string,range.location,range.length);
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *tfText = textField.text;
    if(tfText.length > 0 && [trimmedString isEqualToString:@""] && range.length==1) {
        tfText = [tfText stringByReplacingCharactersInRange:NSMakeRange(tfText.length-1,1) withString:@""];
    }
    self.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //self.text = [NSString stringWithFormat:@"%@%@",tfText,trimmedString];

    //look for valid string
    if((string) && (![trimmedString isEqualToString:@""])) {
        //NSLog(@"valid string");
        if(self.autoCompleteTimer) {
          //  NSLog(@"Invalidating Timer");
            [self.autoCompleteTimer invalidate];
            self.autoCompleteTimer = nil;
        }
        if(!self.autoCompleteTimer) {
            //NSLog(@"Creating Timer");
            self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(autoCompleteFavList) userInfo:nil repeats:NO];
        }
        
        
        //[self fetchAutocompleteListForText];
    }
    else if((self.text.length == 1) && ([trimmedString isEqualToString:@""])) {
        self.searchText = @"";
        TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
        [listview emptyFavAutoCopmpleteList];
    }
    else {
        if(self.autoCompleteTimer) {
           // NSLog(@"Invalidating Timer");
            [self.autoCompleteTimer invalidate];
            self.autoCompleteTimer = nil;
        }
        if(!self.autoCompleteTimer) {
            //NSLog(@"Creating Timer");
            self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(autoCompleteFavList) userInfo:nil repeats:NO];
        }
    }
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //NSLog(@"textFieldShouldReturn");
    [textField resignFirstResponder];
    if(self.text.length > 2) {
        [self addOnReturn:self.text];
    }
    return YES;
}

-(void)addOnReturn:(NSString *)text {
    NSDictionary *data = @{@"name":text,@"id":text};
    [self didSelectFavOption:data];
}

#pragma mark Data Loading  Methods
#pragma mark -
-(NSDictionary*)favSaveRequestDict {
    NSMutableDictionary *favReqDict = [NSMutableDictionary dictionary];
    NSArray *favSectionKeys = self.favDict.allKeys;
    for (int i=0; i<favSectionKeys.count; i++) {
        NSString *key = [favSectionKeys objectAtIndex:i];
        NSArray *favs = [self.favDict objectForKey:key];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for (int i=0; i<favs.count; i++) {
            TMFavTag *tag = [favs objectAtIndex:i];
            [dict setObject:tag.value forKey:tag.key];
        }
        [favReqDict setObject:dict forKey:key];
    }
    
    return favReqDict;
}
-(void)parseFavDictionary:(NSDictionary*)responseDictionary {
    NSDictionary *favDict = [responseDictionary objectForKey:@"favourites"];
    NSArray *allKeys = [favDict allKeys];
    
    for (int i=0; i<allKeys.count; i++) {
        NSString *sectionKey = [allKeys objectAtIndex:i];
        NSDictionary *favsDict = [favDict objectForKey:sectionKey];
        NSDictionary *favContentDict = [favsDict objectForKey:@"favorites"];
        if([[self.favDict objectForKey:sectionKey] count]) {
            [[self.favDict objectForKey:sectionKey] removeAllObjects];
        }
        if(favContentDict && [favContentDict isKindOfClass:[NSDictionary class]]) {
            NSArray *favContentDictkeys = [favContentDict allKeys];
            for (int i=0; i<favContentDictkeys.count; i++) {
                NSMutableArray *favsArray = [self.favDict objectForKey:sectionKey];
                NSString *key = [favContentDictkeys objectAtIndex:i];
                NSString *value = [favContentDict objectForKey:key];
                TMFavTag *tag = [[TMFavTag alloc] init];
                tag.key = key;
                tag.value = value;
                [favsArray addObject:tag];
            }
        }
    }
}
-(void)loadFavData {
    
    self.personalityMgr.trackEventDictionary = self.eventDict;
    
   // if(self.personalityMgr.isNetworkReachable) {
        [self configureViewForDataLoadingStartWithMessage:@"Loading Favourites..."];
        [self.personalityMgr getFavListWithResponse:^(NSDictionary *dict, TMError *error) {
            [self configureViewForDataLoadingFinish];
            if(dict && !error) {
                [self parseFavDictionary:dict];
                [self configureUI];
            }
            else {
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    //remove modal vc and show alart for network error
                    //[self showFavResponseErrorAlert:FAV_LOADING_ERROR_TITLE msg:TM_INTERNET_NOTAVAILABLE_MSG];
                    if (!self.personalityMgr.isCachedResponsePresent) {
                        if (error.errorCode == TMERRORCODE_NONETWORK) {
                            [self showRetryView];
                             [self.retryView.retryButton addTarget:self action:@selector(loadFavData) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else {
                            [self showRetryViewAtError];
                             [self.retryView.retryButton addTarget:self action:@selector(loadFavData) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                }
                else {
                    //unknwon error
                    //[self showFavResponseErrorAlert:FAV_LOADING_ERROR_TITLE msg:PLEASETRYLATER_ERROR_MSG];
                    //[self showRetryViewAtError];
                  //  [self showRetryViewAtUnknownError];
                }
            }
        }];
  //  }
//    else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//        [self showRetryViewAtError];
//    }
}
-(void)saveAction {
    [self.eventDict setObject:@"TMFavouritesViewController" forKey:@"screenName"];
    [self.eventDict setObject:self.eventCategory forKey:@"eventCategory"];
    [self.eventDict setObject:@"save_call" forKey:@"eventAction"];
    self.personalityMgr.trackEventDictionary = self.eventDict;
    
    if(self.personalityMgr.isNetworkReachable) {
        [self configureViewForDataLoadingStartWithMessage:@"Saving Favourites..."];
        NSDictionary *dict = [NSDictionary dictionaryWithObject:[self favSaveRequestDict] forKey:@"favourites"];
        [self.personalityMgr saveFavourite:dict withResponse:^(BOOL status) {
            [self configureViewForDataLoadingFinish];
            if(status) {
                [self.delegate didEditFavourite];
                [self.navigationController popViewControllerAnimated:NO];
            }
            else {
                [self showFavResponseErrorAlert:FAV_SAVING_ERROR_TITLE
                                            msg:nil];
            }
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        if (![self.personalityMgr isCachedResponsePresent]) {
             [self showRetryViewAtError];
        }
    }
}
-(void)cleanup {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = nil;
    self.personalityMgr = nil;
    self.delegate = nil;
    [self cleanup1];
}
-(void)cleanup1 {
    TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
    listview.delegate = nil;
    [listview cleanup];
    [listview removeFromSuperview];
    listview = nil;
}
-(void)fetchAutocompleteListForText {
    //NSLog(@"TL:%lud Rem:%lud",(unsigned long)self.text.length,(self.text.length % 3));
    self.text = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if( (self.text.length>1) && ((self.text.length % 3) == 0) && ![self.text isEqualToString:self.searchText]) {
        self.searchText = self.text;
        //NSLog(@"fetchAutocompleteListForText:%@",self.text);
        if(self.autoCompleteTimer) {
           // NSLog(@"Invalidating Timer");
            [self.autoCompleteTimer invalidate];
            self.autoCompleteTimer = nil;
        }
        
        TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
        if([self.currentCategory isEqualToString:@"others"]) {
            [listview fetchFavOptionListForText:self.text withCategory:@"sports"];
        }else {
            [listview fetchFavOptionListForText:self.text withCategory:self.currentCategory];
        }
    }
}

-(void)autoCompleteFavList {
    //NSLog(@"Timer Completed Fetching AutoComplete List");
    self.text = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //NSLog(@"search text %@",self.searchText);
    //NSLog(@"self text %@",self.text);
    if(self.text.length>2 && ![self.text isEqualToString:self.searchText]) {
        self.searchText = self.text;
        TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
//        if([self.currentCategory isEqualToString:@"others"]) {
//            [listview fetchFavOptionListForText:self.text withCategory:@"sports"];
//        }else {
            [listview fetchFavOptionListForText:self.text withCategory:self.currentCategory];
       // }
    }
}

-(void)didSelectFavOption:(NSDictionary*)dict {
    NSMutableArray *favs = [self.favDict objectForKey:self.currentCategory];
    TMFavTag *tag = [[TMFavTag alloc] init];
    tag.key = [dict objectForKey:@"id"];
    tag.value = [dict objectForKey:@"name"];
    [favs insertObject:tag atIndex:0];
    
    [self.favQuizCollectionView reloadData];
}

-(void)didReceiveResponse {
    if(self.text.length==0) {
        TMFavQuizOptionListView *listview = (TMFavQuizOptionListView*)[self.view viewWithTag:500001];
        [listview emptyFavAutoCopmpleteList];
    }
}

-(void)didSelecFavTag:(TMFavTag*)tag withKeyIdentifier:(NSString *)keyIndentifier{
    NSMutableArray *favList = [self.favDict objectForKey:keyIndentifier];
    [favList removeObjectAtIndex:tag.index];
    
    [self.favQuizCollectionView reloadData];
}

#pragma mark - Error Alert Handler
#pragma mark -
-(void)showFavResponseErrorAlert:(NSString*)title
                               msg:(NSString*)msg  {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

@end

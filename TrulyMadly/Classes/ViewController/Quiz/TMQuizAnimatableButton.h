//
//  QuizDanceButton.h
//  QuizDanceButton
//
//  Created by Abhijeet Mishra on 21/06/15.
//  Copyright (c) 2015 Abhijeet Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMAnimatableButtonProtocol <NSObject>

@optional
- (void) animateButton;

@end

@interface TMQuizAnimatableButton : UIButton <TMAnimatableButtonProtocol>

- (void) setUnreadQuizStatus:(BOOL) unreadQuizStatus;

@end

//
//  TMQuizOverlayQuestionCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizOverlayQuestionCollectionViewCell.h"
#import "NSObject+TMAdditions.h"

@interface TMQuizOverlayQuestionCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *questionImageView;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

@end

@implementation TMQuizOverlayQuestionCollectionViewCell

/**
 * fill details for the question text
 * @param question info dictionary
 */
- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary
{
    NSString* questionString = ([[questionOptionsDictionary objectForKey:@"question_text"] isValidObject])?[questionOptionsDictionary objectForKey:@"question_text"]:@"";
    
    self.questionLabel.text = questionString;
}

@end

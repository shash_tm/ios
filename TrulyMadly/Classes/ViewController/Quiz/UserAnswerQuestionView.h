//
//  UserAnswerQuestionView.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizQuestionDomainObject.h"

@interface UserAnswerQuestionView : UIView

- (void) fillQuestionDetailsForQuestionObject:(QuizQuestionDomainObject*) questionDomainObject andQuestionNumber:(int) questionNo;

@end

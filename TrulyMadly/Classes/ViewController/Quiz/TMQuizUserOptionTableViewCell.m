//
//  TMQuizUserOptionTableViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizUserOptionTableViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface TMQuizUserOptionTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *userOptionTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userOptionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;

@end

#define FEMALE_TEXT_COLOR [UIColor colorWithRed:209.0/255.0 green:12.0/255.0 blue:108.0/255.0 alpha:1.0]
#define MALE_TEXT_COLOR [UIColor colorWithRed:124.0/255.0 green:160.0/255.0 blue:209.0/255.0 alpha:1.0]


@implementation TMQuizUserOptionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) fillUserOptionDetails:(QuizOptionDomainObject*) optionDomainObject profilePicURL:(NSString*) profilePicURL isAnswerCommon:(BOOL) isAnswerCommon isUserAnswer:(BOOL) isUserAnswer isFemaleGender:(BOOL) isFemaleGender
{
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.bounds.size.width/2;
    self.userProfileImageView.clipsToBounds = YES;
    
    //show different UI
    
    BOOL isUserFemale = isFemaleGender;
    
    self.userProfileImageView.image = nil;
    [self.userProfileImageView setImageWithURL:[NSURL URLWithString:profilePicURL] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    self.userOptionTextLabel.text = optionDomainObject.optionText;
    
    //setting the text color
    if (isUserFemale) {
        if (isUserAnswer) {
            self.userOptionTextLabel.textColor = FEMALE_TEXT_COLOR;
        }
        else {
            self.userOptionTextLabel.textColor = MALE_TEXT_COLOR;
        }
    }
    else {
        if (isUserAnswer) {
            self.userOptionTextLabel.textColor = MALE_TEXT_COLOR;
        }
        else {
            self.userOptionTextLabel.textColor = FEMALE_TEXT_COLOR;
        }
    }
    [self.userOptionImageView setImage:nil];
    [self.userOptionImageView setImageWithURL:[NSURL URLWithString:optionDomainObject.optionImageURL]];
    self.userOptionImageView.layer.cornerRadius = self.userOptionImageView.bounds.size.width/2;
    self.userOptionImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

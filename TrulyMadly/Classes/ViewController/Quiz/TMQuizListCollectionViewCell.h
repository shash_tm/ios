//
//  TMQuizListCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 29/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMQuizListCollectionViewCell : UICollectionViewCell

-(void)setQuizIconFromURL:(NSURL*)imageURL;
- (void) resetQuizUserImages;
- (void) setQuizCommonStatusFromImageURLString:(NSString*) commonImageURLString;
- (void) setQuizBothPlayedStatusFromUserImageURLString:(NSString*) userImageURLString matchImageURLString:(NSString*) matchImageURLString;
-(void)setQuizName:(NSString*)name;
-(void)setQuizNewStatus:(BOOL) quizNewStatus;
- (void) setFlareStatus:(BOOL) flareStatus;
- (void) setQuizUnreadStatus:(BOOL) unreadStatus;
- (void) setCommonImagePlaceholder;
- (void) setBothImagePlaceholder;
- (void) setBothImageWithUserProfilePlaceholderAndMatchImageURLString:(NSString*) matchImageURLString;
- (void) setBothImageWithMatchProfilePlaceholderAndUserImageURLString:(NSString*) userImageURLString;
- (void) resetQuizState;

@end

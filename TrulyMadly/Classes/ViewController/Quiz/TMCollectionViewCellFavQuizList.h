//
//  TMCollectionViewCellFavQuizList.h
//  TrulyMadly
//
//  Created by Ankit on 24/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMFavTag;

@protocol TMCollectionViewCellFavQuizListDelegate;

@interface TMCollectionViewCellFavQuizList : UICollectionViewCell

@property(nonatomic,weak)id<TMCollectionViewCellFavQuizListDelegate> delegate;
@property(nonatomic,strong)NSString *key;

-(void)setDataWithArray:(NSArray*)list;

@end

@protocol TMCollectionViewCellFavQuizListDelegate <NSObject>

@optional
-(void)didSelectItem:(NSString *)str withKey:(NSString*)key;
-(void)didSelecFavTag:(TMFavTag*)tag withKeyIdentifier:(NSString*)keyIndentifier;

@end
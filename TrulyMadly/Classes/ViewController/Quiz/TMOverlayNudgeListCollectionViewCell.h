//
//  TMOverlayNudgeListCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 16/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMOverlayBaseCollectionViewCell.h"

@protocol TMOverlayNudgeListCellDelegate <NSObject>

- (void) nudgePressedForMatchID:(NSString*) matchID;

@end

@interface TMOverlayNudgeListCollectionViewCell : TMOverlayBaseCollectionViewCell

@property (nonatomic, weak) id<TMOverlayNudgeListCellDelegate> delegate;

- (IBAction)nudgeButtonPressed:(UIButton *)sender;
- (void) setNudgeSelectionState:(BOOL) isSelected;
- (void) setUpCellWithIndexPath:(NSIndexPath*) indexPath;

@end

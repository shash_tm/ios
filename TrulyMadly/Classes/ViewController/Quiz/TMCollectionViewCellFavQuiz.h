//
//  TMCollectionViewCellFavQuiz.h
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMCollectionViewCellFavQuizDelegate;

@interface TMCollectionViewCellFavQuiz : UICollectionViewCell

@property(nonatomic,weak)id<TMCollectionViewCellFavQuizDelegate> delegate;
@property(nonatomic,strong)UITextField *tf;
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,weak)id textFieldDelegate;

-(void)setActiveIndexPath:(NSIndexPath*)indexPath;

-(void)setPlaceholderText:(NSString*)text;

@end

@protocol TMCollectionViewCellFavQuizDelegate <NSObject>

-(void)didBeginEditingTextInTextFieldWithIndexPath:(NSIndexPath*)indexPath;
-(void)didFinishEditingTextInTextFieldWithIndexPath:(NSIndexPath*)indexPath;

@end
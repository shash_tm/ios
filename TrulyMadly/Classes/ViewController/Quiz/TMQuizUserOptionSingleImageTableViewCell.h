//
//  TMQuizUserOptionSingleImageTableViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 29/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizQuestionDomainObject.h"


@interface TMQuizUserOptionSingleImageTableViewCell : UITableViewCell

- (void) fillDetailsWithUserProfileImageURLString:(NSString*) userProfileImageString optionImageURLString:(NSString*) optionImageURLString;

@end

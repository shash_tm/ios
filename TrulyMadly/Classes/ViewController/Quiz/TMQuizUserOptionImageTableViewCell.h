//
//  TMQuizUserOptionImageTableViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 18/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizQuestionDomainObject.h"

@interface TMQuizUserOptionImageTableViewCell : UITableViewCell

- (void) fillDetailsWithQuestionDomainObject:(QuizQuestionDomainObject*) quizQuestionDomainObject userProfileImageURL:(NSString*) userProfileImageURL matchProfileImageURL:(NSString*) matchProfileImageURL userName:(NSString*) userName matchName:(NSString*) matchName isFemale:(BOOL) isFemale;

@end

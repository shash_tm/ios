//
//  TMFavQuizCollectionFooterView.m
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFavQuizCollectionFooterView.h"

@implementation TMFavQuizCollectionFooterView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(10, (frame.size.height - 44)/2, frame.size.width-20, 44);
        btn.backgroundColor = [UIColor colorWithRed:249/255.0f green:178/255.0f blue:183/255.0f alpha:0.95];
        [btn setTitle:@"Done!" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(saveFavouriteAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
    
    return self;
}

-(void)saveFavouriteAction {
    [self.delegate saveFavAction];
}


@end

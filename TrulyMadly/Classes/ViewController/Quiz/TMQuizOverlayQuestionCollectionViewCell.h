//
//  TMQuizOverlayQuestionCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "TMOverlayBaseCollectionViewCell.h"

@interface TMQuizOverlayQuestionCollectionViewCell : TMOverlayBaseCollectionViewCell

@end

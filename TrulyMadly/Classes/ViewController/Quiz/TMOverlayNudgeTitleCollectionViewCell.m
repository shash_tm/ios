//
//  TMOverlayNudgeTitleCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 16/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMOverlayNudgeTitleCollectionViewCell.h"

@interface TMOverlayNudgeTitleCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nudgeTitleLabel;

@end

@implementation TMOverlayNudgeTitleCollectionViewCell

- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary
{
    self.layer.borderWidth = 0.6f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)awakeFromNib {
    // Initialization code
}

@end

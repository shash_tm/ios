//
//  QuizDanceButton.m
//  QuizDanceButton
//
//  Created by Abhijeet Mishra on 21/06/15.
//  Copyright (c) 2015 Abhijeet Mishra. All rights reserved.
//

#import "TMQuizAnimatableButton.h"

@interface TMQuizAnimatableButton ()

@property (nonatomic, strong) UIImageView* gamesBoundaryView;
@property (nonatomic, strong) UIImageView* blooperImageView;

@end

@implementation TMQuizAnimatableButton

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:CGRectMake(0, 0, 40, 27)]) {
        
        self.gamesBoundaryView = [[UIImageView alloc]initWithFrame:self.bounds];
        [self.gamesBoundaryView setImage:[UIImage imageNamed:@"quizicon"]];
        [self addSubview:self.gamesBoundaryView];
        self.gamesBoundaryView.center = self.center;
        
        self.blooperImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width/4, self.bounds.size.width/4)];
        self.blooperImageView.image = [UIImage imageNamed:@"notification"];
        self.blooperImageView.center = CGPointMake(self.bounds.size.width - self.bounds.size.width/8, 0);
        self.blooperImageView.hidden = YES;
        [self addSubview:self.blooperImageView];

        [self addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        //add observer for the new quiz notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newQuizNotificationReceived:) name:@"newQuizAvailable" object:nil];
    }
    return self;
}

- (void) didMoveToWindow {
    
}

- (void) newQuizNotificationReceived:(NSNotification*) notification
{
    int oppositeMatchID = [[notification object] intValue];
    
    if (self.tag == oppositeMatchID || oppositeMatchID == -1) {
      [self setUnreadQuizStatus:YES];   
    }
}

- (void) setUnreadQuizStatus:(BOOL) unreadQuizStatus
{
    self.blooperImageView.hidden = !unreadQuizStatus;
}

- (void) buttonPressed:(UIButton*) button
{
    [UIView animateWithDuration:0.4 delay:3.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.blooperImageView.hidden = YES;
    } completion:nil];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

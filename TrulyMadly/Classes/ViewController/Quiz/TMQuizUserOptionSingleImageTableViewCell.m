//
//  TMQuizUserOptionSingleImageTableViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 29/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizUserOptionSingleImageTableViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface TMQuizUserOptionSingleImageTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userOptionImageView;


@end

@implementation TMQuizUserOptionSingleImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    //user profile image view
    self.userProfileImageView.clipsToBounds = YES;
    self.userProfileImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userProfileImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                            | UIViewAutoresizingFlexibleHeight
                                            | UIViewAutoresizingFlexibleLeftMargin
                                            | UIViewAutoresizingFlexibleRightMargin
                                            | UIViewAutoresizingFlexibleTopMargin
                                            | UIViewAutoresizingFlexibleWidth);
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.bounds.size.width/2;
    self.userProfileImageView.layer.borderWidth = 1.5f;
    self.userProfileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //user option image view
    self.userOptionImageView.clipsToBounds = YES;
    self.userOptionImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userOptionImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                            | UIViewAutoresizingFlexibleHeight
                                            | UIViewAutoresizingFlexibleLeftMargin
                                            | UIViewAutoresizingFlexibleRightMargin
                                            | UIViewAutoresizingFlexibleTopMargin
                                            | UIViewAutoresizingFlexibleWidth);
    self.userOptionImageView.layer.cornerRadius = self.userOptionImageView.bounds.size.width/2;
    self.userOptionImageView.layer.borderWidth = 1.5f;
    self.userOptionImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //self view
    self.clipsToBounds = YES;
    
    //a bit of customization
    self.userOptionImageView.center = CGPointMake(self.center.x, self.center.y);
    self.userProfileImageView.center = CGPointMake(self.userProfileImageView.center.x, self.center.y);
}

- (void) fillDetailsWithUserProfileImageURLString:(NSString*) userProfileImageString optionImageURLString:(NSString*) optionImageURLString
{
    //setting the profile image
    [self.userProfileImageView setImageWithURL:[NSURL URLWithString:userProfileImageString] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    
    //setting the option image
    [self.userOptionImageView setImageWithURL:[NSURL URLWithString:optionImageURLString] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
}

@end

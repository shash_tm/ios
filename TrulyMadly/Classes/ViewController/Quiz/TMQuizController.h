//
//  TMQuizController.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 18/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class TMQuizInfo;

typedef enum {
    femaleGender,
    maleGender
} userGender;

typedef enum {
    optionTypeAllImages,
    optionTypeOthers
} optionType;

typedef enum {
    quizPlayedByNeither,
    quizPlayedByUser,
    quizPlayedByMatch,
    quizPlayedByBoth
} quizStatus;


@protocol TMQuizControllerDelegateProtocol <NSObject>

- (void) handleLogoutFromQuiz;

@end

@interface TMQuizController : NSObject

@property (nonatomic, weak) id<TMQuizControllerDelegateProtocol> delegate;

- (instancetype) initWithMatchName:(NSString*) matchName matchID:(NSString*) matchID;

- (userGender) getUserGender;
- (quizStatus) getQuizStatusFromDBForQuizID:(NSString*) quizID;
- (void) decideQuizActionForQuizID:(NSString*) quizID withBaseViewController:(UIViewController*) baseViewController;
- (void) decideQuizActionFromChatForQuizID:(NSString*) quizID withBaseViewController:(UIViewController*) baseViewController;
- (void) decideQuizActionFromOverlayForQuizID:(NSString*) quizID;
- (BOOL) isCommonAnswerRequiredAfterNudgeQuizID:(NSString*) quizID;
- (void) saveQuizWithQuizID:(NSString*) quizID answerDictionary:(NSDictionary*) answerDictionary;
- (UIImage*) statusImageForQuizID:(NSString*) quizID;
- (NSArray*) getAllQuizzesFromDB;
- (NSArray*) getAllQuizzesFromDBForLatestQuizVersion;
- (void) fetchUpdatedQuizList;
- (void) fetchPlayedQuizzesForMatchID:(NSString*) matchID;
- (NSString*) getUserProfileImageURL;
- (NSString*) getUserID;
- (void) performOneToOneNudgeWithQuizID:(NSString*) quizID quizName:(NSString*) quizName;
- (void) nudgeUserWithUserArray:(NSArray*) nudgeUserArray quizID:(NSString*) quizID quizName:(NSString*) quizName;
- (void) sendCommonAnswerWithMessageText:(NSString*) text quizID:(NSString*) quizID isFlare:(BOOL) isFlare;
- (TMQuizInfo*)getQuinInfoForQuizId:(NSString*)quizId;
- (NSDictionary*) getCurrentQuizInfoDictionary;
- (void) setIsNudgeActionCancelled:(BOOL)isNudgeActionCancelled;
- (void) updateQuizNewStatus:(BOOL) isNewQuiz forQuizID:(NSString*) quizID;
- (BOOL) isFlareForQuizID:(NSString*) quizID;
- (BOOL) isNewQuizAvailable;
- (void) updateQuizStatusForOppositeMatch;
- (void) saveUpdatedStatusWithUpdatedQuizIDArray:(NSArray*) quizIDArray;
- (BOOL) isQuizStatusUpdatedForOppositeMatch;
- (BOOL) isQuizStatusUpdatedForOppositeMatchWithQuizID:(NSString*) quizID;
- (void) resetQuizStatusForQuizID:(NSString*) quizID;

- (void) makeQuizRequestForStatusAndFlare;

- (NSArray*) getUnreadQuizzesForMatchID:(NSString*) matchID userID:(NSString*) userID;
- (BOOL) removeQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID quizID:(NSString*) quizID;
- (BOOL) removeAllQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID;
- (NSArray*) getAllFlareQuizzesForMatchID:(NSString*) matchID;
- (NSArray*) getAllQuizzesPlayedByUserID:(NSString*) userID;


@end

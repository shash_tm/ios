//
//  TMQuizCommonAnswerTableViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 27/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizQuestionDomainObject.h"

@interface TMQuizCommonAnswerTableViewCell : UITableViewCell

- (void) updateCommonAnswerInfoForUserAnswer:(QuizOptionDomainObject*) userAnswer matchAnswer:(QuizOptionDomainObject*) matchAnswer userProfilePicURL:(NSString*) userProfilePicURL matchProfilePicURL:(NSString*) matchProfilePicURL;

@end

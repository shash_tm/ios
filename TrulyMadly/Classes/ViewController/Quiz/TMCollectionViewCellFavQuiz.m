//
//  TMCollectionViewCellFavQuiz.m
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMCollectionViewCellFavQuiz.h"

@interface TMCollectionViewCellFavQuiz ()

@property(nonatomic,weak)NSIndexPath *indexPath;

@end


@implementation TMCollectionViewCellFavQuiz

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.tf = [[UITextField alloc] initWithFrame:self.bounds];
        self.tf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        //self.tf.tag = 100001;
        self.tf.borderStyle = UITextBorderStyleRoundedRect;
        self.tf.autocorrectionType = UITextAutocorrectionTypeNo;
        //self.tf.adjustsFontSizeToFitWidth = true;
        //self.tf.placeholder = @"Sample Text";
        //tf.delegate = self;
        [self.contentView addSubview:self.tf];
        
        //work image view
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(8, self.tf.frame.origin.y, 22, 44)];
        self.tf.leftView = paddingView;
        self.tf.leftViewMode = UITextFieldViewModeAlways;
        
        self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(8, self.tf.frame.origin.y+14.5,18, 15)];
        [self.contentView addSubview:self.icon];
        
    }
    
    return self;
}

-(void)setActiveIndexPath:(NSIndexPath*)indexPath {
    self.indexPath = indexPath;
}

-(void)setPlaceholderText:(NSString*)text {
    UITextField *tf = (UITextField*)[self.contentView viewWithTag:100001];
    tf.placeholder = text;
}

-(void)setTextFieldDelegate:(id<UITextFieldDelegate>)textFieldDelegate {
    //UITextField *tf = (UITextField*)[self.contentView viewWithTag:100001];
    //tf.delegate = self.delegate;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

//#pragma mark UITextfield delegate methods -
//
//-(void)textFieldDidBeginEditing:(UITextField *)textField {
//    [self.delegate didBeginEditingTextInTextFieldWithIndexPath:self.indexPath];
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [self.delegate didFinishEditingTextInTextFieldWithIndexPath:self.indexPath];
//    [textField resignFirstResponder];
//    return YES;
//}

@end

//
//  TMQuizOverlayViewController.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 09/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizOverlayViewController.h"
#import "TMActivityIndicatorView.h"
#import "TMMessagingController.h"
#import "TMQuizCacheManager.h"
#import "TMQuizQuestions.h"
#import "TMOverlayBaseCollectionViewCell.h"
#import "TMQuizOverlayQuizDescCollectionViewCell.h"
#import "TMQuizOverlayQuestionCollectionViewCell.h"
#import "TMQuizOverlayOptionCollectionViewCell.h"
#import "TMOverlayNudgeListCollectionViewCell.h"
#import "TMOverlayNudgeTitleCollectionViewCell.h"
#import "TMQuizUserAnswerView.h"
#import "NSString+TMAdditions.h"
#import "QuizQuestionDomainObject.h"
#import "TMRetryView.h"
#import "TMLog.h"
#import "NSString+TMAdditions.h"
#import "UIImageView+AFNetworking.h"
#import "TMQuizIntroScreenView.h"
#import "TMQuizNudgeScreenView.h"

typedef enum {
    quizNoFailure,
    quizQuestionFailure,
    commonAnswerFailure,
    saveQuizFailure,
}failureState;

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

#define NAVIGATION_BUTTON_ENABLED_COLOR [UIColor colorWithRed:209.0/255.0 green:12.0/255.0 blue:108.0/255.0 alpha:1.0]
#define NAVIGATION_BUTTON_DISABLED  [UIColor darkGrayColor]

#define QUIZ_COLLECTION_VIEW_TAG_CONSTANT 98789
#define QUIZ_BASE_VIEW_TAG 8767
#define QUESTION_ID @"question_id"

@interface TMQuizOverlayViewController () <TMQuizOverlayOptionDelegate,TMOverlayNudgeListCellDelegate,UIAlertViewDelegate,TMQuizAnswerViewDelegate,UIScrollViewDelegate,TMQuizNudgeScreenDelegate>

//UI elements
@property (strong, nonatomic) UIScrollView *quizBaseScrollView;
@property (strong, nonatomic) UIView *quizBaseView;
@property (strong, nonatomic) UIView *quizTitleView;
@property (strong, nonatomic)  UICollectionView *nudgeCollectionView;
@property (strong, nonatomic) TMQuizUserAnswerView* commonAnswerViewScreen;
@property (weak, nonatomic) IBOutlet UIView *baseBlurredView;
@property (weak, nonatomic) IBOutlet UIButton *overlayNavigationButton;
@property (nonatomic) TMActivityIndicatorView* activityIndicatorView;
@property (strong, nonatomic) TMRetryView* retryView;
@property (weak, nonatomic) IBOutlet UIView *overlayBaseView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *questionProgressLabel;

//data and state values
@property (nonatomic, strong) TMQuizInfo* currentQuizInfo;
@property (nonatomic) TMQuizQuestions* currentQuestionsList;
@property (nonatomic) NSMutableDictionary* answerDict;
@property (nonatomic, assign) optionType currentOptionType;
@property (nonatomic, strong) NSDictionary* commonAnswerDictionary;
@property (nonatomic, assign) failureState currentFailureState;
@property (nonatomic, strong) NSString* currentFailureMessage;
@property (nonatomic, strong) NSNotification* commonAnswerInfoNotificationAfterSaveQuiz;

//nudge list propery
@property (weak, nonatomic) IBOutlet UIView *nudgeBaseView;
@property (weak, nonatomic) IBOutlet UIView *nudgeTitleView;
@property (nonatomic, strong) NSArray* nudgeUserArray;
@property (nonatomic, strong) NSMutableArray* selectedNudgeUserArray;
@property (weak, nonatomic) IBOutlet UIImageView *nudgeTitleQuizImageView;

//intro screen elements
@property (strong, nonatomic) TMQuizIntroScreenView *introScreen;
@property (nonatomic, assign) BOOL hasIntroScreenStartButtonBeenPressed;

@end

@implementation TMQuizOverlayViewController

#define FINISH_BUTTON_TITLE @"Finish"
#define NUDGE_BUTTON_TITLE @"Nudge"

#define MINIMUM_QUESTION_CELL_HEIGHT 60
#define MINIMUM_IMAGE_OPTION_HEIGHT ([UIScreen mainScreen].bounds.size.height < 500)?110:140
#define MINIMUM_TEXT_OPTION_CELL_HEIGHT 30

#define QUIZ_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"QuizOverlayCollectionViewCell"
#define QUESTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"QuestionOverlayCollectionViewCell"
#define OPTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"OptionOverlayCollectionViewCell"

#define NUDGE_TITLE_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"NudgeTitleCollectionViewCell"
#define NUDGE_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"NudgeListCollectionViewCell"

#define ACTIVITY_INDICATOR_BACKGROUND_VIEW_TAG 12321


- (instancetype) initWithNibName:(NSString*) nibNameOrNil bundle:(NSBundle*) nibBundleOrNil quizID:(NSString*)quizID blurredImage:(UIImage*) blurredImage loaderMessage:(NSString*) loaderMessage welcomeScreenToBeShown:(BOOL) isWelcomeScreeenToBeShown quizController:(TMQuizController*) quizController
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:blurredImage]];
        
        self.quizController = quizController;
        
        //fetch the quiz info
        TMQuizInfo* quizInfo = [self.quizController getQuinInfoForQuizId:quizID];
        self.currentQuizInfo = quizInfo;
        
        if (IS_IPHONE_6) {
            //increase the font size for the question progress label
            self.questionProgressLabel.font = [UIFont boldSystemFontOfSize:15];
        }
        
        else if (IS_IPHONE_6_PLUS) {
            self.questionProgressLabel.font = [UIFont boldSystemFontOfSize:13];
        }
        
        if (!isWelcomeScreeenToBeShown) {
            //show activity indicator view
            [self showActivityIndicatorViewWithMessage:loaderMessage];
        }
        else {
            //show the welcome screen
            [self displayIntroScreen];
        }
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    //create borders for the overlay navigation button
    self.overlayNavigationButton.layer.borderWidth = 1.0f;
    self.overlayNavigationButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self.nudgeCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"nudgeFooter"];
    
    //add observer for get available quizzes response
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(displayQuizQuestions:) name:@"quizQuestionsReceived" object:nil];
    
    //add observer for get common answers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(displayCommonAnswers:) name:@"commonAnswersReceived" object:nil];
    
    //add observer for save answer and nudge response
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveQuizResponseReceived:) name:@"saveQuizResponseReceived" object:nil];
    
    //add observer for save answer failure
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveAnswerFailureReceived:) name:@"saveQuizFailed" object:nil];
    
    //adding observer for quiz failure due to version change
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quizActionFailedDueDelayedResponse) name:@"quizVersionChanged" object:nil];
    
    //add observer for decide action failure handling
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decideQuizFailureReceived:) name:@"quizDecideActionFailed" object:nil];
    
    //add observer for logout action
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quizActionLogoutReceived:) name:@"quizLogoutResponseReceived" object:nil];
    
    //add observer for nudge handling //slow network use cases
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quizActionFailedDueDelayedResponse) name:@"autoNudgeResponseRecievedFromChat" object:nil];
    
    //add observer for nudge handling //slow network use cases
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quizActionFailedDueDelayedResponse) name:@"autoNudgeResponseRecieved" object:nil];
    
    //add observer for nudge handling //slow network use cases
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissOverlayView) name:@"quizLogoutResponseReceived" object:nil];
}


- (void) viewWillAppear:(BOOL)animated
{
    UIColor *colorOne = [UIColor colorWithRed:172.0/255.0 green:198.0/255.0 blue:229.0/255.0 alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:255.0/255.0 green:179.0/255.0 blue:185.0/255.0 alpha:1.0];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.frame = self.presentingViewController.view.window.bounds;
    [self.overlayBaseView.layer insertSublayer:headerLayer atIndex:0];
}

/**
 * Registers the collection view cells for the quiz collection view
 */
- (void) registerQuizCollectionViewCellsForCollectionView:(UICollectionView*) collectionView
{
    //Collection View
    //registering class for the reuse identifiers
    [collectionView registerClass:[TMQuizOverlayQuizDescCollectionViewCell class] forCellWithReuseIdentifier:QUIZ_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [collectionView registerClass:[TMQuizOverlayQuestionCollectionViewCell class] forCellWithReuseIdentifier:QUESTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [collectionView registerClass:[TMQuizOverlayOptionCollectionViewCell class] forCellWithReuseIdentifier:OPTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    
    //registering for custom nibs
    [collectionView registerNib:[UINib nibWithNibName:@"TMQuizOverlayQuizDescCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:QUIZ_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [collectionView registerNib:[UINib nibWithNibName:@"TMQuizOverlayQuestionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:QUESTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [collectionView registerNib:[UINib nibWithNibName:@"TMQuizOverlayOptionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:OPTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
}


/**
 * Registers the collection view cells for the quiz collection view
 */
- (void) registerNudgeCollectionViewCells
{
    //Collection View
    //registering class for the reuse identifiers
    [self.nudgeCollectionView registerClass:[TMOverlayNudgeTitleCollectionViewCell class] forCellWithReuseIdentifier:NUDGE_TITLE_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [self.nudgeCollectionView registerClass:[TMOverlayNudgeListCollectionViewCell class] forCellWithReuseIdentifier:NUDGE_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    
    //registering for custom nibs
    [self.nudgeCollectionView registerNib:[UINib nibWithNibName:@"TMOverlayNudgeTitleCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NUDGE_TITLE_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [self.nudgeCollectionView registerNib:[UINib nibWithNibName:@"TMOverlayNudgeListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NUDGE_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    
    //set minimum spacing
    ((UICollectionViewFlowLayout*)self.nudgeCollectionView.collectionViewLayout).minimumInteritemSpacing = 0;
    ((UICollectionViewFlowLayout*)self.nudgeCollectionView.collectionViewLayout).minimumLineSpacing = 0;
}


/**
 * Callback method for get quiz questions response
 * @param the notification object containing quiz questions
 */
- (void) displayQuizQuestions:(NSNotification*) notification
{
    self.currentFailureState = quizNoFailure;
    
    //hide the activity indicator
    [self hideActivityIndicatorView];
    
    if (notification && [[notification object] isMemberOfClass:[TMQuizQuestions class]]) {
        TMQuizQuestions* currentQuizQuestions = [notification object];
        if ([currentQuizQuestions.quizID isEqualToString:self.currentQuizInfo.quizID]) {
            //response received for current quiz ID
            
            self.currentQuestionsList = currentQuizQuestions;
         
            //post notification for option image caching
            [[NSNotificationCenter defaultCenter] postNotificationName:@"quizOptionsRecievedForOptionImageFetching" object:self.currentQuestionsList.questionList];
            
            if (self.introScreen) {
                if (self.hasIntroScreenStartButtonBeenPressed) {
                   [self createQuizQuestionsUI];
                }
            }
            else {
                //display the intro screen here
                [self displayIntroScreen];
            }
        }
    }
}


- (void) createQuizQuestionsUI
{
    //hide intro screen
    self.introScreen.hidden = YES;
    
    CGFloat quizTopMargin = IS_IPHONE_6_PLUS?44:(IS_IPHONE_6?38:([UIScreen mainScreen].bounds.size.height > 500)?36:0);
    
    //show the quiz base view
    self.quizBaseView = [[UIView alloc] initWithFrame:CGRectMake(self.overlayBaseView.bounds.origin.x, self.overlayBaseView.bounds.origin.y + quizTopMargin, self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.size.height - quizTopMargin)];
    self.quizBaseView.backgroundColor = [UIColor clearColor];
    self.quizBaseView.tag = QUIZ_BASE_VIEW_TAG;
    [self.view addSubview:self.quizBaseView];
    
    //show the quiz title view
    self.quizTitleView = [[UIView alloc] initWithFrame:CGRectMake(self.overlayBaseView.bounds.origin.x, self.overlayBaseView.bounds.origin.y, self.overlayBaseView.bounds.size.width, (self.overlayBaseView.bounds.size.height*2)/10)];
    self.quizTitleView.backgroundColor = [UIColor clearColor];
    
    int imageViewHeight = MIN(self.overlayBaseView.bounds.size.width/3, self.overlayBaseView.bounds.size.height/5 - (([UIScreen mainScreen].bounds.size.height > 500)?24:16));
    
    UIImageView* quizLogoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageViewHeight, imageViewHeight)];
    quizLogoImageView.center = CGPointMake(self.quizTitleView.bounds.size.width/2, self.quizTitleView.bounds.size.height*3/5);
    quizLogoImageView.layer.cornerRadius = quizLogoImageView.bounds.size.width/2;
    quizLogoImageView.clipsToBounds = YES;
    quizLogoImageView.backgroundColor = [UIColor clearColor];
    //fetch thh quiz info
    TMQuizInfo* quizInfo = [self.quizController getQuinInfoForQuizId:self.currentQuizInfo.quizID];
    [quizLogoImageView setImageWithURL:[NSURL URLWithString:quizInfo.quizIconURL] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
    [self.quizTitleView addSubview:quizLogoImageView];
    [self.quizBaseView addSubview:self.quizTitleView];
    
    int progressLabelMargin = ([UIScreen mainScreen].bounds.size.height > 600)?48:32;
    
    //show the quiz base scroll view
    self.quizBaseScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.overlayBaseView.bounds.origin.x, self.overlayBaseView.bounds.origin.y + self.quizTitleView.bounds.size.height, self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.size.height - self.quizTitleView.bounds.size.height - progressLabelMargin - quizTopMargin)];
    self.quizBaseScrollView.pagingEnabled = YES;
    self.quizBaseScrollView.backgroundColor = [UIColor clearColor];
    self.quizBaseScrollView.delegate = self;
    self.quizBaseScrollView.showsHorizontalScrollIndicator = NO;
    self.quizBaseScrollView.showsVerticalScrollIndicator = NO;
    self.quizBaseScrollView.contentSize = CGSizeMake(self.overlayBaseView.bounds.size.width*self.currentQuestionsList.questionList.count, self.overlayBaseView.bounds.size.height - self.quizTitleView.bounds.size.height - progressLabelMargin - quizTopMargin);
    [self.quizBaseView addSubview:self.quizBaseScrollView];
    
    //update the progress label
    self.questionProgressLabel.hidden = NO;
    self.questionProgressLabel.text = [NSString stringWithFormat:@"%d/%d",1,(int)self.currentQuestionsList.questionList.count];
    
    
    //adding cover gesture
    UIPanGestureRecognizer* coverPanGesture = [[UIPanGestureRecognizer  alloc] initWithTarget:nil action:nil];
    [self.quizBaseScrollView addGestureRecognizer:coverPanGesture];
    
    for (int index = 0; index < self.currentQuestionsList.questionList.count; index++){
        UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        
        BOOL isImageOption = [[[[self.currentQuestionsList.questionList objectAtIndex:index] objectForKey:@"options"] firstObject] objectForKey:@"option_text"]?NO:YES;
        
        if (isImageOption) {
            flowLayout.footerReferenceSize = CGSizeMake(0,0);
        }
        else {
            flowLayout.footerReferenceSize = CGSizeMake(12, 12);
        }
        //add all the quiz question collection views
        UICollectionView* quizQuestionCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.overlayBaseView.bounds.origin.x + index*self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.origin.y, self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.size.height - self.quizTitleView.bounds.size.height - progressLabelMargin) collectionViewLayout:flowLayout];
        quizQuestionCollectionView.backgroundColor = [UIColor clearColor];
        quizQuestionCollectionView.dataSource = self;
        quizQuestionCollectionView.delegate = self;
        quizQuestionCollectionView.tag = (index + 1)*QUIZ_COLLECTION_VIEW_TAG_CONSTANT;
        [self registerQuizCollectionViewCellsForCollectionView:quizQuestionCollectionView];
        [self.quizBaseScrollView addSubview:quizQuestionCollectionView];
        
        [self loadQuizQuestionForIndex:index forCollectionView:quizQuestionCollectionView];
    }
    
    [self.view bringSubviewToFront:self.cancelButton];
    
    
}


/**
 * Method to display the welcome screen
 */
- (void) displayIntroScreen
{
    self.introScreen.hidden = NO;
    
    //fetch the quiz info
    TMQuizInfo* quizInfo = [self.quizController getQuinInfoForQuizId:self.currentQuizInfo.quizID];
    
    //update the UI accordingly
    self.introScreen = (TMQuizIntroScreenView*)[[[NSBundle mainBundle] loadNibNamed:@"TMQuizIntroScreenView" owner:self options:nil] firstObject];
    self.introScreen.frame = CGRectMake(self.overlayBaseView.frame.origin.x, self.overlayBaseView.frame.origin.y - 5, self.overlayBaseView.frame.size.width, self.overlayBaseView.frame.size.height + 45);
    [self.introScreen fillQuizIntroDataWithQuizName:quizInfo.quizDisplayName quizImageURLString:quizInfo.quizIconURL quizDetailTextString:quizInfo.quizDescription quizSponsoredText:quizInfo.quizSponsoredText withQuizQuestionsFetchStateBlock:^BOOL(void){
        return (self.currentQuestionsList == nil);
    } startButtonPressBlock:^(BOOL quizQuestionsDownloaded){
        if (quizQuestionsDownloaded) {
            [self createQuizQuestionsUI];
        }
        else {
            self.hasIntroScreenStartButtonBeenPressed = YES;
            if (self.currentFailureState == quizQuestionFailure) {
                //failure has been detected
                //show retry view
                [self showRetryViewWithMessage:self.currentFailureMessage?self.currentFailureMessage:@"Save quiz failed."];
            }
        }
    } retryActionBlock:^{
        
        //hide the retry view
       // [self removeRetryView];
        
        //perform decide quiz api action
        [self.quizController performSelector:@selector(decideQuizActionFromOverlayForQuizID:) withObject:self.currentQuizInfo.quizID afterDelay:0];
    }];
    self.introScreen.center = CGPointMake(self.overlayBaseView.center.x, self.overlayBaseView.center.y - 5);
    [self.view addSubview:self.introScreen];
    
    [self.view bringSubviewToFront:self.cancelButton];
    
    [self.quizController decideQuizActionFromOverlayForQuizID:self.currentQuizInfo.quizID];
}


/**
 * Method to obtain info whether common answer are to displayed after quiz or not
 * @return value indicating whether common answer are required or not
 */
- (BOOL) isCommonAnswerRequiredAfterNudge
{
    return (self.commonAnswerInfoNotificationAfterSaveQuiz != nil);
}


/**
 * Callback method for get common answers response
 * @param the notification object containing common answers
 */
- (void) displayCommonAnswers:(NSNotification*) notification
{
    if ([self presentingViewController] && [[self presentingViewController] presentedViewController] == self) {
        self.currentFailureState = quizNoFailure;
        self.currentFailureMessage = nil;
        
        [self hideActivityIndicatorView];
        
        self.introScreen.hidden = YES;
        
        //preventive check to remove retry view
        if(self.retryView) {
            [self removeRetryView];
        }
        
        if (notification && notification.object) {
            NSDictionary* responseDictionary = [notification.object isKindOfClass:[NSDictionary class]]?notification.object:nil;
            
            //capture the response
            self.commonAnswerDictionary = responseDictionary;
            
            if([responseDictionary objectForKey:@"answers"] && ![[responseDictionary objectForKey:@"answers"] isKindOfClass:[NSNull class]]) {
                NSArray* commonQuestionsArray = [responseDictionary objectForKey:@"answers"];
                
                NSString* quizName = ([responseDictionary objectForKey:@"name"] && ![[responseDictionary objectForKey:@"name"] isKindOfClass:[NSNull class]])?[responseDictionary objectForKey:@"name"]:nil;
                
                NSString* quizID = ([responseDictionary objectForKey:@"quiz_id"] && ![[responseDictionary objectForKey:@"quiz_id"] isKindOfClass:[NSNull class]])?[responseDictionary objectForKey:@"quiz_id"]:nil;
                
                NSString* quizImageURL = ([responseDictionary objectForKey:@"image"] && ![[responseDictionary objectForKey:@"image"] isKindOfClass:[NSNull class]])?[responseDictionary objectForKey:@"image"]:nil;
                
                NSMutableArray* quizQuestionsArray = [[NSMutableArray alloc] init];
                
                for (int index = 0; index < commonQuestionsArray.count; index++) {
                    [quizQuestionsArray addObject:[[QuizQuestionDomainObject alloc] initWithCommonAnswersResponseDictionary:[commonQuestionsArray objectAtIndex:index]]];
                }
                
                BOOL isFlare = ([responseDictionary objectForKey:@"flare"] && ![[responseDictionary objectForKey:@"flare"] isKindOfClass:[NSNull class]])?[[responseDictionary objectForKey:@"flare"] boolValue]:NO;
                //show common answers
                [self showCommonAnswerScreenWithQuestionList:quizQuestionsArray quizID:quizID quizName:quizName quizImageURL:quizImageURL isFlare:isFlare];
            }
        }
    }
}


- (void) saveQuizResponseReceived:(NSNotification*) saveQuizResponseNotification {
   
    //check if response is of common answer type
    if ([saveQuizResponseNotification object] && [[saveQuizResponseNotification object] isKindOfClass:[NSDictionary class]] && (([((NSDictionary*)[saveQuizResponseNotification object]) objectForKey:@"answers"] && ![[((NSDictionary*)[saveQuizResponseNotification object]) objectForKey:@"answers"] isKindOfClass:[NSNull class]]))) {
        [self hideActivityIndicatorView];
        [self displayCommonAnswers:saveQuizResponseNotification];
    }
    else {
        [self hideActivityIndicatorView];
        
        //show the auto nudge screen
        TMQuizNudgeScreenView* nudgeScreenView = [[TMQuizNudgeScreenView alloc] initWithFrame:self.overlayBaseView.bounds delegate:self forMatchName:[self.delegate getCurrentMatchName] quizIconURLString:[self.quizController getQuinInfoForQuizId:self.currentQuizInfo.quizID].quizIconURL sponsoredImageURLString:self.currentQuizInfo.quizSponsoredImageURL quizSuccessMessage:self.currentQuizInfo.quizSuccessMessage quizSuccessNudge:self.currentQuizInfo.quizSuccessNudge];
        self.quizBaseScrollView.hidden = YES;
        self.quizTitleView.hidden = YES;
        [self.view addSubview:nudgeScreenView];
        //bring cross button to the front
        [self.view bringSubviewToFront:self.cancelButton];
    }
    //update the status
    [self.quizController makeQuizRequestForStatusAndFlare];
}


/**
 * Callback method for save quiz/nudge response
 * @param the notification object containing the nudge user list
 */
- (void) displayNudgeList:(NSNotification*) notification
{
    self.nudgeCollectionView.hidden = NO;
    
    //remove the activity view
    [self hideActivityIndicatorView];
    
    //update the DB here
    //after the save operation
    // [self.quizController fetchPlayedQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
    
    self.nudgeTitleQuizImageView.clipsToBounds = YES;
    self.nudgeTitleQuizImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.nudgeTitleQuizImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                             | UIViewAutoresizingFlexibleHeight
                                             | UIViewAutoresizingFlexibleLeftMargin
                                             | UIViewAutoresizingFlexibleRightMargin
                                             | UIViewAutoresizingFlexibleTopMargin
                                             | UIViewAutoresizingFlexibleWidth);
    
    //show the nudge collection view
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    
    //add all the quiz question collection views
    UICollectionView* nudgeCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.overlayBaseView.bounds.origin.x, self.overlayBaseView.bounds.origin.y + self.nudgeTitleView.bounds.size.height, self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.size.height - self.nudgeTitleView.bounds.size.height) collectionViewLayout:flowLayout];
    nudgeCollectionView.backgroundColor = [UIColor clearColor];
    nudgeCollectionView.dataSource = self;
    nudgeCollectionView.delegate = self;
    [self registerQuizCollectionViewCellsForCollectionView:nudgeCollectionView];
    [self.nudgeBaseView addSubview:nudgeCollectionView];
    [self.nudgeTitleQuizImageView setImageWithURL:[NSURL URLWithString:[notification.object valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
   
    self.nudgeTitleQuizImageView.layer.cornerRadius = self.nudgeTitleQuizImageView.bounds.size.width/2;
    
    if (notification && notification.object) {
        self.nudgeUserArray = ([[notification.object valueForKey:@"nudge_list"] isKindOfClass:[NSArray class]])?[notification.object valueForKey:@"nudge_list"]:nil;
        self.selectedNudgeUserArray = [self.nudgeUserArray mutableCopy];
        
        //hide the retry view if present
        if(self.retryView) {
            [self removeRetryView];
        }
        
        if (self.nudgeUserArray && self.nudgeUserArray.count > 0) {
            [self showNudgeList];
            [self.overlayNavigationButton setTitle:NUDGE_BUTTON_TITLE forState:UIControlStateNormal];
            
            CGSize contentSize = self.nudgeCollectionView.collectionViewLayout.collectionViewContentSize;
            contentSize.height += self.overlayNavigationButton.bounds.size.height;
            self.nudgeCollectionView.contentSize = contentSize;
            dispatch_async(dispatch_get_main_queue(), ^{
              [self.nudgeCollectionView reloadData];
            });
        }
    }
}


/**
 * Show the nudge list
 */
- (void) showNudgeList
{
    self.quizBaseView.hidden = YES;
    self.nudgeCollectionView.hidden = NO;
    [self.view bringSubviewToFront:self.cancelButton];
}


/**
 * show the common answer screen
 * @param questions list
 * @param quiz name
 * @param quiz image URL
 */
- (void) showCommonAnswerScreenWithQuestionList:(NSArray*) questions quizID:(NSString*) quizID quizName:(NSString*) quizName quizImageURL:(NSString*) quizImageURL isFlare:(BOOL) isFlare
{
    self.quizBaseView.hidden = YES;
    self.nudgeCollectionView.hidden = YES;
    
    self.commonAnswerViewScreen = (TMQuizUserAnswerView*)[[[NSBundle mainBundle] loadNibNamed:@"TMQuizUserAnswerView" owner:self options:nil] firstObject];
    self.commonAnswerViewScreen.frame = CGRectMake(self.overlayBaseView.frame.origin.x, self.overlayBaseView.frame.origin.y + 5, self.overlayBaseView.frame.size.width, self.overlayBaseView.frame.size.height);
    self.commonAnswerViewScreen.quizController = self.quizController;
    [self.commonAnswerViewScreen fillUserAnswerDetailsWithQuizID:quizID quizName:quizName quizImageURL:quizImageURL quizQuestionList:questions isFlare:isFlare];
    self.commonAnswerViewScreen.delegate = self;
    self.commonAnswerViewScreen.center = CGPointMake(self.overlayBaseView.center.x, self.overlayBaseView.center.y);
    [self.view addSubview:self.commonAnswerViewScreen];
    
    [self.commonAnswerViewScreen setUpToolBar];
        
    //TODO::Abhijeet::need to provide a cleaner solution to this view issue
    [self.view bringSubviewToFront:self.cancelButton];
}


/**
 * Method to move to the next quiz question
 */
- (void) moveToNextQuizQuestion
{
    int currentQuestionIndex = [self getCurrentQuizQuestionIndex];
    
    if (currentQuestionIndex == self.currentQuestionsList.questionList.count - 1) {
        //perform save operation
      [self performQuizSaveOperation];
    }
    
    else {
        if (currentQuestionIndex >= 0) {
            //valid question now
            //scroll to required question no.
            //move to next index
            currentQuestionIndex++;
            self.quizBaseScrollView.userInteractionEnabled = NO;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.quizBaseScrollView scrollRectToVisible:CGRectMake(currentQuestionIndex*self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.origin.y, self.overlayBaseView.bounds.size.width, self.overlayBaseView.bounds.size.height) animated:YES];
                    if ([self getCurrentQuizQuestionIndex] + 2 <= self.currentQuestionsList.questionList.count) {
                        //update the progress label
                        self.questionProgressLabel.hidden = NO;
                        self.questionProgressLabel.text = [NSString stringWithFormat:@"%d/%d",[self getCurrentQuizQuestionIndex] + 2,(int)self.currentQuestionsList.questionList.count];
                    }
                });
            });
        }
    }
}


/**
 * perform quiz save operation
 */
- (void) performQuizSaveOperation
{
//    //test code
  //  [self saveQuizResponseReceived:nil];
    
    self.quizBaseView.hidden = YES;
    
    self.questionProgressLabel.hidden = YES;
    
    [self.overlayNavigationButton setHidden:YES];
    
    [self showActivityIndicatorViewWithMessage:@"Yay we're done! Saving your answers.."];
    
    [self.quizController saveQuizWithQuizID:self.currentQuestionsList.quizID answerDictionary:self.answerDict];
}


/**
 * update the option-type of current quiz
 */
- (void) updateOptionTypeForQuestionIndex:(int) questionIndex
{
    self.currentOptionType = optionTypeAllImages;
    
    NSArray* currentOptionsArray = [[self.currentQuestionsList.questionList objectAtIndex:questionIndex] valueForKey:@"options"];
    
    BOOL isTextOptionPresent = NO;
    
    for (int index = 0; index < currentOptionsArray.count && !isTextOptionPresent; index++) {
        if ([[[currentOptionsArray objectAtIndex:index] valueForKey:@"option_text"] isKindOfClass:[NSString class]]) {
            //text present
            isTextOptionPresent = YES;
        }
    }
    
    if (isTextOptionPresent) {
        self.currentOptionType = optionTypeOthers;
    }
    
    //find the required collection view
    //find the appropriate collection view
    int requiredCollectionViewTag = QUIZ_COLLECTION_VIEW_TAG_CONSTANT*(questionIndex + 1);
    
    if ([self.quizBaseScrollView viewWithTag:requiredCollectionViewTag] &&   [[self.quizBaseScrollView viewWithTag:requiredCollectionViewTag] isKindOfClass:[UICollectionView class]]) {
        UICollectionView* quizQuestionCollectionView = (UICollectionView*)[self.quizBaseScrollView viewWithTag:requiredCollectionViewTag];
        if (isTextOptionPresent) {
            self.currentOptionType = optionTypeOthers;
            ((UICollectionViewFlowLayout*)quizQuestionCollectionView.collectionViewLayout).minimumLineSpacing = 0;
            ((UICollectionViewFlowLayout*)quizQuestionCollectionView.collectionViewLayout).minimumInteritemSpacing = 0;
        }
        else {
            ((UICollectionViewFlowLayout*)quizQuestionCollectionView.collectionViewLayout).minimumLineSpacing = 18;
            ((UICollectionViewFlowLayout*)quizQuestionCollectionView.collectionViewLayout).minimumInteritemSpacing = 0;
        }
    }
}


- (void) loadQuizQuestionForIndex:(int) questionIndex forCollectionView:(UICollectionView*) collectionView
{
    //update the view display
    [self updateOptionTypeForQuestionIndex:questionIndex];
    
    [collectionView reloadData];
    [self scrollCollectionViewToBottom:collectionView];
 }


/**
 * shows the activity indicator with a given message
 * @param activity indicator message
 */
- (void) showActivityIndicatorViewWithMessage:(NSString*)message
{
    if (self.activityIndicatorView) {
        [self hideActivityIndicatorView];
    }
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor colorWithRed:(76.0/255.0) green:(76.0/255.0) blue:(76.0/255.0) alpha:1.0];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}


/**
 * hides the activity indicator view
 */
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    while ([self.view viewWithTag:ACTIVITY_INDICATOR_BACKGROUND_VIEW_TAG]) {
        [[self.view viewWithTag:ACTIVITY_INDICATOR_BACKGROUND_VIEW_TAG] removeFromSuperview];
    }
    self.activityIndicatorView = nil;
}


/**
 * IBAction to remove the overlay view
 * @param sender of the IBAction
 */
- (IBAction)removeOverlayView:(id)sender {
    
    //check for common answers on nudge screen
    if (self.commonAnswerViewScreen.hidden == YES && [self isCommonAnswerRequiredAfterNudge]) {
        
        //remove retry view if present
        if(self.retryView) {
            [self removeRetryView];
        }
        
        self.nudgeCollectionView.hidden = YES;
        //self.overlayNavigationButton.hidden = YES;
        
        [self showActivityIndicatorViewWithMessage:@"Let's see our common answers..."];
    }
    else {
        NSString* promptMessage = self.quizBaseView.hidden == NO ? @"Sure you want to leave the quiz?":@"Do you want to exit?";
        
        [[[UIAlertView alloc] initWithTitle:@"Quiz" message:promptMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
    }
}


/**
 * dismiss the overlay view
 */
- (void) dismissOverlayView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/**
 * Tap gesture to remove the overlay
 * @param gesture recognizer for the IBAction
 */
- (IBAction)removeOverlayTapped:(UITapGestureRecognizer *)sender {
    //do
}


/**
 * Callback method for save answer failure
 */
- (void) saveAnswerFailureReceived:(NSNotification*) notification
{
    NSString* failedQuizID = (notification.object && [notification.object isKindOfClass:[NSDictionary class]] && [[notification.object valueForKey:@"quizID"] isKindOfClass:[NSString class]])?[notification.object valueForKey:@"quizID"]:nil;
   
     NSString* failedQuizMessage = (notification.object && [notification.object isKindOfClass:[NSDictionary class]] && [[notification.object valueForKey:@"failureMessage"] isKindOfClass:[NSString class]])?[notification.object valueForKey:@"failureMessage"]:nil;
    
    if ([failedQuizID isEqualToString:self.currentQuizInfo.quizID]) {
        self.currentFailureState = saveQuizFailure;
        self.currentFailureMessage = failedQuizMessage;
        [self showRetryViewWithMessage:failedQuizMessage];
    }
}


- (void) decideQuizFailureReceived:(NSNotification*) notification
{
    NSString* failureMessage = @"Quiz action failed.";
    
    if ([notification.object isKindOfClass:[NSString class]] && [notification.object isEqualToString:@"networkFailure"]) {
        failureMessage = @"No internet connectivity.";
    }
    
    quizStatus currentQuizStatus = [self.quizController getQuizStatusFromDBForQuizID:self.currentQuizInfo.quizID];
    self.currentFailureMessage = failureMessage;
    
    if (currentQuizStatus == quizPlayedByBoth) {
        //common answer failure as question list is already present
        self.currentFailureState = commonAnswerFailure;
         [self showRetryViewWithMessage:failureMessage];
    }
    else {
        //quiz failure as no question list is present
        self.currentFailureState = quizQuestionFailure;
        //the retry view will be shown on the intro screen as per the new flow
    }
}

- (void) quizActionLogoutReceived:(NSNotification*) notification
{
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

-(void)showRetryViewWithMessage:(NSString*) failureMessage {
    if(self.retryView) {
        [self removeRetryView];
    }
    self.retryView = [[TMRetryView alloc] initWithFrame:self.overlayBaseView.frame message:failureMessage];
    self.retryView.center = CGPointMake(self.overlayBaseView.bounds.size.width/2, self.overlayBaseView.bounds.size.height/2);
    self.retryView.layer.cornerRadius = 6.0f;
    [self.retryView.retryButton addTarget:self action:@selector(quizRetryAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.retryView];
    
    [self.view bringSubviewToFront:self.cancelButton];
}
-(void)removeRetryView {
    [self.retryView removeFromSuperview];
    self.retryView = nil;
}

- (void) quizRetryAction
{
    //remove the retry view
    [self removeRetryView];
    
    if (self.currentFailureState == saveQuizFailure) {
        //perform the quiz save operation
        [self performSelector:@selector(performQuizSaveOperation) withObject:nil afterDelay:1];
    }
    else {
        //perform decide quiz api action
      //let the intro screen make the quiz request

    [self.quizController performSelector:@selector(decideQuizActionFromOverlayForQuizID:) withObject:self.currentQuizInfo.quizID afterDelay:1];
    }
}

#pragma mark - UICollectionView methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
        return 2;
    }
    else if (collectionView == self.nudgeCollectionView) {
        return 1;
    }
    else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
        if (section == 0) {
           return (((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]) && ((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]).count > 0)? 1: 0;
            
        }
        else if (section == 1) {
           // return (((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]) && ((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]).count > 0)? 1: 0;
            return ((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]).count;
        }
        else {
            return ((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]).count;
        }
    }
    else if (collectionView == self.nudgeCollectionView) {
        //test data
       
            return [self.nudgeUserArray count];
    }
    else {
        return 0;
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
        if(indexPath.section == 1) {
            
            int requiredHeight = ([UIScreen mainScreen].bounds.size.height < 500)?110:(IS_IPHONE_6_PLUS?190:(IS_IPHONE_6?170:130));
            
            return (self.currentOptionType == optionTypeAllImages)?(CGSizeMake(collectionView.bounds.size.width/2, requiredHeight)):(CGSizeMake(collectionView.bounds.size.width, [self heightForQuizViewRowInSection:(int)indexPath.section row:(int)indexPath.row forCollectionView:collectionView]));
        }
        else {
            return CGSizeMake(collectionView.bounds.size.width, [self heightForQuizViewRowInSection:(int)indexPath.section row:(int)indexPath.row forCollectionView:collectionView]);
        }
    }
    else if (collectionView == self.nudgeCollectionView) {
         return CGSizeMake(self.nudgeCollectionView.bounds.size.width/3, 100);
    }
    else {
        return CGRectZero.size;
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int section = (int)indexPath.section;
    
    TMOverlayBaseCollectionViewCell* cell;
    
    NSDictionary* cellInfoDictionary;
    
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
      
        //quiz collection view
        
        switch (section) {
            case 0: {
                //quiz question view
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:QUESTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
               
                NSString* questionText = [NSString stringWithFormat:@"%@",[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"question_text"]];
                
                cellInfoDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:questionText,@"question_text",[[self getQuizQuestionForCollectionView:collectionView] objectForKey:QUESTION_ID],QUESTION_ID, nil];
                break;
            }
            case 1: {
                //quiz option view
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:OPTION_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
               
                cellInfoDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:[((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]) objectAtIndex:indexPath.row],@"options", nil];
                [(TMQuizOverlayOptionCollectionViewCell*)cell setUpCellWithIndexPath:indexPath withCellSelectionState:([self.answerDict valueForKey:[[self getQuizQuestionForCollectionView:collectionView] valueForKey:QUESTION_ID]])?someOptionSeleted:noOptionSelected];
                ((TMQuizOverlayOptionCollectionViewCell*)cell).delegate = self;
                 break;
            }
            default:
                break;
        }
        [cell fillDetailsForQuestion:cellInfoDictionary];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else
    {
        //nudge cells
        
        switch (section) {
            case 0: {
                //nudge list cell
                
                cell = [self.nudgeCollectionView dequeueReusableCellWithReuseIdentifier:NUDGE_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
                ((TMOverlayNudgeListCollectionViewCell*)cell).delegate = self;
                [((TMOverlayNudgeListCollectionViewCell*)cell) setUpCellWithIndexPath:indexPath];
                
                //set nudge selection state
                   NSString* nudgeMatchID = ([[self.nudgeUserArray objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]] && [((NSDictionary*)[self.nudgeUserArray objectAtIndex:indexPath.row]) objectForKey:@"match_id"] && [[((NSDictionary*)[self.nudgeUserArray objectAtIndex:indexPath.row]) objectForKey:@"match_id"] isKindOfClass:[NSString class]])? [((NSDictionary*)[self.nudgeUserArray objectAtIndex:indexPath.row]) objectForKey:@"match_id"]:nil;
                
                [((TMOverlayNudgeListCollectionViewCell*)cell) setNudgeSelectionState:[self isUserSelectedForNudgeWithMatchID:nudgeMatchID]];
                break;
            }
           default:
                break;
        }
        [cell fillDetailsForQuestion:[self.nudgeUserArray objectAtIndex:indexPath.row]];
        return cell;
    }
    return nil;
}


- (BOOL) isUserSelectedForNudgeWithMatchID:(NSString*) matchID
{
    if(matchID) {
        for (int index = 0; index < self.selectedNudgeUserArray.count; index++) {
            if ([[self.selectedNudgeUserArray objectAtIndex:index] isKindOfClass:[NSDictionary class]] && [((NSDictionary*)[self.selectedNudgeUserArray objectAtIndex:index]) valueForKey:@"match_id"] && [[((NSDictionary*)[self.selectedNudgeUserArray objectAtIndex:index]) valueForKey:@"match_id"] isKindOfClass:[NSString class]]) {
                if ([matchID isEqualToString:[((NSDictionary*)[self.selectedNudgeUserArray objectAtIndex:index]) valueForKey:@"match_id"]]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void) collectionView:(UICollectionView *) collectionView didSelectItemAtIndexPath:(NSIndexPath *) indexPath
{
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
        if (indexPath.section == 1) {
            
            //any option pressed
            NSArray* optionsArray = ((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]);
            
            if (!self.answerDict) {
                self.answerDict = [[NSMutableDictionary alloc] init];
            }
            
            //if answer already present
            if ([self.answerDict objectForKey:[[self getQuizQuestionForCollectionView:collectionView] objectForKey:QUESTION_ID]]) {
                //remove this object
                [self.answerDict removeObjectForKey:[[self getQuizQuestionForCollectionView:collectionView] objectForKey:QUESTION_ID]];
            }
            
            [self.answerDict setObject:[NSString stringWithFormat:@"%@",[[optionsArray objectAtIndex:indexPath.row] objectForKey:@"option_id"]] forKey:[NSString stringWithFormat:@"%@",[[self getQuizQuestionForCollectionView:collectionView] objectForKey:QUESTION_ID]]];
            
            self.overlayNavigationButton.enabled = YES;
            [self.overlayNavigationButton setTitleColor:NAVIGATION_BUTTON_ENABLED_COLOR forState:UIControlStateNormal];
            
            TMQuizOverlayOptionCollectionViewCell* optionCollectionViewCell = (TMQuizOverlayOptionCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
            
            //update the non selected cells
            [self updateNonSelectedCellsForSelectedCellIndexPath:indexPath forCollectionView:collectionView];
            
            [optionCollectionViewCell makeChangesForCellSelection];
            
            //move to the next question / perform finish action
            [self moveToNextQuizQuestion];
        }
    }
    else if (collectionView == self.nudgeCollectionView) {
        if (indexPath.section == 1) {
            TMOverlayNudgeListCollectionViewCell* cell = (TMOverlayNudgeListCollectionViewCell*)[self.nudgeCollectionView cellForItemAtIndexPath:indexPath];
            [cell nudgeButtonPressed:nil];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0 && indexPath.section == 2) {
        TMQuizOverlayOptionCollectionViewCell* optionCollectionViewCell = (TMQuizOverlayOptionCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
        [optionCollectionViewCell makeChangesForCellDeSelectionWithOptionSelectionState:someOptionSeleted];
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0 && indexPath.section == 0) {
        UICollectionReusableView *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        headerview.backgroundColor = [UIColor clearColor];
        return headerview;
    }
    else {
        if ([collectionView isEqual:self.nudgeCollectionView] && kind == UICollectionElementKindSectionFooter) {
            return [self.nudgeCollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"nudgeFooter" forIndexPath:indexPath];
        }
        else {
             UICollectionReusableView *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
            headerview.backgroundColor = [UIColor clearColor];
            //headerview.bounds = CGRectMake(0, 0, 0, 0);
            return headerview;
        }
    }
   return nil;
}

/**
 * updates the non selected cells with the appropriate UI
 */
- (void) updateNonSelectedCellsForSelectedCellIndexPath:(NSIndexPath*) selectedCellIndexPath forCollectionView:(UICollectionView*) collectionView
{
    NSArray* visibleCells = [collectionView visibleCells];
    
    for (int index = 0; index < visibleCells.count; index++) {
        if([[visibleCells objectAtIndex:index] isKindOfClass:[TMQuizOverlayOptionCollectionViewCell class]] && ![((TMQuizOverlayOptionCollectionViewCell*)[visibleCells objectAtIndex:index]) isEqual:[collectionView cellForItemAtIndexPath:selectedCellIndexPath]]) {
            //deselect other cells
            [((TMQuizOverlayOptionCollectionViewCell*)[visibleCells objectAtIndex:index]) makeChangesForCellDeSelectionWithOptionSelectionState:someOptionSeleted];
        }
    }
}


/**
 * returns the height of the row in the desired section
 * works only for the quiz collection view
 * @param section of the collection view
 */
- (int) heightForQuizViewRowInSection:(int) section row:(int) row forCollectionView:(UICollectionView*) collectionView
{
    int screenHeight = [UIScreen mainScreen].bounds.size.height;//self.view.frame.size.height;

    //reducing the top margin
    screenHeight -= (collectionView.frame.origin.y - [UIScreen mainScreen].bounds.origin.y);
    
    //reducing the bottom margin
    screenHeight -= ([UIScreen mainScreen].bounds.size.height - collectionView.frame.size.height);
    
    //reducing the navigation button height
    screenHeight -= self.overlayNavigationButton.bounds.size.height;
    
    
    int optionsCount = (int)((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]).count;
    
    //for collectionview spacing
    screenHeight -= (optionsCount - 1)*((UICollectionViewFlowLayout*)collectionView.collectionViewLayout).minimumLineSpacing;
    
    switch (section) {
//        case 0:
//            return 2*screenHeight/5;
            
        case 0: {
            
            //the appropriate cell height based on the question + option count
            int sectionHeight = 2*(((3*screenHeight)/5)/(optionsCount + 2));
            
            //checking for lower bounds of the cell height
           sectionHeight = sectionHeight < MINIMUM_QUESTION_CELL_HEIGHT ? MINIMUM_QUESTION_CELL_HEIGHT: sectionHeight;
            
            //calculating the required size for the question cell
            NSString* questionText = ([[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"question_text"] isKindOfClass:[NSString class]])?[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"question_text"]:nil;
           
            //obtaining the height requirement for the cell text
            int requiredCellHeight;
            requiredCellHeight = [questionText boundingRectWithConstraintSize:CGSizeMake(collectionView.bounds.size.width, sectionHeight) attributeDictionary:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}].size.height;
            
            //adding margins
            requiredCellHeight += 30;
            
            //checking for upper bounds of the cell height
            int maxQuestionCellHeight = 2*(((3*screenHeight)/5)/(2));
            
            requiredCellHeight = (requiredCellHeight > maxQuestionCellHeight)?maxQuestionCellHeight:requiredCellHeight;
            
            CGFloat actualContentLabelSize = ceilf([questionText boundingRectWithConstraintSize:self.view.bounds.size attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:23]}].size.height);
            
            //check for the actual content size
            requiredCellHeight = (actualContentLabelSize > requiredCellHeight)?actualContentLabelSize:requiredCellHeight;
            
            return requiredCellHeight + 40;
        }
            
        case 1: {
            if (self.currentOptionType == optionTypeAllImages) {
                int optionViewTotalHeight = screenHeight - [self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].height - [self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]].height + 120;
                
                int minimumHeight = MINIMUM_IMAGE_OPTION_HEIGHT;
                
                if (optionsCount%2 == 0) {
                    //even no of cells
                    optionViewTotalHeight = optionViewTotalHeight/(optionsCount/2);
                }
                else {
                    //odd no of cells
                    optionViewTotalHeight = optionViewTotalHeight/(optionsCount/2 + 1);
                }
                
                 optionViewTotalHeight = (optionViewTotalHeight < minimumHeight) ? minimumHeight : optionViewTotalHeight;
                
                return optionViewTotalHeight;
            }
            else {
                int sectionHeight;
                
                if (optionsCount%2 == 0) {
                    //even no of cells
                    sectionHeight = (((3*screenHeight)/5)/ceil((optionsCount + 2)));
                }
                else {
                    //odd no of cells
                    sectionHeight = (((3*screenHeight)/5)/(optionsCount + 2) + 1);
                }
                
                //question no
               int questionIndex = (int)collectionView.tag/QUIZ_COLLECTION_VIEW_TAG_CONSTANT - 1;
                
                //option text
                NSString* optionText;
                
                if ([[self.currentQuestionsList.questionList objectAtIndex:questionIndex] isKindOfClass:[NSDictionary class]] && [[[self.currentQuestionsList.questionList objectAtIndex:questionIndex] objectForKey:@"options"] isKindOfClass:[NSArray class]] && [[((NSArray*)[[self.currentQuestionsList.questionList objectAtIndex:questionIndex] objectForKey:@"options"]) objectAtIndex:row] isKindOfClass:[NSDictionary class]] && [[((NSDictionary*)[((NSArray*)[[self.currentQuestionsList.questionList objectAtIndex:questionIndex] objectForKey:@"options"]) objectAtIndex:row]) objectForKey:@"option_text"] isKindOfClass:[NSString class]]) {
                    optionText = [((NSDictionary*)[((NSArray*)[[self.currentQuestionsList.questionList objectAtIndex:questionIndex] objectForKey:@"options"]) objectAtIndex:row]) objectForKey:@"option_text"];
                }
                
                //obtaining the height requirement for the cell text
                int requiredCellHeight;

                requiredCellHeight = [optionText boundingRectWithConstraintSize:CGSizeMake(collectionView.bounds.size.width, sectionHeight) attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:20]}].size.height;
                
               // requiredCellHeight *= 2;
                
                int marginHeight = (IS_IPHONE_6_PLUS)?64:(IS_IPHONE_6)?48:32;
                
                //adding the padding for the image option
                requiredCellHeight += marginHeight;
                
                return requiredCellHeight;
            }
        }
        default:
            break;
    }
    return 0;
}

/**
 * Method to get current question index
 */
- (int) getCurrentQuizQuestionIndex
{
    NSDictionary* currentQuestionDictionary = [self getQuizQuestionForCurrentQuizCollectionView];
    
    if ([currentQuestionDictionary objectForKey:QUESTION_ID] && [[currentQuestionDictionary objectForKey:QUESTION_ID] isKindOfClass:[NSString class]]) {
        NSString* currentQuestionID = [currentQuestionDictionary objectForKey:QUESTION_ID];
        
        NSArray* questionsArray = self.currentQuestionsList.questionList;
        
        for (int index = 0; index < questionsArray.count; index++) {
            if ([[questionsArray objectAtIndex:index] isKindOfClass:[NSDictionary class]] && [((NSDictionary*)[questionsArray objectAtIndex:index]) objectForKey:QUESTION_ID]) {
                NSString* tempQuestionID = [((NSDictionary*)[questionsArray objectAtIndex:index]) objectForKey:QUESTION_ID];
                if ([tempQuestionID isEqualToString:currentQuestionID]) {
                    return index;
                }
            }
        }
    }
    return -1;
}


/**
 * Method to get question for the given quiz collection view
 */
- (NSDictionary*) getQuizQuestionForCollectionView:(UICollectionView*) quizCollectionView {
    if (quizCollectionView.tag != 0 && quizCollectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
        int questionIndex = ((int)quizCollectionView.tag)/QUIZ_COLLECTION_VIEW_TAG_CONSTANT - 1;
        return [self.currentQuestionsList.questionList objectAtIndex:questionIndex];
    }
    return nil;
}

/**
 * Method to get question for current visible quiz question collection view
 */
- (NSDictionary*) getQuizQuestionForCurrentQuizCollectionView
{
    UICollectionView* currentQuizCollectionView = [self currentVisibleQuizCollectionView];
    if (currentQuizCollectionView.tag != 0 && currentQuizCollectionView.tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
        int questionIndex = ((int)currentQuizCollectionView.tag)/QUIZ_COLLECTION_VIEW_TAG_CONSTANT - 1;
        if (questionIndex >= 0 && questionIndex < self.currentQuestionsList.questionList.count) {
            return [self.currentQuestionsList.questionList objectAtIndex:questionIndex];
        }
    }
    return nil;
}


/**
 * Method to get current visible quiz collection view
 */
- (UICollectionView*) currentVisibleQuizCollectionView
{
    NSArray* subViews = [self.quizBaseScrollView subviews];
    
    for (int index = 0; index < subViews.count; index++) {
        if ([[subViews objectAtIndex:index] isKindOfClass:[UICollectionView class]] && ((UIView*)[subViews objectAtIndex:index]).tag != 0 && ((UIView*)[subViews objectAtIndex:index]).tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0 && CGRectIntersectsRect(self.quizBaseScrollView.bounds,((UIView*)[subViews objectAtIndex:index]).frame)) {
            return (UICollectionView*)[subViews objectAtIndex:index];
        }
    }
    return nil;
}


#pragma mark TMQuizOverlayOptionDelegate delegate methods

/**
 * Callback method for option selected at a given indexpath
 * @param the selected indexpath
 */
- (void) optionSelectedWithIndexPath:(NSIndexPath*) indexPath
{
    //perform deselect operation for any previous selected cells
    [self collectionView:[self currentVisibleQuizCollectionView] didDeselectItemAtIndexPath:[[[self currentVisibleQuizCollectionView] indexPathsForSelectedItems] firstObject]];
    
    //perform select operation for the newly selected cells
    [self collectionView:[self currentVisibleQuizCollectionView] didSelectItemAtIndexPath:indexPath];
    
    //add the newly selected cell to the selected cells property
    [[self currentVisibleQuizCollectionView] selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
}


#pragma mark TMOverlayNudgeListCellDelegate delegate methods

- (void) nudgePressedForMatchID:(NSString *) matchID
{
  //select/deselect the user for the nudge
    if (!self.selectedNudgeUserArray) {
        self.selectedNudgeUserArray = [[NSMutableArray alloc] init];
    }
    
    BOOL isUserSelected = NO;
    
    NSDictionary* selectedDictionary = nil;
    
    for (int index = 0; index < self.selectedNudgeUserArray.count; index++) {
        if([[self.selectedNudgeUserArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary* nudgeUserDictionary = [self.selectedNudgeUserArray objectAtIndex:index];
            
            if ([[nudgeUserDictionary valueForKey:@"match_id"] isKindOfClass:[NSString class]] && [[nudgeUserDictionary valueForKey:@"match_id"] isEqualToString:matchID]) {
                [self.selectedNudgeUserArray removeObject:nudgeUserDictionary];
                isUserSelected = YES;
            }
        }
    }
   
    for (int index = 0; index < self.nudgeUserArray.count; index++) {
        if ([[self.nudgeUserArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            NSDictionary* orginalListNudgeUserDictionary = [self.nudgeUserArray objectAtIndex:index];
            
            if ([[orginalListNudgeUserDictionary valueForKey:@"match_id"] isKindOfClass:[NSString class]] && [[orginalListNudgeUserDictionary valueForKey:@"match_id"] isEqualToString:matchID]) {
                selectedDictionary = orginalListNudgeUserDictionary;
            }
        }
    }
    
    if (!isUserSelected && selectedDictionary) {
        [self.selectedNudgeUserArray addObject:selectedDictionary];
    }
}

#pragma mark TMQuizAnswerViewDelegate methods

- (void) commonAnswerMessageSendingButtonPressed
{
    //common answer message sending button pressed
    
    //remove the UI
    [self dismissOverlayView];
}

- (NSString*) getCurrentMatchProfilePic
{
    return [self.delegate getCurrentMatchProfilePic];
}

- (NSString*) getUserProfilePic
{
    return [self.delegate getUserProfilePic];
}

- (NSString*) getUserName
{
    return [self.delegate getUserName];
}

- (NSString*) getCurrentMatchName {
    return [self.delegate getCurrentMatchName];
}


#pragma mark - UICollection View Scrolling methods

-(void) scrollCollectionViewToBottom:(UICollectionView*) collectionView
{
    [collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:((NSArray*)[[self getQuizQuestionForCollectionView:collectionView] objectForKey:@"options"]).count - 1 inSection:1] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

#pragma mark UIAlertView delegate methods

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self dismissOverlayView];
    }
}

#pragma mark - Dummy Utility method for nudges

//- (void) generateNudgeNotification
//{
//    /**
//     Printing description of notification:
//     NSConcreteNotification 0x7b76b6c0 {name = saveQuizNudgeResponseReceived; object = {
//     "auto_nudge" = 0;
//     image = "http://cdn.trulymadly.com/images/quiz/Hocus_Pocus.jpg";
//     name = "Hocus Pocus";
//     "nudge_list" =     (
//     {
//     checksum = 6f85c0f1401b3820a9f8889a50d714b6;
//     "full_conv_link" = "http://www.trulymadly.com/msg/message_full_conv.php?match_id=1809&mesh=e0ecfdd62ef55563cf2dfee4edfdb6df";
//     "is_played" = 1;
//     "match_id" = 1809;
//     name = Sumit;
//     "profile_pic" = "http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1441568448243_087887861765921120_1117688440.jpg";
//     },
//     {
//     checksum = 964e71709501a3d83c4ad50d884a0ed3;
//     "full_conv_link" = "http://www.trulymadly.com/msg/message_full_conv.php?match_id=2136&mesh=c07ad358449ead8c2bf2e607225ed20b";
//     "is_played" = 0;
//     "match_id" = 2136;
//     name = Udbhav;
//     "profile_pic" = "http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1399617923767.2_359921235591173184_3302138005.jpg";
//     },
//     {
//     checksum = c5f4f41cd2b6436bee91c684169c4653;
//     "full_conv_link" = "http://www.trulymadly.com/msg/message_full_conv.php?match_id=707&mesh=ff5ff22924d364d7e9a1495a0ba069a2";
//     "is_played" = 1;
//     "match_id" = 707;
//     name = Poonam;
//     "profile_pic" = "http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1441698954997.8_625256017316132736_5318464171.jpg";
//     },
//     {
//     checksum = 7fd48539bd7ee92e1d3aa1d66e4c82f5;
//     "full_conv_link" = "http://www.trulymadly.com/msg/message_full_conv.php?match_id=2121&mesh=3707e88dcefb9b537e2724390f5a7bb9";
//     "is_played" = 0;
//     "match_id" = 2121;
//     name = Priya;
//     "profile_pic" = "http://cdn.trulymadly.com/images/um_dummy_female.png";
//     },
//     {
//     checksum = 90c73a802cc087c1a45b74fb607084d8;
//     "full_conv_link" = "http://www.trulymadly.com/msg/message_full_conv.php?match_id=712&mesh=aa9d2831407c71e05f9b55f4f03b8f17";
//     "is_played" = 0;
//     "match_id" = 712;
//     name = Satish;
//     "profile_pic" = "http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1430405409675.3_419730866793543104_0579144526.jpg";
//     }
//     );
//     "quiz_id" = 6;
//     responseCode = 200;
//     "response_type" = "nudge_list";
//     }}
//     (lldb)
//     */
//    
//    NSDictionary* nudgeDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"90c73a802cc087c1a45b74fb607084d8",@"checksum",@"http://www.trulymadly.com/msg/message_full_conv.php?match_id=712&mesh=aa9d2831407c71e05f9b55f4f03b8f17",@"full_conv_link",[NSNumber numberWithInt:0],@"is_played",@"712",@"match_id",@"Satish",@"name",@"http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1430405409675.3_419730866793543104_0579144526.jpg",@"profile_pic", nil];
//    
//    NSMutableArray* nudgeListArray = [NSMutableArray new];
//    
//    for (int index = 0; index < 20; index++) {
//        [nudgeListArray addObject:nudgeDictionary];
//    }
//    
//    NSMutableDictionary* responseDictionary = [NSMutableDictionary new];
//    [responseDictionary setObject:nudgeListArray forKey:@"nudge_list"];
//    [responseDictionary setObject:@"Hocus Pocus" forKey:@"name"];
//    [responseDictionary setObject:@"http://cdn.trulymadly.com/images/quiz/Hocus_Pocus.jpg" forKey:@"image"];
//    [responseDictionary setObject:[NSNumber numberWithInt:0] forKey:@"auto_nudge"];
//    
//     [[NSNotificationCenter defaultCenter] postNotificationName:@"saveQuizResponseReceived" object:responseDictionary];
//}
- (IBAction)nudgeDeselectAllPressed:(id)sender {
    self.selectedNudgeUserArray = nil;
    [self.nudgeCollectionView reloadData];
}


#pragma mark - UIScrollView delegate methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSArray* scrollViewSubviews = [self.quizBaseScrollView subviews];
    
    for (int index = 0; index < scrollViewSubviews.count; index++) {
        if (((UIView*)[scrollViewSubviews objectAtIndex:index]).tag != 0 && ((UIView*)[scrollViewSubviews objectAtIndex:index]).tag%QUIZ_COLLECTION_VIEW_TAG_CONSTANT == 0) {
            if ([[scrollViewSubviews objectAtIndex:index] isKindOfClass:[UICollectionView class]]) {
            //    UICollectionView* quizCollectionView = (UICollectionView*)[scrollViewSubviews objectAtIndex:index];
                self.quizBaseScrollView.userInteractionEnabled = YES;
            }
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    self.quizBaseScrollView.userInteractionEnabled = YES;
}

#pragma mark - Quiz Version change failure callback method
- (void) quizActionFailedDueDelayedResponse
{
    if (self.activityIndicatorView) {
        //dismiss self view as the version has been changed/ nudge response recieved
        [self dismissOverlayView];
    }
}

#pragma mark - TMQuizNudgeScreenDelegate protocol method

- (void) skipButtonPressed
{
    [self nudgeActionCompleted];
}

- (void) nudgeButtonPressed
{
    //send auto nudge to the match
    [self.quizController performOneToOneNudgeWithQuizID:self.currentQuizInfo.quizID quizName:self.currentQuizInfo.quizDisplayName];
    
    [self nudgeActionCompleted];
}

- (void) nudgeActionCompleted
{
    //check for common answers
    if ([self.quizController isCommonAnswerRequiredAfterNudgeQuizID:self.currentQuizInfo.quizID]) {
        
        self.nudgeCollectionView.hidden = YES;
        //self.overlayNavigationButton.hidden = YES;
        
        [self showActivityIndicatorViewWithMessage:@"Let's see our common answers..."];
    }
    else {
        [self dismissOverlayView];
    }
}

-(BOOL)prefersStatusBarHidden {
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

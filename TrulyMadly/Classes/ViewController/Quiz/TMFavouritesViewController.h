//
//  TMFavouritesViewController.h
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"
#include "TMEnums.h"

@protocol  TMFavouritesViewControllerDelegate <TMViewControllerActionDelegate>

-(void)didEditFavourite;

@end

@interface TMFavouritesViewController : TMBaseViewController

@property(nonatomic,weak)id<TMFavouritesViewControllerDelegate> delegate;

-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow;

@end



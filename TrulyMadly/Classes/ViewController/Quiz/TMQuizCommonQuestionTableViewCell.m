//
//  TMQuizCommonQuestionTableViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 08/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizCommonQuestionTableViewCell.h"
#import "NSString+TMAdditions.h"

@interface TMQuizCommonQuestionTableViewCell ()

@property (strong, nonatomic) UIView *questionNoBaseView;
@property (strong, nonatomic) UILabel *questionNoLabel;
@property (strong, nonatomic) UILabel *questionTextLabel;


@end

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)

#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)


@implementation TMQuizCommonQuestionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.questionNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.questionNoLabel.alpha = 0.7f;
    self.questionNoLabel.font = [UIFont systemFontOfSize:17];
    self.questionNoLabel.backgroundColor = [UIColor whiteColor];
    self.questionNoLabel.clipsToBounds = YES;
    self.questionNoLabel.textAlignment = NSTextAlignmentCenter;
    self.questionNoLabel.layer.cornerRadius = self.questionNoLabel.bounds.size.width/2;
    [self addSubview:self.questionNoLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) fillQuestionDetailsForQuestionObject:(QuizQuestionDomainObject*) questionDomainObject andQuestionNumber:(int) questionNo
{
    if (!self.questionTextLabel) {
        self.questionTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        self.questionTextLabel.backgroundColor = [UIColor clearColor];
        self.questionTextLabel.font = [UIFont systemFontOfSize:17];
        self.questionTextLabel.numberOfLines = 0;
        [self addSubview:self.questionTextLabel];
    }
    
    CGRect requiredSize;
    
    int topHeightMargin = 10;
    
    CGFloat labelWidth = IS_IPHONE_6_PLUS?(328):(IS_IPHONE_6?300:270);
    
    CGFloat labelStartX = IS_IPHONE_6_PLUS?(46):(IS_IPHONE_6?40:([UIScreen mainScreen].bounds.size.height > 500)?34:30);
   
     CGFloat noStartX = IS_IPHONE_6_PLUS?(18):(IS_IPHONE_6?12:([UIScreen mainScreen].bounds.size.height > 500)?6:4);
    
    requiredSize = [questionDomainObject.questionText boundingRectWithConstraintSize:CGSizeMake(labelWidth, CGFLOAT_MAX) attributeDictionary:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    
    self.questionTextLabel.frame = CGRectMake(labelStartX, topHeightMargin, labelWidth, requiredSize.size.height);
    self.questionNoLabel.frame = CGRectMake(noStartX, topHeightMargin, 20, 20);
    
    self.questionNoLabel.text = [NSString stringWithFormat:@"%d",questionNo];
    self.questionTextLabel.text = [questionDomainObject questionText];
}


@end

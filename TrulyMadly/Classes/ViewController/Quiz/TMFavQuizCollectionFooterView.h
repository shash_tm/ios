//
//  TMFavQuizCollectionFooterView.h
//  TrulyMadly
//
//  Created by Ankit on 21/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMFavQuizCollectionFooterViewDelegate;

@interface TMFavQuizCollectionFooterView : UICollectionReusableView

@property(nonatomic,weak)id<TMFavQuizCollectionFooterViewDelegate> delegate;

@end

@protocol TMFavQuizCollectionFooterViewDelegate <NSObject>

-(void)saveFavAction;

@end
//
//  TMQuizOverlayQuizDescCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 11/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizOverlayQuizDescCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface TMQuizOverlayQuizDescCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *quizImageView;
@property (weak, nonatomic) IBOutlet UILabel *quizNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingQuestionsLabel;
@property (weak, nonatomic) IBOutlet UIView *quizDescriptionBackgroundView;

@end


@implementation TMQuizOverlayQuizDescCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addQuizDescriptionView];
}

- (void) addQuizDescriptionView {
    
    int imageSquareSide = MIN(self.bounds.size.width, self.bounds.size.height)/2;
    self.quizImageView.layer.cornerRadius = imageSquareSide;
    self.quizImageView.layer.borderColor = [UIColor blackColor].CGColor;
    self.quizImageView.layer.borderWidth = 2.0f;
    self.quizImageView.clipsToBounds = YES;
}

- (void) addGradientToBackgroundView
{
    self.quizDescriptionBackgroundView.backgroundColor = [UIColor clearColor];
   /* UIColor *colorOne = [UIColor darkGrayColor];
    UIColor *colorTwo = [UIColor blackColor];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.frame = self.quizDescriptionBackgroundView.bounds;
    [self.quizDescriptionBackgroundView.layer insertSublayer:headerLayer atIndex:0];
    
    //setting the shadow attributes
    self.quizDescriptionBackgroundView.layer.masksToBounds = NO;
    self.quizDescriptionBackgroundView.layer.shadowOffset = CGSizeMake(0, -10);
    self.quizDescriptionBackgroundView.layer.shadowRadius = 5;
    self.quizDescriptionBackgroundView.layer.shadowOpacity = 0.5;
    */
}

- (void) fillDetailsForQuestion:(NSDictionary*) questionOptionsDictionary
{
    //quiz name
    self.quizNameLabel.text = [[questionOptionsDictionary objectForKey:@"quiz_name"] isKindOfClass:[NSNull class]]?@"":[questionOptionsDictionary objectForKey:@"quiz_name"];
    
    //quiz image
    [self.quizImageView setImageWithURL:[NSURL URLWithString:[questionOptionsDictionary objectForKey:@"quiz_image"]] placeholderImage:[UIImage imageNamed:@"placeholder_banner"]];
    self.quizImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    //remaining quizzes label
    self.remainingQuestionsLabel.text = [questionOptionsDictionary objectForKey:@"question_remaining"];
}

@end

//
//  TMQuizController.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 18/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizController.h"
#import "TMUserSession.h"
#import "TMQuizCacheManager.h"
#import "TMMessagingController.h"
#import "TMQuizManager.h"
#import "TMMessage.h"
#import "TMQuizInfo.h"
#import "TMMessageViewController.h"
#import "TMMessagingController.h"
#import "TMQuizOverlayViewController.h"
#import "UIImageView+AFNetworking.h"
#import "NSObject+TMAdditions.h"
#import "TMAnalytics.h"
#import "TMDataStore.h"

@interface TMQuizController ()

@property (nonatomic, strong) NSMutableDictionary* quizInfoDictionary;
@property (nonatomic, strong) NSDictionary* nudgeInfoDictionary;
@property (nonatomic, strong) NSString* matchName;
@property (nonatomic, strong) NSString* matchID;
@property (nonatomic, strong) NSMutableDictionary* eventDict;
@property (nonatomic, readwrite) BOOL isNudgeActionCancelled;

@end


@implementation TMQuizController


static UIAlertView* nudgeAlertView;


- (instancetype) initWithMatchName:(NSString*) matchName matchID:(NSString*) matchID
{
    if (self = [super init]) {
        
        [self fetchAllQuizInfo];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchImagesForBannerURLArray:) name:@"quizzesFetchedForImageCaching" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchImagesForOptionImageNotification:) name:@"quizOptionsRecievedForOptionImageFetching" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(promptUserForNudge:) name:@"autoNudgeResponseRecieved" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(promptUserForNudgeFromChat:) name:@"autoNudgeResponseRecievedFromChat" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutResponseReceived:) name:@"quizLogoutResponseReceived" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkForPresentedViewControllerWithNotification:) name:@"decideQuizRequestCompleted" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchAllQuizInfo) name:@"playedQuizzesStatusUpdate" object:nil];
        
        self.matchName = matchName;
        
        self.matchID = matchID;
        
        return self;
    }
    return nil;
}

- (void) checkForPresentedViewControllerWithNotification:(NSNotification*) notification
{
    NSString* quizID;
    
    NSString* responseType;
    
    if (notification && [notification object] && [[notification object] isKindOfClass:[NSDictionary class]]) {
        NSDictionary* responseDictionary = [notification object];
        
        quizID = (![[responseDictionary valueForKey:@"quiz_id"] isKindOfClass:[NSNull class]])?[responseDictionary valueForKey:@"quiz_id"]:nil;
        
        responseType = (![[responseDictionary valueForKey:@"response_type"] isKindOfClass:[NSNull class]])?[responseDictionary valueForKey:@"response_type"]:nil;
    }
    
    if ([responseType isEqualToString:@"nudge_list"] || !responseType) {
        //nudge response or empty
        //need not to present view controller
        return;
    }
    
    NSString* loaderMessage = [responseType isEqualToString:@"questions"]?@"Ready, Set, Quiz...":@"Let's see our common answers...";
    
    if ([self.delegate respondsToSelector:@selector(presentViewController:animated:completion:)]) {
        if (![(UIViewController*)self.delegate presentedViewController]) {
            TMQuizOverlayViewController* overlayViewController = [[TMQuizOverlayViewController alloc]initWithNibName:@"TMQuizOverlayViewController" bundle:nil quizID:quizID blurredImage:[self getBlurEffectImage] loaderMessage:loaderMessage welcomeScreenToBeShown:NO quizController:self];
            //TODO::Abhijeet::remove this warning
            overlayViewController.delegate = self.delegate;
            
            [(UIViewController*)self.delegate presentViewController:overlayViewController animated:NO completion:nil];
        }
    }
}

- (void) promptUserForNudgeFromChat:(NSNotification*) notification
{
    NSDictionary* infoDictionary = [notification object];
    
    self.nudgeInfoDictionary = infoDictionary;
    
    BOOL isNudgeForCurrentMatch = NO;
    
    if ([infoDictionary valueForKey:@"nudgedUsers"]) {
        NSArray* nudgedUserArray = [infoDictionary valueForKey:@"nudgedUsers"];
        
        if ([nudgedUserArray containsObject:self.matchID]) {
            isNudgeForCurrentMatch = YES;
        }
    }
    
    if (nudgeAlertView) {
        [nudgeAlertView dismissWithClickedButtonIndex:-1 animated:NO];
    }
    
    if (!self.isNudgeActionCancelled && isNudgeForCurrentMatch) {
        NSString* promptMessage = self.matchName?[NSString stringWithFormat:@"You have already nudged %@. Do you want to nudge again?",self.matchName]:@"You have already nudged this user. Do you want to nudge again ?";
        
        nudgeAlertView = [[UIAlertView alloc] initWithTitle:@"Quiz" message:promptMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [nudgeAlertView show];
    }
}

- (void) promptUserForNudge:(NSNotification*) notification{
    NSDictionary* infoDictionary = [notification object];
    
    self.nudgeInfoDictionary = infoDictionary;
    
    BOOL isNudgeForCurrentMatch = NO;
    
    if ([infoDictionary valueForKey:@"nudgedUsers"]) {
        NSArray* nudgedUserArray = [infoDictionary valueForKey:@"nudgedUsers"];
        
        if ([nudgedUserArray containsObject:self.matchID]) {
            isNudgeForCurrentMatch = YES;
        }
    }
    
    if (nudgeAlertView) {
        [nudgeAlertView dismissWithClickedButtonIndex:-1 animated:NO];
    }
    
    if(!self.isNudgeActionCancelled && isNudgeForCurrentMatch) {
        NSString* promptMessage = self.matchName?[NSString stringWithFormat:@"Nudge %@ to play this quiz?",self.matchName]:@"Nudge the user to play this quiz?";
        
        nudgeAlertView = [[UIAlertView alloc] initWithTitle:@"Quiz" message:promptMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [nudgeAlertView show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1) {
        [self peformNudge];
    }
    else {
        //do nothing
    }
}


- (void) peformNudge
{
    TMQuizController* quizController = self;
    [quizController performOneToOneNudgeWithQuizID:[self.nudgeInfoDictionary valueForKey:@"quiz_id"] quizName:[self.nudgeInfoDictionary valueForKey:@"name"]];
}

- (userGender) getUserGender
{
    BOOL isFemale = ([[TMUserSession sharedInstance].user isUserFemale])? YES: NO;
    
    return isFemale?femaleGender:maleGender;
}

/**
 * fetch the updated list of quizzes from server if required
 */
- (void) fetchUpdatedQuizList
{
    
    //    //fetch them from server
    TMQuizManager* quizManager = [[TMQuizManager alloc] init];
    [quizManager getAllAvailableQuizData];
    
    //fetch all quizzes from server
    //    if (![[QuizDBManager getQuizDBHandlerInstance] getAllQuizzes]) {
    //        //quizzes not present
    //        //fetch them from server
    //        TMQuizManager* quizManager = [[TMQuizManager alloc] init];
    //        [quizManager getAllAvailableQuizData];
    //    }
}


- (quizStatus) getQuizStatusFromDBForQuizID:(NSString*) quizID
{
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    
    BOOL quizTakenByHost = NO;
    BOOL quizTakenByMatch = NO;
    
    //checking for host status
    if ([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]]) {
        quizTakenByHost = YES;
    }
    
    //checking for match status
    if ([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]]) {
        quizTakenByMatch = YES;
    }
    if (quizTakenByHost && quizTakenByMatch) {
        return quizPlayedByBoth;
    }
    else if (quizTakenByHost && !quizTakenByMatch) {
        return quizPlayedByUser;
    }
    else if (!quizTakenByHost && quizTakenByMatch) {
        return quizPlayedByMatch;
    }
    else {
        return quizPlayedByNeither;
    }
}


- (NSArray*) getAllFlareQuizzesForMatchID:(NSString*) matchID
{
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    return [quizCacheManager getAllFlareQuizzesForMatchID:matchID];
}

- (NSArray*) getAllQuizzesPlayedByUserID:(NSString*) userID
{
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    return [quizCacheManager getAllQuizzesPlayedByUserID:userID];
}

/**
 * is flare for the given match id
 * @param: match id
 * @return: flare status
 */
- (BOOL) isFlareForQuizID:(NSString*) quizID
{
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    
    return [quizCacheManager isFlareForQuizID:quizID userID:[[TMMessagingController sharedController] getCurrentMatchId]];
}

/**
 * is common answer required after nudge
 * @param quiz ID
 */
- (BOOL) isCommonAnswerRequiredAfterNudgeQuizID:(NSString*) quizID
{
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    
    if ([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]]) {
        //quiz played by match
        [self performQuizDecideActionFromOverlayForQuizID:quizID];
        return YES;
    }
    //default case
    return NO;
}


/**
 * perform decide quiz action from overlay
 * @param quiz ID
 */
- (void) performQuizDecideActionFromOverlayForQuizID:(NSString*) quizID
{
    //make request for common answers
    TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
    [quizManager decideQuizActionForQuizID:quizID matchID:[[TMMessagingController sharedController] getCurrentMatchId]];
}


/**
 * save quiz with the provided quiz ID
 * @param quiz ID
 * @param answer dictionary
 */
- (void) saveQuizWithQuizID:(NSString*) quizID answerDictionary:(NSDictionary*) answerDictionary
{
    TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
    [quizManager saveQuizWithID:quizID withMatchID:[[TMMessagingController sharedController] getCurrentMatchId] withQuestionAnswerDictionary:answerDictionary];
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    
    if (quizID) {
        //adding event tracking for save quiz action
        [self.eventDict setObject:@"TMQuizOverlayView" forKey:@"screenName"];
        [self.eventDict setObject:quizID forKey:@"label"];
        [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
        [self.eventDict setObject:@"true" forKey:@"GA"];
        [self.eventDict setObject:@"save_quiz_answers" forKey:@"eventAction"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    }
}


/**
 * get blurred image
 */
- (UIImage*) getBlurEffectImage
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


- (void) decideQuizActionForQuizID:(NSString*) quizID withBaseViewController:(UIViewController<TMQuizOverlayViewControllerDelegate>*) baseViewController
{
    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    
    BOOL isAPIRequestRequired = YES;
    
    if([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]] && ![quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]]) {
        //nudge action required
        
    }
    else {
        //show the UI
        
        NSString* loaderMessage = ([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]] && [quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]])?@"Let's see our common answers...":@"Ready, Set, Quiz...";
        
        if (![quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]]) {
            //quiz has not been played by the user
            //welcome screen will be shown
            //API request not required
            isAPIRequestRequired = NO;
        }
        
        
        if (![(UIViewController*)baseViewController presentedViewController]) {
            TMQuizOverlayViewController* overlayViewController = [[TMQuizOverlayViewController alloc]initWithNibName:@"TMQuizOverlayViewController" bundle:nil quizID:quizID blurredImage:[self getBlurEffectImage] loaderMessage:loaderMessage welcomeScreenToBeShown:!isAPIRequestRequired quizController:self];
            overlayViewController.delegate = baseViewController;
            
            [baseViewController presentViewController:overlayViewController animated:YES completion:nil];
        }
    }
    if (isAPIRequestRequired && quizID && matchID) {
        TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
        [quizManager decideQuizActionForQuizID:quizID matchID:matchID];
    }
}


/**
 * Decide quiz action from chat
 */
- (void) decideQuizActionFromChatForQuizID:(NSString*) quizID withBaseViewController:(UIViewController<TMQuizOverlayViewControllerDelegate>*) baseViewController
{
    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    
    BOOL isAPIRequestRequired = YES;
    
    if([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]] && ![quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]]) {
        //nudge action required
        
    }
    else {
        //show the UI
        
        NSString* loaderMessage = ([quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]] && [quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]])?@"Let's see our common answers...":@"Ready, Set, Quiz...";
        
        if (![quizCacheManager hasQuizWithID:quizID beenTakenByUserID:[self getUserID]]) {
            //quiz has not been played by the user
            //welcome screen will be shown
            //API request not required
            isAPIRequestRequired = NO;
        }
        
        if (![(UIViewController*)baseViewController presentedViewController]) {
            TMQuizOverlayViewController* overlayViewController = [[TMQuizOverlayViewController alloc]initWithNibName:@"TMQuizOverlayViewController" bundle:nil quizID:quizID blurredImage:[self getBlurEffectImage] loaderMessage:loaderMessage welcomeScreenToBeShown:!isAPIRequestRequired quizController:self];
            overlayViewController.delegate = baseViewController;
            
            [baseViewController presentViewController:overlayViewController animated:YES completion:nil];
        }
    }
    if (isAPIRequestRequired && quizID && matchID) {
        TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
        [quizManager decideQuizActionFromChatForQuizID:quizID matchID:matchID];
    }
}


/**
 * Decide quiz action from overlay
 * no UI action required here
 */
- (void) decideQuizActionFromOverlayForQuizID:(NSString*) quizID
{
    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
    if (quizID && matchID) {
        TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
        [quizManager decideQuizActionForQuizID:quizID matchID:matchID];
    }
}


/**
 * provides the image for a given quiz
 * @param required quiz ID
 * @return status image for the quiz
 */
- (UIImage*) statusImageForQuizID:(NSString*) quizID
{
    if (quizID) {
        
        
        //gender info
        BOOL isFemale = ([[TMUserSession sharedInstance].user isUserFemale])? YES: NO;
        
        BOOL quizTakenByHost = NO;
        BOOL quizTakenByMatch = NO;
        
        TMQuizCacheManager* quizCacheManger = [[TMQuizCacheManager alloc] initWithConfiguration];
        
        //checking for host status
        if ([quizCacheManger hasQuizWithID:quizID beenTakenByUserID:[TMUserSession sharedInstance].user.userId]) {
            quizTakenByHost = YES;
        }
        
        //checking for match status
        if ([quizCacheManger hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]]) {
            quizTakenByMatch = YES;
        }
        
        if (isFemale)
        {
            //image set for females
            if (quizTakenByHost)
            {
                if (quizTakenByMatch)
                {
                    //quiz taken by both host and match
                    return [UIImage imageNamed:@"BothTaken"];
                }
                else
                {
                    //quiz taken by only the female host
                    return [UIImage imageNamed:@"Invite_Male"];
                }
            }
            else
            {
                if (quizTakenByMatch) {
                    //quiz taken by only the match
                    return [UIImage imageNamed:@"QuizTaken_Male"];
                }
            }
        }
        else
        {
            //image set for males
            if (quizTakenByHost)
            {
                if (quizTakenByMatch)
                {
                    //quiz taken by both host and match
                    return [UIImage imageNamed:@"BothTaken"];
                }
                else
                {
                    //quiz taken by only the male host
                    return [UIImage imageNamed:@"Invite_Female"];
                }
            }
            else
            {
                if (quizTakenByMatch) {
                    //quiz taken by only the match
                    return [UIImage imageNamed:@"QuizTaken_Female"];
                }
            }
        }
    }
    return nil;
}

- (void) fetchAllQuizInfo
{
    NSArray* dbQuizDictArray = [self getAllQuizzesFromDB];
    
    if (!self.quizInfoDictionary) {
        self.quizInfoDictionary = [[NSMutableDictionary alloc] init];
    }
    
    for (int index = 0; index < dbQuizDictArray.count; index++) {
        if ([[dbQuizDictArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            TMQuizInfo* quizInfo = [[TMQuizInfo alloc] initWithDictionary:[dbQuizDictArray objectAtIndex:index]];
            if ([self.quizInfoDictionary valueForKey:quizInfo.quizID]) {
                //remove this previously stored object
                [self.quizInfoDictionary removeObjectForKey:quizInfo.quizID];
            }
            if (quizInfo.quizID) {
                [self.quizInfoDictionary setObject:quizInfo forKey:quizInfo.quizID];
            }
        }
    }
}


- (NSArray*) getAllQuizzesFromDB
{
    TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
    NSMutableArray* dbArray = [[quizManager fetchQuizFromDBForLatestVersion:NO] mutableCopy];
 
   // [dbArray sortUsingDescriptors:[NSArray arrayWithObjects:[[NSSortDescriptor alloc] initWithKey:@"quiz_id" ascending:NO], nil]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"quizzesFetchedForImageCaching" object:dbArray];
    
    return dbArray;
}

- (NSArray*) getAllQuizzesFromDBForLatestQuizVersion {
    
    TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
    NSMutableArray* dbArray = [[quizManager fetchQuizFromDBForLatestVersion:YES] mutableCopy];
    
   // [dbArray sortUsingDescriptors:[NSArray arrayWithObjects:[[NSSortDescriptor alloc] initWithKey:@"quiz_id" ascending:NO], nil]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"quizzesFetchedForImageCaching" object:dbArray];
    
    return dbArray;
}

- (void) fetchPlayedQuizzesForMatchID:(NSString*) matchID
{
    TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
    [quizManager getPlayedQuizesForMatchID:matchID];
}

- (NSString*) getUserProfileImageURL
{
    NSString* userProfileImageURL = [TMUserSession sharedInstance].user.profilePicURL;
    
    return userProfileImageURL;
}

- (NSString*) getUserID
{
    return [TMUserSession sharedInstance].user.userId;
}

- (void) performOneToOneNudgeWithQuizID:(NSString*) quizID quizName:(NSString*) quizName
{
    BOOL hasUserPlayed = [[[TMQuizCacheManager alloc]initWithConfiguration] hasQuizWithID:quizID beenTakenByUserID:[[TMMessagingController sharedController] getCurrentMatchId]];
    
    
    NSString* textMessageString = hasUserPlayed?[NSString stringWithFormat:@"Let's check our %@ quotient",quizName]:[NSString stringWithFormat:@"Ready to play %@?",quizName];
    TMMessageType messageType = (hasUserPlayed) ? MESSAGETTYPE_QUIZ_NUDGE_DOUBLE: MESSAGETTYPE_QUIZ_NUDGE_SINGLE;
    TMMessage* message = [[TMMessage alloc] initWithText:textMessageString type:messageType receiverId:[[[TMMessagingController sharedController] getCurrentMatchId] intValue] quizId:quizID fullConversationLink:nil];
    [[TMMessagingController sharedController] sendMessages:@[message]];
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    
    //adding event tracking for nudge action
    [self.eventDict setObject:@"TMQuizGalleryView" forKey:@"screenName"];
    [self.eventDict setObject:quizID forKey:@"label"];
    [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
    [self.eventDict setObject:@"true" forKey:@"GA"];
    [self.eventDict setObject:@"send_nudge_chat" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
}

- (void) nudgeUserWithUserArray:(NSArray*) nudgeUserArray quizID:(NSString*) quizID quizName:(NSString*) quizName
{
    NSMutableArray* nudgeMessageArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary* nudgeUserDictionary in nudgeUserArray) {
        if ([nudgeUserDictionary valueForKey:@"match_id"] && ![[nudgeUserDictionary valueForKey:@"match_id"] isKindOfClass:[NSNull class]]) {
            BOOL hasUserPlayed = ([nudgeUserDictionary valueForKey:@"is_played"] && ![[nudgeUserDictionary valueForKey:@"is_played"] isKindOfClass:[NSNull class]])?[[nudgeUserDictionary valueForKey:@"is_played"] boolValue]:NO;
            
            NSString* textMessageString = hasUserPlayed?[NSString stringWithFormat:@"Let's check our %@ quotient",quizName]:[NSString stringWithFormat:@"Ready to play %@?",quizName];
            TMMessageType messageType = (hasUserPlayed) ? MESSAGETTYPE_QUIZ_NUDGE_DOUBLE: MESSAGETTYPE_QUIZ_NUDGE_SINGLE;
            TMMessage* message = [[TMMessage alloc] initWithText:textMessageString type:messageType receiverId:[[nudgeUserDictionary valueForKey:@"match_id"] intValue] quizId:quizID fullConversationLink:[[nudgeUserDictionary valueForKey:@"full_conv_link"] isValidObject]?[nudgeUserDictionary valueForKey:@"full_conv_link"]:nil];
            [nudgeMessageArray addObject:message];
        }
    }
    [[TMMessagingController sharedController] sendMessages:nudgeMessageArray];
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    
    //adding event tracking for nudge action
    [self.eventDict setObject:@"TMQuizOverlayView" forKey:@"screenName"];
    [self.eventDict setObject:quizID forKey:@"label"];
    [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
    [self.eventDict setObject:@"true" forKey:@"GA"];
    [self.eventDict setObject:@"send_nudge" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
}

- (void) sendCommonAnswerWithMessageText:(NSString*) text quizID:(NSString*) quizID isFlare:(BOOL) isFlare
{
    TMMessage* message = [[TMMessage alloc] initWithText:text type:MESSAGETTYPE_QUIZ_MESSAGE receiverId:[[[TMMessagingController sharedController] getCurrentMatchId] intValue] quizId:quizID fullConversationLink:nil];
    [[TMMessagingController sharedController] sendMessage:message];
    
    if (quizID) {
        
        //adding event tracking for quiz click action
        self.eventDict = [[NSMutableDictionary alloc] init];
        [self.eventDict setObject:@"TMQuizChatView" forKey:@"screenName"];
        [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
        [self.eventDict setObject:quizID forKey:@"label"];
        [self.eventDict setObject:quizID forKey:@"status"];
        [self.eventDict setObject:isFlare?@"flare_send_quiz_message":@"send_quiz_message" forKey:@"eventAction"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    }
}

- (void) fetchImagesForBannerURLArray:(NSNotification*) quizDataNotification
{
    NSArray* quizDataArray = [quizDataNotification object];
    
    for (int index = 0; index < quizDataArray.count; index++) {
        if ([[quizDataArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            UIImageView* imageView = [[UIImageView alloc] init];
            [imageView setImageWithURL:[NSURL URLWithString:[[quizDataArray objectAtIndex:index] valueForKey:@"banner_url"]]];
        }
    }
}

- (void) fetchImagesForOptionImageNotification:(NSNotification*) notification
{
    if(notification && [notification object] && [notification.object isKindOfClass:[NSArray class]]) {
        NSArray* quizQuestionsArray = notification.object;
        
        for (int index = 0; index < quizQuestionsArray.count; index++) {
            if ([[quizQuestionsArray objectAtIndex:index] isKindOfClass:[NSDictionary class]] && [((NSDictionary*)[quizQuestionsArray objectAtIndex:index]) valueForKey:@"options"] && [[((NSDictionary*)[quizQuestionsArray objectAtIndex:index]) valueForKey:@"options"] isKindOfClass:[NSArray class]]) {
                
                NSArray* optionsArray = [((NSDictionary*)[quizQuestionsArray objectAtIndex:index]) valueForKey:@"options"];
                
                for (int innerIndex = 0; innerIndex < optionsArray.count; innerIndex++) {
                    if ([[optionsArray objectAtIndex:innerIndex] isKindOfClass:[NSDictionary class]] && [((NSDictionary*)[optionsArray objectAtIndex:innerIndex]) valueForKey:@"option_image"] && ![[((NSDictionary*)[optionsArray objectAtIndex:innerIndex]) valueForKey:@"option_image"] isKindOfClass:[NSNull class]]) {
                        UIImageView* imageView = [[UIImageView alloc] init];
                        [imageView setImageWithURL:[NSURL URLWithString:[((NSDictionary*)[optionsArray objectAtIndex:innerIndex]) valueForKey:@"option_image"]]];
                    }
                }
            }
        }
    }
}

- (void) logoutResponseReceived:(NSNotification*) notification
{
    if ([self.delegate respondsToSelector:@selector(handleLogoutFromQuiz)]) {
        [self.delegate handleLogoutFromQuiz];
    }
}

- (TMQuizInfo*)getQuinInfoForQuizId:(NSString*)quizId
{
    return [self getCurrentQuizInfoDictionary][quizId];
    // return self.quizInfoDictionary[quizId];
    
}

- (void) setIsNudgeActionCancelled:(BOOL)isNudgeActionCancelled {
    _isNudgeActionCancelled = isNudgeActionCancelled;
}

- (NSDictionary*) getCurrentQuizInfoDictionary
{
    return [self.quizInfoDictionary copy];
}

- (void) updateQuizNewStatus:(BOOL) isNewQuiz forQuizID:(NSString*) quizID
{
    TMQuizManager* quizManager = [[TMQuizManager alloc] initWithQuizController:self];
    [quizManager updateQuizNewStatus:isNewQuiz forQuizID:quizID];
}

/**
 * Method returns true if any new quiz is avaialble
 */
- (BOOL) isNewQuizAvailable
{
    [self fetchAllQuizInfo];
    
    NSArray* allKeyArray = [self.quizInfoDictionary allKeys];
    
    for (int index = 0; index < allKeyArray.count; index++) {
        if ([[self.quizInfoDictionary valueForKey:[allKeyArray objectAtIndex:index]] isKindOfClass:[TMQuizInfo class]]) {
            TMQuizInfo* localQuizInfo = (TMQuizInfo*)[self.quizInfoDictionary valueForKey:[allKeyArray objectAtIndex:index]];
            if (localQuizInfo.isNew) {
                return YES;
            }
        }
    }
    return NO;
}


#pragma mark - Quiz Status methods


/**
 * updates the quiz status by making a request on the basis of timestamp value stored
 * @param match ID
 */
//- (void) updateQuizStatusForOppositeMatch
//{
//    //check for the timestamp stored for this match id
//
//    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
//
//    BOOL isNetworkRequestRequiredForQuizStatusFetching = NO;
//
//    NSString* currentQuizTimelineKey = [NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId];
//
//    if (![TMDataStore containsObjectForKey:currentQuizTimelineKey] || ![[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] valueForKey:matchID]) {
//        isNetworkRequestRequiredForQuizStatusFetching = YES;
//    }
//    else {
//
//        NSString* lastQuizStateFetchDateString = [[[[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] valueForKey:matchID] componentsSeparatedByString:@"+"] firstObject];
//
//        NSDate* lastQuizStatusFetchDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[lastQuizStateFetchDateString floatValue]];
//        if ([[NSDate date] timeIntervalSinceDate:lastQuizStatusFetchDate] >= 60*60) {
//            isNetworkRequestRequiredForQuizStatusFetching = YES;
//        }
//    }
//
//    if (isNetworkRequestRequiredForQuizStatusFetching) {
//        //make network request for the match
//        [self fetchPlayedQuizzesForMatchID:matchID];
//    }
//}


//- (NSArray*) getAllUnreadQuizzes
//{
//    NSString* quizTimelineKey = [NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId];
//
//    NSString* matchIDKey = [[TMMessagingController sharedController] getCurrentMatchId];
//
//    if ([TMDataStore containsObjectForKey:quizTimelineKey] && [[TMDataStore retrieveObjectforKey:quizTimelineKey] valueForKey:matchIDKey]) {
//        NSString* matchTimelineString = [[TMDataStore retrieveObjectforKey:quizTimelineKey] valueForKey:matchIDKey];
//
//        if ([matchTimelineString componentsSeparatedByString:@"+"].count > 1) {
//            NSString* quizIDCombinedString = [[matchTimelineString componentsSeparatedByString:@"+"] objectAtIndex:1];
//
//            NSArray* quizIDArray = [quizIDCombinedString componentsSeparatedByString:@";"];
//
//            return quizIDArray;
//        }
//    }
//
//    return nil;
//
////    return ([TMDataStore containsObjectForKey:[NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId]] && [[TMDataStore retrieveObjectforKey:LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY] valueForKey:[[TMMessagingController sharedController] getCurrentMatchId]])?(([[[TMDataStore retrieveObjectforKey:[NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId]] valueForKey:[[TMMessagingController sharedController] getCurrentMatchId]] componentsSeparatedByString:@"+"].count > 1)?([[[[[TMDataStore retrieveObjectforKey:[NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId]] valueForKey:[[TMMessagingController sharedController] getCurrentMatchId]] componentsSeparatedByString:@"+"] objectAtIndex:1] componentsSeparatedByString:@";"]):nil):nil;
//}

//- (void) saveUpdatedStatusWithUpdatedQuizIDArray:(NSArray*) quizIDArray
//{
//    NSArray* previouUnreadQuizzesArray = [self getAllUnreadQuizzes];
//
//    if (previouUnreadQuizzesArray && previouUnreadQuizzesArray.count) {
//        quizIDArray = [quizIDArray arrayByAddingObjectsFromArray:previouUnreadQuizzesArray];
//    }
//
//    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
//
//    NSString* currentQuizTimelineKey = [NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId];
//
//    quizIDArray = [[NSOrderedSet orderedSetWithArray:quizIDArray] array];
//
//    //construct the ';' separarted key
//    NSString* updatedQuizIDString = [quizIDArray componentsJoinedByString:@";"];
//
//    //timestamp string
//    NSString* timeStampString = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSinceReferenceDate]];
//
//    NSString* combinedInfoString = (quizIDArray && quizIDArray.count > 0)?[NSString stringWithFormat:@"%@+%@",timeStampString,updatedQuizIDString]:timeStampString;
//
//    NSMutableDictionary* matchQuizTimestampMutableDictionary = ([TMDataStore containsObjectForKey:currentQuizTimelineKey])?[[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] mutableCopy]:[@{} mutableCopy];
//
//    [matchQuizTimestampMutableDictionary setObject:combinedInfoString forKey:matchID];
//
//    //update the cache with info
//    [TMDataStore setObject:matchQuizTimestampMutableDictionary forKey:currentQuizTimelineKey];
//}

- (BOOL) isQuizStatusUpdatedForOppositeMatch
{
    NSArray* allQuizzesKeyArray = [self.quizInfoDictionary allKeys];
    
    NSArray* unreadQuizzes = [self getUnreadQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId] userID:[TMUserSession sharedInstance].user.userId];
    
    for (int index = 0; index < allQuizzesKeyArray.count; index++) {
        if ([[self.quizInfoDictionary valueForKey:[allQuizzesKeyArray objectAtIndex:index]] isKindOfClass:[TMQuizInfo class]]) {
            TMQuizInfo* currentQuizInfo = [self.quizInfoDictionary valueForKey:[allQuizzesKeyArray objectAtIndex:index]];
            BOOL quizStatus = [unreadQuizzes containsObject:currentQuizInfo.quizID];
            if (quizStatus) {
                return YES;
            }
        }
    }
    return NO;
}


//- (BOOL) isQuizStatusUpdatedForOppositeMatchWithQuizID:(NSString*) quizID
//{
//    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
//
//    NSString* currentQuizTimelineKey = [NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId];
//
//    if ([TMDataStore containsObjectForKey:currentQuizTimelineKey] && [[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] valueForKey:matchID]) {
//        NSString* combinedInfoString = [[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] valueForKey:matchID];
//
//        if ([combinedInfoString componentsSeparatedByString:@"+"].count > 1) {
//            NSString* updatedQuizzesString = [[combinedInfoString componentsSeparatedByString:@"+"] objectAtIndex:1];
//            if (updatedQuizzesString && [updatedQuizzesString componentsSeparatedByString:@";"].count > 0) {
//                NSArray* updatedQuizIDAray = [updatedQuizzesString componentsSeparatedByString:@";"];
//                if ([updatedQuizIDAray containsObject:quizID]) {
//                    return YES;
//                }
//            }
//        }
//    }
//    return NO;
//}


//- (void) resetQuizStatusForQuizID:(NSString*) quizID
//{
//    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
//
//    NSString* currentQuizTimelineKey = [NSString stringWithFormat:@"%@_%@",LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY,[TMUserSession sharedInstance].user.userId];
//
//    if ([TMDataStore containsObjectForKey:currentQuizTimelineKey] && [[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] valueForKey:matchID]) {
//        NSString* combinedInfoString = [[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] valueForKey:matchID];
//        if ([combinedInfoString componentsSeparatedByString:@"+"].count > 1) {
//            NSString* updatedQuizIDCombinedString = [[combinedInfoString componentsSeparatedByString:@"+"] objectAtIndex:1];
//
//            if ([updatedQuizIDCombinedString componentsSeparatedByString:@";"].count > 0) {
//                NSArray* updatedQuizIDArray = [updatedQuizIDCombinedString componentsSeparatedByString:@";"];
//                NSMutableArray* finalQuizIDArray = [[NSMutableArray alloc] init];
//                for (int index = 0; index < updatedQuizIDArray.count; index++) {
//                    NSString* localQuizID = [updatedQuizIDArray objectAtIndex:index];
//                    if (![localQuizID isEqualToString:quizID]) {
//                        [finalQuizIDArray addObject:localQuizID];
//                    }
//                }
//
//                finalQuizIDArray = [[[NSOrderedSet orderedSetWithArray:finalQuizIDArray] array] mutableCopy];
//
//                //update the cache after removing the quiz id
//                if (finalQuizIDArray.count > 0) {
//                    NSString* finalQuizIDArrayString = [finalQuizIDArray componentsJoinedByString:@";"];
//                    combinedInfoString = [NSString stringWithFormat:@"%@+%@",[[combinedInfoString componentsSeparatedByString:@"+"] firstObject],finalQuizIDArrayString];
//                }
//                else {
//                    combinedInfoString = [[combinedInfoString componentsSeparatedByString:@"+"] firstObject];
//                }
//
//                NSMutableDictionary* matchQuizTimestampMutableDictionary = ([TMDataStore containsObjectForKey:currentQuizTimelineKey])?[[TMDataStore retrieveObjectforKey:currentQuizTimelineKey] mutableCopy]:[@{} mutableCopy];
//
//                [matchQuizTimestampMutableDictionary setObject:combinedInfoString forKey:matchID];
//
//                [TMDataStore setObject:matchQuizTimestampMutableDictionary forKey:currentQuizTimelineKey];
//            }
//        }
//    }
//}

#pragma mark - Quiz status call method

- (void) makeQuizRequestForStatusAndFlare
{
    TMQuizManager* quizManager = [[TMQuizManager alloc] init];
    [quizManager getPlayedQuizFlareForMatchID];
}


//#pragma quiz status methods

- (BOOL) removeQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID quizID:(NSString*) quizID {
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    return [quizCacheManager removeQuizzesStatusForMatchID:matchID userID:userID quizID:quizID];
}

- (BOOL) removeAllQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID {
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    return [quizCacheManager removeAllQuizzesStatusForMatchID:matchID userID:userID];
}
- (NSArray*) getUnreadQuizzesForMatchID:(NSString*) matchID userID:(NSString*) userID {
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    return [quizCacheManager getUnreadQuizzesForMatchID:matchID userID:userID];
}


- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"autoNudgeResponseRecieved" object:nil];
}


@end

//
//  TMQuizCommonQuestionTableViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 08/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizQuestionDomainObject.h"

@interface TMQuizCommonQuestionTableViewCell : UITableViewCell

- (void) fillQuestionDetailsForQuestionObject:(QuizQuestionDomainObject*) questionDomainObject andQuestionNumber:(int) questionNo;


@end

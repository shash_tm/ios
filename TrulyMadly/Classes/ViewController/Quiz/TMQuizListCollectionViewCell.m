//
//  TMQuizListCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 29/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizListCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface TMQuizListCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *quizImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIImageView *matchImageView;
@property (weak, nonatomic) IBOutlet UIImageView *commonImageView;
@property (weak, nonatomic) IBOutlet UILabel *quizNameLabel;
@property (strong, nonatomic) UIImageView *flareImageView;
@property (strong, nonatomic) UIView* flareBackgroundView;

@end

#define QUIZ_NEW_STATUS_VIEW 11901
#define QUIZ_NEW_STATUS_LABEL_TAG 10901

@implementation TMQuizListCollectionViewCell

- (void)awakeFromNib {
   
    [super awakeFromNib];
    
    //quiz image view
    self.quizImageView.clipsToBounds = YES;
    self.quizImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.quizImageView.layer.cornerRadius = self.quizImageView.bounds.size.width/2;
    self.quizImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.quizImageView.layer.borderWidth = 1.0f;
    
    
    //user image view
    self.userImageView.clipsToBounds = YES;
    self.userImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                           | UIViewAutoresizingFlexibleHeight
                                           | UIViewAutoresizingFlexibleLeftMargin
                                           | UIViewAutoresizingFlexibleRightMargin
                                           | UIViewAutoresizingFlexibleTopMargin
                                           | UIViewAutoresizingFlexibleWidth);
    self.userImageView.layer.cornerRadius = self.userImageView.bounds.size.width/2;
    self.userImageView.layer.borderWidth = 1.0;
    self.userImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //match image view
    self.matchImageView.clipsToBounds = YES;
    self.matchImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.matchImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                           | UIViewAutoresizingFlexibleHeight
                                           | UIViewAutoresizingFlexibleLeftMargin
                                           | UIViewAutoresizingFlexibleRightMargin
                                           | UIViewAutoresizingFlexibleTopMargin
                                           | UIViewAutoresizingFlexibleWidth);
     self.matchImageView.layer.cornerRadius = self.matchImageView.bounds.size.width/2;
    self.matchImageView.layer.borderWidth = 1.0;
    self.matchImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    //common image view
    self.commonImageView.clipsToBounds = YES;
    self.commonImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.commonImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                            | UIViewAutoresizingFlexibleHeight
                                            | UIViewAutoresizingFlexibleLeftMargin
                                            | UIViewAutoresizingFlexibleRightMargin
                                            | UIViewAutoresizingFlexibleTopMargin
                                            | UIViewAutoresizingFlexibleWidth);
    self.commonImageView.layer.cornerRadius = self.commonImageView.bounds.size.width/2;
    self.commonImageView.layer.borderWidth = 1.0;
    self.commonImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.flareBackgroundView = [[UIView alloc] initWithFrame:self.quizImageView.bounds];
    self.flareBackgroundView.alpha = 0.55f;
    self.flareBackgroundView.backgroundColor = [UIColor blackColor];
    self.flareBackgroundView.hidden = YES;
    self.flareBackgroundView.layer.cornerRadius = self.flareBackgroundView.bounds.size.width/2;
    self.flareBackgroundView.clipsToBounds = YES;
    [self.quizImageView addSubview:self.flareBackgroundView];
    
    self.flareImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.quizImageView.bounds.size.width/4, self.quizImageView.bounds.size.height/4, self.quizImageView.bounds.size.width/2, self.quizImageView.bounds.size.height/2)];
    self.flareImageView.image = [UIImage imageNamed:@"quiz_flare"];
    self.flareImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.flareImageView.backgroundColor = [UIColor clearColor];
    self.flareImageView.hidden = YES;
    [self.quizImageView addSubview:self.flareImageView];
    
    self.quizImageView.layer.cornerRadius = self.quizImageView.bounds.size.width/2;
    self.quizImageView.clipsToBounds = YES;
    
    [self bringSubviewToFront:self.matchImageView];
    [self bringSubviewToFront:self.userImageView];
    
    self.userImageView.hidden = YES;
    self.matchImageView.hidden = YES;
    self.commonImageView.hidden = YES;
}

- (void) setQuizUnreadStatus:(BOOL) unreadStatus
{
    if (unreadStatus) {
        self.matchImageView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        
        [UIView animateWithDuration:0.4 delay:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.matchImageView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL completed){
            //do something upon completion
        }];
    }
    else {
         self.matchImageView.transform = CGAffineTransformIdentity;
    }
}

-(void)setQuizIconFromURL:(NSURL*)imageURL
{
    [self.quizImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
}

- (void) setQuizNewStatus:(BOOL) quizNewStatus
{
    //show the overlay with the new status
    
    if ([self.quizImageView viewWithTag:QUIZ_NEW_STATUS_LABEL_TAG]) {
        [[self.quizImageView viewWithTag:QUIZ_NEW_STATUS_LABEL_TAG] removeFromSuperview];
    }
    if ([self.quizImageView viewWithTag:QUIZ_NEW_STATUS_VIEW]) {
        [[self.quizImageView viewWithTag:QUIZ_NEW_STATUS_VIEW] removeFromSuperview];
    }
    
    if (quizNewStatus) {
        
        UIView* quizNewStatusView = [[UIView alloc] initWithFrame:self.quizImageView.bounds];
        quizNewStatusView.tag = QUIZ_NEW_STATUS_VIEW;
        quizNewStatusView.backgroundColor = [UIColor blackColor];
        quizNewStatusView.alpha = 0.6f;
        [self.quizImageView addSubview:quizNewStatusView];
        
        UILabel* quizNewStatusLabel = [[UILabel alloc] initWithFrame:self.quizImageView.bounds];
        quizNewStatusLabel.tag = QUIZ_NEW_STATUS_LABEL_TAG;
        quizNewStatusLabel.backgroundColor = [UIColor clearColor];
        quizNewStatusLabel.textColor = [UIColor whiteColor];
        quizNewStatusLabel.text = @"New";
        quizNewStatusLabel.textAlignment = NSTextAlignmentCenter;
        [self.quizImageView addSubview:quizNewStatusLabel];
    }
}

- (void) setFlareStatus:(BOOL) flareStatus
{
    self.flareImageView.hidden = !flareStatus;
    self.flareBackgroundView.hidden = !flareStatus;
}

- (void) resetQuizState
{
    self.flareBackgroundView.hidden = YES;
    self.flareImageView.hidden = YES;
    
    if ([self.quizImageView viewWithTag:QUIZ_NEW_STATUS_LABEL_TAG]) {
        [[self.quizImageView viewWithTag:QUIZ_NEW_STATUS_LABEL_TAG] removeFromSuperview];
    }
    if ([self.quizImageView viewWithTag:QUIZ_NEW_STATUS_VIEW]) {
        [[self.quizImageView viewWithTag:QUIZ_NEW_STATUS_VIEW] removeFromSuperview];
    }
}


- (void) resetQuizUserImages
{
    self.userImageView.image = nil;
    self.matchImageView.image = nil;
    self.commonImageView.image = nil;
    
    self.userImageView.hidden = YES;
    self.matchImageView.hidden = YES;
    self.commonImageView.hidden = YES;
}

- (void) setCommonImagePlaceholder {
    [self resetQuizUserImages];
    self.commonImageView.hidden = NO;
    self.commonImageView.image = [UIImage imageNamed:@"placeholder_profile"];
}

- (void) setBothImagePlaceholder {
    [self resetQuizUserImages];
    self.userImageView.hidden = NO;
    self.matchImageView.hidden = NO;
    [self.userImageView setImage:[UIImage imageNamed:@"placeholder_profile"]];
    [self.matchImageView setImage:[UIImage imageNamed:@"placeholder_profile"]];
}

- (void) setBothImageWithUserProfilePlaceholderAndMatchImageURLString:(NSString*) matchImageURLString {
    [self resetQuizUserImages];
    
    
    self.userImageView.hidden = NO;
    self.matchImageView.hidden = NO;
    self.userImageView.image = [UIImage imageNamed:@"placeholder_profile"];
    
    if (matchImageURLString) {
        [self.matchImageView setImageWithURL:[NSURL URLWithString:matchImageURLString] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    }
    else {
        self.matchImageView.image = [UIImage imageNamed:@"placeholder_profile"];
    }
}

- (void) setBothImageWithMatchProfilePlaceholderAndUserImageURLString:(NSString*) userImageURLString {
    [self resetQuizUserImages];
    
    self.userImageView.hidden = NO;
    self.matchImageView.hidden = NO;
    
    self.matchImageView.image = [UIImage imageNamed:@"placeholder_profile"];
    
    if (userImageURLString) {
        [self.userImageView setImageWithURL:[NSURL URLWithString:userImageURLString] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    }
    else {
        self.userImageView.image = [UIImage imageNamed:@"placeholder_profile"];
    }
}


- (void) setQuizCommonStatusFromImageURLString:(NSString*) commonImageURLString
{
    [self resetQuizUserImages];
    
    if (commonImageURLString) {
        self.commonImageView.hidden = NO;
        
        [self.commonImageView setImageWithURL:[NSURL URLWithString:commonImageURLString] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    }
}

- (void) setQuizBothPlayedStatusFromUserImageURLString:(NSString*) userImageURLString matchImageURLString:(NSString*) matchImageURLString
{
    [self resetQuizUserImages];
    
    self.userImageView.hidden = NO;
    self.matchImageView.hidden = NO;
    
    [self.userImageView setImageWithURL:[NSURL URLWithString:userImageURLString] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
    [self.matchImageView setImageWithURL:[NSURL URLWithString:matchImageURLString] placeholderImage:[UIImage imageNamed:@"placeholder_profile"]];
}


-(void)setQuizName:(NSString*)name
{
    self.quizNameLabel.text = name;
}

@end

//
//  TMCitySelectionViewController.m
//  TrulyMadly
//
//  Created by Ankit on 18/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCitySelectionViewController.h"
#import "TMAnalytics.h"

@interface TMCitySelectionViewController ()

@property(nonatomic,strong)TMUserCitySelectionView* userCitySelectionView;
@property(nonatomic,assign)NSInteger keyboardHeight;

@end

@implementation TMCitySelectionViewController

- (instancetype)initWithCityData:(NSArray*)cityData
                 popularCityData:(NSArray*)popularCityData
                        delegate:(id<TMUserCitySelectionViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        [self.userCitySelectionView loadCityList:cityData popularCityList:popularCityData];
        self.userCitySelectionView.delegate = delegate;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
    [self startTrackingEvents];
    [self showUserCitySelectionView];
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateCitySelectionViewFrameWithKeyboardHeight:self.keyboardHeight];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(TMUserCitySelectionView*)userCitySelectionView {
    if(!_userCitySelectionView) {
        _userCitySelectionView = [[TMUserCitySelectionView alloc] initWithFrame:CGRectZero];
        
    }
    return _userCitySelectionView;
}

- (void)startTrackingEvents {
    [[TMAnalytics sharedInstance] trackView:@"TMCitySelectionViewController"];
}
-(void)showUserCitySelectionView {
    [self.view addSubview:self.userCitySelectionView];
}

-(void)keyboardOnScreen:(NSNotification *)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [keyboardFrameBegin CGRectValue];
    self.keyboardHeight = CGRectGetHeight(keyboardRect);
    
    [self updateCitySelectionViewFrameWithKeyboardHeight:self.keyboardHeight];
}

-(void)updateCitySelectionViewFrameWithKeyboardHeight:(CGFloat)keyboardHeight {
    CGFloat yPos = 0;
    CGFloat availableHeight = CGRectGetHeight(self.view.frame) - keyboardHeight;
    CGFloat height = availableHeight - yPos;
    self.userCitySelectionView.frame = CGRectMake(CGRectGetMinX(self.view.frame),
                                                  yPos,
                                                  CGRectGetWidth(self.view.frame),
                                                  height);
}

@end

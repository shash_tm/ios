//
//  TMWorkViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

class TMWorkViewController: TMBaseViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var registerData:NSDictionary!
    
    var industryPicker: UIPickerView!
    
    var cookieData: TMCookieData!
    
    var allIncome: NSMutableArray!
    
    var scrollView: UIScrollView!
    
    var companyView: UIView!
    var belowCompanyView: UIView!
    var incomeButton: UIButton!
    var dropDownImage: UIImageView!
    var continueButton: UIButton!
    var dropDownImage1: UIImageView!
    
    var addIndustryButton: UIButton!
    var designationText:UITextField!
    var companyText:UITextField!
    
    var workView: UIView!
    
    var addImage: UIImage!
    
    var pickerCancel: UIButton!
    var pickerDone: UIButton!
    var pickerSeparator: UIView!
    
    var indexForIncome: Int = 0
    
    var imageName = ["Dropdown","LinkedIn","remove"]
    
    var bounds = UIScreen.main.bounds as CGRect
    
    var companyArr = [String]()
    var collegeArr = [String]()
    var position = "";
    
    var isIncomeSelected = false
    
    var editProfile: TMEditProfile = TMEditProfile()
    
    var extraH:CGFloat = 0.0
    
    var activeTextField: UITextField!
    
    var image: UIImage = UIImage(named: "Dropdown_black")!
    
    var navigationFlow:TMNavigationFlow!
    
    var activityIndicator: TMActivityIndicatorView!
    var activityLoader: UIActivityIndicatorView!
    
    var incomeStart = ""
    var incomeEnd = ""
    var addButtonWidth: CGFloat = 0.0
    
    var cancelCompany: String = ""
    
    var alertTitle: String = "Problem in saving the data"
    
    var eventCategory: String = "register_basics"
    
    var startDate = Date()
    
    convenience init(navigationFlow: TMNavigationFlow, registerData: NSDictionary) {
        self.init()
        self.navigationFlow = navigationFlow
        self.registerData = registerData
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        TMAnalytics.sharedInstance().trackView("TMWorkViewController")
        
        self.view.backgroundColor = UIColor.white
        
        self.navigationItem.title = "Work"
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TMWorkViewController.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tap)
        
        if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_EDITPROFILE) {
            extraH = 64.0
        }
        
        self.eventCategory = "edit_profile"
        self.loadData()

    }
    
    func loadData() {
    
        if(editProfile.isNetworkReachable()) {
            self.loadEditData()
        }
        else {
            if(editProfile.isCachedResponsePresent()) {
                self.loadEditData()
            }
            else {
                  self.showRetryViewForError()
            }
        }
    }
    
    func loadEditData() {
        let queryString = ["login_mobile":"true"]
        self.addIndicator()
        self.activityIndicator.titleText = "Loading..."
        self.activityIndicator.startAnimating()
        
        var eventDict:Dictionary<String,AnyObject> = [:]
        eventDict["screenName"] = "TMWorkViewController" as AnyObject?
        eventDict["eventCategory"] = self.eventCategory as AnyObject?
        eventDict["eventAction"] = "page_load" as AnyObject?
        editProfile.trackEventDictionary = NSMutableDictionary(dictionary: eventDict)
        
        editProfile.initCall(queryString as NSDictionary as [NSObject : AnyObject], with: {(response:[AnyHashable:Any]?,error:TMError?) -> Void in
            self.activityIndicator.stopAnimating()
            if(error == nil) {
                self.registerData = response?["user_data"] as! NSDictionary
                
                self.initializeView()
                self.prefillForEdit()
                self.changePlaceholder()
                
            }else {
                if(error?.errorCode == .TMERRORCODE_LOGOUT) {
                    if(self.actionDelegate !=  nil) {
                        self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
                    }
                }
                else if(error?.errorCode == .TMERRORCODE_NONETWORK || error?.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
                    if(!self.editProfile.isCachedResponsePresent()) {
                        if(error?.errorCode == .TMERRORCODE_NONETWORK) {
                            self.showRetryView()
                        }
                        else {
                            self.showRetryViewForError()
                        }
                    }
                }
                else {
                    self.showRetryViewForUnknownError()
                }
            }
        })
    }

    func showRetryViewForUnknownError() {
        if(!self.isRetryViewShown) {
            self.showRetryView(withMessage: TM_UNKNOWN_MSG)
            self.retryView.retryButton.addTarget(self, action: #selector(TMWorkViewController.loadData), for: UIControlEvents.touchUpInside)
        }
    }
    
    func showRetryViewForError() {
        if(!self.isRetryViewShown) {
            self.showRetryView()
            self.retryView.retryButton.addTarget(self, action: #selector(TMWorkViewController.loadData), for: UIControlEvents.touchUpInside)
        }
    }

    func dismissKeyboard(_ sender: AnyObject) {
        if(self.activeTextField != nil) {
            self.activeTextField.resignFirstResponder()
        }
    }
    

    func addIndicator() {
        if(self.isRetryViewShown) {
            self.removeRetryView()
        }
        let fullScreen = TMFullScreenView()
        fullScreen.frame = self.view.frame
        fullScreen.backgroundColor = UIColor.white
        fullScreen.alpha = 0.8
        self.view.addSubview(fullScreen)
        
        self.activityIndicator = TMActivityIndicatorView()
        self.view.addSubview(self.activityIndicator)
        
        self.view.bringSubview(toFront: fullScreen)
        
        self.view.bringSubview(toFront: self.activityIndicator)
    }
    
    func removeLoader() {
        if(self.activityIndicator != nil) {
            self.activityIndicator.stopAnimating()
        }
        for view in self.view.subviews {
            if(view.isKind(of: TMFullScreenView.self)) {
                view.removeFromSuperview()
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func initializeView() {
        
        var textDict:Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 18)
        
        var y: CGFloat = 0.0
        
        self.industryPicker = UIPickerView()
        self.industryPicker.frame.size.width = bounds.size.width
        self.industryPicker.frame.size.height = self.industryPicker.frame.size.height-54
        self.industryPicker.frame.origin.y = bounds.size.height - self.industryPicker.frame.size.height - self.extraH
        self.industryPicker.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.industryPicker.isHidden = true
        self.industryPicker.dataSource = self
        self.industryPicker.delegate = self
        self.view.addSubview(self.industryPicker)
        
        self.pickerSeparator = UIView(frame: CGRect(x: 0,y: self.industryPicker.frame.origin.y-50,width: bounds.size.width,height: 50))
        self.pickerSeparator.backgroundColor = UIColor(red: 0.922, green:0.922, blue:0.922, alpha:1.0)
        self.pickerSeparator.isHidden = true
        self.view.addSubview(self.pickerSeparator)
        
        textDict["ktext"] = "Cancel" as AnyObject
        let stringFrame = TMStringUtil.textRect(forString: textDict)
        let stringWidth = stringFrame.size.width
        var stringHeight = stringFrame.size.height
        
        self.pickerCancel = UIButton(type: UIButtonType.system)
        self.pickerCancel.frame = CGRect(x: 16, y: 0, width: stringWidth,height: 50)
        self.pickerCancel.setTitle("Cancel", for: UIControlState())
        self.pickerCancel.titleLabel?.font = UIFont.systemFont(ofSize: 18)//UIFont(name: "Arial", size: 14)
        self.pickerCancel.addTarget(self, action: #selector(TMWorkViewController.hidePicker(_:)), for: UIControlEvents.touchUpInside)
        self.pickerCancel.isHidden = true
        self.pickerSeparator.addSubview(self.pickerCancel)
        
        textDict["ktext"] = "Done" as AnyObject
        
        let doneFrame = TMStringUtil.textRect(forString: textDict) as CGRect
        self.pickerDone = UIButton(type: UIButtonType.system)
        self.pickerDone.frame = CGRect(x: bounds.size.width-doneFrame.size.width-16, y: 0, width: doneFrame.size.width,height: 50)
        self.pickerDone.setTitle("Done", for: UIControlState())
        self.pickerDone.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.pickerDone.addTarget(self, action: #selector(TMWorkViewController.donePicker(_:)), for: UIControlEvents.touchUpInside)
        self.pickerDone.isHidden = true
        self.pickerSeparator.addSubview(self.pickerDone)
        
        
        textDict["kfont"] = UIFont.systemFont(ofSize: 15)
        
        let naviHeight = self.navigationController?.navigationBar.bounds.size.height
        let statusBarHeight:CGFloat = UIApplication.shared.statusBarFrame.size.height
        
        self.extraH = 64.0
        //scroll view
        self.scrollView = UIScrollView(frame: UIScreen.main.bounds)
        if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_REGISTRATION) {
            if(naviHeight != nil) {
                self.scrollView.frame.origin.y = naviHeight! + 16.0 + statusBarHeight
            }else {
                self.scrollView.frame.origin.y = 44.0 + 16.0 + statusBarHeight
            }
        }else{
            self.scrollView.frame.origin.y = 16//naviHeight! + 30.0 + statusBarHeight
        }
        self.scrollView.frame.size.width = bounds.size.width
        self.scrollView.frame.size.height = bounds.size.height-self.scrollView.frame.origin.y
        self.view.addSubview(self.scrollView)
        self.workView = UIView(frame: CGRect(x: 8, y: y+10, width: bounds.size.width-16, height: 0))
        self.workView.layer.borderWidth = 0.0
        self.scrollView.addSubview(workView)
        
        //designation text field
        let placeholder1 = NSAttributedString(string: "Current Designation", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
        self.designationText = UITextField(frame: CGRect(x: 8, y: 0, width: self.workView.frame.size.width-16, height: 44))
        self.designationText.attributedPlaceholder = placeholder1
        self.designationText.font = UIFont.systemFont(ofSize: 15)
        self.designationText.delegate = self
        self.designationText.autocorrectionType = .no
        self.designationText.addTarget(self, action: #selector(TMWorkViewController.didDesignationChanged(_:)),for:UIControlEvents.editingChanged)
        self.designationText.layer.cornerRadius = 10
        self.designationText.layer.borderWidth = 0.3
        self.designationText.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        let leftView = UIView(frame: CGRect(x: 0,y: 0,width: 10,height: self.designationText.frame.size.height))
        self.designationText.leftView = leftView
        self.designationText.leftViewMode = .always
        self.workView.addSubview(self.designationText)
        
        
        self.workView.frame.size.height = self.workView.frame.size.height + self.designationText.frame.size.height + 10
        
        
        y = self.designationText.frame.origin.y
        
        //let optional = NSMutableAttributedString(string: "Income (Optional)", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
        
        
        // income button
        self.incomeButton = UIButton(type: UIButtonType.system)
        self.incomeButton.frame = CGRect(x: 8,y: y+self.designationText.frame.size.height+10, width: self.workView.frame.size.width-16, height: 0)
        /*self.incomeButton.setTitleColor(UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), for: UIControlState())
        self.incomeButton.setAttributedTitle(optional, for: UIControlState())
        self.incomeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)//UIFont(name: "Arial", size: 14)
        self.incomeButton.titleLabel?.textAlignment = .left
        self.incomeButton.addTarget(self, action: #selector(TMWorkViewController.onIncomeClick(_:)), for: UIControlEvents.touchUpInside)
        self.incomeButton.layer.cornerRadius = 10
        self.incomeButton.layer.borderWidth = 0.3
        self.incomeButton.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.incomeButton.contentHorizontalAlignment = .left
        self.incomeButton.contentEdgeInsets = UIEdgeInsets(top:0.0,left:10.0,bottom:0.0,right:0.0)
        self.workView.addSubview(self.incomeButton)
        
        self.workView.frame.size.height = self.workView.frame.size.height + self.incomeButton.frame.size.height + 10
        */
        
        y = self.incomeButton.frame.origin.y+10
       
        //drop down image
//        self.dropDownImage1 = UIImageView()
//        let image2: UIImage = UIImage(named: imageName[0])!
//        self.dropDownImage1.image = image2
//        self.dropDownImage1.frame = CGRect(x: (self.incomeButton.frame.size.width-20), y: (self.incomeButton.frame.origin.y+(self.incomeButton.frame.size.height-10)/2)+2 ,width: 16,height: 10)
//        self.workView.addSubview(self.dropDownImage1)
//        
        
        y = self.incomeButton.frame.origin.y
        //comapanies text field
        
        self.companyView = UIView(frame: CGRect(x: 8, y: y+self.incomeButton.frame.size.height+5, width: self.workView.frame.size.width-16, height: 54))
        self.companyView.layer.cornerRadius = 10
        self.workView.addSubview(companyView)
        
        self.companyText = UITextField(frame: CGRect(x: 0,y: 5, width: self.workView.frame.size.width-16, height: 44))
        self.companyText.font = UIFont.systemFont(ofSize: 15)
        self.companyText.addTarget(self, action: #selector(TMWorkViewController.onEditCompanies(_:)),for:UIControlEvents.editingChanged)
        self.companyText.tag = 1
        self.companyText.autocorrectionType = .no
        self.companyText.layer.cornerRadius = 10
        self.companyText.layer.borderWidth = 0.3
        self.companyText.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.companyText.delegate = self
        let leftView1 = UIView(frame: CGRect(x: 0,y: 0,width: 10,height: self.companyText.frame.size.height))
        self.companyText.leftView = leftView1
        self.companyText.leftViewMode = .always
        self.companyView.addSubview(self.companyText)
        
        self.workView.frame.size.height = self.workView.frame.size.height + self.companyView.frame.size.height + 10
        
        y = self.companyText.frame.origin.y
        
        //college add button
        textDict["kfont"] = UIFont.systemFont(ofSize: 14)
        textDict["ktext"] = "Add" as AnyObject
        
        self.addImage = UIImage(named: "add_button")!
        self.addButtonWidth = TMStringUtil.textRect(forString: textDict).size.width+30.0
        
        let rightView = UIView(frame: CGRect(x: 0,y: 0, width: addButtonWidth, height: 44))
        self.companyText.rightView = rightView
        self.companyText.rightViewMode = .always
        
        self.addIndustryButton = UIButton(type: UIButtonType.system)
        self.addIndustryButton.frame = CGRect(x: self.companyText.frame.origin.x+self.companyText.frame.size.width-self.addButtonWidth, y: self.companyText.frame.origin.y, width: addButtonWidth, height: 44)
        self.addIndustryButton.setTitleColor(UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0), for: UIControlState())
        self.addIndustryButton.setTitle("Add", for: UIControlState())
        self.addIndustryButton.tag = 10000
        self.addIndustryButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.addIndustryButton.addTarget(self, action: #selector(TMWorkViewController.onAddCompany(_:)), for: UIControlEvents.touchUpInside)
        self.addIndustryButton.titleLabel?.textAlignment = .center
        self.addIndustryButton.isHidden = true
        self.addIndustryButton.setBackgroundImage(addImage, for: UIControlState())
        self.companyView.addSubview(self.addIndustryButton)
        
        y = self.companyText.frame.origin.y
        
        self.workView.frame.size.height = self.workView.frame.size.height + 13.0
        
        //below work section
        y = self.workView.frame.origin.y
        
        //continue button
        self.continueButton = UIButton(type: UIButtonType.system)
        self.continueButton.frame = CGRect(x: 16, y: y+self.workView.frame.size.height+12, width: bounds.size.width-32, height: 44)
        self.continueButton.setTitleColor(UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), for: UIControlState())
        self.continueButton.setTitle("Save", for: UIControlState())
        self.continueButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.continueButton.addTarget(self, action: #selector(TMWorkViewController.onContinue(_:)), for: UIControlEvents.touchUpInside)
        self.continueButton.titleLabel?.textAlignment = .center
        self.continueButton.isEnabled = false
        self.continueButton.layer.cornerRadius = 10
        self.continueButton.backgroundColor = UIColor(red: 0.902, green: 0.906, blue: 0.91, alpha: 1.0)
        self.scrollView.addSubview(self.continueButton)
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0+self.extraH)
        
        self.view.bringSubview(toFront: self.industryPicker)
        self.view.bringSubview(toFront: self.pickerCancel)
        self.view.bringSubview(toFront: self.pickerDone)
        self.view.bringSubview(toFront: self.pickerSeparator)
        
    }

    func onIncomeClick(_ sender: UIButton) {
        self.view.endEditing(true)
        self.industryPicker.isHidden = false
        self.industryPicker.tag = 2
        self.pickerCancel.isHidden = false
        self.pickerDone.isHidden = false
        self.pickerSeparator.isHidden = false
        self.industryPicker.reloadAllComponents()
        self.industryPicker.selectRow(self.indexForIncome, inComponent: 0, animated: false)
    }
    
    
    func hidePicker(_ sender: AnyObject) {
        self.industryPicker.isHidden = true
        self.pickerCancel.isHidden = true
        self.pickerDone.isHidden = true
        self.pickerSeparator.isHidden = true
    }
    
    func donePicker(_ sender: UIButton) {
        if(self.industryPicker.tag == 2) {
            //self.updateIncome(self.indexForIncome)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView,numberOfRowsInComponent component: Int) -> Int{
        if(pickerView.tag == 2) {
            return self.allIncome.count+1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String?{
        
        if(pickerView.tag == 2) {
            if(row == 0) {
                return "Not Applicable"
            }
            let incomeData = self.allIncome[row-1] as! NSDictionary
            return incomeData["value"] as! String!
        }
        
//        if(pickerView.tag == 2) {
//            return self.allIncome[row]["value"] as! String!
//        }
        return " "
    }
    
    func pickerView(_ pickerView: UIPickerView,didSelectRow row: Int,inComponent component: Int){
        
        if(pickerView.tag == 2) {
            self.indexForIncome = row
        }
        
    }
    
    func disableDesignationText(_ flag: Bool) {
        if(flag) {
            self.designationText.isEnabled = true
            self.designationText.text = "Not Working"
            self.designationText.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.designationText.layer.borderWidth = 1.0
            self.designationText.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).cgColor
        }else {
            self.designationText.isEnabled = true
            if(self.designationText.text == "Not Working") {
                self.designationText.text = ""
            }
            self.designationText.layer.borderWidth = 0.3
            self.designationText.layer.borderColor = UIColor(red: 0.502, green: 0.51, blue: 0.522, alpha: 1.0).cgColor
            self.designationText.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.didDesignationChanged(true as AnyObject)
        }
    }
    
    func updateIncome(_ row:Int) {
        let optional = NSMutableAttributedString(string: "", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
        self.incomeButton.setAttributedTitle(optional, for: UIControlState())
        
        self.indexForIncome = row
        self.industryPicker.isHidden = true
        self.pickerCancel.isHidden = true
        self.pickerDone.isHidden = true
        self.pickerSeparator.isHidden = true
    
        
        if(row == 0) {
            self.incomeButton.setTitle("Not Applicable", for:UIControlState())
            self.incomeStart = "-1"
            self.incomeEnd = "-1"
            
            self.dropDownImage1.image = image
            self.incomeButton.setTitleColor(UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0), for: UIControlState())
            self.incomeButton.layer.borderColor = UIColor.black.cgColor
            self.incomeButton.layer.borderWidth = 1.0
        }else if(row == -1) {
            self.indexForIncome = 0
            self.incomeButton.setTitle("Add Income", for:UIControlState())
            self.incomeStart = ""
            self.incomeEnd = ""
        }
        else {
            self.dropDownImage1.image = image
            self.incomeButton.setTitleColor(UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0), for: UIControlState())
            self.incomeButton.layer.borderColor = UIColor.black.cgColor
            self.incomeButton.layer.borderWidth = 1.0
            
            let incomeData = self.allIncome[row-1] as! NSDictionary
            self.incomeButton.setTitle(incomeData["value"] as! String!, for:UIControlState())
            let tmp =  incomeData["key"] as! String!
            let incomeArr = tmp?.components(separatedBy: "-") as NSArray!
            self.incomeStart = incomeArr?[0] as! String
            self.incomeEnd = incomeArr?[1] as! String
        }
        self.isIncomeSelected = true
    }
    
    func onEditCompanies(_ sender: UIButton) {
        if(companyText.text != ""){
            self.addIndustryButton.isHidden = false
        }else{
            self.addIndustryButton.isHidden = true
        }
    }
    
    func onAddCompany(_ sender: AnyObject) {
        self.addIndustryButton.isHidden = true
        self.addCompany(self.companyText.text!)
        self.companyText.text = ""
    }
    
    func addCompany(_ value:String){
        self.addIndustryButton.isHidden = true
        if(self.companyArr.filter { $0 == value }).count>0{
        }else{
            self.addIndustryButton.isHidden = true
            let newString = value.trimmingCharacters(in: CharacterSet.whitespaces)
            if(newString != "") {
                companyArr.append(newString)
                self.addToCompanyView(false)
            }
            self.companyText.text = ""
            if(self.companyArr.count == 1) {
                self.changePlaceholder()
            }
        }
    }
    
    func removeCompany(_ sender:UIButton!){
        
        self.cancelCompany = self.companyArr[sender.tag]
        
        let alert = UIAlertView(title: "" , message: "Are you sure you want to remove this company from the list?" , delegate: self, cancelButtonTitle: "No")
        alert.addButton(withTitle: "Yes")
        
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }

    func alertView(_ alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        let title: NSString = alertView.buttonTitle(at: buttonIndex)! as NSString
        if(title == "Yes") {
            self.companyArr = self.companyArr.filter { $0 != self.cancelCompany as String! }
            self.addToCompanyView(true)
            if(self.companyArr.count == 0) {
                self.changePlaceholder()
            }
        }
    }
    
    func addToCompanyView(_ remove:Bool){
        
        let width = self.companyView.frame.size.width
        var compXPos:CGFloat = 18.0
        var compYPos:CGFloat = self.companyText.frame.size.height+12.0
        let labelHeight:CGFloat = 30.0
        
        var textDict: Dictionary<String,AnyObject> = [:]
        textDict["kfont"] = UIFont.systemFont(ofSize: 14)
        textDict["kmaxwidth"] = (self.companyView.frame.size.width-36) as AnyObject
        
        for view in self.companyView.subviews {
            var button: UIButton!
            if(view.isKind(of: UIButton.self)) {
                button = view as! UIButton
                if(button.tag != 10000) {
                    view.removeFromSuperview()
                }
            }
            else if(!view.isKind(of: UITextField.self)) {
                view.removeFromSuperview()
            }
            else if(view.isKind(of: UILabel.self) || view.isKind(of: UIImageView.self)) {
                view.removeFromSuperview()
            }
        }
        
        self.companyView.frame.size.height = 54.0
        
        var i = companyArr.count-1
        //for(var i=companyArr.count-1; i>=0; i -= 1 ) {
          for _ in (0 ..< companyArr.count) {
            let text_com: AnyObject! = companyArr[i] as AnyObject!
            if(!(text_com is NSNull)) {
                textDict["ktext"] = ("  " + companyArr[i] as String) as AnyObject
                let textFrame = TMStringUtil.textRect(forString: textDict)
                let textWidth = textFrame.size.width + 40
                
                if(compXPos + textWidth > width && i != companyArr.count-1) {
                    compXPos = 18.0
                    compYPos = compYPos + 44.0
                }
                
                let button = UIButton(type: UIButtonType.system)
                button.frame = CGRect(x: compXPos, y: compYPos, width: textWidth, height: 44)
                button.tag = i
                button.addTarget(self, action: #selector(TMWorkViewController.removeCompany(_:)), for: UIControlEvents.touchUpInside)
                
                self.companyView.addSubview(button)
                
                let label = UILabel(frame: CGRect(x: compXPos, y: compYPos+7.0, width: textWidth, height: labelHeight))
                label.text = "  "+companyArr[i]
                label.font = UIFont.systemFont(ofSize: 14)
                label.textColor = UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0)
                label.clipsToBounds = true
                label.layer.cornerRadius = 10
                label.backgroundColor = UIColor(red: 1.0, green:0.702, blue:0.725, alpha:1.0)
                self.companyView.addSubview(label)
                
                self.companyView.frame.size.height = compYPos+44.0+7.0
                
                let closeImage = UIImageView()
                let image: UIImage = UIImage(named: imageName[2])!
                closeImage.image = image
                closeImage.frame = CGRect(x: (compXPos + button.frame.size.width-20), y: (label.frame.origin.y+(label.frame.size.height-10)/2) ,width: 10, height: 10)
                self.companyView.addSubview(closeImage)
                
                compXPos = compXPos + textWidth + 12.0
            }
            
            i = i - 1
        }
        
        //self.view.bringSubviewToFront(self.companyText)
        
        self.workView.frame.size.height = self.companyView.frame.origin.y + self.companyView.frame.size.height + 14.0
        
        self.continueButton.frame.origin.y = self.workView.frame.origin.y + self.workView.frame.size.height + 15.0
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0+self.extraH)
        
    }
    
    
    func enableContinue() {
        
        let newString = designationText.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if(newString != "" && newString.characters.count>=2) {
            self.continueButton.isEnabled = true
            self.continueButton.backgroundColor = UIColor(red: 1.0, green: 0.702, blue: 0.725, alpha: 1.0)
            self.continueButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: UIControlState())
        }else {
            self.continueButton.isEnabled = false
            self.continueButton.backgroundColor = UIColor(red: 0.902, green: 0.906, blue: 0.91, alpha: 1.0)
            self.continueButton.setTitleColor(UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), for: UIControlState())
        }
        
    }
    
    private func toStingFromArray(_ list: Array<String>)-> String {
        
        var stringForServer = String()
        var prefix: String = ""
        stringForServer = stringForServer + "["
        for item in list {
            stringForServer = stringForServer + prefix
            prefix = ","
            stringForServer = stringForServer + "\""
            stringForServer = stringForServer + item.replacingOccurrences(of: "\"", with: "\'", options: NSString.CompareOptions.literal, range: nil)
            stringForServer = stringForServer + "\""
        }
        stringForServer = stringForServer + "]"
        
        return stringForServer
        
    }

    
    func onContinue(_ sender:UIButton!) {
        
        self.companyText.resignFirstResponder()
        self.onAddCompany(true as AnyObject)
        
        self.update()
    }
    
    func addLoaderForUpdate() {
        //loader
        let indicator = TMActivityIndicator()
        self.activityLoader = indicator.addActivity()
        self.scrollView.addSubview(self.activityLoader)
        self.activityLoader.frame.origin.x = self.continueButton.frame.origin.x+80
        self.activityLoader.frame.origin.y = self.continueButton.frame.origin.y+(self.continueButton.frame.size.height-self.activityLoader.frame.size.height)/2
        self.scrollView.bringSubview(toFront: self.activityLoader)
    }
    
    func update() {
        var queryString = ["login_mobile":"true"]
        queryString["param"] = "worknew"
        
        queryString["TM_Company_name_0"] = self.toStingFromArray(self.companyArr) as String
        queryString["TM_Designation_0"] = self.designationText.text!.trimmingCharacters(in: CharacterSet.whitespaces) as String!
        
        queryString["TM_Income_start"] = self.incomeStart
        queryString["TM_Income_end"] = self.incomeEnd
        
        
        var eventDict:Dictionary<String,AnyObject> = [:]
        eventDict["screenName"] = "TMWorkViewController" as AnyObject?
        eventDict["eventCategory"] = self.eventCategory as AnyObject?
        eventDict["eventAction"] = "work_server_save_call" as AnyObject?
        editProfile.trackEventDictionary = NSMutableDictionary(dictionary: eventDict)
        
        if(editProfile.isNetworkReachable()) {
            self.view.isUserInteractionEnabled = false
            self.addLoaderForUpdate()
            self.activityLoader.startAnimating()
            
            self.continueButton.backgroundColor = UIColor(red: 0.902, green: 0.906, blue: 0.91, alpha: 1.0)
            self.continueButton.setTitleColor(UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), for: UIControlState())
            
            editProfile.update(queryString as NSDictionary as [NSObject : AnyObject], with: { (response:String?, error:TMError?) -> Void in
                self.enableContinue()
                self.view.isUserInteractionEnabled = true
                self.activityLoader.stopAnimating()
                self.activityLoader.removeFromSuperview()
                if(response != nil) {
                    if(response == "save") {
                        if(self.delegate != nil) {
                            self.delegate!.didEditProfile!()
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                }else {
                    self.errorResponse(error!)
                }
            })
        }else {
            self.showAlert(TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }
    }
    
    
    func didDesignationChanged(_ sender: AnyObject) {
        let newString = designationText.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        if(newString != "") {
            self.designationText.layer.borderWidth = 1.0
            self.designationText.layer.borderColor = UIColor.black.cgColor
        }else {
            self.designationText.layer.borderWidth = 0.3
            self.designationText.layer.borderColor = UIColor(red: 0.502, green: 0.51, blue: 0.522, alpha: 1.0).cgColor
        }
        self.enableContinue()
    }
    
    func prefillForEdit() {
        
        let userUtility = TMUserUtility()
        
        // designation
        if let designation = self.registerData["designation"] as? String{
            self.designationText.text = designation
            if(designation != "Not Working") {
                self.designationText.layer.borderWidth = 1.0
                self.designationText.layer.borderColor = UIColor.black.cgColor
            }
        }else {
            self.designationText.text = "Not Working"
            self.designationText.layer.borderWidth = 0.3
            self.designationText.layer.borderColor = UIColor(red: 0.502, green: 0.51, blue: 0.522, alpha: 1.0).cgColor
        }
        
        //companies
        let companies = self.registerData["company_name"] as AnyObject
        if(!(companies is NSNull)) {
            self.companyArr = self.registerData["company_name"] as! Array
            if(self.companyArr.count > 0) {
                self.addToCompanyView(false)
            }
        }
        
        //income
        /*
        let incomeStart  = self.registerData["income_start"]
        let incomeEnd = self.registerData["income_end"]
        if((incomeStart is String) && ((incomeEnd is String)) ){
            let income_start = self.registerData["income_start"] as! String
            let income_end = self.registerData["income_end"] as! String
            let incomeIndex = income_start+"-"+income_end as String
            if(incomeIndex != "-1--1") {
                let index = userUtility.getIndexForIndustry(key: incomeIndex, from: self.allIncome)
                if(index != -1) {
                    self.updateIncome(index+1)
                }
            }
            else {
                self.updateIncome(0)
            }
            
        }
        else {
            self.updateIncome(-1)
        }*/
        
        self.enableContinue()
        
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField.text != "") {
            if(textField.tag == 1) {
                self.addCompany(textField.text!)
            }
        }
        textField.resignFirstResponder()
        scrollView.isScrollEnabled = true
        return true;
    }
    
    
    //MARK: - Keyboard Management Methods
    
    // Call this method somewhere in your view controller setup code.
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(TMWorkViewController.keyboardWillBeShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(TMWorkViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // Called when the UIKeyboardDidShowNotification is sent.
    func keyboardWillBeShown(_ sender: Notification) {
        
        self.hidePicker(true as AnyObject)
        
        let info: NSDictionary = (sender as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardSize: CGSize = value.cgRectValue.size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        if let activeTextFieldRect: CGRect? = activeTextField?.frame {
            let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
            if (!aRect.contains(activeTextFieldOrigin!)) {
                scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
            }
        }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden(_ sender: Notification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    //MARK: - UITextField Delegate Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        //scrollView.scrollEnabled = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
        //scrollView.scrollEnabled = false
    }
    
    func showAlert(_ msg: String, title: String) {
        let alert = UIAlertView(title: title , message: msg , delegate: nil, cancelButtonTitle: "OK")
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    func errorResponse(_ err: TMError) {
        if(err.errorCode == .TMERRORCODE_LOGOUT) {
            if(self.actionDelegate !=  nil) {
                self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
            }
        }else if(err.errorCode == .TMERRORCODE_NONETWORK) {
            self.showAlert(TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }else if(err.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
            self.showAlert(TM_SERVER_TIMEOUT_MSG, title: "")
        }
        else if(err.errorCode == .TMERRORCODE_DATASAVEERROR) {
            self.showAlert(err.errorMessage, title: "")
        }else {
            self.showAlert("Please Try Later", title:alertTitle)
        }
    }
    
    func changePlaceholder() {
        if(self.companyArr.count>0) {
            self.companyView.frame.origin.y = self.incomeButton.frame.origin.y+self.incomeButton.frame.size.height+10
            self.companyView.layer.borderWidth = 1.0
            self.companyView.layer.borderColor = UIColor.black.cgColor
            self.companyText.frame.origin.x = 8
            self.companyText.frame.origin.y = 8
            self.companyText.frame.size.width = self.workView.frame.size.width-32
            let placeholder = NSMutableAttributedString(string: "Add More Companies", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
            self.companyText.attributedPlaceholder = placeholder
            self.companyText.layer.borderColor = UIColor.black.cgColor
            self.companyText.layer.borderWidth = 1.0
        }else {
            self.companyView.frame.origin.y = self.incomeButton.frame.origin.y+self.incomeButton.frame.size.height+5
            self.companyText.frame.origin.x = 0
            self.companyText.frame.origin.y = 5
            self.companyText.frame.size.width = self.workView.frame.size.width-16
            self.companyView.layer.borderWidth = 0.0
            let placeholder = NSMutableAttributedString(string: "Companies (Optional)", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
            self.companyText.attributedPlaceholder = placeholder
            self.companyText.layer.borderWidth = 0.3
            self.companyText.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        }
        
        self.addIndustryButton.frame.origin.x = self.companyText.frame.origin.x+self.companyText.frame.size.width-self.addButtonWidth
        self.addIndustryButton.frame.origin.y = self.companyText.frame.origin.y
    }

}

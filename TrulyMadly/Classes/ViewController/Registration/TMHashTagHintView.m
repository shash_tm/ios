//
//  TMHashTagHintView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMHashTagHintView.h"
#import "TMUserHashTagView.h"
#import "NSString+TMAdditions.h"

#define IS_PHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_PHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568.0)

@interface TMHashTagHintView()

@property(nonatomic, strong)NSArray *hintHashTagArr;
@property(nonatomic, strong)NSArray *selectdArr;

@end

@implementation TMHashTagHintView

-(instancetype)initWithFrame:(CGRect)frame hintHashTag:(NSArray *)hintHashTag selectedHashTag:(NSArray *)selectedHashTag {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor colorWithRed:0.949 green:0.949 blue:0.949 alpha:1];
        self.layer.cornerRadius = 10;
        // drop shadow
//        [self.layer setShadowColor:[UIColor blackColor].CGColor];
//        [self.layer setShadowOpacity:0.5];
//        [self.layer setShadowRadius:2.0];
//        [self.layer setShadowOffset:CGSizeMake(2.0, 1.0)];
        
        self.hintHashTagArr = [[NSArray alloc] initWithArray:hintHashTag];
        self.selectdArr = [[NSArray alloc] initWithArray:selectedHashTag];
        [self initializeUI];
    }
    return self;
}

-(void)initializeUI{
    // label
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.frame.size.width, 25)];
    lbl.text = @"Some Suggestions";
    lbl.font = [UIFont systemFontOfSize:18];
    lbl.textColor = [UIColor blackColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lbl];
    
    CGFloat xPos = 10; CGFloat yPos = CGRectGetMaxY(lbl.frame)+20;
    UIFont *font = (IS_PHONE_4 || IS_PHONE_5) ? [UIFont systemFontOfSize:14.0] : [UIFont systemFontOfSize:15.0];
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.frame.size.width, NSUIntegerMax);
    
    CGFloat widthOffset = (IS_PHONE_4 || IS_PHONE_5) ? 25 : 20;
    
    //add scroll view
    if(IS_PHONE_4 || IS_PHONE_5) {
        yPos = 10;
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lbl.frame), self.frame.size.width, self.frame.size.height-CGRectGetMaxY(lbl.frame))];
        [self addSubview:scrollView];
        
        for (int i=0; i<self.hintHashTagArr.count; i++) {
            CGRect rect = [self.hintHashTagArr[i] boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            CGFloat width = rect.size.width + widthOffset;
            
            if(xPos+width > self.frame.size.width) {
                xPos = 10;
                yPos = yPos + 40;
            }
            
            BOOL isSelected = ([self.selectdArr containsObject:self.hintHashTagArr[i]]) ? true : false;
            
            TMUserHashTagView *hashtagView = [[TMUserHashTagView alloc] initWithFrame:CGRectMake(xPos, yPos, width, 30) withTitleText:self.hintHashTagArr[i] showCloseImage:false isSelected:isSelected];
            hashtagView.hashTagDelegate = self.delegate;
            [scrollView addSubview:hashtagView];
            
            xPos = xPos + width + 10;
        }
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, yPos+50);
        
    }else {
        for (int i=0; i<self.hintHashTagArr.count; i++) {
            CGRect rect = [self.hintHashTagArr[i] boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            CGFloat width = rect.size.width + widthOffset;
            
            if(xPos+width > self.frame.size.width) {
                xPos = 10;
                yPos = yPos + 40;
            }
            
            BOOL isSelected = ([self.selectdArr containsObject:self.hintHashTagArr[i]]) ? true : false;
            
            TMUserHashTagView *hashtagView = [[TMUserHashTagView alloc] initWithFrame:CGRectMake(xPos, yPos, width, 30) withTitleText:self.hintHashTagArr[i] showCloseImage:false isSelected:isSelected];
            hashtagView.hashTagDelegate = self.delegate;
            [self addSubview:hashtagView];
            
            xPos = xPos + width + 10;
        }
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, yPos+50);
    }
    
}

-(void)refreshList:(NSArray *)selectedHashTag{
    self.selectdArr = selectedHashTag;
    NSArray *subviews = [self subviews];
    for (int i=0; i<subviews.count; i++) {
        [subviews[i] removeFromSuperview];
    }
    [self initializeUI];
}
@end

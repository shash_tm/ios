//
//  TMBirthdayNudgeViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMBirthdayNudgeViewControllerDelegate;

@interface TMBirthdayNudgeViewController : UIViewController

@property(nonatomic,weak)id<TMBirthdayNudgeViewControllerDelegate> delegate;

@end

@protocol TMBirthdayNudgeViewControllerDelegate <NSObject>

-(void)didPressBirthdayPermissionButton;

@end


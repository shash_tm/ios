//
//  TMPagedScrollViewController.h
//  TrulyMadly
//
//  Created by Ankit on 21/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseLoginViewController.h"

@protocol PagedScrollViewControllerDelegate;

@interface TMPagedScrollViewController : TMBaseLoginViewController

@property(nonatomic,weak)id<PagedScrollViewControllerDelegate> delegate;
@property(nonatomic,assign)BOOL fromDelete;

@end


@protocol PagedScrollViewControllerDelegate <NSObject>

- (void)onClick:(NSString *)key sender:(UIViewController *)sender;

@end
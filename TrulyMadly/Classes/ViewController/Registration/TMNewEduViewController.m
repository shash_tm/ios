//
//  TMNewEduViewController.m
//  TrulyMadly
//
//  Created by Ankit on 07/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMNewEduViewController.h"
#import "TMEditProfile.h"
#import "TMActivityIndicatorView.h"
#import "TMAnalytics.h"
#import "TMError.h"
#import "TMErrorMessages.h"
#import "TMStringUtil.h"
#import "TMSwiftHeader.h"

@interface TMNewEduViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) NSDictionary *registerData;
@property (nonatomic, strong) NSArray *imageNames;
@property (nonatomic, strong) NSMutableArray *collegeArr;
@property (nonatomic, strong) NSString *degree, *cancelCollege, *alertTitle, *eventCategory;
@property (nonatomic, strong) NSArray *allDegree;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSMutableDictionary *eventDict;
@property (nonatomic, strong) TMEditProfile *editProfile;

@property (nonatomic, strong) TMActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIActivityIndicatorView *activityLoader;
@property (nonatomic, strong) UIPickerView *industryPicker;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *progressBar, *dropDownImage2;
@property (nonatomic, strong) UIButton *degreeButton, *addCollegeButton, *continueButton, *pickerCancel, *pickerDone, *backButton;
@property (nonatomic, strong) UITextField *collegeText, *activeTextField;
@property (nonatomic, strong) UIView *collegeView, *eduView, *pickerSeparator;
@property (nonatomic, strong) UIImage *addImage, *image;

@property (nonatomic, assign) TMNavigationFlow navigationFlow;
@property (nonatomic, assign) NSInteger indexForDegree;
@property (nonatomic, assign) CGRect bounds;
@property (nonatomic, assign) BOOL isDegreeSelected;
@property (nonatomic, assign) CGFloat extraH, addButtonWidth;

@end

@implementation TMNewEduViewController

- (instancetype) initWithNavigationFlow:(TMNavigationFlow) navigationFlow registerData:(NSDictionary *) registerData {
    self = [super init];
    if(self) {
        self.navigationFlow = navigationFlow;
        if(registerData != nil) {
            self.registerData = [NSDictionary dictionaryWithDictionary:registerData];
        }
    }
    return self;
}
- (void)initialize {
    self.imageNames = [NSArray arrayWithObjects:@"Dropdown", @"LinkedIn", @"remove", nil];
    self.bounds = [UIScreen mainScreen].bounds;
    self.isDegreeSelected = NO;
    self.extraH = 0.0;
    self.addButtonWidth = 0.0;
    self.alertTitle = @"Problem in saving the data";
    self.eventCategory = @"edit_profile";
    self.image = [UIImage imageNamed:@"Dropdown_black"];
}

-(TMEditProfile *)editProfile {
    if(_editProfile == nil) {
        _editProfile = [[TMEditProfile alloc] init];
    }
    return _editProfile;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialize];
    [self startTrackingEvents];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setTitle:@"Education"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];
    
    if(self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE) {
        self.extraH = 64.0;
    }
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

- (void)loadData {
    if([self.editProfile isNetworkReachable]) {
        [self loadEditData];
    }
    else {
        if([self.editProfile isCachedResponsePresent]) {
            [self loadEditData];
        }
        else {
            [self showRetryViewForError];
        }
    }
}

- (void)loadEditData {
    [self addIndicator];
    [self.activityIndicator setTitleText:@"Loading..."];
    [self.activityIndicator startAnimating];
    [self trackEvents:@"page_load"];
    
    NSDictionary *queryString = @{@"login_mobile" : @"true"};
    [self.editProfile initCall:queryString with:^(NSDictionary *response, TMError *error) {
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
        self.activityIndicator = nil;
        
        if(error == nil) {
            NSDictionary* basicDataDic = response[@"basics_data"];
            NSString* urlString = basicDataDic[@"url"];
            TMUserDataController* userDataController = [[TMUserDataController alloc] init];
            [userDataController getUserBasicDataFromURLString:urlString with:^(BOOL status, TMError * _Nullable error) {
                if(status) {
                    [userDataController getDegreeData:^(NSArray * _Nonnull degreeList) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.allDegree = degreeList;
                            self.registerData = [response objectForKey:@"user_data"];
                            [self initializeView];
                            [self prefillForEdit];
                            [self changePlaceholder];
                        });
                    }];
                }
                else {
                    [self handleError:error];
                }
            }];
        }
        else {
            [self handleError:error];
        }
    }];
}

-(void)handleError:(TMError*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(error.errorCode == TMERRORCODE_LOGOUT) {
            if(self.actionDelegate !=  nil) {
                [self.actionDelegate setNavigationFlowForLogoutAction:false];
            }
        }
        else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
            if([self.editProfile isCachedResponsePresent]) {
                if(error.errorCode == TMERRORCODE_NONETWORK) {
                    [self showRetryView];
                }
                else {
                    [self showRetryViewForError];
                }
            }
        }
        else {
            [self showRetryViewForUnknownError];
        }
    });
}
     
- (void) rightBarButton {
    
    UIImage *image = [UIImage imageNamed:@"back_button"];
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    UIBarButtonItem *backBarButtonItem;
    
    back.frame = CGRectMake(0, 0, 44, 44);
    [back setImage:image forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    self.backButton = back;
    backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

- (void)backButton:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:false];
}

- (void)initializeView {
    
    NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
    CGFloat y = 0.0, xPos, yPos;
    CGRect stringFrame, doneFrame, eduFrame;
    CGFloat stringWidth, stringHeight, naviHeight, statusBarHeight, eduHeight;
    UIImage *image2;
    UIView *leftView2, *rightView;
    
    [textDict setObject:[NSNumber numberWithFloat:CGRectGetWidth(self.bounds) - 32] forKey:@"kmaxwidth"];
    [textDict setObject:[UIFont systemFontOfSize:18.0] forKey:@"kfont"];
    [textDict setObject:@"Cancel" forKey:@"ktext"];
    self.industryPicker = [[UIPickerView alloc] init];
    xPos = self.industryPicker.frame.origin.x;
    yPos = self.bounds.size.height - self.industryPicker.frame.size.height - self.extraH;
    self.industryPicker.frame = CGRectMake(xPos, yPos, self.bounds.size.width, self.industryPicker.frame.size.height - 54);
    self.industryPicker.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.industryPicker.hidden = true;
    self.industryPicker.dataSource = self;
    self.industryPicker.delegate = self;
    [self.view addSubview:self.industryPicker];
  
    self.pickerSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, self.industryPicker.frame.origin.y-50, self.bounds.size.width, 50)];
    self.pickerSeparator.backgroundColor = [UIColor colorWithRed:0.922 green:0.922 blue:0.922 alpha:1.0];
    self.pickerSeparator.hidden = true;
    [self.view addSubview:self.pickerSeparator];
    
    stringFrame = [TMStringUtil textRectForString:textDict];
    stringWidth = stringFrame.size.width;
    stringHeight = stringFrame.size.height;
    
    self.pickerCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    self.pickerCancel.frame = CGRectMake(16.0, 0, stringWidth, 50.0);
    [self.pickerCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.pickerCancel.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [self.pickerCancel addTarget:self action:@selector(hidePicker:) forControlEvents:UIControlEventTouchUpInside];
    self.pickerCancel.hidden = true;
    [self.pickerSeparator addSubview:self.pickerCancel];
  
    [textDict removeObjectForKey:@"ktext"];
    [textDict setObject:@"Done" forKey:@"ktext"];
    doneFrame = [TMStringUtil textRectForString:textDict];
    self.pickerDone = [UIButton buttonWithType:UIButtonTypeSystem];
    self.pickerDone.frame = CGRectMake(self.bounds.size.width - doneFrame.size.width - 16, 0, doneFrame.size.width, 50);
    [self.pickerDone setTitle:@"Done" forState:UIControlStateNormal];
    [self.pickerDone.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [self.pickerDone addTarget:self action:@selector(donePicker:) forControlEvents:UIControlEventTouchUpInside];
    self.pickerDone.hidden = true;
    [self.pickerSeparator addSubview:self.pickerDone];
    
    [textDict removeObjectForKey:@"kfont"];
    [textDict setObject:[UIFont systemFontOfSize:15] forKey:@"kfont"];
  
    naviHeight = self.navigationController.navigationBar.bounds.size.height;
    statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
  
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) {
        //scroll view
        
        if(naviHeight) {
            y = naviHeight + 16.0 + statusBarHeight;
        }else {
            y = 44.0 + 16.0 + statusBarHeight;
        }
        
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, y, self.bounds.size.width, self.bounds.size.height- y);
        [self.view addSubview:self.scrollView];
    } else {
        self.extraH = 64.0;
        //scroll view
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 16.0, self.bounds.size.width, self.bounds.size.height- 16);
        [self.view addSubview:self.scrollView];
    }

    self.eduView = [[UIView alloc] initWithFrame:CGRectMake(8, 10, self.bounds.size.width - 16, 0)];
    [self.scrollView addSubview:self.eduView];
    
    // degree button
    y = self.eduView.frame.origin.y;
    self.degreeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.degreeButton.frame = CGRectMake(8, 0, self.eduView.frame.size.width - 16, 44);
    
    [self.degreeButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.degreeButton setTitle:@"Highest Degree" forState:UIControlStateNormal];
    [self.degreeButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.degreeButton addTarget:self action:@selector(onDegreeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.degreeButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
    self.degreeButton.layer.cornerRadius = 10;
    self.degreeButton.layer.borderWidth = 0.3;
    self.degreeButton.layer.borderColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0].CGColor;
    self.degreeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.degreeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self.eduView addSubview:self.degreeButton];
    eduHeight = self.eduView.frame.size.height + self.degreeButton.frame.size.height + 8.0;
  
    //drop down image
    image2 = [UIImage imageNamed:self.imageNames[0]];
    self.dropDownImage2 = [[UIImageView alloc] init];
    self.dropDownImage2.image = image2;
    self.dropDownImage2.frame = CGRectMake(self.degreeButton.frame.size.width-20, (self.degreeButton.frame.origin.y + (self.degreeButton.frame.size.height-10)/2) , 16, 10);
    [self.eduView addSubview:self.dropDownImage2];
    
    y = self.degreeButton.frame.origin.y;
    //collegeText field
    
    self.collegeView = [[UIView alloc] initWithFrame:CGRectMake(8, y+self.degreeButton.frame.size.height+5, self.eduView.frame.size.width-16, 54)];
    self.collegeView.layer.cornerRadius = 10;
    [self.eduView addSubview:self.collegeView];
    
    self.collegeText = [[UITextField alloc] initWithFrame:CGRectMake(0, 5, self.eduView.frame.size.width-16, 44)];
    [self.collegeText addTarget:self action:@selector(onEditCollege:) forControlEvents:UIControlEventEditingChanged];
    self.collegeText.tag = 2;
    self.collegeText.layer.cornerRadius = 10;
    self.collegeText.layer.borderWidth = 0.3;
    self.collegeText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.collegeText.layer.borderColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0].CGColor;
    self.collegeText.delegate = self;
    
    leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, self.collegeText.frame.size.height)];
    //leftView2.backgroundColor = [UIColor redColor];
    self.collegeText.leftView = leftView2;
    self.collegeText.leftViewMode = UITextFieldViewModeAlways;
    [self.collegeView addSubview:self.collegeText];
    
    eduFrame = self.eduView.frame;
    eduFrame.size.height = self.eduView.frame.size.height + self.collegeView.frame.size.height + 10;
    self.eduView.frame = eduFrame;
    
    y  = self.collegeText.frame.origin.y;
    //college add button
    self.addImage = [UIImage imageNamed:@"add_button"];
    [textDict removeObjectForKey:@"kfont"];
    [textDict removeObjectForKey:@"ktext"];
    [textDict setObject:[UIFont systemFontOfSize:14] forKey:@"kfont"];
    [textDict setObject:@"Add" forKey:@"ktext"];
    
    self.addButtonWidth = [TMStringUtil textRectForString:textDict].size.width + 30.0;
    rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.addButtonWidth, 44)];
    self.collegeText.rightView = rightView;
    self.collegeText.rightViewMode = UITextFieldViewModeAlways;
    
    self.addCollegeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.addCollegeButton.frame = CGRectMake(self.collegeText.frame.origin.x+self.collegeText.frame.size.width-self.addButtonWidth, self.collegeText.frame.origin.y, self.addButtonWidth, 44);
    [self.addCollegeButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    [self.addCollegeButton setTitle:@"Add" forState:UIControlStateNormal];
    [self.addCollegeButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    self.addCollegeButton.tag = 10000;
    [self.addCollegeButton addTarget:self action:@selector(onAddCollege:) forControlEvents:UIControlEventTouchUpInside];
    [self.addCollegeButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    self.addCollegeButton.hidden = true;
    [self.addCollegeButton setBackgroundImage:self.addImage forState:UIControlStateNormal];
    [self.collegeView addSubview:self.addCollegeButton];
    
    y = self.collegeText.frame.origin.y;
    
    eduFrame = self.eduView.frame;
    //self.eduView.frame.size.height = self.eduView.frame.size.height + self.collegeView.frame.size.height + 13.0
    eduFrame.size.height = self.eduView.frame.size.height + 13.0;
    self.eduView.frame = eduFrame;
    
    y = self.eduView.frame.origin.y;
    //continue button
    self.continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.continueButton.frame = CGRectMake(16, y+self.eduView.frame.size.height+12, self.bounds.size.width-32, 44);
    [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.continueButton setTitle:@"Save" forState:UIControlStateNormal];
    [self.continueButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.continueButton addTarget:self action:@selector(onContinue:) forControlEvents:UIControlEventTouchUpInside];

    [self.continueButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    self.continueButton.enabled = false;
    self.continueButton.layer.cornerRadius = 10;
    self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
    [self.scrollView addSubview:self.continueButton];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0+self.extraH);
    
    [self.view bringSubviewToFront:self.industryPicker];
    [self.view bringSubviewToFront:self.pickerCancel];
    [self.view bringSubviewToFront:self.pickerDone];
    [self.view bringSubviewToFront:self.pickerSeparator];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.allDegree count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSDictionary *degreeDict = [self.allDegree objectAtIndex:row];
    return [degreeDict objectForKey:@"value"];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.indexForDegree = row;
}

- (void)hidePicker:(id)button {
    self.industryPicker.hidden = true;
    self.pickerCancel.hidden = true;
    self.pickerDone.hidden = true;
    self.pickerSeparator.hidden = true;
}

- (void)donePicker:(UIButton *)button {
    
    [self updateDegree:self.indexForDegree];
}

- (void)updateDegree:(NSInteger) row {
    NSDictionary *degreeDict = [self.allDegree objectAtIndex:row];
    self.indexForDegree = row;
    self.dropDownImage2.image = self.image;
    self.isDegreeSelected = true;
    [self.degreeButton setTitle:[degreeDict objectForKey:@"value"] forState:UIControlStateNormal];
    [self.degreeButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    self.degreeButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.degreeButton.layer.borderWidth = 1.0;
    self.industryPicker.hidden = true;
    self.pickerCancel.hidden = true;
    self.pickerDone.hidden = true;
    self.pickerSeparator.hidden = true;
    self.degree = [degreeDict objectForKey:@"key"];
    [self enableContinue];
}
- (void)onDegreeClick:(UIButton *)button {
    [self.view endEditing:true];
    self.industryPicker.tag = 3;
    self.industryPicker.hidden = false;
    self.pickerCancel.hidden = false;
    self.pickerDone.hidden = false;
    self.pickerSeparator.hidden = false;
    [self.industryPicker reloadAllComponents];
    [self.industryPicker selectRow:self.indexForDegree inComponent:0 animated:false];
}

- (void)onAddCollege:(UIButton *)button {
    self.addCollegeButton.hidden = true;
    [self addCollege:self.collegeText.text];
    self.collegeText.text = @"";
}

- (void)onContinue:(UIButton *)button {
    
    [self.collegeText resignFirstResponder];
    [self onAddCollege:button];
    [self update];
}

- (void)addCollege:(NSString *)text {
    self.addCollegeButton.hidden = true;
    if([[self.collegeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF == %@", text]] count] > 0) {
        
    }else {
        NSString *newString = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![newString isEqualToString:@""]) {
            [self.collegeArr addObject:newString];
            [self addToCollegeView:false];
        }
        self.collegeText.text = @"";
    }
    if([self.collegeArr count] == 1) {
        [self changePlaceholder];
    }
}

- (void)update {
    
    [self trackEvents:@"education_server_save_call"];
    
    if([self.editProfile isNetworkReachable]) {
        NSMutableDictionary *queryString;
        self.view.userInteractionEnabled = false;
        self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
        [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState: UIControlStateNormal];
        
        queryString = [[NSMutableDictionary alloc] init];
        [queryString setObject:@"true" forKey:@"login_mobile"];
        [queryString setObject:@"education" forKey:@"param"];
        [queryString setObject:self.degree forKey:@"TM_Education_0"];
        [queryString setObject:[self toStringFromArray:self.collegeArr] forKey:@"TM_Institute_0"];
        
        [self addLoaderForUpdate];
        [self.activityLoader startAnimating];
        
        [self.editProfile update:queryString with:^(NSString *response, TMError *error) {
            self.view.userInteractionEnabled = true;
            [self enableContinue];
            [self.activityLoader stopAnimating];
            [self.activityLoader removeFromSuperview];
            
            if(response != nil) {
                if([response isEqualToString:@"save"]) {
                    if(self.delegate != nil) {
                        [self.delegate didEditProfile];
                    }
                    [self.navigationController popViewControllerAnimated:true];
                }
            }else {
                [self errorResponse:error];
            }
        }];
        
    }else {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG title:@""];
    }
}

- (void)showAlert:(NSString *) msg title:(NSString *) title {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
}

- (NSString *)toStringFromArray:(NSArray *) array {
    NSString *stringForServer = @"";
    NSString *prefix = @"";
    stringForServer = [stringForServer stringByAppendingString:@"["];
    for (int i=0; i< array.count; i++) {
        stringForServer = [stringForServer stringByAppendingString:prefix];
        prefix = @",";
        stringForServer = [stringForServer stringByAppendingString:@"\""];
        stringForServer = [stringForServer stringByAppendingString:[array[i] stringByReplacingOccurrencesOfString:@"\"" withString:@"\'"]];
        stringForServer = [stringForServer stringByAppendingString:@"\""];
    }
    stringForServer = [stringForServer stringByAppendingString:@"]"];
    
    return stringForServer;
}

- (void)errorResponse:(TMError *)error {
    if(error.errorCode == TMERRORCODE_LOGOUT) {
        if(self.actionDelegate !=  nil) {
            [self.actionDelegate setNavigationFlowForLogoutAction:false];
        }
    }else if(error.errorCode == TMERRORCODE_NONETWORK) {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG title:@""];
    }else if(error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showAlert:TM_SERVER_TIMEOUT_MSG title:@""];
    }
    else if(error.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self showAlert:error.errorMessage title:@""];
    }else {
        [self showAlert:@"Please Try Later" title:self.alertTitle];
    }
}
- (void)addToCollegeView:(BOOL)shouldAddCollegeView {
    
    CGFloat width, compXPos, compYPos, labelHeight;
    CGRect frame;
    NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
    
    width = self.collegeView.frame.size.width;
    compXPos = 18.0;
    compYPos = self.collegeText.frame.size.height + 14.0;
    labelHeight = 30.0;
    
    [textDict setObject:[UIFont systemFontOfSize:14] forKey:@"kfont"];
    [textDict setObject:[NSNumber numberWithFloat:self.collegeView.frame.size.width] forKey:@"kmaxwidth"];
    
    for (UIView *subview in self.collegeView.subviews)
    {
        UIButton *button;
        if([subview isKindOfClass:[UIButton class]]) {
            button = (UIButton *)subview;
            if(button.tag != 10000) {
                [subview removeFromSuperview];
            }
        }else if(![subview isKindOfClass:[UITextField class]]) {
            [subview removeFromSuperview];
        }else if([subview isKindOfClass:[UILabel class]] || [subview isKindOfClass:[UIImageView class]]) {
            [subview removeFromSuperview];
        }
    }
    
    frame = self.collegeView.frame;
    frame.size.height = 54.0;
    self.collegeView.frame = frame;
    
    NSInteger i;
    for ( i = [self.collegeArr count] - 1; i >= 0; i--) {
        NSString *text_com = [self.collegeArr objectAtIndex:i];
        if(! (text_com == (NSString *)[NSNull null]) ) {
            [textDict setObject:[self.collegeArr objectAtIndex:i] forKey:@"ktext"];
            CGRect textFrame = [TMStringUtil textRectForString:textDict];
            CGFloat textWidth = textFrame.size.width + 40;
            
            if(compXPos + textWidth > width && i != [self.collegeArr count]-1) {
                compXPos = 18.0;
                compYPos = compYPos + 44.0;
            }
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
            button.frame = CGRectMake(compXPos, compYPos, textWidth, 44);
            button.tag = i;
            [button addTarget:self action:@selector(removeCollege:) forControlEvents:UIControlEventTouchUpInside];
            //removeCollege
            [self.collegeView addSubview:button];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(compXPos, compYPos+7.0, textWidth, labelHeight)];
            label.text = [NSString stringWithFormat:@"%@%@", @" ", [self.collegeArr objectAtIndex:i]];
            label.font = [UIFont systemFontOfSize:14];
            label.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
            label.clipsToBounds = true;
            label.layer.cornerRadius = 10.0;
            label.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
            [self.collegeView addSubview:label];
            
            frame = self.collegeView.frame;
            frame.size.height = compYPos + 44.0 + 7.0;
            self.collegeView.frame = frame;
            
            UIImageView *closeImage = [[UIImageView alloc] init];
            closeImage.image = [UIImage imageNamed:[self.imageNames objectAtIndex:2]];
            closeImage.frame = CGRectMake((compXPos + button.frame.size.width-20), (label.frame.origin.y+(label.frame.size.height-10)/2) , 10, 10);
            [self.collegeView addSubview:closeImage];
            compXPos = compXPos + textWidth + 12.0;
        }
    }
  
    frame = self.eduView.frame;
    frame.size.height = self.collegeView.frame.origin.y + self.collegeView.frame.size.height + 10.0;
    self.eduView.frame = frame;
    
    frame = self.continueButton.frame;
    frame.origin.y = self.eduView.frame.origin.y + self.eduView.frame.size.height + 20.0;
    self.continueButton.frame = frame;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0+self.extraH);
    
}

- (void)removeCollege:(UIButton *) sender {
    self.cancelCollege = [self.collegeArr objectAtIndex:sender.tag];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure you want to remove this institution from the list?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [alert show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Yes"]) {
        NSArray *array = [self.collegeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@", self.cancelCollege]];
        [self.collegeArr removeObjectsInArray:array];
        [self addToCollegeView:true];
        if(self.collegeArr.count == 0) {
            [self changePlaceholder];
        }
    }
}

- (void)addLoaderForUpdate {
    TMActivityIndicator *indicator = [[TMActivityIndicator alloc] init];
    self.activityLoader = [indicator addActivity];
    [self.scrollView addSubview:self.activityLoader];
    CGRect frame;
    frame = self.activityLoader.frame;
    frame.origin.x = self.continueButton.frame.origin.x + self.continueButton.frame.size.width / 2;
    frame.origin.y = self.continueButton.frame.origin.y+(self.continueButton.frame.size.height-self.activityLoader.frame.size.height)/2;
    self.activityLoader.frame = frame;
    [self.scrollView bringSubviewToFront:self.activityLoader];
}

- (void)changePlaceholder {
    CGRect viewFrame;
    CGFloat height;
    NSMutableAttributedString *placeholder;
    NSDictionary *attributes;
    if([self.collegeArr count] >0) {
        
        viewFrame = self.collegeView.frame;
        viewFrame.origin.y = self.degreeButton.frame.origin.y+self.degreeButton.frame.size.height + 10;
        self.collegeView.frame = viewFrame;
        self.collegeView.layer.borderWidth = 1.0;
        self.collegeView.layer.borderColor = [UIColor blackColor].CGColor;
        
        height = self.collegeText.frame.size.height;
        self.collegeText.frame = CGRectMake(8, 8, self.eduView.frame.size.width-32, height);
        
        attributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0], NSFontAttributeName :[UIFont systemFontOfSize:15]};
        placeholder = [[NSMutableAttributedString alloc] initWithString:@"Add More College" attributes:attributes];
        self.collegeText.attributedPlaceholder = placeholder;
        self.collegeText.layer.borderColor = [UIColor blackColor].CGColor;
        self.collegeText.layer.borderWidth = 1.0;
    }else {
        
        viewFrame = self.collegeView.frame;
        viewFrame.origin.y = self.degreeButton.frame.origin.y+self.degreeButton.frame.size.height + 5;
        self.collegeView.frame = viewFrame;
        self.collegeView.layer.borderWidth = 0.0;
        
        height = self.collegeText.frame.size.height;
        self.collegeText.frame = CGRectMake(0, 5, self.eduView.frame.size.width-16, height);
        
        attributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0], NSFontAttributeName :[UIFont systemFontOfSize:15]};
        placeholder = [[NSMutableAttributedString alloc] initWithString:@"College (Optional)" attributes:attributes];
        self.collegeText.attributedPlaceholder = placeholder;
        self.collegeText.layer.borderWidth = 0.3;
        self.collegeText.layer.borderColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0].CGColor;
    }
    
    viewFrame = self.addCollegeButton.frame;
    viewFrame.origin.x = self.collegeText.frame.origin.x + self.collegeText.frame.size.width - self.addButtonWidth;
    viewFrame.origin.y = self.collegeText.frame.origin.y;
    self.addCollegeButton.frame = viewFrame;
}

- (void)onEditCollege:(UITextField *)sender {
    if(![self.collegeText.text isEqualToString:@""]){
        self.addCollegeButton.hidden = false;
    }else{
        self.addCollegeButton.hidden = true;
    }
}

- (void)addIndicator {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    self.activityIndicator = [[TMActivityIndicatorView alloc] init];
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.backgroundColor = [UIColor whiteColor];
    [self.view bringSubviewToFront:self.activityIndicator];
}

- (void)enableContinue {
    if(self.isDegreeSelected) {
        self.continueButton.enabled = true;
        self.continueButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
        [self.continueButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    }else {
        self.continueButton.enabled = false;
        self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
        [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    }
}

- (void)showRetryViewForError {
    if(!self.isRetryViewShown) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)showRetryViewForUnknownError {
    if(!self.isRetryViewShown) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)prefillForEdit {
    
    //let userUtility = TMUserUtility()
    
    //colleges
    NSArray *colleges = [self.registerData objectForKey:@"institute_details"];
    if(!(colleges == (NSArray *)[NSNull null])) {
        self.collegeArr = [NSMutableArray arrayWithArray:[self.registerData objectForKey:@"institute_details"]];
        [self addToCollegeView: false];
    }
    
    
    //highest degree
    NSString *highestDegree = [self.registerData objectForKey:@"highest_degree"];
    if(!(highestDegree == (NSString*)[NSNull null])) {
        NSString *degreeIndex = [self.registerData objectForKey:@"highest_degree"];
        NSInteger index = [self getIndexForIndustry:degreeIndex fromList:self.allDegree];
        if(index != -1) {
            [self updateDegree:index];
            self.isDegreeSelected = true;
        }
    }
    
    [self enableContinue];
}

-(NSInteger)getIndexForIndustry:(NSString*)inputKey fromList:(NSArray*)fromList {
    for(int i=0;i<fromList.count;i++){
        NSDictionary* degreeData = fromList[i];
        NSString* key = degreeData[@"key"];
        if([inputKey isEqualToString:key]){
            return i;
        }
    }
    return -1;
}

- (void)trackEvents:(NSString *) eventAction {
    self.eventDict = [[NSMutableDictionary alloc] init];
    [self.eventDict setObject:@"TMEduViewController" forKey:@"screenName"];
    [self.eventDict setObject:self.eventCategory forKey:@"eventCategory"];
    [self.eventDict setObject:eventAction forKey:@"eventAction"];
    self.editProfile.trackEventDictionary = self.eventDict;
}

- (void)dismissKeyboard:(UIGestureRecognizer *)gesture {
    if(self.activeTextField != nil) {
        [self.activeTextField resignFirstResponder];
    }
}

- (void)registerForKeyboardNotifications {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(keyboardWillBeShown:) name:UIKeyboardWillShowNotification object:nil];
    [notificationCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification *) notification {
    
    [self hidePicker:nil];
    NSDictionary *info = notification.userInfo;
    NSValue *value = [info valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = value.CGRectValue.size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect, activeTextFieldRect;
    CGPoint activeTextFieldOrigin;
    aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    
    activeTextFieldRect = self.activeTextField.frame;
    activeTextFieldOrigin = activeTextFieldRect.origin;
    if([self.activeTextField pointInside:activeTextFieldOrigin withEvent:nil]) {
        [self.scrollView scrollRectToVisible:activeTextFieldRect animated:true];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(![textField.text isEqualToString:@""]) {
        [self addCollege:textField.text];
    }
    [textField resignFirstResponder];
    //scrollView.scrollEnabled = true
    return true;
}

- (void)keyboardWillBeHidden:(NSNotification *) notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    //self.scrollView.scrollEnabled = true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.activeTextField = nil;
    //self.scrollView.scrollEnabled = false;
}

- (void)startTrackingEvents {
    [[TMAnalytics sharedInstance] trackView:@"TMEduViewController"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  TMBirthdayNudgeViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMBirthdayNudgeViewController.h"
#import "NSString+TMAdditions.h"
#import <QuartzCore/QuartzCore.h>

@interface TMBirthdayNudgeViewController ()

@end

@implementation TMBirthdayNudgeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    imageView.image = [UIImage imageNamed:@"birthday_nudge.jpg"];
    [self.view addSubview:imageView];
    
    UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-44, 20, 24, 24)];
    closeImage.image = [UIImage imageNamed:@"Cancel"];
    //closeImage.backgroundColor = [UIColor redColor];
    [self.view addSubview:closeImage];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    closeButton.frame = CGRectMake(self.view.bounds.size.width-54, 10, 44, 44);
    //closeButton.backgroundColor = [UIColor yellowColor];
    closeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [closeButton addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(200, 44);
    CGRect rect = [@"Try Again" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    
    UIView *actionView = [[UIView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width*0.3)/2, self.view.bounds.size.height-100, self.view.bounds.size.width*0.7, 44)];
    actionView.layer.cornerRadius = 10;
    actionView.backgroundColor = [UIColor colorWithRed:59.0f/255.0f green:86.0f/255.0f blue:156.0f/255.0f alpha:1];
    [self.view addSubview:actionView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [actionView addGestureRecognizer:tapGesture];
    
    UIImageView *fbIcon = [[UIImageView alloc] initWithFrame:CGRectMake((actionView.bounds.size.width-(21+rect.size.width))/2, 14, 16, 16)];
    fbIcon.backgroundColor = [UIColor clearColor];
    fbIcon.image = [UIImage imageNamed:@"facebook_signup"];
    [actionView addSubview:fbIcon];
    
    
    UILabel *signupTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(fbIcon.frame)+5, (CGRectGetHeight(actionView.frame) - rect.size.height)/2, rect.size.width, rect.size.height)];
    signupTextLabel.textColor = [UIColor whiteColor];
    signupTextLabel.backgroundColor = [UIColor clearColor];
    signupTextLabel.textAlignment = NSTextAlignmentCenter;
    signupTextLabel.font = [UIFont systemFontOfSize:16];
    signupTextLabel.text = @"Try Again";
    [actionView addSubview:signupTextLabel];
    
    actionView.layer.masksToBounds = NO;
    actionView.layer.shadowOffset = CGSizeMake(3, 3);
    actionView.layer.shadowRadius = 5;
    actionView.layer.shadowOpacity = 0.5;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden {
    return true;
}

-(void)closeButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)tapAction {
    [self.delegate didPressBirthdayPermissionButton];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
//
//  TMHashTagHintView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMUserHashTagView.h"

@interface TMHashTagHintView : UIView

@property(nonatomic,weak)id<TMUserHashTagViewDelegate>delegate;

-(instancetype)initWithFrame:(CGRect)frame hintHashTag:(NSArray *)hintHashTag selectedHashTag:(NSArray *)selectedHashTag;
-(void)refreshList:(NSArray *)selectedHashTag;

@end

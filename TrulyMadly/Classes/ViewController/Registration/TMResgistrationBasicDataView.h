//
//  TMResgistrationBasicDataView.h
//  TrulyMadly
//
//  Created by Ankit on 07/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@protocol TMResgistrationBasicDataViewDelegate;

@interface TMResgistrationBasicDataView : UIView

@property(nonatomic,weak)id<TMResgistrationBasicDataViewDelegate> delegate;
@property(nonatomic,strong,readonly)UITextField* workTextField;

-(instancetype)initWithFrame:(CGRect)frame step:(RegistrationStep)step
                               isLocationFilled:(BOOL)isLocationFilled;

-(void)updateHeightButtonTitle:(NSString*)heightButtonTitle;
-(void)updateWorkTextFieldText:(NSString*)workText;
-(void)updateEducationButtonTitle:(NSString*)educationButtonTitle;
-(void)updateCityButtonTitle:(NSString*)cityButtonTitle;

@end


@protocol TMResgistrationBasicDataViewDelegate <NSObject>

@optional

-(void)didButtonClick:(NSString*)text sender:(UIButton*)sender;
-(void)didOccupationChange:(UITextField*)sender;

@end

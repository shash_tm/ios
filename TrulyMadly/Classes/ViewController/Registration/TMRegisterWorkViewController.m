//
//  TMRegisterWorkViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMRegisterWorkViewController.h"
#import "TMRegisterEducationViewController.h"
#import "TMResgistrationBasicDataView.h"
#import "TMAnalytics.h"


@interface TMRegisterWorkViewController()<TMResgistrationBasicDataViewDelegate>

@property(nonatomic,strong)NSMutableDictionary *registerData;
@property(nonatomic,strong)UIButton *continueButton;
@property(nonatomic,strong)TMResgistrationBasicDataView *basicsView;
@property(nonatomic,strong)NSDate *startDate;

@end

@implementation TMRegisterWorkViewController

-(instancetype)initWithRegisterData:(NSMutableDictionary *)registerData
{
    self = [super init];
    if(self){
        self.registerData = [[NSMutableDictionary alloc] init];
        self.registerData = registerData;
        return self;
    }
    return nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.navigationController setNavigationBarHidden:false];
    self.navigationItem.hidesBackButton = true;
    self.navigationItem.title = @"Setting up your profile";
    
    // progress bar
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-60)/2, 64.0+15.0, 60, 10.7)];
    imageView.image = [UIImage imageNamed:@"Progress_2"];
    [self.view addSubview:imageView];
    
    CGRect rect = CGRectMake(20, CGRectGetMaxY(imageView.frame)+15.0, CGRectGetWidth(self.view.frame)-40, CGRectGetHeight(self.view.frame));
    self.basicsView = [[TMResgistrationBasicDataView alloc] initWithFrame:rect step:WORK isLocationFilled:false];
    self.basicsView.delegate = self;
    [self.view addSubview:self.basicsView];
    
    [self createContinueButton];
    
    NSString* designation = self.registerData[@"TM_Designation_0"];
    if(designation) {
        [self.basicsView updateWorkTextFieldText:designation];
        [self enableButtton];
    }
    
    self.startDate = [NSDate date];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
}

-(void)dismissKeyboard {
    [self.basicsView.workTextField resignFirstResponder];
}

-(void)didOccupationChange:(UITextField *)sender {
    NSString *newString = [sender.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(![newString isEqualToString:@""] && newString.length >= 2){
        self.registerData[@"TM_Designation_0"] = newString;
        [self enableButtton];
    }else {
        [self disableButton];
    }
}

-(void)createContinueButton {
    self.continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.continueButton.frame = CGRectMake(15, self.view.frame.size.height-88, self.view.bounds.size.width-30, 44);
    self.continueButton.titleLabel.font = [UIFont systemFontOfSize:15 ];
    [self.continueButton setTitle:@"Next" forState:UIControlStateNormal];
    self.continueButton.layer.cornerRadius = 6;
    [self.continueButton addTarget:self action:@selector(didContinueClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.continueButton];
    [self disableButton];
}

-(void)didContinueClick {
    [self trackRegistration:@"work"];
    TMRegisterEducationViewController *eduView = [[TMRegisterEducationViewController alloc] initWithRegisterData:self.registerData];
    eduView.delegate = self.delegate;
    eduView.actionDelegate = self.actionDelegate;
    [self.navigationController pushViewController:eduView animated:true];
}

-(void)enableButtton {
    self.continueButton.enabled = true;
    self.continueButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
    [self.continueButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
}

-(void)disableButton {
    self.continueButton.enabled = false;
    self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
    [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
}

-(void)trackRegistration:(NSString *)eventAction {
    NSDictionary *eventDict = @{@"screenName":@"TMNewRegisterBasicsViewController", @"eventCategory":@"register_basics", @"eventAction":eventAction, @"status":@"success", @"startDate":self.startDate, @"endDate":[NSDate date]};
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    [[TMAnalytics sharedInstance] trackFacebookEvent:@"registration" withParams:@{@"register_method":@"work"}];
}

@end

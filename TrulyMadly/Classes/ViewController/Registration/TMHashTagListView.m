//
//  TMHashTagListView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMHashTagListView.h"
#import "TMRegisterManager.h"
#import "TMLog.h"
#import "TMError.h"

@interface TMHashTagListView ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)UITableView *hashtagList;
@property(nonatomic,strong)NSMutableArray *contents;
@property(nonatomic,strong)TMRegisterManager *regMngr;
@property(nonatomic, strong)NSArray *badWords;
@property(nonatomic, strong)NSDictionary *hashtagDict;

@end

@implementation TMHashTagListView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.contents = [NSMutableArray arrayWithCapacity:16];
        self.hashtagList = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        self.hashtagList.dataSource = self;
        self.hashtagList.delegate = self;
        [self addSubview:self.hashtagList];
        
//        UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-0, 1)];
//        seperatorImgView.backgroundColor = [UIColor lightGrayColor];
//        [self addSubview:seperatorImgView];
        
    }
    
    return self;
}

-(void)layoutSubviews {
    self.hashtagList.frame = self.bounds;
}

-(void)emptyHashList {
    [self.contents removeAllObjects];
    [self.hashtagList reloadData];
}

-(void)removeSeparator {
    [self.hashtagList setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void)addSeparator {
    [self.hashtagList setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
}

-(void)fetchHashTag:(NSString *)text {
    self.requestInProgress = true;
    
    //TMLOG(@"fetch hash tag");
    
    NSString *toSearch = [NSString stringWithFormat:@"#%@",text];
//    NSDictionary *queryString = @{@"action" : @"search", @"text" : toSearch};
    
    if([self isBadWord:text]) {
        self.requestInProgress = false;
        [self emptyHashList];
        [self.delegate emptyTextField];
        [self showAlertWithTitle:@"Why would you call yourself that?" msg:@"" withDelegate:nil];
    }else {
        self.requestInProgress = false;
        [self.contents removeAllObjects];
        [self.contents addObjectsFromArray:[self searchInHashTagList:toSearch]];
        [self.delegate didReceiveResponse];
    }
    
    [self.hashtagList reloadData];
//    [self.regMngr searchHashTag:(NSDictionary *)queryString with:^(NSDictionary *dict, TMError *error) {
//        self.requestInProgress = false;
//        if(dict) {
//            NSArray* response = [dict valueForKey:@"hashtag"];
//            [self.contents removeAllObjects];
//            [self.contents addObjectsFromArray:response];
//            [self.hashtagList reloadData];
//            [self.delegate didReceiveResponse]
//        }else {
//            [self emptyHashList];
//            [self errorResponse:error];
//        }
//    }];

}

// fetch hashtag from locally stored hashtag list
-(NSMutableArray *)searchInHashTagList:(NSString *)toSearch {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",toSearch]; // if you need case sensitive search avoid '[c]' in the predicate
    
    NSArray* response = [[self hashtagDictionary] valueForKey:@"hashtag"];
    
    NSArray *results = [response filteredArrayUsingPredicate:predicate];
    
    NSMutableArray *finalArr = [NSMutableArray arrayWithArray:results];
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF ==[c] %@",toSearch]; // if you need case sensitive search avoid '[c]' in the predicate
    NSArray *results1 = [finalArr filteredArrayUsingPredicate:predicate1];
   // BOOL isWordAvail = [finalArr containsObject: toSearch];
    if(results1.count>0){
        [finalArr removeObject:toSearch];
        [finalArr insertObject:toSearch atIndex:0];
    }else {
        [finalArr insertObject:toSearch atIndex:0];
    }
    return finalArr;
}

-(NSDictionary *)hashtagDictionary {
    if(!self.hashtagDict) {
        NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"TMhashtag" ofType:@"json"];
        NSData *dta = [NSData dataWithContentsOfFile:file];
        NSError *error = nil;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
        self.hashtagDict = [[NSDictionary alloc] initWithDictionary:json];
    }
    return self.hashtagDict;
}

-(BOOL)isBadWord:(NSString *)text {
    //NSLog(@"text = %@",text);
    if(!self.badWords){
        NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"TMBadWordList" ofType:@"json"];
        NSData *dta = [NSData dataWithContentsOfFile:file];
        NSError *error = nil;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
        NSArray* response = [json valueForKey:@"result"];
        self.badWords = [[NSArray alloc] initWithArray:response];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF ==[c] %@",text]; // if you need case sensitive search avoid '[c]' in the predicate
    NSArray *results = [self.badWords filteredArrayUsingPredicate:predicate];
    //BOOL isWordAvail = [self.badWords containsObject: [text lowercaseString]];
    if(results.count>0){
        return true;
    }
    return false;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(self.requestInProgress) {
        return @"Searching...";
    }
    return @"";
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(self.requestInProgress) {
        return 20;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contents.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reusableIdentifier = @"123";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reusableIdentifier];
    }
    cell.textLabel.text = self.contents[indexPath.row];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];//[UIFont systemFontOfSize:14];
    
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* text = self.contents[indexPath.row];
    [self.delegate didSelectHashTagOption:text];
}

-(void)errorResponse:(TMError*)err {
    if (err.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self.delegate emptyTextField];
        [self showAlertWithTitle:err.errorMessage msg:@"" withDelegate:nil];
    }
}

#pragma mark -  Alert Handler
#pragma mark -
-(void)showAlertWithTitle:(NSString*)title msg:(NSString*)msg withDelegate:(id)delegate  {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    });
    
}

@end

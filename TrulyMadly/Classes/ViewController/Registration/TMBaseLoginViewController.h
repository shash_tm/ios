//
//  TMBaseLoginViewController.h
//  TrulyMadly
//
//  Created by Ankit on 23/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMStringUtil.h"

@interface TMBaseLoginViewController : UIViewController

@property(nonatomic,strong)NSString *identifier;

-(void)validateLoginFromEmailWithParams:(NSDictionary*)paramsDict;
-(void)validateFromFacebook;
-(void)showAlert:(NSString*)msg caller:(NSString*)caller;
-(void)addFullScreenLoader;
-(void)removeFullScreenLoader;

@end

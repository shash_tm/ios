//
//  TMRegisterWorkViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMRegisterWorkViewController : TMBaseViewController

-(instancetype)initWithRegisterData:(NSMutableDictionary *)registerData;

@end

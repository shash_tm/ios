//
//  TMHashTagListView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMHashTagListViewDelegate;

@interface TMHashTagListView : UIView

@property(nonatomic,weak)id<TMHashTagListViewDelegate> delegate;

@property(nonatomic,assign)BOOL requestInProgress;

-(void)fetchHashTag:(NSString*)text;
-(void)emptyHashList;
-(void)removeSeparator;
-(void)addSeparator;

@end

@protocol TMHashTagListViewDelegate <NSObject>

-(void)didSelectHashTagOption:(NSString*)text;
-(void)didReceiveResponse;
-(void)emptyTextField;

@end
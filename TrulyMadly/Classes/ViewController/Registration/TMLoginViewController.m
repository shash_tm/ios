//
//  TMLoginViewController.m
//  TrulyMadly
//
//  Created by Ankit on 24/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMLoginViewController.h"
//#import "TMForgotPasswordViewController.h"
#import "TMUserSession.h"
#import "TMAnalytics.h"
#import "UIColor+TMColorAdditions.h"
#import "TMStringUtil.h"
#import "TMSwiftHeader.h"


@interface TMLoginViewController ()<UITextFieldDelegate>

@property(nonatomic,assign)BOOL loginClicked;
@property(nonatomic,strong)UITextField *activeTextField;
@property(nonatomic,strong)UIImageView *backgroundImage;
@property(nonatomic,strong)UIImageView *logoImage;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIButton *fbButton;
@property(nonatomic,strong)UIImageView *fbIcon;
@property(nonatomic,strong)UITextField *emailText;
@property(nonatomic,strong)UITextField *passText;
@property(nonatomic,strong)UIView *line1;
@property(nonatomic,strong)UIView *line2;
@property(nonatomic,strong)UILabel *orText;
@property(nonatomic,strong)UIView *orView;
@property(nonatomic,strong)UIButton *loginButton;
@property(nonatomic,strong)UILabel *staticText;
@property(nonatomic,strong)UIButton *signUpButton;
@property(nonatomic,strong)UIButton *forgotPasswordButton;
@property(nonatomic,strong)UIActivityIndicatorView *activityLoader;

@end

@implementation TMLoginViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.identifier = @"login";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[TMAnalytics sharedInstance] trackView:@"TMLoginViewController"];
    
    [self.navigationController setNavigationBarHidden:TRUE];
    [self.navigationItem setHidesBackButton:TRUE];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self registerForKeyboardNotifications];
    
    [self initializeView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)prefersStatusBarHidden {
    return true;
}

-(void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)dismissKeyboard {
    if(self.activeTextField) {
        [self.activeTextField resignFirstResponder];
    }
}
-(UIImage*)getBackGroundImage {
    NSString* imageString = @"Screen_Login_bg.jpg";
    CGRect deviceBounds = [UIScreen mainScreen].bounds;
    if(deviceBounds.size.height == 480) {
        imageString = @"Screen_Login_bg_4.jpg";
    }
    
    UIImage *image = [UIImage imageNamed:imageString];
    return image;
}

-(void)initializeView {
    CGRect bounds = self.view.bounds;
    self.backgroundImage = [[UIImageView alloc] init];
    self.backgroundImage.image = [self getBackGroundImage];
    self.backgroundImage.frame = CGRectMake(-1, self.view.frame.origin.y, self.view.frame.size.width+2, self.view.frame.size.height);
    [self.view addSubview:self.backgroundImage];
    
    //scroll view
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = self.view.frame;
    [self.view addSubview:self.scrollView];
    [self.view bringSubviewToFront:self.scrollView];
    
    self.logoImage = [[UIImageView alloc] init];
    self.logoImage.contentMode = UIViewContentModeScaleAspectFit;
    CGFloat liWidth = self.view.bounds.size.width * 0.60;
    CGFloat liXPos = (self.view.bounds.size.width - liWidth)/2;
    CGFloat liYPos = 40;
    CGFloat liHeight = (liWidth * 0.60) / 2.5;
    self.logoImage.frame = CGRectMake(liXPos, liYPos, liWidth, liHeight);
    self.logoImage.image = [UIImage imageNamed:@"tm_logo"];
    [self.scrollView addSubview:self.logoImage];

    
    CGFloat y = self.logoImage.frame.origin.y;
    
    //facebook button
    self.fbButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    self.fbButton.frame = CGRectMake(16, y+self.logoImage.frame.size.height+30, bounds.size.width-32, 44);
    [self.fbButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fbButton setTitle:@"Get Started" forState:UIControlStateNormal];
    self.fbButton.titleLabel.font = [UIFont systemFontOfSize:15];
    self.fbButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.fbButton addTarget:self action:@selector(didfbLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.fbButton.layer.cornerRadius = 10;
    self.fbButton.backgroundColor = [UIColor signupButtonColor];
    [self.scrollView addSubview:self.fbButton];
    
    y = self.fbButton.frame.origin.y;
    
    //facebook icon
    NSDictionary *textDict = @{@"ktext":@"Get Started",@"kfont":[UIFont systemFontOfSize:15]};
    CGRect stringFrame = [TMStringUtil textRectForString:textDict];
    CGFloat stringWidth = stringFrame.size.width;
    self.fbIcon = [[UIImageView alloc] init];
    UIImage *fbImage = [UIImage imageNamed:@"facebook_signup"];
    self.fbIcon.image = fbImage;
    self.fbIcon.frame = CGRectMake(((self.fbButton.frame.size.width-stringWidth)/2)-10, y+(self.fbButton.frame.size.height-stringFrame.size.height)/2 ,stringFrame.size.height,stringFrame.size.height);
    [self.scrollView addSubview:self.fbIcon];
    
    // or text
    textDict = @{@"ktext":@"Or",@"kfont":[UIFont boldSystemFontOfSize:18]};
    stringFrame = [TMStringUtil textRectForString:textDict];
    stringWidth = stringFrame.size.width;
    
    self.orView = [[UIView alloc] initWithFrame:CGRectMake(0, y+self.fbButton.frame.size.height+20,bounds.size.width,stringFrame.size.height)];
    [self.scrollView addSubview:self.orView];
    
    self.orText = [[UILabel alloc] initWithFrame:CGRectMake((bounds.size.width-stringWidth)/2, 0,stringWidth,stringFrame.size.height)];
    self.orText.textAlignment = NSTextAlignmentCenter;
    self.orText.text = @"Or";
    self.orText.textColor = [UIColor whiteColor];
    self.orText.font = [UIFont boldSystemFontOfSize:18];
    [self.orView addSubview:self.orText];
    
    //line draw
    y = self.orText.frame.origin.y;
    self.line1 = [[UIView alloc] initWithFrame:CGRectMake(16,y+(stringFrame.size.height)/2,((bounds.size.width-32)/2)-stringWidth/2-6,2)];
    self.line1.backgroundColor = [UIColor whiteColor];
    [self.orView addSubview:self.line1];
    
    self.line2 = [[UIView alloc] initWithFrame:CGRectMake(self.orText.frame.origin.x+stringWidth+6,y+(stringFrame.size.height)/2,((bounds.size.width-32)/2)-self.orText.frame.size.width/2-6,2)];
    self.line2.backgroundColor = [UIColor whiteColor];
    [self.orView addSubview:self.line2];
    
    y = self.orView.frame.origin.y;
    
    //email textfield
    NSDictionary *attDict = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:15]};
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:attDict];

    self.emailText = [[UITextField alloc] initWithFrame:CGRectMake(16, y+self.orView.frame.size.height+15, bounds.size.width-32, 44)];
    self.emailText.attributedPlaceholder = placeholder;
    self.emailText.font = [UIFont systemFontOfSize:15];
    self.emailText.textColor = [UIColor whiteColor];
    self.emailText.layer.cornerRadius = 10;
    self.emailText.layer.borderWidth = 1.0;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailText.delegate = self;
    self.emailText.tag = 1;
    self.emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailText.layer.borderColor = [UIColor whiteColor].CGColor;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0,0,10,self.emailText.frame.size.height)];
    self.emailText.leftView = leftView;
    self.emailText.leftViewMode = UITextFieldViewModeAlways;
    [self.emailText addTarget:self action:@selector(enableLoginButton:) forControlEvents:UIControlEventEditingChanged];
    [self.scrollView addSubview:self.emailText];
    
    //password textfield
    y = self.emailText.frame.origin.y;
    attDict = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:15]};
    placeholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:attDict];
    
    self.passText = [[UITextField alloc] initWithFrame: CGRectMake(16, y+self.emailText.frame.size.height+10, bounds.size.width-32, 44)];
    self.passText.attributedPlaceholder = placeholder;
    self.passText.font = [UIFont systemFontOfSize:15];
    self.passText.textColor = [UIColor whiteColor];
    self.passText.secureTextEntry = true;
    self.passText.delegate = self;
    self.passText.tag = 2;
    [self.passText addTarget:self action:@selector(enableLoginButton:) forControlEvents:UIControlEventEditingChanged];
    self.passText.layer.cornerRadius = 10;
    self.passText.layer.borderWidth = 1.0;
    self.passText.layer.borderColor = [UIColor whiteColor].CGColor;
    leftView = [[UIView alloc] initWithFrame:CGRectMake(0,0,10,self.passText.frame.size.height)];
    self.passText.leftView = leftView;
    self.passText.leftViewMode = UITextFieldViewModeAlways;
    self.passText.returnKeyType = UIReturnKeyGo;
    [self.scrollView addSubview:self.passText];
    
    // login button
    y = self.passText.frame.origin.y;
    self.loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.loginButton.frame = CGRectMake(16, y+self.passText.frame.size.height+10, bounds.size.width-32, 44);
    //self.loginButton.setTitleColor(UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), forState: UIControlState.Normal)
    [self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.loginButton addTarget:self action:@selector(didLoginButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.loginButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.loginButton.layer.cornerRadius = 10;
    self.loginButton.layer.borderWidth = 1.0;
    self.loginButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.loginButton.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.loginButton];
    
    y = self.loginButton.frame.origin.y;
    textDict = @{@"ktext":@"Don't have an account?",@"kfont":[UIFont systemFontOfSize:16]};
    stringFrame = [TMStringUtil textRectForString:textDict];
    stringWidth = stringFrame.size.width;
    CGFloat stringHeight = stringFrame.size.height;
    self.staticText = [[UILabel alloc] initWithFrame:CGRectMake(40, y+self.loginButton.frame.size.height+20, stringWidth, stringHeight)];
    self.staticText.textAlignment = NSTextAlignmentCenter;
    self.staticText.text = @"Don't have an account?";
    self.staticText.textColor = [UIColor whiteColor];
    self.staticText.font = [UIFont systemFontOfSize:16];
    self.staticText.numberOfLines = 0;
    [self.staticText sizeToFit];
    [self.scrollView addSubview:self.staticText];
    
    attDict = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    NSMutableAttributedString *optional = [[NSMutableAttributedString alloc] initWithString:@"Sign Up" attributes:attDict];
    attDict = @{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle]};
    [optional addAttributes:attDict range:NSMakeRange(0, 7)];
   
    textDict = @{@"ktext":@"Sign Up",@"kfont":[UIFont systemFontOfSize:16]};
    stringFrame = [TMStringUtil textRectForString:textDict];
    stringWidth = stringFrame.size.width;
    
    self.signUpButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.signUpButton addTarget:self action:@selector(didsignUpButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.signUpButton setAttributedTitle:optional forState:UIControlStateNormal];
    self.signUpButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    self.signUpButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.scrollView addSubview:self.signUpButton];
    
    CGFloat stXPos = (CGRectGetWidth(bounds)-(self.staticText.frame.size.width+stringWidth+5))/2;
    self.staticText.frame = CGRectMake(stXPos,
                                       self.staticText.frame.origin.y,
                                       self.staticText.frame.size.width,
                                       self.staticText.frame.size.height);
    CGFloat subXPos =  self.staticText.frame.origin.x+self.staticText.frame.size.width+5;
    self.signUpButton.frame = CGRectMake(subXPos,
                                         y+self.loginButton.frame.size.height+7,
                                         stringWidth,
                                         44);
    ////////
    attDict = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    optional = [[NSMutableAttributedString alloc] initWithString:@"Forgot your password?" attributes:attDict];
    attDict = @{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle]};
    [optional addAttributes:attDict range:NSMakeRange(0, 21)];
    
    y = self.signUpButton.frame.origin.y;
    
    textDict = @{@"ktext":@"Forgot your password?",@"kfont":[UIFont boldSystemFontOfSize:16]};
    stringFrame = [TMStringUtil textRectForString:textDict];
    stringWidth = stringFrame.size.width;
    
    self.forgotPasswordButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.forgotPasswordButton.frame = CGRectMake((bounds.size.width-stringWidth)/2,
                                                 y+self.signUpButton.frame.size.height+10,
                                                 stringWidth,
                                                 44);
    [self.forgotPasswordButton addTarget:self action:@selector(forgotPasswordClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.forgotPasswordButton setAttributedTitle:optional forState:UIControlStateNormal];
    [self.forgotPasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.forgotPasswordButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    self.forgotPasswordButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.scrollView addSubview:self.forgotPasswordButton];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.forgotPasswordButton.frame.origin.y+self.forgotPasswordButton.frame.size.height+15.0);
}

-(void)addLoader {
    self.activityLoader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityLoader.color = [UIColor grayColor];
    [self.scrollView addSubview:self.activityLoader];

    CGFloat alXPos = self.loginButton.frame.origin.x+80;
    CGFloat alYPos = self.loginButton.frame.origin.y+(self.loginButton.frame.size.height-self.activityLoader.frame.size.height)/2;
    CGFloat alWidth = self.activityLoader.frame.size.width;
    CGFloat alHeight = self.activityLoader.frame.size.height;
    self.activityLoader.frame = CGRectMake(alXPos, alYPos, alWidth, alHeight);

    [self.scrollView bringSubviewToFront:self.activityLoader];
    [self.activityLoader startAnimating];
}
-(void)removeLoader {
    if(self.loginClicked) {
        self.loginClicked = false;
        [self.activityLoader stopAnimating];
        [self.activityLoader removeFromSuperview];
    }
}

-(void)didLoginButtonClick:(id)sender {
    if([self.emailText.text isEqualToString:@""] || [self.passText.text isEqualToString:@""]) {
        [self showAlert:@"Email or password not entered" caller:@""];
    }
    else {
        self.loginClicked = true;
        [self.passText resignFirstResponder];
        
        NSDictionary *params = @{@"login":@"1",@"email":self.emailText.text,
                                 @"password":self.passText.text,@"login_mobile":@"true"};
        [self validateLoginFromEmailWithParams:params];
    }
}

-(void)didfbLoginClicked:(UIButton*)sender {
    
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    
    //adding tracking for the fb button click
    [eventDict setObject:@"TMPagedScrollViewController" forKey:@"screenName"];
    [eventDict setObject:@"login" forKey:@"eventCategory"];
    [eventDict setObject:@"fb_click" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
    [self validateFromFacebook];
}
-(void)enableLoginButton:(UITextField*)sender {
    if(![sender.text isEqualToString:@""]) {
        sender.layer.borderWidth = 2.0;
    }
    else {
        sender.layer.borderWidth = 1.0;
    }
}
-(void)didsignUpButtonClick:(UIButton*)sender {
    if (self.delegate != nil) {
        [self.delegate delegateForLogin:@"signup" sender:self];
    }
}

-(void)forgotPasswordClick:(UIButton*)sender {
    TMForgotPasswordViewController *forgot = [[TMForgotPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgot animated:FALSE];
}

#pragma mark - Override Login Hanlers

-(void)handleSuccessResponse {
    TMUserStatus userStatus = [TMUserSession sharedInstance].user.userStatus;
    if(userStatus == AUTHENTIC || userStatus == NON_AUTHENTIC){
        // move to matches
        if (self.delegate != nil) {
            [self.delegate delegateForLogin:@"matches" sender:self];
        }
    }
    else if(userStatus == INCOMPLETE){
        if (self.delegate != nil) {
            [self.delegate delegateForLogin:@"registerBasics" sender:self];
        }
    }
    else if(userStatus == SUSPENDED){
        [self showAlert:@"Your profile is currently deactivated. Do you want to activate your profile." caller:@"suspended"];
    }
    else if(userStatus == BLOCKED){
        [self showAlert:@"Your profile is blocked. Please contact Trulymadly Customer Care." caller:@"blocked"];
    }
}
-(void)handleActivateUserSuccessResponse {
    if (self.delegate != nil) {
        [self.delegate delegateForLogin:@"matches" sender:self];
    }
}
-(void)handleLogoutResponse {
    [self.delegate delegateForLogin:@"logout" sender:self];
}

-(void)configureLoaderForLoginValidationRequestStart {
    if(self.loginClicked) {
        [self.loginButton setTitle:@"Logging In" forState:UIControlStateNormal];
        self.view.userInteractionEnabled = FALSE;
        [self addLoader];
    }
    else {
        [self addFullScreenLoader];
    }
}

-(void)configureLoaderForLoginValidationRequestFinish {
    if(self.loginClicked) {
        [self removeLoader];
        [self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
    }
    else {
        [self removeFullScreenLoader];
    }
}

-(NSMutableDictionary*)eventDictionary {
    NSMutableDictionary *eventDict = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDict[@"screenName"] = @"TMLoginViewController";
    eventDict[@"eventCategory"] = @"login";
    if(self.loginClicked) {
        eventDict[@"eventAction"] = @"login_via_email";
    }
    else {
        eventDict[@"eventAction"] = @"fb_server_call";
    }
    return eventDict;
}

#pragma mark - Keyboard Handlers

 //Called when the UIKeyboardDidShowNotification is sent.
-(void)keyboardWillBeShown:(NSNotification*)sender {
    NSDictionary *info = sender.userInfo;
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if(self.activeTextField) {
        CGPoint activeTextFieldOrigin = self.activeTextField.frame.origin;
        if(!CGRectContainsPoint(aRect, activeTextFieldOrigin)) {
            [self.scrollView scrollRectToVisible:self.activeTextField.frame animated:true];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
-(void)keyboardWillBeHidden:(NSNotification*)sender {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField*)textField {
    self.activeTextField = textField;
    self.scrollView.scrollEnabled = true;
}

-(void)textFieldDidEndEditing:(UITextField*)textField {
    self.activeTextField = nil;
    self.scrollView.scrollEnabled = false;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField  {
    if(textField == self.emailText) {
        [self.passText becomeFirstResponder];
    }
    else if(textField == self.passText) {
        [self didLoginButtonClick:nil];
        [textField resignFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return true;
}


- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

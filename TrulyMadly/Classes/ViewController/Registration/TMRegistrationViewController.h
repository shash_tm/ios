//
//  TMRegistrationViewController.h
//  TrulyMadly
//
//  Created by Ankit on 07/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"
#import "TMEnums.h"

@interface TMRegistrationViewController : TMBaseViewController

- (instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow;


@end

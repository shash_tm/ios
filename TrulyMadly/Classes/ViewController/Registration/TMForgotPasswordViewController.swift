//
//  TMForgotPasswordViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 10/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

class TMForgotPasswordViewController: TMBaseViewController, UITextFieldDelegate {
    
    var activeTextField: UITextField!
    
    var bounds = UIScreen.main.bounds as CGRect
    
    let loginMgr = TMUserDataManager()
    
    var backgroundImage: UIImageView!
    var logoImage: UIImageView!
    var scrollView:UIScrollView!
    var staticText: UILabel!
    var staticText1: UILabel!
    var emailText: UITextField!
    
    var resetButton: UIButton!
    
    var loginButton: UIButton!
    
    var logo: UIImage = UIImage(named: "tm_logo")!
    
    var activityLoader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        TMAnalytics.sharedInstance().trackView("TMForgotPasswordViewController")
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TMForgotPasswordViewController.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tap)
        
        self.initializeView()
    }
    
    func dismissKeyboard(_ sender: AnyObject) {
        if(self.activeTextField != nil) {
            self.activeTextField.resignFirstResponder()
        }
    }
    
    func addLoader() {   // default loader with login button
        self.activityLoader = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        self.activityLoader.color = UIColor(red: 0.502 , green: 0.51 , blue: 0.522 , alpha: 1.0)
        self.scrollView.addSubview(self.activityLoader)
        self.activityLoader.frame.origin.x = self.resetButton.frame.origin.x+60
        self.activityLoader.frame.origin.y = self.resetButton.frame.origin.y+(self.resetButton.frame.size.height-self.activityLoader.frame.size.height)/2
        self.scrollView.bringSubview(toFront: self.activityLoader)
        self.activityLoader.startAnimating()
    }
    
    func removeLoader() {
        self.activityLoader.stopAnimating()
        self.activityLoader.removeFromSuperview()
    }
    
    func getBackGroundImage() -> UIImage {
        var imageString = "Screen_Login_bg.jpg"
        let deviceBounds = UIScreen.main.bounds
        if(deviceBounds.size.height == 480) {
            imageString = "Screen_Login_bg_4.jpg"
        }
        
        let image:UIImage? = UIImage(named: imageString) as UIImage?
        return image!
    }
    
    func initializeView() {
        var textDict: Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 16)
        
        self.backgroundImage = UIImageView()
        self.backgroundImage.image = self.getBackGroundImage()
        self.backgroundImage.frame = CGRect(x: -1,
                                                y: view.frame.origin.y,
                                                width: view.frame.size.width+2,
                                                height: view.frame.size.height)
        self.view.addSubview(self.backgroundImage)
        
        //scroll view
        self.scrollView = UIScrollView()
        self.scrollView.frame = view.frame
        self.view.addSubview(self.scrollView)
        self.view.bringSubview(toFront: self.scrollView)
        
        let labelBackground: UIView = UIView(frame: self.view.frame)
        self.view.frame = self.view.bounds;
        
        let gradLayer: CAGradientLayer = CAGradientLayer()
        gradLayer.frame = labelBackground.layer.bounds
        gradLayer.colors = [UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.15).cgColor, UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha:0.0).cgColor]
        labelBackground.layer.addSublayer(gradLayer)
        labelBackground.addSubview(self.scrollView)
        self.view.addSubview(labelBackground)
        
        self.logoImage = UIImageView()
        self.logoImage.contentMode = UIViewContentMode.scaleAspectFit;
        self.logoImage.frame.size.width = bounds.width * 0.60
        self.logoImage.frame.origin.x = (bounds.width-self.logoImage.frame.size.width)/2
        self.logoImage.frame.origin.y = 40
        self.logoImage.frame.size.height = (self.logoImage.frame.size.width * 0.60) / 2.5
        self.logoImage.image = logo
        self.scrollView.addSubview(self.logoImage)
        
        var y = self.logoImage.frame.origin.y
        
        textDict["ktext"] = "Enter your email address provided by you during registration. Instructions will be sent to this email address." as AnyObject?
        var stringFrame = TMStringUtil.textRect(forString: textDict)
        var stringWidth = stringFrame.size.width
        var stringHeight = stringFrame.size.height
        
        self.staticText = UILabel(frame: CGRect(x: 16, y: y+self.logoImage.frame.size.height+40, width: stringWidth, height: stringHeight))
        self.staticText.textAlignment = NSTextAlignment.left
        self.staticText.text = textDict["ktext"] as? String
        self.staticText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
        self.staticText.font = UIFont.systemFont(ofSize: 16);
        self.staticText.numberOfLines = 0
        self.staticText.sizeToFit()
        self.scrollView.addSubview(self.staticText)
    
        y = self.staticText.frame.origin.y
        //email textfield
        let placeholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
        self.emailText = UITextField(frame: CGRect(x: 16, y: y+self.staticText.frame.size.height+15, width: bounds.size.width-32, height: 44))
        self.emailText.attributedPlaceholder = placeholder
        self.emailText.font = UIFont.systemFont(ofSize: 15)
        self.emailText.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.emailText.layer.cornerRadius = 10
        self.emailText.layer.borderWidth = 1.0
        self.emailText.keyboardType = .emailAddress
        self.emailText.delegate = self
        self.emailText.tag = 1
        self.emailText.autocapitalizationType = .none
        self.emailText.autocorrectionType = .no
        self.emailText.layer.borderColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0).cgColor
        let leftView = UIView(frame: CGRect(x: 0,y: 0,width: 10,height: self.emailText.frame.size.height))
        self.emailText.leftView = leftView
        self.emailText.leftViewMode = .always
        self.emailText.returnKeyType = .go
        self.emailText.addTarget(self, action: #selector(TMForgotPasswordViewController.enableText(_:)),for:UIControlEvents.editingChanged)
        self.scrollView.addSubview(self.emailText)
        
        y = self.emailText.frame.origin.y
        
        // reset button
        self.resetButton = UIButton(type: UIButtonType.system)
        self.resetButton.frame = CGRect(x: 16, y: y+self.emailText.frame.size.height+10, width: bounds.size.width-32, height: 44)
        self.resetButton.setTitleColor(UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0), for: UIControlState())
        self.resetButton.setTitle("Reset Password", for: UIControlState())
        self.resetButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.resetButton.addTarget(self, action: #selector(TMForgotPasswordViewController.didresetButtonClick(_:)), for: UIControlEvents.touchUpInside)
        self.resetButton.titleLabel?.textAlignment = .center
        self.resetButton.layer.cornerRadius = 10
        self.resetButton.layer.borderWidth = 1.0
        self.resetButton.layer.borderColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0).cgColor
        self.resetButton.backgroundColor = UIColor.white
        self.scrollView.addSubview(self.resetButton)
        
        y = self.resetButton.frame.origin.y
        
        textDict["kfont"] = UIFont.boldSystemFont(ofSize: 16)
        textDict["ktext"] = "Back to SignIn" as AnyObject?
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        let optional = NSMutableAttributedString(string: "Back to SignIn", attributes: [NSForegroundColorAttributeName : UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0), NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)])
        optional.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location:0,length:14))
        
        self.loginButton = UIButton(type: UIButtonType.system)
        self.loginButton.frame = CGRect(x: (self.bounds.size.width-stringWidth)/2, y: y+self.resetButton.frame.size.height+7, width: stringWidth, height: 44)
        self.loginButton.setTitleColor(UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0), for: UIControlState())
        self.loginButton.setAttributedTitle(optional, for: UIControlState())
        self.loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.loginButton.addTarget(self, action: #selector(TMForgotPasswordViewController.didsignInButtonClick(_:)), for: UIControlEvents.touchUpInside)
        self.loginButton.titleLabel?.textAlignment = .left
        self.scrollView.addSubview(self.loginButton)
        
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width,height:self.loginButton.frame.origin.y+self.loginButton.frame.size.height+15.0)
        
    }

    func didsignInButtonClick(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override public var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    func enableText(_ sender: UITextField) {
        if(sender.text != "") {
            sender.layer.borderWidth = 2.0
        }
        else {
            sender.layer.borderWidth = 1.0
        }
    }
    
    func emailCheck() -> Bool {
        let newString = self.emailText.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        self.emailText.text = newString
        let flag = newString.isValidEmail()
        return flag
        
    }
    
    func didresetButtonClick(_ sender: AnyObject) {
        if(!self.emailCheck()) {
            self.showAlert("Enter the valid email address", title: "")
        }
        else {
            
            var eventDict:Dictionary<String,AnyObject> = [:]
            eventDict["screenName"] = "TMForgotPasswordViewController" as AnyObject?
            eventDict["eventCategory"] = "login" as AnyObject?
            eventDict["eventAction"] = "forgot_password" as AnyObject?
            loginMgr.trackEventDictionary = NSMutableDictionary(dictionary: eventDict)
            
            if(loginMgr.isNetworkReachable()) {
                
                self.view.isUserInteractionEnabled = false
                self.addLoader()
                
                var loginDict:Dictionary<String,AnyObject> = [:]
                loginDict["email"] = self.emailText.text as AnyObject? //as String
                
                loginMgr.forgotPassword(loginDict, with: { (status: Bool, err: TMError?) -> Void in
                    self.view.isUserInteractionEnabled =  true
                    self.removeLoader()
                    
                    if(err == nil) {
                        if(status) {
                            self.emailText.text = ""
                            self.showAlert("Reset password instructions have been sent to your email address that is registered with TrulyMadly.", title: "")
                        }
                        else {
                            self.errorResponse(err!)
                        }
                    }
                    else {
                        self.errorResponse(err!)
                    }
                })
            }else {
                self.showAlert(TM_INTERNET_NOTAVAILABLE_MSG, title: "")
            }

        }
    }
    
    
    func errorResponse(_ err: TMError) {
        if(err.errorCode == .TMERRORCODE_NONETWORK) {
            self.showAlert(TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }else if(err.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
            self.showAlert(TM_SERVER_TIMEOUT_MSG, title: "")
        }
        else if(err.errorCode == .TMERRORCODE_DATASAVEERROR) {
            self.showAlert(err.errorMessage as String, title: "")
        }else {
            self.showAlert(TM_UNKNOWN_MSG, title: "")
        }
    }
    
    func showAlert(_ msg: String, title: String) {
        
        let alert = UIAlertView(title:title , message:msg , delegate: self, cancelButtonTitle: "Ok")
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.didresetButtonClick(true as AnyObject)
        textField.resignFirstResponder()
        return true;
    }
    
    
    //MARK: - Keyboard Management Methods
    
    // Call this method somewhere in your view controller setup code.
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(TMForgotPasswordViewController.keyboardWillBeShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(TMForgotPasswordViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // Called when the UIKeyboardDidShowNotification is sent.
    func keyboardWillBeShown(_ sender: Notification) {
        let info: NSDictionary = (sender as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardSize: CGSize = value.cgRectValue.size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        if let activeTextFieldRect: CGRect? = activeTextField?.frame {
            let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
            if (!aRect.contains(activeTextFieldOrigin!)) {
                scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
            }
        }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden(_ sender: Notification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    //MARK: - UITextField Delegate Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        // scrollView.scrollEnabled = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
        // scrollView.scrollEnabled = false
    }

    
}

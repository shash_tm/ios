//
//  TMHashTagTextViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMHashTagTextViewController : TMBaseViewController

-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow registerData:(NSMutableDictionary *)registerData showBar:(BOOL)showBar;

@end

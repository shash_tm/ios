//
//  TMPagedScrollViewController.m
//  TrulyMadly
//
//  Created by Ankit on 21/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPagedScrollViewController.h"
#import "UIColor+TMColorAdditions.h"
#include "TMUserSession.h"
#import "TMEnums.h"
#import "NSString+TMAdditions.h"
#import "TMAnalytics.h"
#import "TMHomeScreenInformationPopupViewController.h"

#define PAGECONTROL_OFFSETFROMBOTTOM   126;

#define LOGIN_BUTTON_TITLE @"Member Log In"

#pragma mark - TMSignupActionView
#pragma mark-

@protocol TMSignupActionViewDelegate <NSObject>

-(void)didPressLoginButton;
-(void)didPressSignupByFacebookButton;
-(void)didPressInfoButton;

@end

@interface TMSignupActionView : UIView

@property(nonatomic,weak)id<TMSignupActionViewDelegate> delegate;
@property(nonatomic,strong)UIView *actionView;
@property(nonatomic,strong)UIButton *infoLabelButton;
@property(nonatomic,strong)UILabel *signupTextLabel;
@property(nonatomic,strong)UIImageView *fbIcon;
@property(nonatomic,strong)UIButton *loginButton;
@property(nonatomic,strong)UIButton *infoButton;
@property(nonatomic,strong)UIImageView* infoButtonImageView;
@property(nonatomic,strong)UIView* infoContainerView;

@end

@implementation TMSignupActionView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.actionView = [[UIView alloc] initWithFrame:CGRectZero];
        self.actionView.layer.cornerRadius = 10;
        self.actionView.backgroundColor = [UIColor signupButtonColor];
        [self addSubview:self.actionView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self.actionView addGestureRecognizer:tapGesture];
        
        self.fbIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.fbIcon.backgroundColor = [UIColor clearColor];
        self.fbIcon.image = [UIImage imageNamed:@"facebook_signup"];
        [self.actionView addSubview:self.fbIcon];
        
        self.infoLabelButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.infoLabelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.infoLabelButton addTarget:self.delegate action:@selector(didPressInfoButton) forControlEvents:UIControlEventTouchUpInside];
        self.infoLabelButton.backgroundColor = [UIColor clearColor];
        [self.infoLabelButton titleLabel].textAlignment = NSTextAlignmentCenter;
        [self.infoLabelButton titleLabel].font = [UIFont systemFontOfSize:15];
        [self.infoLabelButton setTitle:@"Trust us, we'll never post on your wall" forState:UIControlStateNormal];
        self.infoLabelButton.layer.shadowColor = [UIColor blackColor].CGColor;
        [self addSubview:self.infoLabelButton];
        
        //the info button
        self.infoButton = [[UIButton alloc] initWithFrame:CGRectMake(self.infoLabelButton.frame.origin.x, self.infoLabelButton.frame.origin.y, 45, 45)];
        [self.infoButton addTarget:self.delegate action:@selector(didPressInfoButton) forControlEvents:UIControlEventTouchUpInside];
        
        self.infoButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 15.5, 14, 14)];
        self.infoButtonImageView.image = [UIImage imageNamed:@"home_screen_more_info_white"];
        [self.infoButton addSubview:self.infoButtonImageView];
        [self addSubview:self.infoButton];
        
        
        self.signupTextLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.signupTextLabel.textColor = [UIColor whiteColor];
        self.signupTextLabel.backgroundColor = [UIColor clearColor];
        self.signupTextLabel.textAlignment = NSTextAlignmentCenter;
        self.signupTextLabel.font = [UIFont systemFontOfSize:16];
        self.signupTextLabel.text = @"Get Started";
        [self.actionView addSubview:self.signupTextLabel];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
        CGSize constrintSize = CGSizeMake(200, 44);
        CGRect rect = [self.signupTextLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        self.signupTextLabel.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
        
        NSString *text = LOGIN_BUTTON_TITLE;
        NSMutableAttributedString *buttonText = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:16]}];
        
        [buttonText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:24.0/255.0 green:27.0/255.0 blue:169.0/255.0 alpha:1.0] range:NSMakeRange(0, [text length])];
        self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.loginButton setAttributedTitle:buttonText forState:UIControlStateNormal];
        self.loginButton.backgroundColor = [UIColor clearColor];
        [self.loginButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.loginButton];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat avWidth = CGRectGetWidth(self.bounds) * 0.70;
    CGFloat avXPos = (CGRectGetWidth(self.bounds) - avWidth)/2;
    self.actionView.frame = CGRectMake(avXPos, 10, avWidth, 44);
    
    CGFloat ilYPos = CGRectGetMaxY(self.actionView.frame)+2;
    
    NSDictionary* attributeDictionary = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    
    CGFloat labelWidth = [self.infoLabelButton.currentTitle boundingRectWithConstraintSize:self.bounds.size attributeDictionary:attributeDictionary].size.width;
    
    self.infoLabelButton.frame = CGRectMake(CGRectGetWidth(self.bounds)/2 - labelWidth/2, ilYPos, labelWidth, 16);
    
    self.infoButton.center = CGPointMake(self.infoLabelButton.center.x + self.infoLabelButton.bounds.size.width/2 + self.infoButton.bounds.size.width/2 + 4, self.infoLabelButton.center.y);
    
    ////////
    self.fbIcon.frame = CGRectMake(34, 14, 16, 16);
    
    CGFloat totalWidth = (CGRectGetWidth(self.fbIcon.bounds) + 5 +CGRectGetWidth(self.signupTextLabel.bounds));
    CGFloat xPos = (CGRectGetWidth(self.actionView.bounds) - totalWidth)/2;
    
    self.fbIcon.frame = CGRectMake(xPos,
                                   self.fbIcon.frame.origin.y,
                                   self.fbIcon.frame.size.width,
                                   self.fbIcon.frame.size.height);
    self.signupTextLabel.frame = CGRectMake(CGRectGetMaxX(self.fbIcon.frame)+5,
                                            (CGRectGetHeight(self.actionView.frame) - CGRectGetHeight(self.signupTextLabel.frame))/2,
                                            self.signupTextLabel.frame.size.width,
                                            self.signupTextLabel.frame.size.height);
    
    CGFloat lbYPos = CGRectGetMaxY(self.infoLabelButton.frame) - (CGRectGetHeight(self.infoLabelButton.frame)/2) + 3;
    
    attributeDictionary = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]};
    
    CGFloat lbWidth = [LOGIN_BUTTON_TITLE boundingRectWithConstraintSize:self.frame.size attributeDictionary:attributeDictionary].size.width;
    self.loginButton.frame = CGRectMake(CGRectGetWidth(self.bounds)/2 - lbWidth/2, lbYPos, lbWidth, 44);
}

-(void)tapAction {
    [self.delegate didPressSignupByFacebookButton];
}

-(void)loginAction {
    [self.delegate didPressLoginButton];
}

@end


#pragma mark - TMPageView
#pragma mark-

@interface TMPageView : UIView

@property(nonatomic,strong)UIImageView *bgImageView;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)CGFloat subTextBaseYPos;

-(instancetype)initWithFrame:(CGRect)frame withIndex:(NSInteger)index;

@end

@implementation TMPageView

-(instancetype)initWithFrame:(CGRect)frame withIndex:(NSInteger)index {
    self = [super initWithFrame:frame];
    if(self) {
        self.index = index;
        
        self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.bgImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.bgImageView];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.bgImageView.frame = self.bounds;
}

@end


#pragma mark - TMPagedScrollViewController
#pragma mark-

@interface TMPagedScrollViewController ()<UIScrollViewDelegate,TMSignupActionViewDelegate>

@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIPageControl *pageControl;
@property(nonatomic,strong)TMSignupActionView *signUpActionView;
@property(nonatomic,strong)NSArray *images;
@property(nonatomic,assign)BOOL isScroll;

@end

@implementation TMPagedScrollViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.identifier = @"signup";
        [self initImages];
        self.isScroll = FALSE;
    }
    return self;
}
-(void)initImages {
    CGRect screenRect = [UIScreen mainScreen].bounds;
    
    if(CGRectGetHeight(screenRect) == 480) {
        self.images = @[[UIImage imageNamed:@"Splash1_4.jpg"],[UIImage imageNamed:@"Splash2_4.jpg"],[UIImage imageNamed:@"Splash3_4.jpg"],[UIImage imageNamed:@"Splash4_4.jpg"]];
    }
    else {
        self.images = @[[UIImage imageNamed:@"Splash1.jpg"],[UIImage imageNamed:@"Splash2.jpg"],[UIImage imageNamed:@"Splash3.jpg"],[UIImage imageNamed:@"Splash4.jpg"]];
    }
}

#pragma mark - view lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    self.scrollView.pagingEnabled = TRUE;
    self.scrollView.bounces = FALSE;
    self.scrollView.showsHorizontalScrollIndicator = FALSE;
    self.scrollView.showsVerticalScrollIndicator = FALSE;
    self.scrollView.contentOffset = CGPointMake(0, 0);
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollView];
    
    self.pageControl = [[UIPageControl alloc] init];
    self.pageControl.pageIndicatorTintColor = [UIColor grayColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.pageControl.defersCurrentPageDisplay = FALSE;
    self.pageControl.numberOfPages = self.images.count;
    self.pageControl.currentPage = 0;
    [self.view addSubview:self.pageControl];
    
    self.signUpActionView = [[TMSignupActionView alloc] initWithFrame:CGRectZero];
    self.signUpActionView.backgroundColor = [UIColor clearColor];
    self.signUpActionView.delegate = self;
    [self.signUpActionView.infoButtonImageView setImage:[UIImage imageNamed:@"home_screen_more_info_black"]];
    [self.signUpActionView.infoLabelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:self.signUpActionView];
    
    for (int i=0; i<self.images.count; i++) {
        [self addSubViewAtIndex:i];
    }
    
    if(self.fromDelete) {
        [self showAlert:@"Please give us 24 hours to delete all your info. Goodbye!" caller:@"delete"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLayoutSubviews {
    self.scrollView.frame = self.view.frame;
    CGFloat contentWidth = CGRectGetWidth(self.view.bounds) * self.images.count;
    CGFloat contentHeight = CGRectGetHeight(self.view.bounds);
    self.scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
    self.scrollView.contentOffset = CGPointMake(0, 0);
    
    CGFloat pcYPos = CGRectGetHeight(self.view.frame) - PAGECONTROL_OFFSETFROMBOTTOM;
    self.pageControl.frame = CGRectMake(0, pcYPos, CGRectGetWidth(self.view.frame), 20);
    
    CGFloat pageXPos = 0;
    for (int i=0; i<self.images.count; i++) {
        TMPageView *view = (TMPageView*)[self.scrollView viewWithTag:10000+i];
        view.frame = CGRectMake(pageXPos,
                                CGRectGetMinY(self.scrollView.bounds),
                                CGRectGetWidth(self.scrollView.bounds),
                                CGRectGetHeight(self.scrollView.bounds));
        
        pageXPos = pageXPos + CGRectGetWidth(self.scrollView.bounds);
    }
    
    CGFloat suViewYPos = CGRectGetMaxY(self.pageControl.frame)-5;
    CGFloat suHeight = CGRectGetMaxY(self.view.frame) - suViewYPos;
    self.signUpActionView.frame = CGRectMake(0, suViewYPos, CGRectGetWidth(self.view.bounds), suHeight);
}

-(BOOL)prefersStatusBarHidden {
    return TRUE;
}

-(void)addSubViewAtIndex:(NSInteger)index {
    TMPageView *view = [[TMPageView alloc] initWithFrame:CGRectZero withIndex:index];
    view.bgImageView.image = self.images[index];
    view.tag = 10000+index;
    view.subTextBaseYPos = CGRectGetHeight(self.view.frame) ;
    [self.scrollView addSubview:view];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0));
    self.pageControl.currentPage = page;
    self.isScroll = true;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSString *eventAction = @"";
    if(self.pageControl.currentPage == 1) {
        eventAction = @"matches";
    }
    else if(self.pageControl.currentPage == 2) {
        eventAction = @"conversation";
    }
    else if(self.pageControl.currentPage == 3) {
        eventAction = @"stickers";
    }
    else if(self.pageControl.currentPage == 4) {
        eventAction = @"quizzes";
    }
    if(self.pageControl.currentPage != 0 && self.isScroll)
        [self trackScreens:eventAction];
    
    self.isScroll = false;
}

-(CGRect)pageControlFrame {
    return self.pageControl.frame;
}

-(void)didPressLoginButton {
    if (self.delegate != nil) {
        [self.delegate onClick:@"login" sender:self];
    }
}
-(void)didPressSignupByFacebookButton {
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    
    //adding tracking for the fb button click
    [eventDict setObject:@"TMPagedScrollViewController" forKey:@"screenName"];
    [eventDict setObject:@"signUp" forKey:@"eventCategory"];
    [eventDict setObject:@"fb_click" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
    [self validateFromFacebook];
}

- (void)didPressInfoButton {
    [self presentViewController:[[TMHomeScreenInformationPopupViewController alloc] initWithNibName:@"TMHomeScreenInformationPopupViewController" bundle:nil] animated:YES completion:nil];
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    
    //adding tracking for the home screen info button press
    [eventDict setObject:@"TMLoginScreen" forKey:@"screenName"];
    [eventDict setObject:@"login" forKey:@"eventCategory"];
    [eventDict setObject:@"true" forKey:@"GA"];
    [eventDict setObject:@"home_info_button_pressed" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}

-(void)handleSuccessResponse {
    TMUserStatus userStatus = [TMUserSession sharedInstance].user.userStatus;
    if(userStatus == AUTHENTIC || userStatus == NON_AUTHENTIC){
        // move to matches
        if (self.delegate != nil) {
            [self.delegate onClick:@"matches" sender:self];
        }
    }
    else if(userStatus == INCOMPLETE){
        if (self.delegate != nil) {
            [self.delegate onClick:@"registerBasics" sender:self];
        }
    }
    else if(userStatus == SUSPENDED){
        [self showAlert:@"Your profile is currently deactivated. Do you want to activate your profile." caller:@"suspended"];
    }
    else if(userStatus == BLOCKED){
        [self showAlert:@"Your profile is blocked. Please contact Trulymadly Customer Care." caller:@"blocked"];
    }
}
-(void)handleActivateUserSuccessResponse {
    if (self.delegate != nil) {
        [self.delegate onClick:@"matches" sender:self];
    }
}
-(void)handleLogoutResponse {
    [self.delegate onClick:@"logout" sender:self];
}

-(void)configureLoaderForLoginValidationRequestStart {
    [self addFullScreenLoader];
}

-(void)configureLoaderForLoginValidationRequestFinish {
    [self removeFullScreenLoader];
}

-(NSMutableDictionary*)eventDictionary {
    NSMutableDictionary *eventDict = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDict[@"screenName"] = @"TMPagedScrollViewController";
    eventDict[@"eventCategory"] = @"signUp";
    eventDict[@"eventAction"] = @"fb_server_call";
    return eventDict;
}

-(void)trackScreens:(NSString*)eventAction {
    NSMutableDictionary *eventDict = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDict[@"screenName"] = @"TMPagedScrollViewController";
    eventDict[@"eventCategory"] = @"homeScreen";
    eventDict[@"eventAction"] = eventAction;
    eventDict[@"GA"] = @"true";
    
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
    //NSLog(@"track =================== %@",eventAction);
}

@end

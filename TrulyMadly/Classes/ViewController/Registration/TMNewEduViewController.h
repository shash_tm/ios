//
//  TMNewEduViewController.h
//  TrulyMadly
//
//  Created by Ankit on 07/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMNewEduViewController : TMBaseViewController

- (instancetype) initWithNavigationFlow:(TMNavigationFlow) navigationFlow registerData:(NSDictionary *) registerData;

@end

//
//  TMLoginViewController.h
//  TrulyMadly
//
//  Created by Ankit on 24/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseLoginViewController.h"

@protocol TMLoginViewControllerDelegate;

@interface TMLoginViewController : TMBaseLoginViewController

@property(nonatomic,weak)id<TMLoginViewControllerDelegate> delegate;

@end


@protocol TMLoginViewControllerDelegate <NSObject>

-(void)delegateForLogin:(NSString*)key sender:(UIViewController*)sender;

@end
//
//  TMRegisterBasicsViewControllerNew.m
//  TrulyMadly
//
//  Created by Suruchi Sinha on 12/13/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMRegisterBasicsViewControllerNew.h"
#import "TMEditProfile.h"
#import "TMActivityIndicatorView.h"
#import "TMErrorMessages.h"
#import "TMError.h"
#import "TMStringUtil.h"
#import "MoEngage.h"
#import "TMSwiftHeader.h"
#import "TMUserDataController.h"
#import "NSObject+TMAdditions.h"
#import "TMCitySelectionViewController.h"

@interface TMRegisterBasicsViewControllerNew ()<UIPickerViewDelegate,UIPickerViewDataSource,TMUserCitySelectionViewDelegate>

@property (nonatomic, strong) TMActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) TMEditProfile *editProfile;
@property (nonatomic, strong) TMUserDataController *userDataController;

@property (nonatomic, strong) UIImageView *progressBar;
@property (nonatomic, strong) UIImageView *dropDownImage;
@property (nonatomic, strong) UIImageView *dropDownImage2;
@property (nonatomic, strong) UIImageView *dropDownImage3;

@property (nonatomic, strong) TMCitySelectionViewController *citySelectionViewController;
@property (nonatomic, strong) UIActivityIndicatorView *activityLoader;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIPickerView *picker;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *pickerSeparator;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) UIButton *dob;
@property (nonatomic, strong) UIButton *cityButton;
@property (nonatomic, strong) UIButton *heightButton;
@property (nonatomic, strong) UIButton *continueButton;
@property (nonatomic, strong) UIButton *pickerCancel;
@property (nonatomic, strong) UIButton *pickerDone;

@property (nonatomic, strong) NSDictionary *registerData;
@property (nonatomic, strong) NSMutableArray *cities;
@property (nonatomic, strong) NSArray *heightData;
@property (nonatomic, strong) NSDictionary *MonthToInt;
@property (nonatomic, strong) NSDictionary *IntToMonth;
@property (nonatomic, strong) NSMutableArray *allCities;
@property (nonatomic, strong) NSArray* popularCityData;

@property (nonatomic, strong) NSString *strDate;
@property (nonatomic, strong) NSString *birthMonth;
@property (nonatomic, strong) NSString *birthDay;
@property (nonatomic, strong) NSString *birthYear;
@property (nonatomic, strong) NSString *haveChildren;
@property (nonatomic, strong) NSString *stayCountry;
@property (nonatomic, strong) NSString *stayState;
@property (nonatomic, strong) NSString *stayCity;
@property (nonatomic, strong) NSString *heightFoot;
@property (nonatomic, strong) NSString *heightInch;
@property (nonatomic, strong) NSString *eventCategory;

@property (nonatomic, assign) CGFloat extraH;
@property (nonatomic, assign) CGRect bounds;
@property (nonatomic, assign) TMNavigationFlow navigationFlow;
@property (nonatomic, assign) BOOL isDateFilled;
@property (nonatomic, assign) BOOL isCityFilled;
@property (nonatomic, assign) BOOL isHeightSelected;

@property (nonatomic, assign) NSInteger indexForHeight;

@end

@implementation TMRegisterBasicsViewControllerNew

- (instancetype) initWithNavigationFlow:(TMNavigationFlow) navigationFlow {
    self = [super init];
    if(self) {
        self.navigationFlow = navigationFlow;
        [self initializeRegisterData];
    }
    return self;
    
}
- (void)initializeRegisterData {
    self.eventCategory = @"edit_profile";
    self.image = [UIImage imageNamed:@"Dropdown_black"];
    self.bounds = [[UIScreen mainScreen] bounds];
    self.isDateFilled = false;
    self.isHeightSelected = false;
    self.isCityFilled = false;
    self.IntToMonth = @{@"01":@"January",@"02":@"February",@"03":@"March",@"04":@"April",@"05":@"May",@"06":@"June",
                        @"07":@"July",@"08":@"August",@"09":@"September",@"10":@"October",@"11":@"November",@"12":@"December"};
    self.heightData = [self.userDataController getHeightData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self startTrackingEvents];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:FALSE];
    [self.navigationItem setHidesBackButton:FALSE];
    [self.navigationItem setTitle:@"Down To Basics"];
    
    if(self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE) {
        self.extraH = 64.0;
    }
    [self loadData];
}

- (void)startTrackingEvents {
    [[TMAnalytics sharedInstance] trackView:@"TMRegisterBasicsViewController"];
}
- (TMEditProfile *)editProfile {
    if(_editProfile == nil) {
        _editProfile = [[TMEditProfile alloc] init];
    }
    return _editProfile;
}
-(TMUserDataController*)userDataController {
    if(!_userDataController) {
        _userDataController = [[TMUserDataController alloc] init];
    }
    return _userDataController;
}
- (void)loadData {
    if([self.editProfile isNetworkReachable]) {
        [self loadEditData];
    }else {
        if([self.editProfile isCachedResponsePresent]) {
            [self loadEditData];
        }else {
            [self showRetryViewForError];
        }
    }
}

- (void)setTrackingDetails:(NSString *) eventAction{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMRegisterBasicsViewController" forKey:@"screenName"];
    [eventDict setObject:self.eventCategory forKey:@"eventCategory"];
    [eventDict setObject:eventAction forKey:@"eventAction"];
    self.editProfile.trackEventDictionary = eventDict;
}

- (void)loadEditData {
    NSDictionary *queryString = @{@"login_mobile":@"true"};
    [self setTrackingDetails:@"page_load"];
    [self addIndicator];
    [self.activityIndicator setTitleText:@"Loading..."];
    [self.activityIndicator startAnimating];
    [self.editProfile initCall:queryString with:^(NSDictionary *response, TMError *error){
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
        self.activityIndicator = nil;
        if(response != nil) {
            NSDictionary *basicDataDic;
            NSString *urlString;
            
            [self addIndicator];
            [self.activityIndicator setTitleText:@"Loading..."];
            [self.activityIndicator startAnimating];
            
            self.registerData = [response objectForKey:@"user_data"];
            basicDataDic = [response objectForKey:@"basics_data"];
            urlString = [basicDataDic objectForKey:@"url"];
            
            [self.userDataController getUserBasicDataFromURLString:urlString with:^(BOOL status, TMError * _Nullable error) {
                [self.activityIndicator stopAnimating];
                [self.activityIndicator removeFromSuperview];
                self.activityIndicator = nil;
                if(status) {
                    [self.userDataController getAvailableCityList:^(NSArray * _Nonnull cityList, NSArray * _Nonnull popularCityList) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.allCities = cityList;
                            self.popularCityData = popularCityList;
                            //self.allStates = [buildForm getStates];
                            [self initialize];
                            [self setDate];
                            self.picker.dataSource = self;
                            self.picker.delegate = self;
                            [self prefillEdit];
                        });
                    }];
                }else {
                    [self showRetryViewForError];
                }

            }];
        }else {
            if(error.errorCode == TMERRORCODE_LOGOUT) {
                if(self.actionDelegate != nil) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
            }else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                if(![self.editProfile isCachedResponsePresent]) {
                    if(error.errorCode == TMERRORCODE_NONETWORK) {
                        [self showRetryView];
                    }
                    else {
                        [self showRetryViewForError];
                    }
                }
            }else {
                [self showRetryViewForUnknownError];
            }
        }
    }];
}

- (void)showRetryViewForError {
    if(! [self isRetryViewShown]) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)showRetryViewForUnknownError {
    if([self isRetryViewShown]) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)showCitySelectionView {
    self.citySelectionViewController = [[TMCitySelectionViewController alloc] initWithCityData:self.allCities popularCityData:self.popularCityData delegate:self];
    [self presentViewController:self.citySelectionViewController animated:TRUE completion:^{
        
    }];
}

- (void)addIndicator {
    if([self isRetryViewShown]) {
        [self removeRetryView];
    }
    
    self.activityIndicator = [[TMActivityIndicatorView alloc] init];
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.backgroundColor = [UIColor whiteColor];
    [self.view bringSubviewToFront:self.activityIndicator];
}

-(void)initialize {
    CGFloat frameHeight, yPos, naviHeight, statusBarHeight, scrollViewXPos, scrollViewYPos;
    NSDictionary *cancelTextDict, *doneTextDict;
    CGRect cancelFrame, doneFrame;
    
    self.picker = [[UIPickerView alloc] init];
    frameHeight = CGRectGetHeight(self.picker.frame) - 54;
    yPos = self.bounds.size.height - CGRectGetHeight(self.picker.frame)- self.extraH;
    self.picker.frame = CGRectMake(0, yPos, CGRectGetWidth(self.bounds), frameHeight);
    self.picker.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.picker.hidden = true;
    [self.view addSubview:self.picker];
    
    self.datePicker = [[UIDatePicker alloc] init];
    frameHeight = CGRectGetHeight(self.datePicker.frame) - 54;
    yPos = self.bounds.size.height - CGRectGetHeight(self.datePicker.frame)- self.extraH;
    self.datePicker.frame = CGRectMake(0, yPos, CGRectGetWidth(self.bounds), frameHeight);
    self.datePicker.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.datePicker.hidden = true;
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.datePicker];
    
    yPos = self.datePicker.frame.origin.y - 50;
    self.pickerSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, CGRectGetWidth(self.bounds), 50)];
    self.pickerSeparator.backgroundColor = [UIColor colorWithRed:0.922 green:0.922 blue:0.922 alpha:1.0];
    self.pickerSeparator.hidden = true;
    [self.view addSubview:self.pickerSeparator];
    
    cancelTextDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:18], @"kfont", [NSNumber numberWithFloat:(CGRectGetWidth(self.bounds) - 32)], @"kmaxwidth", @"Cancel", @"ktext", nil];
    cancelFrame = [TMStringUtil textRectForString:cancelTextDict];
    self.pickerCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    self.pickerCancel.frame = CGRectMake(16, 0, cancelFrame.size.width, 50);
    [self.pickerCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.pickerCancel.titleLabel setFont:[UIFont systemFontOfSize:18]];
    self.pickerCancel.hidden = true;
    [self.pickerCancel addTarget:self action:@selector(hidePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerSeparator addSubview:self.pickerCancel];
    
    doneTextDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:18], @"kfont", [NSNumber numberWithFloat:(CGRectGetWidth(self.bounds) - 32)], @"kmaxwidth", @"Done", @"ktext", nil];
    doneFrame = [TMStringUtil textRectForString:doneTextDict];
    self.pickerDone = [UIButton buttonWithType:UIButtonTypeSystem];
    self.pickerDone.frame = CGRectMake((CGRectGetWidth(self.bounds) - CGRectGetWidth(doneFrame) - 16), 0,CGRectGetWidth(doneFrame), 50);
    [self.pickerDone setTitle:@"Done" forState:UIControlStateNormal];
    [self.pickerDone.titleLabel setFont:[UIFont systemFontOfSize:18]];
    self.pickerDone.hidden = true;
    [self.pickerDone addTarget:self action:@selector(donePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerSeparator addSubview:self.pickerDone];
    
    naviHeight = self.navigationController.navigationBar.bounds.size.height;
    statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollViewXPos = self.scrollView.frame.origin.x;
    if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) {
        if(naviHeight) {
            scrollViewYPos = naviHeight + 16.0 + statusBarHeight;
        }else {
            scrollViewYPos = 60.0 + statusBarHeight;
        }
    }else {
        scrollViewYPos = 16.0;
    }
    self.scrollView.frame = CGRectMake(scrollViewXPos, scrollViewYPos, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds) - scrollViewYPos);
    [self.view addSubview:self.scrollView];
    
    self.dob = [UIButton buttonWithType:UIButtonTypeSystem];
    self.dob.frame = CGRectMake(16.0, 0, CGRectGetWidth(self.bounds) - 32.0, 44.0);
    [self.dob setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.dob setTitle:@"Date of Birth" forState:UIControlStateNormal];
    [self.dob.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    self.dob.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.dob.layer.cornerRadius = 10.0;
    self.dob.layer.borderWidth = 0.3;
    self.dob.layer.borderColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0].CGColor;
    self.dob.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self.dob addTarget:self action:@selector(didBirthClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.dob];
    
    self.dropDownImage = [[UIImageView alloc] init];
    self.dropDownImage.image = [UIImage imageNamed:@"Dropdown"];
    self.dropDownImage.frame = CGRectMake(self.dob.frame.size.width - 9, (self.dob.frame.origin.y + (self.dob.frame.size.height - 10)/2), 16, 10);
    [self.scrollView addSubview:self.dropDownImage];
    
    self.cityButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.cityButton.frame = CGRectMake(16,
                                       self.self.dob.frame.origin.y + CGRectGetHeight(self.self.dob.frame) + 10.0,
                                       CGRectGetWidth(self.bounds) - 32.0,
                                       44.0);
    [self.cityButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.cityButton setTitle:@"Current City" forState:UIControlStateNormal];
    [self.cityButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    self.cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.cityButton.layer.cornerRadius = 10.0;
    self.cityButton.layer.borderWidth = 0.3;
    self.cityButton.layer.borderColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0].CGColor;
    self.cityButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self.cityButton addTarget:self action:@selector(didCityClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.cityButton];
    
    self.dropDownImage2 = [[UIImageView alloc] init];
    self.dropDownImage2.image = [UIImage imageNamed:@"Dropdown"];
    self.dropDownImage2.frame = CGRectMake(self.cityButton.frame.size.width - 9, (self.cityButton.frame.origin.y + (self.cityButton.frame.size.height - 10)/2), 16, 10);
    [self.scrollView addSubview:self.dropDownImage2];
    
    self.heightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.heightButton.frame = CGRectMake(16, self.cityButton.frame.origin.y + CGRectGetHeight(self.cityButton.frame) + 10.0, CGRectGetWidth(self.bounds) - 32.0, 44.0);
    [self.heightButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.heightButton setTitle:@"Current Height" forState:UIControlStateNormal];
    [self.heightButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    self.heightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.heightButton.layer.cornerRadius = 10.0;
    self.heightButton.layer.borderWidth = 0.3;
    self.heightButton.layer.borderColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0].CGColor;
    self.heightButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self.heightButton addTarget:self action:@selector(didHeightClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.heightButton];
    
    self.dropDownImage3 = [[UIImageView alloc] init];
    self.dropDownImage3.image = [UIImage imageNamed:@"Dropdown"];
    self.dropDownImage3.frame = CGRectMake(self.heightButton.frame.size.width - 9, (self.heightButton.frame.origin.y + (self.heightButton.frame.size.height - 10)/2), 16, 10);
    [self.scrollView addSubview:self.dropDownImage3];
    
    self.continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.continueButton.frame = CGRectMake(16, (self.heightButton.frame.origin.y + self.heightButton.frame.size.height + 15), self.bounds.size.width-32, 44);
    [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.continueButton setTitle:@"Save" forState:UIControlStateNormal];
    self.dob.enabled = false;
    [self.continueButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [self.continueButton addTarget:self action:@selector(onContinue:) forControlEvents:UIControlEventTouchUpInside];
    [self.continueButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    self.continueButton.enabled = false;
    self.continueButton.layer.cornerRadius = 10;
    self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
    [self.scrollView addSubview:self.continueButton];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0+self.extraH);
    
    [self.view bringSubviewToFront:self.picker];
    [self.view bringSubviewToFront:self.datePicker];
    [self.view bringSubviewToFront:self.pickerCancel];
    [self.view bringSubviewToFront:self.pickerDone];
    [self.view bringSubviewToFront:self.pickerSeparator];
    
}
-(void)prefillEdit {
    NSString* dobStr = self.registerData[@"DateOfBirth"];
    if(((dobStr != nil)) && (![dobStr isEqualToString:@"0000-00-00"])) {
        NSArray* dobArr = [dobStr componentsSeparatedByString:@"-"];
        NSString* tmp = dobArr[1];
        NSString* month = self.IntToMonth[tmp];
        NSString* date = dobArr[2];
        NSString* year = dobArr[0];
        NSString* tempDate = [NSString stringWithFormat:@"%@ %@, %@",month,date,year];
        self.strDate = tempDate;
        [self updateDate];
    }
    
    //height
    NSNumber* heightInFootTmp = self.registerData[@"height_foot"];
    NSNumber* heightInInchTmp = self.registerData[@"height_inch"];
    if( [heightInFootTmp isValidObject] && [heightInInchTmp isValidObject] ) {
        NSString* heightInFoot = [NSString stringWithFormat:@"%ld",heightInFootTmp.integerValue];
        NSString* heightInInch = [NSString stringWithFormat:@"%ld",heightInInchTmp.integerValue];
        NSString* key = [NSString stringWithFormat:@"%@-%@",heightInFoot,heightInInch];
        NSInteger index = [self getIndexForHeightWithKey:key fromList:self.heightData];
        if(index != -1) {
            [self updateHeight:index];
        }
    }

    //city
    NSString* cityTmp = self.registerData[@"stay_city"];
    if([cityTmp isValidObject]) {
        NSInteger index =  [self getIndexForCityWithKey:cityTmp fromList:self.allCities];
        if(index != -1){
            [self updateCityAtIndex:index];
        }
    }
}

-(NSInteger)getIndexForHeightWithKey:(NSString*)inputKey fromList:(NSArray*)fromList {
    for(int i=0;i<fromList.count;i++){
        NSDictionary* heightData = fromList[i];
        NSString* key = heightData[@"key"];
        if([inputKey isEqualToString:key]){
            return i;
        }
    }
    return -1;
}

-(NSInteger)getIndexForCityWithKey:(NSString*)key fromList:(NSArray*)fromList {
    for(int i=0;i<fromList.count;i++){
        NSDictionary* cities = fromList[i];
        NSString* cityId = cities[@"cityid"];
        if([cityId isEqualToString:key]){
            return i;
        }
    }
    return -1;
}

- (void)datePickerChanged:(UIDatePicker*) datePicker {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    self.strDate = [dateFormatter stringFromDate:datePicker.date];
}

- (void)hidePicker:(UIButton *)sender {
    self.picker.hidden = true;
    self.datePicker.hidden = true;
    self.pickerCancel.hidden = true;
    self.pickerDone.hidden = true;
    self.pickerSeparator.hidden = true;
}

- (void)donePicker:(UIButton *)sender {
    if(!self.datePicker.hidden) {
        [self updateDate];
    }
    if(self.picker.tag == 4) {
        [self updateHeight:self.indexForHeight];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.heightData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSDictionary *dataDict;
    NSString *title;
    if(pickerView.tag == 4) {
        dataDict = self.heightData[row];
        title = [dataDict objectForKey:@"value"];
    }else{
        title = @" ";
    }
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView.tag == 4) {
        self.indexForHeight = row;
    }
}

-(void)didCancelCitySearch {
    [self.citySelectionViewController dismissViewControllerAnimated:TRUE completion:^{
        
    }];
    self.citySelectionViewController = nil;
}
-(void)didSelectCity:(NSDictionary*)cityData {
    [self updateCityWithData:cityData];
    [self.citySelectionViewController dismissViewControllerAnimated:TRUE completion:^{
        
    }];
    self.citySelectionViewController = nil;
}

- (void)updateDate {
    if([self.strDate isEqualToString:@""]) {
        return;
    }
    CGRect dropDownImageFrame;
    NSString *dobStr, *dayString, *month, *year;
    NSArray *dobArr, *day;
    
    [self.dob setTitle:self.strDate forState:UIControlStateNormal];
    UIImage *lockImage = [UIImage imageNamed:@"Lock"];
    self.dropDownImage.image = lockImage;
    dropDownImageFrame = self.dropDownImage.frame;
    dropDownImageFrame.size.height = 15.0;
    self.dropDownImage.frame = dropDownImageFrame;
    
    dobStr = self.dob.currentTitle;
    dobArr = [dobStr componentsSeparatedByString:@" "];
    dayString = [dobArr objectAtIndex:1];
    day = [dayString componentsSeparatedByString:@","];
    month = [dobArr objectAtIndex:0];
    year = [dobArr objectAtIndex:2];
    
    self.birthDay = [day objectAtIndex:0];
    self.birthMonth = [self.MonthToInt objectForKey:month];
    self.birthYear = year;
    self.datePicker.hidden = true;
    self.pickerCancel.hidden = true;
    self.pickerDone.hidden = true;
    self.pickerSeparator.hidden = true;
    self.isDateFilled = true;
    
    [self enableContinue];
}

- (void)updateCityAtIndex:(NSInteger)index {
    if([self.allCities count] > index) {
        NSDictionary *cityDict = self.allCities[index];
        [self updateCityWithData:cityDict];
    }
}
-(void)updateCityWithData:(NSDictionary*)cityData {
    self.dropDownImage2.image = self.image;
    
    [self.cityButton setTitle:[cityData objectForKey:@"cityname"] forState:UIControlStateNormal];
    [self.cityButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    self.cityButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.cityButton.layer.borderWidth = 1.0;
    
    NSString* countryId = [cityData objectForKey:@"cid"];
    NSString* stateId = [cityData objectForKey:@"sid"];
    NSString *cityId = [cityData objectForKey:@"cityid"];
    self.stayCity = [NSString stringWithFormat:@"%@%@%@%@%@",countryId,@"-",stateId,@"-",cityId];
    self.stayState = [NSString stringWithFormat:@"%@%@%@",countryId,@"-",stateId];
    self.stayCountry = countryId;
    self.picker.hidden = true;
    self.pickerCancel.hidden = true;
    self.pickerDone.hidden = true;
    self.pickerSeparator.hidden = true;
    
    self.isCityFilled = TRUE;
    [self enableContinue];
}
- (void)updateHeight:(NSInteger)row {
    NSDictionary *heightDict;
    NSString *heightString;
    NSArray *heightArray;
    
    heightDict = self.heightData[row];
    self.indexForHeight = row;
    self.dropDownImage3.image = self.image;
    [self.heightButton setTitle:[heightDict objectForKey:@"value"] forState:UIControlStateNormal];
    [self.heightButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    self.heightButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.heightButton.layer.borderWidth = 1.0;
    self.picker.hidden = true;
    self.pickerCancel.hidden = true;
    self.pickerDone.hidden = true;
    self.pickerSeparator.hidden = true;
    heightString = [heightDict objectForKey:@"key"];
    heightArray = [heightString componentsSeparatedByString:@"-"];
    self.heightFoot = [heightArray objectAtIndex:0];
    self.heightInch = [heightArray objectAtIndex:1];
    self.isHeightSelected = true;
    [self enableContinue];
}

- (void)setDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeInterval oneYearTime;
    NSDate *todayDate, *minFromToday, *maxFromToday;
    
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    oneYearTime = -365 * 24 * 60 *60;
    todayDate = [NSDate date];
    minFromToday = [todayDate dateByAddingTimeInterval:(18 * oneYearTime)];
    maxFromToday = [todayDate dateByAddingTimeInterval:(70 * oneYearTime)];
    self.datePicker.maximumDate = minFromToday;
    self.datePicker.minimumDate = maxFromToday;
    
    self.strDate = [dateFormatter stringFromDate:self.datePicker.maximumDate];
}

- (void)didBirthClick:(UIButton *)sender {
    self.datePicker.hidden = false;
    self.picker.hidden = true;
    self.pickerCancel.hidden = false;
    self.pickerDone.hidden = false;
    self.pickerSeparator.hidden = false;
}

- (void)didCityClick:(UIButton *)sender {
    [self showCitySelectionView];
}

- (void)didHeightClick:(UIButton *)sender {
    self.picker.hidden = false;
    self.picker.tag = 4;
    self.datePicker.hidden = true;
    self.pickerCancel.hidden = false;
    self.pickerDone.hidden = false;
    self.pickerSeparator.hidden = false;
    [self.picker reloadAllComponents];
    [self.picker selectRow:self.indexForHeight inComponent:0 animated:false];
}

- (void)onContinue:(UIButton *)sender {
    [self update];
}

- (void)update {
    
    NSMutableDictionary *queryString, *eventDict;
    
    queryString = [[NSMutableDictionary alloc] init];
    eventDict = [[NSMutableDictionary alloc] init];
    
    [queryString setObject:[NSNumber numberWithBool:true] forKey:@"login_mobile"];
    [queryString setObject:@"" forKey:@"TM_HaveChildren"];
    [queryString setObject:@"basics" forKey:@"param"];
    [queryString setObject:self.stayState forKey:@"TM_S3_StayState"];
    [queryString setObject:self.stayCity forKey:@"TM_S3_StayCity"];
    [queryString setObject:self.heightFoot forKey:@"TM_S1_HeightFoot"];
    [queryString setObject:self.heightInch forKey:@"TM_S1_HeightInch"];
    [queryString setObject:@"" forKey:@"TM_Marital_status"];
    [queryString setObject:self.stayCountry forKey:@"TM_S3_StayCountry"];
    
    [self setTrackingDetails:@"basics_server_save_call"];
    
    if([self.editProfile isNetworkReachable]) {
        self.view.userInteractionEnabled = false;
        [self addLoaderForUpdate];
        
        self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
        [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
        [self.activityLoader startAnimating];
        
        [self.editProfile update:queryString with:^(NSString *response, TMError *error){
            self.view.userInteractionEnabled = true;
            [self enableContinue];
            [self.activityLoader stopAnimating];
            [self.activityLoader removeFromSuperview];
            
            if(response != nil) {
                if([response isEqualToString:@"save"]) {
                    [[MoEngage sharedInstance] trackEvent:@"Basics Save" andPayload:nil];
                    if(self.delegate != nil) {
                        [self.delegate didEditProfile];
                    }
                    [self.navigationController popViewControllerAnimated:true];
                }
            }else {
                [self errorResponse:error];
            }
        }];
    }else {
        [self showAlert:@"" title:TM_INTERNET_NOTAVAILABLE_MSG];
    }
}
    
- (void)addLoaderForUpdate {
    
    TMActivityIndicator *indicator = [[TMActivityIndicator alloc] init];
    CGRect loaderFrame;
    self.activityLoader = [indicator addActivity];
    [self.scrollView addSubview:self.activityLoader];
    
    loaderFrame = self.activityLoader.frame;
    loaderFrame.origin.x = self.continueButton.frame.origin.x + 80;
    loaderFrame.origin.y = self.continueButton.frame.origin.y + (self.continueButton.frame.size.height-self.activityLoader.frame.size.height)/2;
    self.activityLoader.frame = loaderFrame;
    [self.scrollView bringSubviewToFront:self.activityLoader];
}

- (void)errorResponse:(TMError *)error {
    if(error.errorCode == TMERRORCODE_LOGOUT) {
        if(self.actionDelegate !=  nil) {
            [self.actionDelegate setNavigationFlowForLogoutAction:false];
        }
    }else if(error.errorCode == TMERRORCODE_NONETWORK) {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG title:@""];
    }else if(error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showAlert:TM_SERVER_TIMEOUT_MSG title:@""];
    }
    else if(error.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self showAlert:error.errorMessage title: @""];
    }else {
        [self showAlert:@"Please Try Later" title:@"Problem in saving the data"];
    }
}

- (void)showAlert:(NSString *)msg title:(NSString *)title {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
}

- (void)enableContinue {
    if( self.isDateFilled && self.isCityFilled && self.isHeightSelected){
        self.continueButton.enabled = true;
        self.continueButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
        [self.continueButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    }else{
        self.continueButton.enabled = false;
        self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
        [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

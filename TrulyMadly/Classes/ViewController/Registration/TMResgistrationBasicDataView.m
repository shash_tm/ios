//
//  TMResgistrationBasicDataView.m
//  TrulyMadly
//
//  Created by Ankit on 07/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMResgistrationBasicDataView.h"
#import "NSString+TMAdditions.h"
#import "TMLog.h"

@interface TMResgistrationBasicDataView()<UITextFieldDelegate>

@property(nonatomic,strong)UIButton *heightButton;
@property(nonatomic,strong)UIButton *degreeButton;
@property(nonatomic,strong)UIButton *cityButton;
@property(nonatomic,strong)UITextField* workTextField;
@property(nonatomic,assign)RegistrationStep step;
@property(nonatomic,assign)BOOL isLocationFilled;

@end

@implementation TMResgistrationBasicDataView

-(instancetype)initWithFrame:(CGRect)frame step:(RegistrationStep)step isLocationFilled:(BOOL)isLocationFilled{
    self = [super initWithFrame:frame];
    if(self) {
        TMLOG(@"height is %f",frame.size.height);
        self.isLocationFilled = isLocationFilled;
        self.step = step;
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 10;
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = [[UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0] CGColor];
        if(step == BASICS) {
            [self configureUIForBasics];
        }else if(step == WORK) {
            [self configureUIForWork];
        }else if (step == EDUCATION) {
            [self configureUIForEducation];
        }
    }
    return self;
}

-(void)updateHeightButtonTitle:(NSString*)heightButtonTitle {
    [self.heightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.heightButton setTitle:heightButtonTitle forState:UIControlStateNormal];
}
-(void)updateWorkTextFieldText:(NSString*)workText {
    self.workTextField.text = workText;
}
-(void)updateEducationButtonTitle:(NSString*)educationButtonTitle {
    [self.degreeButton setTitle:educationButtonTitle forState:UIControlStateNormal];
}
-(void)updateCityButtonTitle:(NSString*)cityButtonTitle {
    [self.cityButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.cityButton setTitle:cityButtonTitle forState:UIControlStateNormal];
}

-(void)configureUIForBasics {
    // title
    NSString *text = @"A few basics";//@"Just a few more basics to get you started";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 25);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width-rect.size.width)/2,12,rect.size.width,rect.size.height)];
    title.text=text;
    title.font=[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0 ];
    title.numberOfLines = 0;
    [title sizeToFit];
    [self addSubview:title];
    
    //height button
    self.heightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.heightButton.frame = CGRectMake(15, title.frame.origin.y+title.frame.size.height+12.5, self.bounds.size.width-30, 44);
    [self.heightButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    self.heightButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0 ];
    self.heightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.heightButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 30.0, -15.0, 0.0)];
    [self.heightButton setTitle:@"Height" forState:UIControlStateNormal];
    [self.heightButton addTarget:self action:@selector(didHeightClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.heightButton];
    
    //height image view
    UIImageView *heightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, self.heightButton.frame.origin.y+20,24, 20)];
    [heightImageView setImage:[UIImage imageNamed:@"Height"]];
    [self addSubview:heightImageView];
    
    //heightSeparator
    UIView *heightSeparator = [[UIView alloc] initWithFrame:CGRectMake(15, self.heightButton.frame.origin.y+44, self.heightButton.frame.size.width, 0.5)];
    heightSeparator.backgroundColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
    //self.heightSeparator = heightSeparator;
    [self addSubview:heightSeparator];
    
    //height drop down image
    UIImageView *heightDropDown = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width-35, self.heightButton.frame.origin.y+27, 12, 6)];
    [heightDropDown setImage:[UIImage imageNamed:@"icon-arrow"]];
    [self addSubview:heightDropDown];
    
    if(!self.isLocationFilled) {
        self.cityButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.cityButton.frame = CGRectMake(15, heightSeparator.frame.origin.y+20, self.bounds.size.width-30, 44);
        [self.cityButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
        self.cityButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0 ];
        self.cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.cityButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 30.0, -15.0, 0.0)];
        [self.cityButton setTitle:@"City" forState:UIControlStateNormal];
        [self.cityButton addTarget:self action:@selector(didCityClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cityButton];
        
        //UITextField* textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        //[self addSubview:textField];
        //city image view
        UIImageView *cityImageView = [[UIImageView alloc] initWithFrame:CGRectMake(19, self.cityButton.frame.origin.y+20,14, 20)];
        [cityImageView setImage:[UIImage imageNamed:@"Location"]];
        [self addSubview:cityImageView];
        
        //citySeparator
        UIView *citySeparator = [[UIView alloc] initWithFrame:CGRectMake(15, self.cityButton.frame.origin.y+44,
                                                                         self.cityButton.frame.size.width, 0.5)];
        citySeparator.backgroundColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
        //self.citySeparator = citySeparator;
        [self addSubview:citySeparator];
        
        //city drop down image
//        UIImageView *cityDropDown = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width-35, cityButton.frame.origin.y+27, 12, 6)];
//        [cityDropDown setImage:[UIImage imageNamed:@"icon-arrow"]];
//        [self addSubview:cityDropDown];
        
        CGRect viewFrame = self.frame;
        viewFrame.size.height = citySeparator.frame.origin.y + citySeparator.frame.size.height + 35;
        //viewFrame.origin.y = (self.bounds.size.height-viewFrame.size.height)/2;
        self.frame = viewFrame;
        
    }else {
        CGRect viewFrame = self.frame;
        viewFrame.size.height = heightSeparator.frame.origin.y + heightSeparator.frame.size.height + 35;
        //viewFrame.origin.y = (self.bounds.size.height-viewFrame.size.height)/2;
        self.frame = viewFrame;
    }
}

-(void)configureUIForWork {
    
    // title
    NSString *text = @"My 9-5";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 25);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width-rect.size.width)/2,12,rect.size.width,rect.size.height)];
    title.text=text;
    title.font=[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0 ];
    title.numberOfLines = 0;
    [title sizeToFit];
    [self addSubview:title];
    
    //Occupation textField
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:@"Enter Your Occupation" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0]}];
    UITextField *workTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, title.frame.origin.y+title.frame.size.height+25, self.bounds.size.width-30, 44)];
    workTextField.attributedPlaceholder = placeholder;
    workTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    workTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    workTextField.delegate = self;
    workTextField.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    [workTextField addTarget:self action:@selector(didOccupationChanged:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:workTextField];
    self.workTextField = workTextField;
    
    //work image view
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(15, workTextField.frame.origin.y+20, 28, 20)];
    workTextField.leftView = paddingView;
    workTextField.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *workImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, workTextField.frame.origin.y+10,23, 20)];
    [workImageView setImage:[UIImage imageNamed:@"Occupation"]];
    [self addSubview:workImageView];
    
    
    //workSeparator
    UIView *workSeparator = [[UIView alloc] initWithFrame:CGRectMake(15, workTextField.frame.origin.y+40, workTextField.frame.size.width, 0.5)];
    workSeparator.backgroundColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
    //self.workSeparator = workSeparator;
    [self addSubview:workSeparator];
    
    CGRect viewFrame = self.frame;
    viewFrame.size.height = workSeparator.frame.origin.y + workSeparator.frame.size.height + 35;
    //viewFrame.origin.y = (self.bounds.size.height-viewFrame.size.height)/2;
    self.frame = viewFrame;
}

-(void)configureUIForEducation {
    // title
    NSString *text = @"Honour Roll";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 25);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width-rect.size.width)/2,12,rect.size.width,rect.size.height)];
    title.text=text;
    title.font=[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0 ];
    title.numberOfLines = 0;
    [title sizeToFit];
    [self addSubview:title];
    
    //height button
    UIButton *degreeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    degreeButton.frame = CGRectMake(15, title.frame.origin.y+title.frame.size.height+12.5, self.bounds.size.width-30, 44);
    [degreeButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    degreeButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0 ];
    degreeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [degreeButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 30.0, -15.0, 0.0)];
    [degreeButton setTitle:@"Highest Degree" forState:UIControlStateNormal];
    [degreeButton addTarget:self action:@selector(didDegreeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:degreeButton];
    
    //height image view
    UIImageView *degreeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, degreeButton.frame.origin.y+20,24, 20)];
    [degreeImageView setImage:[UIImage imageNamed:@"Degree"]];
    [self addSubview:degreeImageView];
    
    //heightSeparator
    UIView *degreeSeparator = [[UIView alloc] initWithFrame:CGRectMake(15, degreeButton.frame.origin.y+44, degreeButton.frame.size.width, 0.5)];
    degreeSeparator.backgroundColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
    //self.degreeSeparator = degreeSeparator;
    [self addSubview:degreeSeparator];
    
    //height drop down image
    UIImageView *degreeDropDown = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width-35, degreeButton.frame.origin.y+27, 12, 6)];
    [degreeDropDown setImage:[UIImage imageNamed:@"icon-arrow"]];
    [self addSubview:degreeDropDown];
    
    CGRect viewFrame = self.frame;
    viewFrame.size.height = degreeSeparator.frame.origin.y + degreeSeparator.frame.size.height + 35;
    
    self.frame = viewFrame;
    
}

-(void)didHeightClick:(UIButton*)sender {
    [self.delegate didButtonClick:@"height" sender:sender];
}

-(void)didStateClick:(UIButton*)sender {
    [self.delegate didButtonClick:@"state" sender:sender];
}

-(void)didCityClick:(UIButton*)sender {
    [self.delegate didButtonClick:@"city" sender:sender];
}

-(void)didDegreeClick:(UIButton*)sender {
    [self.delegate didButtonClick:@"degree" sender:sender];
}

-(void)didOccupationChanged:(UITextField*)sender {
    [self.delegate didOccupationChange:sender];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


@end

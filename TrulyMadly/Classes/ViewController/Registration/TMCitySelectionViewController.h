//
//  TMCitySelectionViewController.h
//  TrulyMadly
//
//  Created by Ankit on 18/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMUserCitySelectionView.h"

@interface TMCitySelectionViewController : UIViewController

- (instancetype)initWithCityData:(NSArray*)cityData
                 popularCityData:(NSArray*)popularCityData
                        delegate:(id<TMUserCitySelectionViewDelegate>)delegate;

@end

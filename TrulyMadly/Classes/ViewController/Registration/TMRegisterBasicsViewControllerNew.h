//
//  TMRegisterBasicsViewControllerNew.h
//  TrulyMadly
//
//  Created by Suruchi Sinha on 12/13/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMRegisterBasicsViewControllerNew : TMBaseViewController

- (instancetype) initWithNavigationFlow:(TMNavigationFlow) navigationFlow;

@end

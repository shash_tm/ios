//
//  TMRegistrationViewController.m
//  TrulyMadly
//
//  Created by Ankit on 07/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMRegistrationViewController.h"
#import "TMRegisterWorkViewController.h"
#import "TMResgistrationBasicDataView.h"
#import "TMCitySelectionViewController.h"
#import "TMUserDataController.h"
#import "TMUserSession.h"
#import "NSObject+TMAdditions.h"
#import "TMAnalytics.h"


@interface TMRegistrationViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,
                                           TMResgistrationBasicDataViewDelegate,TMUserCitySelectionViewDelegate>

@property(nonatomic,strong)TMUserDataController* userDataController;

@property(nonatomic,strong)TMResgistrationBasicDataView* resgistrationBasicDataView;
@property(nonatomic,strong)TMCitySelectionViewController* citySelectionViewController;
@property(nonatomic,strong)UIPickerView* picker;
@property(nonatomic,strong)UIView* pickerSeparator;
@property(nonatomic,strong)UIButton* pickerCancel;
@property(nonatomic,strong)UIButton* pickerDone;
@property(nonatomic,strong)UIButton* continueButton;

@property(nonatomic,strong)NSArray* heightData;
@property(nonatomic,strong)NSArray* cityData;
@property(nonatomic,strong)NSArray* popularCityData;
@property(nonatomic,strong)NSDate* startDate; //for tracking
@property(nonatomic,strong)NSString* designation;
@property(nonatomic,strong)NSMutableDictionary* registerBasicsData;

@property(nonatomic,assign)TMNavigationFlow navigationFlow;
@property(nonatomic,assign)NSInteger selectedHeightIndex;

@property(nonatomic,assign)BOOL isLocationFilled;
@property(nonatomic,assign)BOOL isCitySelected;
@property(nonatomic,assign)BOOL isHeightSelected;


@end

@implementation TMRegistrationViewController

#pragma mark - Initialization

- (instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow
{
    self = [super init];
    if (self) {
        self.navigationFlow = navigationFlow;
        if([TMUserSession sharedInstance].user.city) {
            self.isLocationFilled = TRUE;
        }
        else {
            [self.userDataController getCityList:^(NSArray * _Nonnull cityList) {
                self.cityData = cityList;
            }];
            [self.userDataController getPopularCityList:^(NSArray * _Nonnull popularCityList) {
                self.popularCityData = popularCityList;
            }];
        }
        
        self.registerBasicsData = [NSMutableDictionary dictionary];
        self.designation = [[TMUserSession sharedInstance].user fbDesignation];
        
        self.heightData = [self.userDataController getHeightData];
        self.startDate = [NSDate date];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self configureNavigationBar];
    [self configureRegistrationView];
    [self trackRegisterStartEvent];
}

- (void)trackRegisterStartEvent {
    [[TMAnalytics sharedInstance] trackFacebookEvent:@"registration" withParams:@{@"register_method":@"register_start"}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(TMUserDataController*)userDataController {
    if(!_userDataController) {
        _userDataController = [[TMUserDataController alloc] init];
    }
    return _userDataController;
}

#pragma mark - UI Handler

-(void)configureNavigationBar {
    self.navigationItem.title = @"Setting up your profile";
    [self.navigationController setNavigationBarHidden:TRUE];
    self.navigationItem.hidesBackButton = TRUE;
}

-(void)configureRegistrationView {
    
    [self createBasicsView];
    
    [self createContinueButton];
    
    [self createPickerView];
}

-(void)createBasicsView {
    CGFloat maxWidth = CGRectGetWidth(self.view.frame);
    CGFloat imageViewYPos = 65;
    CGFloat imageViewWidth = 60;
    CGFloat imageViewHeight = 10.7;
    CGRect imageViewRect = CGRectMake((maxWidth - imageViewWidth)/2, imageViewYPos,imageViewWidth, imageViewHeight);
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:imageViewRect];
    imageView.image = [UIImage imageNamed:@"Progress_1"];
    [self.view addSubview:imageView];
    
    CGFloat basicViewXPos = 20;
    CGFloat basicViewYPos = CGRectGetMaxY(imageView.frame)+15;
    CGFloat basicViewHeight = CGRectGetHeight(self.view.frame) - basicViewYPos;
    CGRect basicViewRect = CGRectMake(basicViewXPos, CGRectGetMaxY(imageView.frame)+15, maxWidth - (2*basicViewXPos), basicViewHeight);
    self.resgistrationBasicDataView = [[TMResgistrationBasicDataView alloc] initWithFrame:basicViewRect
                                                                                     step:BASICS
                                                                         isLocationFilled:self.isLocationFilled];
    self.resgistrationBasicDataView.delegate = self;
    [self.view addSubview:self.resgistrationBasicDataView];
}
-(void)createContinueButton {
    CGFloat xPos = 15;
    CGFloat maxWidth = CGRectGetWidth(self.view.frame);
    self.continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.continueButton.frame = CGRectMake(xPos, CGRectGetHeight(self.view.frame)-80, maxWidth-(2*xPos), 44);
    self.continueButton.enabled = FALSE;
    self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1];
    [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1]   forState:UIControlStateNormal];
    [self.continueButton setTitle:@"Next" forState:UIControlStateNormal];
    self.continueButton.titleLabel.font = [UIFont systemFontOfSize:15];
    self.continueButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.continueButton.layer.cornerRadius = 10;
    
    [self.continueButton addTarget:self action:@selector(onContinue) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.continueButton];
}
-(void)createPickerView {
    self.picker = [[UIPickerView alloc] init];
    self.picker.frame = CGRectMake(0,
                                   CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.picker.frame),
                                   CGRectGetWidth(self.view.frame),
                                   CGRectGetHeight(self.picker.frame));
    self.picker.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.picker.hidden = TRUE;
    self.picker.dataSource = self;
    self.picker.delegate = self;
    [self.view addSubview:self.picker];
  
    self.pickerSeparator = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                    CGRectGetMinY(self.picker.frame)-50,
                                                                    CGRectGetWidth(self.view.frame), 50)];
    self.pickerSeparator.backgroundColor = [UIColor colorWithRed:0.922 green:0.922 blue:0.922 alpha:1];
    self.pickerSeparator.hidden = TRUE;
    [self.view addSubview:self.pickerSeparator];
    
    CGFloat pickerButtonXOffset = 16;
    CGFloat cancelButtonWidth = 60;
    CGFloat doneButtonWidth = 50;
    CGFloat pickerButtonHeight = 50;
    self.pickerCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    self.pickerCancel.frame = CGRectMake(pickerButtonXOffset, 0, cancelButtonWidth, pickerButtonHeight);
    self.pickerCancel.titleLabel.font = [UIFont systemFontOfSize:18];
    self.pickerCancel.hidden = TRUE;
    [self.pickerCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.pickerCancel addTarget:self action:@selector(hidePicker) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerSeparator addSubview:self.pickerCancel];
    
    self.pickerDone = [UIButton buttonWithType:UIButtonTypeSystem];
    self.pickerDone.frame = CGRectMake(CGRectGetWidth(self.view.frame)-(doneButtonWidth+pickerButtonXOffset), 0,
                                       doneButtonWidth, pickerButtonHeight);
    [self.pickerDone setTitle:@"Done" forState:UIControlStateNormal];
    self.pickerDone.titleLabel.font = [UIFont systemFontOfSize:18];
    self.pickerDone.hidden = TRUE;
    [self.pickerDone addTarget:self action:@selector(donePicker) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerSeparator addSubview:self.pickerDone];
}
-(void)showUserCitySelectionView {
    self.citySelectionViewController = [[TMCitySelectionViewController alloc] initWithCityData:self.cityData popularCityData:self.popularCityData delegate:self];
    [self presentViewController:self.citySelectionViewController animated:TRUE completion:^{
        
    }];
}

#pragma mark - Action Handler

-(void)onContinue {
    [self trackRegistartionEventAction:@"basics"];

    self.registerBasicsData[@"TM_S3_StayCountry"] = @([TMUserSession sharedInstance].user.country);
    if(self.designation) {
        self.registerBasicsData[@"TM_Designation_0"] = self.designation;
    }

    TMRegisterWorkViewController* workViewController = [[TMRegisterWorkViewController alloc] initWithRegisterData:self.registerBasicsData];
    workViewController.delegate = self.delegate;
    workViewController.actionDelegate = self.actionDelegate;
    [self.navigationController pushViewController:workViewController animated:TRUE];
}
-(void)hidePicker {
    self.picker.hidden = TRUE;
    self.pickerCancel.hidden = TRUE;
    self.pickerDone.hidden = TRUE;
    self.pickerSeparator.hidden = TRUE;
}
-(void)donePicker {
    if(self.picker.tag == 1) {
        [self updateHeight:self.selectedHeightIndex];
    }
}
-(void)enableContinueButton {
    if(self.isHeightSelected && (self.isCitySelected || self.isLocationFilled))
    {
        // enable continue button
        self.continueButton.enabled = TRUE;
        [self.continueButton setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] forState:UIControlStateNormal];
        self.continueButton.backgroundColor = [UIColor colorWithRed:1 green:0.702 blue:0.725 alpha:1];
    }
    else {
        // disable continue button
        self.continueButton.enabled = FALSE;
        [self.continueButton setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] forState:UIControlStateNormal];
        self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1];
    }
}

#pragma mark - UI Data State Handler

-(void)updateHeight:(NSInteger)row {
    NSDictionary* heightData = self.heightData[row];
    NSString* height = heightData[@"value"];
    [self.resgistrationBasicDataView updateHeightButtonTitle:height];
    
    self.picker.hidden = TRUE;
    self.pickerCancel.hidden = TRUE;
    self.pickerDone.hidden = TRUE;
    self.pickerSeparator.hidden = TRUE;
    
    NSString* key = heightData[@"key"];
    NSArray* heightArr = [key componentsSeparatedByString:@"-"];
    
    self.registerBasicsData[@"TM_S1_HeightFoot"] = heightArr[0];
    self.registerBasicsData[@"TM_S1_HeightInch"] = heightArr[1];
    
    self.isHeightSelected = TRUE;
    
    [self enableContinueButton];
}

#pragma mark - TMResgistrationBasicDataView Handler

-(void)didButtonClick:(NSString*)text sender:(UIButton*)sender {
    
    if([text isEqualToString:@"height"]) {
        //self.button = sender
        self.picker.tag = 1;
        self.picker.hidden = FALSE;
        self.pickerCancel.hidden = FALSE;
        self.pickerDone.hidden = FALSE;
        self.pickerSeparator.hidden = FALSE;
        [self.picker reloadAllComponents];
        [self.picker selectRow:self.selectedHeightIndex inComponent:0 animated:FALSE];
    }
    else if([text isEqualToString:@"city"]) {
        [self showUserCitySelectionView];
    }
}

#pragma mark - Picker Delegate / Datasource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag == 1) {
        return self.heightData.count;
    }
    else {
        return 0;
    }
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView.tag == 1){
        NSDictionary* heightData = self.heightData[row];
        return heightData[@"value"];
    }
    return nil;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView.tag == 1){
        self.selectedHeightIndex = row;
    }
}

#pragma mark - TMUserCitySelectionViewDelegate

-(void)didCancelCitySearch {
    [self.citySelectionViewController dismissViewControllerAnimated:TRUE completion:^{
        
    }];
    self.citySelectionViewController = nil;
}
-(void)didSelectCity:(NSDictionary*)cityData {
    NSString* city = cityData[@"cityname"];
    self.isCitySelected = TRUE;
    [self.resgistrationBasicDataView updateCityButtonTitle:city];
    [self.citySelectionViewController dismissViewControllerAnimated:TRUE completion:^{
        
    }];
    self.citySelectionViewController = nil;
    [self updateCityData:cityData];
    [self enableContinueButton];
}
-(void)updateCityData:(NSDictionary*)cityData {
    NSString* countryId = cityData[@"cid"];
    NSString* stateId = cityData[@"sid"];
    NSString* cityId = cityData[@"cityid"];

    self.registerBasicsData[@"TM_S3_StayCountry"] = countryId;
    self.registerBasicsData[@"TM_S3_StayState"] = [NSString stringWithFormat:@"%@%@%@",countryId,@"-",stateId];;
    self.registerBasicsData[@"TM_S3_StayCity"] = [NSString stringWithFormat:@"%@%@%@%@%@",countryId,@"-",stateId,@"-",cityId];;
}

#pragma mark - Tracking

-(void)trackRegistartionEventAction:(NSString*)eventAction {
    NSMutableDictionary* eventDict = [NSMutableDictionary dictionary];
    eventDict[@"screenName"] = @"TMNewRegisterBasicsViewController";
    eventDict[@"eventCategory"] = @"register_basics";
    eventDict[@"eventAction"] = eventAction;
    eventDict[@"status"] = @"success";
    eventDict[@"startDate"] = self.startDate;
    eventDict[@"endDate"] = [NSDate date];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    [[TMAnalytics sharedInstance] trackFacebookEvent:@"registration" withParams:@{@"register_method":@"basics"}];
}

@end


//
//  TMBaseLoginViewController.m
//  TrulyMadly
//
//  Created by Ankit on 23/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseLoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "TMUserDataManager.h"
#import "TMActivityIndicatorView.h"
#import "TMUserSession.h"
#import "TMError.h"
#import "TMDataStore.h"
#import "TMNotification.h"
#import "TMErrorMessages.h"
#import "TMLog.h"
#import "TMAnalytics.h"
#import "MoEngage.h"
#import "TMBirthdayNudgeViewController.h"
#import "TMAppViralityController.h"
#import "TMUserDataController.h"


@interface TMBaseLoginViewController ()<UIAlertViewDelegate,TMBirthdayNudgeViewControllerDelegate>

@property(nonatomic,strong)TMUserDataManager *userDataManager;
@property(nonatomic,strong)TMUserDataController *userDataController;
@property(nonatomic,strong)UIView *fullScreenView;
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicator;
@property(nonatomic,assign)BOOL isFacebookStateChangedNotificationReceived;

@end

@implementation TMBaseLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    [self removeObserver];
}

-(TMUserDataController*)userDataController {
    if(!_userDataController) {
        _userDataController = [[TMUserDataController alloc] init];
    }
    return _userDataController;
}

-(TMUserDataManager*)userDataManager {
    if(!_userDataManager) {
        _userDataManager = [[TMUserDataManager alloc] init];
    }
    return _userDataManager;
}

-(void)validateLoginFromEmailWithParams:(NSDictionary*)paramsDict {
    [self validateLoginWithParams:paramsDict];
}

-(void)validateFromFacebook {
    [self addFacebookObserver];
    self.isFacebookStateChangedNotificationReceived = FALSE;
    
    [[FBSession activeSession] closeAndClearTokenInformation];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate openActiveSessionWithPermissions:TRUE from:self.identifier];
}

-(void)executeHandle:(NSNotification*)notiication {
    NSDictionary *dict = [notiication userInfo];
    NSString *from = [dict objectForKey:@"from"];
    
    if( ([FBSession activeSession].state == FBSessionStateOpen || [FBSession activeSession].state == FBSessionStateOpenTokenExtended) && ([from isEqualToString:self.identifier]) && (!self.isFacebookStateChangedNotificationReceived) ) {
        ///first update state to receive no more notification
        self.isFacebookStateChangedNotificationReceived = TRUE;
        [self removeObserver];
        ////////
        NSArray *permissions = [FBSession activeSession].permissions;
        
        if([self checkUserBirthdayPermission:permissions]) {
            NSDictionary *loginParams = [self getQueryParams:@"fb"];
            [self validateLoginWithParams:loginParams];
        }else {
            NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
            
            //adding tracking for the fb button click
            [eventDict setObject:@"TMPagedScrollViewController" forKey:@"screenName"];
            [eventDict setObject:self.identifier forKey:@"eventCategory"];
            [eventDict setObject:@"fb_birthday_permission_shown" forKey:@"eventAction"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
            
            TMBirthdayNudgeViewController *birthdayNudge = [[TMBirthdayNudgeViewController alloc] init];
            birthdayNudge.delegate = self;
            [self presentViewController:birthdayNudge animated:TRUE completion:nil];
           // [self showAlert:@"Hey! We need access to your birthday info to get you started." caller:@"fbP"];
        }
    }
}


-(BOOL)checkUserBirthdayPermission:(NSArray*)permissions {
    for (NSString *permission in permissions) {
        if ([permission isEqualToString:@"user_birthday"]) {
            return YES;
        }
    }
    return NO;
}

-(void)validateLoginWithParams:(NSDictionary*)paramsDict {
    [self configureLoaderForLoginValidationRequestStart];
    self.view.userInteractionEnabled = FALSE;
    [self.userDataController validateUserWithLoginParams:paramsDict didSuccessfullyValidate:^{
        [self configureLoaderForLoginValidationRequestFinish];
        [self configureLoaderForLoginValidationRequestStart];
        
    } didCompleteLoginStep:^(TMError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = TRUE;
            [self configureLoaderForLoginValidationRequestFinish];
            if(!error) {
                
                [[MoEngage sharedInstance]trackEvent:@"Login Successful" andPayload:nil];
                
                [self updateUserIdInCrashlytics];
                [self updateUserLoginStatusAsActvie];
                [self handleSuccessResponse];
                [self updateUserActivityForMoEngage];
                //send login status
                TMUserStatus userStatus = [TMUserSession sharedInstance].user.userStatus;
                if(userStatus != BLOCKED && userStatus != SUSPENDED) {
                    [self sendTokenToServer];
                }
            }else {
                [self handleErrorResponse:error];
            }
        });
    }];
}

-(NSDictionary*)getQueryParams:(NSString*)key {
    NSMutableDictionary *queryString = [NSMutableDictionary dictionary];
    if([key isEqualToString:@"fb"]) {
        NSString *token = [self facebookToken];
        queryString[@"from_fb"] = @"true";
        queryString[@"token"] = token;
        queryString[@"login_mobile"] = @true;
        queryString[@"device_id"] = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
    else if([key isEqualToString:@"activate"]) {
        queryString[@"login_mobile"] = [NSNumber numberWithBool:true];
        queryString[@"activate"] = [NSNumber numberWithBool:true];
    }
    return queryString;
}

-(NSString*)facebookToken {
    NSString *token = [FBSession activeSession].accessTokenData.accessToken;
    return token;
}

-(void)updateUserIdInCrashlytics {
    TMUser *user = [TMUserSession sharedInstance].user;
    NSString *userId = user.userId;
    if(userId) {
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [delegate setCrashlyticsIdentifier:userId];
    }
}

-(void)updateUserActivityForMoEngage {
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate setUserAttributeForMoEngage];
}

-(void)updateUserLoginStatusAsActvie {
    [[TMUserSession sharedInstance] setUserLoginStatus:true];
}

-(void)activateUser {
    
    if(self.userDataManager.isNetworkReachable) {
        [self configureLoaderForLoginValidationRequestStart];
        self.view.userInteractionEnabled = false;
        NSDictionary *loginDict = [self getQueryParams:@"activate"];
        
        [self.userDataManager activateUser:loginDict with:^(NSString *status, TMError *error) {
            self.view.userInteractionEnabled = true;
            [self configureLoaderForLoginValidationRequestFinish];
            
            if(([status isEqualToString:@"success"]) && !error) {
                [self sendTokenToServer];
                [self updateUserLoginStatusAsActvie];
                [self handleActivateUserSuccessResponse];
            }else {
                [self handleErrorResponse:error];
            }
        }];
    }
    else {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG caller:@""];
    }
}

-(void)handleSuccessResponse {
    
}
-(void)handleActivateUserSuccessResponse {
    
}
-(void)handleLogoutResponse {
    
}

-(void)handleErrorResponse:(TMError*)error {
    if(error.errorCode == TMERRORCODE_LOGOUT) {
        [self handleLogoutResponse];
    }
    else if(error.errorCode == TMERRORCODE_NONETWORK) {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG caller:@""];
    }
    else if(error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showAlert:TM_SERVER_TIMEOUT_MSG caller:@""];
    }
    else if(error.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self showAlert:error.errorMessage caller:@""];
    }
    else {
        [self showAlert:TM_UNKNOWN_MSG caller:@""];
    }
}

-(void)showAlert:(NSString*)msg caller:(NSString*)caller {
    NSString *title = @"Login failed";
    if([caller isEqualToString:@"delete"]) {
            title = @"";
    }
    
    NSString *cancelTitle = @"OK";
    if([caller isEqualToString:@"suspended"]){
        cancelTitle = @"No";
    }
    else if ([caller isEqualToString:@"fbP"]) {
        cancelTitle = @"Cancel";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:nil, nil];

    if([caller isEqualToString:@"suspended"]) {
        [alertView addButtonWithTitle:@"Yes"];
    }else if([caller isEqualToString:@"fbP"]) {
        [alertView addButtonWithTitle:@"Ok"];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    if([buttonTitle isEqualToString:@"Yes"]) {
        [self activateUser];
    }else if([buttonTitle isEqualToString:@"Ok"]){
        [self requestBirthday];
    }
}

-(void)requestBirthday{
    [self addFacebookObserver];
    self.isFacebookStateChangedNotificationReceived = FALSE;
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate requestBirthdayPermission:self.identifier];
}

-(void)configureLoaderForLoginValidationRequestStart {
    
}

-(void)configureLoaderForLoginValidationRequestFinish {
    
}

-(void)addFullScreenLoader {
    self.fullScreenView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.fullScreenView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.fullScreenView];

    self.activityIndicator = [[TMActivityIndicatorView alloc] init];
    
    NSString *text = @"";
    if([self.identifier isEqualToString:@"signup"]) {
        text = @"\nWelcome aboard! Now let's get you ready...";
    }
    else if ([self.identifier isEqualToString:@"login"]) {
        text = @"\nLet's see who you're gonna meet today...";
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14], @"kfont", [NSNumber numberWithFloat:300], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    CGRect frameLabel = self.activityIndicator.titleLable.frame;
    frameLabel.origin.x = (self.view.bounds.size.width-rect.size.width)/2;
    frameLabel.size.width = rect.size.width;
    self.activityIndicator.titleLable.frame = frameLabel;
    
    self.activityIndicator.titleText = text;
    //self.activityIndicator.titleText = @"\nGive us a few secs...\n\nJust fetching your info with a little help from Facebook";
    [self.activityIndicator.titleLable sizeToFit];
    [self.view addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];
}

-(void)removeFullScreenLoader {
    [self.fullScreenView removeFromSuperview];
    
    if(self.activityIndicator != nil) {
        [self.activityIndicator stopAnimating];
    }
}

-(void)sendTokenToServer {
    NSString *token = [TMDataStore retrieveObjectforKey:@"deviceToken"];
    if(token != nil) {
        [TMNotification sendToken:@"login" token:token];
    }
}

-(void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)addFacebookObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(executeHandle:) name:@"SessionStateChangeNotification" object:nil];
}
-(NSMutableDictionary*)eventDictionary {
    return nil;
}

-(void)didPressBirthdayPermissionButton {
    [self requestBirthday];
}
@end

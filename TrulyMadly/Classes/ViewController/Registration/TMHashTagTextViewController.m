//
//  TMHashTagTextViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMHashTagTextViewController.h"
#import "TMHashTagListView.h"
#import "NSString+TMAdditions.h"
#import "TMUserHashTagView.h"
#import "TMToastView.h"
#import "TMEditProfile.h"
#import "TMLog.h"
#import "TMSwiftHeader.h"
#import "MoEngage.h"
#import "TMAnalytics.h"
#import "TMRegisterManager.h"
#import "TMUserSession.h"
#import "TMDataStore.h"
#import "TMError.h"

#import "TMHashTagHintView.h"

#define kLEGAL @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

#define spaceBetweenTextAndTextField 20
#define biggerContainerHeight 140

#define IS_PHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_PHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568.0)

@interface TMHashTagTextViewController ()<UITextFieldDelegate,TMHashTagListViewDelegate, TMUserHashTagViewDelegate>

@property(nonatomic ,assign)BOOL showBar;
@property(nonatomic,strong)UILabel *descriptionLbl;
@property(nonatomic,strong)UITextField *hashTf;
@property(nonatomic,weak)NSTimer *autoCompleteTimer;
@property(nonatomic,strong)NSString *text;
@property(nonatomic,strong)NSString *searchText;
@property(nonatomic,assign)CGFloat currentKeyboardHeight;
@property(nonatomic ,assign)TMNavigationFlow navigationFlow;
@property(nonatomic,strong)UIView *biggerContainer;
@property(nonatomic,strong)UIView *hashTagContainer;
@property(nonatomic,strong)UIView *horizontalLine;
@property(nonatomic,strong)NSMutableArray* interestArr;
@property(nonatomic, strong)UIButton *continueButton;
@property(nonatomic, assign)CGFloat extraH;
@property(nonatomic, strong)UIActivityIndicatorView *activityIndicator;
@property(nonatomic, strong)NSMutableDictionary *registerData;
@property(nonatomic, strong)NSDate *startDate;

@property(nonatomic, strong)TMHashTagHintView *hashTagHintView;
@property(nonatomic, strong)NSArray *hintHashTagArr;
@property(nonatomic, assign)BOOL isHintShown;
@property(nonatomic, strong)UIImageView *rightView;
@property(nonatomic, strong)UIImageView *tipView;
@property(nonatomic, strong)NSTimer *idleTimer;

@end

@implementation TMHashTagTextViewController

-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow registerData:(NSMutableDictionary *)registerData showBar:(BOOL)showBar
{
    self = [super init];
    if(self){
        self.showBar = showBar;
        self.navigationFlow = navigationFlow;
        self.registerData = [[NSMutableDictionary alloc] init];
        self.registerData = registerData;
        [self hashtagDictionary];
        return self;
    }
    return nil;
}

- (void)viewDidLoad {
    
    TMLOG(@"screen height = %f", [[UIScreen mainScreen] bounds].size.height);
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"Describe yourself";
    
    if(self.showBar){
        self.navigationItem.hidesBackButton = true;
        self.navigationItem.title = @"Setting up your profile";
    }
    
    [[TMAnalytics sharedInstance] trackView:@"TMInterestViewController"];
    
    self.interestArr = [[NSMutableArray alloc] init];
    
    if(self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE){
        self.extraH = 64.0;
    }else {
        self.extraH = 0.0;
    }
    
    self.currentKeyboardHeight = 0;
    
    [self registerNotifcations];
    
    [self configureLowerUI];
    [self initializeUI];
    if(self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE || (self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION && !self.showBar))
    {
        [self loadPrefillData];
    }
    
    self.startDate = [NSDate date];

    // animate the bulb icon if user is idle for more than 5 sec
    if(!self.idleTimer && self.showBar){
        self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(animateBulbIcon) userInfo:nil repeats:NO];
    }
    [self trackRegistration:@"hashtag"];
}

-(void)loadPrefillData
{
    TMEditProfile *editProfileMngr = [[TMEditProfile alloc] init];
    if([editProfileMngr isNetworkReachable]) {
        NSDictionary *params = @{@"login_mobile":@"true"};
        
        editProfileMngr.trackEventDictionary = [self dictForEventTracking:@"edit_profile" action:@"page_load"];
        
        [self configureViewForDataLoadingStart];
        
        [editProfileMngr initCall:params with:^(NSDictionary *response, TMError *error) {
           [self configureViewForDataLoadingFinish];
           
            if(response) {
                NSDictionary *userData = response[@"user_data"];
                id hashtags = userData[@"hashtags"];
                if (hashtags && [hashtags isKindOfClass:[NSArray class]]) {
                    NSArray *hashtag = userData[@"hashtags"];
                    for (int i=0; i<hashtag.count; i++) {
                        [self.interestArr addObject:userData[@"hashtags"][i]];
                    }
                    [self addHashTagInContainer];
                }
            }
            else {
                //error case
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewAtError];
                }
                else {
                    //unknwon error
                    [self showRetryViewAtUnknownError];
                }
            }
        }];
        
        //[editProfileMngr updateBadWordList];
    }
    else {
        [self showRetryViewAtError];
    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadPrefillData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadPrefillData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)configureViewForDataLoadingStart
{
    TMLOG(@"configureViewForDataLoadingStart");
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading..."];
}

-(void)configureViewForDataLoadingFinish
{
    [self hideActivityIndicatorViewWithtranslucentView];
    TMLOG(@"configureViewForDataLoadingFinish");
    if(self.retryView.superview) {
        TMLOG(@"removing retry view");
        [self removeRetryView];
    }
}

-(void)initializeUI
{
    // bigger container that contain textfield , line and container for hashtag
    CGFloat extraHeight = 0.0;
    CGFloat yPos = 15.0;//(IS_PHONE_4) ? 15.0 : ((IS_PHONE_5) ? 22.0 : 44.0);
    if(self.showBar) {
        CGFloat yOffset = 15.0;//(IS_PHONE_4 || IS_PHONE_5) ? 10.0 : 20.0;
        extraHeight = 64.0;
        // progress bar
        CGFloat imgWidth = 60.0;//(IS_PHONE_4) ? 68.0 : 60.0;
        CGFloat imgHeight = 10.7;//(IS_PHONE_4) ? 12.0 : 10.7;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-imgWidth)/2, extraHeight + yOffset, imgWidth, imgHeight)];
        imageView.image = [UIImage imageNamed:@"Progress_4"];
        [self.view addSubview:imageView];
        
        yPos = imageView.frame.origin.y+imageView.frame.size.height + yOffset;
    }else {
        //yPos = (IS_PHONE_4 || IS_PHONE_5) ? 30.0 : 44.0;
        if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION){
            extraHeight = 64.0;
            yPos = yPos + extraHeight;
        }
    }
    
    CGFloat biggerContHeight = (IS_PHONE_4) ? 110 : biggerContainerHeight;
    
    self.biggerContainer = [[UIView alloc] initWithFrame:CGRectMake(20, yPos, self.view.bounds.size.width-40, biggerContHeight)];
    self.biggerContainer.layer.borderWidth = 1.0;
    self.biggerContainer.layer.borderColor = [[UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0] CGColor];
    self.biggerContainer.layer.cornerRadius = 10;
    [self.view addSubview:self.biggerContainer];
    
    // description text
    CGFloat width = self.biggerContainer.frame.size.width;
    
    UIFont *font =  (IS_PHONE_4 || IS_PHONE_5) ? [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0] : [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0];
    NSString *text = @"Describe yourself in atleast 3 words";//@"#Artsy? #Scholar? Tell us who you are in at least 3 #tags...";
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(width-30, NSUIntegerMax);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGFloat lblYPos = (IS_PHONE_4) ? 5.0 : 15.0;
    
    self.descriptionLbl = [[UILabel alloc] initWithFrame:CGRectMake((width-rect.size.width)/2, lblYPos, rect.size.width, rect.size.height)];
    self.descriptionLbl.text = text;
    self.descriptionLbl.numberOfLines = 0;
    self.descriptionLbl.font = font;
    self.descriptionLbl.textAlignment = NSTextAlignmentCenter;
    [self.descriptionLbl sizeToFit];
    self.descriptionLbl.textColor = [UIColor colorWithRed:68.0/255.0 green:68.0/255.0 blue:68.0/255.0 alpha:1.0];
    [self.biggerContainer addSubview:self.descriptionLbl];
    
    // textfield
    CGFloat textFeildSpace = (IS_PHONE_4) ? 5.0 : spaceBetweenTextAndTextField;
    
    self.hashTf = [[UITextField alloc] initWithFrame:CGRectMake(15, self.descriptionLbl.frame.origin.y+self.descriptionLbl.frame.size.height+textFeildSpace,width-60, 44)];
    self.hashTf.autocorrectionType = UITextAutocorrectionTypeNo;
    self.hashTf.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    self.hashTf.placeholder = @"Funny? #Reader? #GoGetter?";
    [self.biggerContainer addSubview:self.hashTf];
    self.hashTf.delegate = self;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 11, 44)];
    self.hashTf.leftView = paddingView;
    UILabel *paddingText = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 10, 44)];
    [paddingView addSubview:paddingText];
    paddingText.text = @"#";
    self.hashTf.leftViewMode = UITextFieldViewModeAlways;
    
    // horizontal line
    self.horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(15, self.hashTf.frame.origin.y+40, width-30, 0.5)];
    self.horizontalLine.backgroundColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
    [self.biggerContainer addSubview:self.horizontalLine];
    
    // container for hashtags
    self.hashTagContainer = [[UIView alloc] initWithFrame:CGRectMake(15, self.horizontalLine.frame.origin.y+self.horizontalLine.frame.size.height, width-30, 0)];
    [self.biggerContainer addSubview:self.hashTagContainer];
    
    
    // add image view for bulb
    self.rightView = [[UIImageView alloc] initWithFrame:CGRectMake(self.hashTf.frame.size.width+10, self.hashTf.frame.origin.y+2, 35, 35)];
    self.rightView.image = [UIImage imageNamed:@"bulb_icon"];
    self.rightView.userInteractionEnabled = YES;
    [self.biggerContainer addSubview:self.rightView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.rightView addGestureRecognizer:tapGesture];

}

-(void)configureLowerUI
{
    CGFloat spaceFromBottom = 34.0;
    self.continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.continueButton.frame = CGRectMake(15, self.view.frame.size.height-44-self.extraH-spaceFromBottom, self.view.bounds.size.width-30, 44);
    self.continueButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
    if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) {
        [self.continueButton setTitle:@"Next" forState:UIControlStateNormal];
    }else {
        [self.continueButton setTitle:@"Save" forState:UIControlStateNormal];
    }
    self.continueButton.layer.cornerRadius = 6;
    [self.continueButton addTarget:self action:@selector(didContinueClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.continueButton];
    
    [self disableButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(self.autoCompleteTimer) {
        [self.autoCompleteTimer invalidate];
        self.autoCompleteTimer = nil;
    }
    self.searchText = @"";
}

-(void)addOnReturn:(UITextField*)textField {
    TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
    if(textField.text.length >= 2) {
        NSString *updated = [@"#" stringByAppendingString:textField.text];
        if(!listview.requestInProgress) {
            [listview.delegate didSelectHashTagOption:updated];
        }
    }
    [self removeFavOptionListView];
}

-(void)removeFavOptionListView {
    self.hashTf.text = @"";
    [self.hashTf resignFirstResponder];
    
    TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
    [listview removeFromSuperview];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
 
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:kLEGAL] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
    if([string isEqualToString:filtered] && newLength<=17) {
        NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *tfText = textField.text;
        if(tfText.length > 0 && [trimmedString isEqualToString:@""] && range.length==1) {
            tfText = [tfText stringByReplacingCharactersInRange:NSMakeRange(tfText.length-1,1) withString:@""];
        }
        self.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        //look for valid string
        if((string) && (![trimmedString isEqualToString:@""])) {
            if(self.autoCompleteTimer) {
                [self.autoCompleteTimer invalidate];
                self.autoCompleteTimer = nil;
            }
            if(!self.autoCompleteTimer) {
                self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(searchHash) userInfo:nil repeats:NO];
            }
        }
        else if((self.text.length == 1) && ([trimmedString isEqualToString:@""])) {
            self.searchText = @"";
            TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
            listview.hidden = true;
            [listview emptyHashList];
            [listview removeSeparator];
        }
        else {
            if(self.autoCompleteTimer) {
                [self.autoCompleteTimer invalidate];
                self.autoCompleteTimer = nil;
            }
            if(!self.autoCompleteTimer) {
                self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(searchHash) userInfo:nil repeats:NO];
            }
        }
    }else {
        return false;
    }
    
    return true;
}

-(void)searchHash {
    self.text = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(self.text.length>1 && ![self.text isEqualToString:self.searchText]) {
        self.searchText = self.text;
        TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
        listview.hidden = false;
        [listview addSeparator];
        [listview fetchHashTag:self.text];
    }
}

-(void)didReceiveResponse {
    TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
    if(self.text.length==0) {
        [listview emptyHashList];
        [listview removeSeparator];
    }else {
        listview.hidden = false;
    }
}

-(void)didSelectHashTagOption:(NSString*)text {
    self.rightView.hidden = false;
    [self.interestArr removeObject:text];
    [self.interestArr addObject:text];
    
    [self removeFavOptionListView];
    
    [self addHashTagInContainer];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self removeIdleTimerFromView];
    self.rightView.hidden = true;
    [self.rightView.layer removeAnimationForKey:@"SpinAnimation"];
    if(self.hashTagHintView) {
        self.isHintShown = false;
        self.hashTagHintView.hidden = true;
        self.tipView.hidden = true;
    }
    [self showFavOptionListView];
}

-(void)showFavOptionListView {
    CGFloat xPos = self.biggerContainer.frame.origin.x + self.hashTf.frame.origin.x;
    CGFloat yPos= self.biggerContainer.frame.origin.y + self.hashTf.frame.origin.y + self.hashTf.frame.size.height;
    
    TMHashTagListView *listview = [[TMHashTagListView alloc] initWithFrame:CGRectMake(xPos, yPos, self.hashTf.frame.size.width+30, 280)];
    listview.tag = 500001;
    listview.delegate = self;
    listview.hidden = true;
    listview.layer.borderWidth = 0.3;
    listview.layer.borderColor = [[UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0] CGColor];
    [self.view addSubview:listview];
    [listview addSeparator];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    self.rightView.hidden = false;
    [textField resignFirstResponder];
    [self addOnReturn:textField];
    return YES;
}

-(void)registerNotifcations {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

-(void)deRegisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.currentKeyboardHeight = kbSize.height;
    [self setFavQuizOptionListViewHeightForKeyboardHeight];
}

-(void)keyboardDidHide:(NSNotification*)notification {
    self.currentKeyboardHeight = 0;
}

-(void)setFavQuizOptionListViewHeightForKeyboardHeight {
    
    TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
    CGRect rect = listview.frame;
    CGFloat height = self.view.frame.size.height - (rect.origin.y+self.currentKeyboardHeight);
    listview.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, height);
}

-(void)emptyTextField {
    self.hashTf.text = @"";
    TMHashTagListView *listview = (TMHashTagListView*)[self.view viewWithTag:500001];
    listview.hidden = true;
}

-(void)addHashTagInContainer
{
    CGFloat biggerContHeight = (IS_PHONE_4) ? 110 : biggerContainerHeight;
    // first reset the hashtagContainer and big container
    NSArray *subviews = [self.hashTagContainer subviews];
    for (int i=0; i<subviews.count; i++) {
        [subviews[i] removeFromSuperview];
    }
    CGRect hashtagContainerFrame = self.hashTagContainer.frame;
    hashtagContainerFrame.size.height = 0;
    self.hashTagContainer.frame = hashtagContainerFrame;
    
    CGRect biggerContainerFrame = self.biggerContainer.frame;
    biggerContainerFrame.size.height = biggerContHeight;//biggerContainerHeight;
    self.biggerContainer.frame = biggerContainerFrame;
    
    // add hashtag in container and set the frame of hashtag container , big container
    
    CGFloat xPos = 0; CGFloat yPos = 10;
    UIFont *font = (IS_PHONE_4 || IS_PHONE_5) ? [UIFont systemFontOfSize:14.0] : [UIFont systemFontOfSize:15.0];
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.hashTagContainer.frame.size.width, NSUIntegerMax);
    
    CGFloat widthOffset = (IS_PHONE_4 || IS_PHONE_5) ? 35 : 40;
    
    if (self.interestArr.count > 0 && self.interestArr.count <=5) {
        CGFloat hashtagContainerHeight = 50;
        for (int i=0; i<self.interestArr.count; i++) {
            CGRect rect = [self.interestArr[i] boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            CGFloat width = rect.size.width + widthOffset;
            
            if(xPos+width > self.hashTagContainer.frame.size.width) {
                xPos = 0;
                yPos = yPos + 40;
                hashtagContainerHeight = hashtagContainerHeight + 40;
            }
            
            TMUserHashTagView *hashtagView = [[TMUserHashTagView alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width + widthOffset, 30) withTitleText:self.interestArr[i] showCloseImage:true isSelected:true];
            hashtagView.hashTagDelegate = self;
            [self.hashTagContainer addSubview:hashtagView];
            xPos = xPos + width + 10;
        }
        
        CGRect hashtagContainerFrame = self.hashTagContainer.frame;
        hashtagContainerFrame.size.height = hashtagContainerHeight;
        self.hashTagContainer.frame = hashtagContainerFrame;
        
        CGRect biggerContainerFrame = self.biggerContainer.frame;
        biggerContainerFrame.size.height = self.hashTagContainer.frame.origin.y + self.hashTagContainer.frame.size.height;
        //biggerContainerFrame.origin.y = (IS_PHONE_4 || IS_PHONE_5 || self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE) ? biggerContainerFrame.origin.y : (self.view.bounds.size.height-biggerContainerFrame.size.height)/2;
        self.biggerContainer.frame = biggerContainerFrame;
    }
    
    if(self.interestArr.count >= 3){
        [self enableButtton];
    }else{
        [self disableButton];
    }
    
    if (self.interestArr.count >= 5 ) {
        [self startUpperAnimation];
    }
}

-(void)startUpperAnimation
{
    [UIView animateWithDuration:0.5
                          delay: 0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.hashTf.hidden = true;
                         self.rightView.hidden = true;
                         
                         CGRect newFrame = self.horizontalLine.frame;
                         newFrame.origin.y = self.descriptionLbl.frame.origin.y + self.descriptionLbl.frame.size.height + 10;
                         self.horizontalLine.frame = newFrame;
                         self.horizontalLine.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0];
                         
                         newFrame = self.hashTagContainer.frame;
                         newFrame.origin.y = self.horizontalLine.frame.origin.y + self.horizontalLine.frame.size.height;
                         self.hashTagContainer.frame = newFrame;
                         
                         newFrame = self.biggerContainer.frame;
                         newFrame.size.height = self.hashTagContainer.frame.origin.y + self.hashTagContainer.frame.size.height;
                         self.biggerContainer.frame = newFrame;
                         
                     }
                     completion:^(BOOL finished){ [self showToast]; }
     ];

}

-(void)startLowerAnimation
{
    [UIView animateWithDuration:0.5
                          delay: 0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.hashTf.hidden = false;
                         self.rightView.hidden = false;
                         
                         CGRect newFrame = self.horizontalLine.frame;
                         newFrame.origin.y = self.hashTf.frame.origin.y + self.hashTf.frame.size.height;
                         self.horizontalLine.frame = newFrame;
                         self.horizontalLine.backgroundColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
                         newFrame = self.hashTagContainer.frame;
                         newFrame.origin.y = self.horizontalLine.frame.origin.y + self.horizontalLine.frame.size.height;
                         self.hashTagContainer.frame = newFrame;
                         
                         newFrame = self.biggerContainer.frame;
                         newFrame.size.height = self.hashTagContainer.frame.origin.y + self.hashTagContainer.frame.size.height;
                         self.biggerContainer.frame = newFrame;
                         
                     }
                     completion:^(BOOL finished){ }
     ];
    
}


-(void)showToast {
    if(self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE) {
        [TMToastView showToastInParentViewWithEditProfileEnabled:self.view withText:@"You can only add upto 5 #tags. Remove some to add more." withDuaration:4.0 presentationDirection:TMToastPresentationFromTop];
    }else {
        [TMToastView showToastInParentView:self.view withText:@"You can only add upto 5 #tags. Remove some to add more." withDuaration:4.0 presentationDirection:TMToastPresentationFromTop];
    }
}

#pragma mark - TMUserHashTagViewDelegate Method

-(void)didTapOnHashTag:(NSString *)hashtag
{
    if(self.hashTagHintView) {
        self.isHintShown = false;
        self.hashTagHintView.hidden = true;
        self.tipView.hidden = true;
    }
    
    if(![self.interestArr containsObject:hashtag]){
        [self.interestArr addObject:hashtag];
    }else {
        BOOL showAnimation = false;
        if(self.interestArr.count >= 5){
            showAnimation = true;
        }
    
        [self.interestArr removeObject:hashtag];
        
        if(showAnimation){
            [self startLowerAnimation];
        }
    }
    [self addHashTagInContainer];
}

-(void)enableButtton
{
    self.continueButton.enabled = true;
    self.continueButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
    [self.continueButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
}

-(void)disableButton
{
    self.continueButton.enabled = false;
    self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
    [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
}

-(void)didContinueClick
{
    if(self.navigationFlow == TMNAVIGATIONFLOW_EDITPROFILE || (self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION && !self.showBar)){
        // update the data
        [self updateData];
    }else {
        // move to the new screen
        NSString *hashtagString = [self toStingFromArray];
        self.registerData[@"TM_interest"] = hashtagString;
        [self saveData];
    }
}

-(void)updateData{
    TMEditProfile *editProfileMngr = [[TMEditProfile alloc] init];
    if([editProfileMngr isNetworkReachable])
    {
        NSString *hashtagString = [self toStingFromArray];
        NSDictionary *params = @{@"login_mobile":@"true", @"param":@"interest", @"TM_interest":hashtagString};
        
        editProfileMngr.trackEventDictionary = [self dictForEventTracking:@"edit_profile" action:@"interest_server_save_call"];
        
        [self disableViewForUpdate];
        
        [editProfileMngr update:params with:^(NSString *response, TMError *error) {
            [self enableViewAfterUpdate];
            if(response) {
                // Moengage event
                [[MoEngage sharedInstance] trackEvent:@"Hashtag Save" andPayload:nil];
                if(self.delegate) {
                    [self.delegate didEditProfile];
                }
                [self.navigationController popViewControllerAnimated:true];
            }else {
                [self errorResponse:error];
            }
        }];
    }
    else
    {
        [self showAlertWithTitle:@"" msg:TM_INTERNET_NOTAVAILABLE_MSG withDelegate:nil];
    }
}

-(NSString *)toStingFromArray {
    
    NSString *stringForServer = @"";
    NSString *prefix = @"";
    stringForServer = [stringForServer stringByAppendingString:@"["];
    for (int i=0; i<self.interestArr.count; i++) {
        stringForServer = [stringForServer stringByAppendingString:prefix];
        prefix = @",";
        stringForServer = [stringForServer stringByAppendingString:@"\""];
        stringForServer = [stringForServer stringByAppendingString:[self.interestArr[i] stringByReplacingOccurrencesOfString:@"\"" withString:@"\'"]];
        stringForServer = [stringForServer stringByAppendingString:@"\""];
        
        // Moengage event
        NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"value":self.interestArr[i]}];
        [[MoEngage sharedInstance] trackEvent:@"user hashtags" andPayload:eventDict];
    }
    stringForServer = [stringForServer stringByAppendingString:@"]"];

    return stringForServer;
    
}

-(void)disableViewForUpdate
{
    self.view.userInteractionEnabled = false;
    [self disableButton];
    if(!self.activityIndicator) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.continueButton.frame.origin.x+80, self.continueButton.frame.origin.y+(self.continueButton.frame.size.height-20)/2, 20, 20)];
        self.activityIndicator.color = [UIColor colorWithRed:0.463 green:0.463 blue:0.463 alpha:1.0];
        [self.view addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
    }
}

-(void)enableViewAfterUpdate
{
    self.view.userInteractionEnabled = true;
    [self enableButtton];
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    self.activityIndicator = nil;
}

-(NSMutableDictionary *)dictForEventTracking:(NSString *)category action:(NSString *)action
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"screenName":@"TMInterestViewController", @"eventCategory":category, @"eventAction":action}];
    return eventDict;
}

-(void)saveData
{
    TMRegisterManager *registerMgr = [[TMRegisterManager alloc] init];
    
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"screenName":@"TMEduViewController", @"eventCategory":@"register_basics", @"eventAction":@"register_server_save_call"}];
    
    registerMgr.trackEventDictionary = eventDict;
    
    if([registerMgr isNetworkReachable]) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        [userSession setRegisterCookieData:self.registerData];
       
        [self disableViewForUpdate];
        NSDictionary *params = @{@"login_mobile":@"true",@"save":@"basics"};
        
        [registerMgr saveBasics:params with:^(NSString *flag, TMError *error) {
            [self enableViewAfterUpdate];
            if(flag && [flag isEqualToString:@"yes"]){
                                // Moengage event
                [self trackRegisterSaveEvent];
                [[MoEngage sharedInstance] trackEvent:@"Registration Complete" andPayload:nil];
                [userSession deleteRegisterCookieData];
                [TMDataStore setObject:[[NSNumber alloc] initWithBool:true] forKey:@"fromRegisteration"];
                [TMDataStore removeObjectforKey:@"fb_designation"];
                [self.delegate popToRootViewControllerAndMoveToViewControllerWithId:@"myProfile" animated:true];
            }
            else {
                [self errorResponse:error];
            }
        }];
        
    }else {
        [self showAlertWithTitle:@"" msg:TM_INTERNET_NOTAVAILABLE_MSG withDelegate:nil];
    }
}

-(void)errorResponse:(TMError *)err
{
    if(err.errorCode == TMERRORCODE_LOGOUT) {
        if(self.actionDelegate !=  nil) {
            [self.actionDelegate setNavigationFlowForLogoutAction:false];
        }
    }else if(err.errorCode == TMERRORCODE_NONETWORK) {
        [self showAlertWithTitle:@"" msg:TM_INTERNET_NOTAVAILABLE_MSG withDelegate:nil];
    }else if(err.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showAlertWithTitle:TM_SERVER_TIMEOUT_MSG msg:@"" withDelegate:nil];
    }
    else if(err.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self showAlertWithTitle:err.errorMessage msg:@"" withDelegate:nil];
    }else {
        [self showAlertWithTitle:@"Please Try Later" msg:@"Problem in saving the data" withDelegate:nil];
    }
}

-(void)trackRegisterSaveEvent {
     [[TMAnalytics sharedInstance] trackFacebookEvent:@"registration" withParams:@{@"register_method":@"register_server_save_call"}];
}

-(void)trackRegistration:(NSString *)eventAction {
    NSDictionary *eventDict = @{@"screenName":@"TMNewRegisterBasicsViewController", @"eventCategory":@"register_basics", @"eventAction":eventAction, @"status":@"success", @"startDate":self.startDate, @"endDate":[NSDate date]};
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    [[TMAnalytics sharedInstance] trackFacebookEvent:@"registration" withParams:@{@"register_method":@"hashtag"}];
}

#pragma mark hint view 

-(void)hashtagDictionary {
    if(!self.hintHashTagArr) {
        NSMutableArray *randomArr;
        if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
            NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"TMhashtag" ofType:@"json"];
            NSData *dta = [NSData dataWithContentsOfFile:file];
            NSError *error = nil;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
            NSArray* response = [json valueForKey:@"hashtag"];
            NSMutableArray *tmpArr = [[NSMutableArray alloc] initWithArray:response];
            randomArr = [[NSMutableArray alloc] init];
            for (int i=0; i<12; i++) {
                int index = arc4random_uniform((int)tmpArr.count);
                NSString *tag = tmpArr[index];
                [tmpArr removeObjectAtIndex:index];
                [randomArr addObject:tag];
            }
        }
        else {
            randomArr = (NSMutableArray*)@[@"#FOB",@"#whitewashed",@"#Coconut",@"#BornInTheUSA",@"#BornInIndia",
                                           @"#TrulyDesi",@"#BhangraLover",@"#CurryCrazy",@"#ButterChickenLover",
                                           @"#DesiAtHeart",@"#NewDelhiToNewYork",@"#BollywoodBuff",@"#DesiMusicLover",
                                           @"#NorthIndian",@"#SouthIndian"];
        }
        self.hintHashTagArr = [[NSArray alloc] initWithArray:randomArr];
    }
}

-(void)addHintHashTagView {
    [self removeIdleTimerFromView];
    
    [self.rightView.layer removeAnimationForKey:@"SpinAnimation"];
    
    self.isHintShown = !self.isHintShown;
    [self.hashTf resignFirstResponder];
    if(!self.hashTagHintView) {
        CGFloat yPos = self.biggerContainer.frame.origin.y + CGRectGetMaxY(self.rightView.frame);
        CGRect rect = CGRectMake(30, yPos+15, self.view.bounds.size.width-60, CGRectGetMinY(self.continueButton.frame)-yPos-20);
        self.hashTagHintView = [[TMHashTagHintView alloc] initWithFrame:rect hintHashTag:self.hintHashTagArr selectedHashTag:self.interestArr];
        self.hashTagHintView.delegate = self;
        [self.view addSubview:self.hashTagHintView];
        
        CGFloat xPos = CGRectGetMinX(self.biggerContainer.frame)+CGRectGetMinX(self.rightView.frame)+(self.rightView.frame.size.width-20)/2;
        self.tipView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, 20, 20)];
        self.tipView.backgroundColor = [UIColor clearColor];
        self.tipView.image = [UIImage imageNamed:@"Tooltip"];
        
        // drop shadow
//        [self.tipView.layer setShadowColor:[UIColor blackColor].CGColor];
//        [self.tipView.layer setShadowOpacity:0.4];
//        [self.tipView.layer setShadowRadius:1.0];
//        [self.tipView.layer setShadowOffset:CGSizeMake(0.0, -2.0)];
        
        [self.view addSubview:self.tipView];
    }
    self.hashTagHintView.hidden = !self.isHintShown;
    self.tipView.hidden = !self.isHintShown;
    if(self.isHintShown){
        [self.hashTagHintView refreshList:self.interestArr];
    }
}

-(void)tapAction {
    [self addHintHashTagView];
}

-(void)animateBulbIcon {
    if(self.rightView && !self.rightView.hidden){
        // animate the right view
        CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [NSNumber numberWithFloat:( -20* M_PI  / 180)];
        animation.toValue = [NSNumber numberWithFloat: ( 20 * M_PI  / 180)];
        animation.duration = 0.2f;
        animation.repeatCount = INFINITY;
        animation.autoreverses = true;
        [self.rightView.layer addAnimation:animation forKey:@"SpinAnimation"];
    }
    [self removeIdleTimerFromView];
}

-(void)removeIdleTimerFromView {
    if(self.idleTimer){
        [self.idleTimer invalidate];
        self.idleTimer = nil;
    }
}

-(void)removeAnimation {
    
}
@end

//
//  TMUserCitySelectionView.m
//  TrulyMadly
//
//  Created by Ankit on 08/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMUserCitySelectionView.h"

@interface TMUserCitySelectionView ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property(nonatomic,strong)UISearchBar* citySearchBar;
@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong)UILabel* headerLabel;
@property(nonatomic,strong)UIView* headerView;

@property(nonatomic,strong)NSArray* cityList;
@property(nonatomic,strong)NSArray* popularCityList;
@property(nonatomic,strong)NSArray* filteredCityList;

@property(nonatomic,assign)BOOL showTableviewHeader;

@end


@implementation TMUserCitySelectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        CGFloat xPos = 0;
        self.citySearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(xPos, 40, CGRectGetWidth(self.frame)-(2*xPos), 40)];
        self.citySearchBar.barStyle = UIBarStyleDefault;
        self.citySearchBar.showsCancelButton = TRUE;
        self.citySearchBar.showsSearchResultsButton = FALSE;
        self.citySearchBar.delegate = self;
        [self addSubview:self.citySearchBar];
    
        [self setupTableView];
        
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
    }
    return self;
}
-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat xPos = 0;
    self.citySearchBar.frame = CGRectMake(xPos, 40, CGRectGetWidth(self.frame)-(2*xPos), 40);
    
    CGFloat yPos = CGRectGetMaxY(self.citySearchBar.frame);
    CGFloat height = CGRectGetHeight(self.frame) - yPos;
    self.tableView.frame = CGRectMake(0, yPos, CGRectGetWidth(self.frame), height);
}
-(void)setupTableView {
    CGFloat yPos = CGRectGetMaxY(self.citySearchBar.frame);
    CGFloat height = CGRectGetHeight(self.frame) - yPos;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, yPos, CGRectGetWidth(self.frame), height) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubview:self.tableView];
}
-(UIView*)headerView {
    if(!_headerView) {
        CGFloat maxWidth = CGRectGetWidth(self.tableView.frame);
        CGFloat headerViewHeight = 50;
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, maxWidth, headerViewHeight)];
        _headerView.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1];
        _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, maxWidth-40, 30)];
        _headerLabel.text = @"Popular Cities";
        _headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
        [_headerView addSubview:_headerLabel];
    }
    return _headerView;
}
-(void)reloadData {
    [self.tableView reloadData];
}
-(void)loadCityList:(NSArray*)cityList popularCityList:(NSArray*)popularCityList {
    self.cityList = cityList;
    self.filteredCityList = popularCityList;
    self.popularCityList = popularCityList;
    self.showTableviewHeader = TRUE;
    [self.citySearchBar becomeFirstResponder];
}

#pragma mark - UITableView Datasource / Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredCityList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(self.showTableviewHeader) {
        return 50;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reusableIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        cell.textLabel.textColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1];
    }
    
    NSDictionary* cityData = self.filteredCityList[indexPath.row];
    cell.textLabel.text = cityData[@"cityname"];
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary* cityData = self.filteredCityList[indexPath.row];
    [self.delegate didSelectCity:cityData];
}

#pragma mark - UITextField Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
   // NSLog(@"searchBarTextDidBeginEditing");
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    //NSLog(@"searchBarTextDidEndEditing");
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadTableViewForFilteredText:searchText];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
   // NSLog(@"searchBarCancelButtonClicked");
    [searchBar resignFirstResponder];
    [self.delegate didCancelCitySearch];
}


-(void)reloadTableViewForFilteredText:(NSString*)filteredText {
    if(![filteredText isEqualToString:@""]) {
        self.showTableviewHeader = FALSE;
        //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"first_name like[cd] %@",[NSString stringWithFormat:@"*%@*",inFilterText]];
        NSString* formattedFilteredText = [NSString stringWithFormat:@"%@*",filteredText];
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(cityname LIKE[cd] %@)", formattedFilteredText];
        NSArray* filteredArray = [self.cityList filteredArrayUsingPredicate:predicate];
        self.filteredCityList = filteredArray;
    }
    else {
        self.filteredCityList = self.popularCityList;
        self.showTableviewHeader = TRUE;
    }
    
    [self.tableView reloadData];
}

-(void)keyboardOnScreen:(NSNotification *)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [keyboardFrameBegin CGRectValue];
    CGFloat yPos = CGRectGetMinY(self.tableView.frame);
    CGFloat availableHeight = CGRectGetHeight(self.frame) - CGRectGetHeight(keyboardRect);
    CGFloat height = availableHeight - yPos;
    self.tableView.frame = CGRectMake(CGRectGetMinX(self.tableView.frame),
                                      CGRectGetMinY(self.tableView.frame),
                                      CGRectGetWidth(self.tableView.frame),
                                      height);
}

@end


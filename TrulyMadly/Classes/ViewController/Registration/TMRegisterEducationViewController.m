//
//  TMRegisterEducationViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMRegisterEducationViewController.h"
#import "TMHashTagTextViewController.h"
#import "TMUserDataController.h"
#import "TMResgistrationBasicDataView.h"
#import "TMAnalytics.h"

@interface TMRegisterEducationViewController()<UIPickerViewDataSource, UIPickerViewDelegate, TMResgistrationBasicDataViewDelegate>

@property(nonatomic, strong)NSMutableDictionary *registerData;
@property(nonatomic, strong)UIButton *continueButton;
@property(nonatomic, strong)NSArray *allDegrees;
@property(nonatomic, assign)NSInteger indexForDegree;
@property(nonatomic, strong)UIPickerView *picker;
@property(nonatomic, strong)UIView *pickerSeparator;
@property(nonatomic, strong)UIButton *button;
@property(nonatomic, strong)NSDate *startDate;

@end

@implementation TMRegisterEducationViewController

-(instancetype)initWithRegisterData:(NSMutableDictionary *)registerData
{
    self = [super init];
    if(self){
        self.allDegrees = [[NSMutableArray alloc] init];
        self.registerData = [[NSMutableDictionary alloc] init];
        self.registerData = registerData;
        return self;
    }
    return nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.navigationController setNavigationBarHidden:false];
    self.navigationItem.hidesBackButton = true;
    self.navigationItem.title = @"Setting up your profile";
    
    // progress bar
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-60)/2, 64.0+15.0, 60, 10.7)];
    imageView.image = [UIImage imageNamed:@"Progress_3"];
    [self.view addSubview:imageView];
    
    TMUserDataController* userDataController = [[TMUserDataController alloc] init];
    [userDataController getDegreeData:^(NSArray * _Nonnull degreeList) {
        self.allDegrees = degreeList;
    }];
    
    self.indexForDegree = 0;
    
    CGRect rect = CGRectMake(20, imageView.frame.origin.y+imageView.frame.size.height+15.0,
                             self.view.bounds.size.width-40, self.view.bounds.size.height);
    TMResgistrationBasicDataView* regBasicDataView = [[TMResgistrationBasicDataView alloc] initWithFrame:rect
                                                                                                    step:EDUCATION
                                                                                        isLocationFilled:FALSE];
    regBasicDataView.delegate = self;
    [self.view addSubview:regBasicDataView];
    
    [self initializeView];
    
    self.startDate = [NSDate date];
}

-(void)initializeView
{
    [self createContinueButton];
    
    self.picker = [[UIPickerView alloc] init];
    CGRect pickerFrame = self.picker.frame;
    pickerFrame.size.height = self.picker.frame.size.height-54;
    pickerFrame.origin.y = self.view.bounds.size.height - pickerFrame.size.height;
    pickerFrame.origin.x = 0;
    pickerFrame.size.width = self.view.bounds.size.width;
    self.picker.frame = pickerFrame;
    self.picker.backgroundColor = [UIColor whiteColor];
    self.picker.hidden = true;
    self.picker.dataSource = self;
    self.picker.delegate = self;
    [self.view addSubview:self.picker];
    
    self.pickerSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, self.picker.frame.origin.y-50, self.view.bounds.size.width, 50)];
    self.pickerSeparator.backgroundColor = [UIColor colorWithRed:0.922 green:0.922 blue:0.922 alpha:1.0];
    self.pickerSeparator.hidden = true;
    [self.view addSubview:self.pickerSeparator];
    
    UIButton *pickerCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    pickerCancel.frame = CGRectMake(0, 0, 80, 50);
    [pickerCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    pickerCancel.titleLabel.font = [UIFont systemFontOfSize:18];
    [pickerCancel addTarget:self action:@selector(hidePicker) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerSeparator addSubview:pickerCancel];
    
    UIButton *pickerDone = [UIButton buttonWithType:UIButtonTypeSystem];
    pickerDone.frame = CGRectMake(self.view.bounds.size.width-75, 0, 80, 50);
    [pickerDone setTitle:@"Done" forState:UIControlStateNormal];
    pickerDone.titleLabel.font = [UIFont systemFontOfSize:18];
    [pickerDone addTarget:self action:@selector(donePicker) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerSeparator addSubview:pickerDone];
    
}

#pragma mark UIPickerView DataSource methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.allDegrees.count;
}

#pragma mark UIPickerView Delegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.allDegrees[row][@"value"];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.indexForDegree = row;
}

-(void)hidePicker
{
    self.picker.hidden = true;
    self.pickerSeparator.hidden = true;
}

-(void)donePicker
{
    [self updateDegree:self.indexForDegree];
}

-(void)createContinueButton
{
    self.continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.continueButton.frame = CGRectMake(15, self.view.frame.size.height-88, self.view.bounds.size.width-30, 44);
    self.continueButton.titleLabel.font = [UIFont systemFontOfSize:15.0 ];
    [self.continueButton setTitle:@"Next" forState:UIControlStateNormal];
    self.continueButton.layer.cornerRadius = 6;
    [self.continueButton addTarget:self action:@selector(didContinueClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.continueButton];
    [self disableButton];
}

-(void)didContinueClick
{
    [self trackRegistration:@"education"];
    TMHashTagTextViewController *hashView = [[TMHashTagTextViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION registerData:self.registerData showBar:true];
    hashView.delegate = self.delegate;
    hashView.actionDelegate = self.actionDelegate;
    [self.navigationController pushViewController:hashView animated:true];
}

-(void)enableButtton
{
    self.continueButton.enabled = true;
    self.continueButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
    [self.continueButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
}

-(void)disableButton
{
    self.continueButton.enabled = false;
    self.continueButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
    [self.continueButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
}

-(void)updateDegree:(NSInteger)row {
    self.indexForDegree = row;
    self.picker.hidden = true;
    self.pickerSeparator.hidden = true;
    self.registerData[@"TM_Education_0"] = self.allDegrees[row][@"key"];
    [self.button setTitle:self.allDegrees[row][@"value"] forState:UIControlStateNormal];
    [self.button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self enableButtton];
}

#pragma mark TMBasicsDeleaget Methods

-(void)didButtonClick:(NSString *)text sender:(UIButton *)sender
{
    self.button = sender;
    self.picker.hidden = false;
    self.pickerSeparator.hidden = false;
    [self.picker reloadAllComponents];
    [self.picker selectRow:self.indexForDegree inComponent:0 animated:false];
    [self.view bringSubviewToFront:self.picker];
    [self.view bringSubviewToFront:self.pickerSeparator];
}

-(void)trackRegistration:(NSString *)eventAction {
    NSDictionary *eventDict = @{@"screenName":@"TMNewRegisterBasicsViewController", @"eventCategory":@"register_basics", @"eventAction":eventAction, @"status":@"success", @"startDate":self.startDate, @"endDate":[NSDate date]};
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    [[TMAnalytics sharedInstance] trackFacebookEvent:@"registration" withParams:@{@"register_method":@"education"}];
}

@end

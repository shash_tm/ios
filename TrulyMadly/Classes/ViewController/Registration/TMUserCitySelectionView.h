//
//  TMUserCitySelectionView.h
//  TrulyMadly
//
//  Created by Ankit on 08/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMUserCitySelectionViewDelegate;

@interface TMUserCitySelectionView : UIView

@property(nonatomic,weak)id<TMUserCitySelectionViewDelegate> delegate;

-(void)loadCityList:(NSArray*)cityList popularCityList:(NSArray*)popularCityList;

@end


@protocol TMUserCitySelectionViewDelegate <NSObject>

-(void)didCancelCitySearch;
-(void)didSelectCity:(NSDictionary*)cityData;

@end

//
//  TMNativeAdWebViewController.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 10/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMNativeAdWebViewController : UIViewController
- (instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil urlString:(NSString*) urlString webViewTitle:(NSString*) webViewTitle;
@end

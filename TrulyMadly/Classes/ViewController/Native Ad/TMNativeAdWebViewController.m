//
//  TMNativeAdWebViewController.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 10/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMNativeAdWebViewController.h"
#import "TMAnalytics.h"
#import "TMCommonUtil.h"

@interface TMNativeAdWebViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *webViewTitleLabel;
@property (strong, nonatomic) NSMutableDictionary* eventDictionary;

@end

@implementation TMNativeAdWebViewController

- (instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil urlString:(NSString*) urlString webViewTitle:(NSString*) webViewTitle {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
        self.view.backgroundColor = [UIColor clearColor];
        
        [self.webView setScalesPageToFit:YES];
        [self.webView setBackgroundColor:[UIColor clearColor]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request addValue:@"iOSApp" forHTTPHeaderField:@"source"];
        [request addValue:@"iOS" forHTTPHeaderField:@"app"];
        [request addValue:[TMCommonUtil buildNumberString] forHTTPHeaderField:@"app_version_code"];
        
        [self.webView loadRequest:request];
        
        //set title
        self.webViewTitleLabel.text = webViewTitle;
        
    }
    return self;
}

- (void) hideActivityIndicator {
    //hide activity indicator
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
}

#pragma mark - UIWebView delegate methods
- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //tracking event
    self.eventDictionary = [[NSMutableDictionary alloc] init];
    [self.eventDictionary setObject:@"webview_closed" forKey:@"label"];
    [self.eventDictionary setObject:@"webview_closed" forKey:@"status"];
    [self.eventDictionary setObject:@"conversation_list" forKey:@"eventCategory"];
    [self.eventDictionary setObject:@"false" forKey:@"append403"];
    [self.eventDictionary setObject:@"ad_native" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDictionary];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = false;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self hideActivityIndicator];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self hideActivityIndicator];
}

@end

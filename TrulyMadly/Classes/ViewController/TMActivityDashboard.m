//
//  TMActivityDashboard.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 15/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMActivityDashboard.h"

@implementation TMActivityDashboard

-(void)setActivityDashboard:(NSDictionary *)data {
    self.popularity = [data[@"popularity"] integerValue];
    self.noOfView = [data[@"activity"] integerValue];
    self.intro_text = [NSString stringWithFormat:@"%@", data[@"intro_text"]];
    self.popularity_text = [NSString stringWithFormat:@"%@", data[@"popularity_text"]];
    self.activity_text = [NSString stringWithFormat:@"%@", data[@"activity_text"]];
}

@end

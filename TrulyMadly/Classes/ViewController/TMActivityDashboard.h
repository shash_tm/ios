//
//  TMActivityDashboard.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 15/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMActivityDashboard : NSObject

@property(nonatomic,assign)NSInteger popularity;
@property(nonatomic,strong)NSString* intro_text;
@property(nonatomic,strong)NSString* popularity_text;
@property(nonatomic,assign)NSInteger noOfView;
@property(nonatomic,strong)NSString* activity_text;

-(void)setActivityDashboard:(NSDictionary*)data;

@end

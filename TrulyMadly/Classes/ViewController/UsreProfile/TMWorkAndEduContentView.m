//
//  TMWorkAndEduContentView.m
//  TrulyMadly
//
//  Created by Ankit on 18/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMWorkAndEduContentView.h"
#import "TMProfileWorkAndEdu.h"
#import "TMStringUtil.h"
#import "TMLog.h"

@implementation TMWorkAndEduContentView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}

-(void)layoutWorkContent:(TMProfileWorkAndEdu*)profileWorkAndEdu {
    [profileWorkAndEdu workIconLayoutFrame:^(CGRect frame) {
        //TMLOG(@"icon block callled:%@",NSStringFromCGRect(frame));
        UIImageView *workImgView = [[UIImageView alloc] initWithFrame:frame];
        workImgView.image = [UIImage imageNamed:@"work"];
        //workImgView.backgroundColor = [UIColor yellowColor];
        [self addSubview:workImgView];
        
    } workDescriptionLayoutFrame:^(CGRect frame, NSString *content) {
        //TMLOG(@"work label with content:%@ frame:%@ block callled",content,NSStringFromCGRect(frame));
        UILabel *contentLabel = [self contentLabelWithFrame:frame
                                                withContent:content
                                                   withFont:[profileWorkAndEdu titleTextFont]
                                              withTextColor:[UIColor blackColor]
                                                 isSelfEdit:profileWorkAndEdu.isSelfEdit];
        [self addSubview:contentLabel];
        
        
    } companiesLayoutFrame:^(CGRect frame, NSString *content) {
        //TMLOG(@"companies label with content:%@ frame:%@ block callled",content,NSStringFromCGRect(frame));
        UILabel *contentLabel = [self contentLabelWithFrame:frame
                                                withContent:content
                                                   withFont:[profileWorkAndEdu subtitleTextFont]
                                              withTextColor:[UIColor blackColor]
                                                 isSelfEdit:profileWorkAndEdu.isSelfEdit];
        [self addSubview:contentLabel];
    }];
}

-(void)layoutEducationContent:(TMProfileWorkAndEdu*)profileWorkAndEdu {
    [profileWorkAndEdu eduIconLayoutFrame:^(CGRect frame) {
        //TMLOG(@"icon block callled:%@",NSStringFromCGRect(frame));
        UIImageView *eduImgView = [[UIImageView alloc] initWithFrame:frame];
        eduImgView.image = [UIImage imageNamed:@"education"];
        [self addSubview:eduImgView];
        //eduImgView.backgroundColor = [UIColor redColor];
        
    } eduDescriptionLayoutFrame:^(CGRect frame, NSString *content) {
        //TMLOG(@"edu label with content:%@ frame:%@ block callled",content,NSStringFromCGRect(frame));
        UILabel *contentLabel = [self contentLabelWithFrame:frame
                                                withContent:content
                                                   withFont:[profileWorkAndEdu titleTextFont]
                                              withTextColor:[UIColor blackColor]
                                                 isSelfEdit:profileWorkAndEdu.isSelfEdit];
        [self addSubview:contentLabel];

        
    } instituesLayoutFrame:^(CGRect frame, NSString *content) {
        //TMLOG(@"institutes label with content:%@ frame:%@ block callled",content,NSStringFromCGRect(frame));
        UILabel *contentLabel = [self contentLabelWithFrame:frame
                                                withContent:content
                                                   withFont:[profileWorkAndEdu subtitleTextFont]
                                              withTextColor:[UIColor blackColor]
                                                 isSelfEdit:profileWorkAndEdu.isSelfEdit];
        [self addSubview:contentLabel];
    }];
}

-(UILabel*)contentLabelWithFrame:(CGRect)frame withContent:(NSString*)content withFont:(UIFont*)font withTextColor:(UIColor*)color isSelfEdit:(BOOL)isSelfEdit
{
    UILabel *lb = [[UILabel alloc] initWithFrame:frame];
    lb.font = font;
    if(isSelfEdit && ([content isEqualToString:@"Add Income"] || [content isEqualToString:@" Add Income"] || [content isEqualToString:@"Add Workplace"] || [content isEqualToString:@"Add Institute"] )) {
        lb.textColor = [UIColor colorWithRed:0.831 green:0.624 blue:0.0 alpha:1.0];
    }
    else {
        lb.textColor = color;
    }
    lb.numberOfLines = 0;
    lb.textAlignment = NSTextAlignmentLeft;
    lb.backgroundColor = [UIColor clearColor];
    lb.text = content;
    return lb;
}

@end

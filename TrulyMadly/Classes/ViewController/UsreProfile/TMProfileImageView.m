//
//  TMProfileImageView.m
//  TrulyMadly
//
//  Created by Ankit on 18/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileImageView.h"
#import "TMImageDownloader.h"
#import "TMLog.h"
#import "UIImage+TMAdditions.h"
#import "UIImageView+AFNetworking.h"


@interface TMProfileImageView ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)NSString *imageURLString;
@property(nonatomic,assign)BOOL isImageDownloadStarted;
@property(nonatomic,strong)CAGradientLayer *gradient;
@property(nonatomic,strong)NSString *cachePath;

@end


@implementation TMProfileImageView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.scaleImage = FALSE;
        self.isImageDownloadStarted = FALSE;
        self.clearCache = TRUE;
        self.clearCacheAtPath = FALSE;
        self.retryImageDownloadOnFailure = FALSE;
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.clipsToBounds = TRUE;
        [self addSubview:self.imageView];
    }
    return self;
}

-(void)dealloc {
    TMLOG(@":---TMProfileImageView -- dealloc");
    [self removeNotificationObserver];
    [self deregisterForImageDownload];
    if(self.imageView.image && self.clearCache) {
        [self clearImageCache];
    }
    if(self.clearCacheAtPath && self.cachePath) {
        [self clearImageCacheAtCurrentPath];
    }
    self.imageURLString = nil;
    self.imageView = nil;
    self.imageView.image = nil;
}

-(void)configureNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadCompleteAction:) name:TMImageDownloadCompleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadFailAction:) name:TMImageDownloadFailNotification object:nil];
}
-(void)removeNotificationObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Public Fucntion
#pragma mark -

-(void)downloadAndSetImageFromURL:(NSURL*)URL {
    [self.imageView setImageWithURL:URL];
}
-(void)setImageContentMode:(UIViewContentMode)contentMode {
    self.imageView.contentMode = contentMode;
}
-(void)setImageFromURLString:(NSString*)URLString {
    //TMLOG(@"____IP:Image Download URL Set to:%@",URLString);
    self.imageURLString = URLString;
}
-(void)setImageFromURLString:(NSString *)URLString isDeal:(BOOL)isDeal {
    //NSURL *url = [NSURL URLWithString:URLString];
    self.imageURLString = URLString;
    self.imageView.contentMode = UIViewContentModeScaleToFill;
    self.imageView.image = [self getPlaceholderImage];
    if (isDeal) {
        if (!self.gradient.superlayer) {
            self.gradient = [CAGradientLayer layer];
            self.gradient.frame = self.imageView.bounds;
            self.gradient.colors = @[(id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.80] CGColor],
                                     (id)[[UIColor colorWithRed:149.0/255.0 green:126.0/255.0 blue:104.0/255.0 alpha:0.25] CGColor],
                                     (id)[[UIColor colorWithRed:38.0/255.0 green:29.0/255.0 blue:21.0/255.0 alpha:0.80] CGColor]];
            self.gradient.locations = @[@0.0f, @0.5f, @1.0f];
            [self.imageView.layer insertSublayer:self.gradient atIndex:0];
        }
    }
    
    [self configureNotification];
    TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
    [imageDownloader downloadImageFromUrl:[NSURL URLWithString:self.imageURLString]];
}
-(void)setImageFromURLStringWithBackgroundGradient:(NSString *)URLString isDarkCentered:(BOOL) isDarkCentered {

    self.imageURLString = URLString;
    if (!self.gradient.superlayer) {
        self.gradient = [CAGradientLayer layer];
        self.gradient.frame = self.imageView.bounds;
        self.gradient.colors = isDarkCentered?(@[(id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.25] CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.43]CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.62]CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.71]CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.80] CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.71]CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.62] CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.43]CGColor],
                                                 (id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.25] CGColor]]):(@[(id)[[UIColor colorWithRed:64.0/255.0 green:50.0/255.0 blue:36.0/255.0 alpha:0.80] CGColor],
                                                                                                                                                  (id)[[UIColor colorWithRed:149.0/255.0 green:126.0/255.0 blue:104.0/255.0 alpha:0.25] CGColor],
                                                                                                                                                  (id)[[UIColor colorWithRed:38.0/255.0 green:29.0/255.0 blue:21.0/255.0 alpha:0.80] CGColor]]);
        self.gradient.locations = isDarkCentered?(@[@0.0f,@0.12f,@0.25f,@0.37f,@0.5f,@0.62f,@0.75f, @0.87f,@1.0f]):(@[@0.0f,@0.5f,@1.0f]);

        [self.imageView.layer insertSublayer:self.gradient atIndex:0];
    }
}

-(void)setImageFromURLStringWithGradientForProfile:(NSString *)URLString {
    self.imageURLString = URLString;
    if (!self.gradient.superlayer) {
        self.gradient = [CAGradientLayer layer];
        self.gradient.frame = self.imageView.bounds;
        self.gradient.colors = @[(id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0] CGColor],
                                 (id)[[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.68] CGColor]];
        self.gradient.locations = @[@0.65f, @1.0f];
        [self.imageView.layer insertSublayer:self.gradient atIndex:0];
    }
}

-(void)forceStartImageDownloadWithRelativePath:(NSString*)relativePath showLoader:(BOOL)showLoader {
    self.cachePath = relativePath;
    [self configureNotification];
    if(showLoader) {
        [self showActivityIndicatorViewWithMessage:@"Loading Image..."];
    }
    TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
    [imageDownloader downloadImageFromUrl:[NSURL URLWithString:self.imageURLString] cacheAtPath:relativePath];
}
-(void)resetCurrentImage {
    self.imageView.image = nil;
}
-(void)setDefaultImage {
    self.imageView.image = [UIImage imageNamed:@"ProfileBlank"];
}
-(void)setDefaultImage:(UIImage*)image {
    self.imageView.image = image;
}

#pragma mark Notification Handler
#pragma mark -

-(void)imageDownloadCompleteAction:(NSNotification*)notification {
    if([self canPerformActionOnNotification:notification]) {
        [self didReceiveImageDownloadNotification:notification withStatus:TRUE];
    }
}
-(void)imageDownloadFailAction:(NSNotification*)notification {
    if([self canPerformActionOnNotification:notification]) {
       [self didReceiveImageDownloadNotification:notification withStatus:FALSE];
    }
}
-(void)didReceiveImageDownloadNotification:(NSNotification*)notification withStatus:(BOOL)status {
    [self  hideActivityIndicatorView];
    [self removeNotificationObserver];
    if(status) {
        [self setImageFromNotification:notification];
    }
    else if(self.retryImageDownloadOnFailure) {
        //
        TMLOG(@"Image Download Failure:Retrying Again");
    }
}

-(void)deregisterForImageDownload {
    TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
    [imageDownloader deRegisterImageUrl:[NSURL URLWithString:self.imageURLString]];
}
-(void)clearImageCache {
    //TMLOG(@"____IP:Clearing Image Cache");
    TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
    [imageDownloader clearCachedImage:[NSURL URLWithString:self.imageURLString]];
}
-(void)clearImageCacheAtCurrentPath {
    //TMLOG(@"____IP:Clearing Image Cache");
    TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
    [imageDownloader clearCachedImageAtPath:self.cachePath];
}
-(void)showActivityIndicatorViewWithMessage:(NSString*)message {
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor grayColor];
    self.activityIndicatorView.titleText = message;
    self.activityIndicatorView.center = CGPointMake(self.activityIndicatorView.center.x, self.center.y);
    [self.activityIndicatorView startAnimating];
    self.activityIndicatorView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.activityIndicatorView];
}
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
}
-(void)setImageFromNotification:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    UIImage *downloadedImage = [userInfo objectForKey:TMDownloadedImage];
    if(downloadedImage) {
        if(self.scaleImage && downloadedImage.size.width != downloadedImage.size.height) {
            self.imageView.contentMode = UIViewContentModeScaleAspectFill;
            downloadedImage = [downloadedImage scaleImageToSize:CGSizeMake(self.frame.size.width, self.frame.size.height)];
        }
        self.imageView.image = downloadedImage;
    }
}
-(BOOL)canPerformActionOnNotification:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSURL *imageUrl = [userInfo objectForKey:TMImageDownloadUrl];
    if([imageUrl.absoluteString isEqualToString:self.imageURLString]) {
        return TRUE;
    }
    return FALSE;
}

-(UIImage *)getPlaceholderImage {
    return [UIImage imageNamed:@"deal_image_placeholder"];
}
@end

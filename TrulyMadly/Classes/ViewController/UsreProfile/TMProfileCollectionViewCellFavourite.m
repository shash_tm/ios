//
//  TMProfileCollectionViewCellFavourite.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellFavourite.h"
#import "TMFavView.h"
#import "TMProfileFavourites.h"
#import "TMFavouriteItem.h"
#import "TMSwiftHeader.h"


@interface TMProfileCollectionViewCellFavourite ()<TMFavViewDelegate>

@property(nonatomic,assign)NSInteger activeIndex;

@end

@implementation TMProfileCollectionViewCellFavourite

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {

    }
    return self;
}

+(UIEdgeInsets)contentInsetWithFavList {
    UIEdgeInsets inset;
    inset = UIEdgeInsetsMake(10, 0, 10, 0);
    return inset;
}

-(void)configureFavourites:(TMProfileFavourites*)favourites {
    
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        
        [self addHeader:favourites.editMode];
        
        if(favourites.editMode) {
            self.headerTitle.text = [favourites favHeaderText];
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEditAction)];
            [self addGestureRecognizer:tapGesture];
        }
        
        NSString *text = [favourites favHeaderText];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 20);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        CGRect rext = self.headerTitle.frame;
        rext.size.width = rect.size.width;
        self.headerTitle.frame = rext;
        
        UIEdgeInsets contentInset = [TMProfileCollectionViewCellFavourite contentInset];
        float yPos = contentInset.top+favourites.headerHeight;
        float xPos = 0;
        //CGFloat width = 45;
        CGFloat iconWidth = favourites.favIconWidth;
        NSArray *favItems = favourites.favItems;
        
        for (NSInteger i=0; i<favItems.count; i++) {
            
            TMFavouriteItem *favItem = [favItems objectAtIndex:i];
            
            CGRect rect = CGRectMake(xPos, yPos, iconWidth, [favourites iconHeight]);
            TMFavView *favView = [[TMFavView alloc] initWithFrame:rect withUserInteractionEnabled:favItem.status index:i];
            favView.delegate =self;
            favView.tag = 20000+i;
            
            if (favourites.isCommonFav) {
                NSArray *commonFavs = [favourites.commonFav objectAtIndex:i];
                if( (favourites.activeIndex == i) && (favItem.status) ) {
                    self.activeIndex = i;
                    if([commonFavs count]>0) {
                        favView.isCommon = true;
                        //[favView setCommon:favItem isActive:true];
                    }
//                    else {
                        [favView setImage:favItem isActive:TRUE];
//                    }
                }
                else {
                    if([commonFavs count]>0) {
                        [favView setCommon:favItem isActive:false];
                    }else {
                        [favView setImage:favItem isActive:false];
                    }
                }
            }else {
                if( (favourites.activeIndex == i) && (favItem.status) ) {
                    self.activeIndex = i;
                    [favView setImage:favItem isActive:TRUE];
                }
                else {
                    [favView setImage:favItem isActive:false];
                }
            }
            [self.contentView addSubview:favView];
            
            xPos = xPos + iconWidth;
        }
    }
}

-(void)makeActiveWithIndex:(NSInteger)index {
    TMFavView *cuurentFavView = (TMFavView*)[self.contentView viewWithTag:20000+self.activeIndex];
    [cuurentFavView setInActive];
    
    TMFavView *favView = (TMFavView*)[self.contentView viewWithTag:20000+index];
    [favView setActive];
    
    self.activeIndex = index;
    
    [self.delegate didSelectFavourite:index];
    
}

-(void)btnEditAction {
    if(self.delegate) {
        [self.delegate profileEditActionWithModule:TMEDITPROFILE_FAVOURITE];
    }
}

@end

//
//  TMProfileCollectionViewCellWorkAndEdu.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellWorkAndEdu.h"
#import "TMProfileWorkAndEdu.h"
#import "TMWorkAndEduContentView.h"
#import "TMSwiftHeader.h"

@interface TMProfileCollectionViewCellWorkAndEdu ()
@property(nonatomic,strong)TMWorkAndEduContentView *workContentView;
@property(nonatomic,strong)TMWorkAndEduContentView *eduContentView;
@property(nonatomic,strong)TMProfileWorkAndEdu *profileworkAndEdu;
@end


@implementation TMProfileCollectionViewCellWorkAndEdu

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.workContentView = [[TMWorkAndEduContentView alloc] initWithFrame:CGRectZero];
        //self.workContentView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:self.workContentView];
        
        self.eduContentView = [[TMWorkAndEduContentView alloc] initWithFrame:CGRectZero];
        //self.eduContentView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:self.eduContentView];
        
    }
    return self;
}

-(void)configureWorkAndEducation:(TMProfileWorkAndEdu*)profileworkAndEdu {
    if(!self.isCellConfigured) {
        [self addHeader:profileworkAndEdu.editMode];
        
        self.isCellConfigured = true;
        self.profileworkAndEdu = profileworkAndEdu;
        
        if(profileworkAndEdu.editMode) {
            self.headerTitle.text = [profileworkAndEdu workAndEducationHeaderText];
        }
        
        NSString *text = [profileworkAndEdu workAndEducationHeaderText];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 20);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        CGRect rext = self.headerTitle.frame;
        rext.size.width = rect.size.width;
        self.headerTitle.frame = rext;
        
        UIEdgeInsets contentInset = [TMProfileCollectionViewCellWorkAndEdu contentInset];
        CGFloat xPos = 0;
        CGFloat yPos =  contentInset.top + profileworkAndEdu.headerHeight;
        self.workContentView.frame = CGRectMake(xPos,
                                                yPos,
                                                self.frame.size.width,
                                        profileworkAndEdu.workSectionHeight);
        [self.workContentView layoutWorkContent:profileworkAndEdu];
        
        //////////
        yPos += profileworkAndEdu.workSectionHeight + profileworkAndEdu.spaceBetweenWorkAndEduSection;
        self.eduContentView.frame = CGRectMake(xPos,
                                               yPos,
                                               self.frame.size.width,
                                    profileworkAndEdu.educationSectionHeight);
        [self.eduContentView layoutEducationContent:profileworkAndEdu];
    
        UIView *view = [[UIView alloc] initWithFrame:self.workContentView.bounds];
        view.backgroundColor = [UIColor grayColor];
        //[self.contentView addSubview:view];
        
        UIView *view2 = [[UIView alloc] initWithFrame:self.eduContentView.bounds];
        view2.backgroundColor = [UIColor brownColor];
        //[self.contentView addSubview:view2];
    }
    
    
}

-(void)enableEditMode {
    if(!self.isEditCellConfigured) {
        self.isEditCellConfigured = true;
        
        ////////////////
        UIButton *editBtnWork = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 54,
                                                                           -5,
                                                                           64,
                                                                           44)];
        editBtnWork.backgroundColor = [UIColor  clearColor];
        [editBtnWork addTarget:self action:@selector(btnEditActionWork) forControlEvents:UIControlEventTouchUpInside];
        [self.workContentView addSubview:editBtnWork];
        
        UIImageView *editImgViewWork = [[UIImageView alloc] initWithFrame:CGRectMake(25,
                                                                                     8,
                                                                                     20,
                                                                                     20)];
        editImgViewWork.image = [UIImage imageNamed:@"edit"];
        editImgViewWork.backgroundColor = [UIColor clearColor];
        [editBtnWork addSubview:editImgViewWork];
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEditActionWork)];
        [self.workContentView addGestureRecognizer:tapGesture1];

        
        //////////////////////
        UIButton *editBtnEdu = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 54,
                                                                          -5,
                                                                          64,
                                                                          44)];
        editBtnEdu.backgroundColor = [UIColor  clearColor];
        [editBtnEdu addTarget:self action:@selector(btnEditActionEdu) forControlEvents:UIControlEventTouchUpInside];
        [self.eduContentView addSubview:editBtnEdu];
        
        UIImageView *editImgViewEdu = [[UIImageView alloc] initWithFrame:CGRectMake(25,
                                                                                    8,
                                                                                    20,
                                                                                    20)];
        editImgViewEdu.image = [UIImage imageNamed:@"edit"];
        editImgViewEdu.backgroundColor = [UIColor clearColor];
        [editBtnEdu addSubview:editImgViewEdu];
        
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEditActionEdu)];
        [self.eduContentView addGestureRecognizer:tapGesture2];
    }
}

-(void)btnEditActionWork {
    if(self.delegate) {
        [self.delegate profileEditActionWithModule:TMEDITPROFILE_WORK];
    }
}
-(void)btnEditActionEdu {
    if(self.delegate) {
        [self.delegate profileEditActionWithModule:TMEDITPROFILE_EDU];
    }
}

@end

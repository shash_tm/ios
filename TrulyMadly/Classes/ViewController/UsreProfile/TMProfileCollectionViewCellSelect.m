//
//  TMProfileCollectionViewCellSelect.m
//  TrulyMadly
//
//  Created by Ankit on 04/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellSelect.h"
#import "TMProfileSelectInfo.h"
#import <UIImageView+AFNetworking.h>
#import "UIColor+TMColorAdditions.h"
#import "TMUserSession.h"
#import "NSString+TMAdditions.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"

@interface TMProfileCollectionViewCellSelect ()

@property(nonatomic,strong)FLAnimatedImageView *gifAnimationImageView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *actionLabel;
@property(nonatomic,strong)UIImageView *actionImageView;

@end


@implementation TMProfileCollectionViewCellSelect

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapGesture];
        
        CGFloat xPos = 10;
        CGFloat width = 44;
        self.gifAnimationImageView = [[FLAnimatedImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                                           (CGRectGetHeight(frame) - width)/2,
                                                                                           width, width)];
        self.gifAnimationImageView.contentMode = UIViewContentModeScaleToFill;
        self.gifAnimationImageView.clipsToBounds = YES;
       [self addSubview:self.gifAnimationImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.titleLabel];
        
    }
    return self;
}

-(UILabel*)actionLabel {
    if(!_actionLabel) {
        CGFloat labelWidth = 70;
        CGFloat labelHeight = 30;
        CGFloat xOffset = 10;
        
        CGRect rect = CGRectMake(CGRectGetWidth(self.frame) - (labelWidth + xOffset),
                                             (CGRectGetHeight(self.frame) - labelHeight)/2,
                                             labelWidth, labelHeight);
        _actionLabel = [[UILabel alloc] initWithFrame:rect];
        _actionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        _actionLabel.backgroundColor = [UIColor clearColor];
        _actionLabel.textAlignment = NSTextAlignmentCenter;
        _actionLabel.layer.borderWidth = 1.5;
        _actionLabel.layer.borderColor = [UIColor likeColor].CGColor;
        _actionLabel.layer.cornerRadius = 5;
        _actionLabel.textColor = [UIColor likeColor];
        [self addSubview:_actionLabel];
    }
    return _actionLabel;
}
-(UIImageView*)actionImageView {
    if(!_actionImageView) {
        CGFloat imageWidth = 12;
        CGFloat imageHeight = 18;
        CGFloat xOffset = 10;
        CGRect rect = CGRectMake(CGRectGetWidth(self.frame) - (imageWidth + xOffset),
                                 (CGRectGetHeight(self.frame) - imageHeight)/2,
                                 imageWidth, imageHeight);
        
        _actionImageView = [[UIImageView alloc] initWithFrame:rect];
        _actionImageView.contentMode = UIViewContentModeScaleAspectFit;
        _actionImageView.backgroundColor = [UIColor clearColor];
        _actionImageView.tintColor = [UIColor likeColor];
        _actionImageView.image = [[UIImage imageNamed:@"right_arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    return _actionImageView;
}
-(void)tapAction {
    [self.delegate didTapSelectQuizCell];
}

-(void)configureSelectCell:(TMProfileSelectInfo*)selectInfo {
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        
        if([TMUserSession sharedInstance].user.isSelectUser) {
            [self.actionLabel removeFromSuperview];
            self.actionLabel = nil;
            
            ////
            self.gifAnimationImageView.image = nil;
            if(selectInfo.commonImageLink) {
                NSURL *imageURL = [NSURL URLWithString:selectInfo.commonImageLink];
                [self showGifAnimationFromURL:imageURL];
            }
            else {
                self.gifAnimationImageView.image = [UIImage imageNamed:@"selectprofileplaceholder"];
            }
            
            self.titleLabel.text = [selectInfo getCommonText];
            CGFloat xOffsetBetweenImageViewAndTitleLabel = 16;
            CGFloat xOffsetBetweenTitleLabelAndActionLabel = 5;
            CGFloat actionImageviewXOffset = 10;
            CGFloat titleLabelWidth = CGRectGetWidth(self.frame) - (CGRectGetMaxX(self.gifAnimationImageView.frame) +
                                                                    CGRectGetWidth(self.actionImageView.frame) +
                                                                    actionImageviewXOffset +  xOffsetBetweenImageViewAndTitleLabel +
                                                                    xOffsetBetweenTitleLabelAndActionLabel);
            CGFloat titleLabelHeight = 60;
            CGFloat titleLabelXPos = CGRectGetMaxX(self.gifAnimationImageView.frame)+xOffsetBetweenImageViewAndTitleLabel;
            self.titleLabel.frame = CGRectMake(titleLabelXPos,
                                               (CGRectGetHeight(self.frame) - titleLabelHeight)/2,
                                               titleLabelWidth, titleLabelHeight);
            
            [self addSubview:self.actionImageView];
        }
        else {
            [self.actionImageView removeFromSuperview];
            self.actionImageView = nil;
            
            self.gifAnimationImageView.image = [UIImage imageNamed:@"lockedInfo"];
            self.titleLabel.text = selectInfo.defaultText;
            
            //calculate size for cta text
            NSString *titleText = [[TMUserSession sharedInstance].user selectProfileCta];
            NSDictionary *attributes = @{NSFontAttributeName:self.actionLabel.font};
            CGSize constrintSize = CGSizeMake(200, 30);
            CGRect boudingRect = [titleText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            CGFloat actionLabelWidth = CGRectGetWidth(boudingRect)+20;
            CGFloat actionLabelHeight = 30;
            CGFloat actionLabelXOffset = 10;
            
            CGRect rect = CGRectMake(CGRectGetWidth(self.frame) - (actionLabelWidth + actionLabelXOffset),
                                     (CGRectGetHeight(self.frame) - actionLabelHeight)/2,
                                     actionLabelWidth, actionLabelHeight);
            self.actionLabel.frame = rect;
            self.actionLabel.text = titleText;
            
            ///
            CGFloat xOffsetBetweenImageViewAndTitleLabel = 16;
            CGFloat xOffsetBetweenTitleLabelAndActionLable = 5;
            CGFloat titleLabelWidth = CGRectGetWidth(self.frame) - (CGRectGetMaxX(self.gifAnimationImageView.frame) +
                                                                    CGRectGetWidth(self.actionLabel.frame) +
                                                                    actionLabelXOffset +  xOffsetBetweenImageViewAndTitleLabel +
                                                                    xOffsetBetweenTitleLabelAndActionLable);
            CGFloat titleLabelHeight = 60;
            CGFloat titleLabelXPos = CGRectGetMaxX(self.gifAnimationImageView.frame)+xOffsetBetweenImageViewAndTitleLabel;
            self.titleLabel.frame = CGRectMake(titleLabelXPos,
                                               (CGRectGetHeight(self.frame) - titleLabelHeight)/2,
                                               titleLabelWidth, titleLabelHeight);
        }
    }
}

-(void)showGifAnimationFromURL:(NSURL*)imageURL {
    [self getGIFImageDataFromURL:imageURL response:^(NSData *imageData) {
         [self animateGIFImageFromData:imageData];
    }];
}

-(void)getGIFImageDataFromURL:(NSURL*)imageURL response:(void(^)(NSData* imageData))responseBlock {
    NSURLRequest* request = [[NSURLRequest alloc] initWithURL:imageURL];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse* response, NSData* data, NSError* error) {
        if(data) {
            responseBlock(data);
        }
    }];
}

-(void)animateGIFImageFromData:(NSData*)imageData {
    FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
    self.gifAnimationImageView.animatedImage = animatedImage;
    [self.gifAnimationImageView setLoopCompletionBlock:nil];
}

@end




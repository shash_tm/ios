//
//  TMProfileCollectionViewCellWorkAndEdu.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@class TMProfileWorkAndEdu;
@protocol TMProfileCollectionViewCellWorkAndEduDelegate;

@interface TMProfileCollectionViewCellWorkAndEdu : TMProfileCollectionViewCell

@property(nonatomic,weak)id<TMProfileCollectionViewCellWorkAndEduDelegate>delegate;

-(void)configureWorkAndEducation:(TMProfileWorkAndEdu*)profileworkAndEdu;

@end

@protocol TMProfileCollectionViewCellWorkAndEduDelegate <TMProfileCollectionViewCellDelegate>

-(void)didSelectFavourite:(NSInteger)favIndex;

@end

//
//  TMProfileCollectionView.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionView.h"


@interface TMProfileCollectionView ()


@end

@implementation TMProfileCollectionView


#pragma mark - Initialization
- (void)configureCollectionView
{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    self.backgroundColor = [UIColor clearColor];
    self.alwaysBounceVertical = YES;
    self.showsVerticalScrollIndicator = false;
    
    [self registerClass:[TMProfileCollectionViewCellSelect class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellSelect cellReuseIdentifier]];
    [self registerClass:[TMProfileCollectionViewCellLike class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellLike cellReuseIdentifier]];
    [self registerClass:[TMProfileCollectionViewCellWorkAndEdu class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellWorkAndEdu cellReuseIdentifier]];
    [self registerClass:[TMProfileCollectionViewCellFavourite class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellFavourite cellReuseIdentifier]];
    [self registerClass:[TMProfileCollectionViewCellFavouriteList class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellFavouriteList cellReuseIdentifier]];

//    [self registerClass:[TMProfileCollectionViewCellCompatibility class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellCompatibility cellReuseIdentifier]];
//    [self registerClass:[TMProfileCollectionViewCellCommonLikeList class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellCommonLikeList cellReuseIdentifier]];

    [self registerClass:[TMProfileCollectionViewCellVarification class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellVarification cellReuseIdentifier]];
    [self registerClass:[TMProfilePhotoHeaderView class]
forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
  withReuseIdentifier:[TMProfilePhotoHeaderView headerReuseIdentifier]];
}

- (instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        [self configureCollectionView];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureCollectionView];
}

#pragma mark - header

- (TMProfilePhotoHeaderView *)dequeueProfilePhotoHeaderForIndexPath:(NSIndexPath *)indexPath
{
    TMProfilePhotoHeaderView *headerView = [super dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                             withReuseIdentifier:[TMProfilePhotoHeaderView headerReuseIdentifier]
                                                                                    forIndexPath:indexPath];

    return headerView;
}

-(void)didSelectFavourite:(NSInteger)favIndex {
    [self.delegate collectionView:self
                           layout:self.collectionViewLayout
        didSelectFavouriteAtIndex:favIndex];
}

-(void)showCommonLikes {
    [self.delegate collectionView:self layout:self.collectionViewLayout didHideCommonLike:true];
}
-(void)hideCommonLikes {
    [self.delegate collectionView:self layout:self.collectionViewLayout didHideCommonLike:false];
}
-(void)moveToAdaptabilityQuiz:(NSString *)key {
    [self.delegate collectionView:self layout:self.collectionViewLayout takeQuizAction:TMQUIZSCORETYPE_VALUE_ADAPTABILITY];
}
-(void)moveToValuesQuiz:(NSString *)key {
    [self.delegate collectionView:self layout:self.collectionViewLayout takeQuizAction:TMQUIZSCORETYPE_VALUE];
}

-(void)didTapPhotoHeader:(NSArray *)images withCurrentIndex:(NSInteger)currentIndex {
    [self.delegate collectionView:self layout:self.collectionViewLayout fullScreenPhotoViewAction:images currentIndex:currentIndex];
}

-(void)profileEditActionWithModule:(TMEditProfileModule)editProfileModule {
    [self.delegate collectionView:self
                           layout:self.collectionViewLayout
      editProfileActionWithModule:editProfileModule];
}

-(void)editProfileActionWithModule:(TMEditProfileModule)editProfileModule {
    [self.delegate collectionView:self
                           layout:self.collectionViewLayout
      editProfileActionWithModule:editProfileModule];
}
-(void)didTapCommonFacebookFriendView:(UIView *)actionView withConnections:(NSArray *)connections{
    [self.delegate collectionView:self
                           layout:self.collectionViewLayout
       commonFacebookFriendAction:actionView
                mutualConnections:connections];
}

-(void)didTapFBSync {
    [self.delegate collectionView:self layout:self.collectionViewLayout didTapFBSync:TRUE];
}

-(void)didTrustScoreClick:(BOOL)isOpen {
    [self.delegate collectionView:self layout:self.collectionViewLayout didTrustScoreClick:isOpen];
}

-(void)didTapSelectQuizCell {
    [self.delegate didClickSelectQuizCell];
}

@end


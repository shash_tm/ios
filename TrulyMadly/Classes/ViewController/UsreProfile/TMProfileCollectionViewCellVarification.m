//
//  TMProfileCollectionViewCellVarification.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellVarification.h"
#import "TMProfileVerification.h"
#import "TMScoreView.h"
#import "TMEndorseListView.h"
#import "TMVerification.h"
#import "TMSwiftHeader.h"
#import "TMProfileTrustScoreView.h"
#import "TMTrustScoreCircleView.h"

@implementation TMProfileCollectionViewCellVarification

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        //[self addHeader];
    }
    return self;
}

+(UIEdgeInsets)contentInset {
    UIEdgeInsets inset;
    inset = UIEdgeInsetsMake(18, 0, 0, 0);
    return inset;
}

-(void)configureVarificationData:(TMProfileVerification*)profileVerification {
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
//        [self addHeader:profileVerification.editMode];
//        
//        if(profileVerification.editMode) {
//            self.headerTitle.text = [profileVerification verificationHeaderText];
//        }
        
        CGRect rectHeader = CGRectZero;
        
        CGRect frame = self.frame;
        CGFloat y = 0;
        
        if(profileVerification.editMode) {
            self.headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                         15,
                                                                         frame.size.width,
                                                                         20)];
            self.headerTitle.font = [UIFont boldSystemFontOfSize:15];
            self.headerTitle.text = [profileVerification verificationHeaderText];
            self.headerTitle.textColor = [UIColor blackColor];
            self.headerTitle.backgroundColor = [UIColor clearColor];
            [self.contentView addSubview:self.headerTitle];
            
            // add subheader text
            NSString *subHeaderText = [profileVerification verificationSubHeaderText];
            NSDictionary *att = @{NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
            CGSize constraint = CGSizeMake(CGRectGetWidth(self.frame)-25, NSUIntegerMax);
            rectHeader = [subHeaderText boundingRectWithConstraintSize:constraint attributeDictionary:att];
            UILabel *subLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 15+20, rectHeader.size.width, rectHeader.size.height)];
            subLbl.text = subHeaderText;
            subLbl.numberOfLines = 0;
            subLbl.font = [UIFont systemFontOfSize:12.0];
            subLbl.textColor = [UIColor grayColor];
            [subLbl sizeToFit];
            [self.contentView addSubview:subLbl];
            y = 38+rectHeader.size.height;
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEditAction)];
            [self addGestureRecognizer:tapGesture];

        }
        
        UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                      y,
                                                                                      frame.size.width,
                                                                                      0.5)];
        seperatorImgView.backgroundColor = [UIColor separatorColor];
        [self.contentView addSubview:seperatorImgView];
        
        NSString *text = [profileVerification verificationHeaderText];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 20);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        CGRect rext = self.headerTitle.frame;
        rext.size.width = rect.size.width;
        self.headerTitle.frame = rext;

        
        UIEdgeInsets contentInset = [TMProfileCollectionViewCellVarification contentInset];

        CGFloat width = self.frame.size.width;
        
        CGFloat yPos = profileVerification.headerHeight+contentInset.top+rectHeader.size.height;
        
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, width, 50)];
        [self.contentView addSubview:container];
        
        CGFloat xPos = self.bounds.size.width-55;
        
        rect = CGRectMake(xPos, -15, 65, 65);
        
        int score = [profileVerification.trustScore intValue];
        TMTrustScoreCircleView* circleView = [[TMTrustScoreCircleView alloc] initWithFrame:rect score:score isEditMode:profileVerification.editMode];
        //circleView.backgroundColor = [UIColor redColor];
        [container addSubview:circleView];
        
        TMProfileTrustScoreView* scoreView = [[TMProfileTrustScoreView alloc] initWithFrame:CGRectMake(-10, 0, container.frame.size.width, container.frame.size.height) trustData:profileVerification];
        [container addSubview:scoreView];
        [container bringSubviewToFront:circleView];
    }
}

-(void)btnEditAction {
    if(self.delegate) {
        [self.delegate profileEditActionWithModule:TMEDITPROFILE_TRUSTBUILDER];
    }
}


@end

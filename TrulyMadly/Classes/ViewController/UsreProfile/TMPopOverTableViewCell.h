//
//  TMPopOverTableViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 06/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMPopOverTableViewCell : UITableViewCell

@property(nonatomic,strong)UIImageView *profileImageView;
@property(nonatomic,strong)UILabel *profileName;

@end

//
//  TMProfileCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@protocol TMProfileCollectionViewCellDelegate;

@interface TMProfileCollectionViewCell : UICollectionViewCell

//@property(nonatomic,weak)id<TMProfileCollectionViewCellDelegate> delegate;
@property(nonatomic,assign)BOOL isCellConfigured;
@property(nonatomic,assign)BOOL isEditCellConfigured;
@property(nonatomic,strong)UILabel *headerTitle;
@property(nonatomic,strong)UIImageView *editImage;
@property(nonatomic,strong)UIImageView *cellFooterView;


+(NSString*)cellReuseIdentifier;
+(UIEdgeInsets)contentInset;
-(void)addHeader:(BOOL)isEditMode;
-(void)enableEditMode;
-(void)addAlertIcon;
//edit button action handler to be ioveriddeb
-(void)btnEditAction;
//SeventyNine Profile Ad Changes
- (void)addFooter;
@end

@protocol TMProfileCollectionViewCellDelegate <NSObject>

-(void)profileEditActionWithModule:(TMEditProfileModule)editProfileModule;

@end

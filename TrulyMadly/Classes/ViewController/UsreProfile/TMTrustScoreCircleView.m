//
//  TMTrustScoreCircleView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMTrustScoreCircleView.h"

#define DEGREES_TO_RADIANS(degrees)  ((M_PI * degrees)/ 180)
#define scoreColor [UIColor whiteColor]

@interface TMTrustScoreCircleView()

@property(nonatomic, assign)int score;
@property(nonatomic, assign)BOOL isEditMode;

@end

@implementation TMTrustScoreCircleView

- (id)initWithFrame:(CGRect)frame score:(int)score isEditMode:(BOOL)isEditMode
{
    self = [super initWithFrame:frame];
    if (self) {
        self.score = score;
        self.isEditMode = isEditMode;
        self.backgroundColor = [UIColor clearColor];
        // Initialization code
        [self addTickImage];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGFloat xPos = (self.frame.size.width+5)/2;  CGFloat radius = (self.frame.size.width/2)-15-2.15;//(self.frame.size.width-5)/2-4;
    CGFloat yPos = (self.frame.size.height)/2;
    UIColor *circleColor = [UIColor whiteColor];
    UIColor *fillColor = [UIColor clearColor];
    if(self.isEditMode) {
       // circleColor = [UIColor colorWithRed:0.89 green:0.89 blue:0.89 alpha:1];
        circleColor = [UIColor blackColor];
        fillColor = [UIColor clearColor];
        
    }
    
    CGFloat startAngle = (self.score == 100) ? 0 : 270;
    CGFloat endAngle = (self.score == 100) ? 360 : ((360*self.score)/100)-90;
    
    if(self.score > 0) {
        CAShapeLayer *circle = [CAShapeLayer layer];
        // Make a circular shape
        circle.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(xPos, yPos)
                                                 radius:radius
                                             startAngle:DEGREES_TO_RADIANS(startAngle)
                                               endAngle:DEGREES_TO_RADIANS(endAngle)
                                              clockwise:YES].CGPath;

        // Configure the apperence of the circle
        circle.fillColor = [UIColor clearColor].CGColor;
        circle.strokeColor = (self.isEditMode) ? [UIColor blackColor].CGColor:scoreColor.CGColor;
        circle.lineWidth = 2.85;
    
        // Add to parent layer
        [self.layer addSublayer:circle];
    
        // Configure animation
        CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        drawAnimation.duration            = 0.218; // "animate over 10 seconds or so.."
        drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
        // Animate from no part of the stroke being drawn to the entire stroke being drawn
        drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
        drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    
        // Experiment with timing to get the appearence to look the way you want
        drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
        // Add the animation to the circle
        [circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    }
    
    //draw inner circle
    radius = (self.frame.size.width/2)-15-4.5;//(self.frame.size.width-5)/2-6;
    UIBezierPath *innerCircle = [UIBezierPath bezierPathWithArcCenter:CGPointMake(xPos, yPos)
                                                         radius:radius
                                                     startAngle:0
                                                       endAngle:DEGREES_TO_RADIANS(360)
                                                      clockwise:YES];
    [fillColor setFill];
    innerCircle.lineWidth = 1.0;
    [innerCircle fill];
    [circleColor set];
    [innerCircle stroke];
    
    // draw outer circle
    radius = (self.frame.size.width/2)-15;//(self.frame.size.width-5)/2-2;
    UIBezierPath *outerCircle = [UIBezierPath bezierPathWithArcCenter:CGPointMake(xPos, yPos)
                                                               radius:radius
                                                           startAngle:0
                                                             endAngle:DEGREES_TO_RADIANS(360)
                                                            clockwise:YES];
    outerCircle.lineWidth = 1.0;
    [circleColor set];
    [outerCircle stroke];
    
}

-(void)addTickImage
{
    CGFloat innerCircleWidth = 2*(((self.frame.size.width-5)/2)-6);
    CGFloat xPos = self.frame.origin.x + 6 + (innerCircleWidth-14)/2;
    CGFloat yPos = self.frame.origin.y + 6 + (innerCircleWidth-11)/2;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, 17, 18)];
    imageView.center = CGPointMake((self.frame.size.width+5)/2, self.frame.size.height/2);
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = (self.isEditMode) ? [UIImage imageNamed:@"tick_edit"]:[UIImage imageNamed:@"tick"];
    [self addSubview:imageView];

}

@end

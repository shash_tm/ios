//
//  TMProfileTrustScoreView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMProfileVerification;

typedef void(^CollapseBlock)(BOOL);

@interface TMProfileTrustScoreView : UIView

-(instancetype)initWithFrame:(CGRect)frame trustData:(TMProfileVerification*)trustData;

-(BOOL)isAnimating;

-(void)expandChildView;

-(void)collapseView:(CollapseBlock)collapseBlock;

@end

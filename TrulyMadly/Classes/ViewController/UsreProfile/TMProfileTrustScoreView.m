//
//  TMProfileTrustScoreView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfileTrustScoreView.h"
#import "TMProfileVerification.h"
#import "TMVerification.h"
#import "NSString+TMAdditions.h"

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@interface TMProfileTrustScoreView()

@property(nonatomic, strong)TMProfileVerification* trustData;
@property(nonatomic, assign)BOOL isAnimation;
@property(nonatomic, strong)UIView *childView;
@property(nonatomic, strong)UIView *facebookView;
@property(nonatomic, strong)UIView *phoneView;
@property(nonatomic, strong)UIView *linkedinView;
@property(nonatomic, strong)UIView *idView;
@property(nonatomic, strong)UIView *endorseView;

@end

@implementation TMProfileTrustScoreView

-(instancetype)initWithFrame:(CGRect)frame trustData:(TMProfileVerification *)trustData {
    self = [super initWithFrame:frame];
    if(self){
        self.trustData = trustData;
        self.clipsToBounds = true;
        [self createTrustScoreSubViews];
        return self;
    }
    return nil;
}

-(void)createTrustScoreSubViews {
    CGFloat maxWidth = self.frame.size.width-self.frame.size.height+10;
    
    CGFloat xPos = (self.trustData.editMode) ? 0 : self.frame.size.width;
    UIColor *bgColor = (self.trustData.editMode) ? [UIColor whiteColor] : [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.85];
    self.childView = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0, self.frame.size.width, self.frame.size.height)];
    self.childView.backgroundColor = bgColor;
    [self addSubview:self.childView];
    
    xPos = 0;
    CGFloat widthOfEachView = maxWidth/5;
    CGFloat yPos = (self.trustData.editMode) ? 0 : self.frame.size.height;
    CGFloat height = self.frame.size.height;
    // facebook view
    CGRect rect = CGRectMake(xPos, yPos, widthOfEachView, height);
    [self createFacebookView:rect];
    
    // phone view
    xPos = xPos+widthOfEachView;
    rect = CGRectMake(xPos, yPos, widthOfEachView, height);
    [self createPhoneView:rect];
    
    // linkedin view
    xPos = xPos+widthOfEachView;
    rect = CGRectMake(xPos, yPos, widthOfEachView, height);
    [self createLinkedinView:rect];
    
    // id view
    xPos = xPos+widthOfEachView;
    rect = CGRectMake(xPos, yPos, widthOfEachView, height);
    [self createIdView:rect];
    
    // endorse view
    xPos = xPos+widthOfEachView;
    rect = CGRectMake(xPos, yPos, widthOfEachView, height);
    [self createEndorseView:rect];
}

-(void)createFacebookView:(CGRect)frame {
    self.facebookView = [[UIView alloc] initWithFrame:frame];
    self.facebookView.backgroundColor = [UIColor clearColor];
    [self.childView addSubview:self.facebookView];
    
    UIColor *textColor = (self.trustData.editMode) ? [UIColor lightGrayColor] : [UIColor whiteColor];
    UIImage *image = (self.trustData.editMode) ? [UIImage imageNamed:@"FbFriends_me"] : [UIImage imageNamed:@"FbFriends"];
    
    CGFloat width = frame.size.width/2;
    CGFloat xPos = (frame.size.width-width)/2;
    CGFloat yPos = (self.trustData.editMode) ? 0 : (frame.size.height-width)/2;//((IS_IPHONE_6_PLUS || IS_IPHONE_6) ? 8 : 12);
    
    TMVerification *fbVerification = self.trustData.trustMeters[0];
    UIImageView *fbImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, width)];
    fbImageView.image = [UIImage imageNamed:fbVerification.imageName];
    [self.facebookView addSubview:fbImageView];
    
    if(![fbVerification.text isEqualToString:@""]){
        CGFloat offset = 2;//(self.trustData.editMode) ? 7 : 2;
        yPos = yPos+width+offset;
        NSString *text = fbVerification.text;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
        CGSize constrintSize = CGSizeMake(frame.size.width-10, NSUIntegerMax);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        CGFloat xPos = (frame.size.width-rect.size.width-10)/2;
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
        lbl.text = text;
        lbl.font = [UIFont systemFontOfSize:12.0];
        lbl.textColor = textColor;
        [self.facebookView addSubview:lbl];
        
        UIImageView *connView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbl.frame), yPos+2, 16, 10)];
        connView.image = image;
        [self.facebookView addSubview:connView];
    }
}

-(void)createPhoneView:(CGRect)frame {
    self.phoneView = [[UIView alloc] initWithFrame:frame];
    self.phoneView.backgroundColor = [UIColor clearColor];
    [self.childView addSubview:self.phoneView];
    
    CGFloat width = frame.size.width/2;
    CGFloat xPos = (frame.size.width-width)/2;
    CGFloat yPos = (self.trustData.editMode) ? 0 : (frame.size.height-width)/2;//((IS_IPHONE_6_PLUS || IS_IPHONE_6) ? 8 : 12);
    
    TMVerification *phoneVerification = self.trustData.trustMeters[1];
    UIImageView *phoneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, width)];
    phoneImageView.image = [UIImage imageNamed:phoneVerification.imageName];
    [self.phoneView addSubview:phoneImageView];
}

-(void)createLinkedinView:(CGRect)frame {
    self.linkedinView = [[UIView alloc] initWithFrame:frame];
    self.linkedinView.backgroundColor = [UIColor clearColor];
    [self.childView addSubview:self.linkedinView];
    
    UIColor *textColor = (self.trustData.editMode) ? [UIColor lightGrayColor] : [UIColor whiteColor];
    UIImage *image = (self.trustData.editMode) ? [UIImage imageNamed:@"LinkedinConnections_me"] : [UIImage imageNamed:@"LinkedinConnections"];
    
    CGFloat width = frame.size.width/2;
    CGFloat xPos = (frame.size.width-width)/2;
    CGFloat yPos = (self.trustData.editMode) ? 0 : (frame.size.height-width)/2;//((IS_IPHONE_6_PLUS || IS_IPHONE_6) ? 8 : 12);
    
    TMVerification *linkedinVerification = self.trustData.trustMeters[2];
    UIImageView *linkedinImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, width)];
    linkedinImageView.image = [UIImage imageNamed:linkedinVerification.imageName];
    [self.linkedinView addSubview:linkedinImageView];
    
    if(![linkedinVerification.text isEqualToString:@""]){
        CGFloat offset = 2;//(self.trustData.editMode) ? 7 : 2;
        yPos = yPos+width+offset;
        NSString *text = linkedinVerification.text;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
        CGSize constrintSize = CGSizeMake(frame.size.width-10, NSUIntegerMax);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((frame.size.width-rect.size.width-10)/2, yPos, rect.size.width, rect.size.height)];
        lbl.text = text;
        lbl.font = [UIFont systemFontOfSize:12.0];
        lbl.textColor = textColor;
        [self.linkedinView addSubview:lbl];
        
        UIImageView *connView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbl.frame), yPos+2, 12.4, 10)];
        connView.image = image;
        [self.linkedinView addSubview:connView];
    }
}

-(void)createIdView:(CGRect)frame {
    self.idView = [[UIView alloc] initWithFrame:frame];
    self.idView.backgroundColor = [UIColor clearColor];
    [self.childView addSubview:self.idView];
    
    CGFloat width = frame.size.width/2;
    CGFloat xPos = (frame.size.width-width)/2;
    CGFloat yPos = (self.trustData.editMode) ? 0 : (frame.size.height-width)/2;//((IS_IPHONE_6_PLUS || IS_IPHONE_6) ? 8 : 12);
    //NSLog(@"");
    TMVerification *idVerification = self.trustData.trustMeters[3];
    UIImageView *idImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, width)];
    idImageView.image = [UIImage imageNamed:idVerification.imageName];
    [self.idView addSubview:idImageView];
}

-(void)createEndorseView:(CGRect)frame {
    self.endorseView = [[UIView alloc] initWithFrame:frame];
    self.endorseView.backgroundColor = [UIColor clearColor];
    [self.childView addSubview:self.endorseView];
    //NSLog(@"");
    CGFloat width = frame.size.width/2;
    CGFloat xPos = (frame.size.width-width)/2;
    CGFloat yPos = (self.trustData.editMode) ? 0 : (frame.size.height-width)/2;//((IS_IPHONE_6_PLUS || IS_IPHONE_6) ? 8 : 12);
    
    TMVerification *endorseVerification = self.trustData.trustMeters[4];
    UIImageView *endorseImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, width, width)];
    endorseImageView.image = [UIImage imageNamed:endorseVerification.imageName];
    [self.endorseView addSubview:endorseImageView];
    
//    if(![endorseVerification.text isEqualToString:@""]){
//        CGFloat offset = (self.trustData.editMode) ? 7 : 5;
//        yPos = yPos+width+offset;
//        NSString *text = endorseVerification.text;
//        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
//        CGSize constrintSize = CGSizeMake(frame.size.width-10, NSUIntegerMax);
//        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
//    
//        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((frame.size.width-rect.size.width-12)/2, yPos, rect.size.width, rect.size.height)];
//        lbl.text = text;
//        lbl.font = [UIFont systemFontOfSize:12.0];
//        lbl.textColor = [UIColor whiteColor];
//        [self.endorseView addSubview:lbl];
//        
//        UIImageView *connView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbl.frame), yPos+2, 12, 12)];
//        connView.image = [UIImage imageNamed:@"CommonFriends"];
//        [self.endorseView addSubview:connView];
//    }
}

-(BOOL)isAnimating{
    return self.isAnimation;
}

-(void)animateView:(BOOL)upperDir identifier:(NSString*)identifier {
    CGFloat yPos = (upperDir) ? 0 : self.frame.size.height;
    if ([identifier isEqualToString:@"fb"]) {
        CGRect frame = self.facebookView.frame;
        frame.origin.y = yPos;
        self.facebookView.frame = frame;
    }
    if ([identifier isEqualToString:@"phone"]) {
        CGRect frame = self.phoneView.frame;
        frame.origin.y = yPos;
        self.phoneView.frame = frame;
    }
    
    if ([identifier isEqualToString:@"linkedin"]) {
        CGRect frame = self.linkedinView.frame;
        frame.origin.y = yPos;
        self.linkedinView.frame = frame;
    }
    if ([identifier isEqualToString:@"id"]) {
        CGRect frame = self.idView.frame;
        frame.origin.y = yPos;
        self.idView.frame = frame;
    }
    if ([identifier isEqualToString:@"endorse"]) {
        CGRect frame = self.endorseView.frame;
        frame.origin.y = yPos;
        self.endorseView.frame = frame;
    }
}

-(void)expandChildView {
    [UIView animateWithDuration:0.2 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
            animations:^{
                self.isAnimation = true;
                CGRect frame = self.childView.frame;
                frame.origin.x = 0;
                self.childView.frame = frame;
            }
            completion:^(BOOL finished){
                [self expandView];
            }];
}

-(void)expandView {
    [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.isAnimation = true;
                         [self animateView:true identifier:@"fb"];
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                animations:^{
                                    [self animateView:true identifier:@"phone"];
                                }
                                completion:^(BOOL finished){
                                    [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                            animations:^{
                                                [self animateView:true identifier:@"linkedin"];
                                            }
                                            completion:^(BOOL finished){
                                                [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                                        animations:^{
                                                            [self animateView:true identifier:@"id"];
                                                        }
                                                        completion:^(BOOL finished){
                                                            [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                                                    animations:^{
                                                                        [self animateView:true identifier:@"endorse"];
                                                                    }
                                                                    completion:^(BOOL finished){
                                                                        self.isAnimation = false;
                                                                    }];
                                                        }];
                                            }];
                                }];
                     }];
}

-(void)collapseView:(CollapseBlock)collapseBlock {
    [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
            animations:^{
                self.isAnimation = true;
                [self animateView:false identifier:@"fb"];
            }
            completion:^(BOOL finished){
                [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                        animations:^{
                            [self animateView:false identifier:@"phone"];
                        }
                        completion:^(BOOL finished){
                            [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                    animations:^{
                                        [self animateView:false identifier:@"linkedin"];
                                    }
                                    completion:^(BOOL finished){
                                        [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                                animations:^{
                                                    [self animateView:false identifier:@"id"];
                                                }
                                                completion:^(BOOL finished){
                                                    [UIView animateWithDuration:0.1 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                                            animations:^{
                                                                [self animateView:false identifier:@"endorse"];
                                                            }
                                                            completion:^(BOOL finished){
                                                                [UIView animateWithDuration:0.2 delay: 0.0 options:UIViewAnimationOptionCurveEaseInOut
                                                                        animations:^{
                                                                            CGRect frame = self.childView.frame;
                                                                            frame.origin.x = self.frame.size.width;
                                                                            self.childView.frame = frame;
                                                                        }
                                                                        completion:^(BOOL finished){
                                                                            self.isAnimation = false;
                                                                            collapseBlock(true);
                                                                        }];
                                                            }];
                                                }];
                                    }];
                        }];
            }];
}

@end

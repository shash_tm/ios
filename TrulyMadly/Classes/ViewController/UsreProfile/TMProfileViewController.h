//
//  TMProfileViewController.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMProfileCollectionView.h"
#import "TMBaseViewController.h"

@class TMProfile;

@interface TMProfileViewController : TMBaseViewController

@property(nonatomic,strong,readonly)TMProfileCollectionView *collectionView;
@property(nonatomic,strong)TMProfile *profile;
@property(nonatomic,assign)BOOL isEditMode;
@property(nonatomic,assign)BOOL showLastActiveTimestamp;
@property(nonatomic,assign)BOOL isMutualMatch;

-(void)reloadProfileView;
-(void)configureProfileCollectionViewWithProfileData;
-(void)handleSelectQuizCellClick;
-(void)showProfileAdWithPlaceholderView:(UIView*)placeholderView;
-(void)refreshCellForProfileAdLikes:(NSArray *)profileLikes;
@end

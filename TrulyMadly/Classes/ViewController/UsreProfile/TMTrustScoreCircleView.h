//
//  TMTrustScoreCircleView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMTrustScoreCircleView : UIView

-(instancetype)initWithFrame:(CGRect)frame score:(int)score isEditMode:(BOOL)isEditMode;

@end

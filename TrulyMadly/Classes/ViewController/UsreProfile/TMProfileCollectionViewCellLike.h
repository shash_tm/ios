//
//  TMProfileCollectionViewCellLike.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@class TMProfileLikes;
@class TMDateHashtag;
@protocol TMProfileCollectionViewCellLikeDelegate;

@interface TMProfileCollectionViewCellLike : TMProfileCollectionViewCell

@property(nonatomic,weak)id<TMProfileCollectionViewCellLikeDelegate>delegate;

-(void)configureLikes:(TMProfileLikes*)likes;
-(void)configureDealHashtag:(TMDateHashtag *)hashtags;

@end

@protocol TMProfileCollectionViewCellLikeDelegate <TMProfileCollectionViewCellDelegate>

-(void)didSelectFavourite:(NSInteger)favIndex;

@end

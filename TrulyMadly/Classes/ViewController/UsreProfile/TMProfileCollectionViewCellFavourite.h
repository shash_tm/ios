//
//  TMProfileCollectionViewCellFavourite.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@class TMProfileFavourites;
@protocol TMProfileCollectionViewCellFavouriteDelegate;

@interface TMProfileCollectionViewCellFavourite : TMProfileCollectionViewCell

@property(nonatomic,weak)id<TMProfileCollectionViewCellFavouriteDelegate> delegate;
+(UIEdgeInsets)contentInsetWithFavList;
-(void)configureFavourites:(TMProfileFavourites*)favourites;

@end

@protocol TMProfileCollectionViewCellFavouriteDelegate <TMProfileCollectionViewCellDelegate>

-(void)didSelectFavourite:(NSInteger)favIndex;

@end

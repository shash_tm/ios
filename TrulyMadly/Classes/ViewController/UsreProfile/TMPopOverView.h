//
//  TMPopOverView.h
//  TrulyMadly
//
//  Created by Ankit on 31/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMPopOverViewDelegate;

@interface TMPopOverView : UIView

@property(nonatomic,weak)id<TMPopOverViewDelegate> delegate;
@property(nonatomic,strong)UIView *baseView;

-(void)loadData:(NSArray*)content withActionViewFrame:(CGRect)actionViewFrame;
-(void)resetPopOverView;

@end

@protocol TMPopOverViewDelegate <NSObject>

-(void)didTapPopOverView;

@end

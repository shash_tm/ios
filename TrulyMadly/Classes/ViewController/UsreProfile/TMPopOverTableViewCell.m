//
//  TMPopOverTableViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 06/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPopOverTableViewCell.h"

@implementation TMPopOverTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        CGFloat offset = 4;
        CGRect frame = self.frame;
        CGFloat width = frame.size.height - 2*offset;
        
        self.profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2*offset, offset, width , width)];
        self.profileImageView.layer.cornerRadius = width/2;
        self.profileImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.profileImageView];
        
        CGFloat labelXPos = CGRectGetMaxX(self.profileImageView.frame) + offset;
        CGFloat labelHeight = 20;
        CGFloat lableWidth = frame.size.width - labelXPos-(2*offset);
        CGFloat lableYPos = (frame.size.height - labelHeight)/2;
        
        self.profileName = [[UILabel alloc] initWithFrame:CGRectMake(labelXPos, lableYPos, lableWidth, labelHeight)];
        self.profileName.font = [UIFont systemFontOfSize:12];
        self.profileName.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.profileName];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

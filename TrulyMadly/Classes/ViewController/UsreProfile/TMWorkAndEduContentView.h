//
//  TMWorkAndEduContentView.h
//  TrulyMadly
//
//  Created by Ankit on 18/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMProfileWorkAndEdu;

@interface TMWorkAndEduContentView : UIView

-(void)layoutWorkContent:(TMProfileWorkAndEdu*)profileWorkAndEdu;
-(void)layoutEducationContent:(TMProfileWorkAndEdu*)profileWorkAndEdu;

@end

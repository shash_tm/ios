//
//  TMProfileCollectionViewCellFavouriteList.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellFavouriteList.h"
#import "TMFavListView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMProfileFavourites.h"
#import "TMFavouriteItem.h"

#define TMFavListView_TAG  450001



@implementation TMProfileCollectionViewCellFavouriteList

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        TMFavListView *favListView = [[TMFavListView alloc] initWithFrame:CGRectZero withType:FavListType_Match];
        favListView.tag = TMFavListView_TAG;
        favListView.backgroundColor = [UIColor clearColor];
        favListView.color = [UIColor likeunSelectedColor];
        [self.contentView addSubview:favListView];
    }
    return self;
}

+(UIEdgeInsets)contentInset {
    //as favlistview has 10 pt empty sspace in thr bottom so reduce bottom inset by 10
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


-(void)setFavouriteData:(TMFavouriteItem *)favItem commonFavs:(NSArray *)commFavs withContentheight:(CGFloat)contentHeight {
    TMFavListView *favListView = (TMFavListView*) [self.contentView viewWithTag:TMFavListView_TAG];
    
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        favListView.frame = CGRectMake(0,
                                       0,
                                       self.frame.size.width,
                                       contentHeight);
        
    }
    favListView.commonFavorites = commFavs;
    [favListView loadDataWithContent:favItem.favs];
    
}

-(void)reloadFavItem:(TMFavouriteItem*)favItem {
    TMFavListView *favListView = (TMFavListView*) [self.contentView viewWithTag:TMFavListView_TAG];
    [favListView loadDataWithContent:favItem.favs];
}


@end

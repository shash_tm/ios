//
//  TMProfileCollectionViewCellLike.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellLike.h"
#import "TMLikeVIew.h"
#import "TMProfileLikes.h"
#import "TMProfileInterest.h"
#import "TMDateHashtag.h"

@interface TMProfileCollectionViewCellLike ()
@property(nonatomic,strong)TMProfileLikes* likes;

@end


@implementation TMProfileCollectionViewCellLike

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
    }
    return self;
}

+(UIEdgeInsets)contentInsetForEditMode {
    UIEdgeInsets inset;
    inset = UIEdgeInsetsMake(10, 0, 10, 0);
    return inset;
}

-(void)configureLikes:(TMProfileLikes*)likes {
    
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        self.likes = likes;
        
        if(likes.showHeader) {
            [self addHeader:likes.editMode];
        }
        
        UIEdgeInsets contentInset = [TMProfileCollectionViewCellLike contentInset];
        float yPos = contentInset.top;
        if(likes.editMode) {
            yPos += likes.headerHeight;
        }
        
        float xPos = 0;
    
        for (TMProfileInterest *interest in likes.likes) {
            
            CGSize tagSize = [interest tagSizeForMaximumWidth:self.frame.size.width];
            CGRect maxBounds = self.bounds;
            CGFloat width = tagSize.width + likes.likeViewInset.left+likes.likeViewInset.right;
            if(xPos + width > maxBounds.size.width) {
                xPos = 0;
                yPos = yPos + likes.likeViewHeight + likes.likeViewNewLineVerticalMargin;
            }
            
            TMLikeView *view = [[TMLikeView alloc] initWithFrame:CGRectMake(xPos, yPos, width, likes.likeViewHeight) withInterst:interest];
            view.backgroundColor = [UIColor clearColor];
            view.layer.cornerRadius = 16;
            view.layer.masksToBounds = YES;
            view.layer.borderWidth = (interest.commonLike) ? 1.5 : 0.5;
            view.layer.borderColor = [interest tagColor].CGColor;
            [self.contentView addSubview:view];
            
            xPos = xPos + width + likes.likeViewHorizontalMargin;
        }
    }
}

-(void)configureDealHashtag:(TMDateHashtag *)hashtags
{
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        UIEdgeInsets contentInset = [TMProfileCollectionViewCellLike contentInset];
        float yPos = contentInset.top;
    
        float xPos = 0;
    
        for (TMProfileInterest *interest in hashtags.hashtag) {
        
            CGSize tagSize = [interest tagSizeForMaximumWidth:self.frame.size.width];
            CGRect maxBounds = self.bounds;
            CGFloat width = tagSize.width + hashtags.likeViewInset.left+hashtags.likeViewInset.right;
            if(xPos + width > maxBounds.size.width) {
                xPos = 0;
                yPos = yPos + hashtags.likeViewHeight + hashtags.likeViewNewLineVerticalMargin;
            }
        
            TMLikeView *view = [[TMLikeView alloc] initWithFrame:CGRectMake(xPos, yPos, width, hashtags.likeViewHeight) withInterst:interest];
            view.backgroundColor = [UIColor clearColor];
            view.layer.cornerRadius = 12;
            view.layer.masksToBounds = YES;
            view.layer.borderWidth = 1.0;
            view.layer.borderColor = [interest tagColor].CGColor;
            [self.contentView addSubview:view];
        
            xPos = xPos + width + hashtags.likeViewHorizontalMargin;
        }
    }

}

-(void)enableEditMode {
    if(!self.isEditCellConfigured) {
        self.isEditCellConfigured = true;
        [self addHeader:true];
        self.headerTitle.text = [self.likes likeHeaderTextForEditMode];
        
        ////////////////
//        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 54,
//                                                                   0,
//                                                                   64,
//                                                                   44)];
//        btn.backgroundColor = [UIColor  clearColor];
        //[btn addTarget:self action:@selector(btnEditAction) forControlEvents:UIControlEventTouchUpInside];
        //[self.contentView addSubview:btn];
        
        self.editImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30,
                                                                       10,
                                                                       20,
                                                                       20)];
        self.editImage.image = [UIImage imageNamed:@"edit"];
        self.editImage.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.editImage];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEditAction)];
        [self addGestureRecognizer:tapGesture];
    }
    
}

-(void)btnEditAction {
    if(self.delegate) {
        [self.delegate profileEditActionWithModule:TMEDITPROFILE_INTEREST];
    }
}

@end

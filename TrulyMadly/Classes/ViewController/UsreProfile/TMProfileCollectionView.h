//
//  TMProfileCollectionView.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMProfileCollectionViewFlowLayout.h"
#import "TMProfileCollectionViewDataSource.h"
#import "TMProfileCollectionViewDelegateFlowLayout.h"
#import "TMProfileCollectionViewCellLike.h"
#import "TMProfileCollectionViewCellWorkAndEdu.h"
#import "TMProfileCollectionViewCellFavourite.h"
#import "TMProfileCollectionViewCellCompatibility.h"
#import "TMProfileCollectionViewCellVarification.h"
#import "TMProfileCollectionViewCellSelect.h"
#import "TMProfileCollectionViewCellFavouriteList.h"
#import "TMProfilePhotoHeaderView.h"


@interface TMProfileCollectionView : UICollectionView <TMProfileCollectionViewCellLikeDelegate,TMProfileCollectionViewCellWorkAndEduDelegate,TMProfileCollectionViewCellFavouriteDelegate,TMProfileCollectionViewCellCompatibilityDelegate,TMProfileCollectionViewCellVarificationDelegate,TMProfilePhotoHeaderViewDelegate,TMProfileCollectionViewCellSelectDelegate>

/**
 *  The object that provides the data for the collection view.
 *  The data source must adopt the `TMProfileCollectionViewDataSource` protocol.
 */
@property (weak, nonatomic) id<TMProfileCollectionViewDataSource> dataSource;

/**
 *  The object that acts as the delegate of the collection view.
 *  The delegate must adpot the `TMProfileCollectionViewDelegateFlowLayout` protocol.
 */
@property (weak, nonatomic) id<TMProfileCollectionViewDelegateFlowLayout> delegate;

/**
 *  The layout used to organize the collection view’s items.
 */
@property (strong, nonatomic) TMProfileCollectionViewFlowLayout *collectionViewLayout;

/**
 *
 */
- (TMProfilePhotoHeaderView *)dequeueProfilePhotoHeaderForIndexPath:(NSIndexPath *)indexPath;

@end

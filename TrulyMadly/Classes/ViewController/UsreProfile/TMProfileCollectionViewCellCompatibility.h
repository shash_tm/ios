//
//  TMProfileCollectionViewCellCompatibility.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@protocol TMProfileCollectionViewCellCompatibilityDelegate;
@class TMProfileCompatibility;

@interface TMProfileCollectionViewCellCompatibility : TMProfileCollectionViewCell

@property(nonatomic,weak)id<TMProfileCollectionViewCellCompatibilityDelegate>delegate;

-(void)configureCompatibilityCell:(TMProfileCompatibility*)comp;
+(UIEdgeInsets)contentInsetWithCommonLike;

@end

@protocol TMProfileCollectionViewCellCompatibilityDelegate <TMProfileCollectionViewCellDelegate>

-(void)showCommonLikes;
-(void)hideCommonLikes;

-(void)moveToAdaptabilityQuiz:(NSString*)key;
-(void)moveToValuesQuiz:(NSString*)key;;

@end
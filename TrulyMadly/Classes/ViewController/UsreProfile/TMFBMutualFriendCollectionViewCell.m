//
//  TMFBMutualFriendCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 09/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMFBMutualFriendCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface TMFBMutualFriendCollectionViewCell ()

@property(nonatomic,strong)UIImageView *imgView;
@property(nonatomic,strong)UILabel *titleLabel;

@end

@implementation TMFBMutualFriendCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 59, 59)];
        self.imgView.layer.borderWidth = 1;
        self.imgView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.imgView.backgroundColor = [UIColor lightGrayColor];
        self.imgView.layer.cornerRadius = CGRectGetHeight(self.imgView.frame)/2;
        self.imgView.clipsToBounds = TRUE;
        [self addSubview:self.imgView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.imgView.frame), 60, 20)];
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
    }
    return self;
}

-(void)setTitleText:(NSString*)titleText {
    self.titleLabel.text = titleText;
}
-(void)setImageFromURL:(NSURL*)imageURL {
    self.imgView.image = nil;
    [self.imgView setImageWithURL:imageURL];
}

@end

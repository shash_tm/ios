//
//  TMProfilePhotoHeaderView.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfilePhotoHeaderView.h"
#import "TMProfileImageView.h"
#import "DDPageControl.h"
#import "UIColor+TMColorAdditions.h"
#import "TMPopOverView.h"
#import "TMProfileBasicInfo.h"
#import "TMStringUtil.h"
#import "NSObject+TMAdditions.h"
#import "TMDateHeader.h"
#import "TMTrustScoreCircleView.h"
#import "TMProfileVerification.h"
#import "TMProfileTrustScoreView.h"
#import "TMDataStore.h"
#import "TMUserSession.h"
#import "TMCommonFBFriendListView.h"


#define INTIAL_IMAGE_DOWNLOAD_COUNT   2
#define INITIAL_DEAL_IMAGE_DOWNLOAD_COUNT 1

#define PROFILEIMAGEVIEW_BASETAG      2000
#define PAGE_CONTROL_VERTICAL_OFFSET  100

@interface TMProfilePhotoHeaderView ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *horizontalScrollView;
@property(nonatomic,strong)UIImageView *gradientImageView;
@property(nonatomic,strong)UIImageView *selectTagImageView;
@property(nonatomic,strong)UIView *commonFacebookFriendView;
@property(nonatomic,strong)TMCommonFBFriendListView *commonFbFriendListView;
@property(nonatomic,strong)DDPageControl *pageControl;
@property(nonatomic,strong)NSMutableArray *images;
@property(nonatomic,strong)TMPopOverView *popoverView;
@property(nonatomic,strong)TMProfileVerification *trustData;
@property(nonatomic,strong)TMProfileTrustScoreView *scoreView;
@property(nonatomic,strong)TMTrustScoreCircleView *circleView;
@property(nonatomic,strong)UIView* trustTutorial;
@property(nonatomic,weak)NSTimer *autoCompleteTimer;
@property(nonatomic,assign)BOOL isProfileAdCampaign;
@property(nonatomic,assign)BOOL isHeaderConfigured;
@property(nonatomic,assign)BOOL isEditModeHeaderConfigured;
@property(nonatomic,assign)BOOL isDowloadStartedForMoreImages;
@property(nonatomic,assign)BOOL isFirstTime;
@property(nonatomic,assign)BOOL isDummySet;
@property(nonatomic,assign)BOOL isDealProfile;

@property(nonatomic,strong)NSString* matchId;

@end


@implementation TMProfilePhotoHeaderView

+ (NSString *)headerReuseIdentifier
{
    return NSStringFromClass([TMProfilePhotoHeaderView class]);
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.isDowloadStartedForMoreImages = FALSE;
        self.images = [NSMutableArray arrayWithCapacity:8];
        self.horizontalScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self.horizontalScrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
        [self.horizontalScrollView setClipsToBounds:YES];
        self.horizontalScrollView.pagingEnabled = true;
        self.horizontalScrollView.delegate = self;
        self.horizontalScrollView.showsHorizontalScrollIndicator = NO;
        self.horizontalScrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.horizontalScrollView];
        
        self.pageControl = [[DDPageControl alloc] initWithFrame:CGRectZero];
        [self.pageControl setType: DDPageControlTypeOnFullOffEmpty] ;
        [self.pageControl setOnColor: [UIColor colorWithWhite:2.0f alpha: 1.0f]] ;
        [self.pageControl setOffColor: [UIColor colorWithWhite:1.0f alpha: 1.0f]] ;
        [self.pageControl setIndicatorDiameter: 8.0f] ;
        [self.pageControl setIndicatorSpace: 5.0f] ;
        [self addSubview:self.pageControl];
        self.pageControl.frame = CGRectMake((frame.size.width-self.pageControl.frame.size.width)/2,
                                            frame.size.height-PAGE_CONTROL_VERTICAL_OFFSET,
                                            self.pageControl.frame.size.width,
                                            self.pageControl.frame.size.height);
        self.pageControl.backgroundColor = [UIColor clearColor];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapGesture];
        
        self.isFirstTime = false;
    }
    return self;
}

-(TMCommonFBFriendListView*)commonFbFriendListView {
    if(!_commonFbFriendListView) {
        CGFloat xPos = 0;
        CGFloat width = CGRectGetWidth(self.frame);
        CGFloat yPos = 0;
        CGFloat height = CGRectGetHeight(self.frame);
        CGFloat heightOffset = (CGRectGetHeight(self.gradientImageView.frame) + 20);
        self.commonFbFriendListView = [[TMCommonFBFriendListView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)
                                                               withBottonHeightOffset:heightOffset matchId:self.matchId];
    }
    return _commonFbFriendListView;
}

-(void)enableEditMode:(BOOL)isFbConnected {
    if(!self.isEditModeHeaderConfigured) {
        self.isEditModeHeaderConfigured = true;
        CGFloat y = 10;
        if(isFbConnected) {
            y = 25;
        }
        ///////////////photo///////
        UIButton *editBtnPhoto = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 54,
                                                                            y,
                                                                            54,
                                                                            44)];
        [editBtnPhoto addTarget:self action:@selector(btnEditActionPhoto) forControlEvents:UIControlEventTouchUpInside];
        editBtnPhoto.backgroundColor = [UIColor clearColor];
        [self addSubview:editBtnPhoto];
        
        UIImageView *editImgViewPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(14,
                                                                                      y,
                                                                                      30,
                                                                                      30)];
        editImgViewPhoto.image = [UIImage imageNamed:@"WhiteEdit"];
        editImgViewPhoto.clipsToBounds = true;
        editImgViewPhoto.backgroundColor = [UIColor clearColor];
        [editBtnPhoto addSubview:editImgViewPhoto];
        
        ////////////////basics
        UIButton *editBtnBasics = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 54,
                                                                             self.frame.size.height-45,
                                                                             64,
                                                                             44)];
        editBtnBasics.backgroundColor = [UIColor  clearColor];
        [editBtnBasics addTarget:self action:@selector(btnEditActionBasics) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:editBtnBasics];
        
        UIImageView *editImgViewBasics = [[UIImageView alloc] initWithFrame:CGRectMake(14,
                                                                                       10,
                                                                                       20,
                                                                                       20)];
        editImgViewBasics.image = [UIImage imageNamed:@"edit"];
        editImgViewBasics.backgroundColor = [UIColor clearColor];
        [editBtnBasics addSubview:editImgViewBasics];
        
        if(isFbConnected) {
            // facebook button
            UIButton *fbButton = [UIButton buttonWithType:UIButtonTypeSystem];
            fbButton.frame = CGRectMake(0, 0, self.frame.size.width, 44);
            [fbButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [fbButton setTitle:@"Update Profile" forState:UIControlStateNormal];
            fbButton.titleLabel.font = [UIFont systemFontOfSize:15];
            fbButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [fbButton addTarget:self action:@selector(didfbSyncClicked) forControlEvents:UIControlEventTouchUpInside];
            fbButton.backgroundColor = [UIColor colorWithRed:59.0f/255.0f green:86.0f/255.0f blue:156.0f/255.0f alpha:1];
            [fbButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 22.0f, 0.0f, 0.0f)];
            [self addSubview:fbButton];
            
            //facebook icon
            NSDictionary *textDict = @{@"ktext":@"Update Profile",@"kfont":[UIFont systemFontOfSize:15]};
            CGRect stringFrame = [TMStringUtil textRectForString:textDict];
            CGFloat stringWidth = stringFrame.size.width;
            UIImageView *fbIcon = [[UIImageView alloc] init];
            UIImage *fbImage = [UIImage imageNamed:@"Facebook_Icon"];
            fbIcon.image = fbImage;
            fbIcon.frame = CGRectMake(((fbButton.frame.size.width-stringWidth)/2)-12, (fbButton.frame.size.height-stringFrame.size.height)/2 ,stringFrame.size.height,stringFrame.size.height);
            [self addSubview:fbIcon];
        }
    }
}

-(void)configerHeader:(TMProfileBasicInfo*)basicInfo
             editMode:(BOOL)editMode
showLastActiveTimestamp:(BOOL)showLastActiveTimestamp
        isFbConnected:(BOOL)isFbConnected
        trustData:(TMProfileVerification *)trustData {
    self.matchId = basicInfo.matchId;
    
    if(!self.isHeaderConfigured) {
        /////////////////////////
        self.isHeaderConfigured = true;
        
        self.isProfileAdCampaign = basicInfo.isProfileAdCampaign;
        
        // set up the image view
        if(basicInfo.imageUrlString != nil) {
            [self.images addObject:basicInfo.imageUrlString];
        }
        if([basicInfo.otherImages isValidObject]) {
            [self.images addObjectsFromArray:basicInfo.otherImages];
        }
        
        NSInteger noOfImages = (self.images.count>1) ? self.images.count : 0;
        self.pageControl.numberOfPages = noOfImages;
        [self bringSubviewToFront:self.pageControl];
        
        self.horizontalScrollView.contentSize = CGSizeMake(self.frame.size.width*noOfImages,
                                                           self.frame.size.height);
        
        if(editMode && isFbConnected) {

            CGRect scrollFrame = self.horizontalScrollView.frame;
            scrollFrame.origin.y = 44;
            scrollFrame.size.height = scrollFrame.size.height-44;
            self.horizontalScrollView.frame = scrollFrame;
            
            self.horizontalScrollView.contentSize = CGSizeMake(self.frame.size.width*noOfImages,
                                                               self.frame.size.height-44);
        }
        [self addImagesToScrollView];
        
        // add gradient view on top of image scroll view
        CGFloat contentHeight = 70;
        
        ////////////////////////////////////////
        CGFloat gradientYPos = self.horizontalScrollView.frame.origin.y+self.horizontalScrollView.frame.size.height-contentHeight;
        self.gradientImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                               gradientYPos,
                                                                               self.bounds.size.width,
                                                                               contentHeight)];
        self.gradientImageView.userInteractionEnabled = true;
        self.gradientImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.gradientImageView];
        if(editMode) {
            ////add tap view
            UIView *editView = [[UIView alloc] initWithFrame:self.gradientImageView.frame];
            editView.backgroundColor = [UIColor clearColor];
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEditActionBasics)];
            [editView addGestureRecognizer:tapGesture];
            [self addSubview:editView];
        }
        
        if(basicInfo.isProfileAdCampaign) {
            // sponsored campaign name
            UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, self.frame.size.width-20, 20)];
            lb.font = [UIFont systemFontOfSize:18];
            lb.textColor = [UIColor whiteColor];
            lb.backgroundColor = [UIColor clearColor];
            lb.text = basicInfo.name;
            [self.gradientImageView addSubview:lb];
            
            // stamp text for campaign
            CGFloat yPos = CGRectGetMaxY(lb.frame)+2;
            
            lb = [[UILabel alloc] initWithFrame:CGRectMake(10, yPos, self.frame.size.width-20, 15)];
            lb.font = [UIFont systemFontOfSize:11];
            lb.textColor = [UIColor whiteColor];
            lb.backgroundColor = [UIColor clearColor];
            lb.numberOfLines = 0;
            lb.text = @"Sponsored";
            [self.gradientImageView addSubview:lb];
        }
        else {
            self.trustData = trustData;
            
            //isDummySet
            if(basicInfo.isDummySet) {
                self.isDummySet = basicInfo.isDummySet;
            }
            
            //////////////////////////
            CGFloat yPos = 10;
            CGFloat xPos = 10;
            CGFloat maxWidth = (editMode) ? self.frame.size.width - 45-(2*xPos) : self.frame.size.width-(2*xPos);
            
            //calculate middle string height
            NSInteger height = basicInfo.height.integerValue;
            NSInteger height1 = (NSInteger)height / 12;
            NSInteger height2 = (NSInteger)height % 12;
            NSString *htStr = [NSString stringWithFormat:@"%ld'%ld\"",(long)height1,(long)height2];
            NSString *finalString = (basicInfo.facebookMutualConnections) ? [NSString stringWithFormat:@"%@ | %@ | ",basicInfo.city,htStr] : [NSString stringWithFormat:@"%@ | %@",basicInfo.city,htStr];
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:8];
            [dict setObject:[UIFont systemFontOfSize:16] forKey:@"kfont"];
            [dict setObject:[NSNumber numberWithFloat:maxWidth] forKey:@"kmaxwidth"];
            [dict setObject:finalString forKey:@"ktext"];
            CGSize finalSize = [TMStringUtil textRectForString:dict].size;
            
            // name and age height calculation
            [dict setObject:[UIFont systemFontOfSize:18] forKey:@"kfont"];
            [dict setObject:[NSNumber numberWithFloat:self.bounds.size.width-40] forKey:@"kmaxwidth"];
            NSString *text = @"";
            BOOL last_seen_flag = ([TMDataStore containsObjectForKey:@"last_seen_status"]) ? [TMDataStore retrieveBoolforKey:@"last_seen_status"] : false;
            if(last_seen_flag && showLastActiveTimestamp && basicInfo.lastActivity) {
                text = [NSString stringWithFormat:@"%@, %@ | ",[basicInfo.name uppercaseString],basicInfo.age];
            }else {
                text = [NSString stringWithFormat:@"%@, %@ ",[basicInfo.name uppercaseString],basicInfo.age];
            }
            [dict setObject:text forKey:@"ktext"];
            CGSize size = [TMStringUtil textRectForString:dict].size;
                        
            ///add name  age
            UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, size.width, 25)];
            lb.font = [UIFont systemFontOfSize:18];
            lb.textColor = [UIColor whiteColor];
            lb.backgroundColor = [UIColor clearColor];
            lb.text = text;
            [self.gradientImageView addSubview:lb];
            
            xPos = xPos + size.width;
            //last Activity
            
            if(showLastActiveTimestamp && last_seen_flag && basicInfo.lastActivity) {
                [dict setObject:[UIFont systemFontOfSize:16] forKey:@"kfont"];
                [dict setObject:[NSNumber numberWithInt:380] forKey:@"kmaxwidth"];
                [dict setObject:[NSString stringWithFormat:@"%@",basicInfo.lastActivity] forKey:@"ktext"];
                size = [TMStringUtil textRectForString:dict].size;
                //yPos = 5;
                
                UIImageView *lastActiveIcon = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos+4.5, 16, 16)];
                lastActiveIcon.image = [UIImage imageNamed:@"LastActive"];
                lastActiveIcon.backgroundColor = [UIColor clearColor];
                [self.gradientImageView addSubview:lastActiveIcon];
                
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos+21, lastActiveIcon.frame.origin.y+fabs(size.height-lastActiveIcon.frame.size.height)/2-3, size.width, size.height)];
                lbl.font = [UIFont systemFontOfSize:16];
                lbl.textColor = [UIColor whiteColor];
                lbl.backgroundColor = [UIColor clearColor];
                lbl.numberOfLines = 1;
                lbl.text = [NSString stringWithFormat:@"%@",basicInfo.lastActivity];
                [self.gradientImageView addSubview:lbl];
                
                xPos = lb.frame.origin.x+lb.frame.size.width+5;
            }
            
            //celeb status
            if(basicInfo.isCeleb) {
                UIImageView *celebrityIcon = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos + 3, 18, 18)];
                celebrityIcon.image = [UIImage imageNamed:@"tm_stamp"];
                celebrityIcon.backgroundColor = [UIColor clearColor];
                [self.gradientImageView addSubview:celebrityIcon];
            }
            
            ///////
            xPos = 10;
            yPos = CGRectGetMaxY(lb.frame)+2;
            
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, finalSize.width, finalSize.height)];
            label2.font = [UIFont systemFontOfSize:16];
            label2.textColor = [UIColor whiteColor];
            label2.backgroundColor = [UIColor clearColor];
            label2.numberOfLines = 0;
            label2.text = finalString;
            [self.gradientImageView addSubview:label2];
            if(basicInfo.facebookMutualConnections) {
                [self setupFbCommonConnectionViewWithCount:basicInfo.facebookMutualConnections
                                                  withXPos:CGRectGetMaxX(label2.frame)+3
                                                  withYPos:CGRectGetMaxY(lb.frame)-5];
                if(basicInfo.fbMutualFriendList) {
                     [self.commonFbFriendListView loadDataFromArray:basicInfo.fbMutualFriendList];
                }
            }
            
            ///
            if(basicInfo.isSelectMember) {
                self.selectTagImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.frame) - 74, 20, 74, 28)];
                self.selectTagImageView.image = [UIImage imageNamed:@"selectlabel"];
                [self addSubview:self.selectTagImageView];
            }
            
            if(!editMode){
                // add trust score view
                CGFloat width = contentHeight-10;
                xPos = self.frame.size.width-10-width;  yPos = 5;
                CGRect rect = CGRectMake(xPos, yPos, width+10, width);
                [self setupTrustScoreView:rect];
                
                BOOL is_auto_expand_trust_shown = ([TMDataStore containsObjectForKey:@"auto_expand_trust_shown"]) ? [TMDataStore retrieveBoolforKey:@"auto_expand_trust_shown"] : false;
                if(!is_auto_expand_trust_shown) {
                    [self showTrustScoreAnimation];
                    [TMDataStore setBool:true forKey:@"auto_expand_trust_shown"];
                }
            }
        }
    }
    
    if(editMode) {
        [self enableEditMode:isFbConnected];
    }
}

//SeventyNine Profile Ad Changes
- (void) configureHeaderForAdProfile {
    if(! self.isHeaderConfigured) {
        self.isHeaderConfigured = true;
    }
}

-(void)showTrustScoreAnimation {
    if(!self.trustTutorial){
        self.isFirstTime = true;
        CGFloat contentHeight = 120;
        CGFloat contentWidth = 173;
        self.trustTutorial = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width-contentWidth-5,
                                                                      self.horizontalScrollView.frame.origin.y+self.horizontalScrollView.frame.size.height-contentHeight-100,
                                                                      contentWidth,
                                                                      contentHeight)];
        self.trustTutorial.alpha = 0;
        [self addSubview:self.trustTutorial];
        self.trustTutorial.backgroundColor = [UIColor clearColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.trustTutorial.frame.size.width, self.trustTutorial.frame.size.height)];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.image = [[TMUserSession sharedInstance].user isUserFemale] ? [UIImage imageNamed:@"trust_tutorial_male"] : [UIImage imageNamed:@"trust_tutorial_female"];
        [self.trustTutorial addSubview:imageView];
        
        CGFloat delay = 3.0;
        [UIView animateWithDuration:0.5 delay: delay options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.trustTutorial.alpha = 1;
                             CGRect frame = self.trustTutorial.frame;
                             frame.origin.y = frame.origin.y+45;
                             self.trustTutorial.frame = frame;
                         }
                         completion:^(BOOL finished){
                             [self trustScoreTapAction];
                             [UIView animateWithDuration:0.5 delay: 2.0 options:UIViewAnimationOptionCurveEaseInOut
                                              animations:^{
                                                  CGRect frame = self.trustTutorial.frame;
                                                  frame.origin.y = frame.origin.y-45;
                                                  self.trustTutorial.frame = frame;
                                              }
                                              completion:^(BOOL finished){
                                                  self.trustTutorial.alpha = 0;
                                                  [self.trustTutorial removeFromSuperview];
                                                  self.trustTutorial = nil;
                                              }];
                         }];
    }
}
-(void)setupTrustScoreView:(CGRect)frame {
    int score = [self.trustData.trustScore intValue];
    self.circleView = [[TMTrustScoreCircleView alloc] initWithFrame:frame score:score isEditMode:false];
    [self.gradientImageView addSubview:self.circleView];
    [self.gradientImageView bringSubviewToFront:self.circleView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trustScoreTapAction)];
    [self.circleView addGestureRecognizer:tapGesture];
}
-(void)setupFbCommonConnectionViewWithCount:(NSInteger)count withXPos:(CGFloat)xPos withYPos:(CGFloat)yPos {
    //fbmutualfriend
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, 50, 40)];
    view.backgroundColor = [UIColor clearColor];
    [self.gradientImageView addSubview:view];
    [self.gradientImageView bringSubviewToFront:view];
    
    ////
    UIView *counterView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 30, 30)];
    counterView.layer.cornerRadius = 30/2;
    counterView.backgroundColor = [UIColor whiteColor];
    [view addSubview:counterView];
    
    UILabel *counterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    counterLabel.font = [UIFont systemFontOfSize:14];
    counterLabel.textColor = [UIColor redColor];
    counterLabel.backgroundColor = [UIColor clearColor];
    counterLabel.textAlignment = NSTextAlignmentCenter;
    counterLabel.text = [NSString stringWithFormat:@"%ld",(long)count];
    [counterView addSubview:counterLabel];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(view.frame)-36, 36, 36)];
    iconImageView.image = [UIImage imageNamed:@"fbmutualfriend"];
    [view addSubview:iconImageView];
    
    /////
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commonFacebookFriendTapAction)];
    [view addGestureRecognizer:tapGesture];
}
-(void)addImagesToScrollView {
    CGRect frame = self.horizontalScrollView.frame;
    CGFloat xPos = 0;
    CGFloat yPos= 0;
    for (int i=0; i<self.images.count; i++) {
        TMProfileImageView *imgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                                           yPos,
                                                                                frame.size.width,
                                                                                frame.size.width)];
        if(self.isDealProfile) {
            imgView.clearCache = FALSE;
            if(i<INITIAL_DEAL_IMAGE_DOWNLOAD_COUNT) {
                [imgView setImageFromURLString:[self.images objectAtIndex:i] isDeal:self.isDealProfile];
            }else {
                [imgView setImageFromURLStringWithBackgroundGradient:[self.images objectAtIndex:i] isDarkCentered:NO];
            }
        }
        else {
            [imgView setImageFromURLStringWithGradientForProfile:[self.images objectAtIndex:i]];
        }
        
        if(!(i == 0 && self.isDummySet)) {
            imgView.scaleImage = TRUE;
        }
        imgView.tag = PROFILEIMAGEVIEW_BASETAG + i;
        
        [self.horizontalScrollView addSubview:imgView];
        
        // no image caching for deal profile
        if(!self.isDealProfile) {
            if(i<INTIAL_IMAGE_DOWNLOAD_COUNT) {
                //image caching
                NSString *imageURLString = [self.images objectAtIndex:i];
                NSString *lastPathComponent = [imageURLString lastPathComponent];
                [imgView setImageFromURLString:imageURLString];
                [imgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:TRUE];
                imgView.retryImageDownloadOnFailure = TRUE;
                imgView.clearCacheAtPath = TRUE;
            }
        }
        xPos += frame.size.width;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    float fractionalPage = self.horizontalScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
    if(self.isDealProfile) {
        [self downloadDealImageForCurrentIndex];
    }
    else {
        if(!self.isDowloadStartedForMoreImages && (page == INTIAL_IMAGE_DOWNLOAD_COUNT-1)) {
            self.isDowloadStartedForMoreImages = TRUE;
            for (NSInteger i=page+1; i<self.images.count; i++) {
                NSInteger tag = PROFILEIMAGEVIEW_BASETAG + i;
                TMProfileImageView *imgView = (TMProfileImageView*)[self.horizontalScrollView viewWithTag:tag];
                //image caching
                NSString *imageURLString = [self.images objectAtIndex:i];
                NSString *lastPathComponent = [imageURLString lastPathComponent];
                [imgView setImageFromURLString:imageURLString];
                [imgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:TRUE];
                imgView.clearCacheAtPath = TRUE;
            }
        }
    }
}

-(void)downloadDealImageForCurrentIndex
{
     NSInteger currentIndex = (self.pageControl.currentPage >= 0) ? self.pageControl.currentPage : 0;
    if(currentIndex > INITIAL_DEAL_IMAGE_DOWNLOAD_COUNT-1)
    {
        NSInteger tag = PROFILEIMAGEVIEW_BASETAG + currentIndex;
        TMProfileImageView *imgView = (TMProfileImageView*)[self.horizontalScrollView viewWithTag:tag];
        [imgView setImageFromURLString:[self.images objectAtIndex:currentIndex] isDeal:self.isDealProfile];
    }
}

-(void)tapAction {
    NSInteger currentIndex = (self.pageControl.currentPage >= 0) ? self.pageControl.currentPage : 0;
    if(currentIndex == 0 && self.isDummySet) {
        [self.delegate editProfileActionWithModule:TMEDITPROFILE_PHOTO];
    }else {
        [self.delegate didTapPhotoHeader:self.images withCurrentIndex:currentIndex];
    }
}
-(void)btnEditActionPhoto {
    [self.delegate editProfileActionWithModule:TMEDITPROFILE_PHOTO];
}
-(void)btnEditActionBasics {
    [self.delegate editProfileActionWithModule:TMEDITPROFILE_BASICREGISTRATION];
}
-(void)commonFacebookFriendTapAction {
    [self.commonFbFriendListView loadData:self.matchId];
    [self addSubview:self.commonFbFriendListView];
    [self bringSubviewToFront:self.commonFbFriendListView];
}
-(void)didfbSyncClicked {
    [self.delegate didTapFBSync];
}
-(void)trustScoreTapAction {
    if(self.scoreView){
        [self.delegate didTrustScoreClick:false];
        // remove the trust score view
        if(![self.scoreView isAnimating]) {
            [self.scoreView collapseView:^(BOOL status) {
                [self.scoreView removeFromSuperview];
                self.scoreView = nil;
                if(self.autoCompleteTimer) {
                    [self.autoCompleteTimer invalidate];
                    self.autoCompleteTimer = nil;
                }
            }];
        }
    }else {
        [self.delegate didTrustScoreClick:true];
        // show the trust score view
        self.scoreView = [[TMProfileTrustScoreView alloc] initWithFrame:CGRectMake(0, 0, self.gradientImageView.frame.size.width, self.gradientImageView.frame.size.height) trustData:self.trustData];
        [self.gradientImageView addSubview:self.scoreView];
        [self.gradientImageView bringSubviewToFront:self.circleView];
        [self.scoreView expandChildView];
        if(!self.autoCompleteTimer && self.isFirstTime) {
            self.isFirstTime = false;
            self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(trustScoreTapAction) userInfo:nil repeats:NO];
        }
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trustScoreStripTap)];
        [self.scoreView addGestureRecognizer:tapGesture];

    }
}

-(void)trustScoreStripTap {
    
}

-(void)configureHeader:(TMDateHeader *)dateHeader isDealProfile:(BOOL)isDealProfile
{
    if(!self.isHeaderConfigured) {
        /////////////////////////
        self.isHeaderConfigured = true;
        self.isDealProfile = isDealProfile;
        
        [self.images addObjectsFromArray:dateHeader.images];
        
        NSInteger noOfImages = (self.images.count>1) ? self.images.count : 0;
        self.pageControl.numberOfPages = noOfImages;
        [self bringSubviewToFront:self.pageControl];
        
        self.horizontalScrollView.contentSize = CGSizeMake(self.frame.size.width*noOfImages,
                                                           self.frame.size.height);
        [self addImagesToScrollView];
        
        NSString *name = [NSString stringWithFormat:@"%@",dateHeader.name];
        CGFloat maxWidth =  self.frame.size.width-20;
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:8];
        [dict setObject:[UIFont systemFontOfSize:20] forKey:@"kfont"];
        [dict setObject:[NSNumber numberWithFloat:maxWidth] forKey:@"kmaxwidth"];
        [dict setObject:name forKey:@"ktext"];
        CGSize finalSize = [TMStringUtil textRectForString:dict].size;
        
        [dict setObject:[UIFont systemFontOfSize:14] forKey:@"kfont"];
        [dict setObject:dateHeader.location forKey:@"ktext"];
        CGSize locationSize = [TMStringUtil textRectForString:dict].size;
        
        CGFloat totalHeight = finalSize.height+locationSize.height+20+15+5;
        if(totalHeight < PAGE_CONTROL_VERTICAL_OFFSET - 22) {
            CGRect ctlFrame = self.pageControl.frame;
            ctlFrame.origin.y = ctlFrame.origin.y + (PAGE_CONTROL_VERTICAL_OFFSET - 22 - totalHeight);
            self.pageControl.frame = ctlFrame;
        }else {
            CGRect ctlFrame = self.pageControl.frame;
            ctlFrame.origin.y = ctlFrame.origin.y - (totalHeight-(PAGE_CONTROL_VERTICAL_OFFSET-22));
            self.pageControl.frame = ctlFrame;
        }
        
        CGFloat xPos = 10;
        CGFloat yPos = self.pageControl.frame.origin.y+(self.pageControl.frame.size.height)/2+15;
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.frame.size.width-20, finalSize.height)];
        lb.font = [UIFont systemFontOfSize:20];
        lb.textColor = [UIColor whiteColor];
        lb.backgroundColor = [UIColor clearColor];
        lb.text = name;
        lb.numberOfLines = 0;
        [lb sizeToFit];
        [self addSubview:lb];
        
        lb = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos+finalSize.height+10, self.frame.size.width-20, locationSize.height)];
        lb.font = [UIFont systemFontOfSize:14];
        lb.textColor = [UIColor whiteColor];
        lb.backgroundColor = [UIColor clearColor];
        lb.text = dateHeader.location;
        lb.numberOfLines = 0;
        [lb sizeToFit];
        [self addSubview:lb];
    }
}

@end

//
//  TMProfileCollectionViewCellSelect.h
//  TrulyMadly
//
//  Created by Ankit on 04/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@protocol TMProfileCollectionViewCellSelectDelegate;
@class TMProfileSelectInfo;

@interface TMProfileCollectionViewCellSelect : TMProfileCollectionViewCell

@property(nonatomic,weak)id<TMProfileCollectionViewCellSelectDelegate> delegate;

-(void)configureSelectCell:(TMProfileSelectInfo*)selectInfo;

@end

@protocol TMProfileCollectionViewCellSelectDelegate <NSObject>

-(void)didTapSelectQuizCell;

@end

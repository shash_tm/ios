//
//  TMFBMutualFriendCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 09/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMFBMutualFriendCollectionViewCell : UICollectionViewCell

-(void)setTitleText:(NSString*)titleText;
-(void)setImageFromURL:(NSURL*)imageURL;

@end

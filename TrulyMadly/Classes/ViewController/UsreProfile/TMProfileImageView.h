//
//  TMProfileImageView.h
//  TrulyMadly
//
//  Created by Ankit on 18/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMActivityIndicatorView.h"

@interface TMProfileImageView : UIView

@property(nonatomic,assign)BOOL scaleImage;
@property(nonatomic,assign)BOOL clearCache;
@property(nonatomic,assign)BOOL clearCacheAtPath;
@property(nonatomic,assign)BOOL retryImageDownloadOnFailure;


-(void)setImageContentMode:(UIViewContentMode)contentMode;
-(void)setImageFromURLString:(NSString*)URLString;
-(void)setDefaultImage;
-(void)setDefaultImage:(UIImage*)image;
-(void)resetCurrentImage;
-(void)downloadAndSetImageFromURL:(NSURL*)URL;
-(void)forceStartImageDownloadWithRelativePath:(NSString*)relativePath showLoader:(BOOL)showLoader;
-(void)setImageFromURLString:(NSString *)URLString isDeal:(BOOL)isDeal;
-(void)setImageFromURLStringWithBackgroundGradient:(NSString *)URLString isDarkCentered:(BOOL) isDarkCentered;
-(void)setImageFromURLStringWithGradientForProfile:(NSString *)URLString;

@end

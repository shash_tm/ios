//
//  TMProfileCollectionViewCellCommonLikeList.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@interface TMProfileCollectionViewCellCommonLikeList : TMProfileCollectionViewCell

-(void)configureCommonLikes:(NSArray *)commonLikes;

@end

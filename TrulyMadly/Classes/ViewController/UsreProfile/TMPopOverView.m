//
//  TMPopOverView.m
//  TrulyMadly
//
//  Created by Ankit on 31/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPopOverView.h"
#import "TMPopOverTableViewCell.h"
#import "UIImageView+AFNetworking.h"


@interface TMPopOverView ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)UITableView *popoverTableview;
@property(nonatomic,strong)NSArray *content;

@end

@implementation TMPopOverView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 10;
        self.layer.borderColor = [UIColor grayColor].CGColor;
        self.layer.borderWidth = 1;
        
        self.baseView = [[UIView alloc] initWithFrame:CGRectZero];
        self.baseView.backgroundColor = [UIColor blackColor];
        self.baseView.alpha = 0.1;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self.baseView addGestureRecognizer:tapGesture];
        
        [self createTableView];
    }
    return self;
}

-(void)loadData:(NSArray*)content withActionViewFrame:(CGRect)actionViewFrame {
    self.content = content;
    
    [self addSubview:self.popoverTableview];
    [self bringSubviewToFront:self.popoverTableview];
    
    self.popoverTableview.frame = self.bounds;
    
    [self.popoverTableview reloadData];
}

-(void)resetPopOverView {
    if(self.popoverTableview) {
        [self.popoverTableview removeFromSuperview];
        self.popoverTableview = nil;
    }
    if(self.baseView) {
        [self.baseView removeFromSuperview];
        self.baseView = nil;
    }
}

-(void)dealloc {
    [self resetPopOverView];
}

-(void)createTableView {
    self.popoverTableview = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.popoverTableview.delegate = self;
    self.popoverTableview.dataSource = self;
    self.popoverTableview.layer.cornerRadius = 10;
    self.popoverTableview.backgroundColor = [UIColor whiteColor];
}

-(void)tapAction {
    [self.delegate didTapPopOverView];
}

#pragma mark - tableview data source / delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.content.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TMPopOverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commoncell"];
    if(!cell) {
        cell = [[TMPopOverTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"commoncell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    NSDictionary *dict = [self.content objectAtIndex:indexPath.row];
    NSString *imgURL = [dict objectForKey:@"pic"];
    [cell.profileImageView setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"ProfileBlank"]];
    cell.profileName.text = [dict objectForKey:@"name"];
    return cell;
}

@end

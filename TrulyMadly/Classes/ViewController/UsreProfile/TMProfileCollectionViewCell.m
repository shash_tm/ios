//
//  TMProfileCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"
#import "UIColor+TMColorAdditions.h"

@implementation TMProfileCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        //self.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
    }
    return self;
}

+ (NSString *)cellReuseIdentifier {
    return NSStringFromClass([self class]);
}

+(UIEdgeInsets)contentInset {
    return UIEdgeInsetsMake(18, 0, 13, 0);
}

-(void)addHeader:(BOOL)isEditMode {
    CGRect frame = self.frame;
    CGFloat y = 0;
    
    if(isEditMode) {
        y = 38;
        self.headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                 15,
                                                                 frame.size.width,
                                                                 20)];
        self.headerTitle.font = [UIFont boldSystemFontOfSize:15];
        self.headerTitle.textColor = [UIColor blackColor];
        self.headerTitle.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.headerTitle];
    }
    
    UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                  y,
                                                                                  frame.size.width,
                                                                                  0.5)];
    seperatorImgView.backgroundColor = [UIColor profileCollectionViewCellSeparatorColor];
    [self.contentView addSubview:seperatorImgView];

}
//SeventyNine Profile Ad Changes
- (void)addFooter {
    CGRect frame = self.frame;
    CGFloat y = frame.size.height;
    if(!self.cellFooterView.superview) {
        self.cellFooterView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.cellFooterView.backgroundColor = [UIColor profileCollectionViewCellSeparatorColor];
        [self.contentView addSubview:self.cellFooterView];
    }
    self.cellFooterView.frame = CGRectMake(0, y,frame.size.width,0.5);
}

-(void)enableEditMode {
    if(!self.isEditCellConfigured) {
        self.isEditCellConfigured = true;
        ////////////////
//        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 54,
//                                                                   0,
//                                                                   64,
//                                                                   50)];
//        btn.backgroundColor = [UIColor  clearColor];
//        [btn addTarget:self action:@selector(btnEditAction) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:btn];
        
        self.editImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30,
                                                                       10,
                                                                       20,
                                                                       20)];
        self.editImage.image = [UIImage imageNamed:@"edit"];
        self.editImage.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.editImage];
    }
    
}

-(void)addAlertIcon {
    UIImageView *alertIcon = [[UIImageView alloc] initWithFrame:CGRectMake(self.headerTitle.frame.size.width+10, 15, 21, 18)];
    alertIcon.image = [UIImage imageNamed:@"Alert"];
    alertIcon.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:alertIcon];
}

-(void)btnEditAction {
   
}
@end

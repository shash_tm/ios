//
//  TMProfileCollectionViewCellVarification.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@class TMProfileVerification;
@protocol TMProfileCollectionViewCellVarificationDelegate;

@interface TMProfileCollectionViewCellVarification : TMProfileCollectionViewCell

@property(nonatomic,weak)id<TMProfileCollectionViewCellVarificationDelegate>delegate;

-(void)configureVarificationData:(TMProfileVerification*)verification;

@end

@protocol TMProfileCollectionViewCellVarificationDelegate <TMProfileCollectionViewCellDelegate>

-(void)showCommonLikes;
-(void)hideCommonLikes;

-(void)moveToAdaptabilityQuiz:(NSString*)key;
-(void)moveToValuesQuiz:(NSString*)key;;

@end

//
//  TMProfileCollectionViewCellCommonLikeList.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellCommonLikeList.h"
#import "TMFavListView.h"
#import "UIColor+TMColorAdditions.h"


@implementation TMProfileCollectionViewCellCommonLikeList

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        TMFavListView *favListView = [[TMFavListView alloc] initWithFrame:CGRectZero withType:FavListType_Match];
        favListView.tag = 450001;
        favListView.backgroundColor = [UIColor clearColor];
        //favListView.color = [UIColor commonLikeColor];
        [self.contentView addSubview:favListView];
    }
    return self;
}
+(UIEdgeInsets)contentInset {
    //as favlistview has 10 pt empty sspace in thr bottom so reduce bottom inset by 10
    return UIEdgeInsetsMake(0, 0, 5, 0);
}

-(void)configureCommonLikes:(NSArray *)commonLikes  {
    TMFavListView *favListView = (TMFavListView*) [self.contentView viewWithTag:450001];
    if(!self.isCellConfigured) {
        self.isCellConfigured = true;
        favListView.frame = CGRectMake(0, 0, self.bounds.size.width, 44);
    }
    [favListView loadDataWithContent:commonLikes];
}

@end

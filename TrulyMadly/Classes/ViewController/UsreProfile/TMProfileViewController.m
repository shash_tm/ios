//
//  TMProfileViewController.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileViewController.h"
#import "TMProfileCollectionView.h"
#import "TMProfileCollectionViewFlowLayout.h"
#import "TMProfileCollectionViewCellLike.h"
#import "TMProfileCollectionViewCellWorkAndEdu.h"
#import "TMProfileCollectionViewCellFavourite.h"
#import "TMProfileCollectionViewCellFavouriteList.h"
#import "TMProfileCollectionViewCellCompatibility.h"
#import "TMProfileCollectionViewCellCommonLikeList.h"
#import "TMProfileCollectionViewCellVarification.h"
#import "TMProfileCollectionViewCellSelect.h"
#import "TMProfile.h"
#import "TMProfileFavourites.h"
#import "TMFavouriteItem.h"
#import "TMProfileCompatibility.h"
#import "TMProfileLikes.h"
#import "TMProfileVerification.h"
#import "TMProfileWorkAndEdu.h"
#import "TMProfilePhotoHeaderView.h"
#import "TMAnalytics.h"

@interface TMProfileViewController ()<TMProfileCollectionViewDataSource,TMProfileCollectionViewDelegateFlowLayout>

@property(nonatomic,strong)TMProfileCollectionView *collectionView;
@end

@implementation TMProfileViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configureProfileCollectionViewWithProfileData {
    //reset existing profile view
    [self resetProfileCollectionView];
    
    //////
    //self.profile = profile;
    TMProfileCollectionViewFlowLayout *flowLayout = [[TMProfileCollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;

    self.collectionView = [[TMProfileCollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,85.0,0.0);
    self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(0.0,0.0,85.0,0.0);

}

-(void)resetProfileCollectionView {
    [self.collectionView removeFromSuperview];
    self.collectionView = nil;
}

-(void)reloadProfileView {
    [self.collectionView reloadData];
}

#pragma mark - Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.profile.sectionCount;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.profile rowCountForSection:section];
}

- (UICollectionViewCell *)collectionView:(TMProfileCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMProfileCollectionViewCellType cellType = [self.profile cellType:indexPath.section row:indexPath.row];
    if(cellType == TMPROFILECOLLECTIONVIEWCELL_SELECT) {
        TMProfileCollectionViewCellSelect *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellSelect cellReuseIdentifier] forIndexPath:indexPath];
        cell.delegate = collectionView;
        cell.backgroundColor = [UIColor whiteColor];
        [cell configureSelectCell:self.profile.selectInfo];
        return cell;
    }
    if(cellType == TMPROFILECOLLECTIONVIEWCELL_HASHTAG) {
        TMProfileCollectionViewCellLike *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellLike cellReuseIdentifier] forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        if(self.profile.likes != nil) {
           [cell configureLikes:self.profile.likes];
        }
        //SeventyNine Profile Ad Changes
        if(self.profile.isProfileAdCampaign && self.profile.matchId == nil) {
            [cell addFooter];
        }
        
        if(self.isEditMode) {
            [cell enableEditMode];
        }
        cell.delegate = collectionView;
        return cell;
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_WORKANDEDU) {
        TMProfileCollectionViewCellWorkAndEdu *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellWorkAndEdu cellReuseIdentifier] forIndexPath:indexPath];
        [cell configureWorkAndEducation:self.profile.workAndEducation];
        cell.delegate = collectionView;
        if(self.isEditMode) {
            [cell enableEditMode];
            if([self.profile.workAndEducation isAlertShown]) {
                [cell addAlertIcon];
            }
//            if(!(self.profile.workAndEducation.income != nil && self.profile.workAndEducation.companies.count>0 && self.profile.workAndEducation.institutes.count>0)) {
//                [cell addAlertIcon];
//            }
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_FAVOURITE) {
        TMProfileCollectionViewCellFavourite *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellFavourite cellReuseIdentifier] forIndexPath:indexPath];
        cell.delegate = collectionView;
        [cell configureFavourites:self.profile.favourite];
        if(self.isEditMode) {
            [cell enableEditMode];
            if(!self.profile.favourite.isFavsEmpty){
                [cell addAlertIcon];
            }
        }
       // cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_FAVOURITE_LIST) {
        TMProfileCollectionViewCellFavouriteList *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellFavouriteList cellReuseIdentifier] forIndexPath:indexPath];
        
        TMFavouriteItem *fvItem = [self.profile.favourite favItemAtActiveIndex];
        [cell setFavouriteData:fvItem commonFavs:[self.profile.favourite commonFavAtIndex] withContentheight:self.profile.favourite.favListContentHeight];
        //cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_TRUSTSCORE) {
        TMProfileCollectionViewCellVarification *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellVarification cellReuseIdentifier] forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        cell.delegate = collectionView;
        [cell configureVarificationData:self.profile.varification];
        if(self.isEditMode) {
            [cell enableEditMode];
            int trustScore = [self.profile.varification.trustScore intValue];
            if(trustScore < 50){
                [cell addAlertIcon];
            }
        }
        return cell;
    }
    
    return nil;
}

- (UICollectionReusableView *)collectionView:(TMProfileCollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        TMProfilePhotoHeaderView *headerView = [collectionView dequeueProfilePhotoHeaderForIndexPath:indexPath];
        headerView.backgroundColor = [UIColor clearColor];
        headerView.delegate = collectionView;
        //SeventyNine Profile Ad Changes
        if(self.profile.isProfileAdCampaign && self.profile.matchId == nil) {
            [headerView configureHeaderForAdProfile];
            [self showProfileAdWithPlaceholderView:headerView];
        }else {
            [headerView configerHeader:self.profile.basicInfo editMode:self.isEditMode showLastActiveTimestamp:self.showLastActiveTimestamp
                         isFbConnected:self.profile.varification.isFbConnected trustData:self.profile.varification];
        }
        return headerView;
        
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section==0) {
        if(self.isEditMode) {
            if(self.profile.varification.isFbConnected){
                return CGSizeMake([collectionViewLayout itemWidth], [collectionViewLayout headerHeight]+44);
            }else
                return CGSizeMake([collectionViewLayout itemWidth], [collectionViewLayout headerHeight]);
        }
       return CGSizeMake([collectionViewLayout itemWidth], [collectionViewLayout headerHeight]);
    }
    
    return CGSizeZero;
}

#pragma mark - Collection view delegate flow layout

- (CGSize)collectionView:(TMProfileCollectionView *)collectionView
                  layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionViewLayout sizeForItemAtIndexPath:indexPath];
}

- (CGFloat)collectionView:(TMProfileCollectionView *)collectionView
                   layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
 heightContentAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat height = [self.profile contentLayoutHeight:indexPath.section
                                         row:indexPath.row
                                    maxWidth:collectionViewLayout.itemWidth];
    return height;
}

- (UIEdgeInsets)collectionView:(TMProfileCollectionView *)collectionView
                   layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
cellContentInsetAtIndexPath:(NSIndexPath *)indexPath {
    UIEdgeInsets inset = UIEdgeInsetsZero;
    
    TMProfileCollectionViewCellType cellType = [self.profile cellType:indexPath.section row:indexPath.row];
    if(cellType == TMPROFILECOLLECTIONVIEWCELL_HASHTAG) {
        inset = [TMProfileCollectionViewCellLike contentInset];
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_WORKANDEDU) {
        inset = [TMProfileCollectionViewCellWorkAndEdu contentInset];
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_FAVOURITE) {
        if(self.profile.favourite.isFavsAvailable) {
            inset = [TMProfileCollectionViewCellFavourite contentInsetWithFavList];
        }
        else{
            inset = [TMProfileCollectionViewCellFavourite contentInset];
        }
    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_FAVOURITE_LIST) {
        inset = [TMProfileCollectionViewCellFavouriteList contentInset];
    }
//    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_COMPATIBILITY) {
//        inset = [TMProfileCollectionViewCellCompatibility contentInset];
//    }
//    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_COMMONLIKELSIT) {
//        inset = [TMProfileCollectionViewCellCommonLikeList contentInset];
//    }
    else if(cellType == TMPROFILECOLLECTIONVIEWCELL_TRUSTSCORE) {
        inset = [TMProfileCollectionViewCellVarification contentInset];
    }
    return inset;
}
- (void)collectionView:(TMProfileCollectionView *)collectionView
                        layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
     didSelectFavouriteAtIndex:(NSInteger)favIndex {
    
    TMProfileFavourites *fv = [self.profile favourite];
    //TMFavouriteItem *fvItem = [fv.favItems objectAtIndex:fv.activeIndex];
    //fvItem.status = FALSE;
    
    TMFavouriteItem *fvItem2 = [fv.favItems objectAtIndex:favIndex];
    fvItem2.status = TRUE;
    
    fv.activeIndex = favIndex;
    
    [self.collectionView reloadData];
    
    //send events to server
    
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMFavoritesClick" forKey:@"screenName"];
    
    NSString *activity = @"";
    if(self.isEditMode){
        activity = @"my_profile_fav_click";
    }else if(self.isMutualMatch) {
        activity = @"mutual_match_fav_click";
        if([self.profile matchId]) {
            NSDictionary *eventInfoDict = @{@"match_id":[self.profile matchId]};
            [eventDict setObject:eventInfoDict forKey:@"event_info"];
        }
        [eventDict setObject:[self.profile matchId] forKey:@"label"];
    }else {
        activity = @"match_fav_click";
        if ([self.profile matchId]) {
            NSDictionary *eventInfoDict = @{@"match_id":[self.profile matchId]};
            [eventDict setObject:eventInfoDict forKey:@"event_info"];
        }
        [eventDict setObject:[self.profile matchId] forKey:@"label"];
    }
    
    NSString *action = @"movies_click";
    if(favIndex == 1) {
        action = @"music_click";
    }else if(favIndex == 2) {
        action = @"foodntravel_click";
    }else if(favIndex == 3) {
        action = @"books_click";
    }else if(favIndex == 4) {
        action = @"others_click";
    }
    
    [eventDict setObject:activity forKey:@"eventCategory"];
    [eventDict setObject:action forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
}
- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
     didHideCommonLike:(BOOL)status {
    TMProfileCompatibility *comp = [self.profile compatibility];
    comp.isCommonLikeActive = status;
    
    [self.collectionView reloadData];
}

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
        takeQuizAction:(TMQuizScoreType)quizType {
    /////
}
- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
fullScreenPhotoViewAction:(NSArray*)images
          currentIndex:(NSInteger)index {
    
}

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
          didTapFBSync:(BOOL)tapFB {
    
}

-(void)didClickSelectQuizCell {
    [self handleSelectQuizCellClick];
}
-(void)handleSelectQuizCellClick {
    
}
-(void)showProfileAdWithPlaceholderView:(UIView*)placeholderView {
    
}

-(void)refreshCellForProfileAdLikes:(NSArray *)profileLikes {
    [self.profile addLikesForProfileAd:profileLikes];
    NSArray *indexPaths = [self.collectionView indexPathsForVisibleItems];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.collectionView != nil && [indexPaths count]) {
           [self.collectionView reloadItemsAtIndexPaths:indexPaths];
        }
        
    });
    
}

@end

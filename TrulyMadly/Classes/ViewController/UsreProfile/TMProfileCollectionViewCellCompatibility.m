//
//  TMProfileCollectionViewCellCompatibility.m
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCellCompatibility.h"
#import "TMProfileLikes.h"
#import "TMScoreView.h"
#import "TMProfileCompatibility.h"
#import "TMCompatibilityQuiz.h"


@interface TMProfileCollectionViewCellCompatibility ()<TMScoreViewDelegate>

@end

@implementation TMProfileCollectionViewCellCompatibility

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        //[self addHeader];
    }
    return self;
}

+(UIEdgeInsets)contentInsetWithCommonLike {
    UIEdgeInsets inset;
    inset = UIEdgeInsetsMake(10, 0, 10, 0);
    return inset;
}

-(void)configureCompatibilityCell:(TMProfileCompatibility*)comp {
    
    if(!self.isCellConfigured) {
        //self.comp = comp;
        self.isCellConfigured = true;
        self.headerTitle.text = [comp compatibilityHeaderText];
        UIEdgeInsets contentInset = [TMProfileCollectionViewCellCompatibility contentInset];
        CGFloat xPos = 0;
        CGFloat yPos = contentInset.top + comp.headerHeight;
        //+[comp spaceBetweenHeaderAndFirstViewComponent];
        CGFloat iconWidth = comp.iconWidth;
        //NSLog(@"Icon width:%f",iconWidth);
        
        NSInteger count = [comp compabilityItemCount];
        
        //font --> value/ adap title font for action = 12 (4s,5) / 13 (6 & above)
        //header f0nt = 14 (6 & above) / 13 (4<5s)
        //move tap to seee up
        UIScreen *screen = [UIScreen mainScreen];
        UIFont *headerFont = [UIFont systemFontOfSize:13];
        if(screen.bounds.size.width > 320) {
            headerFont = [UIFont systemFontOfSize:14];
        }
        for (int i=0; i<count; i++) {
            TMCompatibilityQuiz *valueQuiz = comp.valueQuiz;
            NSString *circleImg;
            TMScoreView *scoreView = [[TMScoreView alloc] initWithFrame:CGRectMake(xPos,
                                                                                   yPos,
                                                                                   iconWidth,
                                                                        iconWidth+comp.verticalTitleHeight)];
            scoreView.delegate = self;
            scoreView.backgroundColor = [UIColor clearColor];
            [self.contentView addSubview:scoreView];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            if(i==1) {
                if(comp.editMode && [valueQuiz isQuizFilledByUser]) {
                    //
                    [dict removeAllObjects];
                    [dict setObject:@"done" forKey:@"ktitleimgStr"];
                    [dict setObject:[valueQuiz quizTextColor] forKey:@"tcolor"];
                    [scoreView setTitleImageWithAttributeDictionary:dict];
                    
                    circleImg = [valueQuiz quizImageStringForEditMode];
                }
                else {
                    [dict setObject:[valueQuiz titleTextFont] forKey:@"kfont"];
                    [dict setObject:[valueQuiz compatibilityQuizScoreText] forKey:@"ktext"];
                    [dict setObject:[NSNumber numberWithFloat:10] forKey:@"kwidthOffset"];
                    [dict setObject:[valueQuiz quizTextColor] forKey:@"tcolor"];
                    [scoreView setTitleTextWithAttributeDictionary:dict];
                    
                    circleImg = [valueQuiz quizImageString];
                }
                
                ///
                [dict removeAllObjects];
                [dict setObject:circleImg forKey:@"kimgStr"];
                [scoreView setImageWithAttributeDictionary:dict];
                
                /////
                [dict removeAllObjects];
                [dict setObject:headerFont forKey:@"kfont"];
                [dict setObject:[comp valueHeaderText] forKey:@"ktext"];
                [scoreView setHeadeTextWithAttributeDictionary:dict];
                if([valueQuiz isInteractive]) {
                    [scoreView setEnableUserIneraction:YES];
                }
                
                //
            }
            else if(i==2) {
                TMCompatibilityQuiz *adapQuiz = comp.adapQuiz;
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                if(comp.editMode && [adapQuiz isQuizFilledByUser]) {
                    //
                    [dict removeAllObjects];
                    [dict setObject:@"done" forKey:@"ktitleimgStr"];
                    [dict setObject:[adapQuiz quizTextColor] forKey:@"tcolor"];
                    [scoreView setTitleImageWithAttributeDictionary:dict];
                    
                    circleImg = [adapQuiz quizImageStringForEditMode];
                }
                else {
                    [dict setObject:[adapQuiz titleTextFont] forKey:@"kfont"];
                    [dict setObject:[adapQuiz compatibilityQuizScoreText] forKey:@"ktext"];
                    [dict setObject:[NSNumber numberWithFloat:10] forKey:@"kwidthOffset"];
                    [dict setObject:[adapQuiz quizTextColor] forKey:@"tcolor"];
                    [scoreView setTitleTextWithAttributeDictionary:dict];
                    
                    circleImg = [adapQuiz quizImageString];
                }
                
                ///
                [dict removeAllObjects];
                [dict setObject:circleImg forKey:@"kimgStr"];
                [scoreView setImageWithAttributeDictionary:dict];
                
                //////////////////////////////
                [dict removeAllObjects];
                [dict setObject:headerFont forKey:@"kfont"];
                [dict setObject:[comp adaptibilityHeaderText] forKey:@"ktext"];
                [scoreView setHeadeTextWithAttributeDictionary:dict];
                if([adapQuiz isInteractive]) {
                    [scoreView setEnableUserIneraction:YES];
                }
            }
            else if(i==0) {
                TMProfileLikes *likes = comp.commonLike;
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:[likes commonLikeTitleFont] forKey:@"kfont"];
                if(!likes.editMode) {
                    [dict setObject:[likes commonLikeCountAsText] forKey:@"ktext"];
                }
                
                [dict setObject:[NSNumber numberWithFloat:10] forKey:@"kwidthOffset"];
                if([likes isCommonLikeAvailable]) {
                    [dict setObject:[UIFont systemFontOfSize:12] forKey:@"kstfont"];
                    [dict setObject:@"Tap" forKey:@"ksttext"];
                    [scoreView setTitleTextWithAttributeDictionary:dict];
                    [scoreView setEnableUserIneraction:YES];
                }
                [scoreView setTitleTextWithAttributeDictionary:dict];
                ///
                [dict removeAllObjects];
                [dict setObject:[likes commonLikeImageString] forKey:@"kimgStr"];
                [scoreView setImageWithAttributeDictionary:dict];
                /////
                [dict removeAllObjects];
                [dict setObject:headerFont forKey:@"kfont"];
                [dict setObject:[likes commonLikeHeaderText] forKey:@"ktext"];
                [dict setObject:[NSNumber numberWithFloat:10] forKey:@"kheightoffset"];
                if(likes.editMode) {
                    [dict setObject:[UIColor lightGrayColor] forKey:@"ktextcol"];
                    scoreView.alpha = 0.4;
                }
                [scoreView setHeadeTextWithAttributeDictionary:dict];
            }
            
            xPos = xPos + iconWidth + comp.horizontalInterimMargin;
        }
    }
}

-(void)scoreViewDidClickWithState:(BOOL)state {
    if(self.delegate) {
        if(state) {
            [self.delegate showCommonLikes];
        }
        else {
            [self.delegate hideCommonLikes];
        }
    }
}

-(void)scoreViewDidClickWithKey:(NSString*)key {
    if([key isEqualToString:@"On Values"] ) {
        [self.delegate moveToValuesQuiz:@"Values"];
    }
    else if([key isEqualToString:@"On Adaptability"]) {
        [self.delegate moveToAdaptabilityQuiz:@"Adaptability"];
    }
}

-(void)btnEditAction {
    if(self.delegate) {
        [self.delegate profileEditActionWithModule:TMEDITPROFILE_QUIZ];
    }
}


@end

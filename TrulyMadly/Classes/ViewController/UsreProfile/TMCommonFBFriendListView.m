//
//  TMCommonFBFriendListView.m
//  TrulyMadly
//
//  Created by Ankit on 09/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCommonFBFriendListView.h"
#import "TMLinearFlowLayout.h"
#import "TMFBMutualFriendCollectionViewCell.h"
#import "TMMatchManager.h"

#define COUNTERLABEL_TAG      40001
#define TRANSLUCENTVIEW_TAG   40002

@interface TMCommonFBFriendListView ()<UICollectionViewDataSource,UICollectionViewDelegate>

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)UIView *baseview;
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;
@property(nonatomic,strong)NSArray *content;
@property(nonatomic,assign)CGFloat heightOffset;
@property(nonatomic,assign)BOOL isContentLoadInProgress;

@end

@implementation TMCommonFBFriendListView

- (instancetype)initWithFrame:(CGRect)frame withBottonHeightOffset:(CGFloat)offset matchId:(NSString*)matchId
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.heightOffset = offset;
        [self setupListView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
-(void)loadData:(NSString*)matchId {
    if(!self.content && (!self.isContentLoadInProgress)) {
        self.isContentLoadInProgress = TRUE;
        [self showActivityIndicator];
        TMMatchManager *matchManager = [[TMMatchManager alloc] init];
        [matchManager getFBMutualConnectionsWithMatch:matchId.integerValue response:^(NSDictionary *data) {
            [self removeActivityIndicator];
            self.isContentLoadInProgress = FALSE;
            if(data) {
                self.content = data[@"data"];
                [self reloadData];
            }
        }];
    }
}
-(void)loadDataFromArray:(NSArray*)fbMutualFriendList {
    self.content = fbMutualFriendList;
    [self reloadData];
}
-(void)reloadData {
    UILabel *counterLabel = [self.baseview viewWithTag:COUNTERLABEL_TAG];
    NSString *text = nil;
    if(self.content.count < 2) {
        text = [NSString stringWithFormat:@"%ld common friend on Facebook",(long)self.content.count];
    }
    else {
        text = [NSString stringWithFormat:@"%ld common friends on Facebook",(long)self.content.count];
    }
    counterLabel.text = text;
    
    UIView *translucentview = [self.baseview viewWithTag:TRANSLUCENTVIEW_TAG];
    translucentview.alpha = 0.75;
    [self.collectionView reloadData];
}
-(void)showActivityIndicator {
    if(self.activityIndicator) {
        [self removeActivityIndicator];
    }
    
    CGFloat width = 44;
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.baseview.frame) - width)/2,
                                                                                       (CGRectGetHeight(self.baseview.frame) - width)/2,
                                                                                       width, width)];
    self.activityIndicator.hidesWhenStopped = TRUE;
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.activityIndicator startAnimating];
    [self.baseview bringSubviewToFront:self.activityIndicator];
    [self.baseview addSubview:self.activityIndicator];
}
-(void)removeActivityIndicator {
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    self.activityIndicator = nil;
}
-(void)setupListView {
    CGFloat xPos = 20;
    CGFloat width = CGRectGetWidth(self.bounds) - (2*xPos);
    CGFloat height = 120; //scroll ui for fb mutual friend list need to have 120 height
    CGFloat yPos = CGRectGetHeight(self.bounds) - (self.heightOffset+height);
    self.baseview = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    self.baseview.backgroundColor = [UIColor clearColor];
    self.baseview.layer.cornerRadius = 10;
    self.baseview.clipsToBounds = TRUE;
    [self addSubview:self.baseview];
    
    UIView *translucentview = [[UIView alloc] initWithFrame:self.baseview.bounds];
    translucentview.backgroundColor = [UIColor whiteColor];
    translucentview.alpha = 0.95;
    translucentview.tag = TRANSLUCENTVIEW_TAG;
    [self.baseview addSubview:translucentview];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 24, 24)];
    iconImageView.image = [UIImage imageNamed:@"fbmutualfriendgray"];
    iconImageView.backgroundColor = [UIColor clearColor];
    [self.baseview addSubview:iconImageView];
    
    UILabel *counterLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(iconImageView.frame), 3, 300, 20)];
    counterLabel.font = [UIFont systemFontOfSize:14];
    counterLabel.textColor = [UIColor blackColor];
    counterLabel.backgroundColor = [UIColor clearColor];
    counterLabel.textAlignment = NSTextAlignmentLeft;
    counterLabel.tag = COUNTERLABEL_TAG;
    [self.baseview addSubview:counterLabel];
    
    //max subview(of baseview) ypos going to 29 so set collectionview ypos to 30
    [self setupCollectionViewOnBaseViewWithInitYPos:30];
}

-(void)setupCollectionViewOnBaseViewWithInitYPos:(CGFloat)yPos {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 2;
    flowLayout.minimumInteritemSpacing = 5;
    
    CGFloat xPos = 6;
    CGRect rect = CGRectMake(xPos, yPos, CGRectGetWidth(self.baseview.frame)-(2*xPos), 80);
    self.collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.baseview addSubview:self.collectionView];
    
    [self.collectionView registerClass:[TMFBMutualFriendCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
}
-(void)tapAction {
    [self removeFromSuperview];
}

#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.content.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size = CGSizeMake(60, 80);
    return size;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"cell";
    TMFBMutualFriendCollectionViewCell *cell = [collectionView
                                                dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                                forIndexPath:indexPath];
    NSDictionary *data = self.content[indexPath.row];
    [cell setTitleText:data[@"name"]];
    [cell setImageFromURL:[NSURL URLWithString:data[@"url"]]];
    return  cell;
}

@end

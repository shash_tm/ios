//
//  TMCommonFBFriendListView.h
//  TrulyMadly
//
//  Created by Ankit on 09/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCommonFBFriendListView : UIView

- (instancetype)initWithFrame:(CGRect)frame withBottonHeightOffset:(CGFloat)offset matchId:(NSString*)matchId;
-(void)loadData:(NSString*)matchId;
-(void)loadDataFromArray:(NSArray*)fbMutualFriendList;

@end

//
//  TMProfileCollectionViewCellFavouriteList.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewCell.h"

@class TMFavouriteItem;

@interface TMProfileCollectionViewCellFavouriteList : TMProfileCollectionViewCell

-(void)setFavouriteData:(TMFavouriteItem *)favItem commonFavs:(NSArray*)commFavs withContentheight:(CGFloat)contentHeight;

@end

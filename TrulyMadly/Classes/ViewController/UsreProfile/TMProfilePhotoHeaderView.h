//
//  TMProfilePhotoHeaderView.h
//  TrulyMadly
//
//  Created by Ankit on 17/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@class TMProfileBasicInfo;
@class TMDateHeader;
@class TMProfileVerification;
@protocol TMProfilePhotoHeaderViewDelegate;

@interface TMProfilePhotoHeaderView : UICollectionReusableView

@property(nonatomic,weak)id<TMProfilePhotoHeaderViewDelegate>delegate;

+ (NSString *)headerReuseIdentifier;
-(void)configerHeader:(TMProfileBasicInfo*)basicInfo editMode:(BOOL)editMode showLastActiveTimestamp:(BOOL)showLastActiveTimestamp isFbConnected:(BOOL)isFbConnected trustData:(TMProfileVerification*)trustData;
//-(void)enableEditMode;
-(void)configureHeader:(TMDateHeader *)dateHeader isDealProfile:(BOOL)isDealProfile;
- (void) configureHeaderForAdProfile;
@end

@protocol TMProfilePhotoHeaderViewDelegate <NSObject>

@optional
-(void)didTapPhotoHeader:(NSArray*)images withCurrentIndex:(NSInteger)currentIndex;
-(void)editProfileActionWithModule:(TMEditProfileModule)editProfileModule;
-(void)didTapCommonFacebookFriendView:(UIView*)actionView withConnections:(NSArray*)connections;
-(void)didTapFBSync;
-(void)didTrustScoreClick:(BOOL)isOpen;

@end

//
//  TMViewControllerActionDelegate.h
//  TrulyMadly
//
//  Created by Ankit on 09/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@protocol TMViewControllerActionDelegate <NSObject>

@optional
-(void)moveToViewControllerWithId:(NSString*)viewControllerId animated:(BOOL)animated;

-(void)popToRootViewControllerAndMoveToViewControllerWithId:(NSString*)viewControllerId
                                                   animated:(BOOL)animated;

-(void)popToViewController:(UIViewController*)viewController
moveToViewControllerWithId:(NSString*)viewControllerId
                  animated:(BOOL)animated;;

-(void)didPopViewController:(TMViewControllerType)viewControllerType;
-(void)didEditProfile;
- (void)didUploadProfilePicture;
-(void)setNavigationFlowForLogoutAction:(BOOL)fromDelete;
-(void)didPrivacyClicked:(BOOL)fromCancel;
-(void)deleteAccount:(NSString *)reason subReason:(NSString *)subReason rating:(NSInteger)rating;
-(void)showSelectNudgeOnDeleteAction;

@end

//
//  TMBaseView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseView.h"
#import "TMActivityIndicatorView.h"

@interface TMBaseView ()
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)UIView *translucentView;
@property(nonatomic, strong)UIView *missTMView;
@end

@implementation TMBaseView

-(void)showRetryView {
    self.isRetryViewShown = true;
    self.retryView = [[TMRetryView alloc] initWithFrame:self.bounds];
    [self addSubview:self.retryView];
}
-(void)removeRetryView {
    self.isRetryViewShown = false;
    [self.retryView removeFromSuperview];
    self.retryView = nil;
}

-(void)showRetryViewWithMessage:(NSString*)message {
    self.isRetryViewShown = true;
    self.retryView = [[TMRetryView alloc] initWithFrame:self.bounds message:message];
    [self addSubview:self.retryView];
}

-(void)showActivityIndicatorViewWithTranslucentViewAndMessage:(NSString*)message {
    self.translucentView = [[UIView alloc] initWithFrame:self.bounds];
    self.translucentView.backgroundColor = [UIColor whiteColor];
    self.translucentView.alpha = 0.95;
    [self addSubview:self.translucentView];
    [self bringSubviewToFront:self.translucentView];
    
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor darkGrayColor];
    [self addSubview:self.activityIndicatorView];
    [self bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}

-(void)hideActivityIndicatorViewWithtranslucentView {
    if(self.activityIndicatorView) {
        [self.activityIndicatorView stopAnimating];
        [self.activityIndicatorView removeFromSuperview];
        self.activityIndicatorView = nil;
    }
    
    if(self.translucentView){
        [self.translucentView removeFromSuperview];
        self.translucentView = nil;
    }
}


@end

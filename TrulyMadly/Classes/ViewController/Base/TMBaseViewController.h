//
//  TMBaseViewController.h
//  TrulyMadly
//
//  Created by Ankit on 05/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMViewControllerActionDelegate.h"
#import "TMRetryView.h"


@interface TMBaseViewController : UIViewController

@property(nonatomic,weak)id<TMViewControllerActionDelegate> delegate;
@property(nonatomic,weak)id<TMViewControllerActionDelegate> actionDelegate;
@property(nonatomic,strong)TMRetryView *retryView;
@property(nonatomic,assign)BOOL isRetryViewShown;

-(UIViewController*)rootViewController;
-(CGRect)getMaxUsableFrame;
-(void)addCustombackButton;
-(void)showActivityIndicatorViewWithMessage:(NSString*)message;
-(void)showActivityIndicatorViewWithTranslucentViewAndMessage:(NSString*)message;
-(void)hideActivityIndicatorViewWithtranslucentView;
-(void)hideActivityIndicatorView;
-(void)showAlertWithTitle:(NSString*)title
                      msg:(NSString*)msg
             withDelegate:(id)delegate;
-(void)showRetryView;
-(void)removeRetryView;
-(void)showRetryViewWithMessage:(NSString*)message;

-(BOOL)isTopMostViewController;
-(void)showActivityIndicatorViewMissTMAndMessage:(NSString *)message;
-(void)hideActivityIndicatorViewWithTMViewAndTranslucentView;

@end
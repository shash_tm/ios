//
//  TMSlidingViewController.h
//  TrulyMadly
//
//  Created by Ankit on 09/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "ECSlidingViewController.h"
#import "TMViewControllerActionDelegate.h"

@interface TMSlidingViewController : ECSlidingViewController

@property(nonatomic,weak)id<TMViewControllerActionDelegate>actionDelegate;

@end

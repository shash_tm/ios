//
//  TMBaseViewController.m
//  TrulyMadly
//
//  Created by Ankit on 05/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"
#import "TMActivityIndicatorView.h"
#import "UIColor+TMColorAdditions.h"

@interface TMBaseViewController ()
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)UIView *translucentView;
@property(nonatomic, strong)UIView *missTMView;
@end

@implementation TMBaseViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGRect)getMaxUsableFrame {
    CGFloat topBarHeight = CGRectGetHeight(self.navigationController.navigationBar.frame) + CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    CGFloat totalHeight = CGRectGetMaxY(self.view.frame);
    
    CGFloat maxUsableHeight = totalHeight - topBarHeight;
    CGRect rect = CGRectMake(CGRectGetMinX(self.view.bounds),
                             CGRectGetMinY(self.view.bounds),
                             CGRectGetWidth(self.view.bounds),
                             maxUsableHeight);
    return rect;
}


-(void)addCustombackButton {
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]
                                          initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

-(void)backAction {
    [self.navigationController popViewControllerAnimated:false];
}

-(UIViewController*)rootViewController {
    UIViewController *rootViewCon = nil;
    NSArray *viewControllers = [self.navigationController viewControllers];
    if(viewControllers) {
        rootViewCon = [viewControllers objectAtIndex:0];
    }
    return rootViewCon;
}

-(void)showActivityIndicatorViewWithMessage:(NSString*)message {
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor grayColor];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}
-(void)showActivityIndicatorViewWithTranslucentViewAndMessage:(NSString*)message {
    self.translucentView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.translucentView.backgroundColor = [UIColor whiteColor];
    self.translucentView.alpha = 0.95;
    [self.view addSubview:self.translucentView];
    [self.view bringSubviewToFront:self.translucentView];
    
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor darkGrayColor];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}
-(void) showActivityIndicatorViewMissTMAndMessage:(NSString *)message
{
    self.translucentView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.translucentView.backgroundColor = [UIColor whiteColor];
    self.translucentView.alpha = 0.95;
    [self.view addSubview:self.translucentView];
    
    [self.view addSubview:[self missTMViewWithText]];
    [self.view bringSubviewToFront:self.missTMView];
    
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor loaderMessageTextColor];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleLable.font = [UIFont systemFontOfSize:14.0];//[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    self.activityIndicatorView.titleText = message;
    
    CGFloat yPos = (self.view.bounds.size.height-(self.missTMView.frame.origin.y+self.missTMView.frame.size.height+10+self.activityIndicatorView.frame.size.height))/2;
    CGRect missTMFrame = self.missTMView.frame;
    missTMFrame.origin.y = yPos;
    self.missTMView.frame = missTMFrame;
    
    CGRect activityIndicatorFrame = self.activityIndicatorView.frame;
    activityIndicatorFrame.origin.y = self.missTMView.frame.origin.y+self.missTMView.frame.size.height+14;
    self.activityIndicatorView.frame = activityIndicatorFrame;
    
    [self.activityIndicatorView startAnimating];
    //self.activityIndicatorView.backgroundColor = [UIColor redColor];
}
-(UIView *)missTMViewWithText
{
    self.missTMView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, self.view.bounds.size.width, 140)];
    UILabel *missTMLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
    missTMLabel.text = @"Miss TM";
    missTMLabel.textColor = [UIColor missTMtextColor];
    missTMLabel.textAlignment = NSTextAlignmentCenter;
    missTMLabel.font = [UIFont systemFontOfSize:28.0];//[UIFont fontWithName:@"HelveticaNeue" size:22.0f];
    //missTMLabel.backgroundColor = [UIColor redColor];
    [self.missTMView addSubview:missTMLabel];
    
    UILabel *subLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 26, self.view.bounds.size.width, 20)];
    subLabel.font = [UIFont systemFontOfSize:13.0];//[UIFont fontWithName:@"HelveticaNeue" size:11.0f];
    subLabel.textColor = [UIColor missTMtextColor];
    subLabel.textAlignment = NSTextAlignmentCenter;
    subLabel.text = @"Your Virtual Bestie";
    [self.missTMView addSubview:subLabel];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-80)/2, 60, 80, 80)];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = [UIImage imageNamed:@"MissTM"];
    [self.missTMView addSubview:imageView];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(8.0, 101.5, ((self.view.bounds.size.width-80)/2)-16, 1.0)];
    leftView.backgroundColor = [UIColor missTMLineColor];
    [self.missTMView addSubview:leftView];
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(((self.view.bounds.size.width-80)/2)+88, 101.5, ((self.view.bounds.size.width-80)/2)-16, 1.0)];
    rightView.backgroundColor = [UIColor missTMLineColor];
    [self.missTMView addSubview:rightView];
    
    //self.missTMView.backgroundColor = [UIColor lightGrayColor];
    return self.missTMView;
}
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
}
-(void)hideActivityIndicatorViewWithtranslucentView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
    
    [self.translucentView removeFromSuperview];
    self.translucentView = nil;
}
-(void)hideActivityIndicatorViewWithTMViewAndTranslucentView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
    
    [self.translucentView removeFromSuperview];
    self.translucentView = nil;
    
    if(self.missTMView) {
        [self.missTMView removeFromSuperview];
        self.missTMView = nil;
    }
}
-(void)showRetryView {
    self.isRetryViewShown = true;
    self.retryView = [[TMRetryView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.retryView];
}
-(void)removeRetryView {
    self.isRetryViewShown = false;
    [self.retryView removeFromSuperview];
    self.retryView = nil;
}

-(void)showRetryViewWithMessage:(NSString*)message {
    self.isRetryViewShown = true;
    self.retryView = [[TMRetryView alloc] initWithFrame:self.view.bounds message:message];
    [self.view addSubview:self.retryView];
}

-(BOOL)isTopMostViewController {
    if(self.isViewLoaded && self.view.window) {
        return true;
    }
    return false;
}

#pragma mark -  Alert Handler
#pragma mark -
-(void)showAlertWithTitle:(NSString*)title
                      msg:(NSString*)msg
             withDelegate:(id)delegate  {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    });
    
}

@end

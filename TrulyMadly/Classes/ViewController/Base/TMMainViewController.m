//
//  TMMainViewController.m
//  TrulyMadly
//
//  Created by Ankit on 05/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMainViewController.h"
#import "TMSlidingViewController.h"
#import "TMSideMenuViewController.h"
#import "TMPagedScrollViewController.h"
#import "TMPhotoViewController.h"
#import "TMMatchesViewController.h"
#import "TMLoginViewController.h"
#import "TMMyProfileViewController.h"
#import "TMRegistrationViewController.h"
#import "TMActivityIndicatorView.h"

#import "TMMessagingController.h"
#import "TMUserDataController.h"
#import "TMUserDataManager.h"

#import "TMRetryView.h"
#import "TMAnalytics.h"
#import "TMSwiftHeader.h"
#import "TMEnums.h"
#import "TMLog.h"
#import "MoEngage.h"



@interface TMMainViewController ()<PagedScrollViewControllerDelegate,TMLoginViewControllerDelegate,TMViewControllerActionDelegate>

@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)TMRetryView *retryView;

@end

@implementation TMMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    TMUserSession *userSession = [TMUserSession sharedInstance];
    BOOL isUserLoggedIn = [userSession isLogin];
    
    if(isUserLoggedIn){
        BOOL moveToMatches = [userSession navigateUserToMatches];
        if(moveToMatches) {
            //move to match
            [self navigateToViewControllerWithType:TMMATCHVIEWCONTROLLER animated:false];
        }
        else {
            //call to server for checking user state
            [self checkUserState];
        }
    }else {
        TMPagedScrollViewController *pageScrollVC = [self pageScrollViewController];
        [self.navigationController pushViewController:pageScrollVC animated:NO];
    }
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:70/255.0f green:163/255.0f blue:197/255.0f alpha:1.0]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User State Handler
#pragma mark -

-(void)checkUserState {
    [self showActivityIndicatorViewWithMessage:nil];
    
    TMUserDataController* userDataController = [[TMUserDataController alloc] init];
    [userDataController getUserStateWithResponse:^(TMError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideActivityIndicatorView];
            if(!error) {
                [self moveToViewControllerForCurrentUserStatus];
            }
            else {
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self logoutUserByPopingToRootViewController:FALSE fromDelete:false];
                }
                else { //show retry view for all other errord
                    [self showRetryView];
                }
            }
        });
    }];
}

- (void)sendTokenToServer:(NSString*)status {
    NSString *token = [TMDataStore retrieveObjectforKey:@"deviceToken"];
    if(token != nil) {
        [TMNotification sendToken:status token:token];
    }
}

-(void)showActivityIndicatorViewWithMessage:(NSString*)message {
    [self removeRetryView];
    
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor grayColor];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
}
-(void)showRetryView {
    if(self.retryView) {
        [self removeRetryView];
    }
    self.retryView = [[TMRetryView alloc] initWithFrame:self.view.bounds];
    [self.retryView.retryButton addTarget:self action:@selector(retryAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.retryView];
}
-(void)removeRetryView {
    [self.retryView removeFromSuperview];
    self.retryView = nil;
}

-(void)retryAction {
    [self checkUserState];
}
#pragma mark - PageViewController Delegate
#pragma mark -

- (void)onClick:(NSString *)key sender:(UIViewController *)sender {
    if([key isEqualToString:@"login"]) {
        [self navigateToViewControllerWithType:TMLOGINVIEWCONTROLLER animated:false];
    }
    else if ([key isEqualToString:@"matches"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self navigateToViewControllerWithType:TMMATCHVIEWCONTROLLER animated:false];
        [self checkUserForChatContent];
    }
    else if([key isEqualToString:@"registerBasics"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self navigateToViewControllerWithType:TMREGISTRATIONBASICSVIEWCONTROLLER animated:false];
        [self checkUserForChatContent];
    }
    else if ([key isEqualToString:@"logout"]) {
        [self setNavigationFlowForLogoutAction:false];
    }

}

#pragma mark - LoginViewController Delegate
#pragma mark -

-(void)delegateForLogin:(NSString*)key sender:(UIViewController*)sender {
    if ([key isEqualToString:@"matches"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self navigateToViewControllerWithType:TMMATCHVIEWCONTROLLER animated:false];
        [self checkUserForChatContent];
    }
    else if([key isEqualToString:@"registerBasics"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self navigateToViewControllerWithType:TMREGISTRATIONBASICSVIEWCONTROLLER animated:false];
        [self checkUserForChatContent];
    }
    else if([key isEqualToString:@"signup"]) {
        [self.navigationController popViewControllerAnimated:FALSE];
    }
    else if ([key isEqualToString:@"logout"]) {
        [self setNavigationFlowForLogoutAction:false];
    }
}

#pragma mark - TMViewControllerActionDelegate Delegate
#pragma mark -

-(void)moveToViewControllerWithId:(NSString*)viewControllerId animated:(BOOL)animated {
    
    if([viewControllerId isEqualToString:@"matches"]) {
        [self navigateToViewControllerWithType:TMMATCHVIEWCONTROLLER animated:false];
    }
}

-(void)setNavigationFlowForLogoutAction:(BOOL)fromDelete {
    [self logoutUserByPopingToRootViewController:TRUE fromDelete:fromDelete];
}


#pragma mark - ViewController Flow Handler
#pragma mark -

-(void)moveToViewControllerForCurrentUserStatus {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    TMUser *user = userSession.user;
    if( ([user userStatus] == AUTHENTIC) || ([user userStatus] == NON_AUTHENTIC) ) {
        //move to matches
        [self navigateToViewControllerWithType:TMMATCHVIEWCONTROLLER animated:false];
    }
    else if([user userStatus] == INCOMPLETE)  {
        //move to registration basic
        [self navigateToViewControllerWithType:TMREGISTRATIONBASICSVIEWCONTROLLER animated:false];
    }
    else if([user userStatus] == SUSPENDED) {
        [self logoutUserByPopingToRootViewController:FALSE fromDelete:false];
    }
    else if([user userStatus] == BLOCKED) {
        [self logoutUserByPopingToRootViewController:FALSE fromDelete:false];
    }
}

-(void)navigateToViewControllerWithType:(TMViewControllerType)viewControllerType
                               animated:(BOOL)animated {
    UIViewController *viewController = nil;
    if(viewControllerType == TMLOGINVIEWCONTROLLER) {
        viewController = [self loginViewController];
    }
    else if (viewControllerType == TMREGISTRATIONBASICSVIEWCONTROLLER) {
        viewController = [self registrationBasicsViewController];
    }
    else if (viewControllerType == TMTRUSTBUILDERVIEWCONTROLLER) {
        viewController = [self trustBuilderViewcontroller];
    }
    else if (viewControllerType == TMPHOTOVIEWCONTROLLER) {
        viewController = [self photoViewController];
    }
    else if (viewControllerType == TMMATCHVIEWCONTROLLER) {
        viewController = [self matchViewController];
    }
    [self.navigationController pushViewController:viewController animated:animated];
}

-(TMPagedScrollViewController*)pageScrollViewController {
    TMPagedScrollViewController *pageScrollViewCon = [[TMPagedScrollViewController alloc] init];
    pageScrollViewCon.delegate = self;
    return pageScrollViewCon;
}
-(UIViewController*)matchViewController {
    TMSlidingViewController *slidingViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"slidingvc"];
    slidingViewCon.anchorRightRevealAmount = [[UIScreen mainScreen] bounds].size.width*0.8;
    slidingViewCon.actionDelegate = self;
    self.navigationController.navigationBarHidden = YES;
    return slidingViewCon;
}

-(UIViewController*)registrationBasicsViewController {
    TMRegistrationViewController* registrationBasicViewCon = [[TMRegistrationViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION];
    registrationBasicViewCon.delegate = self;
    registrationBasicViewCon.actionDelegate = self;
    return registrationBasicViewCon;
}
-(UIViewController*)loginViewController {
    TMLoginViewController *loginViewController = [[TMLoginViewController alloc] init];
    loginViewController.delegate = self;
    return loginViewController;
}
-(UIViewController*)trustBuilderViewcontroller {
    TMTrustBuilderViewController *trustViewCon = [[TMTrustBuilderViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION];
    trustViewCon.delegate = self;
    trustViewCon.actionDelegate = self;
    return trustViewCon;
}

-(UIViewController*)photoViewController {
    TMPhotoViewController *photoViewCon = [[TMPhotoViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION];
    photoViewCon.delegate = self;
    photoViewCon.actionDelegate = self;
    return photoViewCon;
}

-(void)popToRootViewControllerAndMoveToViewControllerWithId:(NSString*)viewControllerId
                                                   animated:(BOOL)animated {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    if([viewControllerId isEqualToString:@"matches"]) {
        [self navigateToViewControllerWithType:TMMATCHVIEWCONTROLLER animated:false];
    }
    else if([viewControllerId isEqualToString:@"photovc"]) {
        [self navigateToViewControllerWithType:TMPHOTOVIEWCONTROLLER animated:false];
    }
    else if([viewControllerId isEqualToString:@"quiz"]) {
        [self navigateToViewControllerWithType:TMQUIZVIEWCONTROLLER animated:false];
    }
    else if([viewControllerId isEqualToString:@"trustbuildervc"]) {
        [self navigateToViewControllerWithType:TMTRUSTBUILDERVIEWCONTROLLER animated:false];
    }else if([viewControllerId isEqualToString:@"myProfile"]) {
        TMMyProfileViewController *myProfileViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"myprofilevc"];
        myProfileViewCon.actionDelegate = self;
        myProfileViewCon.fromRegistration = TRUE;
        [self.navigationController pushViewController:myProfileViewCon animated:animated];
    }
}

#pragma mark - Logout Handler
#pragma mark -

-(void)logoutUserByPopingToRootViewController:(BOOL)popToRootviewController fromDelete:(BOOL)fromDelete {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clearchatdata" object:nil];
//    TMMatchesViewController *matchViewCon = nil;
//    TMSlidingViewController *slidingViewCon = (TMSlidingViewController*)[self.navigationController topViewController];
//    UINavigationController *matchNavCon = (UINavigationController*)[slidingViewCon topViewController];
//    NSArray *viewControllers = [matchNavCon viewControllers];
//    if(viewControllers.count) {
//        matchViewCon = (TMMatchesViewController*)viewControllers[0];
//        [matchViewCon clearMatchCacheData];
//    }
    
    [[TMMessagingController sharedController] disconnectChat];
    
    TMUserDataManager *loginMgr = [[TMUserDataManager alloc] init];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    
    //execite logout call
    [loginMgr logout:dict with:^(NSString *status, TMError *error) {
        if([status isEqualToString:@"success"]) {
             [[TMUserSession sharedInstance].user setUserDefaults];
             [[MoEngage sharedInstance] resetUser];
        }
    }];
    
    //track pogout event
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"logout" forKey:@"screenName"];
    [eventDict setObject:@"logout" forKey:@"eventCategory"];
    [eventDict setObject:@"click" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
    [self sendTokenToServer:@"logout"];
    
    ///pop to root view controller
    if(popToRootviewController) {
        [self.navigationController setNavigationBarHidden:true];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    
    TMPagedScrollViewController *pageScrollVC = [self pageScrollViewController];
    pageScrollVC.fromDelete = fromDelete;
    [self.navigationController pushViewController:pageScrollVC animated:NO];
}




#pragma mark - Error Alert Handler
#pragma mark -

-(void)showAlert:(NSString*)title
                                 msg:(NSString*)msg  {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

-(void) errorResponse:(TMError*)err {
    if([err errorCode] == TMERRORCODE_LOGOUT) {
        [self logoutUserByPopingToRootViewController:TRUE fromDelete:false];
    }else if([err errorCode] == TMERRORCODE_NONETWORK) {
        [self showAlert:@"" msg:TM_INTERNET_NOTAVAILABLE_MSG];
    }else if([err errorCode] == TMERRORCODE_DATASAVEERROR) {
        [self showAlert:@"" msg:[err errorMessage]];
    }else {
        [self showAlert:@"" msg:@"Unknown Error"];
    }
}

-(void)checkUserForChatContent {
    NSString *cachedUserId = [TMDataStore retrieveObjectforKey:@"cuserid"];
    NSString *currentUserId = [TMUserSession sharedInstance].user.userId;
    if(cachedUserId && (![cachedUserId isEqualToString:currentUserId])) {
        //delete all messages
        [[TMMessagingController sharedController] deleteAllMessageContent];
        [TMDataStore removeObjectforKey:@"rowupdatedts"];
        [TMDataStore removeObjectforKey:@"firstconvlistreq_status"];
    }
    
    [TMDataStore setObject:currentUserId forKey:@"cuserid"];
    
    [[TMMessagingController sharedController] fetchAndCacheDeletedConversationData];
}

@end

//
//  TMBaseView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMRetryView.h"

@interface TMBaseView : UIView

@property(nonatomic,strong)TMRetryView *retryView;
@property(nonatomic,assign)BOOL isRetryViewShown;

-(void)showRetryView;
-(void)removeRetryView;
-(void)showRetryViewWithMessage:(NSString*)message;

-(void)showActivityIndicatorViewWithTranslucentViewAndMessage:(NSString*)message;
-(void)hideActivityIndicatorViewWithtranslucentView;

@end

//
//  TMMyProfileViewController.m
//  TrulyMadly
//
//  Created by Ankit on 08/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMyProfileViewController.h"
#import "TMMatchManager.h"
#import "TMFullScreenPhotoViewController.h"
#import "TMPhotoViewController.h"
#import "TMSwiftHeader.h"
#import "TMDeactivateProfileViewController.h"
#import "TMErrorMessages.h"
#import "TMProfileCollectionView.h"
#import "TMFavouritesViewController.h"
#import "TMSystemMessage.h"
#import "TMToastView.h"
#import "TMHashTagTextViewController.h"
#import "TMNativeAdWebViewController.h"
#import "TMAdTrackingModel.h"
#import "TMAdTrackingManager.h"
#import "TMProfile.h"
#import "TMAppViralityController.h"
#import "TMUserDataController.h"
#import "TMRegisterBasicsViewControllerNew.h"
#import "TMNewEduViewController.h"

@interface TMMyProfileViewController ()<TMViewControllerActionDelegate,UIAlertViewDelegate,TMDeactivateProfileViewControllerDelegate,TMFavouritesViewControllerDelegate>

@property(nonatomic,assign)BOOL isProfileUpdated;
@property(nonatomic,assign)BOOL isTemp;
@property(nonatomic,strong)TMMatchManager *matchMgr;
@property(nonatomic,strong)TMProfileResponse *profileResponse;
@property(nonatomic,strong)TMDeactivateProfileViewController *deactivateViewController;
@property(nonatomic,strong)NSMutableDictionary* eventDict;
@property(nonatomic,assign)TMNavigationFlow navigationFlow;
@property(nonatomic,strong)TMAdTrackingManager *adTrackerManager;

@end

@implementation TMMyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Edit Profile";
    [self.navigationController setNavigationBarHidden:false animated: false];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.isProfileUpdated = false;
    self.isEditMode = true;
    
    if(self.fromRegistration) {
        [self.navigationItem setHidesBackButton:true animated:false];
        self.title = @"My Profile";
        self.navigationFlow = TMNAVIGATIONFLOW_REGISTRATION;
        [TMAppViralityController trackAppViralityEvent:RegistrationComplete];
    }else {
        [self configureProfileUIComponents];
        self.navigationFlow = TMNAVIGATIONFLOW_EDITPROFILE;
    }

    [self loadMyProfileData];
    
    [[TMAnalytics sharedInstance] trackView:@"TMMyProfileViewController"];

}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.isProfileUpdated) {
        self.isProfileUpdated = false;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(TMMatchManager*)matchMgr {
    if(_matchMgr == nil) {
        _matchMgr = [[TMMatchManager alloc] init];
    }
    return _matchMgr;
}

-(void)dealloc {
    self.matchMgr = nil;
    self.adTrackerManager = nil;
}

-(TMAdTrackingManager*)adTrackerManager {
    if(_adTrackerManager == nil) {
        _adTrackerManager = [[TMAdTrackingManager alloc] init];
    }
    return _adTrackerManager;
}


#pragma mark View Components Creation Methods
#pragma mark -

-(void)configureProfileUIComponents {
    //[self addDeactivateProfileBarButton];
}
-(void)addBackButton {
    //
    UIButton *menuBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    menuBtn.frame = CGRectMake(0, 0, 44, 44);
    [menuBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    menuBtn.backgroundColor = [UIColor clearColor];
    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 30, 20)];
    imgview.image = [UIImage imageNamed:@"back_button"];
    [menuBtn addSubview:imgview];
    UIBarButtonItem *leftMenuButton = [[UIBarButtonItem alloc]
                                       initWithCustomView:menuBtn];
    self.navigationItem.leftBarButtonItem = leftMenuButton;
}
-(void)addDeactivateProfileBarButton {
    UIButton *mutualMatchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    mutualMatchBtn.frame = CGRectMake(0, 0, 44, 44);
    [mutualMatchBtn addTarget:self action:@selector(deactivateProfileAction) forControlEvents:UIControlEventTouchUpInside];
    mutualMatchBtn.backgroundColor = [UIColor clearColor];
    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(25, 10, 25, 25)];
    imgview.image = [UIImage imageNamed:@"deactivate"];
    [mutualMatchBtn addSubview:imgview];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]
                                       initWithCustomView:mutualMatchBtn];
    self.navigationItem.rightBarButtonItem = rightBarButton;
}
-(void)addContinueButton {
    UIButton *continueButton = [[UIButton alloc] initWithFrame:CGRectMake(10, self.view.bounds.size.height-50, self.view.bounds.size.width-20, 44)];
    continueButton.frame = CGRectMake(10, self.view.bounds.size.height-50, self.view.bounds.size.width-20, 44);
    [continueButton addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    [continueButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    [continueButton setBackgroundColor:[UIColor colorWithRed:1.0 green: 0.702 blue: 0.725 alpha: 1.0]];
    continueButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0 ];
    [continueButton setTitle:@"Let’s Get Started" forState:UIControlStateNormal];
    continueButton.layer.cornerRadius = 10;
    [self.view addSubview:continueButton];
    [self.view bringSubviewToFront:continueButton];
}
-(void)backAction {
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)continueAction {
    [self.actionDelegate popToRootViewControllerAndMoveToViewControllerWithId:@"matches" animated:true];
}

-(void)showProfile {
    TMProfile *matchProfile = [self currentMatchProfile];
    self.profile = matchProfile;
    [self configureProfileCollectionViewWithProfileData];
    
    if(self.fromRegistration) {
        self.collectionView.contentInset=UIEdgeInsetsMake(64.0,0.0,74.0,0.0);
        self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(64.0,0.0,74.0,0.0);
    }
    else {
        self.collectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
        self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
    }

    [self.view addSubview:self.collectionView];
    if(self.fromRegistration) {
        [self addContinueButton];
    }
    
    if(self.from_nudge) {
        // add nudge toast
        [TMToastView showToastInParentViewWithAlertIcon:self.view withText:@"Complete the missing info" withDuaration:4.0 presentationDirection:TMToastPresentationFromTop];
    }
    
    // call the impression tracker if profile is an ad campaign
    if(matchProfile.isProfileAdCampaign) {
        NSDictionary* matchDetails = @{@"source":@"profile"};
        if(self.profile.matchId){
            matchDetails = @{@"source":@"profile",@"match_id":matchProfile.matchId};
        }
        [self.adTrackerManager trackImpressionUrls:matchProfile.adObj.impressionTracker eventInfo:matchDetails];
    }
}
-(void)configureViewForDataLoadingStart {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading Profile"];
    self.view.userInteractionEnabled = false;
    self.collectionView.alpha = 0.1;
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
    self.view.userInteractionEnabled = true;
    self.collectionView.alpha = 1;
}
-(void)configureUIForDeactivationCall {
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    self.navigationItem.hidesBackButton = YES;
    self.view.userInteractionEnabled = false;
}
-(void)configureUIForDeactivationCallError {
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.backBarButtonItem setEnabled:YES];
    self.view.userInteractionEnabled = true;
    if(!self.fromRegistration) {
        self.navigationItem.hidesBackButton = NO;
    }
}
-(void)removeSubViewsForDataReloading {
    NSArray *subviews = [self.view subviews];
    for (int i=0; i<subviews.count; i++) {
        UIView *subview = [subviews objectAtIndex:i];
        [subview removeFromSuperview];
    }
}

#pragma mark Profile Data Loading
#pragma mark -

-(TMProfile*)currentMatchProfile {
    TMProfile *matchProfile = [self.profileResponse profileForIndex:0 editMode:true];
    return matchProfile;
}
-(void)loadMyProfileData {
  //  if(self.matchMgr.isNetworkReachable) {
        NSMutableDictionary *trackParams = [[NSMutableDictionary alloc] init];
        trackParams[@"screenName"] = @"TMMyProfileViewController";
        trackParams[@"eventCategory"] = @"profile";
        trackParams[@"eventAction"] = @"page_load";
        self.matchMgr.trackEventDictionary = trackParams;
        /////
        [self removeSubViewsForDataReloading];
        [self configureViewForDataLoadingStart];
        ///////
        [self.matchMgr getMyProfileData:^(TMProfileResponse *profileResponse, TMError *error) {
            [self configureViewForDataLoadingFinish];
            if(profileResponse && !error) {
                self.profileResponse = profileResponse;
                [self showProfile];
            }
            else {
                //error case
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    //remove modal vc and show alart for network error
                    //[self showProfileResponseErrorAlert:PROFILE_LOADING_ERROR_TITLE msg:TM_INTERNET_NOTAVAILABLE_MSG];
                    if (![self.matchMgr isCachedResponsePresentForMyProfile]) {
                        if (error.errorCode == TMERRORCODE_NONETWORK) {
                            [self showRetryView];
                             [self.retryView.retryButton addTarget:self action:@selector(loadMyProfileData) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else {
                             [self showRetryViewAtError];
                             [self.retryView.retryButton addTarget:self action:@selector(loadMyProfileData) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                }
                else {
                    //unknwon error
                    //[self showProfileResponseErrorAlert:PROFILE_LOADING_ERROR_TITLE msg:PLEASETRYLATER_ERROR_MSG];
                    [self showRetryViewAtUnknownError];
                     [self.retryView.retryButton addTarget:self action:@selector(loadMyProfileData) forControlEvents:UIControlEventTouchUpInside];
                }
            }
        }];
//    }
//    else {

        //removing retry view from here
//        if ([self.matchMgr isNetworkReachable]) {
//            [self showRetryViewAtError];
//        }
//        
       // UIAlertView *alert
        //[self showProfileResponseErrorAlert:PROFILE_LOADING_ERROR_TITLE msg:TM_INTERNET_NOTAVAILABLE_MSG];
  //  }
    
}
-(void)reloadMyProfileData {
    [self.matchMgr deleteCacheResponseForMyProfile];
    [self loadMyProfileData];
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadMyProfileData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadMyProfileData) forControlEvents:UIControlEventTouchUpInside];
    }
}


#pragma mark Collection view delegate
#pragma mark -
//- (void)collectionView:(TMProfileCollectionView *)collectionView
//                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
//        takeQuizAction:(TMQuizScoreType)quizType {
//    /////
//    if(quizType == TMQUIZSCORETYPE_VALUE) {
//        TMValueQuizzViewController *valueQuizVC = [[TMValueQuizzViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
//        valueQuizVC.delegate = self;
//        [self.navigationController pushViewController:valueQuizVC animated:YES];
//    }
//    else if(quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) {
//        TMAdaptabilityViewController *adapVIewCon = [[TMAdaptabilityViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
//        adapVIewCon.delegate = self;
//        [self.navigationController pushViewController:adapVIewCon animated:true];
//    }
//    /////
//}

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
editProfileActionWithModule:(TMEditProfileModule)editProfileModule {
    
    if(editProfileModule == TMEDITPROFILE_PHOTO) {
        TMPhotoViewController *photoVc = ([TMUserSession sharedInstance].user.isFemaleProfileRejected)?[[TMPhotoViewController alloc] initForFemaleProfileRejectionWithNavigationFlow:self.navigationFlow alertMessage:nil]:[[TMPhotoViewController alloc] initWithNavigationFlow:self.navigationFlow];
        photoVc.delegate = self;
        photoVc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:photoVc animated:YES];
    }
    else if(editProfileModule == TMEDITPROFILE_BASICREGISTRATION) {

//        TMRegisterBasicsViewController *regBasics = [[TMRegisterBasicsViewController alloc] initWithNavigationFlow:self.navigationFlow];
//        regBasics.delegate = self;
//        regBasics.actionDelegate = self.actionDelegate;
        TMNavigationFlow navigationFlow = (self.fromRegistration) ? TMNAVIGATIONFLOW_REGISTRATION : TMNAVIGATIONFLOW_EDITPROFILE;
        TMRegisterBasicsViewControllerNew* registrationViewController = [[TMRegisterBasicsViewControllerNew alloc]
                                                                         initWithNavigationFlow:navigationFlow];
        registrationViewController.delegate = self;
        registrationViewController.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:registrationViewController animated:YES];
    }
    else if(editProfileModule == TMEDITPROFILE_WORK) {
        //TMWorkViewController *wevc = [[TMWorkViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE registerData:nil];
        TMNavigationFlow navigationFlow = (self.fromRegistration) ? TMNAVIGATIONFLOW_REGISTRATION : TMNAVIGATIONFLOW_EDITPROFILE;
        TMWorkViewController *wevc = [[TMWorkViewController alloc] initWithNavigationFlow:navigationFlow registerData:nil];
        wevc.delegate = self;
        wevc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:wevc animated:YES];
    }
    else if(editProfileModule == TMEDITPROFILE_EDU) {
        TMNavigationFlow navigationFlow = (self.fromRegistration) ? TMNAVIGATIONFLOW_REGISTRATION : TMNAVIGATIONFLOW_EDITPROFILE;
        TMNewEduViewController *wevc = [[TMNewEduViewController alloc] initWithNavigationFlow:navigationFlow registerData:nil];
        wevc.delegate = self;
        wevc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:wevc animated:YES];
    }
    else if(editProfileModule == TMEDITPROFILE_FAVOURITE) {
        //TMFavouritesViewController *favViewController = [[TMFavouritesViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
        TMNavigationFlow navigationFlow = (self.fromRegistration) ? TMNAVIGATIONFLOW_REGISTRATION : TMNAVIGATIONFLOW_EDITPROFILE;
        TMFavouritesViewController *favViewController = [[TMFavouritesViewController alloc] initWithNavigationFlow:navigationFlow];
        favViewController.delegate = self;
        favViewController.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:favViewController animated:YES];
    }
    else if(editProfileModule == TMEDITPROFILE_INTEREST) {
        //TMInterestViewController *invc = [[TMInterestViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE registerData:nil];
//        TMInterestViewController *invc = [[TMInterestViewController alloc] initWithNavigationFlow:self.navigationFlow registerData:nil];
//        invc.delegate = self;
//        invc.actionDelegate = self.actionDelegate;
//        [self.navigationController pushViewController:invc animated:YES];
        TMHashTagTextViewController *invc = [[TMHashTagTextViewController alloc] initWithNavigationFlow:self.navigationFlow registerData:nil showBar:false];
        invc.delegate = self;
        invc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:invc animated:YES];
    }
    
    else if(editProfileModule == TMEDITPROFILE_TRUSTBUILDER) {
        //TMTrustBuilderViewController *trvc = [[TMTrustBuilderViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
        TMTrustBuilderViewController *trvc = [[TMTrustBuilderViewController alloc] initWithNavigationFlow:self.navigationFlow];
        trvc.delegate = self;
        trvc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:trvc animated:YES];
    }
}

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
fullScreenPhotoViewAction:(NSArray *)images currentIndex:(NSInteger)index {
    TMProfile *matchProfile = [self currentMatchProfile];
    if(matchProfile.isProfileAdCampaign) {
        // call click trackers
        NSDictionary* matchDetails = @{@"source":@"profile"};
        if(self.profile.matchId){
            matchDetails = @{@"source":@"profile",@"match_id":matchProfile.matchId};
        }
        [self.adTrackerManager trackClickUrls:matchProfile.adObj.clickTracker eventInfo:matchDetails];
        
        if(matchProfile.adObj && matchProfile.adObj.isLandingUrlAvailable) {
            //open web view with landing url
            [self showAdWebView:matchProfile];
        }else {
            TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] initWithReportUserOption:true];
            fullScreenViewCon.currentIndex = index;
            [self.navigationController pushViewController:fullScreenViewCon animated:YES];
            [fullScreenViewCon loadImgArray:images];
        }
    }else{
        TMPhotoViewController *photoVc = ([TMUserSession sharedInstance].user.isFemaleProfileRejected)?[[TMPhotoViewController alloc] initForFemaleProfileRejectionWithNavigationFlow:self.navigationFlow alertMessage:nil]:[[TMPhotoViewController alloc] initWithNavigationFlow:self.navigationFlow];
        photoVc.delegate = self;
        photoVc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:photoVc animated:YES];
//        TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] init];
//        fullScreenViewCon.currentIndex = index;
//        [self.navigationController pushViewController:fullScreenViewCon animated:YES];
//        [fullScreenViewCon loadImgArray:images];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //NSLog(@"HAHAHAHAH");
}


#pragma mark - Show Web view on ad click
- (void)showAdWebView:(TMProfile *)matchProfile {
    //add the web view
    NSString* adLandringURLString = matchProfile.adObj.landingUrl;
    
    NSString* webViewTitle = @"";
    
    TMNativeAdWebViewController* webViewController = [[TMNativeAdWebViewController alloc] initWithNibName:@"TMNativeAdWebViewController" bundle:nil urlString:adLandringURLString webViewTitle:webViewTitle];
    [self presentViewController:webViewController animated:YES completion:nil];
}

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
          didTapFBSync:(BOOL)tapFB {
    if(tapFB) {
        NSMutableDictionary *trackParams = [[NSMutableDictionary alloc] init];
        trackParams[@"screenName"] = @"TMMyProfileViewController";
        trackParams[@"eventCategory"] = @"fbSync";
        trackParams[@"eventAction"] = @"click";
        trackParams[@"status"] = @"success";
        [[TMAnalytics sharedInstance] trackNetworkEvent:trackParams];
        
        if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
            NSString *token = [FBSession activeSession].accessTokenData.accessToken;
            [self syncFacebookData:token];
        } else {
            [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_photos",@"user_relationships",@"user_actions.music",@"user_actions.books",@"user_actions.video",@"user_friends",@"user_birthday",@"user_likes",@"user_work_history",@"user_education_history",@"user_location"] allowLoginUI:YES completionHandler:
             ^(FBSession *session, FBSessionState state, NSError *error) {
                 if (!error && state == FBSessionStateOpen){
                     NSString *token = [FBSession activeSession].accessTokenData.accessToken;
                     [self syncFacebookData:token];
                 }
                 if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed) {
                     // If the session is closed
                 }
             }];
        }
    }
}

-(void)syncFacebookData:(NSString*)token {
    if (self.matchMgr.isNetworkReachable) {
        NSMutableDictionary *trackParams = [[NSMutableDictionary alloc] init];
        trackParams[@"screenName"] = @"TMMyProfileViewController";
        trackParams[@"eventCategory"] = @"fbSync";
        trackParams[@"eventAction"] = @"server_call";
        self.matchMgr.trackEventDictionary = trackParams;
        [self configureUIForDeactivationCall];
        [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Updating your info..."];
        [self.matchMgr updateFBProfile:token response:^(BOOL status, TMError *error) {
    
            [self hideActivityIndicatorViewWithtranslucentView];
            [self configureUIForDeactivationCallError];
            if((!error) && (status)) {
                [self reloadMyProfileData];
            }
            else {
                if(error.errorMessage) {
                    [[FBSession activeSession] closeAndClearTokenInformation];
                    [self showDeactivateProfileErrorAlert:error.errorMessage msg:@""];
                }else {
                    [self showDeactivateProfileErrorAlert:@"Problem in updating your profile. Please Try Later." msg:@""];
                }
                // error handling
            }
        }];
    }
}

#pragma mark Module ViewController Delegate Handler
#pragma mark -

-(void)didEditProfile {
    self.from_nudge = false;
    [self reloadMyProfileData];
    TMUserSession *userSession = [TMUserSession sharedInstance];
    userSession.isUserProfileUpdated = true;
    self.isProfileUpdated = true;
    //[self.delegate reloadMatchDataOnEditProfile];
}

-(void)moveToDeactivateViewController {
    NSDictionary *deactivationDict = [self.profileResponse.systemMessage deactivationDictionary];
    self.deactivateViewController = [[TMDeactivateProfileViewController alloc] initWithDeactivationDictionary:deactivationDict];
    self.deactivateViewController.delegate = self;
    
    ///push with navigation controller
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.deactivateViewController];
    [self presentViewController:navController animated:YES completion:nil];
}
-(void)dismissDeactivateViewController {
    [self.deactivateViewController dismissViewControllerAnimated:NO completion:nil];
}

-(void)showDeactivateProfileErrorAlert:(NSString*)title
                           msg:(NSString*)msg  {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark Quiz VIew COntroller Delegate
#pragma mark -

//-(void)delegateForValues:(NSString*)key {
//    if([key isEqualToString:@"logout"]) {
//        [self.actionDelegate setNavigationFlowForLogoutAction:false];
//    }
//    else {
//        [self reloadMyProfileData];
//        TMUserSession *userSession = [TMUserSession sharedInstance];
//        userSession.isUserProfileUpdated = true;
//        self.isProfileUpdated = true;
//        //[self.delegate reloadMatchDataOnEditProfile];
//    }
//}

//- (void)delegateForAdaptability:(NSString *)key {
//    if([key isEqualToString:@"logout"]) {
//        [self.actionDelegate setNavigationFlowForLogoutAction:false];
//    }
//    else {
//        [self reloadMyProfileData];
//        TMUserSession *userSession = [TMUserSession sharedInstance];
//        userSession.isUserProfileUpdated = true;
//        self.isProfileUpdated = true;
//        //[self.delegate reloadMatchDataOnEditProfile];
//    }
//}

-(void)didEditFavourite {
    [self reloadMyProfileData];
    TMUserSession *userSession = [TMUserSession sharedInstance];
    userSession.isUserProfileUpdated = true;
    self.isProfileUpdated = true;
    //[self.delegate reloadMatchDataOnEditProfile];
}

#pragma mark DeActivate Profile
#pragma mark-

-(void)deactivateProfileAction {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Do you want to deactivate your profile" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
       //confirm action
        [self moveToDeactivateViewController];
    }
}

-(void)didConformDeactivationWithReason:(NSString *)reason {
    ///dismiss deactivate viewcontroller
    [self dismissDeactivateViewController];
    
    ///
    if(self.matchMgr.isNetworkReachable) {
        [self deactivateProfileWithReason:reason];
    }
    else {
        [self showDeactivateProfileErrorAlert:DEACTIVATE_PROFILE_ERROR_TITLE
                                          msg:TM_INTERNET_NOTAVAILABLE_MSG];
    }
    
}

-(void)deactivateProfileWithReason:(NSString*)deactivateReason {
    [self configureUIForDeactivationCall];
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Deactivating your account..."];
    [self.matchMgr deactivateProfile:deactivateReason
                            response:^(BOOL status, TMError *error) {
        
        [self hideActivityIndicatorView];
        if((!error) && (status)) {
            [self.actionDelegate setNavigationFlowForLogoutAction:false];
        }
        else {
            //error handling
            [self configureUIForDeactivationCallError];
            if(error.errorCode == TMERRORCODE_LOGOUT) {
                [self.actionDelegate setNavigationFlowForLogoutAction:false];
            }
            else if(error.errorCode == TMERRORCODE_NONETWORK) {
                //remove modal vc and show alart for network error
                [self showDeactivateProfileErrorAlert:DEACTIVATE_PROFILE_ERROR_TITLE
                                                  msg:TM_INTERNET_NOTAVAILABLE_MSG];
            }
            else {
                //unknwon error
                [self showDeactivateProfileErrorAlert:DEACTIVATE_PROFILE_ERROR_TITLE
                                                  msg:PLEASETRYLATER_ERROR_MSG];
            }
        }
    }];
}

#pragma mark - Error Alert Handler
#pragma mark -
-(void)showProfileResponseErrorAlert:(NSString*)title
                               msg:(NSString*)msg  {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

@end

//
//  TMDeactivateProfileViewController.m
//  TrulyMadly
//
//  Created by Ankit on 14/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMDeactivateProfileViewController.h"
#import "TMActivityIndicatorView.h"
#import "NSString+TMAdditions.h"


@interface TMDeactivateProfileViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property(nonatomic,strong)UITableView *deactivateReasonTableView;
@property(nonatomic,strong)NSArray *deactivateReasonList;
@property(nonatomic,strong)NSString *deactivateHeading;
@property(nonatomic,strong)NSIndexPath *currentIndex;
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)UITextField *textField;

@end

@implementation TMDeactivateProfileViewController

-(instancetype)initWithDeactivationDictionary:(NSDictionary*)deactivationDictionary {
    self = [super init];
    if(self) {
        self.currentIndex = nil;
        self.deactivateReasonList = [deactivationDictionary objectForKey:@"reasons"];
        self.deactivateHeading = [deactivationDictionary objectForKey:@"heading"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:FALSE];
    
    [self initViewComponents];
    [self initNavigationItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initViewComponents {
    self.deactivateReasonTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.deactivateReasonTableView.delegate = self;
    self.deactivateReasonTableView.dataSource = self;
    self.deactivateReasonTableView.sectionHeaderHeight = 40;
    self.deactivateReasonTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.deactivateReasonTableView.scrollEnabled = false;
    [self.view addSubview:self.deactivateReasonTableView];
}

-(void)initNavigationItem {
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(0, 0, 60, 44);
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]
                                         initWithCustomView:cancelBtn];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    ////////
    UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame = CGRectMake(0, 0, 60, 44);
    [confirmBtn setTitle:@"Confirm" forState:UIControlStateNormal];
    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [confirmBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
                                         initWithCustomView:confirmBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

-(void)cancelAction {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)confirmAction {
    if(self.currentIndex != nil) {
        [self showActivityIndicatorView];
        
        [self.delegate didConformDeactivationWithReason:[self.deactivateReasonList objectAtIndex:self.currentIndex.row]];
    }
    else if(self.textField.text && ![[self.textField.text stringByTrimingWhitespace] isEqualToString:@""]) {
        [self.delegate didConformDeactivationWithReason:self.textField.text];
    }
    else {
        //sjhow alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select reason to deactivate your account" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)showActivityIndicatorView {
    self.view.userInteractionEnabled = false;
    self.deactivateReasonTableView.alpha = 0.1;
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    [self.view addSubview:self.activityIndicatorView];
    self.activityIndicatorView.titleText = @"Deactivating Profile";
    self.activityIndicatorView.titleLable.textColor = [UIColor darkGrayColor];
    [self.activityIndicatorView startAnimating];
}

#pragma mark UITableview datasource/delegate
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //1 FOR TEXTFIELD
    return self.deactivateReasonList.count+1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, 30)];
    lb.font = [UIFont boldSystemFontOfSize:14];
    lb.backgroundColor = [UIColor clearColor];
    lb.text = @"Sure you want to deactivate? Tell us why:";
    [bgView addSubview:lb];
    return bgView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(indexPath.row < self.deactivateReasonList.count-1) {
            UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                     cell.frame.size.height-1,
                                                                     cell.frame.size.width+50,
                                                                     1)];
            view1.backgroundColor = [UIColor lightGrayColor];
            view1.alpha = 0.5;
            [cell.contentView addSubview:view1];
        }
        if(indexPath.row == self.deactivateReasonList.count){
            self.textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, 250, 40)];
            self.textField.borderStyle = UITextBorderStyleBezel;
            self.textField.delegate = self;
            [cell.contentView addSubview:self.textField];
        }
    }
    if(indexPath.row < self.deactivateReasonList.count) {
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = [self.deactivateReasonList objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *currentSelectedCell = NULL;
    if(indexPath.row < self.deactivateReasonList.count-1){
        if((self.currentIndex != nil) && (self.currentIndex.row != indexPath.row) ) {
            //deselect selected cell
            UITableViewCell *currentCell = [tableView cellForRowAtIndexPath:self.currentIndex];
            currentCell.accessoryType = UITableViewCellAccessoryNone;
            
            //select new cell
            currentSelectedCell = [tableView cellForRowAtIndexPath:indexPath];
        }
        else {
            currentSelectedCell = [tableView cellForRowAtIndexPath:indexPath];
        }
        self.currentIndex = indexPath;
        currentSelectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.textField.text = @"";
        [self.textField resignFirstResponder];
    }
    
}

#pragma mark UItextfield delegate
#pragma mark -

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITableViewCell *currentCell = [self.deactivateReasonTableView cellForRowAtIndexPath:self.currentIndex];
    currentCell.accessoryType = UITableViewCellAccessoryNone;
    self.currentIndex = nil;
}

@end

//
//  TMDeactivateProfileViewController.h
//  TrulyMadly
//
//  Created by Ankit on 14/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMDeactivateProfileViewControllerDelegate;

@interface TMDeactivateProfileViewController : UIViewController

@property(nonatomic,weak)id<TMDeactivateProfileViewControllerDelegate>delegate;
-(instancetype)initWithDeactivationDictionary:(NSDictionary*)deactivationDictionary;

@end

@protocol TMDeactivateProfileViewControllerDelegate <NSObject>

-(void)didConformDeactivationWithReason:(NSString*)reason;

@end
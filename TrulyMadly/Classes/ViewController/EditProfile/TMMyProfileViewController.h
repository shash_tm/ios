//
//  TMMyProfileViewController.h
//  TrulyMadly
//
//  Created by Ankit on 08/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMProfileViewController.h"

@protocol TMMyProfileViewControllerDelegate;

@interface TMMyProfileViewController : TMProfileViewController<TMViewControllerActionDelegate>

@property(nonatomic,weak)id<TMMyProfileViewControllerDelegate> delegate;
@property(nonatomic,assign)BOOL fromRegistration;
@property(nonatomic,assign)BOOL from_nudge;
//@property(nonatomic,weak)id<TMViewControllerActionDelegate>actionDelegate;

@end

@protocol TMMyProfileViewControllerDelegate <NSObject>

//-(void)reloadMatchDataOnEditProfile;

@end

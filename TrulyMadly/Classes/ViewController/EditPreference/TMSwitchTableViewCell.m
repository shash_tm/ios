//
//  TMSwitchTableViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSwitchTableViewCell.h"

@interface TMSwitchTableViewCell ()

@property(nonatomic,strong)UISwitch* switchButton;
@property(nonatomic,strong)UILabel* titleLabel;

@end

@implementation TMSwitchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.text = @"Show me profiles from US";
        [self.contentView addSubview:self.titleLabel];
        
        self.switchButton = [[UISwitch alloc] init];
        [self.switchButton addTarget:self action:@selector(switchButtonAction) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:self.switchButton];    
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat maxWidth = CGRectGetWidth(self.contentView.frame);
    CGFloat maxHeight = CGRectGetHeight(self.contentView.frame);
    CGFloat yPos = 5;
    CGFloat labelXPos = 10;
    
    self.switchButton.frame = CGRectMake(maxWidth - (CGRectGetWidth(self.switchButton.frame)+labelXPos),
                                         (maxHeight - CGRectGetHeight(self.switchButton.frame))/2,
                                         CGRectGetWidth(self.switchButton.frame),
                                         CGRectGetHeight(self.switchButton.frame));
    
    self.titleLabel.frame = CGRectMake(labelXPos,
                                       yPos,
                                       maxWidth - (CGRectGetWidth(self.switchButton.frame)+20),
                                       40);
    
}
+(NSString*)reuseIdentifier {
    return @"switchcell";
}

-(void)setSwitchButtonActiveStatus:(BOOL)status {
    [self.switchButton setOn:status animated:FALSE];
}

-(void)switchButtonAction {
    [self.delegate didChangeSwitchButtonOnState:self.switchButton.on];
}

@end

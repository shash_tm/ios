//
//  TMEditPreferenceViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMEditPreferenceViewController: TMBaseViewController, UITableViewDelegate, UITableViewDataSource, TMEditPreferenceDelegate, UIAlertViewDelegate {
    
    var navigationFlow:TMNavigationFlow!
    var textDict:Dictionary<String,AnyObject> = [:]
    var stringFrame: CGRect!
    var stringWidth: CGFloat = 0.0
    var stringHeight: CGFloat = 0.0
    var indexPath = [AnyObject]()
    var count = 0
    
    var reloadData = false
    
    var isAllowToSave = false
    
    let editPreferenceMgr = TMEditPreferenceManager()
    
    var activityIndicator: TMActivityIndicatorView!
    var activityLoader: UIActivityIndicatorView!
    
    var editPreference:TMEditPreference!
    
    var tableView: UITableView!
    
    var alertTitle: String = "Problem in saving the data"
    
    var eventCategory: String = "edit_preference"
    
    var isRequestLoading = false;
    
    var eventDict:Dictionary<String,AnyObject> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TMAnalytics.sharedInstance().trackView("TMEditPreferenceViewController")
        eventDict["screenName"] = "TMEditPreferenceViewController" as AnyObject?
        eventDict["eventCategory"] = self.eventCategory as AnyObject?
        eventDict["eventAction"] = "page_load" as AnyObject?
        
        self.view.backgroundColor = UIColor.white
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.title = "Edit Preference"
        
        self.rightBarButton()
        
        self.tableView = UITableView(frame: view.bounds, style: .plain)
        
        self.tableView.register(TMSliderTableViewCell.self, forCellReuseIdentifier: "uiSlider")
        self.tableView.register(TMEmptyTableViewCell.self, forCellReuseIdentifier: "uiEmpty")
        self.tableView.register(TMTextLabelTableViewCell.self, forCellReuseIdentifier: "uiText")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]

        self.view.addSubview(tableView)
        
        self.view.bringSubview(toFront: self.tableView)
        
        self.tableView.separatorStyle = .none
        
        self.loadPrefData()
        
    }
    
    func loadPrefData() {
        if(self.editPreferenceMgr.isNetworkReachable()) {
            self.loadData()
        }else {
            if(self.editPreferenceMgr.isCachedResponsePresent()) {
               self.loadData()
            }
            else {
                 self.showRetryViewForError()
            }
        }
    }
    
    func showRetryViewForUnknownError() {
        if(!self.isRetryViewShown) {
            self.showRetryView(withMessage: TM_UNKNOWN_MSG)
            self.retryView.retryButton.addTarget(self, action: #selector(TMEditPreferenceViewController.loadPrefData), for: UIControlEvents.touchUpInside)
        }
    }
    
    func showRetryViewForError() {
        if(!self.isRetryViewShown) {
            self.showRetryView()
            self.retryView.retryButton.addTarget(self, action: #selector(TMEditPreferenceViewController.loadPrefData), for: UIControlEvents.touchUpInside)
        }
    }
    
    func addIndicator() {
        if(self.isRetryViewShown) {
            self.removeRetryView()
        }
        
        let fullScreen = TMFullScreenView()
        fullScreen.frame = self.view.frame
        fullScreen.backgroundColor = UIColor.white
        fullScreen.alpha = 0.8
        fullScreen.frame.origin.y = fullScreen.frame.origin.y - 64.0
        self.view.addSubview(fullScreen)
        
        self.activityIndicator = TMActivityIndicatorView()
        self.view.addSubview(self.activityIndicator)
        
        self.view.bringSubview(toFront: fullScreen)
        
        self.view.bringSubview(toFront: self.activityIndicator)
        
    }
    
    func removeLoader() {
        if(self.activityIndicator != nil) {
            self.activityIndicator.stopAnimating()
        }
        for view in self.view.subviews {
            if(view.isKind(of: TMFullScreenView.self)) {
                view.removeFromSuperview()
            }
        }
    }
    
    private func loadData() {
        self.view.isUserInteractionEnabled = false
        self.addIndicator()
        self.activityIndicator.titleText = "Loading..."
        self.activityIndicator.startAnimating()
        
        let queryString = ["login_mobile":"true"]
        
        self.editPreferenceMgr.trackEventDictionary = NSMutableDictionary(dictionary: eventDict)
        
        self.editPreferenceMgr.initCall(queryString as NSDictionary as [NSObject : AnyObject], with: {(response:TMEditPreference?,error:TMError?) -> Void in
            self.view.isUserInteractionEnabled = true
            self.removeLoader()
            if(response != nil) {
                self.editPreference = response
                self.editPreference.delegate = self
                self.editPreference.setAge()
                self.editPreference.setHeight()
    
                self.addIndicator()
                self.activityIndicator.titleText = "Loading..."
                self.activityIndicator.startAnimating()
                
                let buildForm = TMLoadRegisterJson()
                let basicDataDic = self.editPreference.basicsData
                let urlString = basicDataDic["url"] as! NSString
                buildForm.getData(urlString as String, with: { (success:Bool, error: TMError?) -> Void in
                    self.removeLoader()
                    
                    if(success) {
                       // self.rightBarButton()
                        self.tableView.separatorStyle = .singleLine
                        self.tableView.backgroundColor = UIColor(red: 0.937, green: 0.937, blue: 0.957, alpha: 1.0)
                        self.editPreference.setStates()
                        self.editPreference.setCities()
                        self.count = 4
                        self.tableView.reloadData()
                    }
                    else {
                        self.showRetryViewForError()
                    }
                })

            }else {
                if(error?.errorCode == .TMERRORCODE_LOGOUT) {
                    self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
                }
                else if((error?.errorCode == .TMERRORCODE_NONETWORK || error?.errorCode == .TMERRORCODE_SERVERTIMEOUT) && !self.editPreferenceMgr.isCachedResponsePresent()) {
                    if(!self.editPreferenceMgr.isCachedResponsePresent()) {
                         self.showRetryViewForError()
                    }
                }
                else {
                    if(!self.editPreferenceMgr.isCachedResponsePresent()) {
                        self.showRetryViewForUnknownError()
                    }
                }
            }
        })
    }
    
    private func rightBarButton() {
        
        let negativeSpacer:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -12;
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_save_button")!, style: .done, target: self, action: #selector(TMEditPreferenceViewController.saveData))
        
        self.navigationItem.setLeftBarButtonItems([negativeSpacer, rightBarButton], animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(self.reloadData) {
            self.editPreference.getSates()
            self.editPreference.getCities()
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(section) {
            //case 0: return 10
        case 0: return 2
        case 1: return 1
        case 2:
            if(self.editPreference.statesToDisplay.count>0) {
                return 2
            }
            return 1
        case 3:
            if(self.editPreference.citiesToDisplay.count>0) {
                return 2
            }
            return 1
            
        default: fatalError("Unknown number of sections")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let widthAllowed = (self.view.bounds.size.width - 32) as AnyObject
        
        switch((indexPath as NSIndexPath).section) {
        case 0:
            switch((indexPath as NSIndexPath).row) {
            case 0:
                let cell:TMSliderTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiSlider") as! TMSliderTableViewCell
                cell.selectionStyle = .none
                
                self.indexPath.append(indexPath as AnyObject)
                self.textDict["kmaxwidth"] = widthAllowed
                self.textDict["kfont"] = UIFont.systemFont(ofSize: 16)
                self.textDict["ktext"] = "Age" as AnyObject?
                self.stringFrame = TMStringUtil.textRect(forString: textDict)
                self.stringWidth = stringFrame.size.width
                self.stringHeight = stringFrame.size.height
                
                cell.headerText.frame.origin.x = 16.0
                cell.headerText.frame.origin.y = 5.0
                cell.headerText.frame.size.width = stringWidth
                cell.headerText.frame.size.height = stringHeight
                cell.headerText.text = "Age"
                
                self.textDict["kmaxwidth"] = widthAllowed
                self.textDict["kfont"] = UIFont.systemFont(ofSize: 16)
                self.textDict["ktext"] = " 18-70 " as AnyObject?
                self.stringFrame = TMStringUtil.textRect(forString: textDict)
                self.stringWidth = stringFrame.size.width
                self.stringHeight = stringFrame.size.height
                
                cell.headerValue.frame.origin.x = self.view.bounds.size.width - self.stringWidth - 16.0
                cell.headerValue.frame.origin.y = 5.0
                cell.headerValue.frame.size.width = stringWidth
                cell.headerValue.frame.size.height = stringHeight
                cell.headerValue.text = self.editPreference.startAge+"-"+self.editPreference.endAge
                
                cell.slider.frame.origin.x = 16.0
                cell.slider.frame.origin.y = cell.headerText.frame.origin.y + cell.headerText.frame.size.height + 20.0
                cell.slider.frame.size.width = self.view.bounds.size.width - 32.0
                cell.slider.frame.size.height = 32.0
                cell.slider.addTarget(self, action: #selector(TMEditPreferenceViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
                cell.slider.tag = (indexPath as NSIndexPath).row
                cell.slider.minimumValue = 18.0
                cell.slider.maximumValue = 70.0
                cell.slider.lowerValue = Double(Int(self.editPreference.startAge)!)
                cell.slider.upperValue = Double(Int(self.editPreference.endAge)!)
                
                return cell
                
            case 1:
                let cell:TMSliderTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiSlider") as! TMSliderTableViewCell
                
                self.indexPath.append(indexPath as AnyObject)
                self.textDict["kmaxwidth"] = widthAllowed
                self.textDict["kfont"] = UIFont.systemFont(ofSize: 16)
                self.textDict["ktext"] = "Height" as AnyObject?
                self.stringFrame = TMStringUtil.textRect(forString: textDict)
                self.stringWidth = stringFrame.size.width
                self.stringHeight = stringFrame.size.height
                
                cell.headerText.frame.origin.x = 16.0
                cell.headerText.frame.origin.y = 5.0
                cell.headerText.frame.size.width = stringWidth
                cell.headerText.frame.size.height = stringHeight
                cell.headerText.text = "Height" as String
                
                self.textDict["kmaxwidth"] = widthAllowed
                self.textDict["kfont"] = UIFont.systemFont(ofSize: 16)
                self.textDict["ktext"] = (self.editPreference.startHeight + "-" + self.editPreference.endHeight) as AnyObject
                self.stringFrame = TMStringUtil.textRect(forString: textDict)
                self.stringWidth = stringFrame.size.width
                self.stringHeight = stringFrame.size.height
                
                cell.headerValue.frame.origin.x = self.view.bounds.size.width - self.stringWidth - 16.0
                cell.headerValue.frame.origin.y = 5.0
                cell.headerValue.frame.size.width = stringWidth
                cell.headerValue.frame.size.height = stringHeight
                cell.headerValue.text = self.editPreference.startHeight+"-"+self.editPreference.endHeight
                
                cell.slider.frame.origin.x = 16.0
                cell.slider.frame.origin.y = cell.headerText.frame.origin.y + cell.headerText.frame.size.height + 20.0
                cell.slider.frame.size.width = self.view.bounds.size.width - 32.0
                cell.slider.frame.size.height = 32.0
                cell.slider.addTarget(self, action: #selector(TMEditPreferenceViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
                cell.slider.tag = (indexPath as NSIndexPath).row
                cell.slider.minimumValue = 48.0
                cell.slider.maximumValue = 95.0
                cell.slider.lowerValue = Double(Int(self.editPreference.start_height)!)
                cell.slider.upperValue = Double(Int(self.editPreference.end_height)!)
                
                cell.selectionStyle = .none
                return cell
                
            default: fatalError("Unknown row")
            }
        case 1:
            let cell:TMEmptyTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiEmpty") as! TMEmptyTableViewCell
            cell.backgroundColor = UIColor(red: 0.937, green: 0.937, blue: 0.957, alpha: 1.0)
            cell.selectionStyle = .none
            cell.view.frame.size.height = 20
            self.indexPath.append(indexPath as AnyObject)
            return cell
            
        case 2:
            switch((indexPath as NSIndexPath).row) {
            case 0:
                let cell:TMTextLabelTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                self.indexPath.append(indexPath as AnyObject)
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "State"
                
                if(self.editPreference.statesToDisplay.count > 0) {
                    //self.textDict["ktext"] =
                    cell.detailTextLabel?.text = String(self.editPreference.statesToDisplay.count)+" States Selected"
                } else {
                    cell.detailTextLabel?.text = "Doesn't Matter"
                }
                
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                cell.backgroundColor = UIColor.white
                cell.headerText.text = ""
                return cell
                
            case 1:
                let cell:TMTextLabelTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.selectionStyle = .none
                cell.textLabel?.text = ""
                cell.detailTextLabel?.text = ""
                cell.accessoryType = .none
                cell.headerValue.text = ""
                if(self.editPreference.statesToDisplay.count>0) {
                    cell.headerWithText(self.editPreference.statesToDisplay.joined(separator: ", "))
                    cell.backgroundColor = UIColor(red: 0.937, green: 0.937, blue: 0.957, alpha: 1.0)
                }
                
                return cell
                
            default: fatalError("Unknown row")
            }
        case 3:
            switch((indexPath as NSIndexPath).row) {
            case 0:
                let cell:TMTextLabelTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.backgroundColor = UIColor.white
                
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "City"
                cell.headerValue.text = ""
                if(self.editPreference.citiesToDisplay.count > 0) {
                    cell.detailTextLabel?.text = String(self.editPreference.citiesToDisplay.count)+" Cities Selected"
                } else {
                    cell.detailTextLabel?.text = "Doesn't Matter"
                }
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                //cell.backgroundColor = UIColor.clearColor()
                cell.headerText.text = ""
                return cell
            case 1:
                let cell:TMTextLabelTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.selectionStyle = .none
                cell.textLabel?.text = ""
                cell.detailTextLabel?.text = ""
                cell.accessoryType = .none
                cell.headerValue.text = ""
                if(self.editPreference.citiesToDisplay.count>0) {
                    cell.headerWithText(self.editPreference.citiesToDisplay.joined(separator: ", "))
                    cell.backgroundColor = UIColor(red: 0.937, green: 0.937, blue: 0.957, alpha: 1.0)
                }
                
                return cell
                
            default: fatalError("Unknown row")
            }
        default: fatalError("Unknown section")
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let widthAllowed = (self.view.bounds.size.width - 32) as AnyObject
        
        switch((indexPath as NSIndexPath).section) {
        case 0: return 90.0
        case 1: return 20
        case 2:
            if((indexPath as NSIndexPath).row == 0) {
                return 50
            } else if((indexPath as NSIndexPath).row == 1) {
                if(self.editPreference.statesToDisplay.count>0) {
                    self.textDict["kmaxwidth"] = widthAllowed
                    self.textDict["kfont"] = UIFont.systemFont(ofSize: 12)
                    self.textDict["ktext"] = self.editPreference.statesToDisplay.joined(separator: ", ") as AnyObject?
                    self.stringFrame = TMStringUtil.textRect(forString: textDict)
                    self.stringHeight = stringFrame.size.height
                    
                    self.textDict["ktext"] = "Showing you matches from: " as AnyObject?
                    self.stringFrame = TMStringUtil.textRect(forString: textDict)
                    let h: CGFloat = stringFrame.size.height
                    return self.stringHeight + 22 + h
                }
                return 0
            }
        case 3:
            if((indexPath as NSIndexPath).row == 0) {
                return 50
            } else if((indexPath as NSIndexPath).row == 1) {
                if(self.editPreference.citiesToDisplay.count>0) {
                    self.textDict["kmaxwidth"] = widthAllowed
                    self.textDict["kfont"] = UIFont.systemFont(ofSize: 12)
                    self.textDict["ktext"] = self.editPreference.citiesToDisplay.joined(separator: ", ") as AnyObject?
                    self.stringFrame = TMStringUtil.textRect(forString: textDict)
                    self.stringHeight = stringFrame.size.height
                    
                    self.textDict["ktext"] = "Showing you matches from: " as AnyObject?
                    self.stringFrame = TMStringUtil.textRect(forString: textDict)
                    let h: CGFloat = stringFrame.size.height
                    return self.stringHeight + 22 + h
                }
                return 0
            }
        default: fatalError("unknown section")
        }
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if(section == 0) {
            return 30.0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if((indexPath as NSIndexPath).section == 2 && (indexPath as NSIndexPath).row == 0) {
            var isDoesNotMatterSelected = true
            if(self.editPreference.statesToDisplay.count>0) {
                isDoesNotMatterSelected = false
            }
            let dataTable: TMDataTableViewController = TMDataTableViewController(navigationFlow: TMNavigationFlow.TMNAVIGATIONFLOW_REGISTRATION, data: self.editPreference.states as NSArray, isDoesNotMatterSelected: isDoesNotMatterSelected, caller: "state")
            dataTable.delegate = self.editPreference
            self.navigationController?.pushViewController(dataTable, animated: false)
        } else if((indexPath as NSIndexPath).section == 3 && (indexPath as NSIndexPath).row == 0) {
            if(self.editPreference.statesToDisplay.count>0) {
                var isDoesNotMatterSelected = true
                if(self.editPreference.citiesToDisplay.count>0) {
                    isDoesNotMatterSelected = false
                }
                let dataTable: TMDataTableViewController = TMDataTableViewController(navigationFlow: TMNavigationFlow.TMNAVIGATIONFLOW_REGISTRATION, data: self.editPreference.cities as NSArray, isDoesNotMatterSelected: isDoesNotMatterSelected, caller: "city")
                dataTable.delegate = self.editPreference
                self.navigationController?.pushViewController(dataTable, animated: false)
            }else {
                self.showAlert("Please select state first", title: "")
            }
            
        }

    }
    
    func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        self.isAllowToSave = true
        if(rangeSlider.tag == 0) {
            let cell = self.tableView.cellForRow(at: self.indexPath[0] as! IndexPath) as! TMSliderTableViewCell
            let minVal = Int(floor(rangeSlider.lowerValue))
            let maxVal = Int(floor(rangeSlider.upperValue))
            if(minVal == 18 && (maxVal == 18 || maxVal == 19)) {
                cell.slider.upperValue = Double(minVal+2)
            }
            cell.headerValue.text = "\(Int(floor(rangeSlider.lowerValue)))"+"-"+"\(Int(floor(rangeSlider.upperValue)))"
            self.editPreference.startAge = String(Int(floor(rangeSlider.lowerValue)))
            self.editPreference.endAge = String(Int(floor(rangeSlider.upperValue)))
        }
        else if(rangeSlider.tag == 1) {
            let cell = self.tableView.cellForRow(at: self.indexPath[1] as! IndexPath) as! TMSliderTableViewCell
            let startHeight: String = self.editPreference.getHeightInFeetandInch(Int(floor(rangeSlider.lowerValue)))
            let endHeight: String = self.editPreference.getHeightInFeetandInch(Int(floor(rangeSlider.upperValue)))
            
            let minVal = Int(floor(rangeSlider.lowerValue))
            let maxVal = Int(floor(rangeSlider.upperValue))
            if(minVal == 48 && (maxVal == 49 || maxVal == 50)) {
                cell.slider.upperValue = Double(minVal+3)
            }

            self.textDict["kmaxwidth"] = (self.view.bounds.size.width - 32) as AnyObject
            self.textDict["kfont"] = UIFont.systemFont(ofSize: 16)
            self.textDict["ktext"] = (startHeight + "-" + endHeight) as AnyObject
            self.stringFrame = TMStringUtil.textRect(forString: textDict)
            self.stringWidth = stringFrame.size.width
            self.stringHeight = stringFrame.size.height
            
            cell.headerValue.frame.size.width = stringWidth
            cell.headerValue.text = startHeight+"-"+endHeight
            self.editPreference.startHeight = startHeight
            self.editPreference.endHeight = endHeight
            
            self.editPreference.start_height = String(Int(floor(rangeSlider.lowerValue)))
            self.editPreference.end_height = String(Int(floor(rangeSlider.upperValue)))
        }
        
    }
    
    // delegate for TMEditpreference
    func didDataChange() {
        self.isAllowToSave = true
        self.reloadData = true
    }
    
    func saveDataToServer() {
        
        if(self.isAllowToSave) {
            if(self.editPreferenceMgr.isNetworkReachable()) {
                //sender.enabled = false
                self.isRequestLoading = true
            
                self.view.isUserInteractionEnabled = false
            
                self.addIndicator()
                self.activityIndicator.titleText = "Saving..."
                self.activityIndicator.startAnimating()
            
                self.eventDict["screenName"] = "TMEditPreferenceViewController" as AnyObject?
                self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
                self.eventDict["eventAction"] = "save_call" as AnyObject?
                self.editPreferenceMgr.trackEventDictionary = NSMutableDictionary(dictionary: self.eventDict)
            
                let queryString = self.editPreference.saveData()
                
                self.editPreferenceMgr.saveData(queryString as NSDictionary as [NSObject : AnyObject], with: { (response:String?, err:TMError?) -> Void in
                    self.view.isUserInteractionEnabled = true
                    self.isRequestLoading = false
                    // sender.enabled = true
                    self.removeLoader()
                
                    if(response != nil) {
                        if(response == "success") {
                        
                            // MoEngage event
                            MoEngage.sharedInstance().trackEvent("Edit Preferences Save", andPayload: nil)
                        
                            // move to matches
                            //update so that match viewcon can reload the data
                            let userSession:TMUserSession = TMUserSession.sharedInstance()
                            userSession.isUserProfileUpdated = true
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        self.errorResponse(err!)
                    }
                })
            }else {
                self.showAlert(TM_INTERNET_NOTAVAILABLE_MSG, title: "")
            }
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // to save the data
    
    func saveData() {
        if(!self.isRequestLoading) {
            self.saveDataToServer()
        }
    }
    
    func showAlert(_ msg: String, title: String) {
        
        let alert = UIAlertView(title:title , message:msg , delegate: self, cancelButtonTitle: "Ok")
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        let title: NSString = alertView.buttonTitle(at: buttonIndex)! as NSString
        if(title == "Ok") {
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    func errorResponse(_ err: TMError) {
        if(err.errorCode == .TMERRORCODE_LOGOUT) {
            self.actionDelegate?.setNavigationFlowForLogoutAction!(false)
        }else if(err.errorCode == .TMERRORCODE_NONETWORK || err.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
            self.showAlert(TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }
        else if(err.errorCode == .TMERRORCODE_DATASAVEERROR) {
            self.showAlert(err.errorMessage as String, title: "")
        }else {
            self.showAlert("Please Try Later", title: alertTitle)
        }
    }

}

//
//  TMEditPreferenceViewController.m
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEditPreferenceViewController.h"
#import "TMSliderTableViewCell.h"
#import "TMEditPrefTextLabelTableViewCell.h"
#import "TMSwitchTableViewCell.h"
#import "TMEditPreferenceManager.h"
#import "TMUserDataController.h"
#import "TMEditPreference.h"
#import "TMUserSession.h"
#import "TMAnalytics.h"
#import "MoEngage.h"
#import "TMSwiftHeader.h"
#import "NSString+TMAdditions.h"


@interface TMEditPreferenceViewController ()<UITableViewDelegate,UITableViewDataSource,TMSwitchTableViewCellDelegate,
                                            TMSliderTableViewCellDelegate,TMEditPreferenceDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong)TMEditPreferenceManager* editPreferenceManager;
@property(nonatomic,strong)TMUserDataController* userDataController;
@property(nonatomic,strong)TMEditPreference* preference;
@property(nonatomic,assign)BOOL requestInProgress;
@property(nonatomic,assign)BOOL isAllowToSave;
@property(nonatomic,assign)BOOL reloadData;

@end

@implementation TMEditPreferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:FALSE animated:FALSE];
    [self.navigationItem setHidesBackButton:TRUE animated:FALSE];
    
    self.title = @"Edit Preference";
    
    [self trackPageLoad];
    [self setupUI];
    [self loadEditPreferenceData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.reloadData) {
        [self.preference updateLocationSelectionState];
        [self.tableView reloadData];
    }
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.tableView.frame = self.view.bounds;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(TMEditPreferenceManager*)editPreferenceManager {
    if(!_editPreferenceManager) {
        _editPreferenceManager = [[TMEditPreferenceManager alloc] init];
    }
    return _editPreferenceManager;
}
-(TMUserDataController*)userDataController {
    if(!_userDataController) {
        _userDataController = [[TMUserDataController alloc] init];
    }
    return _userDataController;
}

-(void)setupUI {
    UIBarButtonItem* negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                    target:nil action:nil];
    negativeSpacer.width = -12;
    UIBarButtonItem* saveBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_save_button"]
                                                                      style:UIBarButtonItemStyleDone
                                                                     target:self action:@selector(savePreferneceData)];
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer,saveBarButton]];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:244.0f/255.0f alpha:1];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.tableView registerClass:[TMSliderTableViewCell class] forCellReuseIdentifier:[TMSliderTableViewCell reuseIdentifier]];
    [self.tableView registerClass:[TMEditPrefTextLabelTableViewCell class] forCellReuseIdentifier:[TMEditPrefTextLabelTableViewCell reuseIdentifier]];
    [self.tableView registerClass:[TMSwitchTableViewCell class] forCellReuseIdentifier:[TMSwitchTableViewCell reuseIdentifier]];
}
-(void)reloadPreferenceData {
    [self.preference loadStateAndCityData];
    if(!self.tableView.superview) {
        [self.view addSubview:self.tableView];
    }
    [self.tableView reloadData];
}
-(void)addLoaderWithMessage:(NSString*)message {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:message];
}

-(void)removeLoader {
    [self hideActivityIndicatorViewWithtranslucentView];
}
-(void)showAlertWithMessage:(NSString*)messgae withTitle:(NSString*)title {
    
    UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:title message:messgae
                                                       delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertview show];
    });
}
-(void)showRetryViewForUnknownError {
    if(!self.isRetryViewShown) {
        [self showRetryView];
        [self.view bringSubviewToFront:self.retryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadEditPreferenceData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewForError {
    if(!self.isRetryViewShown) {
        [self showRetryView];
        [self.view bringSubviewToFront:self.retryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadEditPreferenceData) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)handleErrorResponse:(TMError*)error {
    if(error.errorCode == TMERRORCODE_LOGOUT) {
        [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
    }else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showAlertWithMessage:TM_INTERNET_NOTAVAILABLE_MSG withTitle:@""];
    }
    else if(error.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self showAlertWithMessage:error.errorMessage withTitle:@""];
    }else {
        [self showAlertWithMessage:@"Please Try Later" withTitle:@""];
    }
}
-(void)loadEditPreferenceData {
    [self addLoaderWithMessage:@"Loading..."];
    [self.userDataController getPreferenceDataWithResponse:^(TMEditPreference * _Nullable preference, TMError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self removeLoader];
            if(preference) {
                self.preference = preference;
                self.preference.delegate = self;
                [self reloadPreferenceData];
            }
            else {
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
                }
                else {
                    BOOL isServerIssue = (error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT);
                    BOOL isCachedResponsePresent = [self.editPreferenceManager isCachedResponsePresent];
                    if( isServerIssue && (!isCachedResponsePresent) ) {
                        [self showRetryViewForError];
                    }
                    else {
                        [self showRetryViewForUnknownError];
                    }
                }
            }
        });
    }];
}

-(void)savePreferneceData {
    if(!self.requestInProgress) {
        if(self.isAllowToSave) {
            if(self.editPreferenceManager.isNetworkReachable) {
                self.requestInProgress = TRUE;
                self.view.userInteractionEnabled = FALSE;
                [self addLoaderWithMessage:@"Saving..."];
                
                NSDictionary* queryParams = [self.preference getEditPrefSaveRequestfParams];
                self.editPreferenceManager.trackEventDictionary = [self saveEditPreferneceTrackingParams];
                [self.editPreferenceManager saveData:queryParams with:^(NSString *success, TMError *error) {
                    self.view.userInteractionEnabled = TRUE;
                    self.requestInProgress = FALSE;
                    [self removeLoader];
                    if(success && [success isEqualToString:@"success"]) {
                        [self trackMoengageEvent];
                        [TMUserSession sharedInstance].isUserProfileUpdated = TRUE;
                        [self.navigationController popViewControllerAnimated:TRUE];
                    }
                    else {
                        [self handleErrorResponse:error];
                    }
                }];
            }
            else {
                [self showAlertWithMessage:TM_INTERNET_NOTAVAILABLE_MSG withTitle:@""];
            }
        }
        else {
            [self.navigationController popViewControllerAnimated:TRUE];
        }
    }
}


#pragma mark UITableview datasource/delegate
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
        NSInteger sectionCount = self.preference.outsideProfileSwitchEnabled ? 4 : 3;
        return sectionCount;
    }
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 2;
    }
    else {
        if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
            if(section == 1) {
                NSInteger stateRows = (self.preference.statesToDisplay.count > 0) ? 2 : 1;
                return stateRows;
            }
            else if(section == 2) {
                NSInteger cityRows = (self.preference.citiesToDisplay.count > 0) ? 2 : 1;
                return cityRows;
            }
            else {
                return 1; //show outside india profile
            }
        }
        else {
            if(self.preference.regionToToDisplay.count > 0) {
                return 2;
            }
            return 1;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section != 2) {
        return 30;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        return 100;
    }
    else {
        if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
            if(indexPath.section == 1) {
                if(indexPath.row == 0) {
                    return 60;
                }
                else {
                    NSString* valueText = [self.preference getSelectedStateJoinedText];
                    CGFloat maxWidth = CGRectGetWidth(self.tableView.frame);
                    CGFloat xOffset = 20;
                    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
                    CGSize constrintSize = CGSizeMake(maxWidth - (2*xOffset), NSUIntegerMax);
                    CGRect rect = [valueText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
                    CGFloat rowHeight = 5 + 20 + CGRectGetHeight(rect) + 5;
                    return rowHeight;
                }
            }
            else if(indexPath.section == 2) {
                if(indexPath.row == 0) {
                    return 60;
                }
                else {
                    NSString* valueText = [self.preference getSelectedCityJoinedText];
                    CGFloat maxWidth = CGRectGetWidth(self.tableView.frame);
                    CGFloat xOffset = 20;
                    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
                    CGSize constrintSize = CGSizeMake(maxWidth - (2*xOffset), NSUIntegerMax);
                    CGRect rect = [valueText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
                    CGFloat rowHeight = 5 + 20 + CGRectGetHeight(rect) + 5; //top y offset + label height + content height + bottom yoffset
                    return rowHeight;
                }
            }
            else {
                return 60;
            }
        }
        else {
            if(indexPath.row == 0) {
                return 60;
            }
            else {
                NSString* valueText = [self.preference getSelectedRegionJoinedText];
                CGFloat maxWidth = CGRectGetWidth(self.tableView.frame);
                CGFloat xOffset = 20;
                NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
                CGSize constrintSize = CGSizeMake(maxWidth - (2*xOffset), NSUIntegerMax);
                CGRect rect = [valueText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
                CGFloat rowHeight = 5 + 20 + CGRectGetHeight(rect) + 5;
                return rowHeight;
            }
        }
    }
   return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section < 1) {
        TMSliderTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:[TMSliderTableViewCell reuseIdentifier]];
        cell.delegate = self;
        cell.tag = indexPath.row;
        if(indexPath.row == 0) {
            [cell setSliderValuesForAge];
            [cell setTitleText:@"Age" valueText:[NSString stringWithFormat:@"%@-%@",self.preference.startAge,self.preference.endAge]];
            [cell setSliderUpperValue:[self.preference.startAge doubleValue]
                     sliderLowerValue:[self.preference.endAge doubleValue]];
        }
        else {
            NSString* valueText = [NSString stringWithFormat:@"%@-%@",self.preference.startHeightInFeetandInch,self.preference.endHeightInFeetandInch];
            [cell setSliderValuesForHeight];
            [cell setTitleText:@"Height" valueText:valueText];
            [cell setSliderUpperValue:[self.preference.startHeight doubleValue]
                     sliderLowerValue:[self.preference.endHeight doubleValue]];
        }
        return cell;
    }
    else {
        if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
            if(indexPath.section == 1) {
                BOOL isMultiMode = (indexPath.row == 0) ? FALSE : TRUE;
                NSString* headerText = (indexPath.row == 0) ? @"State" : [self.preference getSelectedLocationCellHeaderText];
                NSString* valueText = (indexPath.row == 0) ? [self.preference getSelectedStateText]
                                                           : [self.preference getSelectedStateJoinedText];
                TMEditPrefTextLabelTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:[TMEditPrefTextLabelTableViewCell reuseIdentifier]];
                [cell setHeaderLabelText:headerText valueLabelText:valueText withMultilineMode:isMultiMode];
                return cell;
            }
            else if(indexPath.section == 2) {
                BOOL isMultiMode = (indexPath.row == 0) ? FALSE : TRUE;
                NSString* headerText = (indexPath.row == 0) ? @"City" : [self.preference getSelectedLocationCellHeaderText];
                NSString* valueText = (indexPath.row == 0) ? [self.preference getSelectedCityText]
                                                           : [self.preference getSelectedCityJoinedText];
                TMEditPrefTextLabelTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:[TMEditPrefTextLabelTableViewCell reuseIdentifier]];
                [cell setHeaderLabelText:headerText valueLabelText:valueText withMultilineMode:isMultiMode];
                return cell;
            }
            else {
                TMSwitchTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:[TMSwitchTableViewCell reuseIdentifier]];
                [cell setSwitchButtonActiveStatus:self.preference.showOutsideIndiaProfiles];
                cell.delegate = self;
                return cell;
            }
        }
        else {
            BOOL isMultiMode = (indexPath.row == 0) ? FALSE : TRUE;
            NSString* headerText = (indexPath.row == 0) ? @"Region" : [self.preference getSelectedLocationCellHeaderText];
            NSString* valueText = (indexPath.row == 0) ? [self.preference getSelectedRegionText]
                                                       : [self.preference getSelectedRegionJoinedText];
            TMEditPrefTextLabelTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:[TMEditPrefTextLabelTableViewCell reuseIdentifier]];
            [cell setHeaderLabelText:headerText valueLabelText:valueText withMultilineMode:isMultiMode];
            return cell;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
        if(indexPath.section == 1) { //state
            if(indexPath.row == 0) {
                BOOL isDoesNotMatterSelected = TRUE;
                if(self.preference.statesToDisplay.count>0) {
                    isDoesNotMatterSelected = FALSE;
                }
                TMDataTableViewController* dataTable = [[TMDataTableViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION
                                                                                                            data:self.preference.states
                                                                                         isDoesNotMatterSelected:isDoesNotMatterSelected
                                                                                                          caller:@"state"
                                                                                                        delegate:self.preference];
                [self.navigationController pushViewController:dataTable animated:TRUE];
            }
        }
        else if(indexPath.section == 2) { //city
            if(indexPath.row == 0) {
                if(self.preference.statesToDisplay.count>0) {
                    BOOL isDoesNotMatterSelected = TRUE;
                    if(self.preference.citiesToDisplay.count>0) {
                        isDoesNotMatterSelected = FALSE;
                    }
                    TMDataTableViewController* dataTable = [[TMDataTableViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION
                                                                                                                data:self.preference.cities
                                                                                             isDoesNotMatterSelected:isDoesNotMatterSelected
                                                                                                              caller:@"city"
                                                                                                            delegate:self.preference];
                    [self.navigationController pushViewController:dataTable animated:TRUE];
                }
                else {
                    [self showAlertWithMessage:@"Please select state first" withTitle:@""];
                }
            }
        }
    }
    else {//region
        if(indexPath.section == 1) {
            if(indexPath.row == 0) {
                BOOL isDoesNotMatterSelected = TRUE;
                if(self.preference.regionToToDisplay.count>0) {
                    isDoesNotMatterSelected = FALSE;
                }
                TMDataTableViewController* dataTable = [[TMDataTableViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_REGISTRATION
                                                                                                            data:self.preference.regions
                                                                                         isDoesNotMatterSelected:isDoesNotMatterSelected
                                                                                                          caller:@"region"
                                                                                                        delegate:self.preference];
                [self.navigationController pushViewController:dataTable animated:TRUE];
            }
        }
    }
}

-(void)didChangePreference {
    self.isAllowToSave = TRUE;
    self.reloadData = TRUE;
}
-(void)sliderCell:(TMSliderTableViewCell*)cell didChangeSliderValueWithMin:(NSInteger)minValue maxSliderValue:(NSInteger)maxValue {
    self.isAllowToSave = TRUE;
    if(cell.tag == 0) {
        if(minValue == 18 && (maxValue == 18 || maxValue == 19)) {
            [cell setSliderUpperValue:minValue+2 sliderLowerValue:minValue];
        }
        [cell setTitleText:@"Age" valueText:[NSString stringWithFormat:@"%ld-%ld",minValue,maxValue]];
        self.preference.startAge = [NSString stringWithFormat:@"%ld",minValue];
        self.preference.endAge = [NSString stringWithFormat:@"%ld",maxValue];
    }
    else {
        if(minValue == 48 && (maxValue == 49 || maxValue == 50)) {
            [cell setSliderUpperValue:minValue+3 sliderLowerValue:minValue];
        }
        self.preference.startHeight = [NSString stringWithFormat:@"%ld",minValue];
        self.preference.endHeight = [NSString stringWithFormat:@"%ld",maxValue];
        self.preference.startHeightInFeetandInch = [self.preference getHeightInFeetandInch:self.preference.startHeight.integerValue];
        self.preference.endHeightInFeetandInch = [self.preference getHeightInFeetandInch:self.preference.endHeight.integerValue];
        
        NSString* valueText = [NSString stringWithFormat:@"%@-%@",self.preference.startHeightInFeetandInch,self.preference.endHeightInFeetandInch];
        [cell setTitleText:@"Height" valueText:valueText];
    }
}
-(void)didChangeSwitchButtonOnState:(BOOL)buttonOnState {
    self.isAllowToSave = TRUE;
    self.preference.showOutsideIndiaProfiles = buttonOnState;
}

#pragma mark - Tracking
-(void)trackPageLoad {
    [[TMAnalytics sharedInstance] trackView:@"TMEditPreferenceViewController"];
}
-(NSMutableDictionary*)saveEditPreferneceTrackingParams {
    NSMutableDictionary* trackingDict = [NSMutableDictionary dictionaryWithCapacity:4];
    trackingDict[@"screenName"] = @"TMEditPreferenceViewController";
    trackingDict[@"eventCategory"] = @"";
    trackingDict[@"eventAction"] = @"save_call";
    return trackingDict;
}
-(void)trackMoengageEvent {
    [[MoEngage sharedInstance] trackEvent:@"Edit Preferences Save" andPayload:nil];
}

@end

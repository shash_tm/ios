//
//  TMSliderTableViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RangeSlider;
@protocol TMSliderTableViewCellDelegate;

@interface TMSliderTableViewCell : UITableViewCell

@property(nonatomic,weak)id<TMSliderTableViewCellDelegate> delegate;

+(NSString*)reuseIdentifier;
-(void)setSliderUpperValue:(NSInteger)upperValue sliderLowerValue:(NSInteger)lowerValue;
-(void)setSliderValuesForHeight;
-(void)setSliderValuesForAge;
-(void)setTitleText:(NSString*)titleText valueText:(NSString*)valueText;

@end

@protocol TMSliderTableViewCellDelegate <NSObject>

-(void)sliderCell:(TMSliderTableViewCell*)cell didChangeSliderValueWithMin:(NSInteger)minValue maxSliderValue:(NSInteger)maxValue;

@end

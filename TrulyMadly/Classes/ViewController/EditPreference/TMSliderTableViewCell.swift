//
//  TMSliderTableViewCell.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMSliderTableViewCell: UITableViewCell {
    
    var headerText: UILabel!
    var headerValue: UILabel!
    //var Lslider: UISlider!
    var slider: RangeSlider!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        
        self.headerText = UILabel()
        self.headerText.font = UIFont.systemFont(ofSize: 16)
        self.headerText.textAlignment = NSTextAlignment.left
        self.contentView.addSubview(self.headerText)
        
        self.headerValue = UILabel()
        self.headerValue.font = UIFont.systemFont(ofSize: 16)
        self.headerValue.textAlignment = NSTextAlignment.left
        self.contentView.addSubview(self.headerValue)
        
        self.slider = RangeSlider(frame: CGRect.zero)
        //self.slider.backgroundColor = UIColor.redColor()
        self.contentView.addSubview(self.slider)
        
//        self.Rslider = UISlider()
//        self.contentView.addSubview(self.Rslider)
////        
//        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC))
//        dispatch_after(time, dispatch_get_main_queue()) {
//            self.slider.trackHighlightTintColor = UIColor.redColor()
//            self.slider.curvaceousness = 0.0
//        }
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

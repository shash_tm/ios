//
//  TMTextLabelTableViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMEditPrefTextLabelTableViewCell : UITableViewCell

+(NSString*)reuseIdentifier;
-(void)setHeaderLabelText:(NSString*)headerLabelText valueLabelText:(NSString*)valueLabelText withMultilineMode:(BOOL)isMultilineMode;

@end

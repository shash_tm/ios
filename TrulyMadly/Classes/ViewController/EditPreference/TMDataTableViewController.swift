//
//  TMDataTableViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

@objc protocol TMDataTableViewControllerDelegate{
    func didStateChange(key: String, index: Int)
}

class TMDataTableViewController: UITableViewController {

    var navigationFlow:TMNavigationFlow!
    var count = 0
    var data = [AnyObject]()
    var cell: UITableViewCell!
    var isDoesNotMatterSelected = false
    var zeroCell: UITableViewCell!
    var caller = ""
    
    var delegate:TMDataTableViewControllerDelegate? = nil
    
    init(navigationFlow: TMNavigationFlow, data: NSArray, isDoesNotMatterSelected: Bool, caller: String) {
        super.init(style: UITableViewStyle.plain)
        self.navigationFlow = navigationFlow
        self.data = data as [(AnyObject)]
        self.isDoesNotMatterSelected = isDoesNotMatterSelected
        self.caller = caller
    }
    init(navigationFlow: TMNavigationFlow, data: NSArray, isDoesNotMatterSelected: Bool, caller: String, delegate: TMDataTableViewControllerDelegate) {
        super.init(style: UITableViewStyle.plain)
        self.navigationFlow = navigationFlow
        self.data = data as [(AnyObject)]
        self.isDoesNotMatterSelected = isDoesNotMatterSelected
        self.caller = caller
        self.delegate = delegate
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    override init(style: UITableViewStyle) {
        super.init(style: style)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "uiText")
        
        self.view.backgroundColor = UIColor.white
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated: false)
        self.count = self.data.count
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.count+1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.cell = self.tableView.dequeueReusableCell(withIdentifier: "uiText") //as! UITableViewCell
        if (self.cell == nil) {
            self.cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier:"uiText")
            
        }
        if((indexPath as NSIndexPath).row == 0) {
            self.cell.textLabel?.text = "Doesn't Matter"
            if(self.isDoesNotMatterSelected) {
                self.cell.accessoryType = .checkmark
            } else {
                self.cell.accessoryType = .none
            }
            self.zeroCell = self.cell
        } else {
            self.cell.textLabel?.text = self.data[(indexPath as NSIndexPath).row-1]["name"] as? String
            let isSelected = self.data[(indexPath as NSIndexPath).row-1]["isSelected"] as! Bool
            if(isSelected) {
                self.cell.accessoryType = .checkmark
            } else {
                self.cell.accessoryType = .none
            }
        }
        
        self.cell.selectionStyle = .none
        return self.cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: UITableViewCell = self.tableView.cellForRow(at: indexPath as IndexPath)!
       
        if((indexPath as NSIndexPath).row == 0) {
            if(self.isDoesNotMatterSelected) {
                cell.accessoryType = .none
            }
            else {
                cell.accessoryType = .checkmark
                self.doesNotMatterSelected()
                if(self.delegate != nil) {
                    self.delegate!.didStateChange(key: self.caller,index: (indexPath as NSIndexPath).row-1)
                }
            }
            self.isDoesNotMatterSelected = !self.isDoesNotMatterSelected
        } else {
            if(self.isDoesNotMatterSelected) {
                self.zeroCell.accessoryType = .none
                self.isDoesNotMatterSelected = false
            }
            let isSelected = self.data[(indexPath as NSIndexPath).row-1]["isSelected"] as! Bool
            if(!isSelected) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            self.updateStateObj(index: (indexPath as NSIndexPath).row-1,isSelected: isSelected)
            if(self.delegate != nil) {
                self.delegate!.didStateChange(key: self.caller,index: (indexPath as NSIndexPath).row-1)
            }
        }
    }

//    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
//        
//    }
    
    func doesNotMatterSelected() {
        
        self.updateDataObj()
        var cells = self.tableView.visibleCells// visibleCells() as NSArray
        for i in (1 ..< cells.count ) {
            let cell: UITableViewCell = cells[i] 
            cell.accessoryType = .none
        }
    }
    
    private func updateStateObj(index: Int, isSelected: Bool) {
        if(self.caller == "state") {
            let tmp = self.data[index] as! NSDictionary
            let stateObj = tmp.mutableCopy() as! NSMutableDictionary
            stateObj["isSelected"] = !isSelected as AnyObject?
            self.data[index] = stateObj as AnyObject
        }
        else if(self.caller == "city") {
            let tmp = self.data[index] as! NSDictionary
            let cityObj = tmp.mutableCopy() as! NSMutableDictionary
            cityObj["isSelected"] = !isSelected as AnyObject
            self.data[index] = cityObj as AnyObject
        }
        else if(self.caller == "region") {
            let tmp = self.data[index] as! NSDictionary
            let regionObj = tmp.mutableCopy() as! NSMutableDictionary
            regionObj["isSelected"] = !isSelected as AnyObject
            self.data[index] = regionObj as AnyObject
        }
    }
    
    private func updateDataObj() {
        if(self.caller == "state") {
            for i in (0 ..< self.data.count) {
                let tmp = self.data[i] as! NSDictionary
                let stateObj = tmp.mutableCopy() as! NSMutableDictionary
                stateObj["isSelected"] = false as Bool as AnyObject?
                self.data[i] = stateObj as AnyObject
            }
        }
        else if(self.caller == "city") {
            for i in (0 ..< self.data.count) {
                let tmp = self.data[i] as! NSDictionary
                let cityObj = tmp.mutableCopy() as! NSMutableDictionary
                cityObj["isSelected"] = false as Bool as AnyObject?
                self.data[i] = cityObj as AnyObject
            }
        }
        else if(self.caller == "region") {
            for i in (0 ..< self.data.count) {
                let tmp = self.data[i] as! NSDictionary
                let regionObj = tmp.mutableCopy() as! NSMutableDictionary
                regionObj["isSelected"] = false as Bool as AnyObject?
                self.data[i] = regionObj as AnyObject
            }
        }
    }
}

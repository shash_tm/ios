//
//  TMSliderTableViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSliderTableViewCell.h"
#import "TMSwiftHeader.h"
#import "NSString+TMAdditions.h"

@interface TMSliderTableViewCell ()

@property(nonatomic,strong)UILabel* titleLabel;
@property(nonatomic,strong)UILabel* valueLabel;
@property(nonatomic,strong)RangeSlider* slider;

@end

@implementation TMSliderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
       
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:self.titleLabel];
        
        self.valueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.valueLabel.backgroundColor = [UIColor clearColor];
        self.valueLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:self.valueLabel];
        
        self.slider = [[RangeSlider alloc] init];
        [self.slider addTarget:self action:@selector(rangeSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:self.slider];
    }
    return self;
}
+(NSString*)reuseIdentifier {
    return @"slidercell";
}
-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat maxWidth = CGRectGetWidth(self.contentView.frame);
    CGFloat sliderXPos = 10;
    self.slider.frame = CGRectMake(sliderXPos,
                                   CGRectGetMaxY(self.valueLabel.frame)+15,
                                   maxWidth - (2*sliderXPos),
                                   32);
    [self updateLabelsFrame];
}
-(void)updateLabelsFrame {
    CGFloat maxWidth = CGRectGetWidth(self.contentView.frame);
    CGFloat labelHeight = 30;
    CGFloat labelXPos = 10;
    CGFloat labelYPos = 5;
    
    NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
    CGSize constrintSize = CGSizeMake(maxWidth/2, labelHeight);
    CGRect titleLabelRect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    self.titleLabel.frame = CGRectMake(labelXPos, labelYPos, CGRectGetWidth(titleLabelRect), labelHeight);
    
    attributes = @{NSFontAttributeName:self.valueLabel.font};
    constrintSize = CGSizeMake(maxWidth/2, labelHeight);
    CGRect valueLabelRect = [self.valueLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    self.valueLabel.frame = CGRectMake(maxWidth - (CGRectGetWidth(valueLabelRect)+labelXPos),
                                       labelYPos,
                                       CGRectGetWidth(valueLabelRect),
                                       labelHeight);
}

-(void)setTitleText:(NSString*)titleText valueText:(NSString*)valueText {
    self.titleLabel.text = titleText;
    self.valueLabel.text = valueText;
    
    [self updateLabelsFrame];
}
-(void)setSliderUpperValue:(NSInteger)upperValue sliderLowerValue:(NSInteger)lowerValue {
    self.slider.upperValue = upperValue;
    self.slider.lowerValue = lowerValue;
}
-(void)setSliderValuesForHeight {
    self.slider.minimumValue = 48.0;
    self.slider.maximumValue = 95.0;
}

-(void)setSliderValuesForAge {
    self.slider.minimumValue = 18.0;
    self.slider.maximumValue = 70.0;
}

-(void)rangeSliderValueChanged:(RangeSlider*)rangeSlider {
    NSInteger minVal = floor(rangeSlider.lowerValue);
    NSInteger maxVal = floor(rangeSlider.upperValue);
    
    [self.delegate sliderCell:self didChangeSliderValueWithMin:maxVal maxSliderValue:minVal];
}

@end

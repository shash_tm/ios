//
//  TMTextLabelTableViewCell.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMTextLabelTableViewCell: UITableViewCell {

    var headerText: UILabel!
    var headerValue: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        
        self.headerText = UILabel()
        self.headerText.font = UIFont.systemFont(ofSize: 16)
        self.headerText.textAlignment = NSTextAlignment.left
        self.contentView.addSubview(self.headerText)
        
        self.headerValue = UILabel()
        self.headerValue.font = UIFont.systemFont(ofSize: 16)
        self.headerValue.textAlignment = NSTextAlignment.left
        self.contentView.addSubview(self.headerValue)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func headerWithText(_ text: String) {
        
        let bounds = UIScreen.main.bounds as CGRect
        
        let fontSize:CGFloat = 12
        let activeColor:UIColor = UIColor(red: 0.427, green: 0.431, blue: 0.443, alpha: 1.0)
        
        var textDict:Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = (bounds.size.width - 32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: fontSize)
        textDict["ktext"] = "Showing you matches from: " as AnyObject?
        var stringFrame = TMStringUtil.textRect(forString: textDict)
        var stringWidth = stringFrame.size.width
        var stringHeight = stringFrame.size.height

        self.headerValue.frame.origin.x = 16.0
        self.headerValue.frame.origin.y = 5.0
        self.headerValue.text = "Showing you matches from: "
        self.headerValue.frame.size.width = stringWidth
        self.headerValue.frame.size.height = stringHeight
        self.headerValue.font = UIFont.systemFont(ofSize: fontSize)
        self.headerValue.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        self.headerValue.numberOfLines = 0
        
        textDict["ktext"] = text as AnyObject?
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.headerText.frame.origin.x = 16.0
        self.headerText.frame.origin.y = self.headerValue.frame.origin.y + self.headerValue.frame.size.height + 2.0
        self.headerText.text = text
        self.headerText.frame.size.width = stringWidth
        self.headerText.frame.size.height = stringHeight
        self.headerText.font = UIFont.systemFont(ofSize: fontSize)
        self.headerText.textColor = activeColor
        self.headerText.numberOfLines = 0
    }

}

//
//  TMEmptyTableViewCell.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

class TMEmptyTableViewCell: UITableViewCell {
    
    var view: UIView!
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        self.view = UIView()
        self.view.frame.size.width = bounds.width
//        self.view.layer.borderWidth = 1.5
//        self.view.layer.borderColor = UIColor(red: 0.427, green:0.431, blue:0.443, alpha:1.0).CGColor
        self.contentView.addSubview(self.view)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}

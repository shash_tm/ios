//
//  TMSwitchTableViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSwitchTableViewCellDelegate;

@interface TMSwitchTableViewCell : UITableViewCell

@property(nonatomic,weak)id<TMSwitchTableViewCellDelegate> delegate;
+(NSString*)reuseIdentifier;
-(void)setSwitchButtonActiveStatus:(BOOL)status;

@end

@protocol TMSwitchTableViewCellDelegate <NSObject>

-(void)didChangeSwitchButtonOnState:(BOOL)buttonOnState;

@end

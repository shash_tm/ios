//
//  TMTextLabelTableViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEditPrefTextLabelTableViewCell.h"
#import "NSString+TMAdditions.h"

@interface TMEditPrefTextLabelTableViewCell ()

@property(nonatomic,strong)UILabel* headerLabel;
@property(nonatomic,strong)UILabel* valueLabel;

@property(nonatomic,assign)BOOL isMultilineMode;

@end


@implementation TMEditPrefTextLabelTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    if(self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.headerLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.headerLabel];
        
        self.valueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.valueLabel.textColor = [UIColor grayColor];
        self.valueLabel.numberOfLines = 0;
        [self.contentView addSubview:self.valueLabel];
    }
    return self;
}

+(NSString*)reuseIdentifier {
    return @"textlabelcell";
}

-(void)layoutSubviews {
    [super layoutSubviews];
    if(self.isMultilineMode) {
        CGFloat maxWidth = CGRectGetWidth(self.contentView.frame);
        CGFloat labelHeight = 30;
        CGFloat labelXPos = 20;
        CGFloat labelYPos = 5;
        
        NSDictionary *attributes = @{NSFontAttributeName:self.headerLabel.font};
        CGSize constrintSize = CGSizeMake(maxWidth-(2*labelXPos), labelHeight);
        CGRect titleLabelRect = [self.headerLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        self.headerLabel.frame = CGRectMake(labelXPos, labelYPos, CGRectGetWidth(titleLabelRect), 20);
        
        attributes = @{NSFontAttributeName:self.valueLabel.font};
        constrintSize = CGSizeMake(maxWidth-(2*labelXPos), NSUIntegerMax);
        CGRect valueLabelRect = [self.valueLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        self.valueLabel.frame = CGRectMake(labelXPos,
                                           CGRectGetMaxY(self.headerLabel.frame),
                                           CGRectGetWidth(valueLabelRect),
                                           CGRectGetHeight(valueLabelRect));
    }
}

-(void)setHeaderLabelText:(NSString*)headerLabelText valueLabelText:(NSString*)valueLabelText withMultilineMode:(BOOL)isMultilineMode {
    
    self.isMultilineMode = isMultilineMode;
    if(self.isMultilineMode) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.backgroundColor = [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:244.0f/255.0f alpha:1];
        self.textLabel.text = nil;
        self.detailTextLabel.text = nil;
        self.headerLabel.text = headerLabelText;
        self.valueLabel.text = valueLabelText;
        self.headerLabel.font = [UIFont systemFontOfSize:12];
        self.valueLabel.font = [UIFont systemFontOfSize:12];
    }
    else {
        self.backgroundColor = [UIColor whiteColor];
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.textLabel.text = headerLabelText;
        self.detailTextLabel.text = valueLabelText;
        
        self.headerLabel.text = nil;
        self.valueLabel.text = nil;
    }
}

@end

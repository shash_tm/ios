//
//  TMTrustBuilderViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit
import DigitsKit

class TMTrustBuilderViewController: TMBaseViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate {

    var from: String = ""
    var timer = Timer()
    var phonePollTimer = 0
    var connectedFrom = "trustbuilder"
    var Lclient:LIALinkedInHttpClient!
    
    
    var progressBar: UIImageView!
    var scrollView:UIScrollView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var bounds = UIScreen.main.bounds as CGRect
    
    var trustMgr = TMTrustBuilderManager()
    var trustBuilder:TMTrustBuilder?
    
    let userSession = TMUserSession.sharedInstance
    
    var trustScoreView: UIImageView!
    var scoreLabel: UILabel!
    var trustText: UILabel!
    var trustMsg: UILabel!
    
    var fbView: UIView!
    var fbButton: UIButton!
    var fbIcon: UIImageView!
    var fbLabel: UILabel!
    var fbTrustText: UILabel!
    var fbMsg: UILabel!
    var verifyFb: UIButton!
    //var rejectfb: UIButton!
    var fbHorizontalLine: UIView!
    //var fbVerticalLine: UIView!
    var fbMoved:Bool = false
    
    
    var linkedinView: UIView!
    var linkedinButton: UIButton!
    var linkedinIcon: UIImageView!
    var linkedinLabel: UILabel!
    var linkedinTrustText: UILabel!
    var linkedinMsg: UILabel!
    var verifylinkedin: UIButton!
    //var rejectlinkedin: UIButton!
    var linkedinHorizontalLine: UIView!
    //var linkedinVerticalLine: UIView!
    var linkedinMoved: Bool = false
    
    
    var phoneView: UIView!
    var phoneButton: UIButton!
    var phoneIcon: UIImageView!
    var phoneLabel: UILabel!
    var phoneTrustText: UILabel!
    var phoneMsg: UILabel!
    var phoneText: UITextField!
    var verifyPhone: UIButton!
    //var rejectPhone: UIButton!
    var phoneHorizontalLine: UIView!
    var phoneHorizontalLine1: UIView!
    //var phoneVerticalLine: UIView!
    var phoneMoved: Bool = false
    var phoneTextLine: UIView!
    
    var idView: UIView!
    var idButton: UIButton!
    var idIcon: UIImageView!
    var idLabel: UILabel!
    var idTrustText: UILabel!
    var idMsg: UILabel!
    var verifyID: UIButton!
    //var rejectID: UIButton!
    var idHorizontalLine: UIView!
    var idHorizontalLine1: UIView!
    //var idVerticalLine: UIView!
    var idPickerButton: UIButton!
    var picker: UIPickerView!
    var pickerCancel:UIButton!
    var passwordText: UITextField!
    var sendPassword: UIButton!
    var reUploadID: UIButton!
    var syncID: UIButton!
    var idMoved:Bool = false
    var idDropDown: UIImageView!
    var pickerDone: UIButton!
    
    
    var endorseView: UIView!
    var endorseButton: UIButton!
    var endorseIcon: UIImageView!
    var endorseLabel: UILabel!
    var endorseTrustText: UILabel!
    var endorseMsg: UILabel!
    var endorseHorizontalLine: UIView!
    //var endorseVerticalLine: UIView!
    var sendEndorse: UIButton!
    //var rejectEndorse: UIButton!
    var endorseUserView: UIScrollView!
    var endorseMoved: Bool = false
    var endorsedUser: UIScrollView!
    var endorseAddMoreView: UIView!
    var endorseAddMore: UIButton!
    var endorseAddMoreText: UILabel!
    
    var yPos:CGFloat = 0
    
    var extraH:CGFloat = 64.0
    
    var activeTextField: UITextField!
    
    var navigationFlow:TMNavigationFlow!
    
    //var IDDocument = ["Passport","Driving License","Pan Card","Voter ID","Aadhar Card"]
    var IDDocument:[String] = []
    var proofType = ""
    
    var continueButton: UIButton!
    
    //var alert: TMAlert!
    
    var pickerSeparator: UIView!
    
    var activityIndicator: TMActivityIndicatorView!
    
    var eventCategory: String = "trustbuilder"
    //var activityLoader: UIActivityIndicatorView!
    
    var inactiveColor:UIColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
    var activeColor:UIColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
    var errorColor: UIColor = UIColor.red
    
    var indexForId: Int = 0
    var gender: String!
    
    var imageName = ["score_inactive","score_active"]
    
    var eventDict:Dictionary<String,AnyObject> = [:]
    
    convenience init(navigationFlow: TMNavigationFlow) {
        self.init()
        self.navigationFlow = navigationFlow
    
        let userSession = TMUserSession.sharedInstance()
        let user = userSession?.user
        self.gender = user?.gender()
        
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
        eventDict["eventCategory"] = self.eventCategory as AnyObject?
        eventDict["eventAction"] = "page_load" as AnyObject?
        
        self.view.backgroundColor = UIColor.white
        Digits.sharedInstance().logOut()
        let countryCode = self.getCountryCode()
        if countryCode! == "+1"{
            self.IDDocument = ["Passport","Driving License"]
        }else if countryCode! == "+91" {
            self.IDDocument = ["Passport","Driving License","Pan Card","Voter ID","Aadhar Card"]
        }
//        let viewControllers = self.rootViewController()
        
//        var dict = Dictionary<String,AnyObject>()
//        dict["screenName"] = "TMTrustBuilderViewController"
////        dict["eventCategory"] = self.eventCategory
////        dict["eventAction"] = "page_load"
        
        TMAnalytics.sharedInstance().trackView("TMTrustBuilderViewController")
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.title = "Trust Score"//"Let's Verify You"
    
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TMTrustBuilderViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        //self.activityIndicator.startAnimating()
        
//        if(self.alert != nil) {
//            self.showAlert(self.alert.message! as String, title: "")
//        }
        
        let naviHeight = self.navigationController?.navigationBar.bounds.size.height
        let statusBarHeight:CGFloat = UIApplication.shared.statusBarFrame.size.height
        
        if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_REGISTRATION) {
            self.extraH = 0.0
            yPos = naviHeight! + statusBarHeight + 10
        }else {
            yPos = 0//naviHeight! //+ statusBarHeight
        }
        
        self.navigationItem.setHidesBackButton(false, animated: false)


        Lclient = self.client()
        self.loadData()
        
        //let notificationIdentifier: String = "SessionStateChangeNotification"
        let onSelectedSkin = Notification.Name("SessionStateChangeNotification")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(TMTrustBuilderViewController.executeHandle(notification:)),
            name: onSelectedSkin,
            object: nil)
    }
    
    func dismissKeyboard() {
        if(self.activeTextField != nil) {
            self.activeTextField.resignFirstResponder()
        }
    }

    
    func loadData() {
        if(trustMgr.isNetworkReachable()) {
            self.loadTrustData()
        }
        else {
            if(self.trustMgr.isCachedResponsePresent()) {
                self.loadTrustData()
            }
            else {
                self.showRetryViewForError()
            }
        }
    }
    
    func loadTrustData() {
        let queryString = ["login_mobile":"true"]
        self.addIndicator()
        self.activityIndicator.titleText = "Loading..."
        self.activityIndicator.startAnimating()
        
        trustMgr.trackEventDictionary = NSMutableDictionary(dictionary: eventDict)
        
        trustMgr.getTrustBuilderData(queryString, with: { (tbData:TMTrustBuilder?, err:TMError?) -> Void in
            self.removeLoader()
            
            if(tbData != nil) {
                self.trustBuilder = tbData
                self.initialize()
            }else {
                if(err?.errorCode == .TMERRORCODE_LOGOUT) {
                    if(self.actionDelegate !=  nil) {
                        self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
                    }
                }
                else if(err?.errorCode == .TMERRORCODE_NONETWORK || err?.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
                    if(!self.trustMgr.isCachedResponsePresent()) {
                        if(err?.errorCode == .TMERRORCODE_NONETWORK) {
                            self.showRetryViewForError()
                        }else {
                            self.showRetryViewForError()
                        }
                    }
                }
                else {
                    self.showRetryViewForUnknownError()
                }
            }
        })
    }
    
    func showRetryViewForUnknownError() {
        if(!self.isRetryViewShown) {
            self.showRetryView(withMessage: TM_UNKNOWN_MSG)
            self.retryView.retryButton.addTarget(self, action: #selector(TMTrustBuilderViewController.loadData), for: UIControlEvents.touchUpInside)
        }
    }
    
    func showRetryViewForError() {
        if(!self.isRetryViewShown) {
            self.showRetryView()
            self.retryView.retryButton.addTarget(self, action: #selector(TMTrustBuilderViewController.loadData), for: UIControlEvents.touchUpInside)
        }
    }
    
    func errorResponse(err: TMError) {
        if(err.errorCode == .TMERRORCODE_LOGOUT) {
            if(self.actionDelegate !=  nil) {
                self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
            }
        }else if(err.errorCode == .TMERRORCODE_NONETWORK) {
            self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }else if(err.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
            self.showAlert(msg: TM_SERVER_TIMEOUT_MSG, title: "")
        }
        else if(err.errorCode == .TMERRORCODE_DATASAVEERROR) {
            self.showAlert(msg: err.errorMessage, title: "")
        }else {
            self.showAlert(msg: "Please Try Later", title: "Oops! something went wrong.")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    func addIndicator() {
        if(self.isRetryViewShown) {
            self.removeRetryView()
        }
        let fullScreen = TMFullScreenView()
        fullScreen.frame = self.view.frame
        fullScreen.frame.origin.y = fullScreen.frame.origin.y - self.extraH
        fullScreen.backgroundColor = UIColor.white
        fullScreen.alpha = 0.8
        self.view.addSubview(fullScreen)
        
        self.activityIndicator = TMActivityIndicatorView()
        self.view.addSubview(self.activityIndicator)
        
        self.view.bringSubview(toFront: fullScreen)
        
        self.view.bringSubview(toFront: self.activityIndicator)
        
        if(self.continueButton != nil) {
            self.continueButton.isEnabled = true
        }
        
    }
    
    func removeLoader() {
        if(self.activityIndicator != nil) {
            self.activityIndicator.stopAnimating()
        }
        for view in self.view.subviews {
            if(view.isKind(of: TMFullScreenView.self)) {
                view.removeFromSuperview()
            }
        }
    }
    
    func initialize() {
        
        if(self.scrollView != nil) {
            self.scrollView.removeFromSuperview()
            self.scrollView = nil
            if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_REGISTRATION) {
                self.extraH = 0.0
                yPos = (self.navigationController?.navigationBar.bounds.size.height)! + UIApplication.shared.statusBarFrame.size.height + 10
            }else {
                yPos = 0
            }
            self.scoreLabel = nil
            self.trustText = nil
        }
        
        
        self.picker = UIPickerView()
        self.picker.frame.size.width = bounds.size.width
        self.picker.frame.size.height = self.picker.frame.size.height-54
        self.picker.frame.origin.y = bounds.size.height - self.picker.frame.size.height - self.extraH
        self.picker.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.picker.isHidden = true
        self.picker.dataSource = self
        self.picker.delegate = self
        self.view.addSubview(self.picker)
        
        self.pickerSeparator = UIView(frame: CGRect(x: 0,y: self.picker.frame.origin.y-50,width: bounds.size.width,height: 50))
        self.pickerSeparator.backgroundColor = UIColor(red: 0.922, green:0.922, blue:0.922, alpha:1.0)
        self.pickerSeparator.isHidden = true
        self.view.addSubview(self.pickerSeparator)
        
        var textDict:Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 18)
        textDict["ktext"] = "Cancel" as AnyObject?
        
        let cancelFrame = TMStringUtil.textRect(forString: textDict) as CGRect
        self.pickerCancel = UIButton(type: UIButtonType.system)
        self.pickerCancel.frame = CGRect(x: 16, y: 0, width: cancelFrame.size.width,height: 50)
        self.pickerCancel.setTitle("Cancel", for: UIControlState())
        self.pickerCancel.titleLabel?.font = UIFont.systemFont(ofSize: 18)//UIFont(name: "Arial", size: 14)
        self.pickerCancel.addTarget(self, action: #selector(TMTrustBuilderViewController.hidePicker(sender:)), for: UIControlEvents.touchUpInside)
        self.pickerCancel.isHidden = true
        self.pickerSeparator.addSubview(self.pickerCancel)

        
        textDict["ktext"] = "Done" as AnyObject?
        
        let doneFrame = TMStringUtil.textRect(forString: textDict) as CGRect
        self.pickerDone = UIButton(type: UIButtonType.system)
        self.pickerDone.frame = CGRect(x: bounds.size.width-doneFrame.size.width-16, y: 0, width: doneFrame.size.width,height: 50)
        self.pickerDone.setTitle("Done", for: UIControlState())
        self.pickerDone.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.pickerDone.addTarget(self, action: #selector(TMTrustBuilderViewController.donePicker(sender:)), for: UIControlEvents.touchUpInside)
        self.pickerDone.isHidden = true
        self.pickerSeparator.addSubview(self.pickerDone)
        
        
        textDict["kfont"] = UIFont.systemFont(ofSize: 18)
    
        //scroll view
        self.scrollView = UIScrollView(frame: self.view.bounds)
        self.scrollView.frame.origin.y = yPos
        self.scrollView.frame.size.width = bounds.size.width
        self.scrollView.frame.size.height = bounds.size.height-self.scrollView.frame.origin.y
        self.view.addSubview(self.scrollView)
        
        yPos = self.scrollView.frame.origin.y
        
        //trust image
        self.trustScoreView = UIImageView()
        let trustImage = UIImage(named: imageName[0])!
        self.trustScoreView.image = trustImage
        self.trustScoreView.frame = CGRect(x: 16+(bounds.size.width-32)/4,y: 30,width: (bounds.size.width-32)/2,height: (bounds.size.width-32)/2)
        self.scrollView.addSubview(self.trustScoreView)
        
        yPos = self.trustScoreView.frame.origin.y
        
        //facebook block
        self.fbView = UIView(frame: CGRect(x: 16, y: yPos+self.trustScoreView.frame.size.width+30, width: bounds.size.width-32, height: 44))
        self.fbView.layer.borderWidth = 0.75
        self.fbView.layer.cornerRadius = 10
        self.fbView.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.scrollView.addSubview(fbView)
        
        self.fbButton = UIButton(type: UIButtonType.system)
        self.fbButton.frame = CGRect(x: 0, y: 0, width: self.fbView.frame.size.width, height: 44)
        self.fbButton.layer.cornerRadius = 10
        self.fbButton.addTarget(self, action: #selector(TMTrustBuilderViewController.expandfacebook(sender:)), for: UIControlEvents.touchUpInside)
        self.fbView.addSubview(self.fbButton)
        
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 13)
        textDict["ktext"] = "Facebook" as AnyObject?
        
        var stringFrame = TMStringUtil.textRect(forString: textDict)
        var stringWidth = stringFrame.size.width
        var stringHeight = stringFrame.size.height
        
        self.fbLabel = UILabel(frame: CGRect(x: 30, y: (self.fbButton.frame.origin.y+self.fbButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.fbLabel.textAlignment = NSTextAlignment.center
        self.fbLabel.text = "Facebook"
        self.fbLabel.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.fbLabel.font = UIFont.systemFont(ofSize: 13);
        self.fbView.addSubview(self.fbLabel)
        
        self.fbIcon = UIImageView()
        let fbImage: UIImage = UIImage(named: "facebook")!
        self.fbIcon.image = fbImage
        self.fbIcon.frame = CGRect(x: self.fbButton.frame.origin.x+10, y: (self.fbButton.frame.origin.y+self.fbButton.frame.size.height-15)/2, width: 15, height: 15)
        self.fbView.addSubview(self.fbIcon)
        
        let fbScore = "\(self.trustBuilder!.fbScore!)%"
        textDict["ktext"] = fbScore as AnyObject
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.fbTrustText = UILabel(frame: CGRect(x: self.fbButton.frame.size.width-stringWidth-10, y: (self.fbButton.frame.origin.y+self.fbButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.fbTrustText.textAlignment = NSTextAlignment.center
        self.fbTrustText.text = fbScore
        self.fbTrustText.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.fbTrustText.font = UIFont.systemFont(ofSize: 13);
        self.fbView.addSubview(self.fbTrustText)
        
        
        if(self.trustBuilder!.isFbVerified) {
            let fbConnections = "\(self.trustBuilder!.fbConnection!) Connections"
            textDict["ktext"] = fbConnections as AnyObject
            stringFrame = TMStringUtil.textRect(forString: textDict)
            stringWidth = stringFrame.size.width
            self.fbView.layer.borderWidth = 0.0
            self.fbButton.isEnabled = false
            self.fbButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.fbLabel.frame.size.width = stringWidth
            self.fbLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.fbLabel.text = fbConnections
            self.fbIcon.image = UIImage(named: "facebook_highlight_trust")
            self.fbTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
        }
        
        yPos = self.fbView.frame.origin.y
        
        // linekdin block
        self.linkedinView = UIView(frame: CGRect(x: 16, y: yPos+self.fbView.frame.size.height+10, width: bounds.size.width-32, height: 44))
        self.linkedinView.layer.borderWidth = 0.75
        self.linkedinView.layer.cornerRadius = 10
        self.linkedinView.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.scrollView.addSubview(linkedinView)
        
        self.linkedinButton = UIButton(type: UIButtonType.system)
        self.linkedinButton.frame = CGRect(x: 0, y: 0, width: self.linkedinView.frame.size.width, height: 44)
        self.linkedinButton.layer.cornerRadius = 10
        self.linkedinButton.addTarget(self, action: #selector(TMTrustBuilderViewController.expandLinkedin(sender:)), for: UIControlEvents.touchUpInside)
        self.linkedinView.addSubview(self.linkedinButton)
        
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 13)
        textDict["ktext"] = "Linkedin" as AnyObject?
        
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.linkedinLabel = UILabel(frame: CGRect(x: 30, y: (self.linkedinButton.frame.origin.y+self.linkedinButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.linkedinLabel.textAlignment = NSTextAlignment.center
        self.linkedinLabel.text = "Linkedin"
        self.linkedinLabel.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.linkedinLabel.font = UIFont.systemFont(ofSize: 13);
        self.linkedinView.addSubview(self.linkedinLabel)
        
        self.linkedinIcon = UIImageView()
        self.linkedinIcon.image = UIImage(named: "link_trust")!
        self.linkedinIcon.frame = CGRect(x: self.linkedinButton.frame.origin.x+10, y: (self.linkedinButton.frame.origin.y+self.linkedinButton.frame.size.height-15)/2, width: 15, height: 15)
        self.linkedinView.addSubview(self.linkedinIcon)
        
        textDict["ktext"] = "\(self.trustBuilder!.linkedinScore!)%" as AnyObject
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.linkedinTrustText = UILabel(frame: CGRect(x: self.linkedinButton.frame.size.width-stringWidth-10, y: (self.linkedinButton.frame.origin.y+self.linkedinButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.linkedinTrustText.textAlignment = NSTextAlignment.center
        self.linkedinTrustText.text = "\(self.trustBuilder!.linkedinScore!)%"
        self.linkedinTrustText.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.linkedinTrustText.font = UIFont.systemFont(ofSize: 13);
        self.linkedinView.addSubview(self.linkedinTrustText)
        
        if(self.trustBuilder!.isLinkedinVerified) {
            let linkedInDesignation = self.trustBuilder!.linkedinDesignation!
            self.linkedinButton.isEnabled = false
            self.linkedinView.layer.borderWidth = 0.0

            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 13)
            textDict["ktext"] = linkedInDesignation as AnyObject?
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            
            self.linkedinLabel.frame.size.width = stringWidth
            self.linkedinButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.linkedinLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.linkedinLabel.text = linkedInDesignation
            self.linkedinIcon.image = UIImage(named: "link_highlight_trust")
            self.linkedinTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            
        }
        
         yPos = self.linkedinView.frame.origin.y
        //phone block
        
        self.phoneView = UIView(frame: CGRect(x: 16, y: yPos+self.linkedinView.frame.size.height+10, width: bounds.size.width-32, height: 44))
        self.phoneView.layer.borderWidth = 0.75
        self.phoneView.layer.cornerRadius = 10
        self.phoneView.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.scrollView.addSubview(phoneView)
        
        self.phoneButton = UIButton(type: UIButtonType.system)
        self.phoneButton.frame = CGRect(x: 0, y: 0, width: self.phoneView.frame.size.width, height: 44)
        self.phoneButton.layer.cornerRadius = 10
        self.phoneButton.addTarget(self, action: #selector(TMTrustBuilderViewController.expandPhone(sender:)), for: UIControlEvents.touchUpInside)
        self.phoneView.addSubview(self.phoneButton)
        
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 13)
        textDict["ktext"] = "Phone Number" as AnyObject?
        
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.phoneLabel = UILabel(frame: CGRect(x: 30, y: (self.phoneButton.frame.origin.y+self.phoneButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.phoneLabel.textAlignment = NSTextAlignment.center
        self.phoneLabel.text = "Phone Number"
        self.phoneLabel.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.phoneLabel.font = UIFont.systemFont(ofSize: 13)
        self.phoneView.addSubview(self.phoneLabel)
        
        self.phoneIcon = UIImageView()
        self.phoneIcon.image = UIImage(named: "Phone")!
        self.phoneIcon.frame = CGRect(x: self.phoneButton.frame.origin.x+10, y: (self.phoneButton.frame.origin.y+self.phoneButton.frame.size.height-16)/2, width: 10, height: 16)
        self.phoneView.addSubview(self.phoneIcon)
        
        textDict["ktext"] = "\(self.trustBuilder!.mobileScore!)%" as AnyObject?
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.phoneTrustText = UILabel(frame: CGRect(x: self.phoneButton.frame.size.width-stringWidth-10, y: (self.phoneButton.frame.origin.y+self.phoneButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.phoneTrustText.textAlignment = NSTextAlignment.center
        self.phoneTrustText.text = "\(self.trustBuilder!.mobileScore!)%"
        self.phoneTrustText.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.phoneTrustText.font = UIFont.systemFont(ofSize: 13);
        self.phoneView.addSubview(self.phoneTrustText)
        if(self.trustBuilder!.isPhoneVerified) {
            self.phoneView.layer.borderWidth = 0.0

            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 13)
            textDict["ktext"] = self.trustBuilder!.phoneNumber! as AnyObject
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            
            self.phoneButton.isEnabled = false
            self.phoneButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.phoneLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.phoneLabel.frame.size.width = stringWidth
            self.phoneLabel.text = self.trustBuilder!.phoneNumber!
            self.phoneIcon.image = UIImage(named: "Phone_Highlight_Trust")
            self.phoneTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            
        }
        
        yPos = self.phoneView.frame.origin.y
        //photo id block
        self.idView = UIView(frame: CGRect(x: 16, y: yPos+self.phoneView.frame.size.height+10, width: bounds.size.width-32, height: 44))
        self.idView.layer.borderWidth = 0.75
        self.idView.layer.cornerRadius = 10
        self.idView.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.scrollView.addSubview(idView)
        
        self.idButton = UIButton(type: UIButtonType.system)
        self.idButton.frame = CGRect(x: 0, y: 0, width: self.idView.frame.size.width, height: 44)
        self.idButton.layer.cornerRadius = 10
        self.idButton.addTarget(self, action: #selector(TMTrustBuilderViewController.expandID(sender:)), for: UIControlEvents.touchUpInside)
        self.idView.addSubview(self.idButton)
        
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 13)
        textDict["ktext"] = "Photo ID" as AnyObject?
        
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.idLabel = UILabel(frame: CGRect(x: 30, y: (self.idButton.frame.origin.y+self.idButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.idLabel.textAlignment = NSTextAlignment.center
        self.idLabel.text = "Photo ID"
        self.idLabel.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.idLabel.font = UIFont.systemFont(ofSize: 13);
        self.idView.addSubview(self.idLabel)
        
        self.idIcon = UIImageView()
        self.idIcon.image = UIImage(named: "PhotoID")!
        self.idIcon.frame = CGRect(x: self.idButton.frame.origin.x+10, y: (self.idButton.frame.origin.y+self.idButton.frame.size.height-14)/2, width: 15, height: 14)
        self.idView.addSubview(self.idIcon)
        
        textDict["ktext"] = "\(self.trustBuilder!.idScore!)%" as AnyObject?
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.idTrustText = UILabel(frame: CGRect(x: self.idButton.frame.size.width-stringWidth-10, y: (self.idButton.frame.origin.y+self.idButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.idTrustText.textAlignment = NSTextAlignment.center
        self.idTrustText.text = "\(self.trustBuilder!.idScore!)%"
        self.idTrustText.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.idTrustText.font = UIFont.systemFont(ofSize: 13);
        self.idView.addSubview(self.idTrustText)
        
        if(self.trustBuilder!.isIdVerified) {
            self.idView.layer.borderWidth = 0.0
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 13)
            textDict["ktext"] = self.trustBuilder!.idType! as AnyObject?
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            
            self.idButton.isEnabled = false
            self.idButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.idLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.idLabel.frame.size.width = stringWidth
            self.idLabel.text = self.trustBuilder!.idType!
            self.idIcon.image = UIImage(named: "PhotoID_Highlight_Trust")
            self.idTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
        }
        
        
        //endorsement block
        yPos = self.idView.frame.origin.y
        
        self.endorseView = UIView(frame: CGRect(x: 16, y: yPos+self.idView.frame.size.height+10, width: bounds.size.width-32, height: 44))
        self.endorseView.layer.borderWidth = 0.75
        self.endorseView.layer.cornerRadius = 10
        self.endorseView.layer.borderColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).cgColor
        self.scrollView.addSubview(endorseView)
        
        self.endorseButton = UIButton(type: UIButtonType.system)
        self.endorseButton.frame = CGRect(x: 0, y: 0, width: self.endorseView.frame.size.width, height: 44)
        self.endorseButton.layer.cornerRadius = 10
        self.endorseButton.addTarget(self, action: #selector(TMTrustBuilderViewController.expandEndorsement(sender:)), for: UIControlEvents.touchUpInside)
        self.endorseView.addSubview(self.endorseButton)
        
        let count = self.trustBuilder!.endorseData.count
        let thresholdCount = self.trustBuilder!.endorseCount
        
        textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 13)
        if(count > 0 && count < thresholdCount) {
            textDict["ktext"] = self.trustBuilder!.endorsementMsg as AnyObject//"You need 1 or more endorsements"
        }else if(count >= thresholdCount) {
            textDict["ktext"] = "Request more friends." as AnyObject?//String(count) + " Endorsements"
        }else {
            textDict["ktext"] = "References" as AnyObject?
        }
        
        let endorsementsText = NSMutableAttributedString(string: textDict["ktext"] as! String, attributes: [NSForegroundColorAttributeName : UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)])
//        if(count == 1) {
//            endorsementsText.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0), range: NSRange(location:9,length:9))
//        }
    
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.endorseLabel = UILabel(frame: CGRect(x: 30, y: (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.endorseLabel.textAlignment = NSTextAlignment.center
        self.endorseLabel.attributedText = endorsementsText //textDict["ktext"] as? String
        self.endorseLabel.font = UIFont.systemFont(ofSize: 13)
        self.endorseView.addSubview(self.endorseLabel)
        
        self.endorseIcon = UIImageView()
        self.endorseIcon.image = UIImage(named: "Endorsements")!
        self.endorseIcon.frame = CGRect(x: self.endorseButton.frame.origin.x+10, y: (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height-stringHeight)/2+1, width: 13, height: stringHeight-2)
        self.endorseView.addSubview(self.endorseIcon)
        
        textDict["ktext"] = "\(self.trustBuilder!.endorseScore!)%" as AnyObject
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.endorseTrustText = UILabel(frame: CGRect(x: self.endorseButton.frame.size.width-stringWidth-10, y: (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height-stringHeight)/2, width: stringWidth, height: stringHeight))
        self.endorseTrustText.textAlignment = NSTextAlignment.center
        self.endorseTrustText.text = "\(self.trustBuilder!.endorseScore!)%"
        self.endorseTrustText.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        self.endorseTrustText.font = UIFont.systemFont(ofSize: 13);
        self.endorseView.addSubview(self.endorseTrustText)
        
        //if(count >= self.trustBuilder?.endorseCount) {
        if(self.trustBuilder!.isEndorseVerified) {
            
            self.endorseView.layer.borderWidth = 0.0
            self.endorseView.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.endorseLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.endorseIcon.image = UIImage(named: "Endorsements_Highlight")
            self.endorseTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
        }
        
        yPos = self.endorseView.frame.origin.y
        
        self.endorsedUser = UIScrollView(frame: CGRect(x: 16, y: (yPos+self.endorseView.frame.size.height+10), width: self.endorseView.frame.size.width, height: 0))
        self.endorsedUser.frame.origin.y = (self.endorseView.frame.origin.y+self.endorseView.frame.size.height + 10)
        self.endorsedUser.frame.size.height = 0
        self.scrollView.addSubview(self.endorsedUser)
        
        if(count > 0) {
            self.endorseButton.isEnabled = false
            self.addEndorseData(from: "initialize")
        }
        
        yPos = self.endorsedUser.frame.origin.y
        
        if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_REGISTRATION) {
            self.extraH = 0.0
        }
        
        self.setTrustScore(trustScore: self.trustBuilder!.trustScore)
        
        if(self.continueButton != nil) {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0+self.extraH)
        }else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
        }
        
        
        if(self.trustBuilder?.fbMismatchReason != nil){
            self.expandfacebook(sender: true as AnyObject)
        }
        
        self.view.bringSubview(toFront: self.picker)
        self.view.bringSubview(toFront: self.pickerSeparator)
        self.view.bringSubview(toFront: self.activityIndicator)

    }
    
    func changeContinueText() {
        if(self.continueButton != nil) {
            self.continueButton.setTitle("Continue", for: UIControlState())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func expandfacebook(sender:AnyObject) {
        self.fbButton.isEnabled = true
        
        self.collapseView(key: "fb")
        
        if(!self.fbMoved) {
            self.fbMoved = true
        
            self.fbView.layer.borderColor = self.activeColor.cgColor
            self.fbLabel.textColor = self.activeColor
            self.fbTrustText.textColor = self.activeColor
            
            self.fbIcon.image = UIImage(named: "facebook_active")!
        
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-52) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
            if(self.trustBuilder!.fbMsg != nil) {
                textDict["ktext"] = self.trustBuilder!.fbMsg as AnyObject?
            }else {
                textDict["ktext"] = "We will never post on your wall." as AnyObject?
            }
        
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            let stringHeight = stringFrame.size.height
        
            self.fbMsg = UILabel(frame: CGRect(x: self.fbIcon.frame.origin.x, y: (self.fbButton.frame.origin.y+self.fbButton.frame.size.height), width: stringWidth, height: stringHeight))
            self.fbMsg.textAlignment = NSTextAlignment.left
            self.fbMsg.text = textDict["ktext"] as? String
            
            if(self.trustBuilder!.fbError) {
                self.fbMsg.textColor = self.errorColor
            }
            else {
                self.fbMsg.textColor = self.activeColor  //UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
            }
            
            self.fbMsg.font = UIFont.systemFont(ofSize: 14);
            self.fbMsg.numberOfLines = 0
            self.fbMsg.sizeToFit()
            self.fbView.addSubview(self.fbMsg)
        
            self.fbView.frame.size.height = self.fbView.frame.size.height + 15 + self.fbMsg.frame.size.height
            self.fbButton.frame.size.height = self.fbView.frame.size.height
        
            let y = self.fbMsg.frame.origin.y
            self.fbHorizontalLine = UIView(frame: CGRect(x: 0,y: y+stringHeight+15,width: self.fbView.frame.size.width,height: 0.6))
            self.fbHorizontalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
            self.fbView.addSubview(fbHorizontalLine)
        
//            self.fbVerticalLine = UIView(frame: CGRectMake(self.fbView.frame.size.width/2,y+stringHeight+15,0.75,44))
//            self.fbVerticalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
//            self.fbView.addSubview(self.fbVerticalLine)
        
            self.fbView.frame.size.height = self.fbView.frame.size.height + 44
        
            self.verifyFb = UIButton(type: UIButtonType.system)
            self.verifyFb.frame = CGRect(x: 0, y: y+stringHeight+15, width: self.fbView.frame.size.width, height: 44)
            self.verifyFb.setTitleColor(self.activeColor, for: UIControlState())
            if(self.trustBuilder?.fbMismatchReason != nil){
                self.verifyFb.setTitle("Sync with facebook", for: UIControlState())
            }else {
                self.verifyFb.setTitle("Verify", for: UIControlState())
            }
            self.verifyFb.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            self.verifyFb.titleLabel?.textAlignment = .center
            self.verifyFb.addTarget(self, action: #selector(TMTrustBuilderViewController.onFacebookClick(sender:)), for: UIControlEvents.touchUpInside)
            self.fbView.addSubview(self.verifyFb)
        
//            self.rejectfb = UIButton(type: UIButtonType.System)
//            self.rejectfb.frame = CGRectMake(self.fbView.frame.size.width/2, y+stringHeight+15, self.fbView.frame.size.width/2, 44)
//            self.rejectfb.setTitleColor(self.activeColor, forState: UIControlState.Normal)
//            self.rejectfb.setTitle("Not Now", forState: UIControlState.Normal)
//            self.rejectfb.titleLabel?.font = UIFont.systemFontOfSize(14)
//            self.rejectfb.titleLabel?.textAlignment = .Center
//            self.rejectfb.addTarget(self, action: "collapseFB:", forControlEvents: UIControlEvents.TouchUpInside)
//            self.fbView.addSubview(self.rejectfb)
        
            if(self.linkedinView != nil) {
                self.linkedinView.frame.origin.y = self.fbView.frame.origin.y + self.fbView.frame.size.height + 10
                if(self.phoneView != nil) {
                    self.phoneView.frame.origin.y = self.linkedinView.frame.origin.y + self.linkedinView.frame.size.height + 10
                    if(self.idView != nil) {
                        self.idView.frame.origin.y = self.phoneView.frame.origin.y + self.phoneView.frame.size.height + 10
                    }
                    if(self.endorseView != nil) {
                        self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
                    }
                    if(self.endorsedUser != nil) {
                        self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
                        //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
                    }
                    if(self.continueButton != nil) {
                        self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
                        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
                    }else {
                        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
                    }
                }
            }
        }else {
            self.collapseFB(sender: true as AnyObject)
        }
    }
    
    func collapseFB(sender: AnyObject) {
//        if(sender as! NSObject == self.rejectfb) {
//            self.fbButton.enabled = true
//        }
        
        self.fbView.layer.borderColor = self.inactiveColor.cgColor
        self.fbLabel.textColor = self.inactiveColor
        self.fbTrustText.textColor = self.inactiveColor
        
        self.fbIcon.image = UIImage(named: "facebook")
        self.fbMsg.removeFromSuperview()
        self.fbHorizontalLine.removeFromSuperview()
        //self.fbVerticalLine.removeFromSuperview()
        self.verifyFb.removeFromSuperview()
        //self.rejectfb.removeFromSuperview()
        
        self.fbView.frame.size.height = 44
        self.fbButton.frame.size.height = self.fbView.frame.size.height
        
        self.linkedinView.frame.origin.y = self.fbView.frame.origin.y + self.fbView.frame.size.height + 10
        
        self.phoneView.frame.origin.y = self.linkedinView.frame.origin.y + self.linkedinView.frame.size.height + 10
        
        self.idView.frame.origin.y = self.phoneView.frame.origin.y + self.phoneView.frame.size.height + 10
        
        self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
        
        self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        if(self.continueButton != nil) {
            self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
        }else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
        }
        
        self.fbMoved = false

    }
    
    
    func expandLinkedin(sender: AnyObject) {
        
        
        self.linkedinButton.isEnabled = true
        
        self.collapseView(key: "linkedin")
     
        if(!self.linkedinMoved) {
            self.linkedinMoved = true
            
            self.linkedinView.layer.borderColor = self.activeColor.cgColor
            self.linkedinLabel.textColor = self.activeColor
            self.linkedinTrustText.textColor = self.activeColor
            
            self.linkedinIcon.image = UIImage(named: "link_active_trust")!
        
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-52) as AnyObject
            
            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
            textDict["ktext"] = self.trustBuilder!.linkedinMsg as AnyObject?//"We won't post on your profile."
        
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            let stringHeight = stringFrame.size.height
        
            self.linkedinMsg = UILabel(frame: CGRect(x: self.linkedinIcon.frame.origin.x, y: (self.linkedinButton.frame.origin.y+self.linkedinButton.frame.size.height), width: stringWidth, height: stringHeight))
            self.linkedinMsg.textAlignment = NSTextAlignment.left
            self.linkedinMsg.text = self.trustBuilder!.linkedinMsg//"We won't post on your profile."
            if(self.trustBuilder!.linkedinError) {
                self.linkedinMsg.textColor = self.errorColor
            }
            else {
                self.linkedinMsg.textColor = self.activeColor
            }
            //self.linkedinMsg.textColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
            self.linkedinMsg.font = UIFont.systemFont(ofSize: 14);
            self.linkedinMsg.numberOfLines = 0
            self.linkedinMsg.sizeToFit()
            self.linkedinView.addSubview(self.linkedinMsg)
        
            self.linkedinView.frame.size.height = self.linkedinView.frame.size.height + 15 + self.linkedinMsg.frame.size.height
            self.linkedinButton.frame.size.height = self.linkedinView.frame.size.height
        
            let y = self.linkedinMsg.frame.origin.y
        
            self.linkedinHorizontalLine = UIView(frame: CGRect(x: 0,y: y+stringHeight+15,width: self.linkedinView.frame.size.width,height: 0.6))
            self.linkedinHorizontalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
            self.linkedinView.addSubview(linkedinHorizontalLine)
        
//            self.linkedinVerticalLine = UIView(frame: CGRectMake(self.linkedinView.frame.size.width/2,y+stringHeight+15,0.75,44))
//            self.linkedinVerticalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
//            self.linkedinView.addSubview(linkedinVerticalLine)
        
            self.linkedinView.frame.size.height = self.linkedinView.frame.size.height + 44
        
            self.verifylinkedin = UIButton(type: UIButtonType.system)
            self.verifylinkedin.frame = CGRect(x: 0, y: y+stringHeight+15, width: self.linkedinView.frame.size.width, height: 44)
            self.verifylinkedin.setTitleColor(self.activeColor, for: UIControlState())
       
            if(self.trustBuilder!.isLinkedinVerified) {
                self.verifylinkedin.setTitle("Re-Verify", for: UIControlState())
            }else {
                self.verifylinkedin.setTitle("Verify", for: UIControlState())
            }
        
            self.verifylinkedin.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            self.verifylinkedin.titleLabel?.textAlignment = .center
            self.verifylinkedin.addTarget(self, action: #selector(TMTrustBuilderViewController.onLinkedinClick(sender:)), for: UIControlEvents.touchUpInside)
            self.linkedinView.addSubview(self.verifylinkedin)
        
//            self.rejectlinkedin = UIButton(type: UIButtonType.System)
//            self.rejectlinkedin.frame = CGRectMake(self.linkedinView.frame.size.width/2, y+stringHeight+15, self.linkedinView.frame.size.width/2, 44)
//            self.rejectlinkedin.setTitleColor(self.activeColor, forState: UIControlState.Normal)
//            self.rejectlinkedin.setTitle("Not Now", forState: UIControlState.Normal)
//            self.rejectlinkedin.titleLabel?.font = UIFont.systemFontOfSize(14)
//            self.rejectlinkedin.titleLabel?.textAlignment = .Center
//            self.rejectlinkedin.addTarget(self, action: "collapseLinkedin:", forControlEvents: UIControlEvents.TouchUpInside)
//            self.linkedinView.addSubview(self.rejectlinkedin)
        
            self.phoneView.frame.origin.y = self.linkedinView.frame.origin.y + self.linkedinView.frame.size.height + 10
        
            self.idView.frame.origin.y = self.phoneView.frame.origin.y + self.phoneView.frame.size.height + 10
            
            self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
            
            self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
            
            //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
            
            if(self.continueButton != nil) {
                self.continueButton.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
            }else {
                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
            }
        }else {
            self.collapseLinkedin(sender: true as AnyObject)
        }
    }
    
    
    func collapseLinkedin(sender: AnyObject) {
        //self.linkedinButton.enabled = true
        
        self.linkedinView.layer.borderColor = self.inactiveColor.cgColor
        self.linkedinLabel.textColor = self.inactiveColor
        self.linkedinTrustText.textColor = self.inactiveColor
        
        if(self.trustBuilder!.isLinkedinVerified) {
            self.linkedinIcon.image = UIImage(named: "link_highlight_trust")!
            self.linkedinButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
        }else {
            self.linkedinIcon.image = UIImage(named: "link_trust")!
            self.linkedinButton.backgroundColor = UIColor.clear
        }
        self.linkedinMsg.removeFromSuperview()
        self.linkedinHorizontalLine.removeFromSuperview()
        //self.linkedinVerticalLine.removeFromSuperview()
        self.verifylinkedin.removeFromSuperview()
        //self.rejectlinkedin.removeFromSuperview()
        
        self.linkedinView.frame.size.height = 44
        self.linkedinButton.frame.size.height = self.linkedinView.frame.size.height
        
        self.phoneView.frame.origin.y = self.linkedinView.frame.origin.y + self.linkedinView.frame.size.height + 10
        
        self.idView.frame.origin.y = self.phoneView.frame.origin.y + self.phoneView.frame.size.height + 10
        
        self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
        
        self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        if(self.continueButton != nil) {
            self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
        }else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
        }

        self.linkedinMoved = false
    }
    
    
    
    func expandPhone(sender: AnyObject) {
       
        
        self.collapseView(key: "phone")
        let countryCode = self.getCountryCode()
        let digits = Digits.sharedInstance()
        let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
        if countryCode != nil {
            configuration?.phoneNumber = countryCode!
        }
        configuration?.appearance = DGTAppearance()
        configuration?.appearance.accentColor = UIColor.lightGray
        digits.authenticate(with: nil, configuration: configuration!) { session, error in
            self.trackVerifyPhone()
            if (session != nil) {
                self.sendDigitVerificationTokenToServer(phoneNumber: (session?.phoneNumber)!)
            } else {
                let error = error as! NSError
                let errorCode = error.code
                if errorCode == 1 {
                    self.sendTrackDetails(eventStatus: "cancelled", eventAction: "phone_click")
                }else {
                    self.showAlert(msg: TM_DIGITS_ERROR_MSG, title: "")
                    self.sendTrackDetails(eventStatus: "digitsErrorCode : \(errorCode)", eventAction: "phone_verify")
                }
            }
        }
 //       return
            
//        self.phoneButton.isEnabled = true
//        if(!self.phoneMoved) {
//            self.phoneMoved = true
//            self.phoneView.layer.borderColor = self.activeColor.cgColor
//            self.phoneLabel.textColor = self.activeColor
//            self.phoneTrustText.textColor = self.activeColor
//        
//            self.phoneIcon.image = UIImage(named: "Phone_active")!
//        
//            var textDict:Dictionary<String,AnyObject> = [:]
//            textDict["kmaxwidth"] = (bounds.size.width-52) as AnyObject
//            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
//            textDict["ktext"] = self.trustBuilder!.phoneRejectedMsg as AnyObject?
//        
//            let stringFrame = TMStringUtil.textRect(forString: textDict)
//            let stringWidth = stringFrame.size.width
//            let stringHeight = stringFrame.size.height
//        
//            self.phoneMsg = UILabel(frame: CGRect(x: self.phoneIcon.frame.origin.x, y: (self.phoneButton.frame.origin.y+self.phoneButton.frame.size.height), width: stringWidth, height: stringHeight))
//            self.phoneMsg.textAlignment = NSTextAlignment.left
//            self.phoneMsg.text = self.trustBuilder!.phoneRejectedMsg
//            self.phoneMsg.font = UIFont.systemFont(ofSize: 14);
//            self.phoneMsg.numberOfLines = 0
//            self.phoneMsg.sizeToFit()
//            self.phoneView.addSubview(self.phoneMsg)
//        
//            if(self.trustBuilder!.number_of_trials < 3) {
//                if(self.trustBuilder!.phoneError) {
//                    self.phoneMsg.textColor = self.errorColor
//                }
//                else {
//                    self.phoneMsg.textColor = self.activeColor
//                }
//                //self.phoneMsg.textColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
//                
//                self.phoneView.frame.size.height = self.phoneView.frame.size.height + 15 + self.phoneMsg.frame.size.height
//                self.phoneButton.frame.size.height = self.phoneView.frame.size.height
//        
//                var y = self.phoneMsg.frame.origin.y
//        
////                self.phoneHorizontalLine = UIView(frame: CGRectMake(0,y+stringHeight+15,self.phoneView.frame.size.width,0.6))
////                self.phoneHorizontalLine.backgroundColor = self.activeColor
////                self.phoneView.addSubview(phoneHorizontalLine)
//        
//                textDict["kfont"] = UIFont.systemFont(ofSize: 15)
//                textDict["ktext"] = "Phone Number" as AnyObject?
//                
//                let stringFramePlace = TMStringUtil.textRect(forString: textDict)
//                let stringPlaceWidth = stringFramePlace.size.width
//                
//                self.phoneText = UITextField(frame: CGRect(x: (self.phoneView.frame.size.width-stringPlaceWidth-28)/2, y: y+stringHeight+5.0, width: stringPlaceWidth+35, height: 44))
//                let placeholder = NSAttributedString(string: "Phone Number", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.82, green:0.82, blue:0.82, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
//                self.phoneText.attributedPlaceholder = placeholder
//                self.phoneText.textAlignment = .left
//                self.phoneText.font = UIFont.systemFont(ofSize: 15)
//                self.phoneText.keyboardType = .phonePad
//                self.phoneText.delegate = self
//                self.phoneText.layer.borderColor = self.activeColor.cgColor
//                self.phoneText.addTarget(self, action: #selector(TMTrustBuilderViewController.textFieldDidChange), for: .editingChanged)
//                self.phoneView.addSubview(phoneText)
//                
//                let paddingView: UIView = UIView(frame: CGRect(x: self.phoneText.frame.origin.x,y: y+stringHeight,width: 28,height: 44))
//                //paddingView.backgroundColor = UIColor.redColor()
//                self.phoneText.leftView = paddingView
//                let paddingText : UILabel = UILabel(frame: CGRect(x: 0,y: 0,width: paddingView.frame.size.width,height: 44))
//                paddingView.addSubview(paddingText)
//                paddingText.text = "+91 ";
//                paddingText.textColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
//                paddingText.font = UIFont.systemFont(ofSize: 15)
//                self.phoneText.leftViewMode = .always
//                
//                self.phoneTextLine = UIView(frame: CGRect(x: self.phoneText.frame.origin.x+28,y: self.phoneText.frame.origin.y+32,width: stringPlaceWidth,height: 1))
//                phoneTextLine.backgroundColor = self.activeColor
//                self.phoneView.addSubview(self.phoneTextLine)
//        
//                self.phoneView.frame.size.height = self.phoneView.frame.size.height + 44
//        
//                self.phoneHorizontalLine1 = UIView(frame: CGRect(x: 0,y: y+stringHeight+15+44,width: self.phoneView.frame.size.width,height: 0.6))
//                self.phoneHorizontalLine1.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
//                self.phoneView.addSubview(phoneHorizontalLine1)
//
//                self.phoneView.frame.size.height = self.phoneView.frame.size.height + 44
//        
//                y = self.phoneHorizontalLine1.frame.origin.y
//        
////                self.phoneVerticalLine = UIView(frame: CGRectMake(self.phoneView.frame.size.width/2,y,0.75,44))
////                self.phoneVerticalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
////                self.phoneView.addSubview(phoneVerticalLine)
//
//                self.verifyPhone = UIButton(type: UIButtonType.system)
//                self.verifyPhone.frame = CGRect(x: 0, y: y, width: self.phoneView.frame.size.width, height: 44)
//                self.verifyPhone.setTitleColor(self.activeColor, for: UIControlState())
//                self.verifyPhone.setTitle("Verify", for: UIControlState())
//                self.verifyPhone.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//                self.verifyPhone.titleLabel?.textAlignment = .center
//                self.verifyPhone.addTarget(self, action: #selector(TMTrustBuilderViewController.verifyPhone(sender:)), for: UIControlEvents.touchUpInside)
//                self.phoneView.addSubview(self.verifyPhone)
//        
////                self.rejectPhone = UIButton(type: UIButtonType.System)
////                self.rejectPhone.frame = CGRectMake(self.phoneView.frame.size.width/2, y, self.fbView.frame.size.width/2, 44)
////                self.rejectPhone.setTitleColor(self.activeColor, forState: UIControlState.Normal)
////                self.rejectPhone.setTitle("Not Now", forState: UIControlState.Normal)
////                self.rejectPhone.titleLabel?.font = UIFont.systemFontOfSize(14)
////                self.rejectPhone.titleLabel?.textAlignment = .Center
////                self.rejectPhone.addTarget(self, action: "collapsePhone:", forControlEvents: UIControlEvents.TouchUpInside)
////                self.phoneView.addSubview(self.rejectPhone)
//                
//            }else {
//                self.phoneMsg.textColor = self.errorColor
//                self.phoneView.frame.size.height = self.phoneView.frame.size.height + self.phoneMsg.frame.size.height + 10
//                self.phoneButton.frame.size.height = self.phoneView.frame.size.height
//            }
//        
//            self.idView.frame.origin.y = self.phoneView.frame.origin.y + self.phoneView.frame.size.height + 10
//            
//            self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
//            
//            self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
//            
//           // self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
//            
//            if(self.continueButton != nil) {
//                self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
//                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
//            }else {
//                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
//            }
//            if((self.phoneText) != nil) {
//                self.phoneText.becomeFirstResponder();
//            }
//        }else{
//            collapsePhone(sender: true as AnyObject)
//        }

    }
    
    
    func collapsePhone(sender: AnyObject) {
        
        self.phoneView.layer.borderColor = self.inactiveColor.cgColor
        self.phoneLabel.textColor = self.inactiveColor
        self.phoneTrustText.textColor = self.inactiveColor
        
        self.phoneButton.isEnabled = true
        self.phoneIcon.image = UIImage(named: "Phone")!
        self.phoneMsg.removeFromSuperview()
        if(self.phoneTextLine != nil) {
            self.phoneTextLine.removeFromSuperview()
        }
        if(self.phoneHorizontalLine1 != nil) {
            self.phoneHorizontalLine1.removeFromSuperview()
        }
//        if(self.phoneVerticalLine != nil) {
//            self.phoneVerticalLine.removeFromSuperview()
//        }
        if(self.verifyPhone != nil) {
            self.verifyPhone.removeFromSuperview()
        }
//        if(self.rejectPhone != nil) {
//            self.rejectPhone.removeFromSuperview()
//        }
        if(self.phoneText != nil) {
            self.phoneText.removeFromSuperview()
        }
        
        self.phoneView.frame.size.height = 44
        self.phoneButton.frame.size.height = self.phoneView.frame.size.height
        
        self.idView.frame.origin.y = self.phoneView.frame.origin.y + self.phoneView.frame.size.height + 10
        
        self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
        
        self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        if(self.continueButton != nil) {
            self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
        }else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
        }
        
        self.phoneMoved = false
        
    }
    
    
    func expandID(sender: AnyObject) {
        self.idButton.isEnabled = true
        
        self.collapseView(key: "id")
        
        if(!self.idMoved) {
            self.idMoved = true
            
            self.idView.layer.borderColor = self.activeColor.cgColor
            self.idLabel.textColor = self.activeColor
            self.idTrustText.textColor = self.activeColor
            
            self.idIcon.image = UIImage(named: "PhotoID_active")!
        
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-52) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
            textDict["ktext"] = self.trustBuilder!.idMsg as AnyObject?
        
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            let stringHeight = stringFrame.size.height
            
            self.idMsg = UILabel(frame: CGRect(x: self.fbIcon.frame.origin.x, y: (self.idButton.frame.origin.y+self.idButton.frame.size.height), width: stringWidth, height: stringHeight))
            self.idMsg.textAlignment = NSTextAlignment.left
            self.idMsg.text = self.trustBuilder!.idMsg
            if(self.trustBuilder!.idError) {
                self.idMsg.textColor = self.errorColor
            }
            else {
                self.idMsg.textColor = self.activeColor
            }
            //self.idMsg.textColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
            self.idMsg.font = UIFont.systemFont(ofSize: 14);
            self.idMsg.numberOfLines = 0
            self.idMsg.sizeToFit()
            self.idView.addSubview(self.idMsg)
            
            if(self.trustBuilder!.idProofStatus == "fail" || self.trustBuilder!.idProofStatus == "under_moderation") {
                
                self.idView.frame.size.height = self.idView.frame.size.height + self.idMsg.frame.size.height + 15
                
                self.idButton.frame.size.height = self.idView.frame.size.height
        
            }else {
                
                self.idView.frame.size.height = self.idView.frame.size.height + 15 + self.idMsg.frame.size.height
                
                self.idButton.frame.size.height = self.idView.frame.size.height
            
                var y = self.idMsg.frame.origin.y
                self.idHorizontalLine = UIView(frame: CGRect(x: 0,y: y+stringHeight+15,width: self.idView.frame.size.width,height: 0.6))
                self.idHorizontalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
                self.idView.addSubview(idHorizontalLine)
                
                self.idView.frame.size.height = self.idView.frame.size.height + 44
                
                //picker button
                self.idPickerButton = UIButton(type: UIButtonType.system)
                self.idPickerButton.frame = CGRect(x: 0, y: y+stringHeight+15, width: self.idView.frame.size.width, height: 44)
                self.idPickerButton.setTitleColor(self.activeColor, for: UIControlState())
                self.idPickerButton.setTitle("Photo ID", for: UIControlState())
                self.idPickerButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                self.idPickerButton.contentHorizontalAlignment = .center
                self.idPickerButton.addTarget(self, action: #selector(TMTrustBuilderViewController.idPickerButtonClick(sender:)), for: UIControlEvents.touchUpInside)
                self.idView.addSubview(self.idPickerButton)
                
                self.idDropDown = UIImageView()
                self.idDropDown.image = UIImage(named: "Dropdown_black")
                self.idDropDown.frame = CGRect(x: (self.idPickerButton.frame.size.width-36), y: (self.idPickerButton.frame.origin.y+(self.idPickerButton.frame.size.height-10)/2) ,width: 16,height: 10)
                self.idView.addSubview(self.idDropDown)
                
                if(self.trustBuilder!.idProofStatus == "rejected" && self.trustBuilder!.idRejectReason == "password_required") {
                    
                    //password Button
                    self.passwordText = UITextField(frame: CGRect(x: 0, y: y+stringHeight+15, width: self.idView.frame.size.width, height: 44))
                    let placeholder = NSAttributedString(string: "Enter Password", attributes: [NSForegroundColorAttributeName : UIColor(red: 0.0, green:0.0, blue:0.0, alpha:1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
                    self.passwordText.attributedPlaceholder = placeholder
                    self.passwordText.textAlignment = .center
                    self.passwordText.font = UIFont.systemFont(ofSize: 14)
                    self.passwordText.delegate = self
                    self.passwordText.isSecureTextEntry = true
                    self.passwordText.layer.borderColor = self.activeColor.cgColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0).CGColor
                    self.idView.addSubview(passwordText)
                    
                    self.idPickerButton.isHidden = true
                    self.idDropDown.isHidden = true
                }
                
                self.idHorizontalLine1 = UIView(frame: CGRect(x: 0,y: y+stringHeight+15+44,width: self.idView.frame.size.width,height: 0.6))
                self.idHorizontalLine1.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
                self.idView.addSubview(idHorizontalLine1)
                
                self.idView.frame.size.height = self.idView.frame.size.height + 44
                
                y = self.idHorizontalLine1.frame.origin.y
                
//                self.idVerticalLine = UIView(frame: CGRectMake(self.idView.frame.size.width/2,y,0.75,44))
//                self.idVerticalLine.backgroundColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
//                self.idView.addSubview(idVerticalLine)

                self.verifyID = UIButton(type: UIButtonType.system)
                self.verifyID.frame = CGRect(x: 0, y: y, width: self.fbView.frame.size.width, height: 44)
                self.verifyID.setTitleColor(self.activeColor, for: UIControlState())
                self.verifyID.setTitle("Upload ID", for: UIControlState())
                self.verifyID.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                self.verifyID.titleLabel?.textAlignment = .center
                self.verifyID.addTarget(self, action: #selector(TMTrustBuilderViewController.idClick(sender:)), for: UIControlEvents.touchUpInside)
                self.idView.addSubview(self.verifyID)
                
//                self.rejectID = UIButton(type: UIButtonType.System)
//                self.rejectID.frame = CGRectMake(self.fbView.frame.size.width/2, y, self.fbView.frame.size.width/2, 44)
//                self.rejectID.setTitleColor(self.activeColor, forState: UIControlState.Normal)
//                self.rejectID.setTitle("Not Now", forState: UIControlState.Normal)
//                self.rejectID.titleLabel?.font = UIFont.systemFontOfSize(14)
//                self.rejectID.titleLabel?.textAlignment = .Center
//                self.rejectID.addTarget(self, action: "collapseID:", forControlEvents: UIControlEvents.TouchUpInside)
//                self.idView.addSubview(self.rejectID)
                
                if(self.trustBuilder!.idProofStatus == "rejected" && self.trustBuilder!.idRejectReason == "password_required") {
                
                    self.sendPassword = UIButton(type: UIButtonType.system)
                    self.sendPassword.frame = CGRect(x: 0, y: y, width: self.fbView.frame.size.width/2, height: 44)
                    self.sendPassword.setTitleColor(self.activeColor, for: UIControlState())
                    self.sendPassword.setTitle("Send Password", for: UIControlState())
                    self.sendPassword.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                    self.sendPassword.titleLabel?.textAlignment = .center
                    self.sendPassword.addTarget(self, action: #selector(TMTrustBuilderViewController.sendPasswordToServer), for: UIControlEvents.touchUpInside)
                    self.idView.addSubview(self.sendPassword)
                    
                    self.reUploadID = UIButton(type: UIButtonType.system)
                    self.reUploadID.frame = CGRect(x: self.fbView.frame.size.width/2, y: y, width: self.fbView.frame.size.width/2, height: 44)
                    self.reUploadID.setTitleColor(self.activeColor, for: UIControlState())
                    self.reUploadID.setTitle("Re Upload ID", for: UIControlState())
                    self.reUploadID.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                    self.reUploadID.titleLabel?.textAlignment = .center
                    self.reUploadID.addTarget(self, action: #selector(TMTrustBuilderViewController.reUploadIdToServer), for: UIControlEvents.touchUpInside)
                    self.idView.addSubview(self.reUploadID)
                    
                    self.verifyID.isHidden = true
                    //self.rejectID.hidden = true
                    
                }
                
                if(self.trustBuilder!.idProofStatus == "rejected" && (self.trustBuilder!.idRejectReason == "name_mismatch" || self.trustBuilder!.idRejectReason == "age_mismatch" || self.trustBuilder!.idRejectReason == "name_age_mismatch")) {
                    
                    self.verifyID.frame = CGRect(x: 0, y: y, width: self.fbView.frame.size.width/2, height: 44)
                    
                    self.syncID = UIButton(type: UIButtonType.system)
                    self.syncID.frame = CGRect(x: self.fbView.frame.size.width/2, y: y, width: self.fbView.frame.size.width/2, height: 44)
                    self.syncID.setTitleColor(self.activeColor, for: UIControlState())
                    self.syncID.setTitle("Sync With Photo ID", for: UIControlState())
                    self.syncID.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                    self.syncID.titleLabel?.textAlignment = .center
                    self.syncID.addTarget(self, action: #selector(TMTrustBuilderViewController.syncWithID), for: UIControlEvents.touchUpInside)
                    self.idView.addSubview(self.syncID)
                    //self.rejectID.hidden = true
                    
                }
            }
            
            self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
            
            self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
            
            //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
            
            if(self.continueButton != nil) {
                self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
            }else {
                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
            }
            
        }else {
            collapseID(sender: true as AnyObject)
        }

    }
    
    func collapseID(sender: AnyObject) {
     
        self.hidePicker(sender: true as AnyObject)
        
        self.idView.layer.borderColor = self.inactiveColor.cgColor
        self.idLabel.textColor = self.inactiveColor
        self.idTrustText.textColor = self.inactiveColor
        
        self.indexForId = 0
        self.proofType = ""
        self.picker.reloadAllComponents()
        self.idButton.isEnabled = true
        self.idIcon.image = UIImage(named: "PhotoID")!
        self.idMsg.removeFromSuperview()
        if(self.trustBuilder!.idProofStatus == "fail" || self.trustBuilder!.idProofStatus == "under_moderation") {
        
        }else {
            if(self.idHorizontalLine != nil) {
                self.idHorizontalLine.removeFromSuperview()
            }
            if(self.idPickerButton != nil) {
                self.idPickerButton.removeFromSuperview()
            }
            if(self.idDropDown != nil) {
                self.idDropDown.removeFromSuperview()
            }
            if(self.idHorizontalLine1 != nil) {
                self.idHorizontalLine1.removeFromSuperview()
            }
//            if(self.idVerticalLine != nil) {
//                self.idVerticalLine.removeFromSuperview()
//            }
            if(self.verifyID != nil) {
                self.verifyID.removeFromSuperview()
            }
//            if(self.rejectID != nil) {
//                self.rejectID.removeFromSuperview()
//            }
            if(self.passwordText != nil) {
                self.passwordText.removeFromSuperview()
            }
            if(self.sendPassword != nil) {
                self.sendPassword.removeFromSuperview()
            }
            if(self.reUploadID != nil) {
                self.reUploadID.removeFromSuperview()
            }
            if(self.syncID != nil) {
                self.syncID.removeFromSuperview()
            }
            //self.idText.removeFromSuperview()
        }
    
        self.idView.frame.size.height = 44
        
        self.idButton.frame.size.height = 44
        
        self.endorseView.frame.origin.y = self.idView.frame.origin.y + self.idView.frame.size.height + 10
        
        self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        if(self.continueButton != nil) {
            self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
        }else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
        }

        self.idMoved = false
        
    }
    
    
    func expandEndorsement(sender: UIButton) {
        
        self.endorseButton.isEnabled = true
        var textColor: UIColor!
        self.collapseView(key: "endorse")
        
        if(!self.endorseMoved) {
            self.endorseMoved = true
            
            self.endorseView.layer.borderColor = self.activeColor.cgColor
            self.endorseLabel.textColor = self.activeColor
            self.endorseTrustText.textColor = self.activeColor
            
            self.endorseIcon.image = UIImage(named: "Endorsements_active")!
            
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-52) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
            textDict["ktext"] = self.trustBuilder!.endorsementMsg as AnyObject?
            
            var stringFrame = TMStringUtil.textRect(forString: textDict)
            var stringWidth = stringFrame.size.width
            var stringHeight = stringFrame.size.height
            
            let count = self.trustBuilder!.endorseData.count
            
            if(count >= self.trustBuilder!.endorseCount) {
                self.endorseIcon.image = UIImage(named: "Endorsements_Highlight")
                textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
                // only photo
                self.endorseUserView = UIScrollView(frame: CGRect(x: 0, y: (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height), width: self.endorseButton.frame.size.width, height: 0))
                self.endorseUserView.frame.origin.y = (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height)
                //self.endorseUserView.frame.size.width = bounds.size.width
                self.endorseUserView.frame.size.height = 0
                self.endorseView.addSubview(self.endorseUserView)
                
                self.addEndorseData(from: "endorse")
                
                stringWidth = 0.0
                stringHeight = 0.0
                
                self.endorseMsg = UILabel(frame: CGRect(x: self.endorseIcon.frame.origin.x, y: (self.endorseUserView.frame.origin.y+self.endorseUserView.frame.size.height), width: 0, height: 0))
                
                self.endorseView.frame.size.height = self.endorseView.frame.size.height + 15 + self.endorseMsg.frame.size.height + self.endorseUserView.frame.size.height

            }else if(count < self.trustBuilder!.endorseCount && count > 0) {
                textColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
                
                self.endorseUserView = UIScrollView(frame: CGRect(x: 0, y: (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height), width: self.endorseButton.frame.size.width, height: 0))
                self.endorseUserView.frame.origin.y = (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height)
                //self.endorseUserView.frame.size.width = bounds.size.width
                self.endorseUserView.frame.size.height = 0
                self.endorseView.addSubview(self.endorseUserView)
                
                self.addEndorseData(from: "endorse")
                
                // photo and message
                let needEndorse = self.trustBuilder!.endorseCount-count
                textDict["ktext"] = ("You need " + String(needEndorse) + " or more endorsements to get 15% score") as AnyObject
                stringFrame = TMStringUtil.textRect(forString: textDict)
                stringWidth = stringFrame.size.width
                stringHeight = stringFrame.size.height
                
                self.endorseMsg = UILabel(frame: CGRect(x: self.endorseIcon.frame.origin.x, y: (self.endorseUserView.frame.origin.y+self.endorseUserView.frame.size.height), width: stringWidth, height: stringHeight))
                self.endorseMsg.textAlignment = NSTextAlignment.left
                self.endorseMsg.text = textDict["ktext"] as? String
                self.endorseMsg.textColor = textColor
                self.endorseMsg.font = UIFont.systemFont(ofSize: 14);
                self.endorseMsg.numberOfLines = 0
                self.endorseMsg.sizeToFit()
                self.endorseView.addSubview(self.endorseMsg)
                
                self.endorseView.frame.size.height = self.endorseView.frame.size.height + 15 + self.endorseMsg.frame.size.height + self.endorseUserView.frame.size.height
                //self.endorseButton.frame.size.height = 44
                
            }else {
                textColor = self.activeColor//UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
                
                self.endorseMsg = UILabel(frame: CGRect(x: self.endorseIcon.frame.origin.x, y: (self.endorseButton.frame.origin.y+self.endorseButton.frame.size.height), width: stringWidth, height: stringHeight))
                self.endorseMsg.textAlignment = NSTextAlignment.left
                self.endorseMsg.text = textDict["ktext"] as? String
                self.endorseMsg.textColor = textColor
                self.endorseMsg.font = UIFont.systemFont(ofSize: 14);
                self.endorseMsg.numberOfLines = 0
                self.endorseMsg.sizeToFit()
                self.endorseView.addSubview(self.endorseMsg)
                
                //self.endorseView.frame.size.height = self.endorseView.frame.size.height + 30 + self.endorseMsg.frame.size.height
                self.endorseView.frame.size.height = self.endorseView.frame.size.height + 15 + self.endorseMsg.frame.size.height
                self.endorseButton.frame.size.height = self.endorseView.frame.size.height
                
                
            }
            
            let y = self.endorseMsg.frame.origin.y
            
            self.endorseHorizontalLine = UIView(frame: CGRect(x: 0,y: y+stringHeight+15,width: self.endorseView.frame.size.width,height: 0.6))
            self.endorseHorizontalLine.backgroundColor = textColor as UIColor
            self.endorseView.addSubview(endorseHorizontalLine)
            
//            self.endorseVerticalLine = UIView(frame: CGRectMake(self.endorseView.frame.size.width/2,y+stringHeight+15,0.75,44))
//            self.endorseVerticalLine.backgroundColor = textColor
//            self.endorseView.addSubview(endorseVerticalLine)
            
            self.endorseView.frame.size.height = self.endorseView.frame.size.height + 44
            
            self.sendEndorse = UIButton(type: UIButtonType.system)
            self.sendEndorse.frame = CGRect(x: 0, y: y+stringHeight+15, width: self.endorseView.frame.size.width, height: 44)
            self.sendEndorse.setTitleColor(textColor, for: UIControlState())
            if(count > 0) {
                self.sendEndorse.setTitle("Request More", for: UIControlState())
            }else {
                self.sendEndorse.setTitle("Send Request", for: UIControlState())
            }
            self.sendEndorse.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            self.sendEndorse.titleLabel?.textAlignment = .center
            self.sendEndorse.addTarget(self, action: #selector(TMTrustBuilderViewController.idClick(sender:)), for: UIControlEvents.touchUpInside)
            self.endorseView.addSubview(self.sendEndorse)
            
//            self.rejectEndorse = UIButton(type: UIButtonType.System)
//            self.rejectEndorse.frame = CGRectMake(self.endorseView.frame.size.width/2, y+stringHeight+15, self.endorseView.frame.size.width/2, 44)
//            self.rejectEndorse.setTitleColor(textColor, forState: UIControlState.Normal)
//            self.rejectEndorse.setTitle("Not Now", forState: UIControlState.Normal)
//            self.rejectEndorse.titleLabel?.font = UIFont.systemFontOfSize(14)
//            self.rejectEndorse.titleLabel?.textAlignment = .Center
//            self.rejectEndorse.addTarget(self, action: "collapseEndorsement:", forControlEvents: UIControlEvents.TouchUpInside)
//            self.endorseView.addSubview(self.rejectEndorse)
        
            self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
            
            //self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
            
            if(self.continueButton != nil) {
                self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
            }else {
                self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
            }
            
        }else {
            self.collapseEndorsement(sender: true as AnyObject)
        }
    }
    
    func addEndorseData(from:String) {
        
        let count = self.trustBuilder!.endorseData.count
        let width = (self.bounds.width-32)/4
        var x:CGFloat = 10.0
        var textcolor: UIColor!
        var textDict:Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = width as AnyObject?
        textDict["kfont"] = UIFont.systemFont(ofSize: 12)
        var stringWidth: CGFloat = 0.0
        var stringHeight: CGFloat = 0.0
        
        if(from == "initialize") {
            self.endorseAddMore = UIButton(type: UIButtonType.system)
            self.endorseAddMore.frame = CGRect(x: x,y: 0,width: width,height: width)
            self.endorseAddMore.layer.cornerRadius = self.endorseAddMore.frame.size.width / 2
            self.endorseAddMore.setBackgroundImage(UIImage(named: "AddMore"), for: UIControlState())
            self.endorseAddMore.addTarget(self, action: #selector(TMTrustBuilderViewController.idClick(sender:)), for: UIControlEvents.touchUpInside)
            self.endorsedUser.addSubview(self.endorseAddMore)
            
            textDict["ktext"] = "Add More" as AnyObject?
            
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            stringWidth = stringFrame.size.width
            stringHeight = stringFrame.size.height
            
            self.endorseAddMoreText = UILabel(frame: CGRect(x: x+(self.endorseAddMore.frame.size.width-stringWidth)/2,y: self.endorseAddMore.frame.size.width+2.0,width: stringWidth,height: stringHeight))
            self.endorseAddMoreText.textAlignment = NSTextAlignment.center
            self.endorseAddMoreText.text = "Add More"
            self.endorseAddMoreText.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.endorseAddMoreText.font = UIFont.systemFont(ofSize: 12);
            self.endorsedUser.addSubview(self.endorseAddMoreText)
            
        }
        
        x = x + width + 10.0
        
        textcolor = self.activeColor
        
        for i in 0 ..< count {
            
            let tmp = self.trustBuilder!.endorseData[i] as! NSDictionary
            
            textDict["ktext"] = tmp["fname"]! as AnyObject?
            
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            stringWidth = stringFrame.size.width
            stringHeight = stringFrame.size.height
        
            
            let url = URL(string: tmp["pic"] as! String)!
            let imageView = UIImageView(frame: CGRect(x: x,y: 0,width: width,height: width))
            imageView.layer.borderWidth=1.0
            imageView.layer.borderColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0).cgColor
            imageView.layer.cornerRadius = 13
            imageView.layer.cornerRadius = imageView.frame.size.width/2
            imageView.clipsToBounds = true
            imageView.setImageWith(url)
            
            if(from == "initialize") {
                self.endorsedUser.addSubview(imageView)
            } else {
                self.endorseUserView.addSubview(imageView)
            }
            
            let nameLabel = UILabel(frame: CGRect(x: x+(width-stringWidth)/2,y: imageView.frame.size.height+2.0,width: stringWidth,height: stringHeight))
            nameLabel.textAlignment = NSTextAlignment.center
            nameLabel.text = tmp["fname"] as? String
            nameLabel.textColor = textcolor
            nameLabel.font = UIFont.systemFont(ofSize: 12);
            
            if(from == "initialize") {
                self.endorsedUser.addSubview(nameLabel)
            } else {
                self.endorseUserView.addSubview(nameLabel)
            }
            
            x = x + width + 10
            
        }
        if(from == "initialize") {
            self.endorsedUser.showsHorizontalScrollIndicator = false
            self.endorsedUser.frame.size.height = width + 20
            self.endorsedUser.contentSize = CGSize(width: x , height:self.endorsedUser.frame.size.height)
        } else {
            self.endorseUserView.showsHorizontalScrollIndicator = false
            self.endorseUserView.frame.size.height = width + 20
            self.endorseUserView.contentSize = CGSize(width: x , height:self.endorseUserView.frame.size.height)
        }
        
    }
    
    func collapseEndorsement(sender: AnyObject) {
        
        self.endorseView.layer.borderColor = self.inactiveColor.cgColor
        self.endorseLabel.textColor = self.inactiveColor
        self.endorseTrustText.textColor = self.inactiveColor
        
        
        if(self.endorseUserView != nil) {
            self.endorseUserView.removeFromSuperview()
        }
        self.endorseIcon.image = UIImage(named: "Endorsements")!
        if(self.trustBuilder!.endorseData.count >= self.trustBuilder!.endorseCount) {
            self.endorseIcon.image = UIImage(named: "Endorsements_Highlight")
        }
        self.endorseMsg.removeFromSuperview()
        self.endorseHorizontalLine.removeFromSuperview()
        //self.endorseVerticalLine.removeFromSuperview()
        self.sendEndorse.removeFromSuperview()
        //self.rejectEndorse.removeFromSuperview()
        
        self.endorseView.frame.size.height = 44
        
        self.endorseButton.frame.size.height = 44
        
        self.endorsedUser.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
       // self.endorseAddMoreView.frame.origin.y = self.endorseView.frame.origin.y + self.endorseView.frame.size.height + 10
        
        if(self.continueButton != nil) {
            self.continueButton.frame.origin.y = self.endorsedUser.frame.origin.y + self.endorsedUser.frame.size.height + 10
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.continueButton.frame.origin.y+self.continueButton.frame.size.height+15.0)
        }else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:self.endorsedUser.frame.origin.y+self.endorsedUser.frame.size.height+15.0+self.extraH)
        }
        
        self.endorseMoved = false

    }
    
    func sendPasswordToServer() {
        
        //self.disableButton("id")
        if(self.passwordText.text == "") {
            self.showAlert(msg: "", title: "Enter your password first")
            return
        }
        
        var queryString = ["type":"1"]
        queryString["action"] = "sendpassword"
        queryString["password"] = self.passwordText.text
        
        if(self.trustMgr.isNetworkReachable()) {
            self.view.isUserInteractionEnabled = false
            self.addIndicator()
            self.activityIndicator.titleText = "Sending..."
            self.activityIndicator.startAnimating()

            self.trustMgr.sendIDPassword(queryString, with: { (status:String?, err:TMError?) -> Void in
                self.view.isUserInteractionEnabled = true
              //  self.enableButton()
                self.removeLoader()
                if(status != nil) {
                    if(status == "success") {
                        self.trustBuilder!.idError = false
                        self.trustBuilder!.idProofStatus = "under_moderation"
                        self.trustBuilder!.idMsg = "Thanks! We will revert within 48 hours after reviewing your document." //"You've succeesfully uploaded your \(self.trustBuilder!.idType!). We'll verify it and get back to you in 48 hours."
                        self.updateIDView()
                        
                        //update the cache
                       // self.loadTrustData()
                    }else {
                        self.errorResponse(err: err!)
                    }
                }else {
                    self.errorResponse(err: err!)
                }
                
            })
        }else {
            self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }
    }
    
    func syncWithID() {
        
      //  self.disableButton("id")
        
        var queryString = ["type":"1"]
        queryString["action"] = "syncNamewithID"
        queryString["update"] = self.trustBuilder!.idRejectReason
        
        if(self.trustMgr.isNetworkReachable()) {
            self.view.isUserInteractionEnabled = false
            
            self.addIndicator()
            self.activityIndicator.titleText = "Syncing...."
            self.activityIndicator.startAnimating()
            
            self.trustMgr.sendIDPassword(queryString, with: { (status:String?, err:TMError?) -> Void in
                self.view.isUserInteractionEnabled = true
         //       self.enableButton()
                self.removeLoader()
                
                if(status != nil) {
                    if(status == "success") {
                        self.trustBuilder!.trustScore = self.trustBuilder!.trustScore + self.trustBuilder!.idScore!
                        self.trustBuilder!.idProofStatus = "active"
                        self.collapseID(sender: true as AnyObject)
                        self.trustBuilder!.isIdVerified = true
                        var textDict:Dictionary<String,AnyObject> = [:]
                        textDict["kmaxwidth"] = (self.bounds.size.width-32) as AnyObject
                        textDict["kfont"] = UIFont.systemFont(ofSize: 14)
                        textDict["ktext"] = self.trustBuilder!.idType as AnyObject?
                        let stringFrame = TMStringUtil.textRect(forString: textDict)
                        let stringWidth = stringFrame.size.width
                        
                        self.idButton.isEnabled = false
                        self.idButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
                        self.idLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
                        self.idLabel.frame.size.width = stringWidth
                        self.idLabel.text = self.trustBuilder!.idType
                        self.idIcon.image = UIImage(named: "PhotoID_Highlight_Trust")
                        self.idTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
                        
                        self.setTrustScore(trustScore: self.trustBuilder!.trustScore)
                        
                        if(self.delegate != nil) {
                            self.delegate!.didEditProfile!()
                        }
                    }else {
                        self.errorResponse(err: err!)
                    }
                }else {
                    self.errorResponse(err: err!)
                }
            })
        }else {
            self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }
    }
    
    func reUploadIdToServer() {
        
        self.passwordText.removeFromSuperview()
        self.sendPassword.removeFromSuperview()
        self.reUploadID.removeFromSuperview()
        
        self.verifyID.isHidden = false
        //self.rejectID.hidden = false
        self.idPickerButton.isHidden = false
    
    }
    
    
    func idPickerButtonClick(sender: UIButton) {
        self.view.endEditing(true)
        self.picker.isHidden = false
        self.pickerCancel.isHidden = false
        self.pickerSeparator.isHidden = false
        self.pickerDone.isHidden = false
        
    }
    func hidePicker(sender: AnyObject) {
        if(self.picker != nil) {
            self.picker.isHidden = true
            self.pickerCancel.isHidden = true
            self.pickerSeparator.isHidden = true
            self.pickerDone.isHidden = true
        }
    }
    
    func updateFacebook(){
        var textDict:Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = (bounds.size.width-52) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 14)

        if(self.trustBuilder!.isFbVerified){
            textDict["ktext"] = "\(self.trustBuilder!.fbConnection!) Connections" as AnyObject?
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            
            self.fbView.layer.borderWidth = 0.0
            self.collapseFB(sender: true as AnyObject)
            self.fbButton.isEnabled = false
            self.fbLabel.frame.size.width = stringWidth
            self.fbButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.fbLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.fbLabel.text = "\(self.trustBuilder!.fbConnection!) Connections"
            self.fbIcon.image = UIImage(named: "facebook_highlight_trust")
            self.fbTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            
            self.setTrustScore(trustScore: self.trustBuilder!.trustScore)
            
            //if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_EDITPROFILE) {
                if(self.delegate != nil) {
                    self.delegate!.didEditProfile!()
                }
                //self.delegate.didEditProfile!()
            //}
            
        }else if(self.trustBuilder?.fbMismatchReason != nil){
            self.collapseFB(sender: true as AnyObject)
            self.expandfacebook(sender: true as AnyObject)
            self.showAlertForFacebook(msg: self.trustBuilder!.fbMsg)
            
        }else{
            self.collapseFB(sender: true as AnyObject)
            self.expandfacebook(sender: true as AnyObject)
            self.showAlert(msg: self.trustBuilder!.fbMsg, title:"")
        }
    }
    
    
    
    func onFacebookClick(sender: AnyObject) {
        eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
        eventDict["eventCategory"] = self.eventCategory as AnyObject?
        eventDict["eventAction"] = "fb_click" as AnyObject?
        TMAnalytics.sharedInstance().trackNetworkEvent(eventDict)
        
        FBSession.active().closeAndClearTokenInformation()
        self.appDelegate.openActiveSession(withPermissions: true , from: "trust")
    }
    
    func onLinkedinClick(sender: AnyObject) {
        self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
        self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
        self.eventDict["eventAction"] = "linkedin_click" as AnyObject?
        TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
        
        Lclient.getAuthorizationCode({ (code:String?) -> Void in
            let verifyLinkedin = TMLinkedinManager()
            let linkedinDict = ["auth_code":code!]
            
            self.view.isUserInteractionEnabled = false
            self.addIndicator()
            self.activityIndicator.titleText = "Verifying...."
            self.activityIndicator.startAnimating()
            
            self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
            self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
            self.eventDict["eventAction"] = "linkedin_server_call" as AnyObject?
            
            verifyLinkedin.trackEventDictionary = NSMutableDictionary(dictionary: self.eventDict)
            verifyLinkedin.sendAuthorizeCode(linkedinDict, with: { (flag: Bool, connections:String?, error: TMError?) -> Void in
                self.view.isUserInteractionEnabled = true
                self.removeLoader()
                self.changeContinueText()
                if(flag){
                    if(self.trustBuilder != nil) {
                        self.trustBuilder?.isLinkedinVerified = true
                        self.trustBuilder!.trustScore = self.trustBuilder!.trustScore + self.trustBuilder!.linkedinScore!
                        self.trustBuilder?.linkedinDesignation = connections
                        self.updateLinkedin()
                        if(self.delegate != nil) {
                            self.delegate!.didEditProfile!()
                        }
                    }
                }else {
                    if(error?.errorCode == .TMERRORCODE_LOGOUT) {
                        if(self.actionDelegate !=  nil) {
                            self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
                        }
                    }else if(error?.errorCode == .TMERRORCODE_NONETWORK) {
                        self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
                    }else if(error?.errorCode == .TMERRORCODE_SERVERTIMEOUT) {
                        self.showAlert(msg: TM_SERVER_TIMEOUT_MSG, title: "")
                    }
                    else if(error?.errorCode == .TMERRORCODE_DATASAVEERROR) {
                        self.trustBuilder?.linkedinError = true
                        self.trustBuilder?.isLinkedinVerified = false
                        self.trustBuilder?.linkedinMsg = (error?.errorMessage)! as String
                        self.updateLinkedin()
                    }else {
                        self.showAlert(msg: "Please Try Later", title: "Oops! something went wrong.")
                    }
                }
            })
        },
            cancel: { () -> Void in
                self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
                self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
                self.eventDict["eventAction"] = "linkedin_popup_cancel" as AnyObject?
                TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
                
                self.view.isUserInteractionEnabled = true
            },
            failure: { (error:Error?) -> Void in
                self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
                self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
                self.eventDict["eventAction"] = "linkedin_error" as AnyObject?
                TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
                
                self.view.isUserInteractionEnabled = true
                self.showAlert(msg: (error?.localizedDescription)!, title: "")
        })
        
    }

    func collapseView(key: String) {
        if(key == "fb") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
        }
        else if(key == "linkedin") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
        }
        else if(key == "phone") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
        }
        else if(key == "id") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
        }
        else if(key == "endorse") {
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
        }

    }
    
    func disableButton(key: String) {
        if(key == "fb") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
            self.linkedinButton.isEnabled = false
            self.phoneButton.isEnabled = false
            self.idButton.isEnabled = false
            self.endorseButton.isEnabled =  false
        }
        else if(key == "linkedin") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
            self.fbButton.isEnabled = false
            self.phoneButton.isEnabled = false
            self.idButton.isEnabled = false
            self.endorseButton.isEnabled =  false
        }
        else if(key == "phone") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
            self.linkedinButton.isEnabled = false
            self.fbButton.isEnabled = false
            self.idButton.isEnabled = false
            self.endorseButton.isEnabled =  false
        }
        else if(key == "id") {
            if(self.endorseMoved) {
                self.collapseEndorsement(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            self.linkedinButton.isEnabled = false
            self.phoneButton.isEnabled = false
            self.fbButton.isEnabled = false
            self.endorseButton.isEnabled =  false
        }
        else if(key == "endorse") {
            if(self.phoneMoved) {
                self.collapsePhone(sender: true as AnyObject)
            }
            if(self.fbMoved) {
                self.collapseFB(sender: true as AnyObject)
            }
            if(self.linkedinMoved) {
                self.collapseLinkedin(sender: true as AnyObject)
            }
            if(self.idMoved) {
                self.collapseID(sender: true as AnyObject)
            }
            self.linkedinButton.isEnabled = false
            self.phoneButton.isEnabled = false
            self.idButton.isEnabled = false
            self.fbButton.isEnabled =  false
        }
    }
    
    func enableButton() {
        if(!self.trustBuilder!.isFbVerified) {
            self.fbButton.isEnabled = true
        }
        if(!self.trustBuilder!.isLinkedinVerified) {
            self.linkedinButton.isEnabled = true
        }
        if(!self.trustBuilder!.isPhoneVerified) {
            self.phoneButton.isEnabled = true
        }
        if(!self.trustBuilder!.isIdVerified) {
            self.idButton.isEnabled = true
        }
        if(self.trustBuilder!.endorseData.count>0) {
            self.endorseButton.isEnabled = false
        }else {
            self.endorseButton.isEnabled = true
        }
        
    }
    
    func executeHandle(notification:Notification) {
        var dictionary = (notification as NSNotification).userInfo as! [String : String]
        let from: String! = dictionary["from"]! as String
        
        if(FBSession.active().state == .open && from == "trust"){
            
            let token:NSString = FBSession.active().accessTokenData.accessToken as NSString
          
            var queryString = ["connected_from":self.connectedFrom]
            queryString["token"] = token as String
            queryString["checkFacebook"] = "check"
            if(self.trustBuilder?.fbMismatchReason != nil){
                queryString["reimport"] = "reimport"
            }

            eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
            eventDict["eventCategory"] = self.eventCategory as AnyObject?
            eventDict["eventAction"] = "fb_server_call" as AnyObject?
            
            self.trustMgr.trackEventDictionary = NSMutableDictionary(dictionary: eventDict)
            
            if(self.trustMgr.isNetworkReachable()) {
                self.view.isUserInteractionEnabled = false
                self.addIndicator()
                self.activityIndicator.titleText = "Verifying..."
                if(self.trustBuilder?.fbMismatchReason != nil){
                    self.activityIndicator.titleText = "Syncing..."
                }
                self.activityIndicator.startAnimating()
                
                self.trustMgr.verifyFacebook(queryString, with: { (response:[AnyHashable:Any]?, err:TMError?) -> Void in
                    self.view.isUserInteractionEnabled = true
                //    self.enableButton()
                    self.removeLoader()
                    if response != nil {
                        self.trustBuilder?.updateFacebookBlock(response as NSDictionary!)
                        self.updateFacebook()
                        self.changeContinueText()
                        //cache the updated response
                       // self.loadTrustData()
                    }else {
                        self.errorResponse(err: err!)
                    }
                    
                })
            }else {
                self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
            }
           
        }
    }

    
    func client()->LIALinkedInHttpClient!{
    
        let application:LIALinkedInApplication! = LIALinkedInApplication.application(withRedirectURL: "http://www.trulymadly.com",
            clientId:LI_API_KEY as String,
            clientSecret:LI_SECRET_KEY as String,
            state:"DCEEFWF45453sdffef424",
            grantedAccess:["r_basicprofile","r_emailaddress"]) as! LIALinkedInApplication
     
        return LIALinkedInHttpClient(for:application,presentingViewController:self)
    }
    
    private func updateLinkedin() {
        
        if(self.trustBuilder!.isLinkedinVerified) {
            self.linkedinView.layer.borderWidth = 0.0
            self.linkedinButton.isEnabled = false
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
            textDict["ktext"] = self.trustBuilder!.linkedinDesignation! as AnyObject?
            let stringFrame = TMStringUtil.textRect(forString: textDict)
            let stringWidth = stringFrame.size.width
            self.linkedinLabel.frame.size.width = stringWidth
            
            self.collapseLinkedin(sender: true as AnyObject)
            self.linkedinLabel.frame.size.width = stringWidth
            self.linkedinButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.linkedinLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.linkedinLabel.text = self.trustBuilder!.linkedinDesignation!
            self.linkedinIcon.image = UIImage(named: "link_highlight_trust")
            self.linkedinTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            
            self.setTrustScore(trustScore: self.trustBuilder!.trustScore)
            
        }else {
            self.collapseLinkedin(sender: true as AnyObject)
            self.expandLinkedin(sender: true as AnyObject)
            self.showAlert(msg: self.trustBuilder!.linkedinMsg, title:"")
            
        }
    }
    
    func verifyPhone(sender: AnyObject) {
        
        //MoEngage Event
        MoEngage.sharedInstance().trackEvent("Phone Verification", andPayload: nil)
        
        if(self.phoneText.text?.characters.count != 10){
            self.showAlert(msg: "", title: "Enter your phone number first")
            return
        }
        
      //  self.disableButton("phone")
        
        self.textFieldShouldReturn(self.phoneText)

        var postdata = ["action":"numberVerify"]
        postdata["number"] = self.phoneText.text
        
        //var phoneNumber = self.phoneText.text //as! String
        
        self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
        self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
        self.eventDict["eventAction"] = "phone_click" as AnyObject?
        TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
        
        self.eventDict["eventAction"] = "phone_server_call" as AnyObject?
        self.trustMgr.trackEventDictionary = NSMutableDictionary(dictionary: self.eventDict)
        
        if(self.trustMgr.isNetworkReachable()) {
            self.view.isUserInteractionEnabled = false
            self.addIndicator()
            self.activityIndicator.titleText = "Call Verifying..."
            self.activityIndicator.startAnimating()
            
            self.trustMgr.submitPhone(postdata, with: { (response:String?, err:TMError?) -> Void in
                
                if(response != nil) {
                    if response == "yes" {
                        self.timer = Timer.scheduledTimer(timeInterval: 10.0,target: self,selector: #selector(TMTrustBuilderViewController.getPhoneStatus(timer:)),userInfo: nil,repeats: true)
                    }else {
                        self.view.isUserInteractionEnabled = true
                        self.removeLoader()
                        
                        self.trustBuilder!.phoneError = true
                        self.trustBuilder!.isPhoneVerified = false
                        self.trustBuilder!.phoneRejectedMsg = response!
                        self.collapsePhone(sender: true as AnyObject)
                        self.expandPhone(sender: true as AnyObject)
                        
                        self.showAlert(msg: self.trustBuilder!.phoneRejectedMsg, title:"")
                        
                    }
                }else {
                    self.view.isUserInteractionEnabled = true
                    self.removeLoader()
                    
                    self.errorResponse(err: err!)
                }
            })
        }else {
            self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }
    }

    
    func getPhoneStatus(timer:Timer){
        
        let queryString = ["action":"checkNumber"]
        
            if(self.phonePollTimer < 100) {
                self.phonePollTimer = self.phonePollTimer + 10
                self.trustMgr.phoneStatus(queryString, with: { (response:[AnyHashable:Any]?, err:TMError?) -> Void in
                    
                    if response != nil {
                        let responseCode = response?["responseCode"] as! Int
                        if(responseCode == 200) {
                            let status = response?["status"] as! String
                            if(status == "active"){
                                self.view.isUserInteractionEnabled = true
                        //        self.enableButton()
                                self.removeLoader()
                                
                                self.trustBuilder!.trustScore = self.trustBuilder!.trustScore + self.trustBuilder!.mobileScore
                                self.trustBuilder!.isPhoneVerified = true
                                self.trustBuilder!.phoneNumber = response?["user_number"] as? String
                                self.updatePhone()
                                
                                //if(self.navigationFlow == TMNavigationFlow.TMNAVIGATIONFLOW_EDITPROFILE) {
                                    if(self.delegate != nil) {
                                        self.delegate!.didEditProfile!()
                                        //update the cache
                                       // self.loadTrustData()
                                    }
                                    //self.delegate.didEditProfile!()
                                //}
                                
                                self.timer.invalidate()
                            }
                        }else if(responseCode == 403) {
                            self.view.isUserInteractionEnabled = true
                            self.removeLoader()
                            
                            //var status = response["status"] as! String
                            var numTrials = 0;
                            if( ((response?["number_of_trials"] as AnyObject).isValidObject() == true) && (response?["number_of_trials"] != nil) ) {
                                let trials = response?["number_of_trials"] as! String
                                numTrials = Int(trials)!
                            }
                            self.trustBuilder!.number_of_trials = numTrials
                            self.trustBuilder!.isPhoneVerified = false
                            self.trustBuilder!.phoneRejectedMsg = response?["error"] as! String
                            self.updatePhone()
                            self.timer.invalidate()
                        }
                    }else {
                        self.view.isUserInteractionEnabled = true
                        self.removeLoader()
                        
                        self.errorResponse(err: err!)
                    }
                })
            }else {
                self.timer.invalidate()
            }
        
    }

    func updatePhone(){
        self.changeContinueText()
        if(self.trustBuilder!.isPhoneVerified){
            self.phoneView.layer.borderWidth = 0.0
            //Commented below line for digits implementation
            //self.collapsePhone(sender: true as AnyObject)
            var textDict:Dictionary<String,AnyObject> = [:]
            textDict["kmaxwidth"] = (bounds.size.width-32) as AnyObject
            textDict["kfont"] = UIFont.systemFont(ofSize: 14)
            textDict["ktext"] = self.trustBuilder!.phoneNumber! as AnyObject?
            
            self.phoneButton.isEnabled = false
            self.phoneButton.backgroundColor = UIColor(red: 0.341, green: 0.557, blue: 0.796, alpha: 1.0)
            self.phoneLabel.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            self.phoneLabel.text = self.trustBuilder!.phoneNumber!
            self.phoneIcon.image = UIImage(named: "Phone_Highlight_Trust")
            self.phoneTrustText.textColor = UIColor(red: 1.0, green:1.0, blue:1.0, alpha:1.0)
            
            self.setTrustScore(trustScore: self.trustBuilder!.trustScore)
            
        }else{
            //Commented below two lines for digits implementation
            //self.collapsePhone(sender: true as AnyObject)
            //self.expandPhone(sender: true as AnyObject)
            self.showAlert(msg: self.trustBuilder!.phoneRejectedMsg, title:"")
        }
    }
    
    func onContinue(sender: UIButton) {
        if(sender.titleLabel!.text == "Skip") {
            self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
            self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
            self.eventDict["eventAction"] = "skip" as AnyObject?
            TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
        }
        self.delegate!.popToRootViewControllerAndMoveToViewController!(withId: "photovc", animated: true)
    }

    func idClick(sender: AnyObject) {
        
        if(self.verifyID != nil) {
            if(self.proofType == "" && sender as! NSObject == self.verifyID) {
                self.showAlert(msg: "Please select document type first",title: "")
                return
            }
        }
        
        let sheet: UIActionSheet = UIActionSheet()
        sheet.delegate = self;
        sheet.addButton(withTitle: "Cancel")
        if(self.verifyID != nil && sender as! NSObject == self.verifyID) {
            self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
            self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
            self.eventDict["eventAction"] = "id_click" as AnyObject?
            sheet.tag = 1
            sheet.addButton(withTitle: "Add from device")
            sheet.addButton(withTitle: "Take from camera")
        }else if((self.sendEndorse != nil && sender as! NSObject == self.sendEndorse) || sender as! NSObject == self.endorseAddMore) {
            self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
            self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
            self.eventDict["eventAction"] = "endorsement_click" as AnyObject?
            sheet.tag = 2
            sheet.addButton(withTitle: "Facebook")
            sheet.addButton(withTitle: "Others")
            //MoEngage Event
            MoEngage.sharedInstance().trackEvent("Endorsement Click", andPayload: nil)
        }
        TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
        
        sheet.cancelButtonIndex = 0;
        
        sheet.show(in: self.view);
        
    }
    
    func actionSheet(_ sheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        //println("index %d %@", buttonIndex, sheet.buttonTitleAtIndex(buttonIndex));
        
        if(buttonIndex == 1 && sheet.tag == 1) {
            self.addfromlibrary()
        }
        else if(buttonIndex == 2 && sheet.tag == 1) {
            self.addFromCamera()
        }else if(buttonIndex == 1 && sheet.tag == 2) {
            self.shareViaMessenger()
        }else if(buttonIndex == 2 && sheet.tag == 2) {
            self.shareViaOthers()
        }
        
    }
    
    func shareViaMessenger() {
        
        // Check if the Facebook app is installed and we can present the message dialog
        let url = self.trustBuilder!.endorseLink
        let params: FBLinkShareParams = FBLinkShareParams()
        params.link = URL(string: url)
        //params.name = "Your friend has picked you to endorse him on TrulyMadly. Click here to endorse:"
        params.name = "Hey, please back me up on TrulyMadly! I’d do the same for you! ;) "//"Hey! Recommend me on TrulyMadly.The girls will trust me more :) "
        
//        if(self.gender == "F") {
//            params.name = "Hey! Recommend me on TrulyMadly.Help me build my trust score :) "
//        }
        params.caption = "TrulyMadly"
        params.picture = URL(string: "http://dev.trulymadly.com/trulymadly/images/TMSeal.png")
        params.linkDescription = "Trulymadly"
        
        // If the Facebook app is installed and we can present the share dialog
        if(FBDialogs.canPresentMessageDialog(with: params)) {
            // Present message dialog
//            FBDialogs.presentMessageDialog(params,clientState: nil, handler: {(call: FBAppCall!, results: [NSObject:AnyObject]!, error: NSError!) -> Void in
//                    if(error != nil) {
//                        //println(error.description)
//                        self.showAlert(error.localizedDescription, title: "")
//                    }else {
//                        let str = results["completionGesture"] as? String
//                        if(str == "message") {
//                            self.changeContinueText()
//                            self.showAlert("Request Sent", title: "")
//                            return
//                        }
//                    }
//            })
            
        } else {
            self.showAlert(msg: "There was an error connecting to FB Messenger. Please make sure that it is installed.", title: "")
        }
        
    }

    func shareViaOthers(){
        let url = self.trustBuilder!.endorseLink
        let textToShare = "Hey, please back me up on TrulyMadly! I’d do the same for you! ;) "//"Hey! Recommend me on TrulyMadly.The girls will trust me more :) "
//        if(self.gender == "F") {
//            textToShare = "Hey! Recommend me on TrulyMadly.Help me build my trust score :) "
//        }
        let link: URL = URL(string: url)!
        let objectsToShare = [textToShare,link] as [Any]
//        let whatsapp: TMCustomActivity = TMCustomActivity(text: textToShare, link: url)
        let activityVC = UIActivityViewController(activityItems: objectsToShare as [AnyObject], applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList, UIActivityType.copyToPasteboard,
            UIActivityType.assignToContact, UIActivityType.saveToCameraRoll]
        activityVC.setValue("Guess who’s on TrulyMadly? ;)", forKey: "Subject")
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func addfromlibrary() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            let controller:UIImagePickerController = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.photoLibrary
            controller.allowsEditing = false
            self.present(controller, animated: false, completion: nil)
        }
    }
    
    func addFromCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            
            let controller:UIImagePickerController = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.camera
            controller.allowsEditing = false
            controller.cameraCaptureMode = .photo
            
            self.present(controller, animated: false, completion: nil);
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let profile_image: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageData:Data = UIImageJPEGRepresentation(profile_image, 0.4)!;
        picker.dismiss(animated: true, completion: nil)
        
        var params = ["type":"1"]
        params["proofType"] = self.proofType
        params["fileSubmit"] = "submit"
        //println(imageData.length)
        //size = 5242880  5 MB
        
        if(imageData.count>5242880) {
            self.showAlert(msg: "File size cannot be larger than 5 MB", title: "")
            return
        }
        
        //  self.disableButton("id")
        
        self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
        self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
        self.eventDict["eventAction"] = "id_server_call" as AnyObject?
        self.trustMgr.trackEventDictionary = NSMutableDictionary(dictionary: self.eventDict)
        
        if(self.trustMgr.isNetworkReachable()) {
            self.view.isUserInteractionEnabled = false
            self.addIndicator()
            self.activityIndicator.titleText = "Uploading...."
            self.activityIndicator.startAnimating()
            
            self.trustMgr.uploadDocument(imageData, params: params, with: { (status:String?, err:TMError?) -> Void in
                //    self.enableButton()
                self.view.isUserInteractionEnabled = true
                // self.activityIndicator.stopAnimating()
                self.removeLoader()
                if (status != nil) {
                    if(status == "success"){
                        self.trustBuilder!.idError = false
                        self.trustBuilder!.idProofStatus = "under_moderation"
                        self.trustBuilder!.idMsg = "You've successfully uploaded your \(self.proofType). We'll verify it and get back to you in 48 hours."
                        self.updateIDView()
                        self.changeContinueText()
                        //update the cache
                      //  self.loadTrustData()
                    }
                }else {
                    self.errorResponse(err: err!)
                }
            })
            
        }else {
            self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
    }
    
    
    func updateIDView() {
        self.collapseID(sender: true as AnyObject)
        self.expandID(sender: true as AnyObject)
        self.idHorizontalLine.removeFromSuperview()
        self.idPickerButton.removeFromSuperview()
        self.idDropDown.removeFromSuperview()
        self.idHorizontalLine1.removeFromSuperview()
        //self.idVerticalLine.removeFromSuperview()
        self.verifyID.removeFromSuperview()
        //self.rejectID.removeFromSuperview()
        
        if(self.passwordText != nil) {
            self.passwordText.removeFromSuperview()
        }
        if(self.sendPassword != nil) {
            self.sendPassword.removeFromSuperview()
        }
        if(self.reUploadID != nil) {
            self.reUploadID.removeFromSuperview()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
    
    
    //MARK: - Keyboard Management Methods
    
    // Call this method somewhere in your view controller setup code.
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(TMTrustBuilderViewController.keyboardWillBeShown(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(TMTrustBuilderViewController.keyboardWillBeHidden(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // Called when the UIKeyboardDidShowNotification is sent.
    func keyboardWillBeShown(sender: Notification) {
        self.hidePicker(sender: true as AnyObject)
        let info: NSDictionary = (sender as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardSize: CGSize = value.cgRectValue.size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
    
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        
        if let activeTextFieldRect: CGRect? = self.idView.frame {//activeTextField?.bounds {
            let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
            if (!aRect.contains(activeTextFieldOrigin!)) {
                scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
            }
        }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden(sender: Notification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    //MARK: - UITextField Delegate Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        // scrollView.scrollEnabled = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
        // scrollView.scrollEnabled = false
    }

    
    
    //UIPickerViewDataSource method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView,numberOfRowsInComponent component: Int) -> Int{
        return self.IDDocument.count
    }
    
    func pickerView(_ pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String?{
        return self.IDDocument[row] as String
    }
    
    func pickerView(_ pickerView: UIPickerView,didSelectRow row: Int,inComponent component: Int){
        self.indexForId = row
    }
    
    func updateIDType(row: Int) {
        self.proofType = self.IDDocument[row]
        self.idPickerButton.setTitle(self.proofType as String, for:UIControlState())
        self.picker.isHidden = true
        self.pickerSeparator.isHidden = true
    }
    
    func donePicker(sender: UIButton) {
        self.updateIDType(row: self.indexForId)
    }
    
    func showAlertForFacebook(msg: String) {
        let title = "Cancel"
        let alert = UIAlertView(title:"" , message:msg , delegate: nil, cancelButtonTitle: title)
       // alert.addButtonWithTitle("Sync With Facebook")
        
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    func showAlert(msg: String, title: String) {
        let alert = UIAlertView(title: title , message:msg , delegate: nil, cancelButtonTitle: "OK")
        
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        let title: NSString = alertView.buttonTitle(at: buttonIndex)! as NSString
        if(title == "Sync With Facebook") {
            self.verifyFb.setTitle(title as String, for: UIControlState())
            self.onFacebookClick(sender: true as AnyObject)
        }
    }
    
    func setTrustScore(trustScore: Int) {
        
        var textDict:Dictionary<String,AnyObject> = [:]
        textDict["kmaxwidth"] = (self.bounds.size.width-32) as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 20)
        textDict["ktext"] = "\(trustScore)%" as AnyObject?
        
        var stringFrame = TMStringUtil.textRect(forString: textDict)
        var stringWidth = stringFrame.size.width
        var stringHeight = stringFrame.size.height
        
        if(self.scoreLabel == nil) {
            
            self.scoreLabel = UILabel(frame: CGRect(x: (self.trustScoreView.frame.origin.x+(self.trustScoreView.frame.size.width-stringWidth)/2), y: (self.trustScoreView.frame.origin.y+(self.trustScoreView.frame.size.height-stringHeight)/2), width: stringWidth, height: stringHeight))
            self.scoreLabel.textAlignment = NSTextAlignment.center
            self.scoreLabel.text = "\(trustScore)%"
            self.scoreLabel.font = UIFont.systemFont(ofSize: 20);
            self.scoreLabel.numberOfLines = 0
            self.scoreLabel.sizeToFit()
            self.scrollView.addSubview(self.scoreLabel)
            
        }else {
            
            textDict["kfont"] = UIFont.systemFont(ofSize: 20)
            textDict["ktext"] = "\(trustScore)%" as AnyObject?
            
            stringFrame = TMStringUtil.textRect(forString: textDict)
            stringWidth = stringFrame.size.width
            stringHeight = stringFrame.size.height
            
            self.scoreLabel.text = "\(trustScore)%"
            self.scoreLabel.frame.origin.x = self.trustScoreView.frame.origin.x+(self.trustScoreView.frame.size.width-stringWidth)/2
            self.scoreLabel.frame.origin.y = self.trustScoreView.frame.origin.y+(self.trustScoreView.frame.size.height-stringHeight)/2-10
            self.scoreLabel.frame.size.width = stringWidth
            self.scoreLabel.frame.size.height = stringHeight
            
        }

        
        if(trustScore >= 100) {
            let trustImage = UIImage(named: imageName[1])!
            self.trustScoreView.image = trustImage
            
            
            if(self.trustMsg != nil) {
                self.trustMsg.removeFromSuperview()
            }
            if(self.trustText != nil) {
                self.trustText.removeFromSuperview()
            }

        }else {
            if(trustScore >= self.trustBuilder!.thresold) {
                textDict["kmaxwidth"] = self.trustScoreView.frame.size.width as AnyObject?
                textDict["kfont"] = UIFont.systemFont(ofSize: 12)
                textDict["ktext"] = (trustScore < 75) ? ("Higher the score, more the \'Likes\'.") as AnyObject : ("Keep building. You\'re almost at 100%!") as AnyObject?
                
                let trustImage = UIImage(named: imageName[1])!
                self.trustScoreView.image = trustImage
            }else {
                textDict["kmaxwidth"] = (self.trustScoreView.frame.size.width-16) as AnyObject
                textDict["kfont"] = UIFont.systemFont(ofSize: 12)
                textDict["ktext"] = "You need \(self.trustBuilder!.thresold!)% score to be verified." as AnyObject?
                
                let trustImage = UIImage(named: imageName[0])!
                self.trustScoreView.image = trustImage
            }
            
            //scoreLabel.frame.origin.y = scoreLabel.frame.origin.y-10
            if(self.trustText == nil) {
                
                stringFrame = TMStringUtil.textRect(forString: textDict)
                stringWidth = stringFrame.size.width
                stringHeight = stringFrame.size.height
                
                self.trustText = UILabel(frame: CGRect(x: (self.trustScoreView.frame.origin.x+(self.trustScoreView.frame.size.width-stringWidth)/2), y: (self.scoreLabel.frame.origin.y+self.scoreLabel.frame.size.height + 2.0), width: stringWidth, height: stringHeight))
                self.trustText.textAlignment = NSTextAlignment.center
                self.trustText.text = textDict["ktext"] as? String
                self.trustText.font = UIFont.systemFont(ofSize: 12);
                self.trustText.numberOfLines = 0
                self.trustText.sizeToFit()
                self.scrollView.addSubview(self.trustText)
                
                self.scoreLabel.frame.origin.y = self.scoreLabel.frame.origin.y-15
                self.trustText.frame.origin.y = self.trustText.frame.origin.y-15
                
            }else {
                
                stringFrame = TMStringUtil.textRect(forString: textDict)
                stringWidth = stringFrame.size.width
                stringHeight = stringFrame.size.height
                
                self.trustText.text = textDict["ktext"] as? String
                self.trustText.textAlignment = NSTextAlignment.center
                self.trustText.frame.origin.x = self.trustScoreView.frame.origin.x+(self.trustScoreView.frame.size.width-stringWidth)/2
                self.trustText.frame.origin.y = (self.scoreLabel.frame.origin.y+self.scoreLabel.frame.size.height + 2.0)
                self.trustText.frame.size.width = stringWidth
                self.trustText.frame.size.height = stringHeight
                
            }

        }
    }
    
    
    // textfield delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let oldLength = textField.text?.characters.count
        let replacementLength = string.characters.count
        let rangeLength = range.length

        let newLength = oldLength! - rangeLength + replacementLength
        if(newLength <= 10){
            return true
        }
        
        return false;
    }
    
    func textFieldDidChange () {
        let attributedString = NSMutableAttributedString(string: self.phoneText.text!)
        attributedString.addAttribute(NSKernAttributeName, value: 1.1, range: NSMakeRange(0, (self.phoneText.text?.characters.count)!))
        
        self.phoneText.attributedText = attributedString
    }
    
    //Digits changes
    //Send track details for phone verification
    func trackVerifyPhone() {
        
        MoEngage.sharedInstance().trackEvent("Phone Verification", andPayload: nil)
        self.sendTrackDetails(eventStatus: nil, eventAction: "phone_click")
//        self.eventDict["eventAction"] = "phone_server_call" as AnyObject?
//        self.trustMgr.trackEventDictionary = NSMutableDictionary(dictionary: self.eventDict)
    }
    
    //Sends Verification token to the server after successful digits login
    func sendDigitVerificationTokenToServer(phoneNumber: String) {
        
        if(self.trustMgr.isNetworkReachable()) {
            self.view.isUserInteractionEnabled = false
            self.addIndicator()
            self.activityIndicator.titleText = "Verifying Number..."
            self.activityIndicator.startAnimating()
            
            let digitsInstance = Digits.sharedInstance()
            let oauthSigningToken = DGTOAuthSigning(authConfig:digitsInstance.authConfig, authSession:digitsInstance.session())
            var authHeaders = oauthSigningToken!.oAuthEchoHeadersToVerifyCredentials()
            let url = authHeaders["X-Auth-Service-Provider"] as! String
            let token = authHeaders["X-Verify-Credentials-Authorization"] as! String
            self.getPhoneVerificationStatus(authUrl: url, authToken: token, phoneNo: phoneNumber)
            
        }else {
            self.showAlert(msg: TM_INTERNET_NOTAVAILABLE_MSG, title: "")
        }
    }
    
    //Fetches verification status of the phone number through the authentication token and url sent to the server.
    func getPhoneVerificationStatus(authUrl : String, authToken : String, phoneNo: String) {
        
        let queryString = ["action":"verifyDigits", "authUrl":authUrl, "authToken":authToken, "number":phoneNo]
        self.trustMgr.phoneStatus(queryString, with: { (response:[AnyHashable:Any]?, err:TMError?) -> Void in
            Digits.sharedInstance().logOut()
            if response != nil {
                let responseCode = response?["responseCode"] as! Int
                if(responseCode == 200) {
                    let status = response?["status"] as! String
                    if(status == "active"){
                        let status = "success"
                        self.sendTrackDetails(eventStatus: status, eventAction:"phone_verify")
                        self.view.isUserInteractionEnabled = true
                        self.removeLoader()
                        self.trustBuilder!.trustScore = self.trustBuilder!.trustScore + self.trustBuilder!.mobileScore
                        self.trustBuilder!.isPhoneVerified = true
                        self.trustBuilder!.phoneNumber = response?["user_number"] as? String
                        self.updatePhone()
                        if(self.delegate != nil) {
                            self.delegate!.didEditProfile!()
                        }
                    }
                }else if(responseCode == 403) {
                    let error_code = response?["error_code"] as! Int
                    let status = "error_code : \(error_code)"
                    self.sendTrackDetails(eventStatus: status, eventAction:"phone_verify")
                    self.view.isUserInteractionEnabled = true
                    self.removeLoader()
                    self.trustBuilder!.isPhoneVerified = false
                    self.trustBuilder!.phoneRejectedMsg = response?["error"] as! String
                    self.updatePhone()
                }
            }else {
                self.view.isUserInteractionEnabled = true
                self.removeLoader()
                self.errorResponse(err: err!)
            }
            
        })
        
    }
    
    //Sends event details to the server
    func sendTrackDetails(eventStatus : String?, eventAction : String) {
        self.eventDict["screenName"] = "TMTrustBuilderViewController" as AnyObject?
        self.eventDict["eventCategory"] = self.eventCategory as AnyObject?
        self.eventDict["eventAction"] =  eventAction as AnyObject?
        if eventStatus != nil {
            self.eventDict["status"] = eventStatus as AnyObject?
        }
        TMAnalytics.sharedInstance().trackNetworkEvent(self.eventDict)
    }
    
    //Returns country code of the user.
    func getCountryCode() -> String? {
        let country = userSession().getPhoneCountryCode()
        var countryCode : String?
        if country == 113 {
            countryCode = "+91"
        }else if country == 254 {
            countryCode = "+1"
        }
        return countryCode
    }
}

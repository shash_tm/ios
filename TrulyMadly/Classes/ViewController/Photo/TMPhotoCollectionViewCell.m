//
//  TMPhotoCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMPhotoCollectionViewCell.h"
#import "TrulyMadly-Swift.h"
#import "TMPhotoImageView.h"
#import "UIImage+TMAdditions.h"
#import "TMFileManager.h"
#import "XLCircleProgressIndicator.h"

static void * TMTaskCountOfBytesSentContext = &TMTaskCountOfBytesSentContext;
static void * TMTaskCountOfBytesReceivedContext = &TMTaskCountOfBytesReceivedContext;

@interface TMPhotoCollectionViewCell ()

@property(nonatomic,strong)UIImageView *bgImgView;
@property(nonatomic,strong)UILabel *rejectedLabel;
@property(nonatomic,strong)TMCircleProgressIndicator *uploadProgressIndicater;

@end


@implementation TMPhotoCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        self.profileImgView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.profileImgView.layer.cornerRadius = 10;
        self.profileImgView.clipsToBounds = true;
        self.profileImgView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:self.profileImgView];
        
        self.bgImgView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self.contentView addSubview:self.bgImgView];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityIndicator.frame = CGRectMake((self.bounds.size.width - 44)/2, (self.bounds.size.height - 44)/2, 44, 44);
        self.activityIndicator.hidesWhenStopped = YES;
        self.activityIndicator.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.activityIndicator];
        
        self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake((self.bounds.size.width - 44)/2, (self.bounds.size.height - 44)/2, 44, 44)];
        self.progressView.backgroundColor = [UIColor yellowColor];
        //[self.contentView addSubview:self.progressView];
        
        self.rejectedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,10, self.frame.size.width, 20)];
        self.rejectedLabel.font = [UIFont boldSystemFontOfSize:14];
        self.rejectedLabel.backgroundColor = [UIColor redColor];
        self.rejectedLabel.textColor = [UIColor whiteColor];
        self.rejectedLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.rejectedLabel];
    }
    
    return self;
}
-(void)dealloc {
    [self hideImageUploadProgressLoader];
}

-(void)layoutSubviews {
    self.profileImgView.frame = self.bounds;
    self.bgImgView.frame = self.bounds;
    self.activityIndicator.frame = CGRectMake((self.bounds.size.width - 44)/2, (self.bounds.size.height - 44)/2, 44, 44);
}

-(void)setIsImageLoding:(BOOL)isImageLoding {
    _isImageLoding = isImageLoding;
    
    if(isImageLoding) {
        [self startActivityLoader];
    }
}

-(void)setIsImageUpLoding:(BOOL)isImageUpLoding {
    _isImageUpLoding = isImageUpLoding;
    
    if(isImageUpLoding) {
        [self startActivityLoader];
    }
}

-(void)startActivityLoader {
    if(self.isImageLoding) {
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    [self.activityIndicator startAnimating];
    [self.contentView bringSubviewToFront:self.activityIndicator];
}
-(void)stopActivityLoader {
    [self.activityIndicator stopAnimating];
}

-(void)showImageUploadProgressLoader:(NSURLSessionDataTask*)task {
//    if(![self.uploadProgressIndicater superview]) {
//        [self addSubview:self.uploadProgressIndicater];
//        
//        [task addObserver:self forKeyPath:@"state" options:(NSKeyValueObservingOptions)0 context:TMTaskCountOfBytesSentContext];
//        [task addObserver:self forKeyPath:@"countOfBytesSent" options:(NSKeyValueObservingOptions)0 context:TMTaskCountOfBytesSentContext];
//    }
}
-(void)hideImageUploadProgressLoader {
//    if(self.uploadProgressIndicater && [self.uploadProgressIndicater superview]) {
//        [self.uploadProgressIndicater removeFromSuperview];
//        self.uploadProgressIndicater = nil;
//        @try {
//            [self removeObserver:self forKeyPath:@"state" context:TMTaskCountOfBytesSentContext];
//            [self removeObserver:self forKeyPath:@"countOfBytesSent" context:TMTaskCountOfBytesSentContext];
//        }
//        @catch (NSException *exception) {}
//    }
}


-(TMCircleProgressIndicator *)uploadProgressIndicater
{
    if(!_uploadProgressIndicater) {
        _uploadProgressIndicater = [[TMCircleProgressIndicator alloc] initWithFrame:CGRectMake(0,
                                                                                               0,
                                                                                               CGRectGetWidth(self.bounds),
                                                                                               CGRectGetHeight(self.bounds))
                                                                   showCancelButton:FALSE];
        _uploadProgressIndicater.center = CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2);
        _uploadProgressIndicater.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin |
        UIViewAutoresizingFlexibleLeftMargin |
        UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin;
        [_uploadProgressIndicater setProgressValue:0.0f];
    }
    return _uploadProgressIndicater;
}

-(void)setPhotoUploadTask:(NSURLSessionDataTask*)task {
    if(task) {
        [self showImageUploadProgressLoader:task];
    }
    else {
       [self hideImageUploadProgressLoader];
    }
}
//Add_Photo_Main
//Add_Extra_Photos
//Remove_Photo_Main
//Remove_Extra_Photo
//extra_neutral
//main_neutral
-(void)setPhotoData:(TMPhoto*)photo isProfilePhotoContainer:(BOOL)profilePhotoContainer{
    UIImage *bgImage = nil;
    NSString *profileImgUrl = nil;
    UIImage *placeholderImage = nil;
    
    if(photo) {
        [self hideImageUploadProgressLoader];
        self.bgImgView.image = nil;
        if(profilePhotoContainer) {
            profileImgUrl = photo.fullphotoname;
            placeholderImage = [UIImage imageNamed:@"main_neutral"];
            [self.contentView bringSubviewToFront:self.profileImgView];
            
            bgImage = nil;
        }
        else {
            profileImgUrl = photo.fullphotoname; //photo.photothumbnail;
            placeholderImage = [UIImage imageNamed:@"extra_neutral"];
            
            bgImage = [UIImage imageNamed:@"Remove_Extra_Photo"];
            [self.contentView bringSubviewToFront:self.profileImgView];
        }
    }
    else {
        profileImgUrl = nil;
        [self.contentView bringSubviewToFront:self.bgImgView];
        
        if(profilePhotoContainer) {
            bgImage = [UIImage imageNamed:@"Add_Photo_Main"];
        }
        else {
            bgImage = [UIImage imageNamed:@"Add_Extra_Photos"];
        }
    }

    if(profileImgUrl) {
        [self hideImageUploadProgressLoader];
        TMFileManager* fileManager = [[TMFileManager alloc] init];
        NSString *lastPathComponent = [photo.fullphotoname lastPathComponent];
        
        if ([fileManager isFilePresentAtPath:lastPathComponent]) {
            self.profileImgView.image = [UIImage imageWithData:[fileManager getFileAtPath:lastPathComponent]];
            self.bgImgView.image = bgImage;
            if(bgImage) {
                [self.contentView bringSubviewToFront:self.bgImgView];
            }
        }
        else {
            [self startActivityLoader];
            NSURL *imgUrl = [NSURL URLWithString:photo.fullphotoname];
            NSURLRequest *req = [NSURLRequest requestWithURL:imgUrl];
            __weak __typeof(self)weakSelf = self;
            [self.profileImgView setImageWithURLRequest:req placeholderImage:placeholderImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                weakSelf.bgImgView.image = bgImage;
                if(bgImage) {
                    [weakSelf.contentView bringSubviewToFront:weakSelf.bgImgView];
                }
                if(image) {
                    weakSelf.profileImgView.image = [image scaleImageToSize:weakSelf.frame.size];
                }
                
                [weakSelf stopActivityLoader];
                [weakSelf hideImageUploadProgressLoader];
                //cache the image
                [fileManager writeFileWithData:UIImagePNGRepresentation(image) toPath:lastPathComponent];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                [weakSelf stopActivityLoader];
                [weakSelf hideImageUploadProgressLoader];
            }];
        }
    }
    else { //
        self.profileImgView.image = nil;
        self.bgImgView.image = bgImage;
    }
    
    if(self.isImageLoding) {
        //NSLog(@"Image Loading Loader:%@",photo.fullphotoname);
    }
    if(photo && (self.isImageLoding || self.isImageUpLoding)) {
        //NSLog(@"Stping Loader:%@",photo.fullphotoname);
        self.isImageLoding = false;
        self.isImageUpLoding = false;
        [self stopActivityLoader];
        [self hideImageUploadProgressLoader];
    }
    //    else if(self.isImageLoding) {
    //        [self startActivityLoader];
    //    }
    
    if(self.isImageRejected) {
        [self.contentView bringSubviewToFront:self.rejectedLabel];
        self.rejectedLabel.text = @"Rejected";
        self.rejectedLabel.frame = CGRectMake(0,10, self.frame.size.width, 20);
    }
    else {
        self.rejectedLabel.text = nil;
        self.rejectedLabel.frame = CGRectZero;
    }
}

//-(void)observeValueForKeyPath:(NSString *)keyPath
//                      ofObject:(id)object
//                        change:(__unused NSDictionary *)change
//                       context:(void *)context
//{
//#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
//    if (context == TMTaskCountOfBytesSentContext || context == TMTaskCountOfBytesReceivedContext) {
//        if ([keyPath isEqualToString:NSStringFromSelector(@selector(countOfBytesSent))]) {
//            if ([object countOfBytesExpectedToSend] > 0) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    CGFloat progress = [object countOfBytesSent] / ([object countOfBytesExpectedToSend] * 1.0f);
//                    [self.uploadProgressIndicater setProgressValue:progress];
//                });
//            }
//        }
//        
//        if ([keyPath isEqualToString:NSStringFromSelector(@selector(countOfBytesReceived))]) {
//            if ([object countOfBytesExpectedToReceive] > 0) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    CGFloat progress = [object countOfBytesReceived] / ([object countOfBytesExpectedToReceive] * 1.0f);
//                    [self.uploadProgressIndicater setProgressValue:progress];
//                });
//            }
//        }
//        
//        if ([keyPath isEqualToString:NSStringFromSelector(@selector(state))]) {
//            if ([(NSURLSessionTask *)object state] == NSURLSessionTaskStateCompleted) {
//                @try {
//                    [object removeObserver:self forKeyPath:NSStringFromSelector(@selector(state))];
//                    
//                    if (context == TMTaskCountOfBytesSentContext) {
//                        [object removeObserver:self forKeyPath:NSStringFromSelector(@selector(countOfBytesSent))];
//                    }
//                    
//                    if (context == TMTaskCountOfBytesReceivedContext) {
//                        [object removeObserver:self forKeyPath:NSStringFromSelector(@selector(countOfBytesReceived))];
//                    }
//                }
//                @catch (NSException * __unused exception) {}
//            }
//        }
//    }
//#endif
//}


@end

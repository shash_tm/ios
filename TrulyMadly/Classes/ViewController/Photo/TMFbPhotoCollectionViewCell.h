//
//  TMFbPhotoCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMFbPhotoCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong)UIImageView *profileImgView;

@end

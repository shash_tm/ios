//
//  TMFacebookPhotoPicker.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFacebookPhotoPicker.h"
#import "TMFbPhotoViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "TrulyMadly-Swift.h"
#import "TMFacebookManager.h"


@interface TMFacebookPhotoPicker ()<UITableViewDataSource,UITableViewDelegate,TMFbPhotoViewControllerDelegate,UIAlertViewDelegate>

@property(nonatomic,strong)NSArray *fbAlbums;
@property(nonatomic,strong)TMFacebookManager *fbMgr;
@property(nonatomic,strong)UITableView *fbAlbumsListView;
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@property(nonatomic,assign)BOOL isFacebookStateChangedNotificationReceived;
@property(nonatomic,assign)BOOL isFBFetchAlbum;

@end

@implementation TMFacebookPhotoPicker

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self.navigationController setNavigationBarHidden:NO];
    self.view.backgroundColor = [UIColor whiteColor];
    self.fbMgr = [[TMFacebookManager alloc] init];
    
    self.isFacebookStateChangedNotificationReceived = false;
    self.isFBFetchAlbum = false;
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    self.navigationItem.leftBarButtonItem = btnItem;
    
    self.fbAlbumsListView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.fbAlbumsListView.dataSource = self;
    self.fbAlbumsListView.delegate = self;
    [self.view addSubview:self.fbAlbumsListView];
    
    [self FetchAlbum];
}

-(void)showActivityIndicatorViewWithMessage:(NSString*)message {
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor grayColor];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancelAction {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.fbAlbums.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reId = @"123";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reId];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *dict = [self.fbAlbums objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [self.fbAlbums objectAtIndex:indexPath.row];
    
    TMFbPhotoViewController *fbPhotoVC  = [[TMFbPhotoViewController alloc] init];
    fbPhotoVC.photoAlbumId = [dict objectForKey:@"id"];
    fbPhotoVC.fbMgr = self.fbMgr;
    fbPhotoVC.delegate = self;
    [self.navigationController pushViewController:fbPhotoVC animated:YES];
}

-(void)didSelectPhoto:(UIImage *)image {//(NSString *)imgUrl {
    
    //[self.delegate didSelectPhotoWithUrl:imgUrl];
    [self.delegate didSelectPhotoWithUrl:image];
}


-(void)FetchAlbum {
    
    if(self.fbMgr.isNetworkReachable) {
        if(!self.isFBFetchAlbum) {
            self.isFBFetchAlbum = true;
            [self showActivityIndicatorViewWithMessage:@"Loading..."];
            [self.fbMgr fetchFacebookAlbumList:^(NSArray *photoAlbums, TMError *error) {
                self.isFBFetchAlbum = false;
                [self hideActivityIndicatorView];
                if(photoAlbums && [photoAlbums count]>0) {
                    self.fbAlbums = photoAlbums;
                    [self.fbAlbumsListView reloadData];
                }else {
                    [[FBSession activeSession] closeAndClearTokenInformation];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Problem in fetching your facebook photos" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Problem in fetching your facebook photos" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

@end


//
//  TMFbPhotoViewController.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMFacebookManager;
@protocol TMFbPhotoViewControllerDelegate;

@interface TMFbPhotoViewController : UIViewController

@property(nonatomic,weak)id<TMFbPhotoViewControllerDelegate> delegate;
@property(nonatomic,strong)NSString *photoAlbumId;
@property(nonatomic,strong)TMFacebookManager *fbMgr;

@end

@protocol TMFbPhotoViewControllerDelegate <NSObject>

//-(void)didSelectPhoto:(NSString*)imgUrl;
-(void)didSelectPhoto:(UIImage *)image;

@end
//
//  TMPhotoViewController.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"
#import "TMEnums.h"

@interface TMPhotoViewController : TMBaseViewController

@property(nonatomic,assign)BOOL fromPIDashboard;
-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow;
-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow
                         alertMessage:(NSString*)alertMessage;
-(instancetype)initForFemaleProfileRejectionWithNavigationFlow:(TMNavigationFlow)navigationFlow
                                                  alertMessage:(NSString*)alertMessage;

@end

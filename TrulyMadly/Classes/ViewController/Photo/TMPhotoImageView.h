//
//  TMPhotoImageView.h
//  TrulyMadly
//
//  Created by Ankit on 21/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMPhotoImageView : UIImageView

-(void)setImageWithURLAndProgressbar:(NSString*)imageUrl;

@end

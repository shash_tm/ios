//
//  TMFacebookPhotoPicker.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMFacebookPhotoPickerDelegate;

@interface TMFacebookPhotoPicker : UIViewController

@property(nonatomic,weak)id<TMFacebookPhotoPickerDelegate> delegate;

@end

@protocol TMFacebookPhotoPickerDelegate <NSObject>

//-(void)didSelectPhotoWithUrl:(NSString*)imgUrl;
-(void)didSelectPhotoWithUrl:(UIImage *)image;

@end
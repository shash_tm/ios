//
//  TMFbPhotoCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFbPhotoCollectionViewCell.h"


@interface TMFbPhotoCollectionViewCell ()

@end

@implementation TMFbPhotoCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.profileImgView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.profileImgView.contentMode = UIViewContentModeScaleAspectFill;
        self.profileImgView.layer.cornerRadius = 10;
        self.profileImgView.clipsToBounds = true;
        self.profileImgView.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:self.profileImgView];
    }
    
    return self;
}

-(void)layoutSubviews {
    self.profileImgView.frame = self.bounds;
}

@end

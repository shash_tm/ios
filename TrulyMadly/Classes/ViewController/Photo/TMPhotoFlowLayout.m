//
//  TMPhotoFlowLayout.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMPhotoFlowLayout.h"

@interface TMPhotoFlowLayout ()

@property (strong, nonatomic) NSMutableArray *itemAttributes;
@property (nonatomic, assign) CGSize contentSize;

@end


@implementation TMPhotoFlowLayout

- (void)prepareLayout {
    
    [super prepareLayout];
    self.itemAttributes = [[NSMutableArray alloc] init];
    
    CGFloat xPos = self.horizontalSideMargin;
    CGFloat yPos = self.verticalSideMargin;
    
    CGFloat contentWidth = 0.0;         // Used to determine the contentSize
    CGFloat contentHeight = 0.0;        // Used to determine the contentSize
    
    //Loop through all items and calculate the UICollectionViewLayoutAttributes for each on
    NSUInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    for (NSUInteger index = 0; index < numberOfItems; index++)
    {
        CGSize itemSize = [self.dataSource contentSizeForIndex:index];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        
        attributes.frame = CGRectIntegral(CGRectMake(xPos, yPos, itemSize.width, itemSize.height));
        
        [self.itemAttributes addObject:attributes];

        if(index == 0) {
            xPos = xPos + itemSize.width + self.itemOffset.horizontal;
        }
        else if(index == 1) {
            yPos = yPos + itemSize.height + self.itemOffset.vertical;
        }
        else if(index == 2) {
            xPos = self.horizontalSideMargin;
            yPos = yPos + itemSize.height + self.itemOffset.vertical;
        }
        else if(index == 5) {
            xPos = 0;
            yPos = yPos + itemSize.height + self.itemOffset.vertical;
        }
        else if(index == 6) {
            //yPos = yPos + itemSize.height + self.itemOffset.vertical;
        }
        else if(index == 7) {
            xPos = 10;
            yPos = yPos + 44 + self.itemOffset.vertical;//self.footerHeight + itemSize.height;
            UICollectionViewLayoutAttributes *attributes = [_itemAttributes lastObject];
            CGRect screenBounds = [UIScreen mainScreen].bounds;
            attributes.frame = CGRectIntegral(CGRectMake(xPos, yPos, screenBounds.size.width-20, self.footerHeight));
            //attributes.frame = CGRectIntegral(CGRectMake(xPos, yPos, 300, self.footerHeight));
        }
        else {
            xPos = xPos + itemSize.width + self.itemOffset.horizontal;
        }

    }

    //NSLog(@"yPos:%f",yPos);
    // Get the last item to calculate the total height of the content
    UICollectionViewLayoutAttributes *attributes = [_itemAttributes lastObject];
    contentHeight = attributes.frame.origin.y+attributes.frame.size.height;
    contentWidth = attributes.frame.origin.x + attributes.frame.size.width;
    
    // Return this in collectionViewContentSize
    self.contentSize = CGSizeMake(contentWidth, contentHeight+80);
}

-(CGSize)collectionViewContentSize
{
    return self.contentSize;
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.itemAttributes objectAtIndex:indexPath.row];
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    return [self.itemAttributes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [evaluatedObject frame]);
    }]];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind
                                                                     atIndexPath:(NSIndexPath *)indexPath {
    
    return nil;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return NO;
}


@end

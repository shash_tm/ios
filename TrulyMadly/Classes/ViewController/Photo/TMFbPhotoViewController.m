//
//  TMFbPhotoViewController.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFbPhotoViewController.h"
#import "TMFbPhotoCollectionViewCell.h"
#import "TMFacebookManager.h"
#import "UIImageView+AFNetworking.h"
#import "TMActivityIndicatorView.h"


@interface TMFbPhotoViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>

@property(nonatomic,strong)UICollectionView *fbPhotoCollectionView;
@property(nonatomic,strong)NSArray *fbPhotos;
@property(nonatomic,strong)TMActivityIndicatorView *activityIndicatorView;
@end

@implementation TMFbPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.minimumLineSpacing = 15;
    flowLayout.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 25);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;

    
    self.fbPhotoCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.fbPhotoCollectionView.backgroundColor = [UIColor clearColor];
    self.fbPhotoCollectionView.dataSource = self;
    self.fbPhotoCollectionView.delegate = self;
    [self.view addSubview:self.fbPhotoCollectionView];
    
    [self.fbPhotoCollectionView registerClass:[TMFbPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"photocvc"];
    
    if(self.fbMgr.isNetworkReachable) {
        [self showActivityIndicatorViewWithMessage:@"Loading Facebook Photos..."];
        [self.fbMgr fetchPhotosFromAlbum:self.photoAlbumId withResponse:^(NSArray *photoAlbums, TMError *error) {
            [self hideActivityIndicatorView];
            if(!error) {
                self.fbPhotos = photoAlbums;
                [self.fbPhotoCollectionView reloadData];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Problem in fetching your facebook photos" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)showActivityIndicatorViewWithMessage:(NSString*)message {
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor grayColor];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.fbPhotos.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusableIdentifier = @"photocvc";
    
    TMFbPhotoCollectionViewCell *cell = [collectionView
                                       dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                       forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    NSDictionary *dict = [self.fbPhotos objectAtIndex:indexPath.row];
    
    NSDictionary *imgDict = NULL;
    NSArray *images = [dict objectForKey:@"images"];
    NSInteger index = (images.count > 1) ? 1 : 0;
    if(images.count) {
        imgDict = [images objectAtIndex:index];
    }
    
    NSURL *url = [NSURL URLWithString:[imgDict objectForKey:@"source"]];
    [cell.profileImgView setImageWithURL:url placeholderImage:nil];
    return  cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (self.view.frame.size.width - 25)/3;
    CGSize size = CGSizeMake(width, width);
    
    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMFbPhotoCollectionViewCell *cell = (TMFbPhotoCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//    NSDictionary *dict = [self.fbPhotos objectAtIndex:indexPath.row];
//    NSArray *images = [dict objectForKey:@"images"];
//    NSDictionary *imgDict = [images objectAtIndex:1];
//   [self.delegate didSelectPhoto:[imgDict objectForKey:@"source"]];
    [self.delegate didSelectPhoto:cell.profileImgView.image];
}

@end

//
//  TMPhotoViewController.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMPhotoViewController.h"
#import "TMPhotoCollectionViewCell.h"
#import "TMPhotoFlowLayout.h"
#import "TMPhotoManager.h"
#import "TMFacebookPhotoPicker.h"
#import <FacebookSDK/FacebookSDK.h>
#import "TMTrustBuilderManager.h"
#import "ECSlidingViewController.h"
#import "UIProgressView+AFNetworking.h"
#import "TMActivityIndicatorView.h"
#import "TMEnums.h"
#import "TMErrorMessages.h"
#import "TMSwiftHeader.h"
#import "UIColor+TMColorAdditions.h"
#import "TMPhotoGuideLinesViewController.h"
#import "TMPhotoAlertView.h"
#import "TMLog.h"
#import "TMProfileCompletionToast.h"
#import "MoEngage.h"
#import "DZNPhotoEditorViewController.h"
#import "TMToastView.h"


@interface TMPhotoViewController () <UICollectionViewDataSource,UICollectionViewDelegate,TMPhotoFlowLayoutDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TMFacebookPhotoPickerDelegate,UIAlertViewDelegate,TMPhotoAlertViewDelegate>

@property(nonatomic,strong)UICollectionView *photoCollectionView;
@property(nonatomic,strong)TMPhotoManager *photoMgr;
@property(nonatomic,strong)TMPhotoResponse *photoResponse;
@property(nonatomic,strong)TMPhoto *selectedPhoto;
@property(nonatomic,strong)NSIndexPath *selectedIndexPath;
@property(nonatomic,assign)BOOL isAddPhotoOptionEnabled;
@property(nonatomic,assign)BOOL isCameraPresent;
@property(nonatomic,assign)TMNavigationFlow navigationFlow;
@property(nonatomic,assign)NSInteger currentPhotoIndex;
@property(nonatomic,assign)NSInteger photoIndexOffset;
@property(nonatomic,strong)TMTrustBuilderManager *trustBuilderMgr;
@property(nonatomic,strong)TMPhotoGuideLinesViewController *photoguidelinesViewController;
@property(nonatomic,assign)BOOL isFemaleProfileRejectionToastToBeShown;
@property(nonatomic,assign)NSString* identifier;
@property(nonatomic,assign)BOOL isFacebookStateChangedNotificationReceived;
@property(nonatomic,strong)TMFacebookPhotoPicker *fbPhotoPicker;
//@property(nonatomic,strong)NSMutableDictionary *photoUploadTask;


@property(nonatomic,strong)NSString *alertMessage;

@end

@implementation TMPhotoViewController

-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow {
    self = [super init];
    if(self) {
        [self initDefaultsForNavigationFlow:navigationFlow];
    }
    return self;
}

-(instancetype)initWithNavigationFlow:(TMNavigationFlow)navigationFlow
                         alertMessage:(NSString*)alertMessage {
    self = [super init];
    if(self) {
        self.alertMessage = alertMessage;
        [self initDefaultsForNavigationFlow:navigationFlow];
    }
    return self;
}


-(instancetype)initForFemaleProfileRejectionWithNavigationFlow:(TMNavigationFlow)navigationFlow
                         alertMessage:(NSString*)alertMessage {
    self = [super init];
    if(self) {
        self.alertMessage = alertMessage;
        self.isFemaleProfileRejectionToastToBeShown = YES;
        [self initDefaultsForNavigationFlow:navigationFlow];
    }
    return self;
}

-(void)initDefaultsForNavigationFlow:(TMNavigationFlow)navigationFlow {
    self.currentPhotoIndex = 0;
    self.photoIndexOffset = 0;
    self.navigationFlow = navigationFlow;
    self.photoMgr = [[TMPhotoManager alloc] init];
    [self fetchPhotoData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Say Cheese!";
    self.identifier = @"photo";
    
    // MoEngage Event
    [[MoEngage sharedInstance] trackEvent:@"User Photos Launched" andPayload:nil];
    
    [[TMAnalytics sharedInstance] trackView:@"TMPhotoViewController"];
    
    [self initViewComponents];
    
    self.isCameraPresent  = [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera];
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    TMLOG(@"viewDidDisappear --- presented view controller:%@",self.presentedViewController);
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.photoMgr = nil;
    TMLOG(@"viewWillDisappear --- presented view controller:%@",self.presentedViewController);
}

-(void)dealloc {
    if(self.photoguidelinesViewController) {
        [self.photoguidelinesViewController dismissViewControllerAnimated:false completion:nil];
    }
    if(self.fbPhotoPicker) {
        [self.fbPhotoPicker dismissViewControllerAnimated:false completion:nil];
    }
}

//-(NSMutableDictionary*)photoUploadTask {
//    if(!_photoUploadTask) {
//        _photoUploadTask = [NSMutableDictionary dictionaryWithCapacity:4];
//    }
//    return _photoUploadTask;
//}


#pragma mark setup ui Components
#pragma mark-

-(void)initViewComponents {
    TMPhotoFlowLayout *flowLayout = [[TMPhotoFlowLayout alloc] init];
    flowLayout.maxWidth = self.view.frame.size.width;
    flowLayout.itemOffset = UIOffsetMake(8, 8);
    flowLayout.horizontalSideMargin = 10;
    flowLayout.verticalSideMargin = 15;
    flowLayout.footerHeight = 50;
    flowLayout.dataSource = self;
    
    if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) {
        self.photoCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    }
    else {
        self.photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-64.0) collectionViewLayout:flowLayout];
    }
    self.photoCollectionView.backgroundColor = [UIColor clearColor];
    self.photoCollectionView.dataSource = self;
    self.photoCollectionView.delegate = self;
    [self.view addSubview:self.photoCollectionView];
    
    [self.photoCollectionView registerClass:[TMPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"photocvc"];
    [self.photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"continue"];
    [self.photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoguidelines"];
    
    [self.photoCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
}
-(void)configureContinueButton:(UIButton*)continueButton {
    if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) {
        /* user has taken some action in trustbuilder so user cannot skip photo upload
         * self.photoResponse.allowSkip == 0
         * self.photoResponse.photos.count ==0 (sanity check)
         */
        if( (!self.photoResponse.photos) && (!self.photoResponse.allowSkip) ) {
            continueButton.userInteractionEnabled = false;
           // continueButton.enabled = false;
            continueButton.backgroundColor = [UIColor inactiveButtonColor];
            [continueButton setTitle:@"Continue" forState:UIControlStateNormal];
        }
        /* user can skip photo upload
         * self.photoResponse.allowSkip == 1
         * self.photoResponse.photos.count != 0, then title = Continue else title = Skip
         */
        else {
            continueButton.userInteractionEnabled = true;
            continueButton.backgroundColor = [UIColor activeButtonColor];
            if(self.photoResponse.photos.count){
                [continueButton setTitle:@"Continue" forState:UIControlStateNormal];
            }
            else {
                [continueButton setTitle:@"Skip" forState:UIControlStateNormal];
            }
        }
    }
    else {
        continueButton.userInteractionEnabled = true;
        //continueButton.enabled = true;
        continueButton.backgroundColor = [UIColor activeButtonColor];
    }
    
}
-(void)continueAction :(id)sender{
    if([[[sender titleLabel] text ] isEqualToString:@"Skip"]) {
        [[TMAnalytics sharedInstance] trackNetworkEvent:[self dictForTracking:@"photo_skip"]];
    }
    [self.delegate popToRootViewControllerAndMoveToViewControllerWithId:@"quiz" animated:YES];
}

-(void)fetchPhotoData {
    if(self.photoMgr == nil) {
        self.photoMgr = [[TMPhotoManager alloc] init];
    }
    
    self.photoMgr.trackEventDictionary = [self dictForTracking:@"page_load"];
  //  if([self.photoMgr isNetworkReachable]) {
        [self configureViewForDataLoadingStart];
        [self.photoMgr getPhotoData:^(TMPhotoResponse *photoResponse, TMError *error) {
            [self hideActivityIndicatorView];
            
            if(photoResponse) {
                self.photoResponse = photoResponse;
                self.currentPhotoIndex = 0;
                self.photoIndexOffset = 0;
                [self.photoCollectionView reloadData];
                [self showProfileRejectedToast];
            }
            else {
                //error case
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    //remove modal vc and show alart for network error
                    //[self showPhotoResponseErrorAlert:PHOTO_LOADING_ERROR_TITLE
                      //                            msg:TM_INTERNET_NOTAVAILABLE_
                    if(![self.photoMgr isCachedResponsePresent]) {
                        if (error.errorCode == TMERRORCODE_NONETWORK) {
                            [self showRetryView];
                             [self.retryView.retryButton addTarget:self action:@selector(retryAction) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else {
                            [self showRetryViewAtError];
                             [self.retryView.retryButton addTarget:self action:@selector(retryAction) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                }
                else {
                    //unknwon error
                    [self showRetryViewAtUnknownError];
                     [self.retryView.retryButton addTarget:self action:@selector(retryAction) forControlEvents:UIControlEventTouchUpInside];
                    //[self showPhotoResponseErrorAlert:PHOTO_LOADING_ERROR_TITLE msg:PLEASETRYLATER_ERROR_MSG];
                }
            }
        }];
//    }
//    else {
//        [self showRetryViewAtError];
//        //[self showPhotoResponseErrorAlert:PHOTO_LOADING_ERROR_TITLE msg:TM_INTERNET_NOTAVAILABLE_MSG];
//    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(retryAction) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.isRetryViewShown) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(retryAction) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)configureViewForDataLoadingStart {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithMessage:@"Loading Photos..."];
}
-(void)retryAction{
    [self fetchPhotoData];
}

-(void)updateProfilePhotoUpdateStatus {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    userSession.isUserProfileUpdated = true;
    userSession.isProfilePhotoUpdated = true;
}

#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger rowCount = 0;
    if(self.photoResponse) {
        rowCount = 7;
//        if(self.navigationFlow == TMNAVIGATIONFLOW_REGISTRATION) {
//            rowCount = 8;
//        }
//        else {
//            rowCount = 7;
//        }
    }
    return rowCount;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(self.view.frame.size.width, 100);
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 7){
        NSString *reusableIdentifier = @"continue";
        
        UICollectionViewCell *cell = [collectionView
                                           dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                           forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        
        UIButton *continueBtn = (UIButton*)[cell.contentView viewWithTag:910001];
        if(!continueBtn) {
            continueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            continueBtn.frame = CGRectMake(10, 5, self.view.frame.size.width-40, 44);
            continueBtn.tag = 910001;
            continueBtn.layer.cornerRadius = 10;
            [continueBtn addTarget:self action:@selector(continueAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:continueBtn];
        }
        [self configureContinueButton:continueBtn];
        return  cell;
    }
    if(indexPath.row == 6){
        NSString *reusableIdentifier = @"photoguidelines";
        
        UICollectionViewCell *cell = [collectionView
                                      dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                      forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        
        UILabel *photoguidelInesText = (UILabel*)[cell.contentView viewWithTag:910002];
        if(!photoguidelInesText) {
            photoguidelInesText = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-220, 0, 210, 25)];
            photoguidelInesText.tag = 910001;
            photoguidelInesText.text = @"Photo Guidelines & Privacy";
            photoguidelInesText.textColor = [UIColor grayColor];
            photoguidelInesText.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:photoguidelInesText];
        }
        return  cell;
    }
    else {
        NSString *reusableIdentifier = @"photocvc";
        
        TMPhotoCollectionViewCell *cell = [collectionView
                                           dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                           forIndexPath:indexPath];
        BOOL isProfilePhotoContainer = (indexPath.row == 0) ? 1 : 0;
        TMPhoto *photo = [self.photoResponse photoForIndex:self.currentPhotoIndex];
        if(indexPath.row == 0 && ![photo isProfilePhoto]) {
            photo = nil;
            self.currentPhotoIndex = 0;
            self.photoIndexOffset = 1;
        }
        else {
            self.currentPhotoIndex++;
        }
        cell.isImageRejected = (photo) ? [photo isPhotoRejected] : FALSE;
        cell.isImageUpLoding = FALSE;
        [cell.activityIndicator stopAnimating];
        [cell setPhotoData:photo isProfilePhotoContainer:isProfilePhotoContainer];
        //NSURLSessionDataTask *task = self.photoUploadTask[indexPath];
        //[cell setPhotoUploadTask:task];
        return  cell;
    }
}

//add from photo gallery, camera, facebook
// make profile pic, delete

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMPhotoCollectionViewCell *cell = (TMPhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if(indexPath.row == 6) {
        self.photoguidelinesViewController = [[TMPhotoGuideLinesViewController alloc] init];
        [self.navigationController presentViewController:self.photoguidelinesViewController animated:YES completion:nil];
    }
    else if((indexPath.row < 6) && !([cell.activityIndicator isAnimating])) {
        NSInteger validPhotoIndex = -1;
        TMPhoto *photo = nil;
        self.selectedIndexPath = indexPath;
        if(indexPath.row !=0) {
            if(self.photoIndexOffset){
                validPhotoIndex = (self.photoIndexOffset) ? (indexPath.row - self.photoIndexOffset) : indexPath.row;
            }
            else {
                validPhotoIndex = indexPath.row;
            }
            if(validPhotoIndex>=0) {
                photo = [self.photoResponse photoForIndex:validPhotoIndex];
                self.selectedPhoto = photo;
            }
        }
        else if(indexPath.row ==0 && !self.photoIndexOffset){
            photo = [self.photoResponse photoForIndex:0];
            self.selectedPhoto = photo;
        }
        
        if(!photo) {
            [self showOptionsToAddphoto];
        }
        else {//if(![photo isProfilePhoto]) {
            //show option to delete or make profile photo
            [self showOptionsToUpdatephoto];
        }
    }
}

-(CGSize)contentSizeForIndex:(NSInteger)index {
    if(index == 0) {
        CGFloat width = floor(((self.view.bounds.size.width - 28) * 2)/3);
        CGSize size = CGSizeMake(width, width+8);
        return size;
    }
    else if(index == 3 || index == 4) {
        CGFloat width = floor((self.view.bounds.size.width - 28)/3);
        CGSize size = CGSizeMake(width-4, width);
        return size;
    }
    else if(index == 6) {
        CGSize size = CGSizeMake(self.view.bounds.size.width, 44);
        return size;
    }
    else {
        CGFloat width = floor((self.view.bounds.size.width - 28)/3);
        CGSize size = CGSizeMake(width, width);
        return size;
    }
}

#pragma mark Action handler
#pragma mark -

-(void)deleteSelectedPhoto {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Want to delete the photo" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.tag = 410001;
    [alert show];
}

-(void)showOptionsToAddphoto {
    
    self.isAddPhotoOptionEnabled = TRUE;
    
    UIActionSheet *actionSheet = NULL;
    
    if (self.isCameraPresent) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Facebook",@"Camera", nil];
    }
    else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Facebook", nil];
    }
    
    [actionSheet showInView:self.view];
}

-(void)showOptionsToUpdatephoto {
    
    self.isAddPhotoOptionEnabled = FALSE;
    UIActionSheet *actionSheet = NULL;
    
    if([self.selectedPhoto isProfilePhoto]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Edit Photo",nil];
        actionSheet.tag = 510003;
    }else if([self.selectedPhoto canSetAsProfilePhoto]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Make Profile Picture", @"Edit Photo", @"Delete Photo",nil];
        actionSheet.tag = 510001;
    }
    else {
        NSString *msgStr = NULL;
        if(self.selectedPhoto.isPhotoRejected) {
            msgStr = @"This image is rejected by TrulyMadly";
            actionSheet = [[UIActionSheet alloc] initWithTitle:msgStr delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete Photo", nil];
            actionSheet.tag = 510002;
        }
        else {
            msgStr = @"This image has not been approved to be set as a profile picture";
            actionSheet = [[UIActionSheet alloc] initWithTitle:msgStr delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Edit Photo", @"Delete Photo", nil];
            actionSheet.tag = 510004;
        }
    }
    
    [actionSheet showInView:self.view];
}

#pragma mark Actionsheet / alertview delegate handler
#pragma mark -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(self.isAddPhotoOptionEnabled) {
        
        if(buttonIndex == 0) { //photo lib
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        else if(buttonIndex == 1) { //facebook
            [self validateFromFacebook];
            
//            TMFacebookPhotoPicker *fbPhotoPicker  = [[TMFacebookPhotoPicker alloc] init];
//            fbPhotoPicker.delegate = self;
//            UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:fbPhotoPicker];
//
//            [self presentViewController:navCont animated:YES completion:^{
//                
//                
//            }];
        }
        else if(buttonIndex == 2 && self.isCameraPresent) { //camera
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        }
    }
    else { //update photo
        if(actionSheet.tag == 510001) {
            if(buttonIndex == 2) { //delete photo
                [self deleteSelectedPhoto];
            }
            else if(buttonIndex == 0) {
                [self setSelectedPhotoAsProfilePic];
            }
            else if(buttonIndex == 1) { // edit photo
                [self editSelectedPhoto];
            }
        }
        else if(actionSheet.tag == 510002) {
            if(buttonIndex == 0) { //delete photo
                [self deleteSelectedPhoto];
            }
        }
        else if(actionSheet.tag == 510003) {
            if(buttonIndex == 0) { //edit photo
                [self editSelectedPhoto];
            }
        }
        else if(actionSheet.tag == 510004) {
            if(buttonIndex == 0) { //edit photo
                [self editSelectedPhoto];
            }
            else if(buttonIndex == 1) { //delete photo
                [self deleteSelectedPhoto];
            }
        }
    }
}

-(void)editSelectedPhoto {
    
    TMPhotoCollectionViewCell *cell = (TMPhotoCollectionViewCell*)[self.photoCollectionView cellForItemAtIndexPath:self.selectedIndexPath];
    UIImage *image = cell.profileImgView.image;
    
    //UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.selectedPhoto.fullphotoname]]];
    
    DZNPhotoEditorViewController *editor = [[DZNPhotoEditorViewController alloc] initWithImage:image];
    editor.cropMode = DZNPhotoEditorViewControllerCropModeSquare;
    if(image.size.width > image.size.height) {
        editor.cropMode = DZNPhotoEditorViewControllerCropModeCustom;
        editor.cropSize = CGSizeMake(CGRectGetWidth(self.view.frame), (CGRectGetWidth(self.view.frame)*image.size.height)/image.size.width);
    }
    
    [editor setAcceptBlock:^(DZNPhotoEditorViewController *editor, NSDictionary *userInfo){
        
        UIImage *editImage = [userInfo valueForKey:UIImagePickerControllerEditedImage];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        [self sendPhotoToServerToEdit:editImage];
    }];
    
    [editor setCancelBlock:^(DZNPhotoEditorViewController *editor){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    // The view controller requieres to be nested in a navigation controller
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:editor];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)sendPhotoToServerToEdit:(UIImage*)image {
    __weak TMPhotoViewController *weakSelf = self;
    if(self.photoMgr == nil) {
        self.photoMgr = [[TMPhotoManager alloc] init];
    }
    
    if([self.photoMgr isNetworkReachable]) {
        NSData *editImg = UIImageJPEGRepresentation(image, 1.0);
        
        TMPhotoCollectionViewCell *cell = (TMPhotoCollectionViewCell*)[self.photoCollectionView cellForItemAtIndexPath:self.selectedIndexPath];
        cell.isImageLoding = TRUE;
        cell.isImageUpLoding = FALSE;
    
        self.photoMgr.trackEventDictionary = [self dictForTracking:@"edit_photo"];
        
        NSURLSessionDataTask *task = [self.photoMgr uploadPhotoWithId:self.selectedPhoto.photoid photoData:editImg
                                response:^(TMPhotoResponse *photoResponse, TMError *error) {
                                    self.currentPhotoIndex = 0;
                                    self.photoIndexOffset = 0;
                                    
                                    if(photoResponse!=nil) {
                                        self.photoResponse = photoResponse;
                                        [self updateProfilePhotoUpdateStatus];
                                        [weakSelf.delegate didEditProfile];
                                    }
                                    [self.photoCollectionView reloadData];
                                }];
        [cell showImageUploadProgressLoader:task];
        //[self.photoUploadTask setObject:task forKey:self.selectedIndexPath];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if((alertView.tag == 410001) && (buttonIndex == 1)) {
        __weak TMPhotoViewController *weakSelf = self;
        if(self.photoMgr == nil) {
            self.photoMgr = [[TMPhotoManager alloc] init];
        }
        [self.photoMgr deletePhotoWithId:self.selectedPhoto.photoid
                           photoResponse:^(TMPhotoResponse *photoResponse, TMError *error) {
                               self.currentPhotoIndex = 0;
                               self.photoIndexOffset = 0;
                               if(photoResponse!=nil) {
                                   self.photoResponse = photoResponse;
                                   [weakSelf.delegate didEditProfile];
                               }
                               [self.photoCollectionView reloadData];
                           }];
    }
    else if(alertView.tag == 100000) {
        NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
        if([buttonTitle isEqualToString:@"Ok"]){
            [self requestPhoto];
        }
    }
}

-(void)setSelectedPhotoAsProfilePic {
    
    __weak TMPhotoViewController *weakSelf = self;
    if(self.photoMgr == nil) {
        self.photoMgr = [[TMPhotoManager alloc] init];
    }
    
    [self.photoMgr setPhotoAsProfilePicWithId:self.selectedPhoto.photoid
                                photoResponse:^(TMPhotoResponse *photoResponse, TMError *error) {
                                    weakSelf.currentPhotoIndex = 0;
                                    weakSelf.photoIndexOffset = 0;
                        
                                    if(photoResponse!=nil) {
                                        weakSelf.photoResponse = photoResponse;
                                        [weakSelf updateProfilePhotoUpdateStatus];
                                        [weakSelf.delegate didEditProfile];
                                    }
                                    [weakSelf.photoCollectionView reloadData];
    }];
    
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        /*
         The user wants to use the camera interface. Set up our custom overlay view for the camera.
         */
        imagePickerController.showsCameraControls = YES;
        [[TMAnalytics sharedInstance] trackNetworkEvent:[self dictForTracking:@"take_from_camera_clicked"]];
    }
    else {
        [[TMAnalytics sharedInstance] trackNetworkEvent:[self dictForTracking:@"add_from_computer_clicked"]];
    }
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate
#pragma mark -

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
//    NSData *tmpImg = UIImageJPEGRepresentation(image, 1.0);
//    NSLog(@"Original File size is : %.2f KB",(float)tmpImg.length/1024.0f);
//    
//    NSLog(@"Original File Width is : %f",image.size.width);
//    NSLog(@"Original File Height is : %f",image.size.height);
    
    NSString *source = @"take_from_camera_upload";
    if(picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        source = @"add_from_computer_upload";
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self addPhotoEditorWithImage:image source:source];
    }];

}

-(void)addPhotoEditorWithImage:(UIImage*)image source:(NSString*)source {
    DZNPhotoEditorViewController *editor = [[DZNPhotoEditorViewController alloc] initWithImage:image];
    editor.cropMode = DZNPhotoEditorViewControllerCropModeSquare;
    //editor.cropMode = DZNPhotoEditorViewControllerCropModeCustom;
    if(image.size.width > image.size.height) {
        editor.cropMode = DZNPhotoEditorViewControllerCropModeCustom;
        editor.cropSize = CGSizeMake(CGRectGetWidth(self.view.frame), (CGRectGetWidth(self.view.frame)*image.size.height)/image.size.width);
    }
    [editor setAcceptBlock:^(DZNPhotoEditorViewController *editor, NSDictionary *userInfo){
        
        UIImage *editImage = [userInfo valueForKey:UIImagePickerControllerEditedImage];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self sendImageToServer:editImage source:source];
    }];
    
    [editor setCancelBlock:^(DZNPhotoEditorViewController *editor){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    // The view controller requieres to be nested in a navigation controller
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:editor];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)sendImageToServer:(UIImage*)image source:(NSString*)source
{
    if(self.photoMgr == nil) {
        self.photoMgr = [[TMPhotoManager alloc] init];
    }
    
    self.photoMgr.trackEventDictionary = [self dictForTracking:source];

    TMPhotoCollectionViewCell *cell = (TMPhotoCollectionViewCell*)[self.photoCollectionView cellForItemAtIndexPath:self.selectedIndexPath];
    cell.isImageLoding = FALSE;
    cell.isImageUpLoding = TRUE;
    
    NSData *editImg = UIImageJPEGRepresentation(image, 1.0);
   // NSLog(@"Edited File size is : %.2f KB",(float)editImg.length/1024.0f);
    CGFloat size = (float)editImg.length/1024.0f;
    if(size >= 1024) {
        editImg = UIImageJPEGRepresentation(image, 0.5);
        //NSLog(@"Edited File size after compress is : %.2f KB",(float)editImg.length/1024.0f);
    }
    
    NSURLSessionDataTask *task = [self.photoMgr uploadPhoto:editImg
            response:^(TMPhotoResponse *photoResponse, TMError *error) {
                    
            self.currentPhotoIndex = 0;
            self.photoIndexOffset = 0;
                
            if(photoResponse!=nil) {
                self.photoResponse = photoResponse;
                [self updateProfilePhotoUpdateStatus];
                [self.delegate didEditProfile];
                [self updateProfilePhotoUpdateStatus];
                if ([self.delegate respondsToSelector:@selector(didUploadProfilePicture)]) {
                    [self.delegate didUploadProfilePicture];
                }
            }
            
            [self.photoCollectionView reloadData];
                
            if(self.fromPIDashboard) {
                TMProfileCompletionToast* completionToast = [[TMProfileCompletionToast alloc] init];
                [completionToast setPiActivityCount];
                completionToast = nil;
            }
                
        }];
    [cell showImageUploadProgressLoader:task];
    //[self.photoUploadTask setObject:task forKey:self.selectedIndexPath];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if(self.photoMgr == nil) {
        self.photoMgr = [[TMPhotoManager alloc] init];
    }
    
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        [[TMAnalytics sharedInstance] trackNetworkEvent:[self dictForTracking:@"take_from_camera_cancel"]];
    }
    else if(picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        [[TMAnalytics sharedInstance] trackNetworkEvent:[self dictForTracking:@"add_from_computer_cancel"]];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

////from facebook

-(void)didSelectPhotoWithUrl:(UIImage *)image {
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self addPhotoEditorWithImage:image source:@"fb_photo_save"];
    }];
    
}

#pragma mark - Error Alert Handler
#pragma mark -

-(void)showPhotoResponseErrorAlert:(NSString*)title
                               msg:(NSString*)msg  {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

-(NSMutableDictionary*)dictForTracking:(NSString*)action {
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMPhotoViewController" forKey:@"screenName"];
    [eventDict setObject:@"photo" forKey:@"eventCategory"];
    [eventDict setObject:action forKey:@"eventAction"];
    return eventDict;
}

#pragma mark - TMPhotoAlertViewDelegate methods

- (void) actionButtonPressed
{
    //show options to add photo
    [self showOptionsToAddphoto];
    
    //adding event tracking
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMPhotoAlertView" forKey:@"screenName"];
    [eventDict setObject:@"PHOTOS" forKey:@"eventCategory"];
    [eventDict setObject:@"discovery_popup_upload_now" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}

- (void) cancelButtonPressed
{
    //adding event tracking
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMPhotoAlertView" forKey:@"screenName"];
    [eventDict setObject:@"PHOTOS" forKey:@"eventCategory"];
    [eventDict setObject:@"discovery_popup_later" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    
}

-(void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)addFacebookObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(executeHandle:) name:@"SessionStateChangeNotification" object:nil];
}

-(void)validateFromFacebook {
    [self addFacebookObserver];
    self.isFacebookStateChangedNotificationReceived = FALSE;
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended){
        NSArray *permissions = [FBSession activeSession].permissions;
        if([self checkUserPhotoPermission:permissions]) {
            NSMutableDictionary *queryString = [NSMutableDictionary dictionary];
            NSString *token = [FBSession activeSession].accessTokenData.accessToken;
            queryString[@"connected_from"] = @"photoRegister";
            queryString[@"token"] = token;
            queryString[@"checkFacebook"] = @"check";
            [self validateToken:queryString];
        }else {
            [self showAlert:@"Share your photos with us on Facebook. We'd love to know you better!" caller:@"fbPhoto"];
        }
        
    } else {
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate openActiveSessionWithPermissions:TRUE from:self.identifier];
    }
}

-(void)executeHandle:(NSNotification*)notiication {
    NSDictionary *dict = [notiication userInfo];
    NSString *from = [dict objectForKey:@"from"];
    
    if( ([FBSession activeSession].state == FBSessionStateOpen || [FBSession activeSession].state == FBSessionStateOpenTokenExtended) && ([from isEqualToString:self.identifier]) && (!self.isFacebookStateChangedNotificationReceived) ) {
        ///first update state to receive no more notification
        self.isFacebookStateChangedNotificationReceived = TRUE;
        [self removeObserver];
        ////////
        NSArray *permissions = [FBSession activeSession].permissions;
        
        if([self checkUserPhotoPermission:permissions]) {
            NSMutableDictionary *queryString = [NSMutableDictionary dictionary];
            NSString *token = [FBSession activeSession].accessTokenData.accessToken;
            queryString[@"connected_from"] = @"photoRegister";
            queryString[@"token"] = token;
            queryString[@"checkFacebook"] = @"check";
            [self validateToken:queryString];
        }else {
            [self showAlert:@"Uh oh! You need to allow Facebook to give us access to your photos." caller:@"fbPhoto"];
        }
    }
}


-(BOOL)checkUserPhotoPermission:(NSArray*)permissions {
    for (NSString *permission in permissions) {
        if ([permission isEqualToString:@"user_photos"]) {
            return YES;
        }
    }
    return NO;
}

-(void)showAlert:(NSString*)msg caller:(NSString*)caller {
    NSString *title = @"OK";
    if([caller isEqualToString:@"suspended"]){
        title = @"No";
    }
    else if ([caller isEqualToString:@"fbPhoto"]) {
        title = @"Cancel";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:title otherButtonTitles:nil, nil];
    alertView.tag = 100000;
    
    if([caller isEqualToString:@"fbPhoto"]) {
        [alertView addButtonWithTitle:@"Ok"];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

-(void)requestPhoto{
    [self addFacebookObserver];
    self.isFacebookStateChangedNotificationReceived = FALSE;
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate requestPhotoPermission:self.identifier];
}

-(void)validateToken:(NSDictionary*)paramsDict {
    
    if(self.photoMgr == nil) {
        self.photoMgr = [[TMPhotoManager alloc] init];
    }
    
    if(self.photoMgr.isNetworkReachable) {
        self.view.userInteractionEnabled = false;
        [self configureLoaderForLoginValidationRequestStart];
    
        [self.photoMgr verifyFacebook:paramsDict with:^(BOOL status, TMError *error) {
            self.view.userInteractionEnabled = true;
            [self hideActivityIndicatorViewWithtranslucentView];
            if(status){
                if(self.photoMgr) {
                    self.fbPhotoPicker  = [[TMFacebookPhotoPicker alloc] init];
                    self.fbPhotoPicker.delegate = self;
                    UINavigationController *navCont = [[UINavigationController alloc] initWithRootViewController:self.fbPhotoPicker];
                    [self presentViewController:navCont animated:YES completion:^{    }];
                }
            }else {
                [[FBSession activeSession] closeAndClearTokenInformation];
                [self handleErrorResponse:error];
            }
        }];
    }
    else {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG caller:@""];
    }
}

-(void)configureLoaderForLoginValidationRequestStart {
    if(self.isRetryViewShown) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading ..."];
}

-(void)handleErrorResponse:(TMError*)error {
    if(error.errorCode == TMERRORCODE_LOGOUT) {
        [self.actionDelegate setNavigationFlowForLogoutAction:false];
    }
    else if(error.errorCode == TMERRORCODE_NONETWORK) {
        [self showAlert:TM_INTERNET_NOTAVAILABLE_MSG caller:@""];
    }
    else if(error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
        [self showAlert:TM_SERVER_TIMEOUT_MSG caller:@""];
    }
    else if(error.errorCode == TMERRORCODE_DATASAVEERROR) {
        [self showAlert:error.errorMessage caller:@""];
    }
    else {
        [self showAlert:TM_UNKNOWN_MSG caller:@""];
    }
}

-(void)showProfileRejectedToast
{
    BOOL isFemaleUser = ([[TMUserSession sharedInstance].user isUserFemale])?YES:NO;
    
    if (isFemaleUser && self.alertMessage && self.isFemaleProfileRejectionToastToBeShown) {
        //self.isFemaleProfileRejectionToastToBeShown = false;
        //show the toast view from bottom
        //[TMToastView showToastInParentView:self.photoCollectionView withText:self.alertMessage withDuaration:4 presentationDirection:TMToastPresentationFromBottom];
    }
}

@end

//
//  TMPhotoImageView.m
//  TrulyMadly
//
//  Created by Ankit on 21/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPhotoImageView.h"
#import "UIImageView+AFNetworking.h"


@implementation TMPhotoImageView


-(void)setImageWithURLAndProgressbar:(NSString*)imageUrl {
    NSURL *url = [NSURL URLWithString:imageUrl];
    [self setImageWithURL:url];
}
@end

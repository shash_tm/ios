//
//  TMPhotoCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMPhoto;

@interface TMPhotoCollectionViewCell : UICollectionViewCell

@property(nonatomic,assign)BOOL isImageLoding;
@property(nonatomic,assign)BOOL isImageUpLoding;
@property(nonatomic,assign)BOOL isImageRejected;
@property(nonatomic,strong)UIProgressView *progressView;
@property(nonatomic,strong)UIImageView *profileImgView;
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;

-(void)setPhotoData:(TMPhoto*)photo isProfilePhotoContainer:(BOOL)profilePhotoContainer;
-(void)showImageUploadProgressLoader:(NSURLSessionDataTask*)task;
-(void)setPhotoUploadTask:(NSURLSessionDataTask*)task;


@end

//
//  TMPhotoFlowLayout.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMPhotoFlowLayoutDataSource;

@interface TMPhotoFlowLayout : UICollectionViewLayout

@property (nonatomic, weak) id<TMPhotoFlowLayoutDataSource> dataSource;
@property (nonatomic, assign) UIOffset itemOffset;
@property (nonatomic, assign) CGFloat maxWidth;
@property (nonatomic, assign) CGFloat horizontalSideMargin;
@property (nonatomic, assign) CGFloat verticalSideMargin;
@property (nonatomic, assign) CGFloat footerHeight;

//added as a hack
-(CGSize)collectionViewContentSize;

@end

@protocol TMPhotoFlowLayoutDataSource <NSObject>

-(CGSize)contentSizeForIndex:(NSInteger)index;

@end
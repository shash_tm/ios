//
//  TMPhotoGuideLinesViewController.m
//  TrulyMadly
//
//  Created by Ankit on 16/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPhotoGuideLinesViewController.h"

@implementation TMPhotoGuideLinesViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect rect = CGRectMake(0, 5,
                             self.view.frame.size.width,
                             self.view.frame.size.height-10);
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:rect];

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:rect];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    //for 4sPhotoGuidelines4s
    //for 4s>PhotoGuidelines
    NSString *imgName = NULL;
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    if(screenBounds.size.height > 420) {
        imgName = @"PhotoGuidelines.jpg";
    }
    else {
        imgName = @"PhotoGuidelines4s.jpg";
    }
    
    imgView.image = [UIImage imageNamed:imgName];
    [scrollView addSubview:imgView];
    [self.view addSubview:scrollView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void)tapAction {
    [self dismissViewControllerAnimated:true completion:nil];
}
@end

//
//  TMAnimationView.h
//  TrulyMadly
//
//  Created by Ankit on 27/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAnimationView : UIView

-(void)showHasLikedBeforeSparkAnimationOnBaseView:(UIView*)baseview pointerYPos:(CGFloat)finalYPos;
-(void)showEventsTutorialOnBaseView:(UIView*)baseview;
-(void)showMutualLikeTutorialOnBaseView:(UIView*)baseview;
-(UIView*)getMatchActionTutorialPointerView;

@end

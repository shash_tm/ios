//
//  TMFavContentCellCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit Jain on 27/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMFavContentCellCollectionViewCell : UICollectionViewCell

-(void)setFavItemText:(NSString*)itemText isCommonFavourite:(BOOL)commonFavourite;
-(void)enableUserInteraction;

@end

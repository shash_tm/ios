//
//  TMEndorseListView.h
//  TrulyMadly
//
//  Created by Ankit on 17/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMEndorseListView : UIView

-(void)loadDataWithContent:(NSArray*)content;

-(void)setContentXPosInset:(CGFloat)xPosInset;

@end

//
//  TMMatchesViewController.h
//  TrulyMadly
//
//  Created by Ankit Jain on 10/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMProfileViewController.h"

@class TMNotificationReciever;

@interface TMMatchesViewController : TMProfileViewController

-(void)navigateToViewControllerWithType:(TMViewControllerType)viewControllerType
                               animated:(BOOL)animated
                   notificationReceiver:(TMNotificationReciever*)notificationReceiver;
-(void)clearMatchCacheData;

@end

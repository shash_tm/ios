//
//  TMLikeVIew.m
//  TrulyMadly
//
//  Created by Ankit Jain on 25/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMLikeView.h"
#import "TMProfileInterest.h"


@interface TMLikeView ()

@property(nonatomic,assign)BOOL addUserInteractionEnabled;
@property(nonatomic,strong)UIImageView *cancelImgView;

@end

@implementation TMLikeView

-(instancetype)initWithFrame:(CGRect)frame withInterst:(TMProfileInterest*)interest {
    
    self = [super initWithFrame:frame];
    
    if(self) {
        
        CGFloat labelXPos = [self labelXPos];
        CGFloat labelHeight = 30;
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(labelXPos,
                                                                (CGRectGetHeight(frame) - labelHeight)/2,
                                                                self.bounds.size.width - (2*labelXPos),
                                                                labelHeight)];
        lb.font = [interest fontForText];
        lb.textColor = [UIColor blackColor];
        lb.backgroundColor = [UIColor clearColor];
        lb.tag = 51000;
        lb.text = interest.tag;
        [self addSubview:lb];
        
        self.cancelImgView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width-15,
                                                                           (frame.size.height - 8)/2,
                                                                           8,
                                                                           8)];
        self.cancelImgView.backgroundColor = [UIColor clearColor];
        self.cancelImgView.image = [UIImage imageNamed:@"remove"];
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame withTitleText:(NSString*)text {
    
    self = [super initWithFrame:frame];
    
    if(self) {
        
        CGFloat labelHeight = 30;
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10,
                                                                (CGRectGetHeight(frame) - labelHeight)/2,
                                                                self.bounds.size.width-20, labelHeight)];
        lb.font = [UIFont systemFontOfSize:14];
        lb.textColor = [UIColor blackColor];
        lb.backgroundColor = [UIColor clearColor];
        lb.tag = 51000;
        lb.text = text;
        [self addSubview:lb];

    }
    return self;
}

-(CGFloat)labelXPos {
    return 12;
}

-(void)layoutSubviews {
    CGFloat xPos = [self labelXPos];
    if(self.addUserInteractionEnabled) {
        xPos = 10;
    }
    UILabel *lb = (UILabel*)[self viewWithTag:51000];
    lb.frame = CGRectMake(xPos, lb.frame.origin.y, self.bounds.size.width-(2*xPos), lb.frame.size.height);
    
    
    self.cancelImgView.frame = CGRectMake(self.bounds.size.width-15,
                                    (self.bounds.size.height - self.cancelImgView.frame.size.height)/2,
                                    self.cancelImgView.frame.size.width,
                                    self.cancelImgView.frame.size.height);
}

-(void)enableUserInteraction {
    self.addUserInteractionEnabled = true;
    [self addSubview:self.cancelImgView];
}

-(void)setInterst:(NSString *)interst {
    _interst = interst;
    
    UILabel *lb = (UILabel*)[self viewWithTag:51000];
    lb.text = interst;
}

-(void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    UILabel *lb = (UILabel*)[self viewWithTag:51000];
    lb.textColor = textColor;
}

-(void)setTextFont:(UIFont*)textFont {
    UILabel *lb = (UILabel*)[self viewWithTag:51000];
    lb.font = textFont;
}

@end

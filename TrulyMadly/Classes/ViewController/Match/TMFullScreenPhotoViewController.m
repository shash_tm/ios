//
//  TMFullScreenPhotoViewController.m
//  TrulyMadly
//
//  Created by Ankit on 11/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFullScreenPhotoViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+TMColorAdditions.h"
#import "TMBlockUserViewController.h"
#import "TMProfileImageView.h"
#import "NSString+TMAdditions.h"


#define VIEW_FOR_ZOOM_TAG (1)

@interface TMFullScreenPhotoViewController ()<UIScrollViewDelegate,TMBlockUserViewControllerDelegate>

@property(nonatomic,strong)UIScrollView *imgScrollView;
@property(nonatomic,strong)UIPageControl *pageControl;
@property(nonatomic,assign)NSInteger count;
@property(nonatomic,assign)BOOL addReportUserOption;
@property(nonatomic,assign)BOOL isDealProfile;
@property(nonatomic,assign)BOOL isDealMenu;

@end

@implementation TMFullScreenPhotoViewController

-(instancetype)initWithReportUserOption:(BOOL)reportUserOption {
    self = [super init];
    if(self) {
        self.addReportUserOption = reportUserOption;
    }
    return self;
}

-(instancetype)initWithDealProfile:(BOOL)isDealProfile {
    self = [super init];
    if(self) {
        self.isDealProfile = isDealProfile;
    }
    return self;
}

-(instancetype)initWithDealMenuAndProfile:(BOOL)isDealProfile isDealMenu:(BOOL)isDealMenu {
    self = [super init];
    if(self) {
        self.isDealProfile = isDealProfile;
        self.isDealMenu = isDealMenu;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(self.isDealMenu) {
        self.title = @"MENU";
    }else {
        self.title = @"PHOTO GALLERY";
    }
    self.view.backgroundColor = [UIColor whiteColor];
    self.imgScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.imgScrollView.backgroundColor = [UIColor clearColor];
    self.imgScrollView.pagingEnabled = YES;
    self.imgScrollView.delegate = self;
    [self.view addSubview:self.imgScrollView];
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-100)/2,
                                                                       10,
                                                                       100,
                                                                       20)];
    self.pageControl.backgroundColor = [UIColor clearColor];
    self.pageControl.pageIndicatorTintColor = [UIColor inactivePageIndexColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    NSInteger noOfImages = (self.count>1) ? self.count : 0;
    self.pageControl.numberOfPages = noOfImages;
    [self.view addSubview:self.pageControl];
    
    if(self.addReportUserOption) {
        [self configureReportUserNavigationItem];
    }
}

-(void)configureReportUserNavigationItem {
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
                                           initWithTitle:@"Report" style:UIBarButtonItemStylePlain target:self action:@selector(reportUserAction)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reportUserAction {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to report this photo" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
        //confirm action
        NSArray *blockReasons = [NSArray arrayWithObjects:@"The picture is obscene/inappropriate",@"This  picture does not belong to the user", nil];
        NSArray *blockFlags = [NSArray arrayWithObjects:@"0",@"0", nil];
        TMBlockUserViewController *blockViewControlller = [[TMBlockUserViewController alloc] initWithBlockReason:blockReasons blockFlags:blockFlags];
        blockViewControlller.delegate = self;
        //[blockViewControlller setHeaderTitle:@"Help Us Understand What's Happening"];
        blockViewControlller.loadingText = @"Reporting User Photo...";
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:blockViewControlller];
        
        [self.navigationController presentViewController:navCon animated:true completion:nil];
    }
}

-(void)blockUserWithReason:(NSString*)reasonText {
    [self performSelector:@selector(transitionForReportUserPhotoWithReason:) withObject:reasonText afterDelay:1];
}

-(void)transitionForReportUserPhotoWithReason:(NSString*)reasonText {
    TMBlockUserViewController *blockViewControlller = (TMBlockUserViewController*)[self.navigationController presentedViewController];
    [blockViewControlller dismissViewControllerAnimated:false completion:nil];
    
    //////
    [self.delegate didReportUserPhotoWithReason:reasonText];
}

/////match profile images
-(void)loadImgArray:(NSArray*)images {
    self.count = images.count;
   CGSize size = CGSizeMake((self.view.frame.size.width*images.count),
                             self.view.frame.size.height);
    self.imgScrollView.contentSize = size;
    
    CGFloat xPos = 0;
    for (int i=0; i<images.count;i++) {
        
        TMProfileImageView *imgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                             30,
                                                                        self.view.frame.size.width,
                                                                        self.view.frame.size.height-130)];
        [imgView setImageContentMode:UIViewContentModeScaleAspectFit];
        imgView.clearCache = FALSE;
        NSString *imgUrlString = [images objectAtIndex:i];
        NSString *lastPathComponent = [imgUrlString getLastPathComponentFromURLString];
        [imgView setImageFromURLString:imgUrlString];
        [imgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:TRUE];
        imgView.clearCacheAtPath = TRUE;
        [self.imgScrollView addSubview:imgView];
        
        xPos = xPos + self.view.frame.size.width;
    }
    self.imgScrollView.contentOffset = CGPointMake(self.view.frame.size.width*self.currentIndex, 0);
    self.pageControl.currentPage = self.currentIndex;
    self.imgScrollView.contentSize = CGSizeMake(self.view.frame.size.width*images.count, self.view.frame.size.height-130);
}

///////////////for date spot images
-(void)loadDateSpotImagesFromArray:(NSArray*)images {
    self.count = images.count;
    CGSize size = CGSizeMake((self.view.frame.size.width*images.count),
                             self.view.frame.size.height);
    self.imgScrollView.contentSize = size;
    
    CGFloat xPos = 0;
    for (int i=0; i<images.count;i++) {
        
        TMProfileImageView *imgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                                           30,
                                                                                           self.view.frame.size.width,
                                                                                           self.view.frame.size.height-130)];
        imgView.clearCache = FALSE;
        NSString *imgUrlString = [images objectAtIndex:i];
        [imgView setImageFromURLString:imgUrlString isDeal:false];
        [imgView setImageContentMode:UIViewContentModeScaleAspectFit];
        [self.imgScrollView addSubview:imgView];
        
        xPos = xPos + self.view.frame.size.width;
    }
    self.imgScrollView.contentOffset = CGPointMake(self.view.frame.size.width*self.currentIndex, 0);
    self.pageControl.currentPage = self.currentIndex;
    self.imgScrollView.contentSize = CGSizeMake(self.view.frame.size.width*images.count, self.view.frame.size.height-130);
}


////////for menu images in deal
-(void)loadImgArrayWithImages:(NSArray *)images
{
    self.count = images.count;
    CGSize size = CGSizeMake((self.view.frame.size.width*images.count),
                             self.view.frame.size.height);
    self.imgScrollView.contentSize = size;
    
    CGFloat xPos = 0;
    
    CGRect innerScrollFrame = self.view.frame;//self.imgScrollView.bounds;
    
    for (int i=0; i<images.count;i++) {
        
        TMProfileImageView *imgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                                           30,
                                                                                           self.view.frame.size.width,
                                                                                           self.view.frame.size.height-130)];
        imgView.clearCache = FALSE;
        imgView.tag = VIEW_FOR_ZOOM_TAG;
        NSString *imgUrlString = [images objectAtIndex:i];
        [imgView setImageFromURLString:imgUrlString isDeal:false];
        [imgView setImageContentMode:UIViewContentModeScaleAspectFit];
        
        UIScrollView *pageScrollView = [[UIScrollView alloc]
                                        initWithFrame:innerScrollFrame];
        pageScrollView.minimumZoomScale = 1.0f;
        pageScrollView.maximumZoomScale = 2.0f;
        pageScrollView.zoomScale = 1.0f;
        pageScrollView.contentSize = imgView.bounds.size;
        pageScrollView.delegate = self;
        pageScrollView.showsHorizontalScrollIndicator = NO;
        pageScrollView.showsVerticalScrollIndicator = NO;
        [pageScrollView addSubview:imgView];
        
        [self.imgScrollView addSubview:pageScrollView];
        
        if (i < images.count-1) {
            innerScrollFrame.origin.x += innerScrollFrame.size.width;
        }

        [pageScrollView addSubview:imgView];
        
        //xPos = xPos + self.view.frame.size.width;
    }
    self.imgScrollView.contentOffset = CGPointMake(self.view.frame.size.width*self.currentIndex, 0);
    self.pageControl.currentPage = self.currentIndex;
    self.imgScrollView.contentSize = CGSizeMake(self.view.frame.size.width*images.count, self.view.frame.size.height-130);
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return [scrollView viewWithTag:VIEW_FOR_ZOOM_TAG];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.imgScrollView.frame.size.width;
    float fractionalPage = self.imgScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}

- (void)dealloc
{
    self.imgScrollView.delegate = nil;
}

@end

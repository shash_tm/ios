//
//  TMEndorseListView.m
//  TrulyMadly
//
//  Created by Ankit on 17/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMEndorseListView.h"
#import "TMLinearFlowLayout.h"
#import "TMEndorseContentCollectionViewCell.h"
#import "TMSwiftHeader.h"
#import "TMStringUtil.h"


@interface  TMEndorseListView()<UICollectionViewDataSource,UICollectionViewDelegate,TMLinearFlowLayoutDataSource>

@property(nonatomic,strong)UICollectionView *endorseCollectionView;
@property(nonatomic,strong)NSMutableArray *content;

@end


@implementation TMEndorseListView

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if(self) {
        
        self.content = [NSMutableArray arrayWithCapacity:16];
        
        TMLinearFlowLayout *flowLayout = [[TMLinearFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.dataSource = self;
        flowLayout.itemOffset = UIOffsetMake(5, 0);
        
        CGRect rect = CGRectMake(0,
                                 0,
                                 self.frame.size.width,
                                 self.frame.size.height);
        self.endorseCollectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
        self.endorseCollectionView.backgroundColor = [UIColor clearColor];
        self.endorseCollectionView.dataSource = self;
        self.endorseCollectionView.delegate = self;
        self.endorseCollectionView.showsHorizontalScrollIndicator = NO;
        
        [self addSubview:self.endorseCollectionView];
        
        [self.endorseCollectionView registerClass:[TMEndorseContentCollectionViewCell class] forCellWithReuseIdentifier:@"endorse"];
    }
    return self;
}

-(void)setContentXPosInset:(CGFloat)xPosInset {
    self.endorseCollectionView.contentInset = UIEdgeInsetsMake(0, xPosInset, 0, 0);
}
-(void)loadDataWithContent:(NSArray*)content {
    [self.content removeAllObjects];
    
    [self.content addObjectsFromArray:content];
    [self.endorseCollectionView reloadData];
}


#pragma mark TMLinearFlowLayoutDataSource datasource -

-(CGSize)contentSizeForIndex:(NSInteger)index {
    CGSize size = CGSizeMake(80, 90);
    return size;
}


#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.content.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"endorse";
    TMEndorseContentCollectionViewCell *cell = [collectionView
                                                dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                                forIndexPath:indexPath];
    
    NSDictionary *dict = [self.content objectAtIndex:indexPath.row];
    TMEndorsement *endorsement = [[TMEndorsement alloc] initWithEndorsementDataDict:dict];
    NSURL *imgUrl = [NSURL URLWithString:endorsement.imageUrl];
    [cell setProfileImage:imgUrl];
    cell.name.text = endorsement.fName;
    cell.backgroundColor = [UIColor clearColor];
    return  cell;
    
}

-(CGSize)nameTextSize:(NSString*)nameText {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:8];
    [dict setObject:[UIFont systemFontOfSize:12] forKey:@"kfont"];
    [dict setObject:[NSNumber numberWithFloat:100] forKey:@"kmaxwidth"];
    [dict setObject:[NSString stringWithFormat:@"%@",nameText] forKey:@"ktext"];
    CGSize size = [TMStringUtil textRectForString:dict].size;
    return size;
}

@end

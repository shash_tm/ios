//
//  TMMatchActioNView.h
//  TrulyMadly
//
//  Created by Ankit on 10/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@protocol TMMatchActionViewDelegate;

@interface TMMatchActionView : UIView <UIAlertViewDelegate>

@property(nonatomic,weak)id<TMMatchActionViewDelegate> delegate;

@property (nonatomic, strong) UIAlertView* alertView;

-(void)addSelecQuizActionBar;
-(void)disableSparkButton;
-(void)enableSparkButton;
-(void)disableLikeButton;
-(void)enableLikeButton;
-(void)updateActionButtonsWithSparkStatus:(BOOL)sparkStatus;
-(void)showHaslikedBeforeAnimation;
//-(void)animateSelectQuizLikeIconWithCompletion:(void(^)(void))completionBlock;
//-(void)animateSelectQuizHideIconWithCompletion:(void(^)(void))completionBlock;
//-(void)setSelectQuizHideButtonAnswerStatus:(BOOL)isAnswered;
//-(void)setSelectQuizLikeButtonAnswerStatus:(BOOL)isAnswered;
-(void)animateSelectQuizActionView:(TMSelectQuizActionType)selectQuizAction withCompletion:(void(^)(void))completionBlock;
-(void)setSelectQuizAction:(TMSelectQuizActionType)selectQuizAction;
-(void)setSelectQuizActionViewToDefault;

@end


@protocol TMMatchActionViewDelegate <NSObject>

-(void)didPeformLikeAction;

-(void)didPeformHideAction;

@optional
//this delegate called when non authen user takes action on the matches
-(void)peformLikeActionWithAlert;

-(void)perfromProfileRejectionAlertDisplay;

-(void)didPerformSparkAction;

@end


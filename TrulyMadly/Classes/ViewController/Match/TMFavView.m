//
//  TMFavView.m
//  TrulyMadly
//
//  Created by Ankit Jain on 24/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFavView.h"
#import "TMFavouriteItem.h"

@interface TMFavView ()

@property(nonatomic,strong)TMFavouriteItem *favItem;
@property(nonatomic,strong)UIView *underlineView;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)BOOL isActive;

@end

@implementation TMFavView

-(instancetype)initWithFrame:(CGRect)frame withUserInteractionEnabled:(BOOL)status index:(NSInteger)index{
    self = [super initWithFrame:frame];
    if(self) {
        
        self.index = index;
        
        CGFloat width = 32;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(frame) - width)/2,
                                                                             (CGRectGetHeight(frame) - width)/2,
                                                                             width, width)];
        imgView.tag = 5001;
        imgView.backgroundColor = [UIColor clearColor];
        [self addSubview:imgView];
        
        self.underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-2, CGRectGetWidth(self.frame), 2)];
        [self addSubview:self.underlineView];
        
        if(status) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = self.bounds;
            [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
        }
    }
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    self.underlineView.frame = CGRectMake(0, CGRectGetHeight(self.frame)-2, CGRectGetWidth(self.frame), 2);
}
-(void)buttonAction:(UIButton*)btn {
    
    if(!self.isActive) {
        self.isActive = true;
        if(self.delegate) {
            [self.delegate makeActiveWithIndex:self.index];
        }
    }
    
}

-(void)setActive {
    self.isActive = true;
    [self updateUnderLineViewForActiveStatus:TRUE];
    UIImageView *imgView = (UIImageView*)[self viewWithTag:5001];
    imgView.image = [UIImage imageNamed: self.favItem.activeImageStr];
    
}

-(void)setInActive {
    self.isActive = false;
    [self updateUnderLineViewForActiveStatus:FALSE];
    UIImageView *imgView = (UIImageView*)[self viewWithTag:5001];
    if(self.isCommon) {
        imgView.image = [UIImage imageNamed: self.favItem.commonImageStr];
    }else {
        imgView.image = [UIImage imageNamed: self.favItem.inActiveImageStr];
    }
}

-(void)setCommon:(TMFavouriteItem*)favItem isActive:(BOOL)isActive{
    
    self.isActive = isActive;
    self.isCommon = true;
    self.favItem = favItem;
    UIImageView *imgView = (UIImageView*)[self viewWithTag:5001];
    imgView.image = [UIImage imageNamed:self.favItem.commonImageStr];
}

-(void)setImage:(TMFavouriteItem*)favItem isActive:(BOOL)isActive {

    self.isActive = isActive;
    
    self.favItem = favItem;
    
    UIImageView *imgView = (UIImageView*)[self viewWithTag:5001];
    NSString *imgStr = NULL;
    
    if(!favItem.status) {
        imgStr = favItem.disabledImageStr;
    }
    else {
        if(isActive) {
            imgStr = favItem.activeImageStr;
        }
        else {
            imgStr = favItem.inActiveImageStr;
        }
    }
    
    imgView.image = [UIImage imageNamed:imgStr];
    
    [self updateUnderLineViewForActiveStatus:isActive];
}

-(void)updateUnderLineViewForActiveStatus:(BOOL)activeStatus {
    if(activeStatus == TRUE) {
        self.underlineView.backgroundColor = [UIColor blackColor];
    }
    else {
        self.underlineView.backgroundColor = [UIColor clearColor];
    }
}

@end


//
//  TMLikeVIew.h
//  TrulyMadly
//
//  Created by Ankit Jain on 25/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMProfileInterest;

@interface TMLikeView : UIView

@property(nonatomic,strong)NSString *interst;
@property(nonatomic,strong)UIColor *textColor;
@property(nonatomic,assign)CGPoint offset;

-(instancetype)initWithFrame:(CGRect)frame withInterst:(TMProfileInterest*)interest;
-(instancetype)initWithFrame:(CGRect)frame withTitleText:(NSString*)text;
-(void)setTextFont:(UIFont*)textFont;
-(void)enableUserInteraction;


@end

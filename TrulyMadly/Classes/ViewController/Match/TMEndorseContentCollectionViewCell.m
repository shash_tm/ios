//
//  TMEndorseContentCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 17/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMEndorseContentCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TMStringUtil.h"


@interface TMEndorseContentCollectionViewCell ()
@property(nonatomic,strong)UIImageView *imgView;


@end


@implementation TMEndorseContentCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width-60)/2,
                                                                     0,
                                                                     60,
                                                                     60)];
        self.imgView.backgroundColor = [UIColor  clearColor];
        self.imgView.layer.cornerRadius = self.imgView.frame.size.width/2;
        self.imgView.layer.masksToBounds = YES;
        self.imgView.clipsToBounds = true;
        [self.contentView addSubview:self.imgView];
        
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                              60,
                                                              80,
                                                              30)];
        self.name.font = [UIFont systemFontOfSize:12];
        self.name.numberOfLines = 0;
        self.name.backgroundColor =[UIColor clearColor];
        self.name.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.name];
    }
    return self;
}

-(void)setProfileImage:(NSURL*)imgUrl {
    [self.imgView setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:@"ProfileBlank"]];
}


@end

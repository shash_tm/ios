//
//  TMFavListView.h
//  TrulyMadly
//
//  Created by Ankit Jain on 27/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMFavTag;

typedef enum {
    FavListType_Match,
    FavListType_Edit
}TMFavListType;

@protocol TMFavListViewDelegate;

@interface TMFavListView : UIView

@property(nonatomic,strong)UICollectionView *favCollectionView;
@property(nonatomic,strong)UIColor *color;
@property(nonatomic,strong)NSArray *commonFavorites;
@property(nonatomic,weak)id<TMFavListViewDelegate> delegate;

-(instancetype)initWithFrame:(CGRect)frame withType:(TMFavListType)type;
-(void)loadDataWithContent:(NSArray*)content;

-(void)enableUserInteraction;

@end


@protocol TMFavListViewDelegate <NSObject>

-(void)didSelectItemAtIndex:(NSString*)str;
-(void)didSelecFavAtIndex:(TMFavTag*)tag;
@end

//
//  TMMatchActioNView.m
//  TrulyMadly
//
//  Created by Ankit on 10/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMatchActionView.h"
#import "TMSwiftHeader.h"
#import "TMPhotoAlertView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMUserSession.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
#import "TMAnimationView.h"

#define MATCHACTIONVIEW_BASETAG  5000

@interface TMMatchActionView()

@property(nonatomic,strong)CAGradientLayer *gradient;
@property(nonatomic,strong)FLAnimatedImageView *flipAnimationImageView;

@end

@implementation TMMatchActionView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
       
    }
    
    return self;
}
-(void)addSelecQuizActionBar
{
    [self setupGradient];
    [self addSelectQuizActionButtons];
}
-(void)setupGradient {
    if (!self.gradient.superlayer) {
        self.gradient = [CAGradientLayer layer];
        self.gradient.frame = self.bounds;
        self.gradient.colors = @[(id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0] CGColor],
                                 (id)[[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8] CGColor]];
        self.gradient.locations = @[@0.2f, @1.0f];
        [self.layer insertSublayer:self.gradient atIndex:0];
    }
}
-(FLAnimatedImageView*)flipAnimationImageView {
    if(!_flipAnimationImageView) {
        _flipAnimationImageView = [[FLAnimatedImageView alloc] init];
        _flipAnimationImageView.contentMode = UIViewContentModeScaleAspectFill;
        _flipAnimationImageView.clipsToBounds = YES;
         UIView *sparkView = [self viewWithTag:5001];
        [sparkView addSubview:self.flipAnimationImageView];
    }
    return _flipAnimationImageView;
}
-(void)disableSparkButton {
    UIView *sparkView = [self viewWithTag:5001];
    sparkView.backgroundColor = [UIColor grayColor];
    sparkView.alpha = 0.5;
    sparkView.userInteractionEnabled = FALSE;
}
-(void)enableSparkButton {
    UIView *sparkView = [self viewWithTag:5001];
    sparkView.backgroundColor = [UIColor sparkColor];
    sparkView.alpha = 1.0;
    sparkView.userInteractionEnabled = TRUE;
}
-(void)disableLikeButton {
    UIView *sparkView = [self viewWithTag:5002];
    sparkView.alpha = 0.2;
}
-(void)enableLikeButton {
    UIView *sparkView = [self viewWithTag:5002];
    sparkView.alpha = 1.0;
}
-(void)updateActionButtonsWithSparkStatus:(BOOL)sparkStatus {
    NSArray *subviews = [self subviews];
    if(subviews.count != 3) {//if spark button not added add spark button from user flag status
        for (UIView *view in subviews) {
            [view removeFromSuperview];
        }
        [self addMatchActionButtons];
    }
    
    if(!sparkStatus) {
        [self disableSparkButton];
    }
}
-(void)animateSelectQuizActionView:(TMSelectQuizActionType)selectQuizAction withCompletion:(void(^)(void))completionBlock; {
    [self updateSelectQuizYesActionViewAsDefault];
    [self updateSelectQuizNoActionViewAsDefault];
    
    UIView *yesView;
    if(selectQuizAction == SelectQuizActionYes) {
        [self updateSelectQuizYesActionViewAsSelected];
        yesView = [self viewWithTag:5001];
    }
    else if(selectQuizAction == SelectQuizActionNo) {
        [self updateSelectQuizNoActionViewAsSelected];
        yesView = [self viewWithTag:5000];
    }
    
    CGFloat width = 80;
    CGFloat yPos = (self.bounds.size.height-width)/2;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         yesView.frame = CGRectMake(CGRectGetMinX(yesView.frame),
                                                    yPos,
                                                    width, width);
                     }
                     completion:^(BOOL finished){
                         completionBlock();
                         if(selectQuizAction == SelectQuizActionYes) {
                             [self updateSelectQuizYesActionViewAsDefault];
                         }
                         else if(selectQuizAction == SelectQuizActionNo) {
                             [self updateSelectQuizNoActionViewAsDefault];
                         }
                     }];
}
-(void)setSelectQuizAction:(TMSelectQuizActionType)selectQuizAction {
    [self updateSelectQuizNoActionViewAsDefault];
    [self updateSelectQuizYesActionViewAsDefault];
    
    if(selectQuizAction == SelectQuizActionYes) {
        [self updateSelectQuizYesActionViewAsSelected];
    }
    else if(selectQuizAction == SelectQuizActionNo) {
        [self updateSelectQuizNoActionViewAsSelected];
    }
}
-(void)setSelectQuizActionViewToDefault {
    [self updateSelectQuizNoActionViewAsDefault];
    [self updateSelectQuizYesActionViewAsDefault];
}
-(void)updateSelectQuizNoActionViewAsDefault {
    CGFloat width = 70;
    UIView *yesView = [self viewWithTag:5000];
    UIImageView *imageView = [[yesView subviews] objectAtIndex:0];
    yesView.frame = CGRectMake(CGRectGetMinX(yesView.frame),
                               CGRectGetMinY(yesView.frame),
                               width, width);
    yesView.backgroundColor = [UIColor hideColor];
    imageView.image = [UIImage imageNamed:@"nope"];
    yesView.layer.cornerRadius = width/2;
    CGFloat iconWidth = 21;
    CGFloat iconHeight = 21;
    CGFloat xPos = (width-(iconWidth))/2;
    imageView.frame = CGRectMake(xPos, (width-iconHeight)/2, iconWidth, iconHeight);
}
-(void)updateSelectQuizNoActionViewAsSelected {
    CGFloat width = 80;
    UIView *yesView = [self viewWithTag:5000];
    UIImageView *imageView = [[yesView subviews] objectAtIndex:0];
    yesView.backgroundColor = [UIColor clearColor];
    imageView.image = [UIImage imageNamed:@"selectquizanswerno"];
    yesView.layer.cornerRadius = width/2;
  
    imageView.frame = yesView.bounds;
}
-(void)updateSelectQuizYesActionViewAsDefault {
    CGFloat width = 70;
    UIView *yesView = [self viewWithTag:5001];
    UIImageView *imageView = [[yesView subviews] objectAtIndex:0];
    yesView.frame = CGRectMake(CGRectGetMinX(yesView.frame),
                               CGRectGetMinY(yesView.frame),
                               width, width);
    yesView.backgroundColor = [UIColor likeColor];
    imageView.image = [UIImage imageNamed:@"like"];
    yesView.layer.cornerRadius = width/2;
    CGFloat iconWidth = 27.6;
    CGFloat iconHeight = 21;
    CGFloat xPos = (width-(iconWidth))/2;
    imageView.frame = CGRectMake(xPos, (width-iconHeight)/2, iconWidth, iconHeight);
}
-(void)updateSelectQuizYesActionViewAsSelected {
    CGFloat width = 80;
    UIView *yesView = [self viewWithTag:5001];
    UIImageView *imageView = [[yesView subviews] objectAtIndex:0];
    yesView.backgroundColor = [UIColor clearColor];
    
    imageView.image = [UIImage imageNamed:@"selectquizyes"];
    yesView.layer.cornerRadius = width/2;
   
    imageView.frame = yesView.bounds;
}
-(void)showHaslikedBeforeAnimation {
    UIView *sparkView = [self viewWithTag:5001];
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"spark_icon_animation" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    self.flipAnimationImageView.frame = sparkView.bounds;
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    self.flipAnimationImageView.animatedImage = animatedImage1;

    __weak __typeof(self)weakSelf = self;
    [self.flipAnimationImageView setLoopCompletionBlock:^(NSUInteger loopCountRemaining) {
        loopCountRemaining = 0;
        if(loopCountRemaining == 0) {
            [weakSelf.flipAnimationImageView removeFromSuperview];
            weakSelf.flipAnimationImageView = nil;
        }
        [weakSelf showAnimation];
    }];
}
-(void)showAnimation {
    UIView *superView = self.superview;
    CGFloat yPos = CGRectGetMinY(self.frame);
    TMAnimationView *animatonView = [[TMAnimationView alloc] initWithFrame:superView.bounds];
    [animatonView showHasLikedBeforeSparkAnimationOnBaseView:superView pointerYPos:yPos];
}
-(void)addMatchActionButtons {
    NSArray *images;
    NSArray *colors;
    NSArray *selectors;
    if([[TMUserSession sharedInstance] isSparkEnabled]) {
        images = @[@"nope",@"spark_icon",@"like"];
        colors = @[[UIColor hideColor],[UIColor sparkColor],[UIColor likeColor]];
        selectors = @[NSStringFromSelector(@selector(dislikeAction)),NSStringFromSelector(@selector(sparkAction)),NSStringFromSelector(@selector(likeAction))];
    }
    else {
        images = @[@"nope",@"like"];
        colors = @[[UIColor hideColor],[UIColor likeColor]];
        selectors = @[NSStringFromSelector(@selector(dislikeAction)),NSStringFromSelector(@selector(likeAction))];
    }
    
    [self createActionViewWithImages:images selector:selectors colors:colors xPosOffset:20];
}
-(void)addSelectQuizActionButtons {
    NSArray *images;
    NSArray *colors;
    NSArray *selectors;
    
    images = @[@"nope",@"like"];
    colors = @[[UIColor hideColor],[UIColor likeColor]];
    selectors = @[NSStringFromSelector(@selector(quizHideAction)),NSStringFromSelector(@selector(quizLikeAction))];
   
    [self createActionViewWithImages:images selector:selectors colors:colors xPosOffset:36];
}
-(void)createActionViewWithImages:(NSArray*)images selector:(NSArray*)selectors colors:(NSArray*)colors xPosOffset:(CGFloat)xOffset {
    NSArray *iconHeights;
    NSArray *iconWidths;
    CGFloat xPos;
    if(images.count == 3) {
        iconHeights = @[@(21),@(42),@(21)];
        iconWidths = @[@(21),@(42),@(27.6)];
        xPos = (self.bounds.size.width-250)/2;
    }
    else {
        iconHeights = @[@(21),@(21)];
        iconWidths = @[@(21),@(27.6)];
        xPos = (self.bounds.size.width-160)/2;
    }
   
    CGFloat width = 70;//cal::(self.bounds.size.width-(xPos*2)-(20*2))/3;
    CGFloat height = width;
    CGFloat yPos = (self.bounds.size.height-width)/2;
    CGFloat iconWidth;
    CGFloat iconHeight;
    for (int i=0; i<images.count; i++) {
        UIView *actionView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
        actionView.layer.cornerRadius = width/2;
        actionView.backgroundColor = colors[i];
        actionView.tag = MATCHACTIONVIEW_BASETAG+i;
        [self addSubview:actionView];
        
        [actionView.layer setShadowColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.75].CGColor];
        [actionView.layer setShadowOpacity:0.5];
        [actionView.layer setShadowRadius:3.0];
        [actionView.layer setShadowOffset:CGSizeMake(0.2, 1.0)];
        
        iconWidth = [iconWidths[i] floatValue];
        iconHeight = [iconHeights[i] floatValue];
        
        xPos = (width-(iconWidth))/2;
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, (height-iconHeight)/2, iconWidth, iconHeight)];
        imgView.image = [UIImage imageNamed:images[i]];
        [actionView addSubview:imgView];
        
        [self bringSubviewToFront:actionView];
        
        SEL sel = NSSelectorFromString(selectors[i]);
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:sel];
        [actionView addGestureRecognizer:tapGesture];
        
        xPos = CGRectGetMaxX(actionView.frame) + xOffset;
    }
}
-(void)likeAction {
  
    BOOL isFemaleRejectedUser = [TMUserSession sharedInstance].user.isFemaleProfileRejected;
    
    if (isFemaleRejectedUser) {
        //show prompt as per the new photo flow
        [self.delegate perfromProfileRejectionAlertDisplay];
    }
    else {
        TMUser *user = [TMUserSession sharedInstance].user;
        TMUserStatus userStatus = user.userStatus;
        
        if((userStatus == NON_AUTHENTIC) && (![user isNonAuthenticUserLikeActionRegistered])) {
            [user registerNonAuthenticUserLikeAction];
            [self.delegate peformLikeActionWithAlert];
        }
        else {
            [self.delegate didPeformLikeAction];
        }
    }
}

-(void)sparkAction {
    [self.delegate didPerformSparkAction];
}

-(void)dislikeAction {
    BOOL isUnlikeActionRegistered = [[TMUserSession sharedInstance].user isUserUnlikeActionRegistered];
    
    if(isUnlikeActionRegistered) {
        [self.delegate didPeformHideAction];
    }
    else {
        self.alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure? This will permanently remove the match from your list" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
        [self.alertView show];
    }
}
-(void)quizLikeAction {
    [self.delegate didPeformLikeAction];
}
-(void)quizHideAction {
    [self.delegate didPeformHideAction];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 1) {
        TMUser *user = [TMUserSession sharedInstance].user;
        [user registerUserUnlikeAction];
        [self.delegate didPeformHideAction];
    }
}

@end

//
//  TMInviteFriendViewController.h
//  TrulyMadly
//
//  Created by Ankit on 27/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMInviteFriendViewControllerDelegate;

@interface TMInviteFriendViewController : UIViewController

@property(nonatomic,weak)id<TMInviteFriendViewControllerDelegate> delegate;

@end


@protocol TMInviteFriendViewControllerDelegate <NSObject>

-(void)didDismissViewController;

@end

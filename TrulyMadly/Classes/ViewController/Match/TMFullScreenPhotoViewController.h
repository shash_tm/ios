//
//  TMFullScreenPhotoViewController.h
//  TrulyMadly
//
//  Created by Ankit on 11/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMFullScreenPhotoViewControllerDelegate;

@interface TMFullScreenPhotoViewController : UIViewController

@property(nonatomic,weak)id<TMFullScreenPhotoViewControllerDelegate> delegate;
@property(nonatomic,assign)NSInteger currentIndex;

-(instancetype)initWithReportUserOption:(BOOL)reportUserOption;
-(instancetype)initWithDealProfile:(BOOL)isDealProfile;
-(instancetype)initWithDealMenuAndProfile:(BOOL)isDealProfile isDealMenu:(BOOL)isDealMenu;
-(void)loadImgArray:(NSArray*)images;
-(void)loadImgArrayWithImages:(NSArray *)images;
-(void)loadDateSpotImagesFromArray:(NSArray*)images;
@end

@protocol TMFullScreenPhotoViewControllerDelegate <NSObject>

-(void)didReportUserPhotoWithReason:(NSString*)reason;

@end
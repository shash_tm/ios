//
//  TMFavContentCellCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit Jain on 27/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFavContentCellCollectionViewCell.h"
#import "TMStringUtil.h"
#import "TMLikeVIew.h"
#import "UIColor+TMColorAdditions.h"

@interface TMFavContentCellCollectionViewCell ()

@property(nonatomic,strong)TMLikeView *view;
@property(nonatomic,strong)UIButton *btn;
@property(nonatomic,strong)UIImageView *cancelImage;


@end

@implementation TMFavContentCellCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.view = [[TMLikeView alloc] initWithFrame:self.bounds withInterst:nil];
        self.view.layer.borderColor = [UIColor commonPropertyColor].CGColor;
        self.view.layer.cornerRadius = CGRectGetHeight(frame)/2;
        self.view.layer.masksToBounds = YES;
        
        [self.contentView addSubview:self.view];
    }
    return self;
}

-(void)enableUserInteraction {
    
    [self.view enableUserInteraction];
}

-(void)layoutSubviews {
    self.view.frame = self.bounds;
}

-(void)setFavItemText:(NSString*)itemText isCommonFavourite:(BOOL)commonFavourite {
    self.view.interst = itemText;
    self.view.textColor = [UIColor blackColor];
    self.view.layer.borderWidth = (commonFavourite) ? 1.5 : 0.5;
    NSString *fontFamily = (commonFavourite) ? @"HelveticaNeue-Medium" : @"HelveticaNeue";
    UIFont *font = [UIFont fontWithName:fontFamily size:14];
    [self.view setTextFont:font];
}

-(void)btnAction {
    //NSLog(@"TMFavContentCellCollectionViewCell --- btnAction");
}

@end

//
//  TMFavView.h
//  TrulyMadly
//
//  Created by Ankit Jain on 24/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMFavouriteItem;

@protocol TMFavViewDelegate;


@interface TMFavView : UIView

@property(nonatomic,weak)id<TMFavViewDelegate> delegate;
@property(nonatomic,strong)NSString *activeImageStr;
@property(nonatomic,strong)NSString *inactiveImageStr;
@property(nonatomic,strong)NSString *disabledImageStr;
@property(nonatomic,assign)BOOL isCommon;


-(instancetype)initWithFrame:(CGRect)frame withUserInteractionEnabled:(BOOL)status index:(NSInteger)index;

-(void)setImage:(TMFavouriteItem*)favItem isActive:(BOOL)isActive;

-(void)setActive;
-(void)setInActive;
-(void)setCommon:(TMFavouriteItem*)favItem isActive:(BOOL)isActive;

@end

@protocol TMFavViewDelegate <NSObject>

-(void)makeActiveWithIndex:(NSInteger)index;

@end
//
//  TMMatchTransitionView.m
//  TrulyMadly
//
//  Created by Ankit on 19/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMatchTransitionView.h"



@implementation TMMatchTransitionView

-(instancetype)initWithFrame:(CGRect)frame withTransitionType:(TMMatchTransitionType)transitionType sparkLeft:(NSString *)sparkLeft {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.72];
        self.alpha = 0;
        CGFloat width = 63; CGFloat height = 63;
        
        NSString *imgName = NULL;
        if(transitionType == TMMATCH_TRANSITION_TYPE_LIKE) {
            imgName = @"like_black";
        }
        else if(transitionType == TMMATCH_TRANSITION_TYPE_SPARK){
            imgName = @"sparkaction";
            width = 103; height = 132;
        }
        else {
            imgName = @"nope_black";
        }
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width-width)/2, (frame.size.height-height)/2, width, height)];
        imgView.image = [UIImage imageNamed:imgName];
        [self addSubview:imgView];
        
        if(transitionType == TMMATCH_TRANSITION_TYPE_SPARK) {
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(imgView.frame)-10, CGRectGetMinY(imgView.frame)-60, 125, 70)];
            lbl.text = sparkLeft;
            lbl.textColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0];
            lbl.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:50];
            [self addSubview:lbl];
        }
    }
    
    return self;
}

@end

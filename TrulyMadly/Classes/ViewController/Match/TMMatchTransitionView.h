//
//  TMMatchTransitionView.h
//  TrulyMadly
//
//  Created by Ankit on 19/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@interface TMMatchTransitionView : UIView

-(instancetype)initWithFrame:(CGRect)frame withTransitionType:(TMMatchTransitionType)transitionType sparkLeft:(NSString *)sparkLeft;

@end

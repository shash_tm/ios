//
//  TMInviteFriendViewController.m
//  TrulyMadly
//
//  Created by Ankit on 27/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMInviteFriendViewController.h"

@interface TMInviteFriendViewController ()

@end

@implementation TMInviteFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgView.image = [UIImage imageNamed:@"inviteoverlay"];
    [self.view addSubview:imgView];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapGesture {
    [self.delegate didDismissViewController];
    
    
}

@end

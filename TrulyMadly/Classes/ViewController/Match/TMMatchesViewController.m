//
//  TMMatchesViewController.m
//  TrulyMadly
//
//  Created by Ankit Jain on 10/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMatchesViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMFullScreenPhotoViewController.h"
#import "TMPhotoViewController.h"
#import "TMMyProfileViewController.h"
#import "TMMessageListViewController.h"
#import "TMSlidingViewController.h"
#import "TMSideMenuViewController.h"
#import "TMMessageProfileViewController.h"
#import "TMInviteFriendViewController.h"
#import "DZNPhotoEditorViewController.h"
#import "TMQuizOverlayViewController.h"
#import "TMDealListViewController.h"
#import "TMNativeAdWebViewController.h"
#import "TMBuySparkViewController.h"
#import "TMSelectQuizResultViewController.h"
#import "TMBuySelectViewController.h"
#import "TMEditPreferenceViewController.h"

#import "TMBadgeView.h"
#import "TMMatchActionView.h"
#import "TMSystemMessageView.h"
#import "TMMatchTransitionView.h"
#import "TMAnimationView.h"
#import "TMPopOverView.h"
#import "TMProfileCompletionToast.h"
#import "TMNudgeScreen.h"
#import "TMActivityDashboard.h"
#import "TMProfileVisibilityView.h"
#import "TMProfileVisibilityAlertView.h"

#import "TMMessagingController.h"
#import "TMMatchDataController.h"
#import "TMNotificationReciever.h"
#import "TMImageDownloader.h"
#import "TMuserFlagManager.h"
#import "TMAdManager.h"
#import "MoEngage.h"

#import "TMConversation.h"
#import "TMSystemMessage.h"
#import "TMProfile.h"
#import "TMNotificationUpdate.h"
#import "TMProfilePrivacy.h"
#import "TMAdTrackingModel.h"
#import "TMAdTrackingManager.h"
#import "TMProfileSelectInfo.h"

#import "TMTimestampFormatter.h"
#import "TMUserSession.h"
#import "TMCommonUtil.h"
#import "TMSwiftHeader.h"
#import "TMLog.h"

#import "TMEventCategoryListViewController.h"
#import "TMEventDetailViewController.h"
#import "TMNotificationHeaders.h"
#import "TMAppViralityController.h"

#import "TMVideoTutorialView.h"
#import "TMSparkPackageBuyView.h"
#import "TMSendSparkView.h"
#import "TMIAPManager.h"
#import "TMError.h"
#import "TMInteractor.h"
#import "TMDismissAnimator.h"
#import "TMSelectQuizViewController.h"

#define TMMATCHACTION_ANIMATIONVIEW_TAG 210001
#define INTERSTITIAL_AD_PLACEMENT_ID 1447639341360

#define trustBuilderFemaleNudge @"trustbuilder_female_nudge"
#define myProfileFemaleNudge @"profile_female_nudge"
#define trustBuilderMaleNudge @"trustbuilder_male_nudge"
#define myProfileMaleNudge @"profile_male_nudge"


@interface TMMatchesViewController () <ECSlidingViewControllerDelegate,TMSystemMessageViewDelegate,TMMyProfileViewControllerDelegate,
UIAlertViewDelegate,TMMatchActionViewDelegate,TMViewControllerActionDelegate,
TMFullScreenPhotoViewControllerDelegate,TMInviteFriendViewControllerDelegate,TMPopOverViewDelegate,
TMNudgeScreenDelegate,TMProfileVisibilityViewDelegate,UIImagePickerControllerDelegate,
UIActionSheetDelegate, TMProfilePrivacyDelegate,
TMProfileVisibilityAlertViewDelegate, MOInAppDelegate, TMVideoTutorialViewDelegate,
TMSparkPackageBuyViewDelegate,TMSideMenuViewControllerDelegate,
TMSendSparkViewDelegate,UIViewControllerTransitioningDelegate,TMBuySparkViewControllerDelegate,TMSelectQuizViewControllerDelegate, TMProfileAdDelegate>


@property(nonatomic,strong)TMMessageListViewController *messageViewController;
@property(nonatomic,strong)UIView *containerView;
@property(nonatomic,strong)UIView *overlayView;
@property(nonatomic,strong)TMPopOverView *popoverView;
@property(nonatomic,strong)TMMatchActionView *matchActionView;
@property(nonatomic,strong)UIAlertView* alertView;
@property(nonatomic,strong)TMProfilePrivacy *profilePrv;
@property(nonatomic,strong)TMProfileVisibilityAlertView *profileAlertView;
@property(nonatomic,strong)TMProfileCompletionToast* completionToast;
@property(nonatomic,strong)TMNudgeScreen* nudgeScreen;
@property(nonatomic,strong)TMNudgeScreen* nudgeDashboardScreen;
@property(nonatomic,strong)TMProfileVisibilityView* profileVisibilityView;
@property(nonatomic,strong)TMActivityDashboard* activityDashboard;
@property(nonatomic,strong)UIActionSheet *actionSheet;

@property(nonatomic,strong)TMMatchDataController *dataController;
@property(nonatomic,strong)TMuserFlagManager *userFlagManager;
@property(nonatomic,strong)TMAdManager* adManager;
@property(nonatomic,strong)TMAdTrackingManager *adTrackerManager;

@property(nonatomic,strong)NSDictionary* nudgeDict;
@property(nonatomic,strong)NSMutableDictionary* eventDict;
@property(nonatomic,strong)NSString* nudgeMessage;
@property(nonatomic,strong)NSString* nudgeTrustMessage;
@property(nonatomic,strong)NSString* trustScore;
@property(nonatomic,strong)NSArray* profileAdHashTags;

@property(nonatomic,assign)TMViewControllerType currentViewControllerType;
@property(nonatomic,assign)NSInteger likeCounter;
@property(nonatomic,assign)NSInteger hideCounter;
@property(nonatomic,assign)NSInteger noOfProfilesHasLiked;
@property(nonatomic,assign)NSInteger noOfProfilesHasHide;
@property(nonatomic,assign)NSInteger noOfProfilesHasSparked;
@property(nonatomic,assign)BOOL isActivityDashboardAvailable;
@property(nonatomic,assign)BOOL isUserFlagCallCompleted;
@property(nonatomic,assign)BOOL isShownNudgeScreen;
@property(nonatomic,assign)BOOL isCameraPresent;
@property(nonatomic,assign)BOOL isAdReady;
@property(nonatomic,assign)BOOL isNudgeRequired;
@property(nonatomic,assign)BOOL appDidEnterBackground;
@property(nonatomic,assign)BOOL isProfileComplete;
@property(nonatomic,assign)BOOL isTrustVerified;
@property(nonatomic,assign)BOOL matchAnimationInProgress;
@property(nonatomic,assign)BOOL isValidProfilePic;
@property(nonatomic,assign)BOOL hasPhotos;
@property(nonatomic,assign)BOOL hasLikedInThePast;
@property(nonatomic,assign)BOOL showProfileVisibilityAlertAfterLike;

@property(nonatomic,strong)TMVideoTutorialView *playerView;
@property(nonatomic,strong)TMNativeAdWebViewController *webViewController;

@property(nonatomic,strong)TMSparkPackageBuyView *sparkPackageView;
@property(nonatomic,strong)TMSendSparkView *sendSparkView;
@property(nonatomic,assign)NSInteger sparkCountLeft;
@property(nonatomic,strong)TMBuySparkViewController *buySparkViewController;
@property(nonatomic,strong)TMInteractor *interactor;

//initializes properties with default values,register for notifications
-(void)setDefaults;

@end

@implementation TMMatchesViewController

#pragma mark Initializers
#pragma mark -

-(void)setDefaults {
    self.dataController = [[TMMatchDataController alloc] init];
    self.showLastActiveTimestamp = TRUE;
    self.currentViewControllerType = TMMATCHVIEWCONTROLLER;
    self.noOfProfilesHasHide = 0;
    self.noOfProfilesHasLiked = 0;
    self.noOfProfilesHasSparked = 0;
    self.likeCounter = -1;
    self.hideCounter = -1;
    self.isActivityDashboardAvailable = FALSE;
    self.isUserFlagCallCompleted = FALSE;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationRecieved:)
                                                 name:@"RecievedNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearMatchCacheData)
                                                 name:@"clearchatdata" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushToEventViewControllerWithEventId:)
                                                 name:@"openEvent" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventMatchAction:)
                                                 name:TMEVENTMATCH_LIKEHIDEACTION_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAppViralityGrowthHackViewController)
                                                 name:@"openRefer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDeepLink:)
                                                 name:TMDEEPLINK_NOTIFICATION object:nil];
}

#pragma mark ViewController LifeCycle Methods
#pragma mark -

- (void)viewDidLoad {
    [super viewDidLoad];
    [[TMAnalytics sharedInstance] trackView:@"TMMatchesViewController"];
    if([[TMUserSession sharedInstance] isLogin]) {
        [[TMIAPManager sharedManager] addIAPTransactionObserver];
    }
    TMLOG(@"matches view did view called");
    [self setDefaults];
    [self configureUIState];
    //SeventyNine Profile Ad Changes
    [self.adManager initializeProfileAdFlags];
    /////fetch match data
    BOOL needForceRefresh = FALSE;
    TMUserSession *userSession = [TMUserSession sharedInstance];
    if(userSession.isUserProfileUpdated) {
        needForceRefresh = TRUE;
        userSession.isUserProfileUpdated = FALSE;
    }
    
    [self loadMatchDataWithForceRefresh:needForceRefresh];
    //configure chat
    [self configureSocket];
    
    //MoEngage event
    [[MoEngage sharedInstance]trackEvent:@"Matches Launched" andPayload:nil];
    
    // check if app lauches from notification after killing the app
    if([TMDataStore containsObjectForKey:@"notificationReceived"]) {
        NSData *dictionaryData = [TMDataStore retrieveObjectforKey:@"notificationReceived"];
        NSDictionary *notificationDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
        if(notificationDictionary){
            [TMDataStore removeObjectforKey:@"notificationReceived"];
            [self handleNotification:notificationDictionary];
        }
    }
    
    // moengage in app messaging
    [[MoEngage sharedInstance]handleInAppMessage];
    [MoEngage sharedInstance].delegate = self;
    
    // check if app launches for event detail page
    if([self isLaunchFromEventURL]) {
        NSString *eventId = [TMDataStore retrieveObjectforKey:@"openEventForId"];
        [TMDataStore removeObjectforKey:@"openEventForId"];
        if(![eventId isEqualToString:@""]) {
            [self launchEventDetailWithEventId:eventId];
        }
    }
    
    [self resetPhotoVisibiltyAlertForLike];
    
    BOOL isQuizPending = [[TMUserSession sharedInstance] isSelectQuizPending];
    if(isQuizPending) {
        TMSelectQuizViewController *quizViewController = [[TMSelectQuizViewController alloc] init];
        quizViewController.quizDelegate = self;
        [self presentViewController:quizViewController animated:TRUE completion:nil];
    }
    
    if(!isQuizPending) { //so that nudge doesnot over lap quiz in corner case
        [self showSelectLongTermNudge];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = FALSE;
    
    self.actionDelegate = [self getActionDelegate];
    [self setMatchViewControllerTitle];
    
    BOOL needForceRefresh = FALSE;
    BOOL needToLoadMatchData = FALSE;
    TMUserSession *userSession = [TMUserSession sharedInstance];
    if(userSession.isUserProfileUpdated) {
        needForceRefresh = TRUE;
        needToLoadMatchData = TRUE;
        userSession.isUserProfileUpdated = FALSE;
    }
    else if(userSession.needToLoadMatchData) {
        needForceRefresh = FALSE;
        needToLoadMatchData = TRUE;
        userSession.needToLoadMatchData = FALSE;
    }
    
    if(needToLoadMatchData) {
        if([TMUserSession sharedInstance].needToLoadProfileOnSelectPayment) {
            [[TMUserSession sharedInstance] cacheSelectMatchId:self.profile.matchId matchHash:self.profile.sparkHash];
        }
        else {
           [[TMUserSession sharedInstance] resetCachedSelectMatchRefreshData];
        }
        [self loadMatchDataWithForceRefresh:needForceRefresh];
    }
    
    
    
    if(self.messageViewController) {
        self.messageViewController = nil;
    }
    
    [self fetchNotificationCount];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self setMatchViewControllerTitleToNil];
    //to reset side menu for all navigation from match view
    [self resetSideMenu];
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect frame = self.view.frame;
    CGFloat height = 90;
    self.matchActionView.frame = CGRectMake(CGRectGetMinX(frame),
                                            CGRectGetHeight(frame) - height,
                                            CGRectGetWidth(frame),
                                            height);
}

- (void)didReceiveMemoryWarning {
    
    if (self.presentedViewController && ![self.presentedViewController isKindOfClass:[TMQuizOverlayViewController class]] && !([self.presentedViewController isKindOfClass:[UINavigationController class]] && ([[(UINavigationController*)(self.presentedViewController) topViewController] isKindOfClass:[TMDealListViewController class]]))) {
        //remove any intention survey UI if present
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
    
    //clear the variables for ad manager
    [self.adManager deinit];
    self.adManager = nil;
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.dataController = nil;
    self.adTrackerManager = nil;
}

#pragma mark Getters / Setters
#pragma mark -

-(void)setNoOfProfilesHasHide:(NSInteger)noOfProfilesHasHide {
    [[TMUserSession sharedInstance] updateHideActionCounter:noOfProfilesHasHide];
    _noOfProfilesHasHide = noOfProfilesHasHide;
}
-(void)setNoOfProfilesHasLiked:(NSInteger)noOfProfilesHasLiked {
    [[TMUserSession sharedInstance] updateLikeActionCounter:noOfProfilesHasLiked];
    _noOfProfilesHasLiked = noOfProfilesHasLiked;
}
-(void)setNoOfProfilesHasSparked:(NSInteger)noOfProfilesHasSparked {
    [[TMUserSession sharedInstance] updateSparkActionCounter:noOfProfilesHasSparked];
    _noOfProfilesHasSparked = noOfProfilesHasSparked;
}

-(TMMatchActionView*)matchActionView {
    if(!_matchActionView) {
        CGRect frame = self.view.frame;
        CGFloat height = 90;
        _matchActionView = [[TMMatchActionView alloc] initWithFrame:CGRectMake(CGRectGetMinX(frame),
                                                                               CGRectGetHeight(frame) - height,
                                                                               CGRectGetWidth(frame),
                                                                               height)];
        _matchActionView.backgroundColor = [UIColor clearColor];
        _matchActionView.delegate = self;
    }
    return _matchActionView;
}
-(UIView*)overlayView {
    if(!_overlayView) {
        _overlayView = [[UIView alloc] initWithFrame:self.view.bounds];
        _overlayView.backgroundColor = [UIColor blackColor];
        _overlayView.alpha = 0.5;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [_overlayView addGestureRecognizer:tapGesture];
    }
    return _overlayView;
}
-(TMuserFlagManager*)userFlagManager {
    if(_userFlagManager == nil) {
        _userFlagManager = [[TMuserFlagManager alloc] init];
    }
    return _userFlagManager;
}
-(TMProfileCompletionToast*)completionToast {
    if(_completionToast == nil) {
        _completionToast = [[TMProfileCompletionToast alloc] init];
    }
    return _completionToast;
}
-(TMAdTrackingManager*)adTrackerManager {
    if(_adTrackerManager == nil) {
        _adTrackerManager = [[TMAdTrackingManager alloc] init];
    }
    return _adTrackerManager;
}
-(TMSendSparkView*)sendSparkView {
    if(!_sendSparkView) {
        _sendSparkView = [[TMSendSparkView alloc] init];
        _sendSparkView.delegate = self;
    }
    return _sendSparkView;
}
-(TMInteractor*)interactor {
    if(!_interactor) {
        _interactor = [[TMInteractor alloc] init];
    }
    return _interactor;
}
-(void)setupBuySparkViewController {
    self.buySparkViewController = [[TMBuySparkViewController alloc] init];
    self.buySparkViewController.transitioningDelegate = self;
    self.buySparkViewController.delegate = self;
    self.buySparkViewController.interactor = self.interactor;
}
- (TMAdManager*) adManager
{
    if (!_adManager) {
        _adManager = [[TMAdManager alloc] init];
        _adManager.adDelegate = self;
    }
    return _adManager;
}
- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return [TMDismissAnimator new];
}
- (nullable id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator {
    return self.interactor.hasStarted ? self.interactor : nil;
}

#pragma mark View Components Creation Methods
#pragma mark -

-(void)configureUIState {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.containerView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self addRightNavigationItem];
    [self addLefttNavigationItem];
}

-(void)addRightNavigationItem {
    CGFloat width = 44;
    CGFloat imageWidth = 24;
    TMBadgeView *chatBadgeView = [[TMBadgeView alloc] initWithFrame:CGRectMake(0, 0, width, width)
                                                          withImage:[UIImage imageNamed:@"Chat"]
                                                     withImageFrame:CGRectMake(width - imageWidth, (width - imageWidth)/2,
                                                                               imageWidth, imageWidth)];
    [chatBadgeView.badgeButton addTarget:self action:@selector(chatAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightMenuButton1 = [[UIBarButtonItem alloc]
                                         initWithCustomView:chatBadgeView];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    NSArray *itemarrs = [NSArray arrayWithObjects:negativeSpacer,rightMenuButton1,nil];
    [self.navigationItem setRightBarButtonItems:itemarrs];
    
}

-(void)addLefttNavigationItem {
    ////////// left menu button ///////////////////
    CGFloat width = 44;
    TMBadgeView *menuBadgeView = [[TMBadgeView alloc] initWithFrame:CGRectMake(0, 0, width, width)
                                                          withImage:[UIImage imageNamed:@"Menu_Icon"]
                                                     withImageFrame:CGRectMake(0, (width-24)/2, 24, 24)];
    [menuBadgeView.badgeButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftMenuButton = [[UIBarButtonItem alloc]
                                       initWithCustomView:menuBadgeView];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    NSArray *itemarrs = [NSArray arrayWithObjects:negativeSpacer,leftMenuButton,nil];
    [self.navigationItem setLeftBarButtonItems:itemarrs];
}
-(void)updateMutualLikeBarButtonItemNotificationStatus:(BOOL)notificationStatus {
    NSArray *barButtonItems = [self.navigationItem rightBarButtonItems];
    UIBarButtonItem *mutualLikeBarButton = [barButtonItems objectAtIndex:1];
    TMBadgeView *mutualLikeBadgeView = (TMBadgeView*)mutualLikeBarButton.customView;
    [mutualLikeBadgeView setBadgeNotificationStatus:notificationStatus];
}
-(void)updateChatBarButtonItemNotificationStatus:(BOOL)notificationStatus {
    NSArray *barButtonItems = [self.navigationItem rightBarButtonItems];
    UIBarButtonItem *mutualLikeBarButton = [barButtonItems objectAtIndex:1];
    TMBadgeView *chatBadgeView = (TMBadgeView*)mutualLikeBarButton.customView;
    [chatBadgeView setBadgeNotificationStatus:notificationStatus];
}
-(void)configureViewForDataLoadingStart {
    
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewMissTMAndMessage:@"Seeking the right matches for you"];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithTMViewAndTranslucentView];
    TMLOG(@"configureViewForDataLoadingFinish");
    if(self.retryView.superview) {
        TMLOG(@"removing retry view");
        [self removeRetryView];
    }
}
-(void)showProfileAdWithPlaceholderView:(UIView *)placeholderView {
    [self.adManager showProfileAdInView:placeholderView viewController:self];
}
-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadMatchDataWithForceRefresh:) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadMatchDataWithForceRefresh:) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)removeSubViewsForDataReloading {
    NSArray *subviews = [self.view subviews];
    for (int i=0; i<subviews.count; i++) {
        UIView *subview = [subviews objectAtIndex:i];
        [subview removeFromSuperview];
    }
    
    //remove the uialertview
    if (self.alertView) {
        [self.alertView dismissWithClickedButtonIndex:-1 animated:NO];
        self.alertView = nil;
    }
    
    if (self.matchActionView.alertView) {
        [self.matchActionView.alertView dismissWithClickedButtonIndex:-1 animated:NO];
        self.matchActionView.alertView = nil;
    }
    [self setMatchViewControllerTitle];
    
    self.matchAnimationInProgress = FALSE;
}
-(void)showMatchProfileView {
    [self.view addSubview:self.containerView];
    [self.view bringSubviewToFront:self.containerView];
    [self.containerView addSubview:self.collectionView];
    if(!self.matchActionView.superview) {
        BOOL isSparkEnabled = (self.profile.isProfileAdCampaign) ? FALSE : TRUE;
        [self.matchActionView updateActionButtonsWithSparkStatus:isSparkEnabled];
        [self.containerView addSubview:self.matchActionView];
    }
    
    CGRect frame = self.view.frame;
    CGFloat height = 90;
    [self.containerView bringSubviewToFront:self.matchActionView];
    self.matchActionView.frame = CGRectMake(CGRectGetMinX(frame),
                                            CGRectGetHeight(frame) - height,
                                            CGRectGetWidth(frame),
                                            height);
    //SeventyNine profile Ad changes
    if(self.profile.isProfileAdCampaign) {
        [self.matchActionView disableSparkButton];
    }
    else if(self.profile.hasLikedBefore || (![[TMUserSession sharedInstance].user isUserCountryIndia] && [self hasReachedLikeLimit])) {
        [self.matchActionView disableLikeButton];
    }
    else {
        [self.matchActionView enableSparkButton];
        [self.matchActionView enableLikeButton];
    }
}

//show meet up tutorial
-(void)showMeetUpTutorial:(CGFloat)delay {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    [userSession registerMeetUpTutorialViewShown];
    
    ////show animation
    TMAnimationView *animatonView = [[TMAnimationView alloc] initWithFrame:self.view.bounds];
    [animatonView showEventsTutorialOnBaseView:self.view];
}

/////create matchview and load with match data for the current index
-(void)showMatchView {
    [self configureProfileCollectionViewWithProfileData];
    [self showMatchProfileView];
    if(self.profile.hasLikedBefore) {
        [self.matchActionView showHaslikedBeforeAnimation];
    }
    [self setMatchViewControllerTitle];
    
    // track impression tracker when ad is showing
    if(self.profile.isProfileAdCampaign && self.profile.matchId != nil) {
        NSDictionary* matchDetails = @{@"source":@"matches"};
        if(self.profile.matchId){
            matchDetails = @{@"source":@"matches",@"match_id":self.profile.matchId};
        }
        [self.adTrackerManager trackImpressionUrls:self.profile.adObj.impressionTracker eventInfo:matchDetails];
    }
    
    if(self.profile.isUserNRI && [self canShowNRIProfileNudge]) {
        [self showNRIProfileNudge];
    }
    //check with match manager for the timestamp for the profile visibility toast
    if ([self isTopMostViewController] && [self.dataController isToastRequiredForFemaleProfileVisibility]) {
        //show the toast for message
        [TMToastView showToastInParentView:self.view
                                  withText:@"You could be missing out on great matches.Turn on 'Profile Visibility' in your Settings" withDuaration:4
                     presentationDirection:TMToastPresentationFromBottom];
    }
    
    // show mutual event nudge
    [self showMutualEventNudge];
    
    // show photo visibility nudge
    [self showPhotoFlowToastForLikeAProfile];
}
-(TMMatchTransitionView*)matchTransitionViewWithType:(TMMatchTransitionType)transitionType {
    CGRect rect = self.view.bounds;
    TMMatchTransitionView *transitonView = [[TMMatchTransitionView alloc] initWithFrame:rect
                                                                     withTransitionType:transitionType
                                                                              sparkLeft:[NSString stringWithFormat: @"%ld", (long)self.sparkCountLeft]];
    transitonView.tag = 71001;
    [self.view addSubview:transitonView];
    TMLOG(@"Subviews:%@",self.view);
    TMLOG(@"SuperView:%@",transitonView.superview);
    return transitonView;
}
-(TMSystemMessageView*)systemMessageViewWithSystemMessageLink:(TMSystemMessageLinkType)systemMsgLinkType {
    TMSystemMessageView *systemMsgView = (TMSystemMessageView*)[self.view viewWithTag:10002];
    if(!systemMsgView) {
        systemMsgView = [[TMSystemMessageView alloc] initWithFrame:self.view.bounds withSystemMessageLink:systemMsgLinkType];
        
        systemMsgView.delegate = self;
        systemMsgView.tag = 10002;
        [self.view addSubview:systemMsgView];
    }
    return systemMsgView;
}
-(void)setMatchViewControllerTitle {
    self.title = @"Matches";
}
-(void)setMatchViewControllerTitleToNil {
    self.title = nil;
}
-(void)resetSideMenu {
    if(self.slidingViewController.currentTopViewPosition != ECSlidingViewControllerTopViewPositionCentered) {
        [self.slidingViewController resetTopViewAnimated:FALSE];
        [self.overlayView removeFromSuperview];
    }
}

#pragma mark Data Loading
#pragma mark -

-(void)updateFBToken {
    [self.dataController updateFBAccessToken:^(BOOL isTokenExpired) {
        if(isTokenExpired) {
            [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
        }
    }];
};
-(void)loadUserFlags {
    if(self.userFlagManager.isNetworkReachable) {
        ///add tracking
        NSMutableDictionary *trackParams = [[NSMutableDictionary alloc] init];
        trackParams[@"screenName"] = @"TMMatchesViewController";
        trackParams[@"eventCategory"] = @"userFlags";
        trackParams[@"eventAction"] = @"server_call";
        
        //set state for cal initiation
        self.isUserFlagCallCompleted = FALSE;
        self.userFlagManager.trackEventDictionary = trackParams;
        [self.userFlagManager userFlags:^(NSDictionary *res) {
            if(res) {
                self.isUserFlagCallCompleted = TRUE;
                [self.adManager initializeSeventyNineSDK];
                [self processUserFlagResponse:res];
            }
        }];
    }
}
-(void)fetchNotificationCount {
    [[TMMessagingController sharedController] fetchAndUpdateUnreadConversationListCount:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateChatBarButtonItemNotificationStatus:status];
        });
    }];
    [[TMMessagingController sharedController] showUnreadConversationBlooper:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateChatBarButtonItemNotificationStatus:status];
        });
    }];
}
-(void)loadMatchDataWithForceRefresh:(BOOL)forceRefresh {
    //load user flags
    [self loadUserFlags];
    [self updateFBToken];
    //BOOL isMaleUser = (![[TMUserSession sharedInstance].user isUserFemale]);
    BOOL loadDataFromServer = forceRefresh;
    
    [self.dataController getMatchDataWithForceRefresh:loadDataFromServer willStartRequest:^{
        self.profile = nil;
        [self removeSubViewsForDataReloading];
        
    } didStartRequest:^{
        [self configureViewForDataLoadingStart];
        
    } profileData:^(TMProfile *profile) {
        self.profile = profile;
        if(loadDataFromServer && profile != nil) {
            [self.adManager resetProfileViewedCounter];
        }
        [self resetSideMenu];
        [self configureViewForDataLoadingFinish];
        [self processMatchResponse:loadDataFromServer];
        
    } error:^(TMError *error) {
        [self resetSideMenu];
        [self configureViewForDataLoadingFinish];
        if(error.errorCode == TMERRORCODE_LOGOUT) {
            [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
        }
        else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
            [self showRetryViewAtError];
        }
        else {
            //unknwon error
            [self showRetryViewAtUnknownError];
        }
    }];
}
-(void)processUserFlagResponse:(NSDictionary *)response {
    
    NSDictionary* counter = response[@"counter"];
    NSDictionary* messages = response[@"messages"];
    self.likeCounter = [counter[@"likeCount"] integerValue];
    self.hideCounter = [counter[@"hideCount"] integerValue];
    self.isProfileComplete = [response[@"isProfileComplete"] boolValue];
    self.isTrustVerified = [response[@"isTrustVerified"] boolValue];
    self.trustScore = [NSString stringWithFormat:@"%@", response[@"trustScore"]];
    self.nudgeMessage = [NSString stringWithFormat:@"%@", messages[@"profileIncompleteMessage"]];
    self.nudgeTrustMessage = [NSString stringWithFormat:@"%@", messages[@"trustIncompleteMessage"]];
    NSDictionary *tutorialVideo = response[@"meetups_video"];
    BOOL sparkStatus = [response[@"sparks_active"] boolValue];
    [[TMUserSession sharedInstance] enableSpark:sparkStatus];
    BOOL isSparkEnabled = (self.profile.isProfileAdCampaign) ? FALSE : TRUE;
    [self.matchActionView updateActionButtonsWithSparkStatus:isSparkEnabled];
    NSDictionary *updateDict = response[@"update_version_flags"];
    NSString *hardVersion = updateDict[@"hard_app_version_ios"];
    NSString *softVersion = updateDict[@"soft_app_version_ios"];
    
    
    if([self isHardUpdatePopUpShown:hardVersion]) {
        if(!self.alertView) {
            NSString *cancelButtonTitle = nil;
            self.alertView = [[UIAlertView alloc] initWithTitle:@"" message:updateDict[@"hard_update_text_ios"] delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:@"Update Now" ,nil];
            self.alertView.tag = 1103901;
            [self.alertView show];
        }
    }
    else if ([self isSoftUpdatePopUpShown:softVersion]) {
        if(!self.alertView) {
            NSString *cancelButtonTitle = @"Cancel";
            self.alertView = [[UIAlertView alloc] initWithTitle:@"" message:updateDict[@"soft_update_text_ios"] delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:@"Update Now" ,nil];
            self.alertView.tag = 1103902;
            [self.alertView show];
        }
    }
    else {
        //why_not_sparks flags
        NSDictionary *sparkPopupDict = response[@"why_not_sparks"];
        if(sparkPopupDict && [sparkPopupDict isKindOfClass:[NSDictionary class]]) {
            [[TMUserSession sharedInstance] updateWhyNotSparkPopupFlags:sparkPopupDict];
        }
        if(response[@"activityData"] != nil) {
            self.isActivityDashboardAvailable = true;
            NSDictionary* data = response[@"activityData"];
            self.activityDashboard = [[TMActivityDashboard alloc] init];
            [self.activityDashboard setActivityDashboard:data];
        }
        NSDictionary *matchOptimizationParams = response[@"matches_cache_optimisations"];
        if(matchOptimizationParams && [matchOptimizationParams isKindOfClass:[NSDictionary class]]) {
            [self.dataController updateOptimizationParams:matchOptimizationParams];
        }
        //spark count
        NSString *sparkCount = response[@"sparkCountersLeft"];
        if(sparkCount && (![sparkCount isKindOfClass:[NSNull class]])) {
            TMUserSession *userSession = [TMUserSession sharedInstance];
            [userSession updateAvailableSparkCount:sparkCount];
        }
        
        ////refer and earn flag//app_virality_flag
        if(response[@"app_virality_flag"] && ![response[@"app_virality_flag"] isKindOfClass:[NSNull class]]){
            BOOL appViralityStatus = [response[@"app_virality_flag"] boolValue];
            [[TMUserSession sharedInstance] setAppViralityActiveEnabled:appViralityStatus];
        }
        // send user email to app virality
        if(response[@"user_email"] && ![response[@"user_email"] isKindOfClass:[NSNull class]]) {
            NSString *email = response[@"user_email"];
            [TMAppViralityController updateUserEmailId:email];
        }
        ////
        TMUserSession *userSession = [TMUserSession sharedInstance];
        BOOL oldValue = [userSession isMeetUpIconShown];
        
        if(response[@"meetups_enabled"] && ![response[@"meetups_enabled"] isKindOfClass:[NSNull class]]) {
            BOOL showMeetUps_icon = [response[@"meetups_enabled"] boolValue];
            if( (oldValue || showMeetUps_icon) && (!oldValue || !showMeetUps_icon) ) {
                //NSArray *barButtonItems = [self.navigationItem rightBarButtonItems];
                //UIBarButtonItem *meetUpBarButton = [barButtonItems objectAtIndex:2];
                //TMBadgeView *meetUpBadgeView = (TMBadgeView*)meetUpBarButton.customView;
                //meetUpBadgeView.hidden = !showMeetUps_icon;
                
                [userSession registerMeetUpIconShown:showMeetUps_icon];
            }
            if([userSession isFirstSessionFlagSet]) {
                if(showMeetUps_icon && ![userSession isVideoTutorialShown] && tutorialVideo[@"video_tutorial_url"] &&
                   ![tutorialVideo[@"video_tutorial_url"] isKindOfClass:[NSNull class]]){
                    if(!self.playerView) {
                        self.playerView = [[TMVideoTutorialView alloc] initWithFrameAndVideoURLString:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height+64) URLString:tutorialVideo[@"video_tutorial_url"]];
                        self.playerView.delegate = self;
                        [[[UIApplication sharedApplication] keyWindow] addSubview:self.playerView];
                        [self trackVideoTutorial:@"started" time_taken:@"0"];
                    }
                }
            }
        }
    }
}

#pragma mark TMVideoTutorialViewDelegate methods
-(void)didMediaFinishPlay:(NSString *)type currentTime:(NSString *)currentTime {
    if(self.playerView){
        [self removeVideoPlayer];
        
        TMUserSession *userSession = [TMUserSession sharedInstance];
        [userSession registerVideoTutorialShown];
        
        CGFloat delay = 0.5;
        // in case of update only
        if(![userSession isMeetUpTutorialShown]) {
            //show meet up tutorial
            [self showMeetUpTutorial:delay];
        }
        
        [self trackVideoTutorial:type time_taken:currentTime];
    }
}

-(void)updateNudgeScreen {
    if(![[TMUserSession sharedInstance].user isUserFemale]) {
        TMSystemMessageLinkType systemMsgLinkType = [self.dataController systemMessageTypeForLastTileLink];
        [self showLastTileWithId:systemMsgLinkType];
    }
}
-(void)processMatchResponse:(BOOL)fromServer {
    self.noOfProfilesHasHide = 0;
    self.noOfProfilesHasLiked = 0;
    self.noOfProfilesHasSparked = 0;
    self.isShownNudgeScreen = false;
    self.isNudgeRequired = false;
    self.nudgeDict = nil;
    
    self.isValidProfilePic = self.dataController.isValidProfilePic;
    self.hasPhotos = self.dataController.hasPhotos;
    self.hasLikedInThePast = self.dataController.hasLikedInThePast;
    
    self.sparkCountLeft = [self getSparkCounterLeft];
    
    
    if(self.dataController.isNudgeAvailable) {
        self.isNudgeRequired = true;
        self.nudgeDict = self.dataController.nudgeDataDictionary;
    }
    NSInteger matchCount = [self.dataController getAvailableMatcheCount];
    if( matchCount > 0) {
        [self.adManager checkForProfileAdPositionReset:matchCount];
    }
    /*
     ** update ui based on combination of count, first tile and last tile as described below
     ** Top level cases 1. No Profile Photo  2. All other
     ** With case 1 need to take action based on first tile only
     ** with second case need to take action based on both first and second tile and match count
     */
    TMSystemMessageLinkType firstTileSystemMsgLinkType = [self.dataController systemMessageLinkTypeForFirstTile];
    if(firstTileSystemMsgLinkType == SYSTEM_MESSAGE_LINK_TYPE_PHOTOUPLOAD) {
        //don't show interstitial ad over upload photo prompt
        if ([[TMUserSession sharedInstance].user isUserFemale]) {
            [TMUserSession sharedInstance].user.isFemaleProfileRejected = YES;
            [TMUserSession sharedInstance].user.hasFemaleProfileBeenRejectedOnce = YES;
            //show matches to female rejected users here
            [self showMatchView];
            [self cacheNextMatchesProfileImage];
        }
        else {
            [TMUserSession sharedInstance].user.isFemaleProfileRejected = NO;
            [TMUserSession sharedInstance].user.hasFemaleProfileBeenRejectedOnce = NO;
        }
        [self showFirstTileWithId:firstTileSystemMsgLinkType];
        [self showLastTileWithId:firstTileSystemMsgLinkType];
    }
    else {
        if ([[TMUserSession sharedInstance].user isUserFemale]) {
            //update the TMUser for the upload failure status
            [TMUserSession sharedInstance].user.isFemaleProfileRejected = NO;
        }
        else {
            [TMUserSession sharedInstance].user.isFemaleProfileRejected = NO;
            [TMUserSession sharedInstance].user.hasFemaleProfileBeenRejectedOnce = NO;
        }
        if([self.dataController isMatchesAvailabe]) {
            [self showMatchView];
            [self cacheNextMatchesProfileImage];
            
            //if first tile is available then show over match view
            if([self.dataController isFirstTileAvailable]) {
                firstTileSystemMsgLinkType = [self.dataController systemMessageLinkTypeForFirstTile];
                [self showFirstTileWithId:firstTileSystemMsgLinkType];
            }
        }
        else { //matches not available show last tile
            TMSystemMessageLinkType systemMsgLinkType = [self.dataController systemMessageTypeForLastTileLink];
            [self showLastTileWithId:systemMsgLinkType];
           // [self.adManager initializeProfileAdFlags];
            
        }
    }
    //SeventyNine Ad Changes
    if ([self.dataController canShowInterstitialAd]) {
        [self.adManager showInterstitialAd];
    }
    
}
-(void)sendUserActionOnMatchProfile:(NSString*)actionURL
                     withTransition:(TMMatchTransitionType)transitionType {
    
        if(!self.matchAnimationInProgress) {
            /////send action url
            [self.dataController sendUserActionOnMatchProfile:self.profile.matchId
                                                    actionURL:actionURL
                                            moveToNextProfile:^(BOOL moveToNextProfile) {
                                                
                ///if match action tutorial view animation is not finsihed then remove tutorial view from superview hierarchy
                TMAnimationView *animatonView = (TMAnimationView*)[self.view viewWithTag:TMMATCHACTION_ANIMATIONVIEW_TAG];
                if(animatonView) {
                    [animatonView removeFromSuperview];
                    animatonView = nil;
                }
                                                
                if(self.profile.matchId != nil) {
                    if(transitionType == TMMATCH_TRANSITION_TYPE_LIKE) {
                        self.noOfProfilesHasLiked++;
                    }
                    else if(transitionType == TMMATCH_TRANSITION_TYPE_HIDE) {
                        self.noOfProfilesHasHide++;
                    }
                    [self trackMatchTransitionType:transitionType];
                }
                
                /////animate and move to next match
                self.matchAnimationInProgress = TRUE;
                [self moveToNextMatchProfileWithTransition:transitionType];
            } response:^(NSDictionary *mutualData) {
                BOOL isMutual = [[mutualData objectForKey:@"isMutualMatch"] boolValue];
                if(isMutual) {
                    //TMLOG(@"________ MUTUAL LIKE ___________");
                    [self updateChatBarButtonItemNotificationStatus:true];
                }
                
            } error:^(TMError *error) {
                
                [self showMatchResponseErrorAlert:TM_INTERNET_NOTAVAILABLE_MSG msg:nil];
            }];
        }
}
-(void)cacheNextMatchesProfileImage {
    [self.dataController cacheMatchesProfileImages];
}

#pragma mark NavBar Item Action Handler Methods
#pragma mark -

-(void)tapAction {
    [self.slidingViewController resetTopViewAnimated:YES];
    [self.overlayView removeFromSuperview];
}
-(void)menuAction {
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered) {
        TMSideMenuViewController *sideMenuViewCon = (TMSideMenuViewController*)self.slidingViewController.underLeftViewController;
        sideMenuViewCon.delegate = self;
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
        [self.view addSubview:self.overlayView];
        [self.view bringSubviewToFront:self.overlayView];
    }
    else {
        [self.slidingViewController resetTopViewAnimated:YES];
        [self.overlayView removeFromSuperview];
    }
}
-(void)chatAction {
    [self updateChatBarButtonItemNotificationStatus:FALSE];
    
    self.messageViewController = [self messageViewControllerWithMessageConversation:nil];
    [self.navigationController pushViewController:self.messageViewController animated:true];
}
-(void)categoryViewAction {
    TMEventCategoryListViewController *eventListViewCon = [[TMEventCategoryListViewController alloc] init];
    eventListViewCon.delegate = self;
    eventListViewCon.actionDelegate = [self getActionDelegate];
    [self.navigationController pushViewController:eventListViewCon animated:true];
}

#pragma mark TMMatchActionViewDelegate Handler
#pragma mark -

-(void)didPeformLikeAction {
    [self moveToNextMatchProfileWithLikeAction];
}

-(void)didPeformHideAction {
    [self moveToNextMatchProfileWithHideAction];
}

-(void)peformLikeActionWithAlert {
    //need to show alert when this delegate is called
    [self moveToNextMatchProfileWithLikeActionAlert];
}
- (void)perfromProfileRejectionAlertDisplay {
    //reset the side menu
    self.showProfileVisibilityAlertAfterLike = true;
    [self resetSideMenu];
    [self moveToNextMatchProfileWithLikeAction];
}
-(void)didPerformSparkAction {
    if([self.profile showSelectNudgeOnSparkAction]) {
        [self showSelectJoinNudgeFromAction:@"spark"];
    }
    else {
        [self showSendSparkViewFromIntro:FALSE];
    }
}
-(void)moveToNextMatchProfileWithLikeAction{
    if([self.profile showSelectNudgeOnLikeAction]) {
        [self showSelectJoinNudgeFromAction:@"like"];
    }
    else if(self.profile.hasLikedBefore) {
        [self showHasLikedBeforeSparkAlertNudge];
    }
    else if([self canShowWhyNotSparkAlert]) {
        [self showWhyNotSparkAlertNudge];
    }
    else if(![[TMUserSession sharedInstance].user isUserCountryIndia] && [self hasReachedLikeLimit]) {
        [self showHasReachedLikeLimitSparkAlertNudge];
    }
    else {
        [self sendUserActionOnMatchProfile:self.profile.likeUrl withTransition:TMMATCH_TRANSITION_TYPE_LIKE];
    }
}
-(void)moveToNextMatchProfileWithHideAction{
    
    [self sendUserActionOnMatchProfile:self.profile.hideUrl withTransition:TMMATCH_TRANSITION_TYPE_HIDE];
}


-(void)moveToNextMatchProfileWithTransition:(TMMatchTransitionType)transitionType {
    
    TMMatchTransitionView *view = [self matchTransitionViewWithType:transitionType];
    [self animateTransitionView:view
                 withCompletion:^(BOOL finished) {
                     self.matchAnimationInProgress = FALSE;
                     
                     if(self.profile.matchId == nil && self.profile.isProfileAdCampaign) {
                         self.profile = [self.dataController getMatchProfile];
                         [self processNextProfile:transitionType];
                     }else {
                         [self.adManager showProfileAdWithResponse:^(BOOL status) {
                             if(status) {
                                 self.profile = [self.dataController getMatchAdProfile];
                             }
                             else {
                                 self.profile = [self.dataController getMatchProfile];
                             }
                             [self processNextProfile:transitionType];
                         }];
                     }
                     
                 }];
}

- (void) processNextProfile:(TMMatchTransitionType)transitionType{
    
    if(self.profile) {
        [self showMatchView];
        if(self.profile.matchId != nil) {
            [self cacheNextMatchesProfileImage];
            if(self.showProfileVisibilityAlertAfterLike && ![self canShowProfileVisibilityAlertForLike]){
                self.showProfileVisibilityAlertAfterLike = FALSE;
                [self registerForLikePhotoFlow];
                [self presentProfileVisibilityAlertView:TRUE];
            }
        }
    }
    else {
        TMSystemMessageLinkType systemMsgLinkType = [self.dataController systemMessageTypeForLastTileLink];
        [self showLastTileWithId:systemMsgLinkType];
    }
    
    ///////////////////
    if(transitionType == TMMATCH_TRANSITION_TYPE_LIKE) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        if(![userSession isMutualLikeAnimationShown]) {
            [self animateMutualLikeViewTutorial];
        }
    }
    //show the miss TM Nudge
    if(self.isNudgeRequired) {
        NSString *msg = [self.dataController nudgeKeyForCurrentMatch];
        if(![msg isEqualToString:@""]) {
            [self showMissTMNudge:msg];
        }
    }
    
    // for female user only
    if([[TMUserSession sharedInstance].user isUserFemale] && ![TMUserSession sharedInstance].user.isFemaleProfileRejected && self.isUserFlagCallCompleted) {
        TMLOG(@"server like counter = %ld",(long)self.likeCounter);
        TMLOG(@"server hide counter = %ld",(long)self.hideCounter);
        TMLOG(@"hide counter = %ld",(long)self.noOfProfilesHasHide);
        TMLOG(@"like counter = %ld",(long)self.noOfProfilesHasLiked);
        
        // after total threshold like count check whether to show
        if(!self.isTrustVerified && self.noOfProfilesHasLiked == self.likeCounter) {
            if([self.completionToast willShowTrustToast]) {
                [self.completionToast setDailyParameterForTrustBuilder];
                [self showEditNudgeScreen:@"female" nudgeView:TMTrustBuilderView];
            }
        }
        if(!self.isProfileComplete && self.noOfProfilesHasHide == self.hideCounter) { //self.hideCounter
            if([self.completionToast willShowIncompleteProfileToast]) {
                // show incomplete profile toast
                [self.completionToast setDailyParameterForProfile];
                [self showEditNudgeScreen:@"female" nudgeView:TMProfileView];
            }
        }
    }
    
    if((transitionType == TMMATCH_TRANSITION_TYPE_SPARK) && (self.sparkCountLeft == 1)) {
        if(!self.sparkPackageView) {
            self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
            self.sparkPackageView.delegate = self;
        }
        [self.sparkPackageView showLastSparkAlertState];
    }
    
    //SeventyNine Ad Changes
    if ([self.dataController canShowInterstitialAd]) {
        [self.adManager showInterstitialAd];
    }
}

-(void)moveToNextMatchProfileWithReportUserAction {
    
    self.profile = [self.dataController getMatchProfile];
    if(self.profile) {
        [self showMatchView];
        [self cacheNextMatchesProfileImage];
    }
    else {
        TMSystemMessageLinkType systemMsgLinkType = [self.dataController systemMessageTypeForLastTileLink];
        [self showLastTileWithId:systemMsgLinkType];
    }
}
-(void)moveToNextMatchProfileWithLikeActionAlert {
    
    self.alertView = [[UIAlertView alloc] initWithTitle:[self.dataController getLikeActionSystemMessage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    self.alertView.tag = 50002;
    [self.alertView show];
}
-(void)animateTransitionView:(TMMatchTransitionView*)tansitionView
              withCompletion:(void(^)(BOOL finished))animationBlock {
    [UIView animateWithDuration:0.4
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         tansitionView.alpha = 0.7;
                     }
                     completion:^(BOOL finished){
                         ////////////////////////
                         [UIView animateWithDuration:0.5
                                               delay: 0.2
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              tansitionView.alpha = 0.0;
                                          }
                                          completion:^(BOOL finished){
                                              [tansitionView removeFromSuperview];
                                              animationBlock(TRUE);
                                          }];
                     }];
}


#pragma mark Alert delegate
#pragma mark -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 50001) { //user varification alert handling
        if(buttonIndex == 1){
            TMTrustBuilderViewController *trustbuilderViewCon = [self trustBuilderViewcontrollerWithMessage:nil];
            [self.navigationController pushViewController:trustbuilderViewCon
                                                 animated:NO];
        }
    }
    else if(alertView.tag ==  50002) { //
        [self moveToNextMatchProfileWithLikeAction];
    }
    else if(alertView.tag == 1103901){
        if(buttonIndex == 0){
            NSString *iTunesLink = @"https://itunes.apple.com/us/app/apple-store/id964395424?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
    }
    else if(alertView.tag == 1103902){
        if(buttonIndex == 1){
            NSString *iTunesLink = @"https://itunes.apple.com/us/app/apple-store/id964395424?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    self.alertView = nil;
}


#pragma mark View Util Method
#pragma mark -

-(void)showSendSparkViewFromIntro:(BOOL)didShowFromIntro {
    if(![[TMUserSession sharedInstance] isSparkTutorialShown]) {
        if(!self.sparkPackageView) {
            self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
            self.sparkPackageView.delegate = self;
        }
        [self.sparkPackageView showSparkTutorialView];
        [self trackSparkIntorShownEventWithStatus:@"view"];
    }
    else {
        if([[TMIAPManager sharedManager] canConnect]) {
            self.sparkCountLeft = [self getSparkCounterLeft];
            if(self.sparkCountLeft > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.sendSparkView showSendMessageDialog];
                });
                self.sendSparkView.didShowFromIntro = didShowFromIntro;
                [self trackSendSparkEventWithStatus:@"view" errorMessage:nil];
                
                [[TMIAPManager sharedManager] getCommanalityStringWithMatch:self.profile.matchId
                                                                   response:^(NSArray *commonalityList,NSString* errorMessage) {
                                                                       
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [self.sendSparkView showCommonalityString:commonalityList];
                                                                       });
                                                                       
                                                                       self.sendSparkView.didShowDefaultCommonalityString = (errorMessage) ? TRUE : FALSE;
                                                                       if(errorMessage) {
                                                                           [self trackSendSparkEventWithStatus:@"interests_error" errorMessage:errorMessage];
                                                                       }
                                                                   }];
            }
            else {
                [self showSparkPackages];
                self.sparkPackageView.didShowFromIntro = didShowFromIntro;
                self.sparkPackageView.didShowFromSideMenu = FALSE;
                [self trackBuySparkEventViewStatus];
            }
        }
        else {
            [self showMatchResponseErrorAlert:TM_INTERNET_NOTAVAILABLE_MSG msg:nil];
        }
    }
}
-(void)showFirstTileWithId:(TMSystemMessageLinkType)systemMessageLinkType {
    NSString *msgString = [self.dataController getFirstSystemTileTitle];
    if(systemMessageLinkType == SYSTEM_MESSAGE_LINK_TYPE_PHOTOUPLOAD) {
        if([self isTopMostViewController]) {
            self.isShownNudgeScreen = true;
            if ([[TMUserSession sharedInstance].user isUserFemale]) {
                //[self showProfileVisibilityView];
                if([self canShowProfileVisibilityAlertOnLaunch]){
                    [self presentProfileVisibilityAlertView:false];
                }else {
                    self.isShownNudgeScreen = false;
                }
            }
            else {
                //male user
                TMPhotoViewController *photoViewCon = [self photoViewControllerWithMessage:msgString];
                [self.navigationController pushViewController:photoViewCon
                                                     animated:NO];
            }
        }
    }
    else if(systemMessageLinkType == SYSTEM_MESSAGE_LINK_TRUSTBUILDER) {
        TMUser *user = [TMUserSession sharedInstance].user;
        if([user isNonAuthenticUserFirstInteractionedWithMatch] == false) {
            self.alertView = [[UIAlertView alloc] initWithTitle:msgString
                                                        message:nil delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Verify Now", nil];
            self.alertView.tag = 50001;
            [self.alertView show];
            
            //update status
            [user registerNonAuthenticUserFirstInteractionWithMatch];
        }
        else {
            if([self isTopMostViewController]) {
                TMTrustBuilderViewController *trustbuilderViewCon = [self trustBuilderViewcontrollerWithMessage:msgString];
                [self.navigationController pushViewController:trustbuilderViewCon
                                                     animated:NO];
            }
        }
    }
    else if(systemMessageLinkType == SYSTEM_MESSAGE_LINK_SHOWMATCHES) {
        TMSystemMessageView *systemMsgView =  [self systemMessageViewWithSystemMessageLink:systemMessageLinkType];
        [systemMsgView setTitleText:msgString];
        [systemMsgView setButtonTitleText:@"Show me my matches"];
    }
}

-(void)showLastTileWithId:(TMSystemMessageLinkType)systemMessageLinkType {
    [self setMatchViewControllerTitle];
    TMSystemMessageView *systemMsgView = NULL;
    
    if(systemMessageLinkType == SYSTEM_MESSAGE_TYPE_INVITEFRIEND) {
        systemMsgView = [self systemMessageViewWithSystemMessageLink:systemMessageLinkType];
        [systemMsgView setTitleText:[self.dataController getLastTileSystemMessage]];
        [systemMsgView setButtonTitleText:@"Invite"];
        //as invite friend functionality is not implemented
        //[systemMsgView setButtonHidden];
    }
    else if(systemMessageLinkType == SYSTEM_MESSAGE_LINK_TRUSTBUILDER) {
        systemMsgView = [self systemMessageViewWithSystemMessageLink:systemMessageLinkType];
        [systemMsgView setTitleText:[self.dataController getLastTileSystemMessage]];
        [systemMsgView setButtonTitleText:@"Verify Now"];
    }
    else if(systemMessageLinkType == SYSTEM_MESSAGE_LINK_TYPE_PHOTOUPLOAD) {
        //only for male users
        //for female users no need to show any ui onlast if type is photo
        if( (![[TMUserSession sharedInstance].user isUserFemale]) ) {
            systemMsgView = [self systemMessageViewWithSystemMessageLink:systemMessageLinkType];
            [systemMsgView setTitleText:[self.dataController getFirstSystemTileTitle]];
            [systemMsgView setButtonTitleText:@"Upload Photo"];
        }
    }
    else {
        systemMsgView = [self systemMessageViewWithSystemMessageLink:SYSTEM_MESSAGE_LINK_NONE];
        [systemMsgView setTitleText:[self.dataController getLastTileSystemMessage]];
        [systemMsgView setButtonHidden];
    }
    
    // for male user only
    if(![[TMUserSession sharedInstance].user isUserFemale] && self.isUserFlagCallCompleted && !self.isShownNudgeScreen) {
        if(!self.isTrustVerified) {
            [self showEditNudgeScreen:@"male" nudgeView:TMTrustBuilderView];
        }
        else if(!self.isProfileComplete) {
            [self showEditNudgeScreen:@"male" nudgeView:TMProfileView];
        }
        if(self.isActivityDashboardAvailable && [self.completionToast willShowPIDashboard]) {
            [self showActivityDashBoardScreen:self.activityDashboard];
        }
    }
}
-(void)showSelectLongTermNudge {
    if([self canShowSelectNudge]) {
        if(!self.sparkPackageView) {
            self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
            self.sparkPackageView.delegate = self;
            self.sparkPackageView.source = @"matches";
            self.sparkPackageView.eventName = @"select_serious_intent_nudge";
        }
        NSString *content = [[TMUserSession sharedInstance] selectNudgeContent];
        [self.sparkPackageView showSelectLongTermNudgeAlertWithTitleText:content];
        NSDictionary *trackingInfo = @{@"source":@"matches"};
        [self trackSelectEvent:@"select_serious_intent_nudge" withStatus:@"view" userInfo:trackingInfo];
    }
    
}
-(void)showSelectJoinNudgeFromAction:(NSString*)action {
    if(!self.sparkPackageView) {
        self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
        self.sparkPackageView.delegate = self;
        self.sparkPackageView.action = action;
        self.sparkPackageView.source = @"matches";
        self.sparkPackageView.eventName = @"select_on_action_nudge";
    }
    NSString *userProfileImageURL = [TMUserSession sharedInstance].user.profilePicURL;
    NSString *matchImageURL = self.profile.basicInfo.imageUrlString;
    NSArray *imageURLStrings = @[matchImageURL,userProfileImageURL];
    NSString *selectProfileCTA = [[TMUserSession sharedInstance].user selectProfileCta];
    NSString *actionButtonString = [NSString stringWithFormat:@"%@!",selectProfileCTA];
    [self.sparkPackageView showSelectActionNudgeAlertWithActionText:[actionButtonString uppercaseString]
                                                withImageURLStrings:imageURLStrings];
    NSDictionary *trackingInfo = @{@"source":@"matches",@"on_action":action};
    [self trackSelectEvent:@"select_on_action_nudge" withStatus:@"view" userInfo:trackingInfo];
}

-(void)showHasLikedBeforeSparkAlertNudge {
    [TMToastView showToastInParentView:self.view
                              withText:@"Hey! You've already liked her. How about a spark this time?" withDuaration:4
                 presentationDirection:TMToastPresentationFromBottom];
    /*if(!self.sparkPackageView) {
        self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
        self.sparkPackageView.delegate = self;
    }
    NSString *userProfileImageURL = [TMUserSession sharedInstance].user.profilePicURL;
    NSString *matchImageURL = self.profile.basicInfo.imageUrlString;
    NSArray *imageURLStrings = @[matchImageURL,userProfileImageURL];
    [self.sparkPackageView showHasLikedBeforeAlertWithImageURLStrings:imageURLStrings];
    
    [self trackSparkNowEventStatus:@"viewed"];*/
}

- (void)showHasReachedLikeLimitSparkAlertNudge {
    NSString *toastText;
    if([[TMUserSession sharedInstance].user isUserFemale]) {
        toastText = @"Hey! You've run out of likes for the day. Spark him instead.";
    }else {
        toastText = @"Hey! You've run out of likes for the day. Spark her instead.";
    }
    [TMToastView showToastInParentView:self.view
                              withText:toastText withDuaration:4
                 presentationDirection:TMToastPresentationFromBottom];
}

-(void)showWhyNotSparkAlertNudge {
    if(!self.sparkPackageView) {
        self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
        self.sparkPackageView.delegate = self;
    }
    NSString *userProfileImageURL = [TMUserSession sharedInstance].user.profilePicURL ? [TMUserSession sharedInstance].user.profilePicURL : @"";
    NSString *matchImageURL = self.profile.basicInfo.imageUrlString ? self.profile.basicInfo.imageUrlString : @"";
    NSArray *imageURLStrings = @[matchImageURL,userProfileImageURL];
    [self.sparkPackageView showSparkNowAlertViewWithImageURLStrings:imageURLStrings];
    
    [self trackSparkNowEventStatus:@"viewed"];
}

#pragma mark TMSystemMessageViewDelegate Handler
#pragma mark -

-(void)handleActionOnSystemMessageLinkType:(TMSystemMessageLinkType)systemMsgLink {
    if(systemMsgLink == SYSTEM_MESSAGE_LINK_SHOWMATCHES) {
        TMSystemMessageView *systemMsgView = (TMSystemMessageView*)[self.view viewWithTag:10002];
        [systemMsgView removeFromSuperview];
    }
    else if(systemMsgLink == SYSTEM_MESSAGE_LINK_TRUSTBUILDER) {
        TMTrustBuilderViewController *trustbuilderViewCon = [self trustBuilderViewcontrollerWithMessage:nil];
        [self.navigationController pushViewController:trustbuilderViewCon
                                             animated:NO];
    }
    else if(systemMsgLink == SYSTEM_MESSAGE_LINK_TYPE_PHOTOUPLOAD) {
        TMPhotoViewController *photoViewCon = [self photoViewControllerWithMessage:nil];
        [self.navigationController pushViewController:photoViewCon
                                             animated:NO];
    }
    else if(systemMsgLink == SYSTEM_MESSAGE_TYPE_INVITEFRIEND) {
        TMInviteFriendViewController *inviteFriendViewController = [[TMInviteFriendViewController alloc] init];
        inviteFriendViewController.delegate = self;
        [self presentViewController:inviteFriendViewController animated:true completion:^{
            
        }];
    }
}

#pragma mark TMProfileCollectionView delegate
#pragma mark -

-(void)checkAndCacheSelectProfileOnBuyAction {
    if(self.profile.selectInfo.isSelectUser) {
        
    }
}
-(void)updateSelectBuyActionOnSelectProfileStatus {
    [TMUserSession sharedInstance].selectBuyActionOnSelectProfile = TRUE;
}
-(void)handleSelectQuizCellClick {
    if([[TMUserSession sharedInstance].user isSelectUser]) {
        NSURL *matchImageURL = [NSURL URLWithString:self.profile.basicInfo.imageUrlString];
        NSInteger matchId = self.profile.matchId.integerValue;
        NSString *headerTitle = [self.profile.selectInfo getCommonText];
        NSString *headerSubtitle = self.profile.selectInfo.quotesText;
        
        TMSelectQuizResultViewController *selectQuizResultViewController = [[TMSelectQuizResultViewController alloc] initWithMatchId:matchId matchImageURL:matchImageURL headerTitle:headerTitle headerSubTitle:headerSubtitle];
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        selectQuizResultViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self presentViewController:selectQuizResultViewController animated:TRUE completion:nil];
    }
    else {
        [self updateSelectBuyActionOnSelectProfileStatus];
        [self moveToBuySelectFromSource:@"select_detail_card_profile:matches"];
    }
    
    NSDictionary *trackInfo = @{@"source":@"matches"};
    [self trackSelectEvent:@"select_detail_card_profile" withStatus:@"clicked" userInfo:trackInfo];
}
- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
fullScreenPhotoViewAction:(NSArray *)images
          currentIndex:(NSInteger)index {
    TMProfile *matchProfile = self.profile;
    if(matchProfile.isProfileAdCampaign && matchProfile.matchId != nil) {
        // call click trackers
        NSDictionary* matchDetails = @{@"source":@"matches"};
        if(matchProfile.matchId){
            matchDetails = @{@"source":@"matches",@"match_id":matchProfile.matchId};
        }
        [self.adTrackerManager trackClickUrls:matchProfile.adObj.clickTracker eventInfo:matchDetails];
        
        if(matchProfile.adObj && matchProfile.adObj.isLandingUrlAvailable) {
            //open web view with landing url
            [self showAdWebView:matchProfile];
        }else {
            TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] initWithReportUserOption:true];
            fullScreenViewCon.currentIndex = index;
            fullScreenViewCon.delegate = self;
            [self.navigationController pushViewController:fullScreenViewCon animated:YES];
            [fullScreenViewCon loadImgArray:images];
        }
    }
    else if(matchProfile.matchId != nil){
        TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] initWithReportUserOption:true];
        fullScreenViewCon.currentIndex = index;
        fullScreenViewCon.delegate = self;
        [self.navigationController pushViewController:fullScreenViewCon animated:YES];
        [fullScreenViewCon loadImgArray:images];
    }
}

- (void)collectionView:(TMProfileCollectionView *)collectionView
                layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
commonFacebookFriendAction:(UIView*)actionView
     mutualConnections:(NSArray *)connections{
    
    CGRect convertedRect = [self.collectionView convertRect:actionView.frame toView:self.view];
    
    CGFloat xPos = CGRectGetMaxX(convertedRect) - (CGRectGetWidth(convertedRect)/2) - 108;
    CGFloat yPos = CGRectGetMinY(convertedRect) + CGRectGetHeight(convertedRect)/2;
    CGRect rect = CGRectMake(xPos,
                             yPos,
                             108,
                             44*3);
    
    self.popoverView = [[TMPopOverView alloc] initWithFrame:rect];
    self.popoverView.delegate = self;
    [self.view addSubview:self.popoverView];
    [self.popoverView loadData:connections withActionViewFrame:actionView.frame];
    
    self.popoverView.baseView.frame = self.view.bounds;
    [self.view addSubview:self.popoverView.baseView];
    [self.view bringSubviewToFront:self.popoverView];
}

-(void)collectionView:(TMProfileCollectionView *)collectionView
               layout:(TMProfileCollectionViewFlowLayout *)collectionViewLayout
   didTrustScoreClick:(BOOL)isOpen
{
    if(isOpen) {
        [self trackActivityDashboard:@"trustscore" status:@"open"];
    }else {
        [self trackActivityDashboard:@"trustscore" status:@"close"];
    }
}

#pragma mark - TMSideMenuViewControllerDelegate
-(void)didClickTMFrills {
    [self resetSideMenu];
    if([[TMIAPManager sharedManager] canConnect]) {
        [self showSparkPackages];
        self.sparkPackageView.didShowFromIntro = FALSE;
        self.sparkPackageView.didShowFromSideMenu = TRUE;
        [self trackBuySparkEventViewStatus];
    }
    else {
        [self showMatchResponseErrorAlert:TM_INTERNET_NOTAVAILABLE_MSG msg:nil];
    }
}

#pragma mark - Show Web view on ad click
- (void)showAdWebView:(TMProfile *)matchProfile {
    //add the web view
    NSString* adLandringURLString = matchProfile.adObj.landingUrl;
    
    NSString* webViewTitle = @"";
    
    self.webViewController = [[TMNativeAdWebViewController alloc] initWithNibName:@"TMNativeAdWebViewController" bundle:nil urlString:adLandringURLString webViewTitle:webViewTitle];
    [self presentViewController:self.webViewController animated:YES completion:nil];
}

#pragma mark - TMFullScreenPhotoViewController Delegate Handler
#pragma mark -

-(void)didReportUserPhotoWithReason:(NSString *)reason {
    [self.navigationController popToViewController:self animated:false];
    [self.dataController sendUserActionOnMatchProfile:self.profile.matchId
                                            actionURL:self.profile.hideUrl
                                    moveToNextProfile:^(BOOL moveToNextProfile) {
                                        
                                        [self moveToNextMatchProfileWithReportUserAction];
                                        
                                    } response:^(NSDictionary *mutualData) {
                                        
                                        
                                    } error:^(TMError *error) {
                                        
                                        [self showMatchResponseErrorAlert:TM_INTERNET_NOTAVAILABLE_MSG
                                                                      msg:@"Error in reporting user photo"];
                                    }];
}

#pragma mark - Error Alert Handler
#pragma mark -

-(void)showMatchResponseErrorAlert:(NSString*)title
                               msg:(NSString*)msg  {
    self.alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [self.alertView show];
}


#pragma mark Remote Notification Navigation
#pragma mark -

-(void)notificationRecieved:(NSNotification*)notification {
    if(self.playerView){
        [self removeVideoPlayer];
    }
    NSDictionary *dictionary = notification.userInfo;
    [self handleNotification:dictionary];
}

-(void)handleNotification:(NSDictionary *)notificationDictionary {
    
    //remove any presented view controller
    if (self.navigationController.topViewController.presentedViewController) {
        [self.navigationController.topViewController dismissViewControllerAnimated:FALSE completion:nil];
    }
    
    TMNotificationReciever *notificationReciever = [notificationDictionary objectForKey:@"notificationData"];
    NSString *pushType = notificationReciever.pushType;
    
    TMLOG(@"Received Notification:%@",pushType);
    
    if ([pushType isEqualToString:@"TRUST"]) {
        [self navigateToViewControllerWithType:TMTRUSTBUILDERVIEWCONTROLLER animated:false notificationReceiver:notificationReciever];
    }
    else if ([pushType isEqualToString:@"MESSAGE"]) {
        if([notificationReciever.isAdminMessage boolValue]) {
            TMSideMenuViewController *sideMenuViewController = (TMSideMenuViewController*) self.slidingViewController.underLeftViewController;
            if(![sideMenuViewController isFeedbackViewControllerActive]) {
                [self.navigationController popToViewController:self animated:false];
                [sideMenuViewController launchFeedbackViewController];
            }
        }
        else {
            // one on one message
            [self navigateToViewControllerWithType:TMMESSAGELISTVIEWCONTROLLER animated:false notificationReceiver:notificationReciever];
        }
    }
    else if ([pushType isEqualToString:@"MATCH"]) {
        // mutual match
    }
    else if([pushType isEqualToString:@"PHOTO"]) {
        //photo
        [self navigateToViewControllerWithType:TMPHOTOVIEWCONTROLLER animated:false notificationReceiver:notificationReciever];
    }
    else if([pushType isEqualToString:@"PREFERENCES"]) {
        //edit preference
        [self navigateToViewControllerWithType:TMEDITPREFERENCEVIEWCONTROLLER animated:false notificationReceiver:notificationReciever];
    }
    else if ([pushType isEqualToString:@"MYPROFILE"]) {
        [self moveToMyProfile:FALSE];
    }
    else if ([pushType isEqualToString:@"BASICS"]) {
        [self.navigationController popToViewController:self animated:false];
        /*TMRegisterBasicsViewController *regBasics = [[TMRegisterBasicsViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
        regBasics.delegate = self;
        regBasics.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:regBasics animated:YES];*/
    }
    else if ([pushType isEqualToString:@"HASHTAGS"]) {
        [self.navigationController popToViewController:self animated:false];
        TMHashTagTextViewController *invc = [[TMHashTagTextViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE registerData:nil showBar:false];
        invc.delegate = self;
        invc.actionDelegate = self.actionDelegate;
        [self.navigationController pushViewController:invc animated:YES];
    }
    else if([pushType isEqualToString:@"CONVERSATION_LIST"]) {
        [self.navigationController popToViewController:self animated:false];
        [self chatAction];
    }
    else if([pushType isEqualToString:@"APP_VIRALITY"] || [pushType isEqualToString:@"APP_VIRALITY_EARNINGS"]) {
        [self showAppViralityGrowthHackViewController];
    }
    else if([pushType isEqualToString:@"EVENT"]) {
        NSString *eventId = notificationReciever.eventId;
        if(eventId){
            [self launchEventDetailWithEventId:eventId];
        }
    }
    else if([pushType isEqualToString:@"EVENT_LIST"]) {
        NSString *categoryId = notificationReciever.categoryId;
        NSString *title = notificationReciever.categoryTitle;
        if(categoryId){
            [self launchEventListWithCategoryId:categoryId title:title];
        }
    }
    else if([pushType isEqualToString:@"SOCIALS"]) {
        [self.navigationController popToViewController:self animated:false];
        [self categoryViewAction];
    }
    else if([pushType isEqualToString:@"BUY_SPARKS"]) {
        [self.navigationController popToViewController:self animated:false];
        [self showSparkPackages];
    }
    else if([pushType isEqualToString:@"BUY_SELECT"]) {
        [self.navigationController popToViewController:self animated:false];
        [self moveToBuySelectFromSource:@"on_matches_launch"];
    }
}

-(void)navigateToViewControllerWithType:(TMViewControllerType)viewControllerType
                               animated:(BOOL)animated
                   notificationReceiver:(TMNotificationReciever*)notificationReceiver {
    
    UIViewController *viewController = nil;
    
    BOOL needToPushViewController = TRUE;
    
    if (viewControllerType == TMTRUSTBUILDERVIEWCONTROLLER) {
        viewController = [self trustBuilderViewcontrollerWithMessage:nil];
    }
    else if (viewControllerType == TMPHOTOVIEWCONTROLLER) {
        if(notificationReceiver.showRejectToastOnPhoto) {
            viewController = [self photoViewControllerWithMessage:TM_PHOTO_FLOW_NOTIFICATION_MSG];
        }
        else {
            viewController = [self photoViewControllerWithMessage:nil];
        }
    }
    else if(viewControllerType == TMMATCHPROFILEVIEWCONTROLLER) {
        viewController = [self messageProfileViewController];
    }
    else if(viewControllerType == TMMESSAGELISTVIEWCONTROLLER) {
        TMConversation *msgConversation = [[TMConversation alloc] init];
        msgConversation.fullConvLink = notificationReceiver.url;
        msgConversation.matchId = notificationReceiver.senderId;
        msgConversation.seen = FALSE;
        msgConversation.sender = MESSAGESENDER_MATCH;
        msgConversation.isMsTm = [[TMMessagingController sharedController] isSenderIdMsTm:msgConversation.matchId];
        if(!self.messageViewController) {
            viewController = [self messageViewControllerWithMessageConversation:msgConversation];
            self.messageViewController = (TMMessageListViewController*)viewController;
        }
        else {
            [self.messageViewController showMessageConversation:msgConversation];
            needToPushViewController = FALSE;
        }
    }
    else if(viewControllerType == TMEDITPREFERENCEVIEWCONTROLLER) {
        viewController = [self editPreferenceViewController];
    }
    if(needToPushViewController) {
        TMLOG(@"2/navigateToViewControllerWithType");
        //pop to matchview controller
        //push new controller
        [self.navigationController popToViewController:self animated:false];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:viewController animated:animated];
        });
    }
}

#pragma mark ViewController Navigation
#pragma mark -

-(id<TMViewControllerActionDelegate>)getActionDelegate {
    TMSlidingViewController *slidingVC = (TMSlidingViewController*)self.slidingViewController;
    return slidingVC.actionDelegate;
}
-(TMTrustBuilderViewController*)trustBuilderViewcontrollerWithMessage:(NSString*)messgaeString {
    TMTrustBuilderViewController *trustViewCon = [[TMTrustBuilderViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
    trustViewCon.delegate = self;
    trustViewCon.actionDelegate = [self getActionDelegate];
    return trustViewCon;
}
-(TMEditPreferenceViewController*)editPreferenceViewController {
    TMEditPreferenceViewController *editPreference = [[TMEditPreferenceViewController alloc] init];
    editPreference.actionDelegate = [self getActionDelegate];
    return editPreference;
}
-(TMPhotoViewController*)photoViewControllerWithMessage:(NSString*)messageString {
    TMPhotoViewController *photoViewCon = NULL;
    if(messageString) {
        photoViewCon = [[TMPhotoViewController alloc] initForFemaleProfileRejectionWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE alertMessage:messageString];
    }
    else {
        photoViewCon = [[TMPhotoViewController alloc] initWithNavigationFlow:TMNAVIGATIONFLOW_EDITPROFILE];
    }
    photoViewCon.delegate = self;
    photoViewCon.actionDelegate = [self getActionDelegate];
    return photoViewCon;
}
-(UIViewController*)messageProfileViewController {
    TMMessageProfileViewController *messageProfileViewControllr = [[TMMessageProfileViewController alloc] init];
    messageProfileViewControllr.actionDelegate = [self getActionDelegate];
    return messageProfileViewControllr;
}
-(TMMessageListViewController*)messageViewControllerWithMessageConversation:(TMConversation*)messsageConversation {
    TMMessageListViewController *messageViewController = nil;
    messageViewController = [[TMMessageListViewController alloc] initWithNavigationToMessgaeConversation:messsageConversation];
    messageViewController.delegate = self;
    messageViewController.actionDelegate = [self getActionDelegate];
    return messageViewController;
}

-(void)didPopViewController:(TMViewControllerType)viewControllerType {
    if(viewControllerType == TMMESSAGELISTVIEWCONTROLLER) {
        self.messageViewController = nil;
    }
}
-(void)showAppViralityGrowthHackViewController {
    [self.navigationController popToViewController:self animated:false];
    if(self.webViewController) {
        [self.webViewController dismissViewControllerAnimated:false completion:^{
            self.webViewController = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                [TMAppViralityController showGrowthHackScreenFromController:self.navigationController];
            });
        }];
    }else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [TMAppViralityController showGrowthHackScreenFromController:self.navigationController];
        });
    }
}
-(void)handleDeepLink:(NSNotification*)notification {
    [self.navigationController popToRootViewControllerAnimated:FALSE];
    if(self.webViewController) {
        [self.webViewController dismissViewControllerAnimated:FALSE completion:nil];
    }
    NSDictionary *userInfo = notification.userInfo;
    if(userInfo) {
        NSString *target = userInfo[@"target"];
        
        if([target isEqualToString:@"buysparks"]) {
            [self showSparkPackages];
        }
        else if([target isEqualToString:@"buyselect"]) {
            [self moveToBuySelectFromSource:@"deep_link"];
        }
        else if([target isEqualToString:@"trustbuilder"]) {
            [self navigateToViewControllerWithType:TMTRUSTBUILDERVIEWCONTROLLER animated:FALSE notificationReceiver:nil];
        }
        else if([target isEqualToString:@"conversation"]) {
            [self chatAction];
        }
        else if([target isEqualToString:@"myprofile"]) {
            [self moveToMyProfile:FALSE];
        }
    }
}


#pragma mark - Notification Handler

-(void)appWillEnterForeground:(NSNotification*)notification {
    if(self.appDidEnterBackground) {
        self.appDidEnterBackground = false;
        TMUserSession *userSession = [TMUserSession sharedInstance];
        [userSession sendDeviceTokenForLoginEvent];
        
        if([self isTopMostViewController]) {
            TMLOG(@"appWillEnterForeground");
            [self loadMatchDataWithForceRefresh:FALSE];
        }
        else {
            TMUserSession *userSession = [TMUserSession sharedInstance];
            userSession.needToLoadMatchData = TRUE;
        }
    }
    
    [self fetchNotificationCount];
    
    //reset the side menu
    [self resetSideMenu];
    
    [self configureSocket];
    
    [self resetPhotoVisibiltyAlertForLike];
    
    [self showSelectLongTermNudge];
}

-(BOOL)isLaunchFromEventURL {
    if([TMDataStore containsObjectForKey:@"openEventForId"]) {
        return true;
    }
    return false;
}
-(BOOL)isLaunchFromBuySparkDeepLink {
    if([TMDataStore containsObjectForKey:@"openEventForId"]) {
        return true;
    }
    return false;
}
-(void)appDidEnterBackground:(NSNotification*)notification {
    self.appDidEnterBackground = TRUE;
    if(self.actionSheet) {
        [self.actionSheet dismissWithClickedButtonIndex:0 animated:FALSE];
        self.actionSheet = nil;
    }
    if(self.playerView) {
        [self removeVideoPlayer];
    }
    [self didRemoveSparkView];
}
-(void)eventMatchAction:(NSNotification*)notification {
    NSDictionary *dictionary = notification.userInfo;
    NSInteger matchId = [dictionary[@"matchid"] integerValue];
    [self.dataController markMatchProfileAsShown:matchId];
    [self loadMatchDataWithForceRefresh:FALSE];
}
-(void)pushToEventViewControllerWithEventId:(NSNotification*)notification {
    if(self.playerView) {
        [self removeVideoPlayer];
    }
    
    TMLOG(@"open event function called");
    NSDictionary *dictionary = notification.userInfo;
    NSString *event_id = [dictionary objectForKey:@"event_id"];
    if(![event_id isEqualToString:@""]) {
        [self launchEventDetailWithEventId:event_id];
    }
}
-(void)launchEventDetailWithEventId:(NSString *)event_id {
    [self.navigationController popToViewController:self animated:false];
    TMEventDetailViewController *eventDetailViewCon = [[TMEventDetailViewController alloc] initWithEventId:event_id];
    eventDetailViewCon.delegate = self;
    eventDetailViewCon.actionDelegate = [self actionDelegate];
    [self.navigationController pushViewController:eventDetailViewCon animated:true];
}
-(void)launchEventListWithCategoryId:(NSString *)categoryId title:(NSString *)title {
    [self.navigationController popToViewController:self animated:false];
    
    TMSideMenuViewController *sideMenuVC = (TMSideMenuViewController*)self.slidingViewController.underLeftViewController;
    [sideMenuVC launchEventListWithCategoryId:categoryId];
}

-(void)clearMatchCacheData {
    [self.dataController clearCacheData];
}
-(void)configureSocket {
    TMUserSession *sharedSession = [TMUserSession sharedInstance];
    if(sharedSession.user.userId) {
        TMMessagingController *messagingController = [TMMessagingController sharedController];
        [messagingController connectChat];
        
        [messagingController fetchSparkData];
        
        ///mark photo messages in sending state as failed
        [messagingController cancelPhotoMessgaesInSendingStateAndMarkAsFailed];
    }
}


#pragma mark delegate -

-(void)didTapPopOverView {
    [self.popoverView resetPopOverView];
    [self.popoverView removeFromSuperview];
    self.popoverView = nil;
}

-(void)didDismissViewController {
    [self.presentedViewController dismissViewControllerAnimated:false completion:^{
        [FBWebDialogs
         presentRequestsDialogModallyWithSession:nil
         message:NSLocalizedString(@"FBinviteMessage", nil)
         title:nil
         parameters:nil
         handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
             //TMLOG(@"Result:%lu URL:%@  Error:%@",result,resultURL,error)
             if(resultURL && !error) {
                 TMSystemMessageView *systemMsgView = [self systemMessageViewWithSystemMessageLink:SYSTEM_MESSAGE_TYPE_INVITEFRIEND];
                 [systemMsgView showInviteFriendThanksMessage];
             }
         }];
    }];
}

- (void)didUploadProfilePicture
{
    if ([[TMUserSession sharedInstance].user isUserFemale]) {
        //Need to reload match here
        //TODO::Abhijeet:: reset the cache
        [TMUserSession sharedInstance].user.isFemaleProfileRejected = NO;
    }
    //show the toast view from bottom
    [TMToastView showToastInParentView:self.navigationController.topViewController.view withText:@"Hoping your pics meet our easy guidelines." withDuaration:4 presentationDirection:TMToastPresentationFromBottom];
}

-(void)didEditProfile {
    
}

#pragma mark - Animation Methods

-(void)animateMatchActionViewTutorial {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    [userSession registerMatchActionViewAnimation];
    
    //show animation
    CGRect frame = self.view.frame;
    CGRect actionViewFrame = self.matchActionView.frame;
    
    self.matchActionView.frame = CGRectMake(CGRectGetMinX(frame),
                                            CGRectGetHeight(frame),
                                            CGRectGetWidth(frame),
                                            CGRectGetHeight(actionViewFrame));
    
    //added just as layer on top of match view
    TMAnimationView *animatonView = [[TMAnimationView alloc] initWithFrame:self.view.bounds];
    animatonView.tag = TMMATCHACTION_ANIMATIONVIEW_TAG;
    [self.view addSubview:animatonView];
    
    [UIView animateWithDuration:0.3
                          delay: 0.3
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         self.matchActionView.frame = CGRectMake(CGRectGetMinX(frame),
                                                                 CGRectGetHeight(frame) - CGRectGetHeight(actionViewFrame),
                                                                 CGRectGetWidth(frame),
                                                                 CGRectGetHeight(actionViewFrame));
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    /////////////////////////////////////////////
    UIView *tutorialView = [animatonView getMatchActionTutorialPointerView];
    [animatonView addSubview:tutorialView];
    tutorialView.frame = CGRectMake((self.view.frame.size.width-tutorialView.frame.size.width)/2,
                                    CGRectGetHeight(frame) - CGRectGetHeight(actionViewFrame)-160,
                                    CGRectGetWidth(tutorialView.frame),
                                    CGRectGetHeight(tutorialView.frame));
    tutorialView.alpha = 0.0;
    
    [UIView animateWithDuration:0.5
                          delay: 0.6
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         tutorialView.alpha = 1;
                         tutorialView.frame = CGRectMake((self.view.frame.size.width-tutorialView.frame.size.width)/2,
                                                         CGRectGetHeight(frame) - CGRectGetHeight(actionViewFrame)-110,
                                                         CGRectGetWidth(tutorialView.frame),
                                                         CGRectGetHeight(tutorialView.frame));
                         
                     }
                     completion:^(BOOL finished){
                         //TutorialPointer
                         [UIView animateWithDuration:0.5
                                               delay: 2.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              tutorialView.alpha = 0;
                                          }
                                          completion:^(BOOL finished){
                                              [animatonView removeFromSuperview];
                                              [tutorialView removeFromSuperview];
                                          }];
                     }];
    
}

-(void)animateMutualLikeViewTutorial {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    [userSession registerMutualLikeAnimation];
    
    ////show animation
    TMAnimationView *animatonView = [[TMAnimationView alloc] initWithFrame:self.view.bounds];
    [animatonView showMutualLikeTutorialOnBaseView:self.view];
}

-(void)showActivityDashBoardScreen:(TMActivityDashboard*)activityDashboard {
    [self.completionToast setPiActivityCount];
    self.nudgeDashboardScreen = [[TMNudgeScreen alloc] initWithFrame:self.view.bounds];
    self.nudgeDashboardScreen.delegate = self;
    [self.nudgeDashboardScreen showActivityDashboard:self.activityDashboard];
    [self.view addSubview:self.nudgeDashboardScreen];
    [self.view bringSubviewToFront:self.nudgeDashboardScreen];
    [self trackActivityDashboard:@"pi_activity_shown" status:@"success"];
}

-(void)showEditNudgeScreen:(NSString*)gender nudgeView:(TMNudgeView)nudgeView {
    if(self.nudgeMessage || self.nudgeTrustMessage) {
        NSString *msg = self.nudgeMessage;
        if(nudgeView == TMTrustBuilderView) {
            msg = self.nudgeTrustMessage;
        }
        self.nudgeScreen = [[TMNudgeScreen alloc] initWithFrame:self.view.bounds];
        self.nudgeScreen.delegate = self;
        [self.nudgeScreen showNudgeScreen:gender nudgeView:nudgeView textMessage:msg trustScore:self.trustScore];
        [self.view addSubview:self.nudgeScreen];
        [self.view bringSubviewToFront:self.nudgeScreen];
        [self trackUserActionOnNudgeScreen:nudgeView eventStatus:@"shown"];
        
        [self.completionToast setNudgeCount];
    }
}

- (BOOL) isCompletionNudgeShown {
    for (UIView* subView in self.view.subviews) {
        if ([subView isKindOfClass:[TMNudgeScreen class]]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - TMQuizViewControllerDelegate methods
#pragma mark -
-(void)didCompleteQuiz {
    [self dismissViewControllerAnimated:TRUE completion:^{
    }];
}



#pragma mark - Profile Visibility methods

-(void)presentProfileVisibilityAlertView:(BOOL)from_like_action
{
    NSString *msg = TM_PHOTO_FLOW_LIKE_MSG;
    NSString *eventType = @"overlay_no_uploaded_pic";
    
    if(!from_like_action) {
        if(!self.isValidProfilePic){
            if(!self.hasPhotos){
                msg = TM_PHOTO_FLOW_REJECT_MSG;
                eventType = @"overlay_rejected_photo";
            }else {
                msg = TM_NO_PHOTO_MSG;
            }
        }
    }else {
        eventType = @"overlay_like_button";
    }
    
    self.profileAlertView = [[TMProfileVisibilityAlertView alloc] initWithMessage:msg frame:[[UIScreen mainScreen] bounds] fromLike:from_like_action];
    if(self.profileAlertView) {
        self.profileAlertView.alertViewDelegate = self;
        [[[UIApplication sharedApplication] keyWindow] addSubview:self.profileAlertView];
        [self trackPhotoFlowEvent:eventType];
    }
}

#pragma mark - TMProfileVisibilityAlertView Delegate method

-(void)didRemoveView:(BOOL)fromLike
{
    if(self.profileAlertView)
    {
        [self.profileAlertView removeFromSuperview];
        self.profileAlertView = nil;
    }
    [self showProfileVisibilityView:!fromLike];
}
-(void)didNotNowClick {
    if(self.profileAlertView)
    {
        [self.profileAlertView removeFromSuperview];
        self.profileAlertView = nil;
    }
}
- (void) showProfileVisibilityView:(BOOL)showPrivacy
{
    if(self.profilePrv)
    {
        [self.profilePrv removeFromSuperview];
        self.profilePrv = nil;
    }
    
    self.profileVisibilityView = [[TMProfileVisibilityView alloc] initWithFrame:self.view.bounds showPrivacy:showPrivacy];
    self.profileVisibilityView.delegate = self;
    [self.view addSubview:self.profileVisibilityView];
    [self.view bringSubviewToFront:self.profileVisibilityView];
}

-(void)didCancelVisibilityNudge:(BOOL)showPrivacy {
    [self.profileVisibilityView removeFromSuperview];
    self.profileVisibilityView = nil;
    
    if(showPrivacy && !self.profilePrv) {
        self.profilePrv = [[TMProfilePrivacy alloc] initWithFrame:self.view.bounds];
        self.profilePrv.delegate = self;
        [self.view addSubview:self.profilePrv];
        [self.view bringSubviewToFront:self.profilePrv];
        [self trackPhotoFlowEvent:@"pv_nudge_skip"];
    }
}

#pragma mark TMProfilePrivacyDelegate methods

-(void)didAddProfileClick
{
    [self showProfileVisibilityView:false];
    if(self.profileVisibilityView){
        [self trackPhotoFlowEvent:@"privacy_add_pic"];
        [self.profileVisibilityView showOptionsToUploadPhoto];
    }
}
-(void)didSkipClick
{//////
    if(self.profilePrv){
        [self.profilePrv removeFromSuperview];
        self.profilePrv = nil;
        [self trackPhotoFlowEvent:@"privacy_no_thanks"];
    }
}

#pragma mark - edit profile nudge deleage methods

-(void)didMaybeLaterClick:(TMNudgeView)nudgeView {
    if(nudgeView == TMProfileView) {
        self.noOfProfilesHasHide = 0;
    }
    else if(nudgeView == TMTrustBuilderView) {
        self.noOfProfilesHasLiked = 0;
    }
    [self.nudgeScreen removeFromSuperview];
    self.nudgeScreen = nil;
    [self trackUserActionOnNudgeScreen:nudgeView eventStatus:@"cancel"];
}

-(void)didContinueClick:(TMNudgeView)nudgeView {
    [self.nudgeScreen removeFromSuperview];
    self.nudgeScreen = nil;
    if(nudgeView == TMTrustBuilderView) {
        self.noOfProfilesHasLiked = 0;
        TMTrustBuilderViewController *trustbuilderViewCon = [self trustBuilderViewcontrollerWithMessage:nil];
        [self.navigationController pushViewController:trustbuilderViewCon
                                             animated:NO];
    }
    else if (nudgeView == TMProfileView) {
        self.noOfProfilesHasHide = 0;
        [self moveToMyProfile:true];
    }
    [self trackUserActionOnNudgeScreen:nudgeView eventStatus:@"continue"];
}

-(void)didPhotoContinueClick {
    [self.nudgeDashboardScreen removeFromSuperview];
    self.nudgeDashboardScreen = nil;
    TMPhotoViewController *photoViewCon = [self photoViewControllerWithMessage:nil];
    photoViewCon.fromPIDashboard = true;
    [self.navigationController pushViewController:photoViewCon
                                         animated:NO];
    [self trackActivityDashboard:@"pi_activity_continue_photo" status:@"success"];
}

-(void)moveToMyProfile:(BOOL)fromNudge {
    TMSideMenuViewController *sideMenuVC = (TMSideMenuViewController*)self.slidingViewController.underLeftViewController;
    [sideMenuVC launchMyProfileViewController:fromNudge];
}

-(void)moveToBuySelectFromSource:(NSString*)source {
    TMSideMenuViewController *sideMenuVC = (TMSideMenuViewController*)self.slidingViewController.underLeftViewController;
    [sideMenuVC launchBuySelectViewControllerFromSource:source];
}

-(void)moveToEditPreferencesForNRIProfile {
    TMSideMenuViewController *sideMenuVC = (TMSideMenuViewController*)self.slidingViewController.underLeftViewController;
    [sideMenuVC launchEditPreferencesViewController];
}
// profile visibility code starts
-(void)didPhotoClick {
    [self showOptionsToAddphoto];
    [self trackPhotoFlowEvent:@"add_photo_click"];
}
-(void)didUploadPhotoSuccessfully {
    [self loadMatchDataWithForceRefresh:TRUE];
}

-(void)showOptionsToAddphoto {
    self.actionSheet = NULL;
    self.isCameraPresent  = [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera];
    
    if (self.isCameraPresent) {
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library",@"Camera", nil];
    }
    else {
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", nil];
    }
    
    [self.actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) { //photo lib
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    else if(buttonIndex == 1 && self.isCameraPresent) {
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    }
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self.profileVisibilityView;
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        imagePickerController.showsCameraControls = YES;
    }
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)addPhotoEditorWithImage:(UIImage*)image {
    DZNPhotoEditorViewController *editor = [[DZNPhotoEditorViewController alloc] initWithImage:image];
    editor.cropMode = DZNPhotoEditorViewControllerCropModeSquare;
    if(image.size.width > image.size.height) {
        editor.cropMode = DZNPhotoEditorViewControllerCropModeCustom;
        editor.cropSize = CGSizeMake(CGRectGetWidth(self.view.frame), (CGRectGetWidth(self.view.frame)*image.size.height)/image.size.width);
    }
    
    [editor setAcceptBlock:^(DZNPhotoEditorViewController *editor, NSDictionary *userInfo){
        
        UIImage *editImage = [userInfo valueForKey:UIImagePickerControllerEditedImage];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if(self.profileVisibilityView) {
            [self.profileVisibilityView sendImageToServer:editImage];
        }
    }];
    
    [editor setCancelBlock:^(DZNPhotoEditorViewController *editor){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    // The view controller requieres to be nested in a navigation controller
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:editor];
    [self presentViewController:controller animated:YES completion:NULL];
}
-(void)showMissTMNudge:(NSString *)msg {
    [TMToastView showToastInParentViewWithMissTM:self.view withText:msg withDuration:4 presentationDirection:TMToastPresentationFromLeft];
}
-(void)showMutualEventNudge {
    for (UIView *subView in [self.view subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            [subView removeFromSuperview];
            break;
        }
    }
    if([self isTopMostViewController] && self.profile.isMutualEventAvail) {
        TMLOG(@"mutual event available");
        [TMToastView showToastInParentViewForMutualEvent:self.view withMutualEventData:self.profile.mutualEventArr withDuaration:4.0 presentationDirection:TMToastPresentationFromLeft];
    }
}

-(BOOL)canShowNRIProfileNudge {
    BOOL showNudge = FALSE;
    showNudge = [[TMUserSession sharedInstance] canShowNRINudge];
    return !showNudge;
}

-(void)showNRIProfileNudge {
    [[TMUserSession sharedInstance] setNRINudgeShown];
    if(!self.sparkPackageView) {
        self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
        self.sparkPackageView.delegate = self;
    }
    [self.sparkPackageView showNRIProfileNudgeAlertwithText];
    
}

-(void)showPhotoFlowToastForLikeAProfile {
    if([self isTopMostViewController] && self.profile.isPhotoVisibilityShown) {
        
        for (UIView *subView in [self.view subviews]) {
            if ([subView isKindOfClass:[TMToastView class]])
            {
                [subView removeFromSuperview];
                break;
            }
        }
        
        [TMToastView showToastInParentViewForPhotoFlow:self.view imageName:@"liked_icon_nudge" textMessage:@"Yay! She's already Liked you. Like her back and start UnSingling." withDuaration:4.0 presentationDirection:TMToastPresentationFromLeft removeAfterTime:false];
    }
}
-(BOOL)isHardUpdatePopUpShown:(NSString *)hardVersion {
    int current_version = [[[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey] intValue];
    int hard_version = [hardVersion intValue];
    return current_version<hard_version;
}
-(BOOL)isSoftUpdatePopUpShown:(NSString *)softVersion {
    int current_version = [[[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey] intValue];
    int soft_version = [softVersion intValue];
    if(current_version < soft_version) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        return [userSession isSoftUpdatePopShownMoreThanOneDay];
    }
    return false;
}

-(BOOL)canShowProfileVisibilityAlertOnLaunch {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    return [userSession canPhotoVisibilityShown];
}
-(void)resetPhotoVisibiltyAlertForLike {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    [userSession deregisterPhotoVisibilityAlertForLike];
}
-(BOOL)canShowProfileVisibilityAlertForLike {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    return [userSession canPhotoVisibilityShownForLike];
}
-(void)registerForLikePhotoFlow {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    [userSession registerPhotoVisibilityAlertForLike];
}

#pragma mark - Player Handling

-(void)removeVideoPlayer {
    if(self.playerView){
        [self.playerView deinit];
        [self.playerView removeFromSuperview];
        self.playerView = nil;
    }
}

-(void)didRemoveSendSparkView {
    [self.sendSparkView deinit];
    self.sendSparkView = nil;
}


#pragma mark - Spark
#pragma mark -

-(BOOL)canShowSelectNudge {
    BOOL canShowAlert = FALSE;
    BOOL isSelectUser = [[TMUserSession sharedInstance].user isSelectUser];
    BOOL isSelectEnabled = [[TMUserSession sharedInstance] isSelectEnabled];
    BOOL isSelectNudgeEnabled = [[TMUserSession sharedInstance] isSelectNudgeEnabled];
    if((!isSelectUser) && (isSelectEnabled) && (isSelectNudgeEnabled)) {
        NSString *key = @"com.match.selectnudgealert";
        NSDate *lastShownTime = [TMDataStore retrieveObjectforKey:key];
        if(!lastShownTime) {
            [TMDataStore setObject:[NSDate date] forKey:key];
            canShowAlert = TRUE;
        }
        else {
            NSInteger nudgeTimeInterval = [[TMUserSession sharedInstance] selectNudgeTimeInterval];
            NSDate *date = [NSDate date];
            NSTimeInterval mins = [[TMTimestampFormatter sharedFormatter] minsBetweenDate:lastShownTime andDate:date];
            NSInteger hours = (mins/60);
            if(hours >= nudgeTimeInterval) {
                [TMDataStore setObject:[NSDate date] forKey:key];
                canShowAlert = TRUE;
            }
        }
    }
    return canShowAlert;
}
-(BOOL)canShowWhyNotSparkAlert {
    BOOL canShowAlert = FALSE;
    BOOL enabled = [[TMUserSession sharedInstance] isWhyNotSparkPopupEnabled];
    BOOL isAdCampaignProfile = self.profile.isProfileAdCampaign;
    NSInteger count = [[TMUserSession sharedInstance] whyNotSparkPopupCount];
    if(!isAdCampaignProfile && enabled && self.noOfProfilesHasLiked == count) {
        NSString *key = @"com.match.sparknowalert";
        NSDate *lastShownTime = [TMDataStore retrieveObjectforKey:key];
        if(!lastShownTime) {
            [TMDataStore setObject:[NSDate date] forKey:key];
            canShowAlert = TRUE;
        }
        else {
            NSDate *date = [NSDate date];
            NSTimeInterval mins = [[TMTimestampFormatter sharedFormatter] minsBetweenDate:lastShownTime andDate:date];
            NSInteger hours = (mins/60);
            if(hours > 24) {
                [TMDataStore setObject:[NSDate date] forKey:key];
                canShowAlert = TRUE;
            }
        }
    }
    return canShowAlert;
}

#pragma mark - TMSparkBaseViewDelegate methods

-(void)didRemoveSparkView {
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}
-(void)didInitiatePaymentWithPackageId:(NSString*)packageId {
    [self trackBuySparkEventStatusForPkgId:packageId];
}
-(void)didCompletePaymentForPackageId:(NSString*)packageId sparkCount:(NSInteger)sparkCount show:(BOOL)chatAssistIcon{
    [self.buySparkViewController dismissViewControllerAnimated:FALSE completion:nil];
    self.buySparkViewController = nil;
    if(!self.sparkPackageView) {
        self.sparkPackageView = [[TMSparkPackageBuyView alloc] init];
        self.sparkPackageView.delegate = self;
    }
    [self.sparkPackageView showSparkPaymentCompletionWithNewSparkCount:sparkCount show:chatAssistIcon];
    [self trackBuySparkEventSuccessStatusForPkgId:packageId sparkCount:sparkCount];
}
-(void)didFailToCompletePaymentWithTrackingError:(NSString*)errorDescription {
    [self trackBuySparkEventErrorStatus:errorDescription];
}
-(void)didClickSparkTutorialKnowMoreButton {
    [self trackSparkNowEventStatus:@"send_spark"];
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    [self showSendSparkViewFromIntro:FALSE];
}
-(void)didClickSparkNowMayBeLaterButton {
    [self trackSparkNowEventStatus:@"maybe_later"];
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}
-(void)didClickSparkNowButton {
    [self trackSparkIntorShownEventWithStatus:@"tryit_clicked"];
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    [self showSendSparkViewFromIntro:TRUE];
}
-(void)didClickSparkNowButtonFromAlert {
    [self trackSparkIntorShownEventWithStatus:@"tryit_clicked"];
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    [self showSendSparkViewFromIntro:FALSE];
}
-(void)didTakeLikeNowActionOnHasLikedSparkAlert {
    [self trackSparkNowEventStatus:@"maybe_later"];
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    [self sendUserActionOnMatchProfile:self.profile.likeUrl withTransition:TMMATCH_TRANSITION_TYPE_LIKE];
}
-(void)didClickSparkPackageBuySuccessContinueButton {
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}
-(void)didClickLastSparkAlertBuyButton {
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    [self showSparkPackages];
}
-(void)didClickJoinSelectButton {
    NSDictionary *trackingInfo = [self.sparkPackageView getTrackingInfoDictionary];
    [self trackSelectEvent:self.sparkPackageView.eventName withStatus:@"clicked_yes" userInfo:trackingInfo];
    NSString *source = [NSString stringWithFormat:@"%@:%@",self.sparkPackageView.eventName,self.sparkPackageView.source];
    if([self.sparkPackageView.eventName isEqualToString:@"select_on_action_nudge"]) {
        [self updateSelectBuyActionOnSelectProfileStatus];
    }
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    
    [self moveToBuySelectFromSource:source];
}
-(void)didCancelJoinSelectNudge {
    NSDictionary *trackingInfo = [self.sparkPackageView getTrackingInfoDictionary];
    [self trackSelectEvent:self.sparkPackageView.eventName withStatus:@"clicked_no" userInfo:trackingInfo];
    
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}

-(void)didCancelShowNRIProfileNudge {
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
}

-(void)didEditPreferenceForNRIProfile {
    [self.sparkPackageView deinit];
    self.sparkPackageView = nil;
    
    [self moveToEditPreferencesForNRIProfile];
}
-(void)sendSparkActionWithMessage:(NSString*)sparkMessage {
    if([[TMMessagingController sharedController] canConnect]) {
        [self.sendSparkView setSendingState];
        [[TMMessagingController sharedController] sendSparkMessage:sparkMessage
                                                           toMatch:self.profile.matchId
                                                         sparkHash:self.profile.sparkHash
                                           isHasLikedBeforeProfile:self.profile.hasLikedBefore
                                                          response:^(BOOL status,NSString* errorDescription) {
            
          if(status) {
              self.noOfProfilesHasSparked++;
              [self.dataController markProfileAsShownForSendSparkActionWithMatchId:self.profile.matchId.integerValue];
              self.sparkCountLeft = [self getSparkCounterLeft];
              [self trackSendSparkEventWithStatus:@"send" errorMessage:nil];
              [self.sendSparkView deinit];
              self.sendSparkView = nil;
              [self moveToNextProfileWithSparkAnimation];
          }
          else {
              [self.sendSparkView resetToNormalState];
              [self showToastForErrorMsgOnWindow:@"Error in sending spark. Please try again!"];
              [self trackSendSparkEventWithStatus:@"send_error" errorMessage:errorDescription];
          }
        }];
        [self trackMatchTransitionType:TMMATCH_TRANSITION_TYPE_SPARK];
    }
    else {
        [self showMatchResponseErrorAlert:TM_INTERNET_NOTAVAILABLE_MSG msg:nil];
    }
}

-(void)showSparkPackages {
    if(!self.buySparkViewController.parentViewController) {
        self.buySparkViewController = nil;
        [self setupBuySparkViewController];
        [self.buySparkViewController showSparkPackages];
        [self presentViewController:self.buySparkViewController animated:TRUE completion:nil];
    }
}
-(void)moveToNextProfileWithSparkAnimation {
    [self moveToNextMatchProfileWithTransition:TMMATCH_TRANSITION_TYPE_SPARK];
}
-(NSInteger)getSparkCounterLeft {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    return [userSession getAvailableSparkCount];
}
-(void)showToastForErrorMsg:(NSString *)msg {
    [TMToastView showToastInParentView:self.view
                              withText:msg
                         withDuaration:4
                 presentationDirection:TMToastPresentationFromBottom];
}
-(void)showToastForErrorMsgOnWindow:(NSString *)msg {
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [TMToastView showToastInParentView:keyWindow
                              withText:msg
                         withDuaration:4
                 presentationDirection:TMToastPresentationFromBottom];
}

#pragma mark Tracking
#pragma mark -

-(void)trackMatchTransitionType:(TMMatchTransitionType)transitionType {
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    eventDict[@"screenName"] = @"TMMatchesViewController";
    eventDict[@"eventCategory"] = @"matches";
    
    NSString *selectStatus = ([[TMUserSession sharedInstance].user isSelectUser]) ? @"true" : @"false";
    NSString *profileSelectStatus = (self.profile.selectInfo.isSelectUser) ? @"true" : @"false";
    NSDictionary *eventInfo = @{@"is_select_member":selectStatus,@"is_profile_select":profileSelectStatus};
    
    NSString *eventAction;
    if(transitionType == TMMATCH_TRANSITION_TYPE_LIKE) {
        eventAction = @"like";
        eventDict[@"GA"] = @"true";
    }
    else if(transitionType == TMMATCH_TRANSITION_TYPE_HIDE) {
        eventAction = @"hide";
    }
    else if(transitionType == TMMATCH_TRANSITION_TYPE_SPARK) {
        eventAction = @"spark";
    }
    
    eventDict[@"GA"] = @"true";
    eventDict[@"eventAction"] = eventAction;
    if(eventInfo) {
        eventDict[@"event_info"] = eventInfo;
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}
-(void)trackUserActionOnNudgeScreen:(TMNudgeView)nudgeView eventStatus:(NSString*)eventStatus {
    //adding event tracking
    NSString *eventAction = @"";
    if(nudgeView == TMTrustBuilderView && [[TMUserSession sharedInstance].user isUserFemale]) {
        eventAction = trustBuilderFemaleNudge;
    }
    else if(nudgeView == TMProfileView && [[TMUserSession sharedInstance].user isUserFemale]){
        eventAction = myProfileFemaleNudge;
    }
    else if(nudgeView == TMTrustBuilderView && ![[TMUserSession sharedInstance].user isUserFemale]){
        eventAction = trustBuilderMaleNudge;
    }
    else if(nudgeView == TMProfileView && ![[TMUserSession sharedInstance].user isUserFemale]){
        eventAction = myProfileMaleNudge;
    }
    self.eventDict = [[NSMutableDictionary alloc] init];
    [self.eventDict setObject:@"TMMatchesViewController" forKey:@"screenName"];
    [self.eventDict setObject:@"matches" forKey:@"eventCategory"];
    [self.eventDict setObject:eventAction forKey:@"eventAction"];
    [self.eventDict setObject:eventStatus forKey:@"status"];
    [self.eventDict setObject:@"false" forKey:@"append403"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    self.eventDict = nil;
}
-(void)trackActivityDashboard:(NSString*)eventAction status:(NSString*)status {
    self.eventDict = [[NSMutableDictionary alloc] init];
    [self.eventDict setObject:@"TMMatchesViewController" forKey:@"screenName"];
    [self.eventDict setObject:@"matches" forKey:@"eventCategory"];
    [self.eventDict setObject:eventAction forKey:@"eventAction"];
    [self.eventDict setObject:status forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    self.eventDict = nil;
}
-(void)trackPhotoFlowEvent:(NSString *)event_type
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMMatchesViewController" forKey:@"screenName"];
    [eventDict setObject:@"photo_flow" forKey:@"eventCategory"];
    [eventDict setObject:event_type forKey:@"eventAction"];
    [eventDict setObject:@"success" forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}
-(void)trackVideoTutorial:(NSString *)status time_taken:(NSString *)time_taken {
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMMatchesViewController" forKey:@"screenName"];
    [eventDict setObject:@"matches" forKey:@"eventCategory"];
    [eventDict setObject:@"scenes_tutorial" forKey:@"eventAction"];
    [eventDict setObject:status forKey:@"status"];
    [eventDict setObject:time_taken forKey:@"time_taken"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}
-(void)trackSparkIntorShownEventWithStatus:(NSString*)status {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    if([status isEqualToString:@"tryit_clicked"]) {
        eventInfo[@"likes_done"] = @(self.noOfProfilesHasLiked);
        eventInfo[@"hides_done"] = @(self.noOfProfilesHasHide);
        eventInfo[@"sparks_done"] = @(self.noOfProfilesHasSparked);
        eventInfo[@"sparks_left"] = @(self.sparkCountLeft);
    }
    
    [[TMAnalytics sharedInstance] trackSparkEvent:@"intro" status:status eventInfo:eventInfo];
}
-(void)trackSparkNowEventStatus:(NSString*)status {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    
    eventInfo[@"likes_done"] = @(self.noOfProfilesHasLiked);
    eventInfo[@"hides_done"] = @(self.noOfProfilesHasHide);
    eventInfo[@"sparks_done"] = @(self.noOfProfilesHasSparked);
    eventInfo[@"sparks_left"] = @(self.sparkCountLeft);
    
    [[TMAnalytics sharedInstance] trackSparkEvent:@"has_liked_before" status:status eventInfo:eventInfo];
}
-(void)trackBuySparkEventViewStatus {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    eventInfo[@"likes_done"] = @(self.noOfProfilesHasLiked);
    eventInfo[@"hides_done"] = @(self.noOfProfilesHasHide);
    eventInfo[@"sparks_done"] = @(self.noOfProfilesHasSparked);
    eventInfo[@"sparks_left"] = @(self.sparkCountLeft);
    eventInfo[@"from_intro"] = (self.sparkPackageView.didShowFromIntro) ? @"true" : @"false";
    eventInfo[@"from_sidemenu"] = (self.sparkPackageView.didShowFromSideMenu) ? @"true" : @"false";
    [[TMAnalytics sharedInstance] trackSparkEvent:@"buy" status:@"view" eventInfo:eventInfo];
}
-(void)trackBuySparkEventStatusForPkgId:(NSString*)packageId {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    eventInfo[@"likes_done"] = @(self.noOfProfilesHasLiked);
    eventInfo[@"hides_done"] = @(self.noOfProfilesHasHide);
    eventInfo[@"sparks_done"] = @(self.noOfProfilesHasSparked);
    eventInfo[@"sparks_left"] = @(self.sparkCountLeft);
    eventInfo[@"pkg_id"] = packageId;
    eventInfo[@"from_intro"] = (self.sparkPackageView.didShowFromIntro) ? @"true" : @"false";
    eventInfo[@"from_sidemenu"] = (self.sparkPackageView.didShowFromSideMenu) ? @"true" : @"false";
    [[TMAnalytics sharedInstance] trackSparkEvent:@"buy" status:@"buy" eventInfo:eventInfo];
}
-(void)trackBuySparkEventSuccessStatusForPkgId:(NSString*)packageId sparkCount:(NSInteger)sparkCount {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    eventInfo[@"from_sidemenu"] = (self.sparkPackageView.didShowFromSideMenu) ? @"true" : @"false";
    eventInfo[@"pkg_id"] = packageId;
    eventInfo[@"total_sparks"] = @(sparkCount);
    [[TMAnalytics sharedInstance] trackSparkEvent:@"buy" status:@"success" eventInfo:eventInfo];
}
-(void)trackBuySparkEventErrorStatus:(NSString*)errorMessage {
    //check for //buy_error //tm_error
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    eventInfo[@"from_sidemenu"] = (self.sparkPackageView.didShowFromSideMenu) ? @"true" : @"false";
    eventInfo[@"error"] = errorMessage; 
    [[TMAnalytics sharedInstance] trackSparkEvent:@"buy" status:@"buy_error" eventInfo:eventInfo];
}
-(void)trackSendSparkEventWithStatus:(NSString*)status errorMessage:(NSString*)errorMessage {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString* matchId = self.profile.matchId;
    if(matchId) {
        eventInfo[@"match_id"] = matchId;
    }
    if([status isEqualToString:@"view"]) {
        eventInfo[@"likes_done"] = @(self.noOfProfilesHasLiked);
        eventInfo[@"hides_done"] = @(self.noOfProfilesHasHide);
        eventInfo[@"sparks_done"] = @(self.noOfProfilesHasSparked);
        eventInfo[@"sparks_left"] = @(self.sparkCountLeft);
        eventInfo[@"from_intro"] = (self.sendSparkView.didShowFromIntro) ? @"true" : @"false";
    }
    else if([status isEqualToString:@"send"]) {
        eventInfo[@"likes_done"] = @(self.noOfProfilesHasLiked);
        eventInfo[@"hides_done"] = @(self.noOfProfilesHasHide);
        eventInfo[@"sparks_done"] = @(self.noOfProfilesHasSparked);
        eventInfo[@"sparks_left"] = @(self.sparkCountLeft);
        eventInfo[@"from_intro"] = (self.sendSparkView.didShowFromIntro) ? @"true" : @"false";
        eventInfo[@"alert"] = (self.sendSparkView.didShowMessageTextAlert) ? @"true" : @"false";
        eventInfo[@"swiped"] = (self.sendSparkView.didSwipeCommonIntroText) ? @"true" : @"false";
        eventInfo[@"def_likes"] = (self.sendSparkView.didShowDefaultCommonalityString) ? @"true" : @"false";
        eventInfo[@"msg_len"] = @(self.sendSparkView.messageLength);
        
        [[TMAnalytics sharedInstance] trackFacebookEvent:@"spark_sent" withParams:nil];
        //eventInfo[@"time_taken"] = @(0);
    }
    else if([status isEqualToString:@"send_error"]) {
        eventInfo[@"error"] = errorMessage;
    }
    else if([status isEqualToString:@"interests_error"]) {
        eventInfo[@"error"] = errorMessage;
    }
    
    [[TMAnalytics sharedInstance] trackSparkEvent:@"send" status:status eventInfo:eventInfo];
}
-(void)trackSelectEvent:(NSString*)eventName withStatus:(NSString*)status userInfo:(NSDictionary*)trackingInfo {
    NSString *isSelectMember = ([[TMUserSession sharedInstance].user isSelectUser]) ? @"true" : @"false";
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    if(trackingInfo) {
        [eventInfo addEntriesFromDictionary:trackingInfo];
    }
    eventInfo[@"is_select_member"] = isSelectMember;
    [[TMAnalytics sharedInstance] trackSelectEvent:eventName status:status eventInfo:eventInfo];
}
-(NSDictionary*)getTrackingInfoForBuySelectLaunch {
    NSDictionary *trackingInfo = @{@"source":@"",@"match_id":@"",@"likes_done":@"",@"hides_done":@"",@"sparks_done":@""};
    return trackingInfo;
}

-(void) hashTagForProfileAd:(NSArray *) hashTags {
    [self refreshCellForProfileAdLikes:hashTags];
}

-(BOOL)hasReachedLikeLimit {
    int likeLimit = [[TMUserSession sharedInstance] getLikeLimitForUser];
    int likeLimitCounter = [[TMUserSession sharedInstance] getLimitedLikeCounter];
    BOOL isLikeLimit = (likeLimitCounter == likeLimit)? YES : NO;
    return isLikeLimit;
}
@end

//
//  TMAnimationView.m
//  TrulyMadly
//
//  Created by Ankit on 27/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMAnimationView.h"
#import "NSString+TMAdditions.h"

@implementation TMAnimationView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}
-(void)showHasLikedBeforeSparkAnimationOnBaseView:(UIView*)baseview pointerYPos:(CGFloat)finalYPos {
    //self.backgroundColor = [UIColor greenColor];
    
    [baseview addSubview:self];

    UIView *tutorialView = [self getHasLikedBeforeSparkAnimationPointerView];
    [baseview addSubview:tutorialView];
    //tutorialView.center = CGPointMake(CGRectGetWidth(tutorialView.frame) - center.x, center.y - 300);
    tutorialView.frame = CGRectMake((baseview.frame.size.width - tutorialView.frame.size.width)/2,
                                    finalYPos - 150,
                                    CGRectGetWidth(tutorialView.frame),
                                    CGRectGetHeight(tutorialView.frame));
    tutorialView.alpha = 0.0;
    
    [UIView animateWithDuration:0.75
                          delay: 0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         tutorialView.alpha = 1;
                         tutorialView.frame = CGRectMake(CGRectGetMinX(tutorialView.frame),
                                                         finalYPos-100,
                                                         CGRectGetWidth(tutorialView.frame),
                                                         CGRectGetHeight(tutorialView.frame));
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         [UIView animateWithDuration:0.75
                                               delay: 2.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              tutorialView.alpha = 0;   
                                          }
                                          completion:^(BOOL finished){
                                              [tutorialView removeFromSuperview];
                                          }];
                     }];
}
-(void)showMutualLikeTutorialOnBaseView:(UIView*)baseview {
    [baseview addSubview:self];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIView *tutorialView = [self getMutualLikeTutorialPointerView];
    [window addSubview:tutorialView];
    tutorialView.frame = CGRectMake(baseview.frame.size.width - tutorialView.frame.size.width - 6,
                                    140,
                                    CGRectGetWidth(tutorialView.frame),
                                    CGRectGetHeight(tutorialView.frame));
    tutorialView.alpha = 0.0;
    
    [UIView animateWithDuration:0.75
                          delay: 0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         tutorialView.alpha = 1;
                         tutorialView.frame = CGRectMake(baseview.frame.size.width - tutorialView.frame.size.width - 6,
                                                         115,
                                                         CGRectGetWidth(tutorialView.frame),
                                                         CGRectGetHeight(tutorialView.frame));
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         [UIView animateWithDuration:0.75
                                               delay: 1.5
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              tutorialView.alpha = 0;
                                              
                                          }
                                          completion:^(BOOL finished){
                                              [tutorialView removeFromSuperview];
                                          }];
                     }];
}
-(void)showEventsTutorialOnBaseView:(UIView*)baseview {
    [baseview addSubview:self];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIView *tutorialView = [self getMeetUpTutorialPointerView];
    [window addSubview:tutorialView];
    tutorialView.frame = CGRectMake(baseview.frame.size.width - tutorialView.frame.size.width - 20,
                                    140,
                                    CGRectGetWidth(tutorialView.frame),
                                    CGRectGetHeight(tutorialView.frame));
    tutorialView.alpha = 0.0;
    [UIView animateWithDuration:0.5
                          delay: 0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         tutorialView.alpha = 1;
                         tutorialView.frame = CGRectMake(baseview.frame.size.width - tutorialView.frame.size.width - 20,
                                                         115,
                                                         CGRectGetWidth(tutorialView.frame),
                                                         CGRectGetHeight(tutorialView.frame));
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         [UIView animateWithDuration:0.5
                                               delay: 1.5
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              tutorialView.alpha = 0;
                                              
                                          }
                                          completion:^(BOOL finished){
                                              [tutorialView removeFromSuperview];
                                          }];
                     }];
}

-(UIView*)getMatchActionTutorialPointerView {
    //
    CGFloat height = 60;
    NSString *text = @"Like or Nope to proceed. Your name & action aren't revealed until mutually matched.";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds)-20, height);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width+16, height)];
    view.backgroundColor = [UIColor blackColor];
    view.layer.cornerRadius = 10;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((view.frame.size.width-16)/2,
                                                                         view.frame.size.height,
                                                                         12,
                                                                         65)];
    imgView.image = [UIImage imageNamed:@"TutorialPointer"];
    imgView.transform = CGAffineTransformMakeRotation(M_PI);
    
    [view addSubview:imgView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake((view.frame.size.width-rect.size.width)/2, (view.frame.size.height-rect.size.height)/2, rect.size.width, rect.size.height)];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.text = text;
    textLabel.numberOfLines = 0;
    textLabel.font = [UIFont systemFontOfSize:16];
    [view addSubview:textLabel];
    return view;
}

-(UIView*)getHasLikedBeforeSparkAnimationPointerView {
    //
    CGFloat height = 44;
    NSString *text = @"This time get noticed quicker with Spark";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), height);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width+20, height)];
    view.backgroundColor = [UIColor blackColor];
    view.layer.cornerRadius = 10;
    
    CGFloat xPosOffset = 0;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(screenWidth == 320) {
        xPosOffset = 30;
    }
    else if(screenWidth == 375) {
        xPosOffset = 28;
    }
    else {
        xPosOffset = 33;
    }
    
    CGFloat imageViewWidth = 12;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(((CGRectGetWidth(view.frame) - imageViewWidth)/2),
                                                                         CGRectGetMaxY(view.frame),
                                                                         imageViewWidth,
                                                                         65)];
    imgView.image = [UIImage imageNamed:@"TutorialPointer"];
    imgView.transform = CGAffineTransformMakeRotation(M_PI);
    [view addSubview:imgView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake((view.frame.size.width-rect.size.width)/2,
                                                                   (view.frame.size.height-rect.size.height)/2,
                                                                   rect.size.width,
                                                                   rect.size.height)];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.text = text;
    textLabel.font = [UIFont systemFontOfSize:16];
    [view addSubview:textLabel];
    return view;
}
-(UIView*)getMutualLikeTutorialPointerView {
    //
    CGFloat height = 44;
    NSString *text = @"See if they have liked you";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), height);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width+20, height)];
    view.backgroundColor = [UIColor blackColor];
    view.layer.cornerRadius = 10;
    
    CGFloat xPosOffset = 0;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(screenWidth == 320) {
        xPosOffset = 30;
    }
    else if(screenWidth == 375) {
        xPosOffset = 28;
    }
    else {
        xPosOffset = 33;
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((view.frame.size.width-xPosOffset),
                                                                         -65,
                                                                         12,
                                                                         65)];
    imgView.image = [UIImage imageNamed:@"TutorialPointer"];
    [view addSubview:imgView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake((view.frame.size.width-rect.size.width)/2, (view.frame.size.height-rect.size.height)/2, rect.size.width, rect.size.height)];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.text = text;
    textLabel.font = [UIFont systemFontOfSize:16];
    [view addSubview:textLabel];
    return view;
}

-(UIView *)getMeetUpTutorialPointerView {
    CGFloat height = 44;
    NSString *text = @"Find experiences you love.\nMeet like-minded peeps.";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), height);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width+30, rect.size.height+30)];
    view.backgroundColor = [UIColor blackColor];
    view.layer.cornerRadius = 25;
    
    CGFloat xPosOffset = 0;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(screenWidth == 320) {
        xPosOffset = 30+30;
    }
    else if(screenWidth == 375) {
        xPosOffset = 28+30;
    }
    else {
        xPosOffset = 33+30;
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((view.frame.size.width-xPosOffset),
                                                                         -65,
                                                                         12,
                                                                         65)];
    imgView.image = [UIImage imageNamed:@"TutorialPointer"];
    [view addSubview:imgView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake((view.frame.size.width-rect.size.width)/2, (view.frame.size.height-rect.size.height)/2, rect.size.width, rect.size.height)];
    textLabel.numberOfLines = 0;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.text = text;
    textLabel.font = [UIFont systemFontOfSize:16];
    textLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:textLabel];
    return view;
}


@end

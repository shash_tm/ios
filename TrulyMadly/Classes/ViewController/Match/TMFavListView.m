//
//  TMFavListView.m
//  TrulyMadly
//
//  Created by Ankit Jain on 27/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFavListView.h"
#import "TMStringUtil.h"
#import "TMFavContentCellCollectionViewCell.h"
#import "TMLinearFlowLayout.h"
#import "NSString+TMAdditions.h"
#import "TMFavTag.h"
#import "UIColor+TMColorAdditions.h"


@interface  TMFavListView()<UICollectionViewDataSource,UICollectionViewDelegate,TMLinearFlowLayoutDataSource>


@property(nonatomic,strong)NSMutableArray *content;
@property(nonatomic,assign)BOOL addUserInteractionEnabled;
@property(nonatomic,assign)TMFavListType type;
//@property(nonatomic,assign)BOOL isInnerContentDict;

@end



@implementation TMFavListView

-(instancetype)initWithFrame:(CGRect)frame withType:(TMFavListType)type {

    self = [super initWithFrame:frame];
    if(self) {
        self.type = type;
        self.addUserInteractionEnabled = false;
        self.content = [NSMutableArray arrayWithCapacity:16];
        
        TMLinearFlowLayout *flowLayout = [[TMLinearFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.dataSource = self;
        flowLayout.itemOffset = UIOffsetMake(10, ((frame.size.height - 28)/2)-5);
        
        self.favCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
        self.favCollectionView.backgroundColor = [UIColor clearColor];
        self.favCollectionView.dataSource = self;
        self.favCollectionView.delegate = self;
        self.favCollectionView.showsHorizontalScrollIndicator = NO;
        [self addSubview:self.favCollectionView];
        
        [self.favCollectionView registerClass:[TMFavContentCellCollectionViewCell class] forCellWithReuseIdentifier:@"photo"];
    }
    return self;
}

-(void)layoutSubviews {
    TMLinearFlowLayout *flowLayout = (TMLinearFlowLayout*)self.favCollectionView.collectionViewLayout;
    flowLayout.itemOffset= UIOffsetMake(10, ((self.frame.size.height - 28)/2)-5);
    self.favCollectionView.frame = self.bounds;
}

-(void)loadDataWithContent:(NSArray*)content {
    [self.content removeAllObjects];
    
    if([self.commonFavorites count]>0) {
        NSMutableArray* tmpArr = [[NSMutableArray alloc] init];
        for (int i=0; i<[self.commonFavorites count]; i++) {
            [tmpArr addObject:self.commonFavorites[i]];
        }
        for (int i=0; i<[content count]; i++) {
            if (![tmpArr containsObject: content[i]] ) {
                [tmpArr addObject:content[i]];
            }
        }
        [self.content addObjectsFromArray:tmpArr];
    }else {
        [self.content addObjectsFromArray:content];
    }
    [self.favCollectionView reloadData];
    
    [self.favCollectionView setContentOffset:CGPointZero];
}

-(NSString*)contentAtIndex:(NSInteger)index {
    NSString *content = nil;
    if(self.type == FavListType_Edit) {
        TMFavTag *tag = [self.content objectAtIndex:index];
        content = tag.value;
    }
    else {
        content = [self.content objectAtIndex:index];
    }
    return content;
}


-(void)enableUserInteraction {
    self.addUserInteractionEnabled = true;
}

#pragma mark TMLinearFlowLayoutDataSource datasource -

-(CGSize)contentSizeForIndex:(NSInteger)index {
    NSString *text = [TMStringUtil stringByReplacingInvalidContent:[self contentAtIndex:index]];
    BOOL isCommomFavourite = FALSE;
    if ([self.commonFavorites containsObject:text]) {
        isCommomFavourite = TRUE;
    }
    NSString *fontFamily = (isCommomFavourite) ? @"HelveticaNeue-Medium" : @"HelveticaNeue";
    UIFont *font = [UIFont fontWithName:fontFamily size:14];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:300], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];

    CGFloat widthOffset = 24;
    if(self.addUserInteractionEnabled) {
        widthOffset = 34;
    }
    CGSize size = CGSizeMake(rect.size.width+widthOffset, 34);
    return size;
}


#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.content.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"photo";
    TMFavContentCellCollectionViewCell *cell = [collectionView
                                                      dequeueReusableCellWithReuseIdentifier:reusableIdentifier
                                                      forIndexPath:indexPath];
    NSString *text = [self contentAtIndex:indexPath.row];
    text = [TMStringUtil stringByReplacingInvalidContent:text];
    text = [text stringByTrimingWhitespace];
    if(self.addUserInteractionEnabled) {
        [cell enableUserInteraction];
    }
    
    BOOL isCommomFavourite = FALSE;
    if ([self.commonFavorites containsObject:text]) {
        isCommomFavourite = TRUE;
    }
    [cell setFavItemText:text isCommonFavourite:isCommomFavourite];
    return  cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(self.type == FavListType_Edit) {
        TMFavTag *tag = [self.content objectAtIndex:indexPath.row];
        tag.index = indexPath.row;
        [self.delegate didSelecFavAtIndex:tag];
    }
    else {
        NSString *text = [self contentAtIndex:indexPath.row];
        [self.delegate didSelectItemAtIndex:text];
    }
}

@end

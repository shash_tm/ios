//
//  TMEndorseContentCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 17/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMEndorseContentCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong)UILabel *name;;
-(void)setProfileImage:(NSURL*)imgUrl;

@end

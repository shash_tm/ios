//
//  TMTextFieldTableViewCell.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

import UIKit

class TMTextFieldTableViewCell: UITableViewCell {

    var headerTextField: UITextView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        
        let width = UIScreen.main.bounds.size.width
        self.headerTextField = UITextView(frame: CGRect(x: 10, y: 10, width: width-20, height: 100))
        self.headerTextField.font = UIFont.systemFont(ofSize: 15)
        self.headerTextField.text = " Write here"
        self.headerTextField.textAlignment = NSTextAlignment.left
        self.headerTextField.layer.borderColor = UIColor.lightGray.cgColor
        self.headerTextField.layer.borderWidth = 0.5
        self.headerTextField.autocorrectionType = UITextAutocorrectionType.no
        self.headerTextField.textColor = UIColor.lightGray
        self.contentView.addSubview(self.headerTextField)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}

//
//  TMReasonsExplainViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

import UIKit

class TMReasonsExplainViewController: TMBaseViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, TMCustomAlertViewDelegate {

    var tableView: UITableView?
    var deleteReason: DeleteReason!
    var header: String?
    var subHeader: String?
    var textReason = ""
    var showPrivacy = false
    var textView: UITextView!
    var rating:Int = 0

    convenience init(deleteReason: DeleteReason, showPrivacy:Bool) {
        self.init()
        self.deleteReason = deleteReason
        self.showPrivacy = showPrivacy
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(red: 237/255, green: 236/255, blue: 242/255, alpha: 1.0)
        self.navigationController?.navigationItem.hidesBackButton = true
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 200), style: .grouped)
        
        if let theTableView = tableView{
            
            theTableView.register(TMTextFieldTableViewCell.self, forCellReuseIdentifier: "uiTextView")
            theTableView.register(TMPublicTableViewCell.self, forCellReuseIdentifier: "uiPublic")
            
            theTableView.dataSource = self
            theTableView.delegate = self
            //theTableView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            
            theTableView.isScrollEnabled = false
            view.addSubview(theTableView)
            
        }
        if(self.deleteReason == DeleteReason.Found) {
            self.header = ""
            self.subHeader = "We\'d love to hear your story"
            self.title = "Met Someone"
        }
        else if(self.deleteReason == DeleteReason.Public) {
            self.header = "WHY DELETE?"
            self.subHeader = "Try Profile Visibility and be visible only to the ones you \'Like\'."
            self.title = "Was Trying The App"
        }
        
        self.leftBarButton()

        self.newViewForFooter()
//        view.addSubview(self.newViewForFooter())
//        view.bringSubviewToFront(self.newViewForFooter())
    }
    
    override func viewDidLayoutSubviews() {
        tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func leftBarButton() {
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_button")!, style: .done, target: self, action: #selector(TMReasonsExplainViewController.backButtonClicked))
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func backButtonClicked() {
        self.navigationController!.popViewController(animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(self.deleteReason == DeleteReason.Found) {
            return 120
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if(self.deleteReason == DeleteReason.Public) {
            return 100
        }
        return 70
    }
    
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 250
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        return newViewForHeaderWithText()
    }
    
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return newViewForFooter()
//    }
    
    func newViewForFooter() {
        
        let height: CGFloat = 50
        let footerView = UIView(frame: CGRect(x: 0, y: view.frame.size.height-150, width: view.bounds.width, height: height))
        
        let deletebutton = deleteButton()
        footerView.addSubview(deletebutton)
        
        self.view.addSubview(footerView)

    }
    
    func deleteButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44)
        button.setTitleColor(UIColor(red:3.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1.0), for: UIControlState())
        button.setTitle("Delete Anyway", for: UIControlState())
        button.addTarget(self, action: #selector(TMReasonsExplainViewController.showRateAlert), for: UIControlEvents.touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.titleLabel?.textAlignment = .center
        return button
    }
    
    func privacyButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 10, y: 0, width: view.bounds.size.width-20, height: 44)
        button.backgroundColor = UIColor(red: 1.0, green: 0.702, blue: 0.725, alpha: 1.0)
        button.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: UIControlState())
        button.setTitle("Try Profile Visibility", for: UIControlState())
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.addTarget(self, action: #selector(TMReasonsExplainViewController.profileVisibilityClicked), for: UIControlEvents.touchUpInside)
        button.titleLabel?.textAlignment = .center
        return button
    }

    func newViewForHeaderWithText() -> UIView{
        let headerLabel = newLabelWithTitle(self.header!, subHeading: false)
        
        headerLabel.frame.origin.x += 10
        headerLabel.frame.origin.y = 25
        
        let subheaderLabel = newLabelWithTitle(self.subHeader!, subHeading: true)
        subheaderLabel.frame.origin.x = headerLabel.frame.origin.x
        subheaderLabel.frame.origin.y = headerLabel.frame.origin.y + headerLabel.frame.size.height+5
        
        let resultFrame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 70)
        let headerView = UIView(frame: resultFrame)
        headerView.addSubview(headerLabel)
        headerView.addSubview(subheaderLabel)
        
        return headerView
    }
    
    func newLabelWithTitle(_ title: String, subHeading:Bool) -> UILabel{
        let label = UILabel()
        label.text = title
        if(subHeading) {
            label.frame = CGRect(x: 10, y: 0, width: self.view.bounds.size.width-20, height: 15)
            label.font = UIFont.systemFont(ofSize: 14)
            //label.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        }else {
            label.font = UIFont(name: "HelveticaNeue-Light", size: 15)
            //label.textColor = UIColor.blackColor()
        }
        label.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if(self.deleteReason == DeleteReason.Found) {
            let cell:TMTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiTextView") as! TMTextFieldTableViewCell
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell.headerTextField.delegate = self
                cell.selectionStyle = .none
                self.textView = cell.headerTextField
                return cell
            default: fatalError("Unknown row")
            }
        }
        else {
            let cell:TMPublicTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiPublic") as! TMPublicTableViewCell
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell.selectionStyle = .none
                cell.profileVisibiltyButton.addTarget(self, action: #selector(TMReasonsExplainViewController.profileVisibilityClicked), for: UIControlEvents.touchUpInside)
                return cell
            default: fatalError("Unknown row")
            }
        }
        
    }
    
    // pragma mark textview delegate methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == " Write here") {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == "") {
            textView.text = " Write here"
            textView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.textReason = textView.text.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func showRateAlert() {
        if(self.textView != nil) {
            self.textView.resignFirstResponder()
        }
        let customAlert = TMCustomAlertView()
        customAlert.initAlert(withMessageAndRateView: "All your info will be permanently deleted. Share your Experience before you go!", parentView:UIApplication.shared.windows.first , delegate: self, buttonTitle:"Delete")
    }
    
    // TMCustomAlertView Delegate methods
    func actionButtonPressed(_ rateScore: NSNumber!) {
        if(rateScore != nil){
            self.rating = rateScore.intValue
        }else {
            self.rating = 0
        }
        self.delegate?.deleteAccount!(self.title, subReason: self.getReason(), rating: self.rating)
    }
    
    func cancelButtonPressed() {
        let eventDict = ["status":"success", "screenName":"TMSettingsView", "eventAction":"delete_cancel_click", "eventCategory":"settings"]
        TMAnalytics.sharedInstance().trackNetworkEvent(eventDict as [NSObject : AnyObject])
    }
    
    func profileVisibilityClicked() {
        self.delegate?.didPrivacyClicked!(false)
    }

    func getReason()->String {
        if(self.deleteReason == DeleteReason.Found) {
            return self.textReason
        }
        else {
            return ""
        }
    }
}

//
//  TMNotificationTableViewCell.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

class TMNotificationTableViewCell: UITableViewCell {

    var headerText: UILabel!
    var button: UISwitch!
    var infoLabel: UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        
        self.headerText = UILabel()
        self.headerText.font = UIFont.systemFont(ofSize: 16)
        self.headerText.textAlignment = NSTextAlignment.left
        self.contentView.addSubview(self.headerText)
        
        self.button = UISwitch()
        self.button.setOn(true, animated:true)
        self.contentView.addSubview(self.button)
        
        self.infoLabel = UILabel()
        self.infoLabel.font = UIFont.systemFont(ofSize: 13)
        self.infoLabel.textAlignment = NSTextAlignment.left
        self.contentView.addSubview(self.infoLabel)

    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func labelWithText(_ text: String, infoLabel: String, infoLabelColor: UIColor) {
        
        var textDict:Dictionary<String,AnyObject> = [:]
        let widthAllowed = bounds.size.width - 32
        textDict["kmaxwidth"] = widthAllowed as AnyObject
        textDict["kfont"] = UIFont.systemFont(ofSize: 16)
        textDict["ktext"] = text as AnyObject?
        var stringFrame = TMStringUtil.textRect(forString: textDict)
        var stringWidth = stringFrame.size.width
        var stringHeight = stringFrame.size.height
        
        self.headerText.text = text
        self.headerText.frame.size.width = stringWidth
        self.headerText.frame.size.height = stringHeight
        self.headerText.frame.origin.x = 16
        self.headerText.frame.origin.y = (50-stringHeight)/2
        self.headerText.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        self.button.frame.origin.x = bounds.width-self.button.frame.size.width-10
        self.button.frame.origin.y = (50-self.button.frame.size.height)/2
        
        textDict["kfont"] = UIFont.systemFont(ofSize: 13)
        textDict["ktext"] = infoLabel as AnyObject?
        stringFrame = TMStringUtil.textRect(forString: textDict)
        stringWidth = stringFrame.size.width
        stringHeight = stringFrame.size.height
        
        self.infoLabel.text = infoLabel
        self.infoLabel.frame.size.width = stringWidth
        self.infoLabel.frame.size.height = stringHeight
        self.infoLabel.frame.origin.x = 16
        self.infoLabel.frame.origin.y = self.headerText.frame.origin.x + self.headerText.frame.size.height + 4.0
        self.infoLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        self.infoLabel.textColor = infoLabelColor
    }


}

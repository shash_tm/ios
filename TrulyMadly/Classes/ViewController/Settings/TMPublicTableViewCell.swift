//
//  TMPublicTableViewCell.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

import UIKit

class TMPublicTableViewCell: UITableViewCell {

    var headerText: UILabel!
    var profileVisibiltyButton: UIButton!
    var deleteButton: UIButton!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: UITableViewCellStyle.value1, reuseIdentifier: reuseIdentifier)
        
        let width = UIScreen.main.bounds.size.width
        
        self.profileVisibiltyButton = UIButton(type: UIButtonType.system)
        self.profileVisibiltyButton.frame = CGRect(x: 10, y: 3, width: width-20, height: 44)
        self.profileVisibiltyButton.setTitleColor(UIColor(red:3.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1.0), for: UIControlState())
        self.profileVisibiltyButton.setTitle("Try Profile Visibility", for: UIControlState())
        self.profileVisibiltyButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.profileVisibiltyButton.contentHorizontalAlignment = .left
        self.contentView.addSubview(self.profileVisibiltyButton)
    
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}

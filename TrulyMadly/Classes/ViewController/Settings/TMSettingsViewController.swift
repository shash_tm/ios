//
//  TMSettingsTableViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

class TMSettingsViewController: TMBaseViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, TMViewControllerActionDelegate {
    
    var tableView: UITableView?
    var isPrivacyShown: Bool = false
    var isVisibilityOn: Bool = true
    var settingsMngr: TMSettingsManager!
    var gender: String?
    var isRequestInProgress: Bool = false
    var eventDict:NSMutableDictionary!
    var activityIndicator: TMActivityIndicatorView!
    
    func initialize() {
        let userSession = TMUserSession.sharedInstance()
        let user = userSession?.user
        self.gender = user?.gender()
        if(self.gender == "F" && (TMUserSession.sharedInstance().user.hasFemaleProfileBeenRejectedOnce || TMDataStore.containsObject(forKey: "is_discovery_on"))) {
            self.isPrivacyShown = true
            
            var isDiscoverySet:Bool
            if(TMDataStore.containsObject(forKey: "is_discovery_on")) {
                isDiscoverySet = TMDataStore.retrieveBoolforKey("is_discovery_on")
            }
            else {
                //by default true
                isDiscoverySet = true
                TMDataStore.setBool(isDiscoverySet, forKey: "is_discovery_on")
            }
            self.isVisibilityOn = isDiscoverySet
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        
        self.initialize()
        
        tableView = UITableView(frame: view.bounds, style: .grouped)
        
        if let theTableView = tableView{
            
            theTableView.register(TMTextLabelTableViewCell.self, forCellReuseIdentifier: "uiText")
            theTableView.register(TMNotificationTableViewCell.self, forCellReuseIdentifier: "uiSwitch")
            theTableView.register(TMEmptyTableViewCell.self, forCellReuseIdentifier: "uiEmpty")
            
            theTableView.dataSource = self
            theTableView.delegate = self
            theTableView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            
            view.addSubview(theTableView)
        }
        //add label for shwoing chat connection type in buildmode==develop
        var version: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let build : String = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    
        if(TM_APP_BUILD_MODE == TM_BUILD_MODE_DEVELOPMENT) {
            version = version + " (" + build + ")"
            if(TM_APP_SERVER_ENVIRONMENT == TM_SERVER_ENVIRONMENT_DEVELOPMENT){
                version = version + " (dev)"
            }else if(TM_APP_SERVER_ENVIRONMENT == TM_SERVER_ENVIRONMENT_T1) {
                version = version + " (t1)"
            }
        }
        
        let chatSettings = TMChatConnectionSetting()
        let label = UILabel()
        label.text = version//chatSettings.isSocketEnabled() ? "S" : "P"
        label.frame = CGRect(x: 0, y: view.bounds.size.height-104, width: view.bounds.size.width, height: 20)
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = chatSettings.isSocketEnabled() ? (UIColor.gray) : (UIColor.black)
        label.backgroundColor = UIColor.clear
        label.textAlignment = NSTextAlignment.center
        view.addSubview(label)
    }
    
    
    override func viewDidLayoutSubviews() {
        tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let noOfSections = 2
        if(isPrivacyShown) {
            return (noOfSections+1)
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if(isPrivacyShown) {
            if (section == 0 || section == 1) {
                rowCount = 1
            }
            else{
             rowCount = 4
            }
        }
        else {
            if (section == 0) {
                rowCount = 1
            }
            else{
                rowCount = 4
            }
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(indexPath.section == 0) {
            if(!isPrivacyShown) {
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "Help Desk"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                return cell
            }
            else {
                let cell:TMNotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiSwitch") as! TMNotificationTableViewCell
                if(self.isVisibilityOn) {
                    cell.button.setOn(true, animated: false)
                    if(self.isRequestInProgress) {
                        cell.labelWithText("Profile Visibility",infoLabel: "Updating...", infoLabelColor: UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0))
                    }
                    else {
                        cell.labelWithText("Profile Visibility",infoLabel: "All your matches", infoLabelColor: UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0))
                    }
                }
                else {
                    cell.button.setOn(false, animated: false)
                    if(self.isRequestInProgress) {
                        cell.labelWithText("Profile Visibility",infoLabel: "Updating...", infoLabelColor: UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0))
                    }
                    else{
                        cell.labelWithText("Profile Visibility",infoLabel: "Only the ones you like", infoLabelColor: UIColor.red)
                    }
                }
                cell.button.tag = (indexPath as NSIndexPath).row
                cell.selectionStyle = .none
                cell.button.addTarget(self, action: #selector(TMSettingsViewController.stateChanged(_:)), for: UIControlEvents.valueChanged)
                return cell
            }
        }
        else if(indexPath.section == 1) {
            if(isPrivacyShown) {
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "Help Desk"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                return cell
            }
            else {
                switch((indexPath as NSIndexPath).row) {
                case 0:
                    let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.text = "Terms of Use"
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                    cell.selectionStyle = .none
                    return cell
                case 1:
                    let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.text = "Safety Guidelines"
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                    cell.selectionStyle = .none
                    return cell
                case 2:
                    let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.text = "Trust & Security"
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                    cell.selectionStyle = .none
                    return cell
                case 3:
                    let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.text = "True Compatibility"
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                    cell.selectionStyle = .none
                    return cell
                default: fatalError("Unknown row")
                }
            }
        }
        else {
            switch((indexPath as NSIndexPath).row) {
            case 0:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "Terms of Use"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "Safety Guidelines"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                return cell
            case 2:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "Trust & Security"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                return cell
            case 3:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "True Compatibility"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = .none
                return cell
            default: fatalError("Unknown row")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if((indexPath as NSIndexPath).section == 0 && isPrivacyShown) {
            return 62
        }
        return 50
    }
    
    func updateProfileVisibilityStatus() {
         var cell:TMNotificationTableViewCell
        
        cell = self.tableView?.cellForRow(at: IndexPath(row: 0, section: 0)) as! TMNotificationTableViewCell
        if(self.isVisibilityOn) {
            
            if(self.isRequestInProgress) {
                cell.labelWithText("Profile Visibility",infoLabel: "Updating...", infoLabelColor: UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0))
            }
            else {
                cell.labelWithText("Profile Visibility",infoLabel: "All your matches", infoLabelColor: UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0))
            }
        }
        else {
            
            if(self.isRequestInProgress) {
                cell.labelWithText("Profile Visibility",infoLabel: "Updating...", infoLabelColor: UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0))
            }
            else{
                cell.labelWithText("Profile Visibility",infoLabel: "Only the ones you like", infoLabelColor: UIColor.red)
            }
            
        }
    }
    
    func newLabelWithTitle(_ title: String) -> UILabel{
        let label = UILabel()
        label.text = title
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        label.backgroundColor = UIColor.clear
        label.sizeToFit()
        return label
    }
    
    func logoutButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44)
        button.backgroundColor = UIColor(red: 1.0, green: 0.702, blue: 0.725, alpha: 1.0)
        button.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: UIControlState())
        button.setTitle("Log Out", for: UIControlState())
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.addTarget(self, action: #selector(TMSettingsViewController.onLogout(_:)), for: UIControlEvents.touchUpInside)
        button.titleLabel?.textAlignment = .center
        return button

    }
    
    func deleteButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44)
        //button.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        //button.setTitleColor(UIColor(red: 0.75, green:0.75, blue:0.75, alpha:1.0), forState: UIControlState.Normal)
        button.setTitleColor(UIColor(red:0.596, green:0.596, blue:0.596, alpha:1.0), for: UIControlState())
        button.setTitle("Delete My Account", for: UIControlState())
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.addTarget(self, action: #selector(TMSettingsViewController.onDeleteClick), for: UIControlEvents.touchUpInside)
        button.titleLabel?.textAlignment = .center
        return button
        
    }
    
    func newViewForHeaderWithText(_ text: String, showPrivacy: Bool, section: Int) -> UIView{
        let headerLabel = newLabelWithTitle(text)
        
        /* Move the label 10 points to the right */
        headerLabel.frame.origin.x += 10
        /* Go 5 points down in y axis */
        headerLabel.frame.origin.y = 20
        if(showPrivacy) {
            if(section == 0) {
                headerLabel.frame.origin.y = 30
            }else {
                headerLabel.frame.origin.y = 11
            }
        }
        
        /* Give the container view 10 points more in width than our label
        because the label needs a 10 extra points left-margin */
        let resultFrame = CGRect(x: 0, y: 0, width: headerLabel.frame.size.width + 10, height: headerLabel.frame.size.height)
        let headerView = UIView(frame: resultFrame)
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func newViewForFooter() -> UIView {
        
        let height: CGFloat = 300
        let logoutbutton = logoutButton()
        logoutbutton.frame.origin.y = 30
        
        let deletebutton = deleteButton()
        deletebutton.frame.origin.y = height-150
        
        let resultFrame = CGRect(x: 0, y: 0, width: view.bounds.width, height: height)
        let headerView = UIView(frame: resultFrame)
        headerView.addSubview(logoutbutton)
        
        headerView.addSubview(deletebutton)
        
        return headerView
    }
    
    func emptyViewForFooter() -> UIView {
        let footerView = UIView(frame: CGRect.zero)
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        var headerHeight:CGFloat = 0
        if(isPrivacyShown) {
            if (section == 0) {
                headerHeight = 50
            }
            else{
                headerHeight = 45
            }
        }
        else {
            headerHeight = 45
        }

        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        var footerHeight:CGFloat = 0
        if(isPrivacyShown) {
            if (section == 0 || section == 1) {
                footerHeight = 0
            }
            else{
                footerHeight = 300
            }
        }
        else {
            if (section == 0) {
                footerHeight = 0
            }
            else{
                footerHeight = 300
            }
        }
        return footerHeight //140//76
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        if(isPrivacyShown) {
            if (section == 0) {
                 return newViewForHeaderWithText("PRIVACY", showPrivacy:self.isPrivacyShown, section: 0)
            }
            else if(section == 1) {
                 return newViewForHeaderWithText("SUPPORT", showPrivacy:self.isPrivacyShown, section: 0)
            }
            else{
                return newViewForHeaderWithText("TERMS OF SERVICE", showPrivacy: self.isPrivacyShown, section: 1)
            }
        }
        else {
            if (section == 0) {
                return newViewForHeaderWithText("SUPPORT", showPrivacy:self.isPrivacyShown, section: 0)
            }
            else{
                return newViewForHeaderWithText("TERMS OF SERVICE", showPrivacy: self.isPrivacyShown, section: 1)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?{
        if(isPrivacyShown) {
            if (section == 0 || section == 1) {
                return emptyViewForFooter()
            }
            else{
                return newViewForFooter()
            }
        }
        else {
            if (section == 0) {
                return emptyViewForFooter()
            }
            else{
                return newViewForFooter()
            }
        }
    }
    
    func stateChanged(_ switchState: UISwitch) {
        
        var isDiscoveryOn = false
        if(switchState.isOn) {
            isDiscoveryOn = true
        }
        
        self.privacyChanged(isDiscoveryOn)
    }
    
    func privacyChanged(_ isDiscoveryOn:Bool) {
        if(self.settingsMngr == nil) {
            self.settingsMngr  = TMSettingsManager()
        }
        if(self.settingsMngr.isNetworkReachable()) {
            var queryString = Dictionary<String,AnyObject>()
            queryString["is_discovery_on"] = !isDiscoveryOn as AnyObject?
            self.tableView?.isUserInteractionEnabled = false
            self.isRequestInProgress = true
            self.updateProfileVisibilityStatus()
            self.settingsMngr.updateProfileDiscovery(queryString as [NSObject : AnyObject], with: { (response:Bool, error:TMError?) -> Void in
                self.isRequestInProgress = false
                self.tableView?.isUserInteractionEnabled = true
                
                if(response) {
                    if(!isDiscoveryOn) {
                        //remove the previous timestamp stored
                        //  TMDataStore.removeObjectforKey(PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP)
                        // TMDataStore.removeObjectforKey(PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP)
                        //instead of removing the timestamp, instead update it
                        if(TMDataStore.containsObject(forKey: PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP)) {
                            //update timestamp
                            let currentTimeInterval:TimeInterval = Date.timeIntervalSinceReferenceDate
                            let myDouble = NSNumber(value: currentTimeInterval)
                            TMDataStore.setObject(myDouble, forKey: PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP)
                            TMDataStore.synchronize()
                        }
                    }
                    self.isVisibilityOn = isDiscoveryOn
                    TMDataStore.setBool(isDiscoveryOn, forKey: "is_discovery_on")
                    TMDataStore.synchronize()
                    self.tableView?.reloadData()
                    
                    //adding event tracking
                    //tracking in accordance to the parameter being sent
                    
                    let status:String
                    
                    if (isDiscoveryOn) {
                        status = "false"
                    }
                    else {
                        status = "true"
                    }
                    
                    self.eventDict = NSMutableDictionary();
                    self.eventDict.setObject(status, forKey: "status" as NSCopying)
                    self.eventDict.setObject("TMPhotoAlertView", forKey:"screenName" as NSCopying)
                    self.eventDict.setObject("SETTINGS", forKey: "eventCategory" as NSCopying)
                    self.eventDict.setObject("discovery_option_toggled", forKey: "eventAction" as NSCopying)
                    self.eventDict.setObject("false", forKey: "append403" as NSCopying)
                    (TMAnalytics.sharedInstance()).trackNetworkEvent(self.eventDict as [NSObject : AnyObject])
                }else {
                    // handle error condition
                }
                self.tableView?.reloadData()
            })
            
        }else {
            //network handling
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isPrivacyShown) {
            if(indexPath.section == 1) {
                let faqViewController:TMFaqViewController = TMFaqViewController()
                self.navigationController?.pushViewController(faqViewController, animated: true)
            }
            else {
                if((indexPath as NSIndexPath).row == 0) {
                    // Terms of Use
                    let webCon: TMWebViewController = TMWebViewController(caller: "Terms of Use")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
                else if((indexPath as NSIndexPath).row == 1) {
                    // Safety Guidelines
                    let webCon: TMWebViewController = TMWebViewController(caller: "Safety Guidelines")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
                else if((indexPath as NSIndexPath).row == 2) {
                    // trust & security
                    let webCon: TMWebViewController = TMWebViewController(caller: "Trust & Security")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
                else if((indexPath as NSIndexPath).row == 3) {
                    // true compatibility
                    let webCon: TMWebViewController = TMWebViewController(caller: "True Compatibility")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
            }
        }
        else {
            if (indexPath.section == 0) {
                let faqViewController:TMFaqViewController = TMFaqViewController()
                self.navigationController?.pushViewController(faqViewController, animated: true)
            }
            else{
                if((indexPath as NSIndexPath).row == 0) {
                    // Terms of Use
                    let webCon: TMWebViewController = TMWebViewController(caller: "Terms of Use")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
                else if((indexPath as NSIndexPath).row == 1) {
                    // Safety Guidelines
                    let webCon: TMWebViewController = TMWebViewController(caller: "Safety Guidelines")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
                else if((indexPath as NSIndexPath).row == 2) {
                    // trust & security
                    let webCon: TMWebViewController = TMWebViewController(caller: "Trust & Security")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
                else if((indexPath as NSIndexPath).row == 3) {
                    // true compatibility
                    let webCon: TMWebViewController = TMWebViewController(caller: "True Compatibility")
                    self.navigationController?.pushViewController(webCon, animated: false)
                }
            }
        }
    }
    
    func onLogout(_ sender: UIButton) {
        //println("logout")
        sender.isEnabled = false
        self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
    }
    
    func onDeleteClick() {
        
        self.eventDict = NSMutableDictionary();
        self.eventDict.setObject("success", forKey: "status" as NSCopying)
        self.eventDict.setObject("TMSettingsView", forKey:"screenName" as NSCopying)
        self.eventDict.setObject("settings", forKey: "eventCategory" as NSCopying)
        self.eventDict.setObject("delete_click", forKey: "eventAction" as NSCopying)
        self.eventDict.setObject("false", forKey: "append403" as NSCopying)
        (TMAnalytics.sharedInstance()).trackNetworkEvent(self.eventDict as [NSObject : AnyObject])
        self.eventDict = nil
        
        let alert = UIAlertView(title: "Delete Account" , message: "Deleting your account will permanently remove your info and forget you in our system!" , delegate: self, cancelButtonTitle: "Cancel")
        
        alert.addButton(withTitle: "Continue")
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    func movetoReasons() {
        
        self.eventDict = NSMutableDictionary();
        self.eventDict.setObject("success", forKey: "status" as NSCopying)
        self.eventDict.setObject("TMSettingsView", forKey:"screenName" as NSCopying)
        self.eventDict.setObject("settings", forKey: "eventCategory" as NSCopying)
        self.eventDict.setObject("delete_continue_click", forKey: "eventAction" as NSCopying)
        self.eventDict.setObject("false", forKey: "append403" as NSCopying)
        (TMAnalytics.sharedInstance()).trackNetworkEvent(self.eventDict as [NSObject : AnyObject])
        self.eventDict = nil
        
        let reasonView: TMDeleteReasonViewController = TMDeleteReasonViewController()
        reasonView.delegate = self
        reasonView.actionDelegate = self.actionDelegate
        self.navigationController?.pushViewController(reasonView, animated: false)
    }
    
    // alert view delegate method
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if(buttonIndex == 1) {
            // redirect to reasons page
            movetoReasons()
        }
    }
    
    func didPrivacyClicked(_ fromCancel: Bool) {
        self.navigationController?.popToViewController(self, animated: false)
        if(!fromCancel){
            self.eventDict = NSMutableDictionary();
            self.eventDict.setObject("success", forKey: "status" as NSCopying)
            self.eventDict.setObject("TMSettingsView", forKey:"screenName" as NSCopying)
            self.eventDict.setObject("settings", forKey: "eventCategory" as NSCopying)
            self.eventDict.setObject("discovery_option_toggled_delete", forKey: "eventAction" as NSCopying)
            self.eventDict.setObject("false", forKey: "append403" as NSCopying)
            (TMAnalytics.sharedInstance()).trackNetworkEvent(self.eventDict as [NSObject : AnyObject])
            self.eventDict = nil
            
            self.isPrivacyShown = true
            self.tableView?.reloadData()
            self.privacyChanged(false)
        }
    }
    
    func showSelectNudgeOnDeleteAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    // delegate to delete account
    func deleteAccount(_ reason:String ,subReason:String ,rating:Int) {
        if(self.settingsMngr == nil) {
            self.settingsMngr = TMSettingsManager()
        }
        
        if(self.settingsMngr.isNetworkReachable()) {
            var queryString = Dictionary<String,AnyObject>()
            queryString["action"] = "delete_account" as AnyObject?
            queryString["reason"] = reason as AnyObject?
            queryString["sub_reason"] = subReason as AnyObject?
            queryString["app_rate"] = rating as AnyObject?
            self.topView().isUserInteractionEnabled = false
            self.addIndicator()
            
            self.settingsMngr.deleteUserProfile(queryString as [NSObject : AnyObject], with: { (response:Bool, error:TMError?) -> Void in
                self.topView().isUserInteractionEnabled = true
                self.removeLoader()
                
                if(response) {
                    if(self.actionDelegate !=  nil) {
                        self.actionDelegate!.setNavigationFlowForLogoutAction!(true)
                    }
                }else {
                    // handle error condition
                    if(error?.errorCode == .TMERRORCODE_LOGOUT) {
                        if(self.actionDelegate !=  nil) {
                            self.actionDelegate!.setNavigationFlowForLogoutAction!(false)
                        }
                    }
                    else if(error?.errorCode == .TMERRORCODE_NONETWORK) {
                        self.showAlert(withTitle: TM_INTERNET_NOTAVAILABLE_MSG, msg: "", withDelegate: nil)
                    }
                    else if(error?.errorCode == .TMERRORCODE_DATASAVEERROR){
                        self.showAlert(withTitle: error?.errorMessage, msg: "", withDelegate: nil)
                    }
                    else {
                        self.showAlert(withTitle: TM_UNKNOWN_MSG, msg: "", withDelegate: nil)
                    }
                }
            })
        }
        else {
            self.showAlert(withTitle: TM_INTERNET_NOTAVAILABLE_MSG, msg: "", withDelegate: nil)
        }
    }
    
    func addIndicator() {
        if(self.isRetryViewShown) {
            self.removeRetryView()
        }
        //self.navigationController?.topViewController!.navigationItem.hidesBackButton = true
        self.navigationController?.topViewController!.navigationItem.leftBarButtonItem?.isEnabled = false
        let fullScreen = TMFullScreenView()
        fullScreen.frame = self.view.frame
        fullScreen.frame.origin.y = fullScreen.frame.origin.y - 64.0
        fullScreen.backgroundColor = UIColor.white
        fullScreen.alpha = 1.0
        //self.view.addSubview(fullScreen)
        self.topView().addSubview(fullScreen)
        
        self.activityIndicator = TMActivityIndicatorView()
        self.activityIndicator.titleText = "Processing your request..."
        self.activityIndicator.startAnimating()
        self.topView().addSubview(self.activityIndicator)
        
        self.topView().bringSubview(toFront: fullScreen)
        
        self.topView().bringSubview(toFront: self.activityIndicator)
        
    }
    
    func removeLoader() {
        self.navigationController?.topViewController!.navigationItem.leftBarButtonItem?.isEnabled = true
        //self.navigationController?.topViewController!.navigationItem.hidesBackButton = false
        if(self.activityIndicator != nil) {
            self.activityIndicator.stopAnimating()
        }
        for view in self.topView().subviews {
            if(view.isKind(of: TMFullScreenView.self)) {
                view.removeFromSuperview()
            }
        }
    }
    
    func topView() -> UIView {
        let controller = self.navigationController?.topViewController
        if(controller?.view !=  nil){
            return (controller?.view)!
        }
        return self.view
    }
    
}

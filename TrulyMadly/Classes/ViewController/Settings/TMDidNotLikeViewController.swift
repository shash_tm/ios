//
//  TMDidNotLikeViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

import UIKit

class TMDidNotLikeViewController: TMBaseViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIAlertViewDelegate {

    var tableView: UITableView?
    var reasonDic:[Int:Array<Bool>] = [0:[false,false], 1:[false,false,false,false], 2:[false], 3:[false], 4:[false], 5:[false]]
    let ReasonArr:[Int:Array<String>] = [0:["Married matches","Provided wrong info"], 1:["Didn\'t like any profiles", "Intentions didn\'t match","Boring conversations","Others"],2:["Privacy Concerns"],3:["App is slow"] ]
    var textView: UITextView!
    var showPrivacy = false
    var textReason = ""
    var indexPath: IndexPath!
    
    convenience init(showPrivacy:Bool) {
        self.init()
        self.showPrivacy = showPrivacy
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationItem.hidesBackButton = true
        self.title = "Didn\'t Like It Here"
        tableView = UITableView(frame: view.bounds, style: .grouped)
        
        if let theTableView = tableView{
            
            theTableView.register(TMTextFieldTableViewCell.self, forCellReuseIdentifier: "uiTextView")
            theTableView.register(TMPublicTableViewCell.self, forCellReuseIdentifier: "uiPublic")
            
            theTableView.dataSource = self
            theTableView.delegate = self
            theTableView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            
            view.addSubview(theTableView)
        }
        self.leftBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func dismissKeyboard() {
        if(self.textView != nil) {
            self.textView.resignFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        tableView?.reloadData()
    }
    
    private func leftBarButton() {
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_button")!, style: .done, target: self, action: #selector(TMDidNotLikeViewController.backButtonClicked))
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func backButtonClicked() {
        self.navigationController!.popViewController(animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(self.showPrivacy) {
            return self.reasonDic.count
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.reasonDic[section]?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if((indexPath as NSIndexPath).section == 4) {
            return 120
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(self.showPrivacy) {
            if(section == 5) {
                return 120
            }
        }else {
            if(section == 4) {
                return 120
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 50
        }
        if(section == 1 || section == 4){
            return 30
        }
        if(section == 5) {
            return 75
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(self.showPrivacy && section == 5) {
            return newViewForFooter(isEmpty: false)
        }
        else if(!self.showPrivacy && section == 4) {
            return newViewForFooter(isEmpty: false)
        }
        return newViewForFooter(isEmpty: true)
    }
    
    func newViewForFooter(isEmpty:Bool) -> UIView {
        let height: CGFloat = (isEmpty) ? 0 : 120
        let resultFrame = CGRect(x: 0, y: 0, width: view.bounds.width, height: height)
        let headerView = UIView(frame: resultFrame)
        
        if(!isEmpty) {
            let deletebutton = deleteButton()
            deletebutton.frame.origin.y = 38
            headerView.addSubview(deletebutton)
        }
        return headerView
    }
    
    func deleteButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44)
        button.setTitleColor(UIColor(red:3.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1.0), for: UIControlState())
        button.setTitle("Delete Anyway", for: UIControlState())
        button.addTarget(self, action: #selector(TMDidNotLikeViewController.deleteButtonClicked), for: UIControlEvents.touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.titleLabel?.textAlignment = .center
        return button
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        if(section == 0) {
            return newViewForHeaderWithText(header: "", subHeader: "PROFILES WERE MISLEADING", section: 0)
        }
        else if(section == 1) {
            return newViewForHeaderWithText(header: "", subHeader: "DIDN\'T FIND ANYONE", section: 1)
        }
        else if(section == 4) {
            return newViewForHeaderWithText(header: "", subHeader: "OTHERS", section: 4)
        }
        else if(section == 5) {
            return newViewForHeaderWithText(header: "WHY DELETE?", subHeader: "Try Profile Visibility and be visible only to the ones you \'Like\'.", section: 5)
        }
        return newViewForHeaderWithText(header: "", subHeader: "", section: 2)
    }
    
    func newViewForHeaderWithText(header:String, subHeader:String, section:Int)-> UIView {
        
        let headerLabel = newLabelWithTitle(title: header, subHeading: false)
        
        headerLabel.frame.origin.x += 13
        headerLabel.frame.origin.y = 5
        
        var height: CGFloat = 10
        if(section == 0){
            height = 50
            headerLabel.frame.origin.y = 20
        }
        else if(section == 1 || section == 4) {
            height = 30
            headerLabel.frame.origin.y = 0
            headerLabel.frame.size.height = 0
        }
        else if(section == 5) {
            height = 75
        }

        let subheaderLabel = newLabelWithTitle(title: subHeader, subHeading: true)
        subheaderLabel.frame.origin.x = headerLabel.frame.origin.x
        subheaderLabel.frame.origin.y = headerLabel.frame.origin.y + headerLabel.frame.size.height+5
        
        let resultFrame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: height)
        let headerView = UIView(frame: resultFrame)
        headerView.addSubview(headerLabel)
        headerView.addSubview(subheaderLabel)
        
        return headerView
    }
    
    func newLabelWithTitle(title: String, subHeading:Bool) -> UILabel{
    
        let label = UILabel()
        label.text = title
        
        var rect:CGRect
        
        let textColor: UIColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        var textFont: UIFont = UIFont.systemFont(ofSize: 14)
        
        let widthAllowed = self.view.bounds.size.width - 20 as AnyObject
        if(subHeading) {
            let attributes : Dictionary<String,AnyObject> = ["kfont": textFont, "ktext":title as AnyObject, "kmaxwidth":widthAllowed]
            rect = TMStringUtil.textRect(forString: attributes)
        }else {
            textFont = UIFont(name: "HelveticaNeue-Light", size: 15)!
            let attributes : Dictionary<String,AnyObject> = ["kfont": textFont, "ktext":title as AnyObject, "kmaxwidth":widthAllowed]
            rect = TMStringUtil.textRect(forString: attributes)
        }
        
        label.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        label.font = textFont
        label.textColor  = textColor
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch((indexPath as NSIndexPath).section) {
        case 0:
            var cell = tableView.dequeueReusableCell(withIdentifier: "uiText")
            if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier:"uiText")
            }
            cell!.selectionStyle = .none
            switch((indexPath as NSIndexPath).row) {
                case 0:
                    cell?.textLabel?.text = self.ReasonArr[0]![0]
                    return cell!
                case 1:
                    cell?.textLabel?.text = self.ReasonArr[0]![1]
                    return cell!
                default: fatalError("Unknown row")
            }
        case 1:
            var cell = tableView.dequeueReusableCell(withIdentifier: "uiText")
            if (cell == nil) {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier:"uiText")
            }
            cell!.selectionStyle = .none
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell?.textLabel?.text = self.ReasonArr[1]![0]
                return cell!
            case 1:
                cell?.textLabel?.text = self.ReasonArr[1]![1]
                return cell!
            case 2:
                cell?.textLabel?.text = self.ReasonArr[1]![2]
                return cell!
            case 3:
                cell?.textLabel?.text = self.ReasonArr[1]![3]
                return cell!
            default: fatalError("Unknown row")
            }
        case 2:
            var cell = tableView.dequeueReusableCell(withIdentifier: "uiText")
            if (cell == nil) {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier:"uiText")
            }
            cell!.selectionStyle = .none
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell?.textLabel?.text = self.ReasonArr[2]![0]
                return cell!
            default: fatalError("Unknown row")
            }
        case 3:
            var cell = tableView.dequeueReusableCell(withIdentifier: "uiText")
            if (cell == nil) {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier:"uiText")
            }
            cell!.selectionStyle = .none
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell?.textLabel?.text = self.ReasonArr[3]![0]
                return cell!
            default: fatalError("Unknown row")
            }
        case 4:
            let cell:TMTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiTextView") as! TMTextFieldTableViewCell
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell.headerTextField.delegate = self
                cell.selectionStyle = .none
                self.textView = cell.headerTextField
                self.indexPath = indexPath
                return cell
            default: fatalError("Unknown row")
            }
        case 5:
            let cell:TMPublicTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiPublic") as! TMPublicTableViewCell
            switch((indexPath as NSIndexPath).row) {
            case 0:
                cell.selectionStyle = .none
                cell.profileVisibiltyButton.addTarget(self, action: #selector(TMDidNotLikeViewController.profileVisibilityClicked), for: UIControlEvents.touchUpInside)
                return cell
            default: fatalError("Unknown row")
            }
            
        default: fatalError("Unknown  Section")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if((indexPath as NSIndexPath).section == 0 || (indexPath as NSIndexPath).section == 1 || (indexPath as NSIndexPath).section == 2 || (indexPath as NSIndexPath).section == 3) {
                let cell: UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
                if(self.reasonDic[(indexPath as NSIndexPath).section]![(indexPath as NSIndexPath).row]) {
                    cell.accessoryType = .none
                }else {
                    cell.accessoryType = .checkmark
                }
                self.reasonDic[(indexPath as NSIndexPath).section]![(indexPath as NSIndexPath).row] = !self.reasonDic[(indexPath as NSIndexPath).section]![(indexPath as NSIndexPath).row]
        }
    }
    
    func profileVisibilityClicked() {
        self.delegate?.didPrivacyClicked!(false)
    }

    // pragma mark textview delegate methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == " Write here") {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == "") {
            textView.text = " Write here"
            textView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.textReason = textView.text.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func deleteButtonClicked() {
        self.dismissKeyboard()
        
        let alert = UIAlertView(title: "" , message: "There is no looking back! All your info will be permanently deleted." , delegate: self, cancelButtonTitle: "Cancel")
        alert.addButton(withTitle: "Delete")
        
        DispatchQueue.main.async(execute: { () -> Void in
            alert.show()
        })
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        let title: NSString = alertView.buttonTitle(at: buttonIndex)! as NSString
        if(title == "Delete") {
            self.delegate?.deleteAccount!(self.title, subReason: self.getReason(), rating: 0)
        }else if(title == "Cancel") {
            let eventDict = ["status":"success", "screenName":"TMSettingsView", "eventAction":"delete_cancel_click", "eventCategory":"settings"]
            TMAnalytics.sharedInstance().trackNetworkEvent(eventDict as [NSObject : AnyObject])
        }
    }
    
    func getReason() -> String {
        var reasonText = ""
        var singleReason = true
        for i in (0 ..< self.reasonDic.count-2) {
            let tmpArr : Array<Bool> = self.reasonDic[i]!
            for j in (0 ..< tmpArr.count) {
                if(tmpArr[j]) {
                    if(singleReason) {
                        singleReason = false
                        reasonText = reasonText + self.ReasonArr[i]![j]
                    }
                    else {
                        reasonText = reasonText + ","
                        reasonText = reasonText + self.ReasonArr[i]![j]
                    }
                }
            }
        }
        if(self.textReason != ""){
            if(singleReason) {
                reasonText = reasonText + self.textReason
            }else {
                reasonText = reasonText + ","
                reasonText = reasonText + self.textReason
            }
        }
        return reasonText
    }
    
    // keyboard handling
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(TMDidNotLikeViewController.keyboardWillBeShown(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(TMDidNotLikeViewController.keyboardWillBeHidden(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // Called when the UIKeyboardDidShowNotification is sent.
    func keyboardWillBeShown(sender: Notification) {
        
        let info: NSDictionary = (sender as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardSize: CGSize = value.cgRectValue.size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        self.tableView!.contentInset = contentInsets;
        self.tableView!.scrollIndicatorInsets = contentInsets;
        self.tableView!.scrollToRow(at: self.indexPath, at: UITableViewScrollPosition.top, animated: true)
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden(sender: Notification) {
        self.tableView!.contentInset = UIEdgeInsets.zero;
        self.tableView!.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    

}

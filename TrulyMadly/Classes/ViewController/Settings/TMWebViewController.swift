//
//  TMWebViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 07/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

import UIKit

class TMWebViewController: TMBaseViewController, UIWebViewDelegate {

    var caller: String!
    var webView: UIWebView!
    var activityIndicator: UIActivityIndicatorView!
    
    convenience init(caller: String) {
        self.init()
        self.caller = caller
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.navigationItem.title = caller
        
        if(webView == nil) {
            webView = UIWebView(frame: self.view.frame)
            webView.delegate = self
            self.view.addSubview(webView)
            
            activityIndicator = UIActivityIndicatorView(frame:CGRect(x: 0,y: 0,width: 54,height: 54))
            activityIndicator.center = self.view.center
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            activityIndicator.hidesWhenStopped = true
            self.view.addSubview(activityIndicator);
            activityIndicator.startAnimating()
        }
        
        if(caller != nil) {
            let mainUrl: String = self.getUrl(caller)
            
            let urlString = TM_BASE_URL+"/"+mainUrl
            
            let url = URL(string: urlString)
            let request = NSMutableURLRequest(url: url!)
            
            let tmRequest: TMRequest = TMRequest(urlString: mainUrl)
            let header: NSDictionary = tmRequest.httpHeaders() as NSDictionary
            
            for (key,value) in header{
                request.addValue((value as? String)!, forHTTPHeaderField: key as! String)
            }
            webView.loadRequest(request as URLRequest)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        webView.frame = self.view.bounds;
    }
    
    func loadURL(_ URLString: String) -> Void {
        let url = URL(string: URLString)
        let request = NSMutableURLRequest(url: url!)
        if(webView == nil) {
            webView = UIWebView(frame: self.view.bounds)
            webView.delegate = self;
            self.view.addSubview(webView)
            
            activityIndicator = UIActivityIndicatorView(frame:CGRect(x: 0,y: 0,width: 54,height: 54))
            activityIndicator.center = self.view.center
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            activityIndicator.hidesWhenStopped = true
            self.view.addSubview(activityIndicator);
            activityIndicator.startAnimating()
        }
        
        request.addValue("iOSApp", forHTTPHeaderField:"source")
        request.addValue("iOS", forHTTPHeaderField:"app")
        request.addValue(TMCommonUtil.buildNumberString(), forHTTPHeaderField:"app_version_code")
        
        webView.loadRequest(request as URLRequest)
    }
    private func getUrl(_ key: String) -> String {
        var urlString = ""
        if(key == "Terms of Use") {
            urlString = KAPI_TERM_OF_USE
        }
        else if(key == "Safety Guidelines") {
            urlString = KAPI_SAFETY_GUIDELINES
        }
        else if(key == "Trust & Security") {
            urlString = KAPI_TRUST_SECURITY
        }
        else if(key == "True Compatibility") {
            urlString = KAPI_TRUE_COMPATIBILITY
        }
        return urlString
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error?) {
        activityIndicator.stopAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
}

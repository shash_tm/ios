//
//  TMDeleteReasonViewController.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

import UIKit

class TMDeleteReasonViewController: TMBaseViewController, UITableViewDelegate, UITableViewDataSource, TMCustomAlertViewDelegate {

    var tableView: UITableView?
    var gender: String?
    var isDiscoverySet: Bool = true
    
    func initialize() {
        self.gender = TMUserSession.sharedInstance().user.gender()

        if(TMDataStore.containsObject(forKey: "is_discovery_on")) {
            self.isDiscoverySet = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Delete My Account"
        self.navigationController?.navigationItem.hidesBackButton = true
        self.initialize()
        
        tableView = UITableView(frame: view.bounds, style: UITableViewStyle.grouped)
        
        if let theTableView = tableView{
            
            theTableView.register(TMTextLabelTableViewCell.self, forCellReuseIdentifier: "uiText")
            
            theTableView.dataSource = self
            theTableView.delegate = self
            theTableView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            
            view.addSubview(theTableView)
        }
        self.leftBarButton()
    }

    override func viewDidLayoutSubviews() {
        tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func leftBarButton() {
       let leftBarButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_button")!, style: .done, target: self, action: #selector(TMDeleteReasonViewController.backButtonClicked))
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func backButtonClicked() {
        self.navigationController!.popViewController(animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(self.gender == "F" && self.isDiscoverySet) {
//            return 4
//        }
        return 3
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch((indexPath as NSIndexPath).row) {
            case 0:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.textLabel?.text = "Met someone"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            case 1:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.textLabel?.text = "Was trying the app"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            case 2:
                let cell:TMTextLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "uiText") as! TMTextLabelTableViewCell
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textLabel?.text = "Didn\'t like it here"
                return cell
            default: fatalError("Unknown row")
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        return newViewForHeaderWithText()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if((indexPath as NSIndexPath).row == 0) {
           // print("found someone")
            let reasonExplain: TMReasonsExplainViewController = TMReasonsExplainViewController.init(deleteReason: DeleteReason.Found, showPrivacy:false)
            reasonExplain.delegate = self.delegate
            reasonExplain.actionDelegate = self.actionDelegate
            self.navigationController?.pushViewController(reasonExplain, animated: false)
        }
        else if((indexPath as NSIndexPath).row == 2) {
           // print("did not like")
            var showPrivacy = false
            if(self.gender == "F" && self.isDiscoverySet) {
                showPrivacy = true
            }
            let reasonExplain: TMDidNotLikeViewController = TMDidNotLikeViewController.init(showPrivacy: showPrivacy)
            reasonExplain.delegate = self.delegate
            reasonExplain.actionDelegate = self.actionDelegate
            self.navigationController?.pushViewController(reasonExplain, animated: false)
        }
        else if((indexPath as NSIndexPath).row == 1) {
            if(self.gender == "M") {
                //show alert with rate view
                self.addCustomAlert()
            }else {
                // already shown profile visibility option then show alert with rate view
                if(!self.isDiscoverySet) {
                    self.addCustomAlert()
                }else {
                    //move to another view with option to turn off profile visibility
                    let reasonExplain: TMReasonsExplainViewController = TMReasonsExplainViewController.init(deleteReason: DeleteReason.Public, showPrivacy:false)
                    reasonExplain.delegate = self.delegate
                    reasonExplain.actionDelegate = self.actionDelegate
                    self.navigationController?.pushViewController(reasonExplain, animated: false)
                }
            }
        }
    }
    
    func newViewForHeaderWithText() -> UIView{
        let headerLabel = newLabelWithTitle(title: "REASONS FOR DELETING", subHeading: false)
        
        headerLabel.frame.origin.x += 10
        headerLabel.frame.origin.y = 25
    
        let subheaderLabel = newLabelWithTitle(title: "It\'s hard to see you go. Tell us why", subHeading: true)
        subheaderLabel.frame.origin.x = headerLabel.frame.origin.x
        subheaderLabel.frame.origin.y = headerLabel.frame.origin.y + headerLabel.frame.size.height+5
        
        let resultFrame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 70)
        let headerView = UIView(frame: resultFrame)
        headerView.addSubview(headerLabel)
        headerView.addSubview(subheaderLabel)
        
        return headerView
    }
    
    func newLabelWithTitle(title: String, subHeading:Bool) -> UILabel{
        let label = UILabel()
        label.text = title
        if(subHeading) {
            label.font = UIFont.systemFont(ofSize: 14)
            //label.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        }else {
            label.font = UIFont(name: "HelveticaNeue-Light", size: 15)
            //label.textColor = UIColor.blackColor()
        }
        label.textColor = UIColor(red: 0.502, green:0.51, blue:0.522, alpha:1.0)
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }
    
    func addCustomAlert()
    {
        let customAlert = TMCustomAlertView()
        customAlert.initAlert(withMessageAndRateView: "All your info will be permanently deleted. Share your Experience before you go!", parentView:UIApplication.shared.windows.first , delegate: self, buttonTitle:"Delete")
    }
    
    // TMCustomAlertView Delegate methods
    func actionButtonPressed(_ rateScore: NSNumber!) {
        var rating: Int = 0
        if(rateScore != nil){
            rating = rateScore.intValue
        }
        self.delegate?.deleteAccount!("Was trying the app", subReason: "", rating: rating)
    }
    
    func cancelButtonPressed() {
        let eventDict = ["status":"success", "screenName":"TMSettingsView", "eventAction":"delete_cancel_click", "eventCategory":"settings"]
        TMAnalytics.sharedInstance().trackNetworkEvent(eventDict as [NSObject : AnyObject])
    }
    
}

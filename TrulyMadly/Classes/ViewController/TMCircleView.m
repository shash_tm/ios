//
//  TMCircleView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 12/09/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCircleView.h"
#import "TMUserSession.h"

@implementation TMCircleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    CGFloat radius = 90;
    if([[TMUserSession sharedInstance].user isUserFemale] && screenHeight > 480) {
        radius = 120;
    }
    CGFloat rectX = self.frame.size.width / 2;
    CGFloat rectY = self.frame.size.height / 2;
    CGFloat width = radius;
    CGFloat height = radius;
    CGFloat centerX = rectX - width/2;
    CGFloat centerY = rectY - height/2;
    
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(centerX, centerY, width, height)];
    bezierPath.lineWidth = 5;
    [[UIColor whiteColor] set];
    [bezierPath stroke];
}

@end

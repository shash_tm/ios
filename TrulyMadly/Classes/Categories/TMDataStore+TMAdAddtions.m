//
//  TMDataStore+TMAdAddtions.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 09/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDataStore+TMAdAddtions.h"
#import "TMUserSession.h"
#import "NSObject+TMAdditions.h"
#import "TMUser.h"
#import "TMAdManager.h"

@implementation TMDataStore (TMAdAddtions)

+ (void) setObjectWithUserIDForObject:(id) object key:(NSString*) key {
    [TMDataStore setObject:object forKey:[NSString stringWithFormat:@"%@_%@",key,[TMUserSession sharedInstance].user.userId]];
}

+ (BOOL) containsObjectWithUserIDForKey:(NSString*) key {
    return [TMDataStore containsObjectForKey:[NSString stringWithFormat:@"%@_%@",key,[TMUserSession sharedInstance].user.userId]]?YES:NO;
}

+ (id) retrieveObjectWithUserIDForKey:(NSString*) key {
    return [TMDataStore retrieveObjectforKey:[NSString stringWithFormat:@"%@_%@",key,[TMUserSession sharedInstance].user.userId]];
}

//updating SeventyNine Ad flags
+ (void) updateAdFlagsWithDictionary:(NSDictionary *)adFlagDictionary {
    
    if(adFlagDictionary) {
        NSString *globalInterstitialCounter, *interstitialTimeInterval, *seventyNineEnabled, *mediationEnabled, *matchesInterstitialCounter, *nativeTimeInterval, *profileEnabled, *profileRepeat, *profileFinalPosition, *profileTimeInterval;
        
        globalInterstitialCounter = [adFlagDictionary objectForKey:@"global_insterstitial_counter"];
        interstitialTimeInterval = [adFlagDictionary objectForKey:@"interstitial_time_interval"];
        seventyNineEnabled = [adFlagDictionary objectForKey:@"is_79_enabled"];
        mediationEnabled = [adFlagDictionary objectForKey:@"is_mediation_enabled"];
        matchesInterstitialCounter = [adFlagDictionary objectForKey:@"matches_insterstitial_counter"];
        nativeTimeInterval = [adFlagDictionary objectForKey:@"native_time_interval"];
        profileEnabled = [adFlagDictionary objectForKey:@"profile_ads_enabled"];
        profileFinalPosition = [adFlagDictionary objectForKey:@"profile_ads_final_position"];
        profileTimeInterval = [adFlagDictionary objectForKey:@"profile_ads_time_interval"];
        profileRepeat = [adFlagDictionary objectForKey:@"repeat"];
        
        if([seventyNineEnabled isValidObject]) {
            [TMDataStore setBool:[seventyNineEnabled boolValue] forKey:TMAD_ENABLED];
            /*if([seventyNineEnabled boolValue] && ![TMDataStore retrieveBoolforKey:TMAD_SDK_INITIALIZED]) {
              [TMAdManager initializeSeventyNineSDK]; //Initializing SeventyNine SDK if the flag is enabled.
            }*/
        }else {
            [TMDataStore setBool:NO forKey:TMAD_ENABLED];
        }
        
        if([globalInterstitialCounter isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:[globalInterstitialCounter intValue]] key:TMAD_INTERSTITIAL_GLOBAL_COUNT];
        }else {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_GLOBAL_COUNT];
        }
        
        if([interstitialTimeInterval isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:[interstitialTimeInterval doubleValue]] key:TMAD_INTERSTITIAL_TIME_INTERVAL];
        }else {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_INTERSTITIAL_TIME_INTERVAL];
        }
        
        if([mediationEnabled isValidObject]) {
            [TMDataStore setBool:[mediationEnabled boolValue] forKey:TMAD_MEDIATION_ENABLED];
        }else {
            [TMDataStore setBool:NO forKey:TMAD_MEDIATION_ENABLED];
        }
        
        if([matchesInterstitialCounter isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:[matchesInterstitialCounter intValue]] key:TMAD_INTERSTITIAL_MATCHES_COUNT];
        }else {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_MATCHES_COUNT];
        }
        
        if([nativeTimeInterval isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:[nativeTimeInterval doubleValue]] key:TMAD_NATIVE_TIME_INTERVAL];
        }else {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_NATIVE_TIME_INTERVAL];
        }
        
        if([profileEnabled isValidObject]) {
            [TMDataStore setBool:[profileEnabled boolValue] forKey:TMAD_PROFILE_ENABLED];
            if([profileEnabled boolValue]) {
                if(![TMDataStore retrieveBoolforKey:TMAD_PROFILE_VIEWED]) {
                   [TMDataStore setBool:NO forKey:TMAD_PROFILE_VIEWED];
                }
                if(![TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_FIRST_TIMESTAMP]) {
                   [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_PROFILE_FIRST_TIMESTAMP];
                }
                
            }
        }else {
            [TMDataStore setBool:NO forKey:TMAD_PROFILE_ENABLED];
        }
        
        if([profileFinalPosition isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:[profileFinalPosition intValue]] key:TMAD_PROFILE_FINAL_POSITION];
        }else {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:-1] key:TMAD_PROFILE_FINAL_POSITION];
        }
        
        if([profileTimeInterval isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:[profileTimeInterval doubleValue]] key:TMAD_PROFILE_TIME_INTERVAL];
        }else {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_PROFILE_TIME_INTERVAL];
        }
        
        if([profileRepeat isValidObject]) {
            [TMDataStore setBool:[profileRepeat boolValue] forKey:TMAD_PROFILE_REPEAT];
        }else {
            [TMDataStore setBool:NO forKey:TMAD_PROFILE_REPEAT];
        }
        
        if(![[TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_LAST_TIMESTAMP] isValidObject]) {
            [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_INTERSTITIAL_LAST_TIMESTAMP];
        }
    }else {
        [self resetAdFlags];
    }
}

+ (void) resetAdFlags {
    
    //Reset SeventyNine Ad Flags
    [TMDataStore setBool:NO forKey:TMAD_ENABLED];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_GLOBAL_COUNT];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_INTERSTITIAL_TIME_INTERVAL];
    [TMDataStore setBool:NO forKey:TMAD_MEDIATION_ENABLED];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_MATCHES_COUNT];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_NATIVE_TIME_INTERVAL];
    [TMDataStore setBool:NO forKey:TMAD_PROFILE_ENABLED];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:-1] key:TMAD_PROFILE_FINAL_POSITION];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_PROFILE_TIME_INTERVAL];
    [TMDataStore setBool:NO forKey:TMAD_PROFILE_REPEAT];
    
}

+ (void) resetInterstitialCounterFlags {
    //Reset SeventyNine Interstitial Ad Counter
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_CURRENT_COUNT];
}

+(void)removeObjectWithUserIDForKey:(NSString *)key {
    [TMDataStore removeObjectforKey:[NSString stringWithFormat:@"%@_%@",key,[TMUserSession sharedInstance].user.userId]];
}



@end

//
//  NSString+TMAdditions.h
//  TrulyMadly
//
//  Created by Ankit on 08/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface NSString (TMAdditions)

-(NSString*)trimBrTag;
-(NSString*)stringByReplacingInvalidContent;
-(NSString *)stringByTrimingWhitespace;
-(BOOL)isValidEmail;
-(BOOL)isNULLString;
-(CGRect)boundingRectWithConstraintSize:(CGSize)constraint  attributeDictionary:(NSDictionary*)attributes;
-(NSString*)getLastPathComponentFromURLString;
-(NSAttributedString*)attributedStringWithNonBoldAttributes:(NSDictionary*)nonBoldAttributes
                                             boldAttributes:(NSDictionary*)boldAttributes
                                                  boldRange:(NSRange)range;

@end

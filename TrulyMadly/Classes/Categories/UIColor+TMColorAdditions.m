//
//  UIColor+TMColorAdditions.m
//  TrulyMadly
//
//  Created by Ankit on 10/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "UIColor+TMColorAdditions.h"

@implementation UIColor (TMColorAdditions)

+(UIColor*)noNetworkIndicatorViewColor {
    return [UIColor colorWithRed:255.0f/255.0f green:81.0f/255.0f blue:81.0f/255.0f alpha:1];
}
+(UIColor*)loaderMessageTextColor
{
    UIColor *textColor = [UIColor colorWithRed:0.384 green:0.557 blue:0.745 alpha:1.0];
    return textColor;
}
+(UIColor*)alertBackgroundColor {
    UIColor *selColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];//[UIColor colorWithRed:135.0f/255.0f green:135.0f/255.0f blue:135.0f/255.0f alpha:1.0];
    return selColor;
}
+(UIColor*)signupButtonColor {
    return [UIColor colorWithRed:59.0f/255.0f green:86.0f/255.0f blue:156.0f/255.0f alpha:1];
}
+(UIColor*)likeColor {
    return [UIColor colorWithRed:255/255.0f green:0/255.0f blue:114/255.0f alpha:1.0];
}
+(UIColor*)hideColor {
    return [UIColor colorWithRed:66/255.0f green:199/255.0f blue:233/255.0f alpha:1.0];
}
+(UIColor*)sparkColor {
    return [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:1.0];
}
+(UIColor*)separatorColor {
    return [UIColor colorWithRed:70/255.0f green:163/255.0f blue:197/255.0f alpha:1.0];
}
+(UIColor*)profileCollectionViewCellSeparatorColor {
    return [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
}
+(UIColor*)facebookCommonFriendCountTextColor {
    return [UIColor colorWithRed:249.0f/255.0f green:160.0f/255.0f blue:169.0f/255.0f alpha:1];
}
+(UIColor*)nudgeMessageViewColor {
    return [UIColor colorWithRed:173.0/255.0 green:197.0/255.0 blue:232.0/255.0 alpha:1];//[UIColor colorWithRed:240.0f/255.0f green:235.0f/255.0f blue:165.0f/255.0f alpha:1];
}
+(UIColor*)likeselectedColor {
    UIColor *selColor = [UIColor colorWithRed:172.0f/255.0f green:198.0f/255.0f blue:229.0f/255.0f alpha:1];
    return selColor;
}
+(UIColor*)commonPropertyColor {
    /*UIColor *selColor = [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:114.0f/255.0f alpha:1];*/
    UIColor *selColor = [UIColor blackColor];
    return selColor;
}
+(UIColor*)likeunSelectedColor {
    UIColor *unselColor = [UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1];
    //UIColor *unselColor = [UIColor colorWithRed:230.0f/255.0f green:231.0f/255.0f blue:232.0f/255.0f alpha:1];
    return unselColor;
}
+(UIColor*)commonLikeColor {
    UIColor *selColor = [UIColor colorWithRed:172.0f/255.0f green:198.0f/255.0f blue:229.0f/255.0f alpha:1];
    return selColor;
}
+(UIColor*)activeVarificationBgColor {
    return [UIColor colorWithRed:88/255.0f green:141/255.0f blue:204/255.0f alpha:1];
}
+(UIColor*)inactiveVarificationBgColor {
    return [UIColor colorWithRed:242/255.0f green:242/255.0f blue:242/255.0f alpha:1];
}
+(UIColor*)activeVarificationTextColor {
    return [UIColor whiteColor];
}
+(UIColor*)inactiveVarificationTextColor {
    return [UIColor colorWithRed:209/255.0f green:211/255.0f blue:212/255.0f alpha:1];
}
+(UIColor*)inactivePageIndexColor {
    return [UIColor colorWithRed:154/255.0f green:154/255.0f blue:154/255.0f alpha:0.2];
}
+(UIColor*)activeButtonColor {
    return [UIColor colorWithRed:249.0f/255.0f green:162.0f/255.0f blue:170.0f/255.0f alpha:1];
}
+(UIColor*)inactiveButtonColor {
    return [UIColor colorWithRed:224.0f/255.0f green:225.0f/255.0f blue:226.0f/255.0f alpha:1];
}


+(UIColor*)conversationListSeperatorColor {
    return [UIColor colorWithRed:231.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:1];
}
+(UIColor*)messgaeListSubTitleTextColor {
    return [UIColor colorWithRed:97.0f/255.0f green:97.0f/255.0f blue:97.0f/255.0f alpha:1];
}
+(UIColor*)unreadConversationDateColor {
    return [UIColor colorWithRed:21.0f/255.0f green:122.0f/255.0f blue:223.0f/255.0f alpha:1];
}
+(UIColor*)readConversationDateColor {
    return [UIColor colorWithRed:119.0f/255.0f green:119.0f/255.0f blue:119.0f/255.0f alpha:1];
}
+(UIColor*)sparkConversationTimerColor {
    return [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:114.0f/255.0f alpha:1];
}
+(UIColor*)archiveMessageCellTextColor {
    UIColor *selColor = [UIColor colorWithRed:9.0f/255.0f green:92.0f/255.0f blue:255.0f/255.0f alpha:1];
    return selColor;
}
+(UIColor*)sendButtonColor {
    return [UIColor colorWithRed:60.0f/255.0f green:137.0f/255.0f blue:166.0f/255.0f alpha:1];
}
+(UIColor*)unreadMessageBgColor {
    return [UIColor colorWithRed:255.0f/255.0f green:179.0f/255.0f blue:185.0f/255.0f alpha:1];
}
+(UIColor*)messageInputToolbarTintColor {
    return [UIColor colorWithRed:97.0f/255.0f green:97.0f/255.0f blue:97.0f/255.0f alpha:1];//[UIColor colorWithRed:128.0f/255.0f green:130.0f/255.0f blue:133.0f/255.0f alpha:1];
}
+(UIColor*)messageCollectionViewHeaderTitleColor {
    return [UIColor colorWithRed:84/255.0 green:84/255.0 blue:84/255.0 alpha:1.0];
}
+(UIColor*)messageCollectionViewHeaderSubTitleColor {
    return [UIColor colorWithRed:84/255.0 green:84/255.0 blue:84/255.0 alpha:1.0];
}
+(UIColor*)outgoingMessageColor {
    return [UIColor colorWithRed:255/255.0 green:232/255.0 blue:236/255.0 alpha:1.0];
}
+(UIColor*)incomingMessageColor {
    return [UIColor colorWithRed:255/255.0 green:248/255.0 blue:229/255.0 alpha:1.0];
}
+(UIColor*)quizMessageColor {
    UIColor *selColor = [UIColor colorWithRed:162.0f/255.0f green:191.0f/255.0f blue:201.0f/255.0f alpha:1];
    return selColor;
}
+(UIColor *)missTMtextColor
{
    return [UIColor colorWithRed:0.929 green:0.043 blue:0.427 alpha:1];
}
+(UIColor *)missTMLineColor
{
    return [UIColor colorWithRed:0.573 green:0.698 blue:0.835 alpha:1] ;
}
+(UIColor*)sparkTileInConversatinHeaderColor {
    return [UIColor colorWithRed:60/255.0f green:181/255.0f blue:212/255.0f alpha:1.0];
}
+(UIColor*)conversationHeaderColor {
    return [UIColor whiteColor];//[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
}
+(UIColor*)conversationHeaderTitleColor {
    return [UIColor blackColor];//[UIColor colorWithRed:129/255.0 green:127/255.0 blue:127/255.0 alpha:1.0];
}
+(UIColor*)sparkCollectionViewCellTileColor {
    return [UIColor colorWithRed:70/255.0 green:163/255.0 blue:197/255.0 alpha:1.0];
}
+(UIColor*)sparkCollectionViewCellMessageColor {
    return [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
}
+(UIColor*)sparkCollectionViewCellSeperatorColor {
    return [UIColor colorWithRed:66/255.0 green:199/255.0 blue:233/255.0 alpha:1.0];
}

@end

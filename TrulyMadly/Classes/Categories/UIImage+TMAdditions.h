//
//  UIImage+TMAdditions.h
//  TrulyMadly
//
//  Created by Ankit on 08/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TMAdditions)

+ (UIImage *)bubbleCompactImage;
- (UIImage*)scaleImageToSize:(CGSize)size;
- (UIImage*)imageMaskedWithColor:(UIColor*)maskColor;
- (UIImage *)fixOrientation;

@end

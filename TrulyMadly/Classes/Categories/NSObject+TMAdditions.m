//
//  NSObject+TMAdditions.m
//  TrulyMadly
//
//  Created by Ankit on 14/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "NSObject+TMAdditions.h"

@implementation NSObject (TMAdditions)

-(BOOL)isValidObject {
    if(self != nil && (![self isKindOfClass:[NSNull class]])) {
        return TRUE;
    }
    return FALSE;
}

-(BOOL)isNStringObject {
    if([self isKindOfClass:[NSString class]]) {
        return TRUE;
    }
    return FALSE;
}

@end

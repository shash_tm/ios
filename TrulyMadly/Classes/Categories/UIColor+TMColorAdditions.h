//
//  UIColor+TMColorAdditions.h
//  TrulyMadly
//
//  Created by Ankit on 10/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (TMColorAdditions)

+(UIColor*)likeselectedColor;
+(UIColor*)likeunSelectedColor;
+(UIColor*)commonLikeColor;
+(UIColor*)activeVarificationBgColor;
+(UIColor*)inactiveVarificationBgColor;
+(UIColor*)activeVarificationTextColor;
+(UIColor*)inactiveVarificationTextColor;
+(UIColor*)inactivePageIndexColor;
+(UIColor*)activeButtonColor;
+(UIColor*)inactiveButtonColor;
+(UIColor*)sendButtonColor;
+(UIColor*)unreadMessageBgColor;
+(UIColor*)messgaeListSubTitleTextColor;
+(UIColor*)messageInputToolbarTintColor;
+(UIColor*)facebookCommonFriendCountTextColor;
+(UIColor*)signupButtonColor;
+(UIColor*)noNetworkIndicatorViewColor;
+(UIColor*)nudgeMessageViewColor;
+(UIColor*)quizMessageColor;
+(UIColor*)archiveMessageCellTextColor;
+(UIColor*)loaderMessageTextColor;
+(UIColor*)missTMtextColor;
+(UIColor*)missTMLineColor;
+(UIColor*)alertBackgroundColor;
+(UIColor*)likeColor;
+(UIColor*)hideColor;
+(UIColor*)commonPropertyColor;
+(UIColor*)sparkColor;
+(UIColor*)separatorColor;
+(UIColor*)sparkTileInConversatinHeaderColor;
+(UIColor*)conversationHeaderColor;
+(UIColor*)conversationHeaderTitleColor;
+(UIColor*)sparkCollectionViewCellTileColor;
+(UIColor*)sparkCollectionViewCellMessageColor;
+(UIColor*)sparkCollectionViewCellSeperatorColor;
+(UIColor*)messageCollectionViewHeaderTitleColor;
+(UIColor*)messageCollectionViewHeaderSubTitleColor;
+(UIColor*)conversationListSeperatorColor;
+(UIColor*)unreadConversationDateColor;
+(UIColor*)readConversationDateColor;
+(UIColor*)outgoingMessageColor;
+(UIColor*)incomingMessageColor;
+(UIColor*)sparkConversationTimerColor;
+(UIColor*)profileCollectionViewCellSeparatorColor;

@end

//
//  NSObject+TMAdditions.h
//  TrulyMadly
//
//  Created by Ankit on 14/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (TMAdditions)

-(BOOL)isValidObject;
-(BOOL)isNStringObject;
@end

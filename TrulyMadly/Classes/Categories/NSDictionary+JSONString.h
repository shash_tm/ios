//
//  NSDictionary+JSONString.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSONString)

-(NSString*) jsonStringWithPrettyPrint:(BOOL) prettyPrint ;

@end

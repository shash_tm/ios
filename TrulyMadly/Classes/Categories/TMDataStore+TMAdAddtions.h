//
//  TMDataStore+TMAdAddtions.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 09/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDataStore.h"

//ads specific keys
#define AD_INTERSTITIAL_TOTAL_COUNT @"adInterstitialTotalCount"
#define AD_NATIVE_TOTAL_COUNT @"adNativeTotalCount"
#define AD_INTERSTITIAL_MATCHES_COUNT @"adInterstitialMatchesCount"
#define AD_INTERSTITIAL_CONVERSATION_COUNT @"adInterstitialConversationCount"
#define AD_INTERSTITIAL_CURRENT_TOTAL_COUNT @"adInterstitialCurrentTotalCount"
#define AD_INTERSTITIAL_CURRENT_MATCHES_COUNT @"adInterstitialCurrentMatchesCount"
#define AD_INTERSTITIAL_CURRENT_CONVERSATION_COUNT @"adInterstitialCurrentConversationCount"
#define AD_NATIVE_CURRENT_COUNT @"nativeCurrentCount"
#define AD_INTERSTITIAL_TIME_INTERVAL @"adInterstitialTimeInterval"
#define AD_NATIVE_TIME_INTERVAL @"adNativeTimeInterval"
#define AD_NATIVE_LAST_TIMESTAMP @"adNativeLastTimestamp"
#define AD_INTERSTITIAL_LAST_TIMESTAMP @"adInterstitialLastTimestamp"
#define AD_PROFILE_COMPLETITION_VISIBILITY_COUNT @"adProfileCompletionVisibilityCount"

//SeventyNine Ad specific keys
#define TMAD_ENABLED @"is79Enabled"
#define TMAD_MEDIATION_ENABLED @"isMediationEnabled"
#define TMAD_SDK_INITIALIZED @"sdkInitialized"
#define TMAD_INTERSTITIAL_GLOBAL_COUNT @"globalInsterstitialCounter"
#define TMAD_INTERSTITIAL_TIME_INTERVAL @"interstitialTimeInterval"
#define TMAD_INTERSTITIAL_MATCHES_COUNT @"matchesInterstitialCounter"
#define TMAD_INTERSTITIAL_LAST_TIMESTAMP @"interstitialLastTimestamp"
#define TMAD_INTERSTITIAL_CURRENT_COUNT @"interstitialCurrentCount"
#define TMAD_PROFILE_ENABLED @"isProfileEnabled"
#define TMAD_PROFILE_FINAL_POSITION @"profileFinalPosition"
#define TMAD_PROFILE_REPEAT @"profileRepeat"
#define TMAD_PROFILE_VIEWED_COUNT @"profileViewedCount"
#define TMAD_PROFILE_VIEWED @"profileViewed"
#define TMAD_PROFILE_FIRST_TIMESTAMP @"profileFirstTimestamp"
#define TMAD_PROFILE_TIME_INTERVAL @"profileTimeInterval"
#define TMAD_NATIVE_TIME_INTERVAL @"nativeTimeInterval"
#define TMAD_NATIVE_LAST_TIMESTAMP @"nativeLastTimestamp"

@interface TMDataStore (TMAdAddtions)

/**
 * sets object in user defaults with userID included key
 * @param object
 * @param key
 */
+ (void) setObjectWithUserIDForObject:(id) object key:(NSString*) key;


/**
 * if user defaults contains object corresponding to user id specific key
 * @param key 
 * @return yes if object presene else no
 */
+ (BOOL) containsObjectWithUserIDForKey:(NSString*) key;


/**
 * returns object with the user id specific key
 * @param key
 * @return the object corresponding to the key
 */
+ (id) retrieveObjectWithUserIDForKey:(NSString*) key;


/**
 * resets the ad flags
 */
+ (void) resetAdFlags;


/**
 * resets the interstitial counter flags
 */
+ (void) resetInterstitialCounterFlags;


/**
 * reset the key
 * @param key
 */
+ (void) removeObjectWithUserIDForKey:(NSString*) key;

//SeventyNine Ad Flags Update method
/**
 * updates the cache with Ad Flags using the response object.
 * @param response object
 */
+ (void) updateAdFlagsWithDictionary:(NSDictionary*) adFlagDictionary;

@end

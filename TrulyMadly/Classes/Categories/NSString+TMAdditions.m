//
//  NSString+TMAdditions.m
//  TrulyMadly
//
//  Created by Ankit on 08/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "NSString+TMAdditions.h"

#import <UIKit/UIKit.h>

@implementation NSString (TMAdditions)

-(NSString*)trimBrTag {
    return [self stringByReplacingOccurrencesOfString:@"<br />" withString:@""];
}

-(NSString*)stringByReplacingInvalidContent {
    if(![self isKindOfClass:[NSString class]]){
        return @"";
    }
    return self;
}

-(NSString *)stringByTrimingWhitespace
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(BOOL)isValidEmail {
    if([self length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";//@"[\\w\\.\\+-]+@([\\w\\-]+\\.)+[a-zA-Z]{2,4}";
   
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:self options:0 range:NSMakeRange(0, [self length])];
    
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

-(CGRect)boundingRectWithConstraintSize:(CGSize)constraint  attributeDictionary:(NSDictionary*)attributes {
    
    CGRect rect = [self boundingRectWithSize:constraint
                                     options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                  attributes:attributes
                                     context:nil];
    return rect;
}

-(BOOL)isNULLString {
    if( [self isEqualToString:@"null"] || [self isEqualToString:@"<null>"] || [self isEqualToString:@"(null)"] ){
        return TRUE;
    }
    return FALSE;
}

-(NSString*)getLastPathComponentFromURLString {
    NSString *lastPath = [self lastPathComponent];
    return lastPath;
}

-(NSAttributedString*)attributedStringWithNonBoldAttributes:(NSDictionary*)nonBoldAttributes
                                  boldAttributes:(NSDictionary*)boldAttributes
                                    boldRange:(NSRange)range {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self attributes:nonBoldAttributes];
    [attributedString setAttributes:boldAttributes range:range];
    return attributedString;
}
@end

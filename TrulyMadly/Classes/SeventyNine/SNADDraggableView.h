//
//  SNADDraggableView.h
//  Bubble
//
//  Created by Poonam on 3/11/16.
//  Copyright © 2016 Seventynine. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNADDraggableViewDelegate;
@interface SNADDraggableView : UIView <UIGestureRecognizerDelegate>


@property (nonatomic, assign) id<SNADDraggableViewDelegate> delegate;
@property (nonatomic,strong)SNADDraggableView *draggableView;

- (void)snapViewCenterToPoint:(CGPoint)point edge:(CGRectEdge)edge;



@end

@protocol SNADDraggableViewDelegate <NSObject>

- (void)draggableViewHold:(SNADDraggableView *)view;
- (void)draggableView:(SNADDraggableView *)view didMoveToPoint:(CGPoint)point;
- (void)draggableViewReleased:(SNADDraggableView *)view;

- (void)draggableViewTouched:(SNADDraggableView *)view;

- (void)draggableViewNeedsAlignment:(SNADDraggableView *)view;

@end
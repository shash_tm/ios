//
//  SNADViewMAnager.h
//  libSeventynineAds
//
//  Created by Abhishek Sharma on 30/07/13.
//  Copyright (c) 2013 Seventynine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

//extern static NSString *k
#define __SNADSK

/***************************************************************************************************************/
/***************************************************************************************************************/

/*********************************         Show Documentation On Page         *********************************/
/**********      http://appjacket-developer-wiki.seventynine.mobi/how-to/ios-manual-integration      **********/

/***************************************************************************************************************/
/***************************************************************************************************************/



//Runtime Parameters For Developer
extern NSString * const SNpublisherId;

extern NSString * const SNAD_TyrooHashKey;

extern NSString * const SN_AdAutoRefreshTime; // Time in milliseconds (int)
extern NSString * const SN_InterstitialAdCloseAfterTime; // Time in milliseconds (int)
extern NSString * const SN_AdCloseButtonAppearTime; // Time in milliseconds (int)
extern NSString * const SN_AdClickDisabled; // Disable Click on Ad. [True/False]
extern NSString * const SN_AdContentType; // Select Ad Type: Video/Interstitial/VideoelseInterstitial
extern NSString * const SN_AdStartZone; // Make Video Ad visible/invisble by using 1 and 0. [On:1/Off:0]
extern NSString * const SN_AdHeaderZone; // Make Header Ad visible/invisble by using 1 and 0. [On:1/Off;0]
extern NSString * const SN_AdFooterZone; // Make Footer Ad visible/invisble by using 1 and 0. [On:1/Off:0]
extern NSString * const SN_AdAudioButton; // Enable/Disable sound of Ad. [Mute:0/Unmute:1]
extern NSString * const SN_AdLogoPosition; // Position of Logo when Ad is displayed.
extern NSString * const SN_AdSessionExcluded; //The Ad will not display on this session count (comma separated numerical values only).
extern NSString * const SN_AdSessionPattern; // Session Pattern For Ad Display.
extern NSString * const SN_GPSUseAllow; // Session Pattern For Ad Display.

extern NSString * const kSNADSDKReadyNotificationName;

/*
 * Shake Gesture Delegate Key Name
 */
extern NSString * const kSNADShakeNotification_adWillPresent;
extern NSString * const kSNADShakeNotification_adDidPresent;
extern NSString * const kSNADShakeNotification_adWillClose;
extern NSString * const kSNADShakeNotification_adDidClose;
extern NSString * const kSNADShakeNotification_adWillLeaveApplication;
extern NSString * const kSNADShakeNotification_adIdUpdated;
extern NSString * const kSNADShakeNotification_handleAdClicked;
extern NSString * const kSNADShakeNotification_adViewWillRemoveWithAdid;
extern NSString * const kSNADShakeNotification_adViewDidFailWithError;
extern NSString * const kSNADShakeNotification_adDidGetTrulyMadlyHashTag;
extern NSString * const kSNADShakeNotification_adDidClick;
extern NSString * const kSNADShakeNotification_adWillPresentWithHeight;


/*  *****************************************************************************************  */
/*  ********* Below key string use for following methods of ADBaseViewDelegate to
 - (void)delegateFired:(NSString *)delegateString adid:(NSString *)adId adType:(SeventynineAdType)adType
 
 */
extern NSString * const kSNADNoShow;
extern NSString * const kSNADAdReady;
extern NSString * const kSNADAdStarted;
extern NSString * const kSNADStart;
extern NSString * const kSNADFirstQuartile;
extern NSString * const kSNADSecondQuartile;
extern NSString * const kSNADThirdQuartile;
extern NSString * const kSNADCompleted;
extern NSString * const kSNADSkipEnabled;

extern NSString * const kSNADSmallBannerContentAvilable;
extern NSString * const kSNADSmallBannerContentUnavilable;

/*  *****************************************************************************************  */
/*  ************************** These key use to give data in method + (NSArray *)companionContent 
  return array contain dictionary objects **************************  */
extern NSString * const kSNADCompanionKey_width;
extern NSString * const kSNADCompanionKey_hieght;
extern NSString * const kSNADCompanionKey_contentType;
extern NSString * const kSNADCompanionKey_imagePath;
extern NSString * const kSNADcampaignKey_name;


typedef enum{
    NativeViewTypeGrid,
    NativeViewTypeDefault
} NativeViewType;

typedef enum{
	SmallViewTypeHeader,
	SmallViewTypeFooter
} SmallViewType;

// Cross Position by Developer
typedef NS_ENUM(NSInteger, SeventynineClosePosition)  {
    SeventynineClosePositionTop,
    SeventynineClosePositionBottom
};

typedef NS_ENUM(NSInteger, SeventynineBrandPosition)  {
    SeventynineBrandPositionTop,
    SeventynineBrandPositionBottom
};

typedef NS_ENUM(NSInteger, SeventynineBrandContent)  {
    SeventynineBrandContentNone,
    SeventynineBrandContentImage,
    SeventynineBrandContentText
};

typedef enum {
	kHeaderFooterContentTypeText,
	kHeaderFooterContentTypeWeb,
	kHeaderFooterContentTypeNone
	
}HeaderFooterContentType;

typedef enum {
	kSplashFullScreenContentTypeBoth,
	kSplashFullScreenContentTypeVideo,
	kSplashFullScreenContentTypeInterstitial,
	kSplashFullScreenContentTypeNone
	
}SplashFullScreenContentType;

typedef enum tagADViewState {
	kADViewStateDuplicate, //
	kADViewStateOldInstance = 0,
	kADViewStateNotConnectedToInternet,
	kADViewStateAppInBackground,
	kADViewStateNoAdToShow,
	kADViewStateNoViewToShow,
	kADViewStateUserActionClick,
	kADViewStateUserClosed,
	kADViewStateAdFinished,
	kADViewStateDeveloperClosed,
	kADViewStateNativeViewRemoved,
    kADViewStateUnKnown,
} ADViewState;

typedef NS_ENUM(NSInteger, SeventynineAdTheme) {
	SeventynineAdThemeDefault,
	SeventynineAdThemeCustom
};

typedef NS_ENUM(NSInteger, SeventynineAdType) {
	SeventynineAdTypeSmallBanner,
	SeventynineAdTypeMainStream,
    SeventynineAdTypeNative,
    SeventynineAdTypeTyroo
};

typedef NS_ENUM(NSInteger, SeventynineMainStreamAdLocation) {
	SeventynineMainStreamAdLocationMid,
	SeventynineMainStreamAdLocationPre
};

typedef NS_ENUM(NSInteger, SeventynineUserGender) {
    SeventynineUserGenderMale,
    SeventynineUserGenderFemale,
    SeventynineUserGenderUnknown
};

/*  *****************************************************************************************  */
/*  ************************** Ad UI and behavior decider variable **************************  */

extern NSString * const kSNADVariable_DelegateKey;// delgate object
extern NSString * const kSNADVariable_InAppBrowser;//Possible value @"YES", @"NO", default @"NO"
extern NSString * const kSNADVariable_ZoneIdKey;// zone id in string

// If you give background color black it prefer also pass this value @(SeventynineAdThemeCustom)
extern NSString * const kSNADVariable_BackgroundColorKey;// UIColor class object

extern NSString * const kSNADVariable_ThemeKey;//Possible value @(SeventynineAdThemeCustom), @(SeventynineAdThemeDefault), default @(SeventynineAdThemeDefault)

// These value used in video ad border design
extern NSString * const kSNADVariable_DrawBorderKey;//Possible value @"YES", @"NO", default @"YES"
extern NSString * const kSNADVariable_BorderAlphaKey;//NSNumber class object value between 0.0 to 1.0

// if yow want in fixed area ads will show instead of full screen pass this value @"NO" and you are responsible to handle frame of view and UI on orientation change of device
//extern NSString * const kSNADVariable_SdkHandleViewFrameKey;//Possible value @"YES", @"NO", default @"YES"
extern NSString * const kSNADVariable_AdIdKey;//any value in string, help to sort out for which Ad ADBaseViewDelegate method called
// It prefered give @(SeventynineMainStreamAdLocationPre) for ad come on first screen when launch app
extern NSString * const kSNADVariable_AdLocationKey;// Possible value @(SeventynineMainStreamAdLocationMid), @(SeventynineMainStreamAdLocationPre), default @(SeventynineMainStreamAdLocationMid)

extern NSString * const kSNADVariable_AdMobKey;

extern NSString * const kSNADVariable_CrossButtonKey; // Mange cross button viewability for native ad, Possible value @"YES", @"NO", default @"YES".
extern NSString * const kSNADVariable_CrossAppearTime;

extern NSString * const kSNADVariable_BrandContent;
extern NSString * const kSNADVariable_ClosePosition; //Possible value @(Top), @(Bottom), default @(Top)
extern NSString * const kSNADVariable_BrandPosition;//Possible value @(Top), @(Bottom), default @(Bottom)

extern NSString * const kSNADVariable_FixedViewRemovedFromSuperViewBySDKKey;//Possible value @"YES", @"NO", default @"NO"
extern NSString * const kSNADVariable_VideoAdInitialAudioStateMuteKey;//Possible value @"YES", @"NO"

extern NSString * const kSNADVariable_BannerType; //Possible value @(SmallViewTypeHeader), @(SmallViewTypeFooter), default @(SmallViewTypeFooter)

//extern NSString * const kSNADVariable_CrossPosition; //Possible value @(Top), @(Bottom), default @(Top)

// Tyroo Ad variables.
extern NSString * const kSNADTyrooAdVariable_SizeKey; // number of tyroo ads to be displayed in list.
extern NSString * const kSNADTyrooAdVariable_tyrooAdIdKey; //Ad Id Provided by Tyroo to show Tyroo ads
extern NSString * const kSNADTyrooAdVariable_tyrooAdWallIdKey; //Ad Wall Id Provided by Tyroo to show Tyroo ads

extern NSString * const kSNADVariable_HeightKey; // Set Your view according ad height.
extern NSString * const kSNADVariable_CustomNativeAdKey; //Possible value @"YES", @"NO", default @"NO"

extern NSString * const kSNADVariable_FullScreenAdKey;// For future use

extern NSString * const kSNADVP_Seventynine; // Set this key to show seventynine ads on priority.
extern NSString * const kSNADVP_TyrooNativeDefault; // Set this key to show tyroo native list view ads.
extern NSString * const kSNADVP_TyrooNativeGrid; // Set this key to show tyroo native grid view ads.
extern NSString * const kSNADVP_TyrooFullScreen;  // Set this key to show tyroo Interstitial ads.
extern NSString * const kSNADVariable_HeightKey;

extern NSString * const kSNADVariable_BannerAdSize;
extern NSString * const kSNADVariable_CustomEventName;
extern NSString * const kSNADVariable_BannerAutoRefreshEnable;

// App updation & Bubble Notification keys
extern NSString * const kSNADLastAppUpdated;
extern NSString * const kSNAD_isBubble;
extern NSString * const kSNAD_AppUpdated_Image_notification;
extern NSString * const kSNAD_Bubble_Image_notification;
extern NSString * const kSNAD_BubbleHide_notification;
extern NSString * const kSNAD_BubbleShow_notification;
extern NSString * const kSNAD_BubbleShow_WithDev_notification;
extern NSString * const kSNAD_BubbleHide_WithDev_notification;

/*  *****************************************************************************************  */
/*  *****************************************************************************************  */

extern NSString * const kSNADVariable_NativeAdViewKey; // You have to give UIView in this key to call Ad in Fixed View.


typedef void(^SNADAdReadyCompletion)(BOOL isReady);
typedef void (^SNADAdReadyBlock)(void);


@class SNADViewManager;
@protocol ADBaseViewDelegate <NSObject>

@optional
// Delegate Methods
- (void)adViewDidFailWithError:(NSError *)error;
- (void)adViewWillRemoveWithAdid:(NSString *)adId adState:(ADViewState)adstate adType:(SeventynineAdType)adType;

- (void)delegateFired:(NSString *)delegateString adid:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)adWillPresent:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adDidPresent:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)adWillClose:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adDidClose:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adWillLeaveApplication:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)adIdUpdated:(NSString *)newAdId oldAdId:(NSString *)oldAdId;

// Delegate Method for SDK Mediation.
- (void)adDidClickedWithAdId:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adWillPresentWithHeight:(NSInteger)height adId:(NSString *)adId adType:(SeventynineAdType)adType;

// Delegate Method for Truly Madly Parameter
- (void)adDidGetTrulyMadlyHashTag:(NSString *)hashTag;

@end

@interface SNADViewManager : NSObject
/**
 * Call this method to initialize seventynine sdk
 *
 * @param dict contain key-value pair for initialize sdk with option, for info about keys show SNADViewManager.h file
 */
+ (void)setRunTimeConfigration:(NSDictionary *)dict;

/**
 * Call this method to show small banner ad
 *
 * @param zoneId zone id for which want to check availability of ad
 * @param audioAd specify YES for you want specifically audio ad
 * @param developerHandleSize give YES if sdk does not handle ad view frame
 * @param completion completion is called on main thread as result calculated
 */

+ (void)isAdReadyWithZoneId:(NSString *)zoneId
                    audioAd:(BOOL)audioAd
               nativeAdView:(BOOL)nativeAdView
             goForMediation:(BOOL)goForMediation
                 completion:(SNADAdReadyCompletion)completion;

/**
 * Call this method to show small banner ad
 *
 * @param zoneId zone id for which want to check availability of ad
 * @param audioAd specify YES for you want specifically audio ad
 * @param developerHandleSize give YES if sdk does not handle ad view frame
 * @param completion completion is called on main thread as result calculated
 */
+ (void)isBannerAdReadyWithZoneId:(NSString *)zoneId
                   goForMediation:(BOOL)goForMediation
                       completion:(SNADAdReadyCompletion)completion;


/**
 * This method take a empty block which is excuted as ad ready to show.
 *
 * @param developerBlock emptyblock.
 */
+ (void)submitBlock:(SNADAdReadyBlock)developerBlock;

/**
 * Call this method to show FullScreen ads
 *
 * @param viewController ViewController in which you want to show ad content
 * @param options Options contain key-value pair for customise ad, for info about keys show SNADViewManager.h file
 * @param priority contains array for setting ad priority. Possibe Values: kSNADVP_TyrooFullScreen,kSNADVP_Seventynine, Default Value:kSNADVP_Seventynine
 * @param targettingCondition contains key-value pair for getting targeted ads. The keys can be like: id, lang, age, location and their value only location have specific value format in string (lat, lon, radius).
 */
+ (void)showFullScreenAdInViewController:(UIViewController*)viewController
                                priority:(NSArray *)priorityArray
                                 options:(NSDictionary *)options
                         targetCondition:(NSDictionary *)targetCondition;

/**
 * Call this method to play audio ad
 *
 * @param zoneId If want to play specific zone ad then provide zone id otherwise pass nil
 * @param adId This adId use in ADBaseViewDelegate method to specify for which ad object corresponding delegate called
 * @param delegate provide call back as audio ad finish
 */
+ (void)playAudioWithZoneId:(NSString *)zoneId
                       adId:(NSString *)adId
                   delegate:(id)delegate
            targetCondition:(NSDictionary *)targetCondition;

/**
 * Call this method to show Native ads
 *
 * @param view View in which you want to show ad content
 * @param viewController ViewController in which you want to show ad content
 * @param options Options contain key-value pair for customise ad, for info about keys show SNADViewManager.h file
 * @param priority contains array for setting ad priority. Possible Values: kSNADVP_TyrooNativeDefault, kSNADVP_TyrooNativeGrid, kSNADVP_Seventynine
    Default Value: kSNADVP_Seventynine
 * @param targettingCondition contains key-value pair for getting targeted ads. The keys can be like: id, lang, age, location and their value only location have specific value format in string (lat, lon, radius).
 */
+ (void)showNativeOrBannerAdInView:(UIView *)view
                          isBanner:(BOOL)isBanner
                          priority:(NSArray *)priorityArray
                    viewController:(UIViewController *)viewController
                           options:(NSDictionary *)options
                   targetCondition:(NSDictionary *)targetCondition;

/**
 *
 * Start a new session for ads.
 *
 */
+ (void)changeSession;

/**
 *
 * Close all ad except small banner.
 *
 */
+ (void)closeAd;

/*
 *
 * Get User's Info
 *
 */
+ (void)setAge:(NSInteger)age;
+ (void)setDateOfBirth:(NSDate *)dob;
+ (void)setGender:(SeventynineUserGender)gender;
+ (void)setLanguage:(NSString *)language;
+ (void)setEmail:(NSString *)email;
+ (void)setCompilationId:(NSString *)compId;
+ (void)setContentLanguage:(NSString *)contentLang;

/*
 *
 * Custom Value
 *
 */
+ (void)customKey:(NSString *)customKey value:(NSString *)value;

/**
 * This method return companion data for ad.
 *
 * @return Array of companion data associated with mainstream ad.
 */
+ (NSArray *)companionContent;

// For app jacket
+ (void)appJacketModeWithViewController:(UIViewController *)controller;

// For Facebook info
+ (void)facebookAccessToken:(NSString *)token;

/**
 * Call this method to show Mobikwik
 *
 * @return Array of Native Data For MobiKwik
 *
 */
+ (NSArray *)getCustomNativeAdDataWithZoneId:(NSString *)zoneId;
/**
 * Call this method to Fire Mobikwik Click Event
 *
 * Pass OrderId and BannerContentMd5(Provided in array of Native Data) For click event to be performed.
 *
 */
+ (void)nativeClickEventWithOrderId:(NSString *)orderID zoneId:(NSString *)zoneId bannerId:(NSString *)bannerId bannerContentMD5:(NSString *)bannerContentMD5;
/**
 *
 */
+ (void)OnNativeAdImpressionWithZoneId:(NSString *)zoneId bannerId:(NSString *)bannerId bannerContentMD5:(NSString *)bannerContentMD5;


+ (void)showBubbleWithDeveloperOption;
+ (void)hideBubbleWithDeveloperOption;

// App Updation Check
+ (void)checkForIfAppUpdateRequired;

// Push Notification
//+ (void)getDeviceTokenForPushNotification :(NSString *)deviceToken;
// Shake Gesture Method
+ (void)showAdOnShakeWithMotionManager:(CMMotionManager*)motionManager;


@end



/**********************************************************************************/
/***            Below category used by only seventynine other product           ***/
/**********************************************************************************/

@interface SNADViewManager (SNP_Additions)

+ (BOOL)snp_showNativeAdInView:(UIView *)view
                   options:(NSDictionary *)options
           targetCondition:(NSDictionary *)targetCondition;

+ (NSDictionary *)snp_configurationForVideoPlayer;

@end


@interface SNADViewManager (SNAD_BubbleAddition)

+ (void)showChathead;
+ (void)hideChathead;

// Notification Banner
+ (void)getNotification:(UILocalNotification *)notification;
+ (void)notificationDelegateIOS8:(NSString *)identifier notification:(UILocalNotification *) notification completionHandler:(void(^)())completionHandler;

@end

@interface SNADViewManager (SNAD_Anonymous)

+ (BOOL)hasAdAvilableForZoneId:(NSString *)zoneId;

@end

@interface SNADNativeAdView : UIView

@end



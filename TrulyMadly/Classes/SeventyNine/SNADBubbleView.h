//
//  SNADBubbleView.h
//
//  Created by Poonam on 3/11/16..
//  Copyright © 2016 Seventynine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNADBubbleView : UIView

@property (strong, nonatomic) UIImage *image;


@end

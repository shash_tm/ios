//
//  SNADDraggableView+Bubble.h
//  Bubble
//
//  Created by Poonam on 3/11/16.
//  Copyright © 2016 Seventynine. All rights reserved.
//

#import "SNADDraggableView.h"
#import "SNADBubbleView.h"

@interface SNADDraggableView (Bubble)


+ (id)draggableViewWithImage:(UIImage *)image;
+(id)createDraggableView;
@end

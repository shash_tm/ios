//
//  SNADDraggingCoordinator.h
//  Bubble
//
//  Created by Poonam on 3/11/16.
//  Copyright © 2016 Seventynine. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SNADDraggableView.h"

typedef enum {
    SNADSnappingEdgeBoth,
    SNADSnappingEdgeRight,
    SNADSnappingEdgeLeft
} SNADSnappingEdge;

@protocol SNADDraggingCoordinatorDelegate;
@interface SNADDraggingCoordinator : NSObject <SNADDraggableViewDelegate>

@property (nonatomic) SNADSnappingEdge snappingEdge;

@property (nonatomic, weak) id<SNADDraggingCoordinatorDelegate> delegate;

@property BOOL viewTouched;

- (id)initWithWindow:(UIWindow *)window draggableViewBounds:(CGRect)bounds;

@end

@protocol SNADDraggingCoordinatorDelegate <NSObject>

- (UIViewController *)draggingCoordinator:(SNADDraggingCoordinator *)coordinator viewControllerForDraggableView:(SNADDraggableView *)draggableView;

@end

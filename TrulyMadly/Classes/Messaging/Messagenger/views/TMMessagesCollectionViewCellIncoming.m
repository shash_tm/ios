//
//  TMMessagesCollectionViewCellIncoming.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesCollectionViewCellIncoming.h"

@implementation TMMessagesCollectionViewCellIncoming

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.cellBottomLabel.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat heightOffset = (CGRectGetHeight(self.cellTopLabel.bounds) + CGRectGetHeight(self.cellBottomLabel.bounds));
    
    CGFloat height = CGRectGetHeight(self.contentView.bounds) - heightOffset;
    self.sentFailedIndicatorContainerView.frame = CGRectMake(CGRectGetMinX(self.contentView.bounds)+5,
                                                             5,
                                                             5,
                                                             5);
    
    CGFloat xPos = CGRectGetMaxX(self.sentFailedIndicatorContainerView.frame);
    self.messageBubbleContainerView.frame = CGRectMake(xPos,
                                                       CGRectGetMaxY(self.cellTopLabel.bounds),
                                                       self.bubbleContainerWidth,
                                                       height);
    
    self.messageBubbleImageView.frame = self.messageBubbleContainerView.bounds;
    
    self.messageTextView.frame = CGRectMake(6,
                                            CGRectGetMinY(self.messageBubbleContainerView.bounds),
                                            CGRectGetWidth(self.messageBubbleContainerView.bounds)-6,
                                            CGRectGetHeight(self.messageBubbleContainerView.bounds));
}

@end

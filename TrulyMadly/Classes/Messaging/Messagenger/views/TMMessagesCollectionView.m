//
//  TMMessagesCollectionView.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesCollectionView.h"
#import "TMMessagesCollectionViewCellIncoming.h"
#import "TMMessagesCollectionViewCellOutgoing.h"


@implementation TMMessagesCollectionView

@dynamic dataSource;
@dynamic delegate;
@dynamic collectionViewLayout;

-(void)configureCollectionView {
    self.backgroundColor = [UIColor whiteColor];
    self.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    self.alwaysBounceVertical = YES;
    self.bounces = YES;
    
    [self registerClass:[TMMessagesCollectionViewCellIncoming class] forCellWithReuseIdentifier:[TMMessagesCollectionViewCellIncoming cellReuseIdentifier]];
    [self registerClass:[TMMessagesCollectionViewCellIncoming class] forCellWithReuseIdentifier:[TMMessagesCollectionViewCellIncoming mediaCellReuseIdentifier]];
    
    [self registerClass:[TMMessagesCollectionViewCellOutgoing class] forCellWithReuseIdentifier:[TMMessagesCollectionViewCellOutgoing cellReuseIdentifier]];
    [self registerClass:[TMMessagesCollectionViewCellOutgoing class] forCellWithReuseIdentifier:[TMMessagesCollectionViewCellOutgoing mediaCellReuseIdentifier]];
    
}

-(instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if(self) {
        [self configureCollectionView];
    }
    return self;
}


@end

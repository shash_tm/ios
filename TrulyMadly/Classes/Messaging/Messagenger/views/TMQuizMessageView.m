//
//  TMQuizMessageView.m
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizMessageView.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+TMColorAdditions.h"
#import "TMLog.h"

#define ICONIMAGEVIEW_HEIGHTWIDTH  40

@interface TMQuizMessageView ()

@property(nonatomic,strong)UIImageView *iconImageView;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,assign)BOOL isOutgoingView;
@property(nonatomic,strong)UIView *flareIconBackgroundView;
@property(nonatomic,strong)UIImageView *flareIconImageView;


@end


@implementation TMQuizMessageView

-(instancetype)initWithFrame:(CGRect)frame outgoingMessageView:(BOOL)outgoingView {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor =[UIColor nudgeMessageViewColor];
        self.clipsToBounds = YES;
        self.isOutgoingView = outgoingView;
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.iconImageView.backgroundColor = [UIColor clearColor];
        self.iconImageView.clipsToBounds = TRUE;
        self.iconImageView.layer.borderWidth = .30;
        self.iconImageView.layer.borderColor = [UIColor blackColor].CGColor;
        self.iconImageView.layer.cornerRadius = ICONIMAGEVIEW_HEIGHTWIDTH/2;
        [self addSubview:self.iconImageView];
        
        self.infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.infoLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        self.infoLabel.backgroundColor = [UIColor clearColor];
        self.infoLabel.numberOfLines = 0;
        [self addSubview:self.infoLabel];
        
        //adding the flare background view
        self.flareIconBackgroundView = [[UIView alloc] initWithFrame:self.iconImageView.frame];
        self.flareIconBackgroundView.center = self.iconImageView.center;
        self.flareIconBackgroundView.layer.cornerRadius = self.flareIconBackgroundView.bounds.size.width/2;
        self.flareIconBackgroundView.clipsToBounds = YES;
        self.flareIconBackgroundView.backgroundColor = [UIColor blackColor];
        self.flareIconBackgroundView.alpha = 0.55;
        [self addSubview:self.flareIconBackgroundView];
        
        //adding the flare view
        self.flareIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.flareIconBackgroundView.bounds.size.width/2, self.flareIconBackgroundView.bounds.size.height/2)];
        self.flareIconImageView.center = self.flareIconBackgroundView.center;
        self.flareIconImageView.image = [UIImage imageNamed:@"quiz_flare"];
        self.flareIconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.flareIconImageView];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    //TMLOG(@"TMQuizMessageView----layoutSubviews---frame:%@",NSStringFromCGRect(self.frame));
    ///image view frame setting
    CGFloat iconHeightWidth = ICONIMAGEVIEW_HEIGHTWIDTH;
    CGFloat xPosStartingMargin = 5;
    CGFloat iconXPos = (self.isOutgoingView) ? xPosStartingMargin
                                             : (CGRectGetWidth(self.bounds) - (xPosStartingMargin + iconHeightWidth));
    CGFloat iconYPos = 5;
    self.iconImageView.frame = CGRectMake(iconXPos,
                                          iconYPos,
                                          iconHeightWidth,
                                          iconHeightWidth);
    
    ///lable frame setting
    CGFloat infoLabelYPos = (CGRectGetHeight(self.bounds) - self.textSize.height)/2;
    CGFloat infoLabelXPos = (self.isOutgoingView) ? CGRectGetMaxX(self.iconImageView.frame) + 5
    : 15;
    CGFloat infoLabelWidth = self.textSize.width;
    CGFloat infoLabelHeight = self.textSize.height;
    self.infoLabel.frame = CGRectMake(infoLabelXPos,
                                      infoLabelYPos,
                                      infoLabelWidth,
                                      infoLabelHeight);
    
    //adjusting the flare background view
    self.flareIconBackgroundView.frame = self.iconImageView.frame;
    self.flareIconBackgroundView.center = self.iconImageView.center;
    self.flareIconBackgroundView.layer.cornerRadius = self.flareIconBackgroundView.bounds.size.width/2;
    self.flareIconBackgroundView.clipsToBounds = YES;
    self.flareIconBackgroundView.backgroundColor = [UIColor blackColor];
    self.flareIconBackgroundView.alpha = 0.55;
    
    //adjusting the flare view
    self.flareIconImageView.frame = CGRectMake(0, 0, self.flareIconBackgroundView.bounds.size.width/2, self.flareIconBackgroundView.bounds.size.height/2);
    self.flareIconImageView.center = self.flareIconBackgroundView.center;
    self.flareIconImageView.image = [UIImage imageNamed:@"quiz_flare"];
    self.flareIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    
}

-(void)setTitleText:(NSString *)titleText {
    _titleText = titleText;
    self.infoLabel.text = titleText;
}

-(void)setBannerURLString:(NSString *)bannerURLString {
    [self.iconImageView setImageWithURL:[NSURL URLWithString:bannerURLString]];
}

- (void) setIsFlare:(BOOL) isFlare {
    self.flareIconBackgroundView.hidden = !isFlare;
    self.flareIconImageView.hidden = !isFlare;
}

- (void) setNudgeBackgroundColor:(UIColor*) color {
    self.backgroundColor = color;
}

-(UIFont*)textFont {
    return self.infoLabel.font;
}

@end

//
//  TMNudgeMessageView.m
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNudgeMessageView.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+TMColorAdditions.h"

@interface TMNudgeMessageView ()

@property(nonatomic,strong)UIImageView *iconImageView;
@property(nonatomic,strong)UIImageView *playIconImageView;
@property(nonatomic,strong)UIView *flareIconBackgroundView;
@property(nonatomic,strong)UIImageView *flareIconImageView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *actionLabel;

@end

@implementation TMNudgeMessageView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.clipsToBounds = YES;
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.height/3, self.bounds.size.height/3)];
        self.iconImageView.center = CGPointMake(self.bounds.size.width/2, 16 + self.iconImageView.bounds.size.height/2);
        self.iconImageView.clipsToBounds = YES;
        self.iconImageView.layer.cornerRadius = self.iconImageView.bounds.size.height/2;
        self.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.iconImageView.hidden = YES;
        [self addSubview:self.iconImageView];
        
        //adding the flare background view
        self.flareIconBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.height/3, self.bounds.size.height/3)];
        self.flareIconBackgroundView.center = CGPointMake(self.bounds.size.width/2, 16 + self.iconImageView.bounds.size.height/2);
        self.flareIconBackgroundView.layer.cornerRadius = self.flareIconBackgroundView.bounds.size.width/2;
        self.flareIconBackgroundView.clipsToBounds = YES;
        self.flareIconBackgroundView.backgroundColor = [UIColor blackColor];
        self.flareIconBackgroundView.alpha = 0.55;
        self.flareIconBackgroundView.hidden = YES;
        [self addSubview:self.flareIconBackgroundView];
        
        //adding the flare view
        self.flareIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.flareIconBackgroundView.bounds.size.width/2, self.flareIconBackgroundView.bounds.size.height/2)];
        self.flareIconImageView.center = self.flareIconBackgroundView.center;
        self.flareIconImageView.image = [UIImage imageNamed:@"quiz_flare"];
        self.flareIconImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.flareIconImageView.hidden = YES;
        [self addSubview:self.flareIconImageView];
        
        //adding the title label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width*7/10, self.bounds.size.height/3)];
        self.titleLabel.center = CGPointMake(self.bounds.size.width/2, self.iconImageView.frame.origin.y + self.iconImageView.bounds.size.height + self.bounds.size.height/8);
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        self.titleLabel.hidden = YES;
        [self addSubview:self.titleLabel];
        
        //adding the play/check view
        self.actionLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, self.frame.origin.y +  self.frame.size.height - 42, self.bounds.size.width - 24, 32)];
        self.actionLabel.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:111.0/255.0 blue:178.0/255.0 alpha:1.0];
        self.actionLabel.textColor = [UIColor whiteColor];
        self.actionLabel.textAlignment = NSTextAlignmentCenter;
        self.actionLabel.hidden = YES;
        self.actionLabel.layer.cornerRadius = self.actionLabel.bounds.size.height/2;
        self.actionLabel.clipsToBounds = YES;
        self.actionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.actionLabel.center = CGPointMake(self.bounds.size.width*48.5/100, self.actionLabel.center.y);
        [self addSubview:self.actionLabel];
        
        self.layer.borderColor = [UIColor grayColor].CGColor;
        self.layer.borderWidth = 1;
    }
    return self;
}


-(void)setIconImageURLString:(NSString*) iconImageURLString {
    self.iconImageView.hidden = NO;
    [self.iconImageView setImageWithURL:[NSURL URLWithString:iconImageURLString]];
}

-(void)setTitleText:(NSString*)titleText {
    self.titleLabel.hidden = NO;
    self.titleLabel.text = titleText;
}

-(void)setActionLabelText:(NSString*) actionText {
    self.actionLabel.hidden = NO;
    self.actionLabel.text = actionText;
}

- (void) setIsFlare:(BOOL) isFlare {
    self.flareIconBackgroundView.hidden = !isFlare;
    self.flareIconImageView.hidden = !isFlare;
}

- (void) setMessageSender:(TMMessageSender) sender {
    if (sender == MESSAGESENDER_ME) {
        self.actionLabel.center = CGPointMake(self.bounds.size.width*48.5/100, self.actionLabel.center.y);
    }
    else {
        self.actionLabel.center = CGPointMake(self.bounds.size.width*51.5/100, self.actionLabel.center.y);
    }
}

- (void) setNudgeBackgroundColor:(UIColor*) color {
    self.backgroundColor = color;
}
@end

//
//  TMQuizMessageView.h
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMQuizMessageView : UIView

@property(nonatomic,copy,readonly)NSString *titleText;

@property(nonatomic,assign)CGSize textSize;

-(instancetype)initWithFrame:(CGRect)frame outgoingMessageView:(BOOL)outgoingView;
-(void)setBannerURLString:(NSString *)bannerURLString;
-(void)setTitleText:(NSString *)titleText;
-(UIFont*)textFont;
- (void) setIsFlare:(BOOL) isFlare;

@end

//
//  TMShareEventView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMShareEventView : UIView

@property(nonatomic,assign)BOOL isOutGoingMessage;

-(instancetype)initWithFrame:(CGRect)frame isFailedMessage:(BOOL)failedMessage;
-(void)setBackgroundImageFromURL:(NSURL*)imageURL isDarkCentered:(BOOL) isDarkCentered;
-(void)setupUIForASKMessage;
-(void)setTitleText:(NSString *)titleText;

@end

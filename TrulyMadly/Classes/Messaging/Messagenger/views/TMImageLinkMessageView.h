//
//  TMImageLinkMessageView.h
//  TrulyMadly
//
//  Created by Ankit on 30/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMImageLinkMessageView : UIView

-(void)setupViewWithImageURL:(NSURL*)imageURL;

@end

//
//  TMMessagesCellTextView.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 28/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesCellTextView.h"

@implementation TMMessagesCellTextView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.editable = NO;
        self.selectable = YES;
        self.userInteractionEnabled = TRUE;
        self.dataDetectorTypes = UIDataDetectorTypeNone;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.scrollEnabled = NO;
        self.backgroundColor = [UIColor clearColor];
        self.contentInset = UIEdgeInsetsZero;
        self.scrollIndicatorInsets = UIEdgeInsetsZero;
        self.contentOffset = CGPointZero;
        self.textContainerInset = UIEdgeInsetsZero;
        self.textContainer.lineFragmentPadding = 0;
        self.linkTextAttributes = @{ NSForegroundColorAttributeName : [UIColor blackColor],
                                     NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
        self.textColor = [UIColor blackColor];
    }
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
}
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    //  ignore double-tap to prevent copy/define/etc. menu from showing
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        UITapGestureRecognizer *tap = (UITapGestureRecognizer *)gestureRecognizer;
        if (tap.numberOfTapsRequired == 2) {
            return NO;
        }
    }
    return YES;
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    //  ignore double-tap to prevent copy/define/etc. menu from showing
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        UITapGestureRecognizer *tap = (UITapGestureRecognizer *)gestureRecognizer;
        if (tap.numberOfTapsRequired == 2) {
            return NO;
        }
    }
    return YES;
}
- (BOOL)canBecomeFirstResponder {
    return NO;
}

@end

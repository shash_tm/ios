//
//  TMMessageCollectionViewHeader.h
//  TrulyMadly
//
//  Created by Ankit on 08/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMMessageCollectionViewHeaderDelegate;
@class TMChatUser;

@interface TMMessageCollectionViewHeader : UICollectionReusableView

@property(nonatomic,weak)id<TMMessageCollectionViewHeaderDelegate> delegate;
@property(nonatomic,strong)NSString *sparkStartTime;

-(void)updatUI:(TMChatUser*)sparkUser sparkRemainingTime:(NSString*)sparkRemainingTime;

@end

@protocol TMMessageCollectionViewHeaderDelegate <NSObject>

-(void)deleteSparkAction;
-(void)didTapSparkUserProfile;

@end

//
//  TMStickerImageView.h
//  TrulyMadly
//
//  Created by Ankit on 09/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSticker.h"

@interface TMStickerImageView : UIView

//@property(nonatomic,strong)NSString *imageURL;
@property(nonatomic,strong) TMSticker* sticker;
@end

//
//  TMDealMessageView.m
//  TrulyMadly
//
//  Created by Ankit on 05/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMDealMessageView.h"
#import "UIImageView+AFNetworking.h"
#import "TMLikeVIew.h"
#import "TMProfileInterest.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"


@interface TMDealMessageView ()

@property(nonatomic,strong)TMProfileImageView *profileImgView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *subtitleLabel;
@property(nonatomic,strong)UILabel *actionLabel;
@property(nonatomic,assign)BOOL isFailedMessage;

@end


@implementation TMDealMessageView

-(instancetype)initWithFrame:(CGRect)frame isFailedMessage:(BOOL)failedMessage {
    self = [super initWithFrame:frame];
    if(self) {
        self.isFailedMessage = failedMessage;
        self.profileImgView = [[TMProfileImageView alloc] initWithFrame:self.bounds];
        self.profileImgView.clipsToBounds = true;
        [self.profileImgView setImageContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.profileImgView];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    self.profileImgView.frame = self.bounds;
    [self setTitleText:self.titleLabel.text subTitleText:self.subtitleLabel.text];
}

-(void)setBackgroundImageFromURL:(NSURL*)imageURL isDarkCentered:(BOOL) isDarkCentered {

    if(imageURL && [imageURL isKindOfClass:[NSURL class]]) {
        NSString *lastPathComponent = [imageURL lastPathComponent];
        [self.profileImgView setDefaultImage:[UIImage imageNamed:@"deal_image_placeholder"]];
        [self.profileImgView setImageFromURLStringWithBackgroundGradient:[imageURL absoluteString] isDarkCentered:isDarkCentered];
  [self.profileImgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    }
}
-(void)setTitleText:(NSString *)titleText subTitleText:(NSString*)subTitle {
    if(subTitle) { //voucher
        self.titleLabel.text = titleText;
        self.subtitleLabel.text = subTitle;
        
        CGFloat initXPos = 30;
        CGFloat widthOffset = initXPos + 30;
        CGFloat maxWidth = CGRectGetWidth(self.bounds)- widthOffset;
        NSInteger maxNumberOfLines = (self.isFailedMessage) ? 3 : 5;
        CGFloat heightPerLine = (self.isFailedMessage) ? 16 : 20;
        CGFloat  yPosOffsetBetweenTitleAndActionLabel = (self.isFailedMessage) ? 10 : 20;
        CGSize actionLabelSize = CGSizeMake(130, 32);
        CGFloat yPosOffset = (self.isFailedMessage) ? 30 : 40;
        CGFloat maxHeight = heightPerLine * maxNumberOfLines;
        NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
        CGSize constrintSize = CGSizeMake(maxWidth, maxHeight);
        CGRect rect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        CGFloat totalContentHeight =  (rect.size.height + yPosOffsetBetweenTitleAndActionLabel + actionLabelSize.height);
        CGFloat titleLabelYPos = ((CGRectGetHeight(self.bounds)-yPosOffset) - totalContentHeight)/2;
        self.titleLabel.frame = CGRectMake(initXPos, titleLabelYPos, maxWidth, rect.size.height);
        
        //////
        self.subtitleLabel.frame = CGRectMake(initXPos+20, CGRectGetMaxY(self.titleLabel.frame)+8, maxWidth-40, 20);
        CGFloat actionLabelYPos = CGRectGetMaxY(self.subtitleLabel.frame) + yPosOffsetBetweenTitleAndActionLabel;
        self.actionLabel.frame = CGRectMake((CGRectGetWidth(self.bounds) - actionLabelSize.width)/2,
                                            actionLabelYPos,
                                            actionLabelSize.width,
                                            actionLabelSize.height);
        self.actionLabel.layer.cornerRadius = self.actionLabel.bounds.size.height/2;
    }
    else { //ask message ui
        self.titleLabel.text = titleText;
        
        CGFloat initXPos = (self.isOutGoingMessage) ? 10 : 15;
        CGFloat widthOffset = (self.isOutGoingMessage) ? 25 : 23;
        CGFloat maxWidth = CGRectGetWidth(self.bounds)- widthOffset;
        NSInteger maxNumberOfLines = 5;
        CGFloat heightPerLine = 20;
        CGFloat yPosOffsetBetweenTitleAndActionLabel = 20;
        CGSize actionLabelSize = CGSizeMake(120, 32);
        
        CGFloat maxHeight = heightPerLine * maxNumberOfLines;
        NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
        CGSize constrintSize = CGSizeMake(maxWidth, maxHeight);
        CGRect rect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        CGFloat totalContentHeight =  (rect.size.height + yPosOffsetBetweenTitleAndActionLabel + actionLabelSize.height);
        CGFloat titleLabelYPos = (CGRectGetHeight(self.bounds) - totalContentHeight)/2;
        CGFloat actionLabelYPos = CGRectGetMaxY(self.titleLabel.frame) + yPosOffsetBetweenTitleAndActionLabel;
        
        self.titleLabel.frame = CGRectMake(initXPos, titleLabelYPos, maxWidth, rect.size.height);
        self.actionLabel.frame = CGRectMake((CGRectGetWidth(self.bounds) - actionLabelSize.width)/2,
                                            actionLabelYPos,
                                            actionLabelSize.width,
                                            actionLabelSize.height);
        self.actionLabel.layer.cornerRadius = self.actionLabel.bounds.size.height/2;
    }
}
-(void)setTitleText:(NSString *)titleText {
    
}

-(void)setupUIForASKMessage {
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self addSubview:self.titleLabel];
    
    self.actionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.actionLabel.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:111.0/255.0 blue:178.0/255.0 alpha:1.0];
    self.actionLabel.textColor = [UIColor whiteColor];
    self.actionLabel.textAlignment = NSTextAlignmentCenter;
    self.actionLabel.clipsToBounds = YES;
    self.actionLabel.text = @"Check It Out";//@"Tell Me More";
    [self addSubview:self.actionLabel];
}
-(void)setupUIForVoucherKMessage {
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20,
                                                                         20,
                                                                         CGRectGetMaxX(self.bounds)-40,
                                                                         CGRectGetMaxY(self.bounds)-40)];
    imgView.image = [UIImage imageNamed:@"voucheborder"];
    [self addSubview:imgView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = (self.isFailedMessage) ? [UIFont boldSystemFontOfSize:16] : [UIFont boldSystemFontOfSize:18];
    [self addSubview:self.titleLabel];
    
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.subtitleLabel.backgroundColor = [UIColor clearColor];
    self.subtitleLabel.textColor = [UIColor whiteColor];
    self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
    self.subtitleLabel.numberOfLines = 0;
    self.subtitleLabel.font = (self.isFailedMessage) ? [UIFont systemFontOfSize:12] : [UIFont systemFontOfSize:14];
    [self addSubview:self.subtitleLabel];
    
    self.actionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.actionLabel.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:111.0/255.0 blue:178.0/255.0 alpha:1.0];
    self.actionLabel.textColor = [UIColor whiteColor];
    self.actionLabel.textAlignment = NSTextAlignmentCenter;
    self.actionLabel.layer.cornerRadius = self.actionLabel.bounds.size.height/2;
    self.actionLabel.clipsToBounds = YES;
    self.actionLabel.text = @"View Voucher";
    [self addSubview:self.actionLabel];
}

@end

//
//  TMDealMessageView.h
//  TrulyMadly
//
//  Created by Ankit on 05/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMDealMessageView : UIView

@property(nonatomic,assign)BOOL isOutGoingMessage;

-(instancetype)initWithFrame:(CGRect)frame isFailedMessage:(BOOL)failedMessage;
-(void)setBackgroundImageFromURL:(NSURL*)imageURL isDarkCentered:(BOOL) isDarkCentered;
-(void)setTitleText:(NSString *)titleText;
-(void)setTitleText:(NSString *)titleText subTitleText:(NSString*)subTitle;
-(void)setupUIForASKMessage;
-(void)setupUIForVoucherKMessage;


@end

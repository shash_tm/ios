//
//  TMMessageCollectionViewHeader.m
//  TrulyMadly
//
//  Created by Ankit on 08/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMessageCollectionViewHeader.h"
#import "UIColor+TMColorAdditions.h"
#import "TMChatUser.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"
#import "TMNotificationHeaders.h"
#import "TMTimestampFormatter.h"
#import "TMUserSession.h"


@interface TMSparkDataContainerView : UIView

-(void)setProfileImages:(NSArray*)images;
-(void)setName:(NSString*)name age:(NSInteger)age location:(NSString*)location;
-(void)setDesignation:(NSString*)designation;

@end

@implementation TMSparkDataContainerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor colorWithRed:66/255.0f green:199/255.0f blue:233/255.0f alpha:1.0].CGColor;
        self.layer.borderWidth = 1.5f;
    }
    return self;
}

-(void)setProfileImages:(NSArray*)images {

    CGFloat width = 120;
    CGFloat xPos = (CGRectGetWidth(self.frame)-120)/2;
    if(images.count == 3) {
        CGFloat xOffset = ((120*3) - (2*40));
        xPos = (CGRectGetWidth(self.frame) - xOffset)/2;
    }
    else if(images.count == 2) {
        CGFloat xOffset = ((120*2) - (1*40));
        xPos = (CGRectGetWidth(self.frame) - xOffset)/2;
    }
    
    int i=0;
    for(NSString *imageURLString in images) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, 30, width, width)];
        imageView.backgroundColor = [UIColor grayColor];
        imageView.layer.cornerRadius = width/2;
        imageView.clipsToBounds = TRUE;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [imageView setImageWithURL:[NSURL URLWithString:imageURLString]];
        if((images.count == 3) && (i==1)) {
            imageView.layer.zPosition = 10001;
        }
        else {
            imageView.layer.zPosition = 1000;
        }
        imageView.tag = 60001+i;
        [self addSubview:imageView];
        
        xPos = CGRectGetMaxX(imageView.frame) - 40;
        i++;
    }
}

-(void)setName:(NSString*)name age:(NSInteger)age location:(NSString*)location {
    NSDictionary *nameAttributes= @{NSForegroundColorAttributeName:[UIColor messageCollectionViewHeaderTitleColor],
                                                                    NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:18]};
    
    NSAttributedString *nameAttString = [[NSAttributedString alloc] initWithString:name attributes:nameAttributes];
    
    NSDictionary *seperatorAttributes = @{NSForegroundColorAttributeName:[UIColor redColor],
                                                                        NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:18]};
    
    NSAttributedString *seperatorAttString1 = [[NSAttributedString alloc] initWithString:@"  |  " attributes:seperatorAttributes];
    
    
    NSAttributedString *ageAttString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld years",(long)age] attributes:nameAttributes];
    
    NSAttributedString *seperatorAttString2 = [[NSAttributedString alloc] initWithString:@"  |  " attributes:seperatorAttributes];
    
    NSAttributedString *locationAttString = [[NSAttributedString alloc] initWithString:location attributes:nameAttributes];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nameAttString];
    [attributedString appendAttributedString:seperatorAttString1];
    [attributedString appendAttributedString:ageAttString];
    [attributedString appendAttributedString:seperatorAttString2];
    [attributedString appendAttributedString:locationAttString];
    
    UIImageView *imageView = [self viewWithTag:60001];
    
    CGFloat yPos = CGRectGetMaxY(imageView.frame) + 20;
    CGFloat xPos = 20;//CGRectGetMinX(imageView.frame);
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, CGRectGetWidth(self.frame)-2*xPos, 30)];
    nameLabel.attributedText = attributedString;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.tag = 70001;
    [self addSubview:nameLabel];

}
-(void)setDesignation:(NSString*)designation {
    UILabel *label = (UILabel*)[self viewWithTag:70001];
    
    CGFloat xPos = CGRectGetMinX(label.frame);
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, CGRectGetMaxY(label.frame), CGRectGetWidth(self.frame)-2*xPos, 30)];
    nameLabel.text = designation;
    nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.textColor = [UIColor messageCollectionViewHeaderSubTitleColor];
    [self addSubview:nameLabel];
}

@end

@interface TMSparkDeleteContainerView : UIView

@property(nonatomic,strong)UIButton *deleteButton;

@end

@implementation TMSparkDeleteContainerView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor colorWithRed:66/255.0f green:199/255.0f blue:233/255.0f alpha:1.0].CGColor;
        self.layer.borderWidth = 1.5f;
        
        CGFloat buttonyPos = ((CGRectGetHeight(self.frame) - (36+20+10)))/2;
        self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.deleteButton.frame = CGRectMake(50, buttonyPos, 41, 46);
        [self.deleteButton setImage:[UIImage imageNamed:@"sparkdelete"] forState:UIControlStateNormal];
        [self addSubview:self.deleteButton];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(45, CGRectGetMaxY(self.deleteButton.frame), 100, 20)];
        label.text = @"Delete";
        label.font = [UIFont systemFontOfSize:16];
        label.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:197/255.0f alpha:1.0];
        [self addSubview:label];
    }
    return self;
}

@end


@interface TMMessageCollectionViewHeader ()

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)TMSparkDataContainerView *sparkDataContainview;
@property(nonatomic,strong)TMSparkDeleteContainerView *deleteSparkContianer;

@property(nonatomic,assign)BOOL isUIUPdated;

@end

@implementation TMMessageCollectionViewHeader

+ (NSString *)cellReuseIdentifier
{
    return NSStringFromClass([self class]);
}
+ (NSString *)mediaCellReuseIdentifier
{
    return [NSString stringWithFormat:@"%@_TMMedia", NSStringFromClass([self class])];
}
+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:[NSBundle bundleForClass:[self class]]];
}
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sparkProgressTimer:) name:TMSPARKTIMER_NOTIFICATION object:nil];
        
        self.backgroundColor = [UIColor clearColor];
        self.isUIUPdated = FALSE;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, CGRectGetWidth(self.frame)-20, 40)];
        //self.titleLabel.text = @"Reply before his Spark vanishes.";
        self.titleLabel.textColor = [UIColor sparkTileInConversatinHeaderColor];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        [self addSubview:self.titleLabel];
        
        CGFloat yOffsetForTitle = CGRectGetMaxY(self.titleLabel.frame) + 20;
        CGFloat scrollViewHeigth = CGRectGetHeight(self.frame)-yOffsetForTitle;
        CGFloat scrollViewWidth = CGRectGetWidth(self.frame);
        
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                         yOffsetForTitle,
                                                                         scrollViewWidth,
                                                                         scrollViewHeigth)];
        self.scrollView.pagingEnabled = TRUE;
        self.scrollView.showsHorizontalScrollIndicator = FALSE;
        self.scrollView.backgroundColor = [UIColor clearColor];
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.scrollView.frame)+130, CGRectGetHeight(self.scrollView.frame));
        [self addSubview:self.scrollView];
        
        //add spark data view
        CGFloat xPos = 20;
        self.sparkDataContainview = [[TMSparkDataContainerView alloc] initWithFrame:CGRectMake(xPos,
                                                                                               0,
                                                                                               CGRectGetWidth(self.scrollView.frame)-2*20,
                                                                                               CGRectGetHeight(self.scrollView.frame))];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self.sparkDataContainview addGestureRecognizer:tapGesture];
        [self.scrollView addSubview:self.sparkDataContainview];
        
        //add spark delete view
        self.deleteSparkContianer = [[TMSparkDeleteContainerView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.scrollView.frame),
                                                                                                 0,
                                                                                                 300,
                                                                                                 CGRectGetHeight(self.scrollView.frame))];
        [self.deleteSparkContianer.deleteButton addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:self.deleteSparkContianer];
    }
    return self;
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)updatUI:(TMChatUser*)sparkUser sparkRemainingTime:(NSString*)sparkRemainingTime {
    if(!self.isUIUPdated) {
        self.isUIUPdated = TRUE;
        [self updateTitleLable:sparkRemainingTime];
        [self.sparkDataContainview setProfileImages:[sparkUser getCombinedProfileImages]];
        [self.sparkDataContainview setName:sparkUser.fName age:sparkUser.age location:sparkUser.city];
        [self.sparkDataContainview setDesignation:sparkUser.designation];
        
        if([[TMUserSession sharedInstance] canShowSlideAnimationOnSparkChat]) {
            [self showSparkAnimation];
            [[TMUserSession sharedInstance] updateSlideAnimationOnSparkChat];
        }
    }
}

-(void)deleteAction {
    [self.delegate deleteSparkAction];
}

-(void)tapAction {
    [self.delegate didTapSparkUserProfile];
}

-(void)sparkProgressTimer:(NSNotification*)notification {
    NSString *remainingTime = notification.object;
    //NSString *remainingTime = userInfo[@"sparkexpmins"];
    [self updateTitleLable:remainingTime];
}

-(void)updateTitleLable:(NSString*)remainingTime {
    self.titleLabel.text = [NSString stringWithFormat:@"Your Spark expires in %@. Reply Now!",remainingTime];
}

-(void)showSparkAnimation {
    [UIScrollView beginAnimations:@"scrollAnimation1" context:nil];
    [UIScrollView setAnimationDuration:1.0f];
    [UIScrollView setAnimationDelay:0.55];
    [self.scrollView setContentOffset:CGPointMake(100, 0)];
    [UIScrollView commitAnimations];
    
    [UIScrollView beginAnimations:@"scrollAnimation2" context:nil];
    [UIScrollView setAnimationDuration:1.0f];
    [UIScrollView setAnimationDelay:2];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
    [UIScrollView commitAnimations];
}

@end

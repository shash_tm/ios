//
//  TMMessagesCollectionViewCellOutgoing.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesCollectionViewCellOutgoing.h"

@implementation TMMessagesCollectionViewCellOutgoing

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.cellBottomLabel.textAlignment = NSTextAlignmentRight;
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat heightOffset = (CGRectGetHeight(self.cellTopLabel.bounds) + CGRectGetHeight(self.cellBottomLabel.bounds));
    
    CGFloat height = CGRectGetHeight(self.contentView.bounds) - heightOffset;
    CGFloat xPos = CGRectGetWidth(self.contentView.bounds) - self.bubbleContainerWidth - self.sentFailedIndicatorViewSize.width;
    self.messageBubbleContainerView.frame = CGRectMake(xPos,
                                                       CGRectGetMaxY(self.cellTopLabel.bounds),
                                                       self.bubbleContainerWidth,
                                                       height);
    self.messageBubbleImageView.frame = self.messageBubbleContainerView.bounds;
    self.messageTextView.frame = CGRectMake(6,
                                            CGRectGetMinY(self.messageBubbleContainerView.bounds),
                                            CGRectGetWidth(self.messageBubbleContainerView.bounds)-6,
                                            CGRectGetHeight(self.messageBubbleContainerView.bounds));
    
    //////
    self.sentFailedIndicatorContainerView.frame = CGRectMake(CGRectGetMaxX(self.contentView.bounds)-self.sentFailedIndicatorViewSize.width,
                ((CGRectGetHeight(self.messageBubbleContainerView.bounds) - self.sentFailedIndicatorViewSize.height)/2)+CGRectGetHeight(self.cellTopLabel.bounds),
                                                             self.sentFailedIndicatorViewSize.width,
                                                             self.sentFailedIndicatorViewSize.height);
    
    if(CGSizeEqualToSize(self.sentFailedIndicatorViewSize, CGSizeMake(44, 44))) {
        self.sentFailedIndicatorImageView.image = [UIImage imageNamed:@"NotSent"];
        CGFloat width = 17;
        self.sentFailedIndicatorImageView.frame = CGRectMake((44-width)/2,
                                                             (44-width)/2,
                                                             width,
                                                             width);
        if(!self.sentFailedIndicatorImageView.superview) {
            [self.sentFailedIndicatorContainerView addSubview:self.sentFailedIndicatorImageView];
        }
    }
    else {
        self.sentFailedIndicatorImageView.image = nil;
        self.sentFailedIndicatorImageView.frame = CGRectZero;
        [self.sentFailedIndicatorImageView removeFromSuperview];
    }
}


@end

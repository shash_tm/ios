//
//  TMMessageCollectionViewFooter.m
//  TrulyMadly
//
//  Created by Ankit on 10/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMessageCollectionViewFooter.h"
#import "TMMessagesBubbleImageFactory.h"
#import "UIColor+TMColorAdditions.h"


@interface TMMessageCollectionViewFooter ()

@end


@implementation TMMessageCollectionViewFooter

+ (NSString *)cellReuseIdentifier
{
    return NSStringFromClass([self class]);
}
+ (NSString *)mediaCellReuseIdentifier
{
    return [NSString stringWithFormat:@"%@_TMMedia", NSStringFromClass([self class])];
}
+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:[NSBundle bundleForClass:[self class]]];
}
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        UIView* baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        baseView.backgroundColor = [UIColor clearColor];
        [self addSubview:baseView];
        
        //add the image view
        TMMessagesBubbleImageFactory *bubbleFactory = [[TMMessagesBubbleImageFactory alloc] init];
        TMMessagesBubbleImage *incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor likeunSelectedColor]];
        UIImageView* bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 65, frame.size.height - 20)];
        bubbleImageView.image = incomingBubbleImageData.messageBubbleImage;
        bubbleImageView.contentMode = UIViewContentModeScaleToFill;
        [baseView addSubview:bubbleImageView];
        
        //add the image view for the dot bubbles
        UIImageView* dotImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 6)];
        dotImageView.center = CGPointMake(bubbleImageView.bounds.size.width*52.5/100, bubbleImageView.bounds.size.height/2);
        dotImageView.image = [UIImage imageNamed:@"typing_indicator"];
        dotImageView.contentMode = UIViewContentModeScaleAspectFit;
        dotImageView.backgroundColor = [UIColor clearColor];
        [bubbleImageView addSubview:dotImageView];
    }
    return self;
}

@end

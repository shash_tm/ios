//
//  TMMessagesCollectionViewCell.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesCollectionViewCell.h"
#import "TMMessagesCellTextView.h"
#import "TMMessagesCollectionViewLayoutAttributes.h"
#import "TMMessageContainerView.h"
#import "TMCommonUtil.h"


@interface TMMessagesCollectionViewCell ()

@property(strong, nonatomic) UILabel *cellTopLabel;
@property(strong, nonatomic) UILabel *cellBottomLabel;
@property(strong, nonatomic) TMMessageContainerView *messageBubbleContainerView;
@property(strong, nonatomic) TMMessagesCellTextView *messageTextView;
@property(strong, nonatomic) UIImageView *messageBubbleImageView;
@property(strong, nonatomic) UIImageView *sentFailedIndicatorImageView;
@property(strong, nonatomic) UIView *sentFailedIndicatorContainerView;
@property(assign, nonatomic) UIEdgeInsets textViewFrameInsets;
@property(assign, nonatomic) NSInteger selectedIndex;
@property(weak, nonatomic, readwrite) UITapGestureRecognizer *tapGestureRecognizer;
@property(weak, nonatomic, readwrite) UITapGestureRecognizer *sentFailedViewTapGestureRecognizer;
@property(weak, nonatomic, readwrite) UITapGestureRecognizer *mediaViewTapGestureRecognizer;

@end


@implementation TMMessagesCollectionViewCell

+ (NSString *)cellReuseIdentifier
{
    return NSStringFromClass([self class]);
}
+ (NSString *)mediaCellReuseIdentifier
{
    return [NSString stringWithFormat:@"%@_TMMedia", NSStringFromClass([self class])];
}
+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:[NSBundle bundleForClass:[self class]]];
}
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self createUI];
        [self configureCell];
    }
    return self;
}

-(void)createUI {
    
    self.cellTopLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.cellTopLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.cellTopLabel];
    
    self.messageBubbleContainerView = [[TMMessageContainerView alloc] initWithFrame:CGRectZero];
    self.messageBubbleContainerView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.messageBubbleContainerView];
    
    self.messageBubbleImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.messageBubbleImageView.backgroundColor = [UIColor clearColor];
    [self.messageBubbleContainerView addSubview:self.messageBubbleImageView];
    
    self.messageTextView = [[TMMessagesCellTextView alloc] initWithFrame:CGRectZero];
    self.messageTextView.backgroundColor = [UIColor clearColor];
    [self.messageBubbleContainerView addSubview:self.messageTextView];
    
    self.sentFailedIndicatorContainerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.sentFailedIndicatorContainerView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.sentFailedIndicatorContainerView];
    
    self.cellBottomLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.cellBottomLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.cellBottomLabel];
    
    self.sentFailedIndicatorContainerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.sentFailedIndicatorContainerView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.sentFailedIndicatorContainerView];
    
    self.sentFailedIndicatorImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.sentFailedIndicatorImageView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSentMessageFailedViewTapAction:)];
    [self.sentFailedIndicatorContainerView addGestureRecognizer:tap];
    self.sentFailedViewTapGestureRecognizer = tap;
}

-(void)configureCell {
    self.sentFailedIndicatorViewSize = CGSizeZero;
    
    self.cellTopLabel.textAlignment = NSTextAlignmentCenter;
    self.cellTopLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    self.cellTopLabel.textColor = [UIColor lightGrayColor];
    
    self.cellBottomLabel.font = [UIFont systemFontOfSize:11.0f];
    self.cellBottomLabel.textColor = [UIColor grayColor];
//
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self addGestureRecognizer:tap];
    self.tapGestureRecognizer = tap;

}

- (void)dealloc
{
    _delegate = nil;
    
    _cellTopLabel = nil;
    _cellBottomLabel = nil;
    
    _messageTextView = nil;
    _messageBubbleImageView = nil;
    _mediaView = nil;
    
    _sentFailedIndicatorContainerView = nil;
    
    [_tapGestureRecognizer removeTarget:nil action:NULL];
    _tapGestureRecognizer = nil;
    
    [_mediaViewTapGestureRecognizer removeTarget:nil action:NULL];
    _mediaViewTapGestureRecognizer = nil;
}

#pragma mark - Collection view cell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.cellTopLabel.text = nil;
    self.cellBottomLabel.text = nil;
    self.messageTextView.dataDetectorTypes = UIDataDetectorTypeNone;
    self.messageTextView.text = nil;
    self.messageTextView.attributedText = nil;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    return layoutAttributes;
}
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    
    TMMessagesCollectionViewLayoutAttributes *customAttributes = (TMMessagesCollectionViewLayoutAttributes *)layoutAttributes;
    if (self.messageTextView.font != customAttributes.messageBubbleFont) {
        self.messageTextView.font = customAttributes.messageBubbleFont;
    }
    
    if (!UIEdgeInsetsEqualToEdgeInsets(self.messageTextView.textContainerInset, customAttributes.textViewTextContainerInsets)) {
        self.messageTextView.textContainerInset = customAttributes.textViewTextContainerInsets;
    }
    
    self.textViewFrameInsets = customAttributes.textViewFrameInsets;
    
    self.topLabelHeight = customAttributes.cellTopLabelHeight;
    self.bottomLabelHeight = customAttributes.cellBottomLabelHeight;
    self.bubbleContainerWidth = customAttributes.messageBubbleContainerViewWidth;
    
    self.selectedIndex = customAttributes.indexPath.row;
    
    self.sentFailedIndicatorViewSize = customAttributes.sentFailedIndicatorViewSize;
    [self layoutSubviews];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    if([TMCommonUtil isCurrentDeviceBeforeiOS8]) {
        CGRect contentViewFrame = self.contentView.frame;
        contentViewFrame.size = self.frame.size;
        self.contentView.frame = contentViewFrame;
    }
    
    self.cellTopLabel.frame = CGRectMake(CGRectGetMinX(self.contentView.bounds)+10,
                                         CGRectGetMinY(self.contentView.bounds),
                                         CGRectGetWidth(self.contentView.bounds)-20,
                                         self.topLabelHeight);
    CGFloat yPos = CGRectGetHeight(self.contentView.bounds) - self.bottomLabelHeight;
    self.cellBottomLabel.frame = CGRectMake(CGRectGetMinX(self.contentView.bounds)+10,
                                            yPos,
                                            CGRectGetWidth(self.contentView.bounds)-20,
                                            self.bottomLabelHeight);
}
- (void)setTextViewFrameInsets:(UIEdgeInsets)textViewFrameInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(textViewFrameInsets, self.textViewFrameInsets)) {
        return;
    }
}

- (void)setMediaView:(UIView *)mediaView
{
    [self.messageBubbleImageView removeFromSuperview];
    [self.messageTextView removeFromSuperview];
    [self.messageBubbleContainerView addSubview:mediaView];
    
    _mediaView = mediaView;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMediaTapGesture:)];
    [_mediaView addGestureRecognizer:tap];
    self.mediaViewTapGestureRecognizer = tap;
    
    //  because of cell re-use (and caching media views, if using built-in library media item)
    //  we may have dequeued a cell with a media view and add this one on top
    //  thus, remove any additional subviews hidden behind the new media view
    dispatch_async(dispatch_get_main_queue(), ^{
        for (NSUInteger i = 0; i < self.messageBubbleContainerView.subviews.count; i++) {
            if (self.messageBubbleContainerView.subviews[i] != _mediaView) {
                
                [self.messageBubbleContainerView.subviews[i] removeFromSuperview];
            }
        }
    });
}
-(void)handleTapGesture:(UITapGestureRecognizer*)tapGesture {
    [self.delegate didTapCellAtIndex:self.selectedIndex];
}
-(void)handleMediaTapGesture:(UITapGestureRecognizer*)tapGesture {
    [self.delegate didSelectMediaCell:self.selectedIndex];
}
-(void)handleSentMessageFailedViewTapAction:(UITapGestureRecognizer*)longPressGesture {
    [self.delegate didSelectSentFailedMessageCellAtIndex:self.selectedIndex];
}

@end

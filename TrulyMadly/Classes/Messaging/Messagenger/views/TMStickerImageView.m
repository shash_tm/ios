//
//  TMStickerImageView.m
//  TrulyMadly
//
//  Created by Ankit on 09/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerImageView.h"
#import "UIImageView+AFNetworking.h"
#import "TMStickerManager.h"
#import "TMStickerFileManager.h"

@interface TMStickerImageView ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)NSString* currentStickerURL;

@end

@implementation TMStickerImageView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        //NSLog(@"TMStickerImageView---initWithFrame");
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.imageView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloaded:) name:@"imageDownloaded" object:nil];
    }
    return self;
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)layoutSubviews {
    //NSLog(@"TMStickerImageView---layoutSubviews");
    self.imageView.frame = self.bounds;
    if(self.imageView.image) {
        //NSLog(@"TMStickerImageView---layoutSubviews---%@",self.imageURL);
    }
    else {
        //NSLog(@"TMStickerImageView---layoutSubviews---NO Image");
    }
}
- (void) setSticker:(TMSticker *)sticker {
    _sticker = sticker;
    self.currentStickerURL = self.sticker.highQualityImageURLString;
    [self loadImage];
}
- (void) loadImage {
    
    TMStickerFileManager* stickerFileManager = [[TMStickerFileManager alloc] init];
    
    TMStickerManager* stickerManager = [TMStickerManager sharedStickerManager];
    
    if (self.sticker) {
        if (self.sticker.hdFilePathURL && [stickerFileManager isFilePresentAtPath:self.sticker.hdFilePathURL]) {
            //if hd image is stored in the directory
            [self.imageView setImage:[stickerManager getHDImageForSticker:self.sticker]];
        }
        else {
            
            //thumbnail followed by hd images
            
            if (self.sticker.highQualityImageURLString) {
                UIImage* fileImage =  [stickerManager getHDImageForSticker:self.sticker];
                if (fileImage) {
                    self.imageView.image = fileImage;
                }
                else {
                    self.imageView.image = nil;
                }
            }
        }
    }
    else {
        //do nothing
    }
}

- (void) imageDownloaded:(NSNotification*) notification {
    NSString* downloadedURL = ([notification.object isKindOfClass:[NSString class]])?notification.object:nil;
    if ([downloadedURL isEqualToString:self.currentStickerURL]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = [[TMStickerManager sharedStickerManager] getImageForSticker:self.sticker];
        });
    }
}

@end

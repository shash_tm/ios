//
//  TMNudgeMessageView.h
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@interface TMNudgeMessageView : UIView

-(void)setIconImageURLString:(NSString*)iconImageURLString ;
-(void)setTitleText:(NSString *)titleText;
-(void)setActionLabelText:(NSString*)actionText;
- (void) setIsFlare:(BOOL) isFlare;
- (void) setMessageSender:(TMMessageSender) sender;

@end

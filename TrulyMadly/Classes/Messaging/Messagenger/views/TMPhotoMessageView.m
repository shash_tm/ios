//
//  TMPhotoMessageView.m
//  TrulyMadly
//
//  Created by Ankit on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMPhotoMessageView.h"
#import "UIImageView+AFNetworking.h"
#import "TMPhotoShareContext.h"
#import "XLCircleProgressIndicator.h"
#import "TMAnalytics.h"

static void * TMTaskCountOfBytesSentContext = &TMTaskCountOfBytesSentContext;
static void * TMTaskCountOfBytesReceivedContext = &TMTaskCountOfBytesReceivedContext;


@interface TMPhotoMessageView ()

@property(nonatomic,strong)TMPhotoShareContext *photoshareContext;
@property(nonatomic,strong)NSString *fullImageURL;
@property(nonatomic,strong)NSString *thumbnailImageURL;

@property(nonatomic,strong)UIImageView *imgView;
@property(nonatomic,strong)UIImageView *downloadImageView;
@property(nonatomic,strong)UIView *bgView;
//@property(nonatomic,strong)UIButton *downloadButton;
@property(nonatomic,strong)UILabel *sizeLabel;

@property(nonatomic,strong)XLCircleProgressIndicator *uploadProgressIndicater;
//
@end


@implementation TMPhotoMessageView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor lightGrayColor];
        self.imgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.imgView.clipsToBounds = true;
        [self.imgView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.imgView];
    }
    return self;
}

-(void)dealloc {
    [self resetViewForPhotoUploadProgress];
}


-(XLCircleProgressIndicator *)uploadProgressIndicater
{
    if(!_uploadProgressIndicater) {
        _uploadProgressIndicater = [[XLCircleProgressIndicator alloc] initWithFrame:CGRectMake(0,
                                                                                               0,
                                                                                               CGRectGetWidth(self.bounds),
                                                                                               CGRectGetHeight(self.bounds))
                                                                   showCancelButton:TRUE];
        _uploadProgressIndicater.center = CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2);
        _uploadProgressIndicater.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin |
                                                        UIViewAutoresizingFlexibleLeftMargin |
                                                        UIViewAutoresizingFlexibleRightMargin |
                                                        UIViewAutoresizingFlexibleTopMargin;
        [_uploadProgressIndicater setProgressValue:0.0f];
    }
    return _uploadProgressIndicater;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    self.imgView.frame = self.bounds;
    self.bgView.frame = CGRectMake((CGRectGetWidth(self.bounds)-100)/2,
                                   (CGRectGetMaxY(self.frame)-44)/2,
                                   100,
                                   44);
    //self.downloadButton.frame =
    CGRect frame = CGRectMake(CGRectGetMinX(self.bgView.frame)+5,
                              (CGRectGetMaxY(self.frame)-40)/2,
                              40,
                              40);
    self.downloadImageView.frame = CGRectMake(CGRectGetMinX(frame),
                                              (CGRectGetMinY(frame)+1),
                                              38,
                                              38);
    
    self.sizeLabel.frame = CGRectMake(CGRectGetMaxX(frame)+5,
                                      (CGRectGetMaxY(self.frame)-20)/2,
                                      50,
                                      20);
    
    _uploadProgressIndicater.frame = CGRectMake(0,
                                                 0,
                                                 CGRectGetWidth(self.bounds),
                                                 CGRectGetHeight(self.bounds));
    _uploadProgressIndicater.center = CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2);
    

}

-(void)setupThumbnailViewWithPhotoShareContext:(TMPhotoShareContext*)photoshareContext {
    
    self.photoshareContext = photoshareContext;
    UIImage *thumbnailImage = [photoshareContext getThumbnailImage];
    if(!thumbnailImage) {
        [self.imgView setImageWithURL:[NSURL URLWithString:photoshareContext.thumbnailImageURLString]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:photoshareContext.thumbnailImageURLString]];
        [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        
        NSMutableDictionary *eventInfoParams = [NSMutableDictionary dictionary];
        eventInfoParams[@"size"] = self.photoshareContext.jpegImageSize;
        eventInfoParams[@"matchid"] = @(self.matchId);
        NSMutableDictionary *eventParams = [NSMutableDictionary dictionary];
        NSDate* startDate = [NSDate date];
        eventParams[@"startDate"] = startDate;
        
        __weak __typeof(self) weakSelf = self;
        [self.imgView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            
            weakSelf.imgView.image = image;
            [weakSelf.photoshareContext cacheThumbnailImage:image];
            
            NSDate* endDate = [NSDate date];
            eventParams[@"endDate"] = endDate;
            [weakSelf trackThumbnailImageDownloadEventAction:@"download_thumbnail"
                                                  withStatus:@"success"
                                                  withParams:eventParams
                                         withEventInfoParams:eventInfoParams];
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            //track
            NSDate* endDate = [NSDate date];
            eventParams[@"endDate"] = endDate;
            if(error.localizedDescription) {
                eventInfoParams[@"reason"] = error.localizedDescription;
            }
            [weakSelf trackThumbnailImageDownloadEventAction:@"download_thumbnail"
                                                  withStatus:@"fail"
                                                  withParams:eventParams
                                         withEventInfoParams:eventInfoParams];
        }];
    }
    else {
        self.imgView.image = thumbnailImage;
    }
    /////
    UIView *mainImageView = [self viewWithTag:61000];
    if(mainImageView) {
        [mainImageView removeFromSuperview];
        mainImageView = nil;
    }
    /////
    self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
    self.bgView.backgroundColor = [UIColor blackColor];
    self.bgView.alpha = 0.6;
    self.bgView.layer.cornerRadius = 22;
    [self addSubview:self.bgView];
    
    //////////
    //self.downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
   // self.downloadButton.frame = CGRectZero;
    //[self.downloadButton setImage:[UIImage imageNamed:@"Download"] forState:UIControlStateNormal];
    //self.downloadButton.backgroundColor = [UIColor redColor];
    //[self addSubview:self.downloadButton];
    
    self.downloadImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Download"]];
    self.downloadImageView.frame = CGRectZero;
    [self addSubview:self.downloadImageView];
    
    self.sizeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.sizeLabel.text = photoshareContext.jpegImageSize;
    self.sizeLabel.textColor = [UIColor whiteColor];
    self.sizeLabel.font = [UIFont systemFontOfSize:12];
    //self.sizeLabel.backgroundColor = [UIColor redColor];
    [self addSubview:self.sizeLabel];
}

-(void)setupViewWithFullImage:(UIImage*)image {
    self.imgView.image = image;
}

-(void)setMainImage:(UIImage*)image {
    [self.bgView removeFromSuperview];
    self.bgView = nil;
//    [self.downloadButton removeFromSuperview];
//    self.downloadButton = nil;
    [self.downloadImageView removeFromSuperview];
    self.downloadImageView = nil;
    [self.sizeLabel removeFromSuperview];
    self.sizeLabel = nil;
    
    self.imgView.image = image;
}

-(void)setupViewForFullImageDownload:(UIImageView*)imageView {
    imageView.frame = self.bounds;
    imageView.tag = 61000;
    [self addSubview:imageView];
    
    [self.bgView removeFromSuperview];
    self.bgView = nil;
//    [self.downloadButton removeFromSuperview];
//    self.downloadButton = nil;
    [self.downloadImageView removeFromSuperview];
    self.downloadImageView = nil;
    self.downloadImageView.image = nil;
    [self.sizeLabel removeFromSuperview];
    self.sizeLabel = nil;
}

-(void)setupViewForPhotoUploadProgress:(NSURLSessionDataTask*)task {
    if(![self.uploadProgressIndicater superview]) {
        [self addSubview:self.uploadProgressIndicater];
        
        [task addObserver:self forKeyPath:@"state" options:(NSKeyValueObservingOptions)0 context:TMTaskCountOfBytesSentContext];
        [task addObserver:self forKeyPath:@"countOfBytesSent" options:(NSKeyValueObservingOptions)0 context:TMTaskCountOfBytesSentContext];
    }
}
-(void)resetViewForPhotoUploadProgress {
    //NSLog(@"PV SUperview :%@",[self.uploadProgressIndicater superview]);
    if(self.uploadProgressIndicater && [self.uploadProgressIndicater superview]) {
        [self.uploadProgressIndicater removeFromSuperview];
        self.uploadProgressIndicater = nil;
        @try {
            [self removeObserver:self forKeyPath:@"state" context:TMTaskCountOfBytesSentContext];
            [self removeObserver:self forKeyPath:@"countOfBytesSent" context:TMTaskCountOfBytesSentContext];
        }
        @catch (NSException *exception) {}
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(__unused NSDictionary *)change
                       context:(void *)context
{
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
    if (context == TMTaskCountOfBytesSentContext || context == TMTaskCountOfBytesReceivedContext) {
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(countOfBytesSent))]) {
            if ([object countOfBytesExpectedToSend] > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    CGFloat progress = [object countOfBytesSent] / ([object countOfBytesExpectedToSend] * 1.0f);
                    [self.uploadProgressIndicater setProgressValue:progress];
                });
            }
        }
        
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(countOfBytesReceived))]) {
            if ([object countOfBytesExpectedToReceive] > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    CGFloat progress = [object countOfBytesReceived] / ([object countOfBytesExpectedToReceive] * 1.0f);
                    [self.uploadProgressIndicater setProgressValue:progress];
                });
            }
        }
        
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(state))]) {
            if ([(NSURLSessionTask *)object state] == NSURLSessionTaskStateCompleted) {
                @try {
                    [object removeObserver:self forKeyPath:NSStringFromSelector(@selector(state))];
                    
                    if (context == TMTaskCountOfBytesSentContext) {
                        [object removeObserver:self forKeyPath:NSStringFromSelector(@selector(countOfBytesSent))];
                    }
                    
                    if (context == TMTaskCountOfBytesReceivedContext) {
                        [object removeObserver:self forKeyPath:NSStringFromSelector(@selector(countOfBytesReceived))];
                    }
                }
                @catch (NSException * __unused exception) {}
            }
        }
    }
#endif
}


//-(void)trackThumbnailImageDownloadEventAction:(NSString*)eventAction
//                          withStatus:(NSString*)status
//                          withParams:(NSDictionary*)eventParams {
//    
//    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
//    eventDictionary[@"eventCategory"] = @"photo_share";
//    eventDictionary[@"eventAction"] = eventAction;
//    eventDictionary[@"label"] = status;
//    eventDictionary[@"status"] = status;
//    if(eventParams) {
//        eventDictionary[@"event_info"] = eventParams;
//    }
//    
//    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
//}

-(void)trackThumbnailImageDownloadEventAction:(NSString*)eventAction
                          withStatus:(NSString*)status
                          withParams:(NSDictionary*)eventParams
                 withEventInfoParams:(NSDictionary*)eventInfoParams{
    
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"photo_share";
    eventDictionary[@"eventAction"] = eventAction;
    eventDictionary[@"label"] = status;
    eventDictionary[@"status"] = status;
    if(eventParams) {
        [eventDictionary addEntriesFromDictionary:eventParams];
    }
    if(eventInfoParams) {
        eventDictionary[@"event_info"] = eventInfoParams;
    }
    
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}

//
//-(void)downloadMainImage {
//    ////
//    [self.bgView removeFromSuperview];
//    [self.downloadButton removeFromSuperview];
//    [self.downloadImageView removeFromSuperview];
//    [self.sizeLabel removeFromSuperview];
//    
//    //////
//    NSURL *url = [NSURL URLWithString:self.fullImageURL];
//    [self.activityInicator startAnimating];
//    [self.imgView setImageWithURLRequest:[NSURLRequest requestWithURL:url] placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
//        
//        self.imgView.image = image;
//        [self.photoshareContext cacheImageData1:UIImageJPEGRepresentation(image, 1)];
//        [self.activityInicator stopAnimating];
//        self.activityInicator = nil;
//        
//        
//        
//    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
//        [self.activityInicator stopAnimating];
//        self.activityInicator = nil;
//    }];
//}

@end

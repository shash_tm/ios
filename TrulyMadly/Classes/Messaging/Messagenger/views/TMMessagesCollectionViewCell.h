//
//  TMMessagesCollectionViewCell.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMMessagesCellTextView.h"
#import "TMMessageContainerView.h"


@protocol TMMessagesCollectionViewCellDelegate;

@interface TMMessagesCollectionViewCell : UICollectionViewCell

@property(weak, nonatomic) id<TMMessagesCollectionViewCellDelegate> delegate;
@property(strong, nonatomic, readonly) UILabel *cellTopLabel;
@property(strong, nonatomic, readonly) UILabel *cellBottomLabel;
@property(strong, nonatomic, readonly) TMMessagesCellTextView *messageTextView;
@property(strong, nonatomic, readonly) UIImageView *messageBubbleImageView;
@property(strong, nonatomic, readonly) TMMessageContainerView *messageBubbleContainerView;
@property(strong, nonatomic, readonly) UIImageView *sentFailedIndicatorImageView;
@property(strong, nonatomic, readonly) UIView *sentFailedIndicatorContainerView;
@property(strong, nonatomic) UIView *mediaView;
@property(weak, nonatomic, readonly) UITapGestureRecognizer *tapGestureRecognizer;
@property (assign, nonatomic) CGFloat bottomLabelHeight;
@property (assign, nonatomic) CGFloat topLabelHeight;
@property (assign, nonatomic) CGFloat bubbleContainerWidth;
@property (assign, nonatomic) CGSize sentFailedIndicatorViewSize;

+ (NSString *)cellReuseIdentifier;
+ (NSString *)mediaCellReuseIdentifier;
+ (UINib *)nib;

@end


@protocol TMMessagesCollectionViewCellDelegate <NSObject>

-(void)didTapCellAtIndex:(NSInteger)selectedIndex;
-(void)didSelectSentFailedMessageCellAtIndex:(NSInteger)selectedIndex;
-(void)didSelectMediaCell:(NSInteger)selectedIndex;

@end

//
//  TMImageLinkMessageView.m
//  TrulyMadly
//
//  Created by Ankit on 30/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMImageLinkMessageView.h"
#import "UIImageView+AFNetworking.h"

@interface TMImageLinkMessageView ()

@property(nonatomic,strong)UIImageView *imageView;

@end


@implementation TMImageLinkMessageView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.imageView.clipsToBounds = true;
        [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.imageView];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = self.bounds;
}

-(void)setupViewWithImageURL:(NSURL*)imageURL {
    
    __weak __typeof(self) weakSelf = self;
    [self.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL]
                          placeholderImage:nil
                                   success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
                                       [weakSelf.imageView setImage:image];
                                       
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
      
        
    }];
}


@end

//
//  TMPhotoMessageView.h
//  TrulyMadly
//
//  Created by Ankit on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMPhotoShareContext;


@interface TMPhotoMessageView : UIView


@property(nonatomic,assign)NSInteger matchId; //for tracking

-(void)setupThumbnailViewWithPhotoShareContext:(TMPhotoShareContext*)photoshareContext;
-(void)setupViewWithFullImage:(UIImage*)image;
//-(void)downloadMainImage;
-(void)setMainImage:(UIImage*)image;
-(void)setupViewForFullImageDownload:(UIImageView*)imageView;
-(void)setupViewForPhotoUploadProgress:(NSURLSessionDataTask*)task;
-(void)resetViewForPhotoUploadProgress;

@end

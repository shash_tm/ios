//
//  TMMessagesCollectionView.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMMessagesCollectionViewDelegateFlowLayout.h"
#import "TMMessagesCollectionViewDataSource.h"
#import "TMMessagesCollectionViewFlowLayout.h"

@interface TMMessagesCollectionView : UICollectionView

@property (weak, nonatomic) id<TMMessagesCollectionViewDataSource> dataSource;

@property (weak, nonatomic) id<TMMessagesCollectionViewDelegateFlowLayout> delegate;

@property (strong, nonatomic) TMMessagesCollectionViewFlowLayout *collectionViewLayout;

@end

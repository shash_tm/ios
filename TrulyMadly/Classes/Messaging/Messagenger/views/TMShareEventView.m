//
//  TMShareEventView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMShareEventView.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"

@interface TMShareEventView ()

@property(nonatomic,strong)TMProfileImageView *profileImgView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,assign)BOOL isFailedMessage;
@property(nonatomic,strong)UILabel *actionLabel;

@end


@implementation TMShareEventView

-(instancetype)initWithFrame:(CGRect)frame isFailedMessage:(BOOL)failedMessage {
    self = [super initWithFrame:frame];
    if(self) {
        self.isFailedMessage = failedMessage;
        self.profileImgView = [[TMProfileImageView alloc] initWithFrame:self.bounds];
        self.profileImgView.clipsToBounds = true;
        [self.profileImgView setImageContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.profileImgView];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    self.profileImgView.frame = self.bounds;
    [self setTitleText:self.titleLabel.text];
}

-(void)setBackgroundImageFromURL:(NSURL*)imageURL isDarkCentered:(BOOL) isDarkCentered {
    
    if(imageURL && [imageURL isKindOfClass:[NSURL class]]) {
        NSString *lastPathComponent = [imageURL lastPathComponent];
        //[self.profileImgView setDefaultImage:[UIImage imageNamed:@"deal_image_placeholder"]];
        [self.profileImgView setImageFromURLStringWithBackgroundGradient:[imageURL absoluteString] isDarkCentered:isDarkCentered];
        [self.profileImgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
    }
}

-(void)setupUIForASKMessage {
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self addSubview:self.titleLabel];
    
    self.actionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.actionLabel.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:111.0/255.0 blue:178.0/255.0 alpha:1.0];
    self.actionLabel.textColor = [UIColor whiteColor];
    self.actionLabel.textAlignment = NSTextAlignmentCenter;
    self.actionLabel.clipsToBounds = YES;
    self.actionLabel.text = @"Take A Look";
    [self addSubview:self.actionLabel];
}

-(void)setTitleText:(NSString *)titleText {
    
    self.titleLabel.text = titleText;
        
    CGFloat initXPos = (self.isOutGoingMessage) ? 10 : 15;
    CGFloat widthOffset = (self.isOutGoingMessage) ? 25 : 23;
    CGFloat maxWidth = CGRectGetWidth(self.bounds)- widthOffset;
    NSInteger maxNumberOfLines = 5;
    CGFloat heightPerLine = 20;
    CGFloat yPosOffsetBetweenTitleAndActionLabel = 20;
    CGSize actionLabelSize = CGSizeMake(120, 32);
        
    CGFloat maxHeight = heightPerLine * maxNumberOfLines;
    NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
    CGSize constrintSize = CGSizeMake(maxWidth, maxHeight);
    CGRect rect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
    CGFloat totalContentHeight =  (rect.size.height + yPosOffsetBetweenTitleAndActionLabel + actionLabelSize.height);
    CGFloat titleLabelYPos = (CGRectGetHeight(self.bounds) - totalContentHeight)/2;
    CGFloat actionLabelYPos = CGRectGetMaxY(self.titleLabel.frame) + yPosOffsetBetweenTitleAndActionLabel;
        
    self.titleLabel.frame = CGRectMake(initXPos, titleLabelYPos, maxWidth, rect.size.height);
    self.actionLabel.frame = CGRectMake((CGRectGetWidth(self.bounds) - actionLabelSize.width)/2,
                                            actionLabelYPos,
                                            actionLabelSize.width,
                                            actionLabelSize.height);
    self.actionLabel.layer.cornerRadius = self.actionLabel.bounds.size.height/2;

}

@end

//
//  TMMessageViewController.m
//  TrulyMadly
//
//  Created by Ankit on 30/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageViewController.h"
#import "TMMessagingController.h"
#import "TMMessagesCollectionViewFlowLayout.h"
#import "TMMessagesCollectionViewFlowLayoutInvalidationContex.h"
#import "TMMessagesCollectionViewCell.h"
#import "TMMessagesCollectionViewCellIncoming.h"
#import "TMMessagesCollectionViewCellOutgoing.h"
#import "TMMessageBubbleImageDataSource.h"
#import "TMMessageData.h"
#import "TMMessage.h"
#import "TMTimestampFormatter.h"
#import "TMMessagesBubbleImage.h"
#import "TMMessagesBubbleImageFactory.h"
#import "UIColor+TMColorAdditions.h"
#import "TMMessageConfiguration.h"
#import "TMStickerListView.h"
#import "TMKeyboardController.h"
#import "NSString+TMAdditions.h"
#import "TMMessagingController.h"
#import "TMLog.h"
#import "TMQuizListView.h"
#import "TMQuizOverlayViewController.h"
#import "TMQuizController.h"
#import "TMMessageMediaData.h"
#import "TMMessagesCollectionViewCell.h"
#import "TMQuizInfo.h"
#import "TMMediaItem.h"
#import "TMAnalytics.h"
#import "TMDataStore.h"
#import "TMActivityIndicatorView.h"
#import "TMMessageCollectionViewFooter.h"
#import "TMMessageCollectionViewHeader.h"


#define QUIZ_VIEW_TAG 12321
#define CHAT_TYPING_TIMEOUT 5
#define CHAT_TYPING_INDICATOR_VIEW_TAG 1234321
#define CHAT_TYPING_INDICATOR_VIEW_HEIGHT 56


typedef enum {
    TMInputType_None,
    TMInputType_Keyboard=1,
    TMInputType_Sticker,
    TMInputType_Quiz
}TMMessageInputType;

static void * kJSQMessagesKeyValueObservingContext = &kJSQMessagesKeyValueObservingContext;

@interface TMMessageViewController ()<TMMessagesCollectionViewDataSource,TMMessagesCollectionViewDelegateFlowLayout,UITextViewDelegate,TMMessagesInputToolbarDelegate,TMKeyboardControllerDelegate,TMStickerListViewDataSource,TMStickerListViewDelegate,TMQuizListViewDelegate,TMMessagesCollectionViewCellDelegate,UIActionSheetDelegate,TMMessageCollectionViewHeaderDelegate>

@property(nonatomic,strong) NSIndexPath *userLastMsgSentIndex;

@property(nonatomic,strong) TMMessagesBubbleImage *outgoingBubbleImageData;
@property(nonatomic,strong) TMMessagesBubbleImage *incomingBubbleImageData;
@property (copy, nonatomic) NSString *outgoingCellIdentifier;
@property (copy, nonatomic) NSString *outgoingMediaCellIdentifier;
@property (copy, nonatomic) NSString *incomingCellIdentifier;
@property (copy, nonatomic) NSString *incomingMediaCellIdentifier;

@property(nonatomic,strong) TMStickerListView *stickerListView;
@property(nonatomic,strong) TMQuizListView *quizListView;
@property(assign,nonatomic) BOOL isObserving;
@property(assign,nonatomic) BOOL isStickerShown;
@property(assign,nonatomic) BOOL isQuizShown;
@property(assign,nonatomic) BOOL isScrollingKeyboardOffsetAdded; //as ha hack until autolayout works
@property(assign,nonatomic) TMMessageInputType messageInputType;

@property(nonatomic,strong)NSMutableDictionary* eventDict;

@property(nonatomic,strong) UITapGestureRecognizer *tapGrestureRecognizer;

@property(nonatomic,assign) NSInteger sentIndex;
@property(nonatomic,assign) NSInteger readIndex;

@property(nonatomic, strong) TMActivityIndicatorView* activityIndicatorView;
@property(nonatomic, strong) UIActionSheet* actionSheet;
@property(nonatomic, strong) TMMessage* failedMessage;
@property(nonatomic, assign) NSInteger failedMessageIndex;

@property(nonatomic, strong) NSTimer* chatTypingIndicatorTimer;
@property(nonatomic, strong) NSDate* lastChatTypeDate;
@property(nonatomic, assign) BOOL isTypingIndicatorShown;

@property(nonatomic, strong) NSArray* galleryArray;
@property(nonatomic, strong) NSArray* stickerArray;

@end

@implementation TMMessageViewController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.automaticallyScrollsToMostRecentMessage = YES;
        self.shownDateIndexes = [NSMutableDictionary dictionaryWithCapacity:8];
        self.conversations = [NSMutableArray arrayWithCapacity:16];
        self.trackDict = [NSMutableDictionary dictionary];
        
        TMMessagesBubbleImageFactory *bubbleFactory = [[TMMessagesBubbleImageFactory alloc] init];
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor outgoingMessageColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor incomingMessageColor]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self configureUIComponents];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideActivityIndicatorViewWithtranslucentView) name:@"decideQuizRequestCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makeStickerAPIRequest) name:@"stickerVersionUpdated" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateKeyboardTriggerPoint];
    
    [self updateTypingIndicatorViewFrame];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.keyboardController beginListeningForKeyboard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    //NSLog(@"TMMessageConversationViewController --- dealloc");
    
    [self.collectionView removeFromSuperview];
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
    self.collectionView = nil;
    
    self.inputToolbar = nil;
    
    self.outgoingCellIdentifier = nil;
    self.incomingCellIdentifier = nil;
    
    [self.keyboardController endListeningForKeyboard];
    self.keyboardController = nil;
    
    if(self.actionSheet) {
        [self.actionSheet dismissWithClickedButtonIndex:2 animated:FALSE];
        self.actionSheet = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)configureUIComponents{
    
    ///message collectionview
    CGRect maxUsableRect = [self getMaxUsableFrame];
    CGFloat inputToolbarHeight = 54;
    
    TMMessagesCollectionViewFlowLayout *flowLayout = [[TMMessagesCollectionViewFlowLayout alloc] init];
    CGRect rect = CGRectMake(CGRectGetMinX(maxUsableRect),
                             CGRectGetMinY(maxUsableRect),
                             CGRectGetWidth(maxUsableRect),
                             CGRectGetMaxY(maxUsableRect)-inputToolbarHeight);
    self.collectionView = [[TMMessagesCollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerClass:[TMMessageCollectionViewFooter class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
    [self.collectionView registerClass:[TMMessageCollectionViewHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self.view addSubview:self.collectionView];
    
    self.outgoingCellIdentifier = [TMMessagesCollectionViewCellOutgoing cellReuseIdentifier];
    self.outgoingMediaCellIdentifier = [TMMessagesCollectionViewCellOutgoing mediaCellReuseIdentifier];
    
    self.incomingCellIdentifier = [TMMessagesCollectionViewCellIncoming cellReuseIdentifier];
    self.incomingMediaCellIdentifier = [TMMessagesCollectionViewCellIncoming mediaCellReuseIdentifier];
    
    ///input toolbar
    [self setupInputToolbar];
    
    ////add stickerview
    CGFloat stickerViewCellWidth = floor(CGRectGetWidth(self.view.frame)/4);
    CGFloat stickerViewWidth = CGRectGetWidth(maxUsableRect);
    CGFloat stickerViewHeight = 6 * floor(CGRectGetWidth(self.view.frame)/9);
    CGFloat yPos = CGRectGetMaxY(maxUsableRect);
    CGFloat xPos = CGRectGetMinX(maxUsableRect);
    self.stickerListView = [[TMStickerListView alloc] initWithFrame:CGRectMake(xPos,
                                                                               yPos,
                                                                               stickerViewWidth,
                                                                               stickerViewHeight)
                                                       withCellSize:CGSizeMake(stickerViewCellWidth, stickerViewCellWidth) delegate:self dataSource:self];
    [self.view addSubview:self.stickerListView];
    
    //check for the quiz timestamp
    BOOL isQuizzesFetchingRequired = NO;
    
    //check the previous timestamp
    if ([TMDataStore containsObjectForKey:LAST_QUIZZES_FETCH_DATE]) {
        NSDate* previousStickerFetchDate = [TMDataStore retrieveObjectforKey:LAST_QUIZZES_FETCH_DATE];
        
        if ([[NSDate date] timeIntervalSinceDate:previousStickerFetchDate] > 60*60) {
            isQuizzesFetchingRequired = YES;
        }
    }
    else {
        isQuizzesFetchingRequired = YES;
    }
    
    if (isQuizzesFetchingRequired) {
        //make the request for the quizzes
        // [self.quizController fetchPlayedQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
    }
    
    ////keyboard controller
    self.tapGrestureRecognizer = [[UITapGestureRecognizer alloc] init];
    [self.collectionView addGestureRecognizer:self.tapGrestureRecognizer];
    
    self.keyboardController = [[TMKeyboardController alloc] initWithTextView:self.inputToolbar.contentView.textView
                                                                 contextView:self.view
                                                        panGestureRecognizer:self.collectionView.panGestureRecognizer
                                                        tapGestureRecognizer:self.tapGrestureRecognizer
                                                                    delegate:self];
    
}
-(void)setupInputToolbar {
    CGRect maxUsableRect = [self getMaxUsableFrame];
    CGFloat inputToolbarHeight = 54;
//    CGRect rect = CGRectMake(CGRectGetMinX(maxUsableRect),
//                             CGRectGetMinY(maxUsableRect),
//                             CGRectGetWidth(maxUsableRect),
//                             CGRectGetMaxY(maxUsableRect)-inputToolbarHeight);
    CGRect rect = CGRectMake(CGRectGetMinX(maxUsableRect),
                      CGRectGetMaxY(maxUsableRect) - inputToolbarHeight,
                      CGRectGetWidth(maxUsableRect),
                      inputToolbarHeight);
    self.inputToolbar = [[TMMessagesInputToolbar alloc] initWithFrame:rect];
    self.inputToolbar.backgroundColor = [UIColor clearColor];
    self.inputToolbar.delegate = self;
    self.inputToolbar.contentView.textView.placeHolder = NSLocalizedStringFromTable(@"New Message", @"JSQMessages", @"Placeholder text for the message input text view");
    self.inputToolbar.contentView.textView.delegate = self;
    [self.view addSubview:self.inputToolbar];
}
- (void)scrollToBottomAnimated:(BOOL)animated hideCollectionVewWhileScrolling:(BOOL)hidden
{
    //TMLOG(@":_______scrollToBottomAnimated1");
    if ([self.collectionView numberOfSections] == 0) {
        return;
    }
    
    NSInteger items = [self.collectionView numberOfItemsInSection:0];
    
    if (items == 0) {
        return;
    }
    
    if(hidden) {
        self.collectionView.hidden = hidden;
    }
    CGFloat collectionViewContentHeight = [self.collectionView.collectionViewLayout collectionViewContentSize].height;
    BOOL isContentTooSmall = (collectionViewContentHeight < self.collectionView.bounds.size.height);
    
    if (isContentTooSmall) {
        //  workaround for the first few messages not scrolling
        //  when the collection view content size is too small, `scrollToItemAtIndexPath:` doesn't work properly
        //  this seems to be a UIKit bug, see #256 on GitHub
        [self.collectionView scrollRectToVisible:CGRectMake(0.0, collectionViewContentHeight - 1.0f, 1.0f, 1.0f)
                                        animated:animated];
        return;
    }
    
    //  workaround for really long messages not scrolling
    //  if last message is too long, use scroll position bottom for better appearance, else use top
    //  possibly a UIKit bug, see #480 on GitHub
    NSUInteger finalRow = MAX(0, [self.collectionView numberOfItemsInSection:0] - 1);
    NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
    CGSize finalCellSize = [self.collectionView.collectionViewLayout sizeForItemAtIndexPath:finalIndexPath];
    
    CGFloat maxHeightForVisibleMessage = CGRectGetHeight(self.collectionView.bounds) - self.collectionView.contentInset.top - 54;
    
    UICollectionViewScrollPosition scrollPosition = (finalCellSize.height > maxHeightForVisibleMessage) ? UICollectionViewScrollPositionBottom : UICollectionViewScrollPositionTop;
    //TMLOG(@":_______scrollToBottomAnimated2");
    [self.collectionView scrollToItemAtIndexPath:finalIndexPath
                                atScrollPosition:scrollPosition
                                        animated:animated];
    //TMLOG(@":_______scrollToBottomAnimated3");
}

-(void)tapAction {
    if(self.messageInputType == TMInputType_Keyboard) {
        [self.view endEditing:TRUE];
    }
    else if(self.messageInputType == TMInputType_Sticker) {
        [self showStickerView];
    }
    else if (self.isQuizShown) {
        [self hideQuizView];
    }
    
    self.messageInputType = TMInputType_None;
}
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                      date:(NSDate *)date
{
    
}

- (void)finishSendingMessage
{
    [self finishSendingMessageAnimated:false];
}

- (void)finishSendingMessageAnimated:(BOOL)animated {
    
    UITextView *textView = self.inputToolbar.contentView.textView;
    textView.text = nil;
    [self.inputToolbar toggleSendButtonEnabled];
    [self setupSendActionButtonForTextLength:textView.text.length];
    [[NSNotificationCenter defaultCenter] postNotificationName:UITextViewTextDidChangeNotification object:textView];
}

-(TMMessage*)messageAtIndex:(NSInteger)msgIndex {
    TMMessage *messageItem = NULL;
    id messageData = [self.conversations objectAtIndex:msgIndex];
    if([messageData isKindOfClass:[NSDictionary class]]) {
        messageItem = [[TMMessage alloc] initWithMessageDictionary:messageData];
    }
    else {
        messageItem = (TMMessage*)messageData;
    }
    return messageItem;
}

#pragma mark - Chat Typing method

/*
 * Callback method for receiving chat typing emit
 * @param match ID
 */
-(void)didReceiveChatTypingEmit:(NSString*)matchId
{
    if ([matchId isEqualToString:[[TMMessagingController sharedController] getCurrentMatchId]]) {
        
        if (!(self.chatTypingIndicatorTimer && self.chatTypingIndicatorTimer.isValid)) {
            //show typing view only if previous one is not visible
            [self showChatTypingIndicatorView];
        }
        //schedule the timer for configured seconds
        if (self.chatTypingIndicatorTimer) {
            [self.chatTypingIndicatorTimer invalidate];
            self.chatTypingIndicatorTimer = nil;
        }
        self.chatTypingIndicatorTimer = [NSTimer scheduledTimerWithTimeInterval:CHAT_TYPING_TIMEOUT target:self selector:@selector(chatTypingIndicatorTimedOut) userInfo:nil repeats:NO];
    }
}

- (void) updateTypingIndicatorViewFrame {
    [self.collectionView.collectionViewLayout invalidateLayout];
}


/**
 * shows the chat typing indicator 
 */
- (void) showChatTypingIndicatorView {
    
    //set the variable
    self.isTypingIndicatorShown = YES;
    self.collectionView.collectionViewLayout.footerReferenceSize = CGSizeMake(self.collectionView.bounds.size.width, CHAT_TYPING_INDICATOR_VIEW_HEIGHT);
    [self.collectionView reloadData];
    [self scrollToBottomIfLastMessageShown];
}


/**
 * Scrolls the collection view to bottom in case last message is visible
 */
- (void) scrollToBottomIfLastMessageShown {
    if(self.collectionView.contentOffset.y >= (self.collectionView.contentSize.height - self.collectionView.frame.size.height)) {
        //user has scrolled to the bottom
       [self scrollToBottomAnimated:YES hideCollectionVewWhileScrolling:NO];
    }
}


/**
 * hides the chat typing indicator
 */
-(void) hideChatTypingIndicatorView {
    
    self.isTypingIndicatorShown = NO;
    
    if (self.chatTypingIndicatorTimer) {
        [self.chatTypingIndicatorTimer invalidate];
        self.chatTypingIndicatorTimer = nil;
    }
}


/**
 * hides the chat typing indicator due to incoming message
 */
- (void) hideTypingIndicatorDueToIncomingMessageOrNoConnectionStatus {
    [self hideChatTypingIndicatorView];
}


/**
 * Callback method when the chat typing indicator times out
 */
-(void) chatTypingIndicatorTimedOut
{
    [self hideChatTypingIndicatorView];
    
    //reload to remove typing indicator
    [self.collectionView reloadData];
}

#pragma mark - TMMessages collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.conversations.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (id<TMMessageData>)collectionView:(TMMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMMessage *messageItem = [self messageAtIndex:indexPath.row];
    return messageItem;
}

- (id<TMMessageBubbleImageDataSource>)collectionView:(TMMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMMessage *message = [self messageAtIndex:indexPath.row];
    
    BOOL isOutgoingMessage =  (message.senderType == MESSAGESENDER_ME) ? 1 : 0;
    if(isOutgoingMessage) {
        return self.outgoingBubbleImageData;
    }
    else {
        return self.incomingBubbleImageData;
    }
    
    return nil;
}

- (CGSize)collectionView:(TMMessagesCollectionView *)collectionView sentFailedIndicatorSizeAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(5, 5);
    TMMessage *message = [self messageAtIndex:indexPath.row];
    if((message.senderType == MESSAGESENDER_ME) && (message.deliveryStatus == SENDING_FAILED)) {
        size = CGSizeMake(44, 44);
    }
    return size;
}

- (NSAttributedString *)collectionView:(TMMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"attributedTextForCellTopLabelAtIndexPath");
    NSString *index = [self.shownDateIndexes objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    if(index) {
        TMMessage *message = [self messageAtIndex:indexPath.row];
        TMTimestampFormatter *dateFormatter = [TMTimestampFormatter sharedFormatter];
        NSDate *date = [dateFormatter absoulteDateFromString:message.timestamp];
        NSAttributedString *text = [[TMTimestampFormatter sharedFormatter] attributedDateStringForDate:date];
        return text;
    }
    return nil;
}

-(NSAttributedString*)collectionView:(TMMessagesCollectionView*)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath*)indexPath
{
    NSString *messageStatusString = nil;
    TMMessage *message = [self messageAtIndex:indexPath.row];
    TMTimestampFormatter *dateFormatter = [TMTimestampFormatter sharedFormatter];
    NSDate *date = [dateFormatter absoulteDateFromString:message.timestamp];
    
    if(message.senderType == MESSAGESENDER_ME && self.showDeliveryStatus) {
        if(message.deliveryStatus == SENT) {
            NSDate *dateq = [dateFormatter absoulteDateFromString:self.lastSeenMsgTs];
            //TMLOG(@"dateq:%@ lastseenmessagets:%@",dateq,self.lastSeenMsgTs);
            NSTimeInterval distanceBetweenDates = [dateq timeIntervalSinceDate:date];
            if((self.lastSeenMsgTs) && (distanceBetweenDates>=0)) {
                messageStatusString = @"Read";
            }
            else {
                messageStatusString = @"Delivered";
            }
        }
        else if(message.deliveryStatus == SENDING) {
            messageStatusString = @"Sending";
        }
        else if(message.deliveryStatus == SENDING_FAILED) {
            if(message.type == MESSAGETTYPE_CD_ASK) {
                messageStatusString = @"Failed to send invite. Tap to retry";
            }
            else if(message.type == MESSAGETTYPE_CD_VOUCHER) {
                messageStatusString = @"Failed to get voucher. Tap to retry";
            }
            else {
                messageStatusString = @"Tap to retry";
            }
        }
    }
    
    NSAttributedString *dateText = [[TMTimestampFormatter sharedFormatter] attributedTimestampForDate:date byAppendingDeliveryStatus:messageStatusString];
    
    return dateText;
}

- (UICollectionViewCell *)collectionView:(TMMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMMessage *messageItem = [self messageAtIndex:indexPath.row];
    BOOL isOutgoingMessage =  [messageItem isOutgoingMessage];
    BOOL isMediaMessage = [messageItem isMediaMessage];

    NSString *cellIdentifier = nil;
    if (isMediaMessage) {
        cellIdentifier = isOutgoingMessage ? self.outgoingMediaCellIdentifier : self.incomingMediaCellIdentifier;
    }
    else {
        cellIdentifier = isOutgoingMessage ? self.outgoingCellIdentifier : self.incomingCellIdentifier;
    }
    
    TMMessagesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    if (!isMediaMessage) {
        //NSLog(@"TEXT:%@",[messageItem attributedText]);
        cell.messageTextView.text = [messageItem text];
        cell.messageTextView.dataDetectorTypes = UIDataDetectorTypeLink;
        
        NSParameterAssert(cell.messageTextView.text != nil);
        
        id<TMMessageBubbleImageDataSource> bubbleImageDataSource = [collectionView.dataSource collectionView:collectionView messageBubbleImageDataForItemAtIndexPath:indexPath];
        if (bubbleImageDataSource != nil) {
            cell.messageBubbleImageView.image = [bubbleImageDataSource messageBubbleImage];
        }
    }
    else {
        messageItem.media.appliesMediaViewMaskAsOutgoing = isOutgoingMessage;
        
        if((messageItem.type == MESSAGETTYPE_QUIZ_NUDGE_SINGLE) ||
           (messageItem.type == MESSAGETTYPE_QUIZ_NUDGE_DOUBLE) ) {
            TMQuizInfo *quizInfo = [self.quizController getQuinInfoForQuizId:messageItem.quizId];
            [messageItem.media setImageURLString:quizInfo.quizIconURL];
            [messageItem.media setTitleText:quizInfo.quizDisplayName];
            [messageItem.media setIsFlare:[quizInfo isFlareForUserID:[[TMMessagingController sharedController] getCurrentMatchId]]];
            [messageItem.media setMessageSender:messageItem.senderType];
            [messageItem.media setNudgeBackgroundColor:isOutgoingMessage?[UIColor likeselectedColor]:[UIColor likeunSelectedColor]];
        }
        else if(messageItem.type == MESSAGETTYPE_QUIZ_MESSAGE) {
            TMQuizInfo *quizInfo = [self.quizController getQuinInfoForQuizId:messageItem.quizId];
            [messageItem.media setImageURLString:quizInfo.quizIconURL];
            
            BOOL isFlare = [quizInfo isFlareForUserID:[[TMMessagingController sharedController] getCurrentMatchId]];
            [messageItem.media setIsFlare:isFlare];
            [messageItem.media setNudgeBackgroundColor:isOutgoingMessage?[UIColor likeselectedColor]:[UIColor likeunSelectedColor]];
        }
        else if([messageItem isCuratedDealMessage]) {
            [messageItem.media setImageURLString:messageItem.datespotImageURLString];
            [messageItem.media setTitleText:[messageItem text] subTitleText:[messageItem datespotAddress]];
        }
        else if([messageItem isPhotoMessage]) {
            [messageItem.media updateUIState];
        }
        else if([messageItem isImageLinkMessage]) {
            [messageItem.media setImageLinkFromURLString:[messageItem imageLinkURLString]];
        }
        else if([messageItem isShareEventMessage]) {
            [messageItem.media setImageURLString:[messageItem shareEventImageURLString]];
            [messageItem.media setTitleText:[messageItem text]];
        }
        
        cell.mediaView = messageItem.media.mediaView;
        TMMediaItem *mediaItem = (TMMediaItem*)messageItem.media;
        [mediaItem updateMask];
    }
    
    cell.cellTopLabel.attributedText = [collectionView.dataSource collectionView:collectionView
                                        attributedTextForCellTopLabelAtIndexPath:indexPath];
    
    cell.cellBottomLabel.attributedText = [collectionView.dataSource collectionView:collectionView
                                        attributedTextForCellBottomLabelAtIndexPath:indexPath];
    
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.shouldRasterize = YES;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {

    if(kind == UICollectionElementKindSectionFooter) {
        TMMessageCollectionViewFooter *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        return footerView;
    }
    else {
        TMMessageCollectionViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerView.delegate = self;
        [headerView updatUI:self.messageConfiuration.sparkUser sparkRemainingTime:self.messageConfiuration.sparkRemainingTime];
        return headerView;
    }
}


#pragma mark - Collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    if(self.isSparkConvsersation) {
        return CGSizeMake(CGRectGetWidth(self.view.frame), 320);
    }
    else {
        return CGSizeZero;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if(!self.isTypingIndicatorShown){
        return CGSizeZero;
    } else {
        return CGSizeMake(self.collectionView.bounds.size.width, CHAT_TYPING_INDICATOR_VIEW_HEIGHT);
    }
}

- (CGSize)collectionView:(TMMessagesCollectionView *)collectionView
                  layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"Size for Row:%@",NSStringFromCGSize([collectionViewLayout sizeForItemAtIndexPath:indexPath]));
    return [collectionViewLayout sizeForItemAtIndexPath:indexPath];
}

- (CGFloat)collectionView:(TMMessagesCollectionView *)collectionView
                   layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *index = [self.shownDateIndexes objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    if(index) {
        return 20.0f;
    }
    else {
        if(indexPath.row > [index integerValue] || !index) {
            TMMessage *message = [self messageAtIndex:indexPath.row];
            TMTimestampFormatter *dateFormatter = [TMTimestampFormatter sharedFormatter];
            NSDate *date = [dateFormatter absoulteDateFromString:message.timestamp];
            BOOL showDate = false;
            if(!self.lastShownDate && date) {
                self.lastShownDate = date;
                showDate = true;
            }
            else {
                NSInteger numberOfDays = [dateFormatter daysBetweenDate:self.lastShownDate andDate:date];
                if(numberOfDays > 0) {
                    showDate = true;
                    self.lastShownDate = date;
                }
            }
            if (showDate) {
                message.showTopLabel = true;
                [self.shownDateIndexes setObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row] forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                //TMLOG(@"2.heightForCellTopLabelAtIndexPath:20");
                return 20.0f;
            }
        }
    }
    return 0.0f;
}

- (CGFloat)collectionView:(TMMessagesCollectionView *)collectionView
                   layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 20.0f;
}

- (CGSize)collectionView:(TMMessagesCollectionView *)collectionView
                  layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout sizeForSentFailedMessageIndicatorViewAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(5, 5);
    TMMessage *message = [self messageAtIndex:indexPath.row];
    if((message.senderType == MESSAGESENDER_ME) && (message.deliveryStatus == SENDING_FAILED)) {
        size = CGSizeMake(44, 44);
    }
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TMLOG(@"Message selected");
}

#pragma mark - Input toolbar delegate

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
       didPressLeftBarButton:(UIButton *)sender {
    
    [self showStickerView];
}
-(void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
      didPressRightBarButton:(UIButton *)sender {
    if (toolbar.sendButtonOnRight) {
        [self didPressSendButton:sender
                 withMessageText:[self currentlyComposedMessageText]
                            date:[NSDate date]];
    }
}
- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
  didPressPhotoSharingButton:(UIButton *)sender {
    [self showPhotoSharingOptions];
}

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
didPressCuratedDealBarButton:(UIButton *)sender {
    [self showCuratedDeal];
   
    //add tracking for icon_clicked action
    //adding event tracking for date button click action
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    [self.eventDict setObject:@"datespots" forKey:@"eventCategory"];
    [self.eventDict setObject:@"icon_clicked" forKey:@"eventAction"];
    [self.eventDict setObject:[[TMMessagingController sharedController] getCurrentMatchId] forKey:@"status"];
    [self.eventDict setObject:[[TMMessagingController sharedController] getCurrentMatchId] forKey:@"label"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
}
-(void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar didPressLeftQuizBarButton:(UIButton *)sender{
    [self showQuizView];
}

- (NSString *)currentlyComposedMessageText
{
    NSString *text = self.inputToolbar.contentView.textView.text;
    return [text stringByTrimingWhitespace];
}

#pragma mark - Make Sticker API request
- (void) makeStickerAPIRequest {
    [[TMMessagingController sharedController] makeStickerAPITRequestWithGalleryRefreshBlock:^{
        if (self.isStickerShown) {
            [self.stickerListView reloadStickerGallery];
        }
    } stickerRefreshBlock:^(int galleryID, int stickerID){
        //do nothing
    } allGalleriesRefreshBlock:^{
        if (self.isStickerShown) {
            [self.stickerListView reloadStickerGallery];
        }
    } failureBlock:^{
        //do nothing
    }];
}


#pragma mark - TMStickerListViewDataSource methods

- (NSArray*) getAllStickerGalleries {
    return [[TMMessagingController sharedController] getAllStickerGalleries];
}

- (NSArray*) getFirstStickerGallery {
    return [[TMMessagingController sharedController] getFirstStickerGallery];
}

- (NSArray*) stickersForGalleryID:(NSUInteger) galleryID {
    return [[TMMessagingController sharedController] stickersForGalleryID:galleryID];
}

- (NSArray*) stickersForTimestampGallery {
    return [[TMMessagingController sharedController] stickersForTimestampGallery];
}

- (UIImage*) getImageForSticker:(TMSticker*) sticker {
    return [[TMMessagingController sharedController] getImageForSticker:sticker];
}


#pragma mark - TMStickerListViewDelegate methods


- (void) didSendSticker:(TMSticker*) sticker {
    //method to be overriden by the subclass
}

- (void) storeHDImageForSticker:(TMSticker* ) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock {
    [[TMMessagingController sharedController] storeHDImageForSticker:sticker stickerRefreshBlock:stickerRefreshBlock];
}
- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID {
    return [[TMMessagingController sharedController] updateTimestampForStickerWithStickerID:stickerID galleryID:galleryID];
}


#pragma mark - Text view delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView != self.inputToolbar.contentView.textView) {
        return;
    }
    
    [textView becomeFirstResponder];
    
    //disable sticker and quiz view
    if (self.isStickerShown) {
        [self hideStickerView];
    }
    if (self.isQuizShown) {
        [self hideQuizView];
    }
    
    if (self.automaticallyScrollsToMostRecentMessage) {
        [self scrollToBottomAnimated:YES hideCollectionVewWhileScrolling:false];
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView != self.inputToolbar.contentView.textView) {
        return;
    }
    
    [self setupSendActionButtonForTextLength:textView.text.length];
    [self.inputToolbar toggleSendButtonEnabled];
    [self fireChatTyping];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView != self.inputToolbar.contentView.textView) {
        return;
    }
    [self setupSendActionButtonForTextLength:textView.text.length];

    self.messageInputType = TMInputType_None;
}


#pragma mark - Key-value observing
#pragma mark -
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == kJSQMessagesKeyValueObservingContext) {
        
        if (object == self.inputToolbar.contentView.textView
            && [keyPath isEqualToString:NSStringFromSelector(@selector(contentSize))]) {
            
            CGSize oldContentSize = [[change objectForKey:NSKeyValueChangeOldKey] CGSizeValue];
            CGSize newContentSize = [[change objectForKey:NSKeyValueChangeNewKey] CGSizeValue];
            
            CGFloat dy = newContentSize.height - oldContentSize.height;
            
            [self adjustInputToolbarForComposerTextViewContentSizeChange:dy];
            [self updateCollectionViewInsets];
            if(self.automaticallyScrollsToMostRecentMessage) {
                [self scrollToBottomAnimated:false hideCollectionVewWhileScrolling:false];
            }
        }
    }
}


#pragma mark - Keyboard controller delegate
#pragma mark -
- (void)setToolbarBottomYPosition:(CGFloat)yPosition
{
    CGFloat yPos = 0;
    if(yPosition == 0) {
        yPos = self.view.frame.size.height;
    }
    else {
        yPos = yPosition;
    }
    //NSLog(@"Setting yPos:%f",yPosition);
    self.inputToolbar.frame = CGRectMake(self.inputToolbar.frame.origin.x,
                                         yPos+32,
                                         self.inputToolbar.frame.size.width,
                                         self.inputToolbar.frame.size.height);
}

- (void)updateKeyboardTriggerPoint
{
    self.keyboardController.keyboardTriggerPoint = CGPointMake(0.0f, CGRectGetHeight(self.inputToolbar.bounds));
}

#pragma mark - Input toolbar utilities
#pragma mark -
- (void) fireChatTyping {
    BOOL canFireChatTypingEvent = FALSE;
    
    if (self.lastChatTypeDate) {
        if ([[NSDate date] timeIntervalSinceDate:self.lastChatTypeDate] >= CHAT_TYPING_TIMEOUT) {
            canFireChatTypingEvent = TRUE;
        }
    }
    else {
        canFireChatTypingEvent = TRUE;
    }
    
    if(canFireChatTypingEvent) {
        [[TMMessagingController sharedController] fireChatTyping];
        //update the chat typing timestamp
        self.lastChatTypeDate = [NSDate date];
    }
}
-(void)setupSendActionButtonForTextLength:(NSInteger)textLength {

}
//will be overridden by subclass which will show curated deal view controller
-(void)showCuratedDeal {
    
}
-(void)showPhotoSharingOptions {
    
}
-(void)showQuizView {
    BOOL showQuizView = TRUE;
    if(self.messageInputType == TMInputType_Quiz) {
        showQuizView = FALSE;
    }
    
    //hide the sticker view if it is shown
    if (self.isStickerShown) {
        [self hideStickerView];
    }
    
    ///////
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    CGRect quizFrame = self.quizListView.frame;
    CGFloat inputToolbarYPos = 0.0f;
    CGFloat yPos = 0.0f;
    
    //show quiz view
    if(showQuizView) {
        [self loadQuizView];
        quizFrame = self.quizListView.frame;
        [self.view endEditing:TRUE];
        self.messageInputType = TMInputType_Quiz;
        yPos =  CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(quizFrame);
        inputToolbarYPos = yPos - CGRectGetHeight(self.inputToolbar.frame);
        
        [self.tapGrestureRecognizer addTarget:self action:@selector(tapAction)];
        
        self.quizListView.frame = CGRectMake(quizFrame.origin.x,
                                             yPos,
                                             quizFrame.size.width,
                                             quizFrame.size.height);
        
        [self.quizListView showQuizGallery];
    }
    else { ///hide sticker view
        self.messageInputType = TMInputType_None;
        
        //yPos =  CGRectGetMaxY(maxUsableFrame);
        inputToolbarYPos = CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(self.inputToolbar.frame);
        
        [self.quizListView removeFromSuperview];
        self.isQuizShown = NO;
    }
    
    [self setToolbarBottomPosition:inputToolbarYPos];
}

/**
 * Abhijeet
 * hide the quiz view
 */
- (void) hideQuizView
{
    CGFloat inputToolbarYPos = 0.0f;
    //CGFloat yPos = 0.0f;
    
    self.messageInputType = TMInputType_None;
    
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    
    //yPos =  CGRectGetMaxY(maxUsableFrame);
    inputToolbarYPos = CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(self.inputToolbar.frame);
    
    [self.quizListView removeFromSuperview];
    self.isQuizShown = NO;
    [self setToolbarBottomPosition:inputToolbarYPos];
}

-(void)loadQuizView{
    //add quiz view
    CGRect maxUsableRect = [self getMaxUsableFrame];
    CGFloat quizViewCellWidth = floor(CGRectGetWidth(self.view.frame)/(3.5));
    CGFloat quizViewWidth = CGRectGetWidth(maxUsableRect);
    CGFloat quizViewHeight = 6 * floor(CGRectGetWidth(self.view.frame)/9);
    CGFloat quizViewCellHeight = floor(quizViewHeight/(2.15));
    CGFloat yPos = CGRectGetMaxY(maxUsableRect);
    CGFloat xPos = CGRectGetMinX(maxUsableRect);
    self.quizListView = [[TMQuizListView alloc] initWithFrame:CGRectMake(xPos,
                                                                         yPos,
                                                                         quizViewWidth,
                                                                         quizViewHeight)
                                                 withCellSize:CGSizeMake(quizViewCellWidth, quizViewCellHeight) quizContoller:self.quizController];
    
    self.quizListView.delegate = self;
    self.quizListView.tag = QUIZ_VIEW_TAG;
    
    //display the quiz list only for the first quiz button press for a particular conversation
    // [self.quizController fetchPlayedQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
    [self.view addSubview:self.quizListView];
    self.isQuizShown = YES;
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    
    //adding event tracking for quiz click action
    [self.eventDict setObject:@"TMQuizChatView" forKey:@"screenName"];
    [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
    [self.eventDict setObject:@"true" forKey:@"GA"];
    [self.eventDict setObject:@"quiz_icon_click_call" forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
}

-(void)showStickerView {
    //end editing if keyboard is visible then it will hide
    
    BOOL showStickerView = TRUE;
    if(self.messageInputType == TMInputType_Sticker) {
        showStickerView = FALSE;
    }
    
    //if quiz view is shown then hide it
    if (self.isQuizShown) {
        [self hideQuizView];
    }
    
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    CGRect stickerFrame = self.stickerListView.frame;
    CGFloat inputToolbarYPos = 0.0f;
    CGFloat yPos = 0.0f;
    
    //show sticker view
    if(showStickerView) {
        [self.view endEditing:TRUE];
        self.messageInputType = TMInputType_Sticker;
        yPos =  CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(stickerFrame);
        inputToolbarYPos = yPos - CGRectGetHeight(self.inputToolbar.frame);
        
        [self.tapGrestureRecognizer addTarget:self action:@selector(tapAction)];
        
        self.isStickerShown = YES;
       
        dispatch_async(dispatch_get_main_queue(), ^{
            //set sticker view
            [self.stickerListView reloadStickerGallery];
        });
    }
    else { ///hide sticker viewx
        self.messageInputType = TMInputType_None;
        
        yPos =  CGRectGetMaxY(maxUsableFrame);
        inputToolbarYPos = CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(self.inputToolbar.frame);
        self.isStickerShown = NO;
    }
    
    self.stickerListView.frame = CGRectMake(stickerFrame.origin.x,
                                            yPos,
                                            stickerFrame.size.width,
                                            stickerFrame.size.height);
    
    [self setToolbarBottomPosition:inputToolbarYPos];
}


- (void) hideStickerView
{
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    CGRect stickerFrame = self.stickerListView.frame;
    CGFloat inputToolbarYPos = 0.0f;
    CGFloat yPos = 0.0f;
    
    ///hide sticker viewx
    self.messageInputType = TMInputType_None;
    
    yPos =  CGRectGetMaxY(maxUsableFrame);
    inputToolbarYPos = CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(self.inputToolbar.frame);
    
    self.stickerListView.frame = CGRectMake(stickerFrame.origin.x,
                                            yPos,
                                            stickerFrame.size.width,
                                            stickerFrame.size.height);
    
    [self setToolbarBottomPosition:inputToolbarYPos];
}

#pragma mark - Keyboard controller delegate

- (void)keyboardControllerKeyboardWillStopPanning:(TMKeyboardController*)keyboardController {
    CGFloat heightFromBottom = CGRectGetHeight(self.collectionView.frame);
    
    if(self.isScrollingKeyboardOffsetAdded && !self.keyboardController.isKeyboardScrolling) {
        self.isScrollingKeyboardOffsetAdded = false;
        [self setToolbarBottomPosition:heightFromBottom];
    }
}
- (void)keyboardController:(TMKeyboardController*)keyboardController keyboardDidChangeFrame:(CGRect)keyboardFrame
{
    if (![self.inputToolbar.contentView.textView isFirstResponder]) {
        return;
    }
    
    if(self.messageInputType == TMInputType_Sticker) {
        [self showStickerView];
    }
    
    self.messageInputType = TMInputType_Keyboard;
    
    CGFloat heightFromBottom = CGRectGetMinY(keyboardFrame) - CGRectGetHeight(self.inputToolbar.frame);
    
    TMLOG(@"KFY:%f___IT:%f___HFB:%f",CGRectGetMinY(keyboardFrame),CGRectGetHeight(self.inputToolbar.frame),heightFromBottom);
    if(self.keyboardController.isKeyboardScrolling) {
        self.isScrollingKeyboardOffsetAdded = true;
        heightFromBottom = heightFromBottom - 54;
    }
    
    heightFromBottom = MIN(self.collectionView.frame.size.height, heightFromBottom);
    
    [self setToolbarBottomPosition:heightFromBottom];
    
    //update the typing indicator view frame
    [self updateTypingIndicatorViewFrame];
}
- (void)keyboardControllerKeyboardDidHide:(TMKeyboardController*)keyboardController
{
    if (![self.inputToolbar.contentView.textView isFirstResponder]) {
        return;
    }
    self.messageInputType = TMInputType_None;
    [self.inputToolbar.contentView.textView resignFirstResponder];
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    CGFloat heightFromBottom = CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(self.inputToolbar.frame);
    [self setToolbarBottomPosition:heightFromBottom];
}

-(void)setToolbarBottomPosition:(CGFloat)bottonYPositin {
    TMLOG(@"yPos:%f",bottonYPositin);
    
    self.inputToolbar.frame = CGRectMake(self.inputToolbar.frame.origin.x,
                                         bottonYPositin,
                                         self.inputToolbar.frame.size.width,
                                         self.inputToolbar.frame.size.height);
    
    
    TMLOG(@"%@",NSStringFromCGRect(self.inputToolbar.frame));
    
    [self updateCollectionViewInsets];
}

-(void)updateCollectionViewInsets {
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    CGFloat minYPos = (CGRectGetMinY(self.inputToolbar.frame));
    [self setCollectionViewInsetsTopValue:0 bottomValue:CGRectGetMaxY(maxUsableFrame)-minYPos-54];
    [self scrollToBottomAnimated:false hideCollectionVewWhileScrolling:false];
}

- (BOOL)inputToolbarHasReachedMaximumHeight
{
    return (CGRectGetMinY(self.inputToolbar.frame) == 0);
}

- (void)adjustInputToolbarForComposerTextViewContentSizeChange:(CGFloat)dy
{
    BOOL contentSizeIsIncreasing = (dy > 0);
    
    if ([self inputToolbarHasReachedMaximumHeight]) {
        BOOL contentOffsetIsPositive = (self.inputToolbar.contentView.textView.contentOffset.y > 0);
        
        if (contentSizeIsIncreasing || contentOffsetIsPositive) {
            [self scrollComposerTextViewToBottomAnimated:YES];
            return;
        }
    }
    
    CGFloat toolbarOriginY = CGRectGetMinY(self.inputToolbar.frame);
    CGFloat newToolbarOriginY = toolbarOriginY - dy;
    
    //  attempted to increase origin.Y above topLayoutGuide
    if (newToolbarOriginY <= 0) {
        dy = toolbarOriginY - (self.topLayoutGuide.length + self.topContentAdditionalInset);
        [self scrollComposerTextViewToBottomAnimated:YES];
    }
    
    [self adjustInputToolbarHeightConstraintByDelta:dy];
    
    [self updateKeyboardTriggerPoint];
    
    if (dy < 0) {
        [self scrollComposerTextViewToBottomAnimated:NO];
    }
}

- (void)adjustInputToolbarHeightConstraintByDelta:(CGFloat)dy
{
    //    self.
    //    [self.view setNeedsUpdateConstraints];
    [self.view layoutIfNeeded];
}

- (void)scrollComposerTextViewToBottomAnimated:(BOOL)animated
{
    UITextView *textView = self.inputToolbar.contentView.textView;
    CGPoint contentOffsetToShowLastLine = CGPointMake(0.0f, textView.contentSize.height - CGRectGetHeight(textView.bounds));
    
    if (!animated) {
        textView.contentOffset = contentOffsetToShowLastLine;
        return;
    }
    
    [UIView animateWithDuration:0.01
                          delay:0.01
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         textView.contentOffset = contentOffsetToShowLastLine;
                     }
                     completion:nil];
}

#pragma mark - Collection view utilities

- (void)setCollectionViewInsetsTopValue:(CGFloat)top bottomValue:(CGFloat)bottom
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    insets = UIEdgeInsetsMake(top, 0.0f, bottom, 0.0f);
    self.collectionView.contentInset = insets;
    self.collectionView.scrollIndicatorInsets = insets;
}

#pragma mark - TMQuizListViewDelegate delegate methods

-(void)didClickOnQuiz:(NSArray*)quizData
{
    //hide the quiz view
    [self hideQuizView];
}

- (void) didClickOnNudgeQuiz
{
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Ready, Set, Quiz..."];
}

#pragma mark - Utilities

- (void)addObservers
{
    if (self.isObserving) {
        return;
    }
    
    [self.inputToolbar.contentView.textView addObserver:self
                                             forKeyPath:NSStringFromSelector(@selector(contentSize))
                                                options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                                                context:kJSQMessagesKeyValueObservingContext];
    
    self.isObserving = YES;
}

- (void)removeObservers
{
    if (!_isObserving) {
        return;
    }
    
    @try {
        [_inputToolbar.contentView.textView removeObserver:self
                                                forKeyPath:NSStringFromSelector(@selector(contentSize))
                                                   context:kJSQMessagesKeyValueObservingContext];
    }
    @catch (NSException * __unused exception) { }
    
    _isObserving = NO;
}

#pragma mark - Messaging Controller

-(TMMessagingController*)messagingController {
    TMMessagingController *messagingController = [TMMessagingController sharedController];
    return messagingController;
}

#pragma mark - TMMessagingCollectionViewCell Delegate

-(void)didTapCellAtIndex:(NSInteger)selectedIndex {
    if(self.messageInputType == TMInputType_Keyboard) {
        [self.view endEditing:TRUE];
    }
    else if(self.messageInputType == TMInputType_Sticker) {
        [self showStickerView];
    }
    else if(self.messageInputType == TMInputType_Quiz) {
        [self hideQuizView];
    }
    
    self.messageInputType = TMInputType_None;
}
////for failed messages
-(void)didSelectSentFailedMessageCellAtIndex:(NSInteger)selectedIndex {
    if(!self.actionSheet) {
        TMMessage *messageItem = [self messageAtIndex:selectedIndex];
        if(messageItem.deliveryStatus == SENDING_FAILED) {
            TMLOG(@"didLongPressCellAtIndex");
            self.failedMessage = messageItem;
            self.failedMessageIndex = selectedIndex;
            [self tapAction];
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Your message was not sent"delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Try Again",@"Delete", @"Cancel",nil];
            self.actionSheet.tag = FAILED_MESSAGE_ACTIONSHEET_TAG;
            [self.actionSheet showInView:self.view];
        }
    }
}
/////
-(void)didSelectMediaCell:(NSInteger)selectedIndex {
    TMMessage *messageItem = [self messageAtIndex:selectedIndex];
    if(messageItem.quizId && [messageItem isQuizMessage]) {
        if (!messageItem.isOutgoingMessage && ([self.quizController getQuizStatusFromDBForQuizID:messageItem.quizId] == quizPlayedByUser || [self.quizController getQuizStatusFromDBForQuizID:messageItem.quizId] == quizPlayedByNeither)) {
            //incoming quiz message
            //with status as quiz played by neither or user
            //updated db status
            // [self.quizController fetchPlayedQuizzesForMatchID:[[TMMessagingController sharedController] getCurrentMatchId]];
        }
        
        TMQuizController *quizController = self.quizController;
        if (messageItem.quizId) {
            //dismiss the toolbar keypad
            [self.inputToolbar.contentView.textView resignFirstResponder];
            if ([quizController getQuizStatusFromDBForQuizID:messageItem.quizId] == quizPlayedByUser) {
                [self didClickOnNudgeQuiz];
            }
        }
        
        [quizController decideQuizActionFromChatForQuizID:messageItem.quizId withBaseViewController:self];
        if (messageItem.type == MESSAGETTYPE_QUIZ_NUDGE_SINGLE || messageItem.type == MESSAGETTYPE_QUIZ_NUDGE_DOUBLE) {
            self.eventDict = [[NSMutableDictionary alloc] init];
            NSString* eventAction = (messageItem.type == MESSAGETTYPE_QUIZ_NUDGE_SINGLE)?@"quiz_item_in_chat_clicked_call_play_quiz":@"quiz_item_in_chat_clicked_call_view_answers";
            
            //adding event tracking for common answers
            [self.eventDict setObject:@"TMQuizChatView" forKey:@"screenName"];
            [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
            [self.eventDict setObject:messageItem.quizId forKey:@"label"];
            [self.eventDict setObject:messageItem.quizId forKey:@"status"];
            [self.eventDict setObject:eventAction forKey:@"eventAction"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
        }
    }
    else if(messageItem.datespotId && [messageItem isCuratedDealMessage] && (messageItem.deliveryStatus == SENT)) {
        [self handleTapActionOnCuratedDealMessage:messageItem];
        
    }
    else if([messageItem isPhotoMessage]) {
        [self handleTapActionOnPhotoShareMessage:messageItem];
    }
    else if([messageItem isImageLinkMessage]) {
        [self handleTapActionOnImageLinkMessage:messageItem];
    }
    else if([messageItem isShareEventMessage]){
        [self handleTapActionOnShareEventMessage:messageItem];
    }
}

-(void)handleTapActionOnCuratedDealMessage:(TMMessage*)messageItem {
    
}
-(void)handleTapActionOnPhotoShareMessage:(TMMessage*)messageItem {
    
}
-(void)handleTapActionOnImageLinkMessage:(TMMessage*)messageItem {
    
}
-(void)handleTapActionOnShareEventMessage:(TMMessage*)messageItem {
    
}

#pragma mark - Actionsheet delegate
#pragma mark -
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == FAILED_MESSAGE_ACTIONSHEET_TAG) {
        [self handleFailedMessageActionSheetActionForButtonIndex:buttonIndex];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(actionSheet.tag == FAILED_MESSAGE_ACTIONSHEET_TAG) {
        self.actionSheet = nil;
        self.failedMessage = nil;
    }
}

-(void)handleFailedMessageActionSheetActionForButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        TMMessage *message = self.failedMessage;
        message.isFailedMessageRetry = TRUE;
        if(!self.showDeliveryStatus) {
            message.isAdmin = TRUE;
        }
        message.sparkAcceptanceMessageHash = self.messageConfiuration.sparkHash;
        [[self messagingController] sendMessage:message];
    }
    else if(buttonIndex == 1) { //Delete
        [[self messagingController] deleteMessage:self.failedMessage];
        [self.conversations removeObjectAtIndex:self.failedMessageIndex];
        [self.collectionView reloadData];
    }
}

@end

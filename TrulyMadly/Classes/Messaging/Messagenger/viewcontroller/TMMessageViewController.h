//
//  TMMessageViewController.h
//  TrulyMadly
//
//  Created by Ankit on 30/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"
#import "TMKeyboardController.h"
#import "TMMessageConfiguration.h"
#import "TMMessagesCollectionView.h"
#import "TMMessagesInputToolbar.h"
#import "TMQuizController.h"


#define FAILED_MESSAGE_ACTIONSHEET_TAG  150
#define MORE_OPTION_ACTIONSHEET_TAG     151
#define PHOTOSHARING_ACTIONSHEET_TAG    152

@class TMMessagingController;
@class TMSpark;

@interface TMMessageViewController : TMBaseViewController

@property(nonatomic,strong) TMMessagesCollectionView *collectionView;

@property(nonatomic,strong) TMMessageConfiguration *messageConfiuration;
@property(nonatomic,strong) NSMutableArray *conversations;
@property(nonatomic,strong) NSMutableDictionary *shownDateIndexes;
@property(nonatomic,strong) NSMutableDictionary *trackDict;
@property(nonatomic,strong) NSDate *lastShownDate;
@property(nonatomic,strong) TMMessagesInputToolbar *inputToolbar;
@property(nonatomic,strong) TMKeyboardController *keyboardController;
@property(strong,nonatomic) TMQuizController* quizController;
@property(nonatomic,strong) NSString *lastSeenMsgTs;
@property(assign,nonatomic) BOOL automaticallyScrollsToMostRecentMessage;
@property(nonatomic,assign) CGFloat topContentAdditionalInset;
@property(nonatomic,assign) BOOL isBack;
@property(nonatomic,assign) BOOL showDeliveryStatus;
@property(nonatomic,assign) BOOL sendInProgress;
@property(nonatomic,assign) BOOL isSparkConvsersation;

-(TMMessagingController*)messagingController;
-(void)updateKeyboardTriggerPoint;
-(void)scrollToBottomAnimated:(BOOL)animated hideCollectionVewWhileScrolling:(BOOL)hidden;
-(void)finishSendingMessageAnimated:(BOOL)animated;
-(void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                      date:(NSDate *)date;
-(void)tapAction;
-(void)handleFailedMessageActionSheetActionForButtonIndex:(NSInteger)buttonIndex;
- (void)hideTypingIndicatorDueToIncomingMessageOrNoConnectionStatus;
-(void)setupSendActionButtonForTextLength:(NSInteger)textLength;
-(void)setupInputToolbar;

@end

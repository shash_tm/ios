//
//  TMImageLinkContext.m
//  TrulyMadly
//
//  Created by Ankit on 30/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMImageLinkContext.h"

@implementation TMImageLinkContext

-(instancetype)initWithMetaParams:(NSDictionary*)metaParams {
    self = [super init];
    if(self) {
        self.imageURLString = metaParams[@"link_image_url"];
        self.landingURLString = metaParams[@"link_landing_url"];
    }
    return self;
}
@end

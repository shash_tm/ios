//
//  TMQuizMediaItem.m
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizMediaItem.h"
#import "TMQuizMessageView.h"
#import "TMMessagesMediaViewBubbleImageMasker.h"
#import "UIColor+TMColorAdditions.h"


@interface TMQuizMediaItem ()

@property (strong, nonatomic) TMQuizMessageView *quizMessageViewView;
@property (assign, nonatomic) CGSize size;

@end

@implementation TMQuizMediaItem

- (instancetype)initWithQuizMessage:(NSString*)quizMessage
                        isOutgoingMessage:(BOOL)outgoingMessage {
    self = [super init];
    if(self) {
        self.quizMessageViewView = [[TMQuizMessageView alloc] initWithFrame:CGRectZero outgoingMessageView:outgoingMessage];
        self.quizMessageViewView.backgroundColor = self.appliesMediaViewMaskAsOutgoing?[UIColor likeunSelectedColor]:[UIColor likeselectedColor];
        [self.quizMessageViewView setTitleText:quizMessage];
        [self computeSize];
    }
    return self;
}

- (void)dealloc
{
    _quizMessageViewView = nil;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    _quizMessageViewView = nil;
}
-(void)computeSize {
    CGFloat xWidth = (5+40+5+5+10); //(left margin(5)+imageview width(40)+margin between imageview and lable(5))
    CGFloat defaultHeight = 50; //(top margin(5)+imageview height(40)+bottom margin(5))
    CGFloat maximumTextWidth = 244 - xWidth; //(max alowed width - (quiz icon size + x pos margin))
    CGFloat topbottomMargin = 20; //(top margin(5)+bottom margin(5))
    CGRect stringRect = [self.quizMessageViewView.titleText boundingRectWithSize:CGSizeMake(maximumTextWidth, CGFLOAT_MAX)
                                                                         options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                                      attributes:@{ NSFontAttributeName : [self.quizMessageViewView textFont] }
                                                                         context:nil];
    
    self.quizMessageViewView.textSize = CGRectIntegral(stringRect).size;
    CGSize stringSize = CGRectIntegral(stringRect).size;
    /////////
    CGFloat finalHeight = (stringSize.height < defaultHeight) ? defaultHeight : stringSize.height+topbottomMargin;
    CGFloat finalWidth = xWidth+stringSize.width;
    //NSLog(@"Qiiz View Size:%@",NSStringFromCGSize(CGSizeMake(finalWidth, finalHeight)));
    self.size = CGSizeMake(finalWidth, finalHeight);
}
- (CGSize)mediaViewDisplaySize
{
    return self.size;
}

#pragma mark - Setters

//- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
//{
//    [super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
//    _quizMessageViewView = nil;
//}

#pragma mark - Getters

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    self.quizMessageViewView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
    return self.quizMessageViewView;
}

-(void)updateMask {
    if (!self.isMaskCreated) {
        self.isMaskCreated = TRUE;
        [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:self.quizMessageViewView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
    }
}
-(void)setImageURLString:(NSString*)imageURLString {
    [self.quizMessageViewView setBannerURLString:imageURLString];
}

- (void) setIsFlare:(BOOL) isFlare {
    [self.quizMessageViewView setIsFlare:isFlare];
}

- (void) setNudgeBackgroundColor:(UIColor*) color {
    [self.quizMessageViewView setBackgroundColor:color];
}

#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.quizMessageViewView.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.quizMessageViewView, @(self.appliesMediaViewMaskAsOutgoing)];
}


@end

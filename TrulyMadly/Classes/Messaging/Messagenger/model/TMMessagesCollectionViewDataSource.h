//
//  TMMessagesCollectionViewDataSource.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class TMMessagesCollectionView;
@protocol TMMessageData;
@protocol TMMessageBubbleImageDataSource;


@protocol TMMessagesCollectionViewDataSource <UICollectionViewDataSource>

@required

- (id<TMMessageData>)collectionView:(TMMessagesCollectionView *)collectionView
      messageDataForItemAtIndexPath:(NSIndexPath *)indexPath;

- (id<TMMessageBubbleImageDataSource>)collectionView:(TMMessagesCollectionView *)collectionView
            messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath;

- (CGSize)collectionView:(TMMessagesCollectionView *)collectionView
                  sentFailedIndicatorSizeAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (NSString *)senderId;

- (NSAttributedString *)collectionView:(TMMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath;

- (NSAttributedString *)collectionView:(TMMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath;

@end

//
//  TMImageContext.h
//  TrulyMadly
//
//  Created by Ankit on 16/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMPhotoShareContext : NSObject

@property(nonatomic,strong)NSString *imageName;
@property(nonatomic,strong)NSString *imageCachePath;
@property(nonatomic,strong)NSString *jpegImageURLString;
@property(nonatomic,strong)NSString *jpegImageSize;
@property(nonatomic,strong)NSString *webPImageURLString;
@property(nonatomic,strong)NSString *webPImageSize;
@property(nonatomic,strong)NSString *thumbnailImageURLString;
@property(nonatomic,strong)NSString *thumbnailImageName;
@property(nonatomic,strong)NSString *thumbnailImageCachePath;
@property(nonatomic,strong)NSString *photoUploadProgressKey;

@property(nonatomic,assign)BOOL uploadInProgress;
@property(nonatomic,assign)BOOL downloadInProgress;

-(instancetype)initWithParams:(NSDictionary*)params senderId:(NSInteger)senderId receiverId:(NSInteger)receiverId;
-(void)cacheImageData:(NSData*)imageData;
-(void)cacheThumbnailImage:(UIImage*)image;
-(void)cacheFullJpegImage:(UIImage*)image;

-(UIImage*)getThumbnailImage;
-(UIImage*)getCachedSentImage;
-(UIImage*)getCachedJpegImage;

-(void)downloadAndCacheMainImage;

-(void)deleteCachedImage;


////////
//-(void)downloadAndCacheMainImage;
//-(NSData*)getCacheImageData;
//-(UIImage*)getFullImage;
//-(void)cacheImageData1:(NSData*)imageData;
//-(UIImage*)getCacheImageWithLocalName;
//-(NSData*)getCacheImageDataAtPath:(NSString*)path;

@end

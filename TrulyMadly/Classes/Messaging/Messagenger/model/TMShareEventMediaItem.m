//
//  TMShareEventMediaItem.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMShareEventMediaItem.h"
#import "TMMessagesMediaViewBubbleImageMasker.h"
#import "TMShareEventView.h"

@interface TMShareEventMediaItem ()

@property(nonatomic,assign)CGSize size;
@property(nonatomic,assign)TMMessageType messageType;

@property (strong, nonatomic) TMShareEventView *shareEventView;

@end


@implementation TMShareEventMediaItem

-(instancetype)initWithMessageType:(TMMessageType)type failedMessage:(BOOL)isFailedMessage {
    self = [super init];
    if(self) {
        isFailedMessage = isFailedMessage;
        self.messageType = type;
        self.size = CGSizeMake(220, 200);
        
        self.shareEventView = [[TMShareEventView alloc] initWithFrame:CGRectMake(0, 0, self.size.width, self.size.height) isFailedMessage:isFailedMessage];
        self.shareEventView.backgroundColor = [UIColor grayColor];
        
        [self.shareEventView setupUIForASKMessage];
        
    }
    return self;
}

-(CGSize)mediaViewDisplaySize {
    return self.size;
}
-(UIView*)mediaView {
    return self.shareEventView;
}
-(void)updateMask {
    if (!self.isMaskCreated) {
        self.isMaskCreated = TRUE;
        [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:self.shareEventView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
    }
}
-(void)setImageURLString:(NSString*)imageURLString {
    [self.shareEventView setBackgroundImageFromURL:[NSURL URLWithString:imageURLString] isDarkCentered:YES];
}
-(void)setTitleText:(NSString*)titleText {
    self.shareEventView.isOutGoingMessage = self.appliesMediaViewMaskAsOutgoing;
    [self.shareEventView setTitleText:titleText];
}


@end

//
//  TMPhotoMediaItem.h
//  TrulyMadly
//
//  Created by Ankit on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMediaItem.h"

@class TMPhotoShareContext;

@interface TMPhotoMediaItem : TMMediaItem <TMMessageMediaData>

@property(nonatomic,assign)BOOL needFullImageDownload;
@property(assign,nonatomic)NSInteger matchId;
@property(assign,nonatomic)TMMessageDeliveryStatus deliveryStatus;

-(instancetype)initWithContext:(TMPhotoShareContext*)photoshareContext withMessageDeliveryStatus:(TMMessageDeliveryStatus)status;
-(void)downloadFullImage;

-(void)startPhotoUploadProgressTracking;
-(void)stopPhotoUploadProgressTracking;

@end

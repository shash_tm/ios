//
//  TMMediaItem.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 01/07/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMediaItem.h"


@interface TMMediaItem ()

@property (strong, nonatomic) UIView *cachedPlaceholderView;

@end

@implementation TMMediaItem

@synthesize appliesMediaViewMaskAsOutgoing;

#pragma mark - Initialization

- (instancetype)init
{
    return [self initWithMaskAsOutgoing:YES];
}

- (instancetype)initWithMaskAsOutgoing:(BOOL)maskAsOutgoing
{
    self = [super init];
    if (self) {
        self.appliesMediaViewMaskAsOutgoing = maskAsOutgoing;
        _cachedPlaceholderView = nil;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarningNotification:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _cachedPlaceholderView = nil;
}

//- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
//{
////    appliesMediaViewMaskAsOutgoing = appliesMediaViewMaskAsOutgoing;
////    _cachedPlaceholderView = nil;
//}

- (void)clearCachedMediaViews
{
    _cachedPlaceholderView = nil;
}

#pragma mark - Notifications

- (void)didReceiveMemoryWarningNotification:(NSNotification *)notification
{
    [self clearCachedMediaViews];
}

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    NSAssert(NO, @"Error! required method not implemented in subclass. Need to implement %s", __PRETTY_FUNCTION__);
    return nil;
}
-(void)updateMask {
    
}
- (CGSize)mediaViewDisplaySize
{
    return CGSizeMake(60.0f, 60.0f);
}

- (UIView *)mediaPlaceholderView
{
    if (self.cachedPlaceholderView == nil) {
        CGSize size = [self mediaViewDisplaySize];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        view.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        self.cachedPlaceholderView = view;
    }
    return self.cachedPlaceholderView;
}

- (NSUInteger)mediaHash
{
    return self.hash;
}

-(void)setImageURLString:(NSString*)imageURLString {
    
}

- (void) setIsFlare:(BOOL)isFlare {
    
}

- (void) setNudgeBackgroundColor:(UIColor *)color {
    
}

-(void)updateUIState {
        
}

#pragma mark - NSObject

- (BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    
    TMMediaItem *item = (TMMediaItem *)object;
    
    return self.appliesMediaViewMaskAsOutgoing == item.appliesMediaViewMaskAsOutgoing;
}

- (NSUInteger)hash
{
    return [NSNumber numberWithBool:self.appliesMediaViewMaskAsOutgoing].hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: appliesMediaViewMaskAsOutgoing=%@>",
            [self class], @(self.appliesMediaViewMaskAsOutgoing)];
}

- (id)debugQuickLookObject
{
    return [self mediaView] ?: [self mediaPlaceholderView];
}

@end

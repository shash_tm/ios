//
//  TMMessagesBubbleImage.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMMessageBubbleImageDataSource.h"

@interface TMMessagesBubbleImage : NSObject  <TMMessageBubbleImageDataSource>

@property(strong, nonatomic, readonly)UIImage* messageBubbleImage;

- (instancetype)initWithMessageBubbleImage:(UIImage *)image;


@end

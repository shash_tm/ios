//
//  TMImageLinkMediaItem.m
//  TrulyMadly
//
//  Created by Ankit on 30/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMImageLinkMediaItem.h"
#import "TMImageLinkMessageView.h"
#import "TMMessagesMediaViewBubbleImageMasker.h"

@interface TMImageLinkMediaItem ()

@property(strong,nonatomic)TMImageLinkMessageView *messageView;
@property(assign,nonatomic)CGSize size;

@end


@implementation TMImageLinkMediaItem

-(instancetype)init {
    self = [super init];
    if(self) {
        self.messageView = [[TMImageLinkMessageView alloc] initWithFrame:CGRectZero];
        self.size = CGSizeMake(200, 200);
    }
    return self;
}

- (void)dealloc
{
    self.messageView = nil;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    self.messageView = nil;
}

- (CGSize)mediaViewDisplaySize
{
    return self.size;
}

#pragma mark - Getters

-(TMImageLinkMessageView*)messageView {
    if(!_messageView) {
        _messageView = [[TMImageLinkMessageView alloc] initWithFrame:CGRectZero];
    }
    return _messageView;
}
#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    self.messageView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
    return self.messageView;
}

-(void)updateMask {
    if (!self.isMaskCreated) {
        self.isMaskCreated = TRUE;
        [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:self.messageView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
    }
}

-(void)setImageLinkFromURLString:(NSString*)URLString {
    [self.messageView setupViewWithImageURL:[NSURL URLWithString:URLString]];
}


#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.messageView.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.messageView, @(self.appliesMediaViewMaskAsOutgoing)];
}



@end

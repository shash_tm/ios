//
//  TMQuizMediaItem.h
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMediaItem.h"

@interface TMQuizMediaItem : TMMediaItem <TMMessageMediaData>

- (instancetype)initWithQuizMessage:(NSString*)quizMessage
                  isOutgoingMessage:(BOOL)outgoingMessage;
-(void)updateMask;

@end

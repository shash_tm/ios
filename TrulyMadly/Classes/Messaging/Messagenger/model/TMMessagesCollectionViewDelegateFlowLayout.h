//
//  TMMessagesCollectionViewDelegateFlowLayout.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMMessagesCollectionView;
@class TMMessagesCollectionViewFlowLayout;

@protocol TMMessagesCollectionViewDelegateFlowLayout <UICollectionViewDelegateFlowLayout>

@optional

- (CGFloat)collectionView:(TMMessagesCollectionView *)collectionView
                   layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath;

- (CGFloat)collectionView:(TMMessagesCollectionView *)collectionView
                   layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath;

- (CGSize)collectionView:(TMMessagesCollectionView *)collectionView
                   layout:(TMMessagesCollectionViewFlowLayout *)collectionViewLayout sizeForSentFailedMessageIndicatorViewAtIndexPath:(NSIndexPath *)indexPath;

@end

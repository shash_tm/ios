//
//  TMImageContext.m
//  TrulyMadly
//
//  Created by Ankit on 16/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMPhotoShareContext.h"
#import "NSString+TMAdditions.h"
#import "TMImageDownloader.h"
#import "TMFileManager.h"
#import "TMUserSession.h"


@interface TMPhotoShareContext ()

@property(nonatomic,assign)NSInteger senderId;
@property(nonatomic,assign)NSInteger receiverId;
@end

@implementation TMPhotoShareContext

-(instancetype)initWithParams:(NSDictionary*)params senderId:(NSInteger)senderId receiverId:(NSInteger)receiverId {
    self = [super init];
    if(self) {
        self.senderId = senderId;
        self.receiverId = receiverId;
        self.imageName = params[@"imagename"];
        self.jpegImageURLString = params[@"jpeg"];
        self.jpegImageSize = params[@"jpeg_size"];
        self.thumbnailImageURLString = params[@"thumbnail_jpeg"];
        
        //[self configureNotification];
    }
    return self;
}

-(void)dealloc {
    [self removeNotificationObserver];
}
-(void)configureNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadCompleteAction:) name:TMImageDownloadCompleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadFailAction:) name:TMImageDownloadFailNotification object:nil];
}
-(void)removeNotificationObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/////////////////////////////////////////////////////////////
-(void)cacheFullJpegImage:(UIImage*)image {
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    [[TMImageDownloader sharedImageDownloader] cacheImageData:imageData
                                                       atPath:[self messageImageCachePath]
                                                     withName:[self.jpegImageURLString getLastPathComponentFromURLString]];
}
-(void)cacheThumbnailImage:(UIImage*)image {
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    [[TMImageDownloader sharedImageDownloader] cacheImageData:imageData
                                                       atPath:[self messageImageCachePath]
                                                     withName:[self.thumbnailImageURLString getLastPathComponentFromURLString]];
}
-(void)cacheImageData:(NSData*)imageData {
    [[TMImageDownloader sharedImageDownloader] cacheImageData:imageData
                                                       atPath:[self messageImageCachePath]
                                                     withName:self.imageName];
}

///////////////////////////////////////////////
-(UIImage*)getThumbnailImage {
    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",[self messageImageCachePath],
                                                              [self.thumbnailImageURLString getLastPathComponentFromURLString]];
    UIImage *imageData = [[TMImageDownloader sharedImageDownloader] imageAtPath:imagePath];
    return imageData;
}

-(UIImage*)getCachedSentImage {
    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",[self messageImageCachePath],self.imageName];
    UIImage *image = [[TMImageDownloader sharedImageDownloader] imageAtPath:imagePath];
    if(!image) {
        image = [self getCachedJpegImage];
    }
    return image;
}

-(UIImage*)getCachedJpegImage {
    NSString *fullImagePath = [NSString stringWithFormat:@"%@/%@",[self messageImageCachePath],
                                                                  [self.jpegImageURLString getLastPathComponentFromURLString]];
    UIImage *image = [[TMImageDownloader sharedImageDownloader] imageAtPath:fullImagePath];
    return image;
}

-(NSString*)messageImageCachePath {
    NSString *imageCachePath = [NSString stringWithFormat:@".images/conversation/%ld-%ld",(long)[self userId],(long)[self matchId]];
    return imageCachePath;
}

-(NSInteger)userId {
    return [TMUserSession sharedInstance].user.userId.integerValue;
}

-(NSInteger)matchId {
    NSInteger userId = [TMUserSession sharedInstance].user.userId.integerValue;
    if(userId == self.senderId) {
        return self.receiverId;
    }
    else {
        return self.senderId;
    }
}


/////////////////////////////////////////////////
-(void)downloadAndCacheMainImage {
    [self configureNotification];
    TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
    NSString *URLString = self.jpegImageURLString;
    NSString *imageCachePath = [NSString stringWithFormat:@"%@/%@",[self messageImageCachePath],
                           [URLString getLastPathComponentFromURLString]];
    [imageDownloader downloadImageFromUrl:[NSURL URLWithString:URLString]
                              cacheAtPath:imageCachePath];
}


/////////////////////////////////////////////////
-(void)deleteCachedImage {
    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",[self messageImageCachePath],self.imageName];
    [TMFileManager removeFileAtPath:imagePath];
}


/////////////////////////////////////////////////
#pragma mark Notification Handler
#pragma mark -

-(void)imageDownloadCompleteAction:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    UIImage *downloadedImage = [userInfo objectForKey:TMDownloadedImage];
    NSURL *downloadedImageURL = userInfo[TMImageDownloadUrl];
    NSString *downloadedImageURLString = [downloadedImageURL absoluteString];
    if([downloadedImageURLString isEqualToString:self.jpegImageURLString]) {
        //NSLog(@"Image Downloaded");
        [self cacheFullJpegImage:downloadedImage];
        ////fire notification
    }
//    NSDictionary *userInfoDict = @{@"image":downloadedImage};
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"jpegimagenotificatiob"
//                                                        object:nil
//                                                      userInfo:userInfoDict];
}
-(void)imageDownloadFailAction:(NSNotification*)notification {
//    [[NSNotificationCenter defaultCenter] postNotificationName:notification
//                                                        object:nil
//                                                      userInfo:userInfoDictionary];
}


//////////////////////////////
//-(void)cacheImageData1:(NSData*)imageData  {
////    [[TMImageDownloader sharedImageDownloader] cacheImageData:imageData
////                                                       atPath:self.fullImageCachePath
////                                                     withName:self.imageName];
//}

//-(NSString*)getFullImagePath {
//    NSString *path = nil;
////    if(self.fullImageCachePath) {
////        path = self.fullImageCachePath;
////    }
////    else {
////        path = [self.fullJpegImageURLString getLastPathComponentFromURLString];
////    }
//    return path;
//}


@end

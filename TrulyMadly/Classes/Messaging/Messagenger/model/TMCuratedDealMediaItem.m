//
//  TMCuratedDealMediaItem.m
//  TrulyMadly
//
//  Created by Ankit on 30/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMCuratedDealMediaItem.h"
#import "TMMessagesMediaViewBubbleImageMasker.h"
#import "TMDealMessageView.h"

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

@interface TMCuratedDealMediaItem ()

@property(nonatomic,assign)CGSize size;
@property(nonatomic,assign)TMMessageType dealMessageType;

@property (strong, nonatomic) TMDealMessageView *dealMessageView;

@end

@implementation TMCuratedDealMediaItem

-(instancetype)initWithMessageType:(TMMessageType)type failedMessage:(BOOL)isFailedMessage {
    self = [super init];
    if(self) {
        isFailedMessage = FALSE;
        self.dealMessageType = type;
        if(type == MESSAGETTYPE_CD_ASK) {
            self.size = CGSizeMake(220, 200);
        }
        else {
            CGSize failedMessageSizeOffset = (isFailedMessage) ? CGSizeMake(90,60) : CGSizeZero;
            CGSize size = [UIScreen mainScreen].bounds.size;
            int requiredHeight = (IS_IPHONE_6_PLUS?(242):(IS_IPHONE_6?222:([UIScreen mainScreen].bounds.size.height > 500)?246:216));
            self.size = CGSizeMake(size.width-20-failedMessageSizeOffset.width    ,requiredHeight-failedMessageSizeOffset.height);
        }
        self.dealMessageView = [[TMDealMessageView alloc] initWithFrame:CGRectMake(0, 0, self.size.width, self.size.height) isFailedMessage:isFailedMessage];
        self.dealMessageView.backgroundColor = [UIColor grayColor];
        
        if(type == MESSAGETTYPE_CD_ASK) {
            [self.dealMessageView setupUIForASKMessage];
        }
        else {
            [self.dealMessageView setupUIForVoucherKMessage];
        }
    }
    return self;
}

-(void)setImageURLString:(NSString*)imageURLString {
    [self.dealMessageView setBackgroundImageFromURL:[NSURL URLWithString:imageURLString] isDarkCentered:(self.dealMessageType == MESSAGETTYPE_CD_ASK)?YES:NO];
}
-(void)setTitleText:(NSString*)titleText subTitleText:(NSString*)subTitle {
    self.dealMessageView.isOutGoingMessage = self.appliesMediaViewMaskAsOutgoing;
    if(self.dealMessageType == MESSAGETTYPE_CD_ASK) {
        [self.dealMessageView setTitleText:titleText subTitleText:nil];
    }
    else {
        [self.dealMessageView setTitleText:titleText subTitleText:subTitle];
    }
}
-(CGSize)mediaViewDisplaySize {
    return self.size;
}
-(UIView*)mediaView {
    return self.dealMessageView;
}
-(void)updateMask {
    if (!self.isMaskCreated) {
        self.isMaskCreated = TRUE;
        if(self.dealMessageType == MESSAGETTYPE_CD_VOUCHER) {
            [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToVoucherView:self.dealMessageView];
        }
        else {
            [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:self.dealMessageView
                                                                       isOutgoing:self.appliesMediaViewMaskAsOutgoing];
        }
    }
}
@end

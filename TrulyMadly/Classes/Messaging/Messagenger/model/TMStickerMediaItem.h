//
//  TMStickerMediaItem.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 01/07/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMediaItem.h"
#import "TMSticker.h"

@interface TMStickerMediaItem : TMMediaItem <TMMessageMediaData>

- (instancetype)initWithSticker:(TMSticker*) sticker;
@end

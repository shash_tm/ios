//
//  TMMessage.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessage.h"
#import <UIKit/UIKit.h>
#import "NSString+TMAdditions.h"
#import "TMTimestampFormatter.h"
#import "TMUserSession.h"
#import "NSObject+TMAdditions.h"
#import "TMBuildConfig.h"
#import "TMUserSession.h"
#import "TMStickerMediaItem.h"
#import "NSObject+TMAdditions.h"
#import "TMQuizMediaItem.h"
#import "TMNudgeMediaItem.h"
#import "TMCuratedDealMediaItem.h"
#import "TMPhotoMediaItem.h"
#import "TMImageLinkMediaItem.h"
#import "TMDealMessageContext.h"
#import "NSString+TMAdditions.h"
#import "TMPhotoShareContext.h"
#import "TMImageLinkContext.h"
#import "TMPhotoMediaItem.h"
#import "TMShareEventContext.h"
#import "TMShareEventMediaItem.h"


@interface TMMessage ()

@property(copy,nonatomic) id<TMMessageMediaData> media;
@property(nonatomic,strong)TMDealMessageContext *dealContext;
@property(nonatomic,strong)TMImageLinkContext *imageLinkContext;
@property(nonatomic,strong)NSData* imageData;

@end


@implementation TMMessage

#pragma mark - Initialization

-(instancetype)initWithText:(NSString *)text
                        type:(TMMessageType)messageType
                  receiverId:(NSInteger)receiverId
                  sparkHash:(NSString*)sparkHash {
    self = [super init];
    if(self) {
        self.type = messageType;
        _text = text;
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = [self generateUniqueId];
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        self.sparkAcceptanceMessageHash = sparkHash;
        if (self.type == MESSAGETTYPE_STICKER) {
            if (!self.sticker) {
                //for backward compatibility of old sticker URL
                self.sticker = [[TMSticker alloc] initWithStickerURLString:self.text];
            }
        }
    }
    return self;
}
- (instancetype)initSparkSendMessageWithText:(NSString*)text
                                   messageId:(NSInteger)messageId
                                  receiverId:(NSInteger)receiverId
                                     senderId:(NSInteger)senderId
                                   messageTs:(NSString*)messageTs
{
    self = [super init];
    if (self) {
        self.text = text;
        self.receiverId = receiverId;
        self.senderId = senderId;
        self.messageId = messageId;
        self.timestamp = messageTs;
        self.randomId = [self generateUniqueId];
        self.type = MESSAGETTYPE_TEXT;
        self.deliveryStatus = SENT;
    }
    return self;
}
-(instancetype)initPhotoMessageWithImageData:(NSData*)imageData
                                  receiverId:(NSInteger)receiverId
                               imageSource:(NSString*)imageSource {
    self = [super init];
    if(self) {
        self.type = MESSAGETTYPE_PHOTO;
        _text = @"You've received an image!";
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = [self generateUniqueId];
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        self.imageData = imageData;
        self.metadataDictionary =  @{@"urls":@{@"imagename":self.randomId},
                                     @"imagesource":imageSource,
                                     @"message_type":@"IMAGE"};
        self.photoshareContext = [[TMPhotoShareContext alloc] initWithParams:self.metadataDictionary[@"urls"]
                                                                    senderId:self.senderId
                                                                  receiverId:self.receiverId];
        self.photoshareContext.photoUploadProgressKey = _randomId;
        self.imageSource = imageSource;
        //self.imageName = self.randomId;
        //self.metadataDictionary = @{@"urls":@{@"imagename":self.imageName}};
    }
    return self;
}
-(instancetype)initPhotoMessageWithMetaParams:(NSDictionary*)metaParams
                                   receiverId:(NSInteger)receiverId
                                     randomId:(NSString*)randomId {
    self = [super init];
    if(self) {
        self.type = MESSAGETTYPE_PHOTO;
        _text = @"You've received an image!";
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = randomId;
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        self.metadataDictionary = @{@"urls":metaParams,
                                    @"message_type":@"IMAGE"};
        self.photoshareContext = [[TMPhotoShareContext alloc] initWithParams:self.metadataDictionary[@"urls"]
                                                                    senderId:self.senderId
                                                                  receiverId:self.receiverId];
        self.photoshareContext.photoUploadProgressKey = _randomId;
        //self.imageName = self.randomId;
        //self.metadataDictionary = @{@"urls":@{@"imagename":self.imageName}};
    }
    return self;
}
-(instancetype)initWithDealMessageContext:(TMDealMessageContext*)dealMessageContext
                 withMessageType:(TMMessageType)messageType
                  withReceiverId:(NSInteger)receiverId {
    self = [super init];
    if(self) {
        self.type = messageType;
        _text = [dealMessageContext messageText];
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        //self.datespotId = dealMessageContext.datespotId;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = [self generateUniqueId];
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        _dealContext = dealMessageContext;
    }
    return self;
}
-(instancetype)initWithShareEventContext:(TMShareEventContext *)shareEventContext
                         withMessageType:(TMMessageType)messageType
                           withReceiverId:(NSInteger)receiverId {
    self = [super init];
    if(self) {
        self.type = messageType;
        _text = shareEventContext.eventName;
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = [self generateUniqueId];
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        _shareEventContext = shareEventContext;
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        metadataParams[@"event_id"] = _shareEventContext.eventId;
        metadataParams[@"event_name"] = _shareEventContext.eventName;
        metadataParams[@"event_image_url"] = _shareEventContext.eventImgURL;
        metadataParams[@"message_type"] = [self messageTypeString];
        self.metadataDictionary = metadataParams;
    }
    return self;
}
- (instancetype)initWithText:(NSString*)text
                        type:(TMMessageType)messageType
                  receiverId:(NSInteger)receiverId
                      quizId:(NSString*)quizId fullConversationLink:(NSString*)fullConverstionLink {
    self = [super init];
    if(self) {
        _text = text;
        self.type = messageType;
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = [self generateUniqueId];
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        self.quizId = quizId;
        self.fullConversationLink = fullConverstionLink;
        if (self.type == MESSAGETTYPE_STICKER) {
            if (!self.sticker) {
                //for backward compatibility of old sticker URL
                self.sticker = [[TMSticker alloc] initWithStickerURLString:self.text];
            }
        }
    }
    return self;
}
-(instancetype)initWithMessageDictionary:(NSDictionary*)dict {
    self = [super init];
    if(self) {
        _text = [self getMessageText:dict];
        self.type = [self getMessageType:dict];
        self.receiverId = [self getReceiverId:dict];
        self.senderId = [self getSenderId:dict];
        NSString *message = [dict objectForKey:@"msg_id"];
        if([message isValidObject]) {
            _messageId = [[dict objectForKey:@"msg_id"] integerValue];
        }
        else {
            _messageId = 0;
        }
        
        self.timestamp = dict[@"time"];
        self.deliveryStatus = SENT;
        self.senderType = ([self loggedInUserId] == self.receiverId) ? MESSAGESENDER_ME : MESSAGESENDER_MATCH;
        
        NSString *uniqueId = [dict objectForKey:@"unique_id"];
        self.randomId = ([uniqueId isValidObject]) ? uniqueId : nil;
        NSInteger quizId = 0;
        id  quizIdObj = dict[@"quiz_id"];
        if(quizIdObj) {
            if([quizIdObj isValidObject] && [quizIdObj isKindOfClass:[NSNumber class]]) {
                quizId = [quizIdObj integerValue];
            }
            else if(([quizIdObj isValidObject]) && ([quizIdObj isKindOfClass:[NSString class]]) && (![quizIdObj isNULLString])) {
                quizId = [quizIdObj integerValue];
            }
        }
        self.quizId = [NSString stringWithFormat:@"%ld",(long)quizId];
        if (self.type == MESSAGETTYPE_STICKER) {
            if (!self.sticker) {
                //for backward compatibility of old sticker URL
                self.sticker = [[TMSticker alloc] initWithStickerURLString:self.text];
            }
        }
        if(dict[@"update_spark"]) {
            self.isSparkAcceptanceMessage = TRUE;
        }
        
        //////parse metadata if present
        id metaParams = dict[@"metadata"];
        if(metaParams && [metaParams isKindOfClass:[NSDictionary class]]) {
            self.metadataDictionary = metaParams;
        }
        else if(metaParams && [metaParams isKindOfClass:[NSString class]]) {
            NSData *jsonData = [metaParams dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
            if(jsonDictionary && [jsonDictionary isKindOfClass:[NSDictionary class]]) {
                self.metadataDictionary = jsonDictionary;
            }
        }
        
        if(self.metadataDictionary) {
            if([self isCuratedDealMessage]) {
                self.dealContext = [[TMDealMessageContext alloc] init];
                self.dealContext.datespotId = self.metadataDictionary[@"date_spot_id"];
                self.dealContext.dealId = self.metadataDictionary[@"deal_id"];
                self.dealContext.messageImageURL = self.metadataDictionary[@"image_uri"];
                self.dealContext.datespotAddress = self.metadataDictionary[@"address"];
                self.dealContext.dealMessageType = self.type;
            }
            else if([self isPhotoMessage]) {
                //create photosahre context object
                self.photoshareContext = [[TMPhotoShareContext alloc] initWithParams:self.metadataDictionary[@"urls"]
                                                                            senderId:self.senderId
                                                                          receiverId:self.receiverId];
                self.photoshareContext.photoUploadProgressKey = _randomId;
            }
            else if([self isImageLinkMessage]) {
                self.imageLinkContext = [[TMImageLinkContext alloc] initWithMetaParams:self.metadataDictionary];
            }
            else if(self.type == MESSAGETTYPE_SHARE_EVENT) {
                self.shareEventContext = [[TMShareEventContext alloc] init];
                self.shareEventContext.eventId = self.metadataDictionary[@"event_id"];
                self.shareEventContext.eventName = self.metadataDictionary[@"event_name"];
                self.shareEventContext.eventImgURL = self.metadataDictionary[@"event_image_url"];
            }
        }
    }
    return self;
}
-(instancetype)initWithSocketParamDict:(NSDictionary*)dict {
    self = [super init];
    if(self) {
        _text = [self getMessageText:dict];
        _type = [self getMessageType:dict];
        _receiverId = [self getReceiverId:dict];
        _senderId = self.loggedInUserId;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _deliveryStatus = SENDING_FAILED;
        _randomId = [dict objectForKey:@"unique_id"];
        NSInteger quizId = 0;
        id  quizIdObj = dict[@"quiz_id"];
        if(quizIdObj) {
            if([quizIdObj isValidObject] && [quizIdObj isKindOfClass:[NSNumber class]]) {
                quizId = [quizIdObj integerValue];
            }
            else if(([quizIdObj isValidObject]) && ([quizIdObj isKindOfClass:[NSString class]]) && (![quizIdObj isNULLString])) {
                quizId = [quizIdObj integerValue];
            }
        }
        self.quizId = [NSString stringWithFormat:@"%ld",(long)quizId];
        if (self.type == MESSAGETTYPE_STICKER) {
            if (!self.sticker) {
                //for backward compatibility of old sticker URL
                self.sticker = [[TMSticker alloc] initWithStickerURLString:self.text];
            }
        }
        NSDictionary *metaParams = dict[@"metadata"];
        
        if(metaParams && [metaParams isKindOfClass:[NSDictionary class]]) { ////metaparams
            self.metadataDictionary = metaParams;
            if([self isCuratedDealMessage]) {
                self.dealContext = [[TMDealMessageContext alloc] init];
                self.dealContext.datespotId = metaParams[@"date_spot_id"];
                self.dealContext.dealId = metaParams[@"deal_id"];
                self.dealContext.messageImageURL = metaParams[@"image_uri"];
                self.dealContext.datespotAddress = metaParams[@"address"];
            }
            else if([self isPhotoMessage]) {
                //create photosahre context object
                self.photoshareContext = [[TMPhotoShareContext alloc] initWithParams:self.metadataDictionary[@"urls"]
                                                                            senderId:self.senderId
                                                                          receiverId:self.receiverId];
                self.photoshareContext.photoUploadProgressKey = _randomId;
            }
            else if([self isImageLinkMessage]) {
                self.imageLinkContext = [[TMImageLinkContext alloc] initWithMetaParams:self.metadataDictionary];
            }
            else if(self.type == MESSAGETTYPE_SHARE_EVENT) {
                self.shareEventContext = [[TMShareEventContext alloc] init];
                self.shareEventContext.eventId = self.metadataDictionary[@"event_id"];
                self.shareEventContext.eventName = self.metadataDictionary[@"event_name"];
                self.shareEventContext.eventImgURL = self.metadataDictionary[@"event_image_url"];
            }
        }////metaparams
    }
    return self;
}
- (instancetype)initWithSticker:(TMSticker*)sticker
                           text:(NSString*) text
                     receiverId:(NSInteger)receiverId
                      sparkHash:(NSString*)sparkHash {
    self = [super init];
    if(self) {
        self.type = MESSAGETTYPE_STICKER;
        self.text = text;
        self.sticker = sticker;
        self.receiverId = receiverId;
        self.senderType = MESSAGESENDER_ME;
        _timestamp = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
        _randomId = [self generateUniqueId];
        _senderId = self.loggedInUserId;
        _deliveryStatus = SENDING;
        if (!self.sticker) {
            //for backward compatibility of old sticker URL
            self.sticker = [[TMSticker alloc] initWithStickerURLString:self.text];
        }
        self.sparkAcceptanceMessageHash = sparkHash;
    }
    return self;
}
-(instancetype)initWithBadWordMessageDictionary:(NSDictionary *)dict {
    self = [super init];
    if(self) {
        self.text = dict[@"error_msg"];
        self.randomId = dict[@"unique_id"];
        self.receiverId = [dict[@"receiver_id"] integerValue];
        self.senderId = [dict[@"sender_id"] integerValue];
    }
    return self;
}
- (void)dealloc
{
    self.text = nil;
    self.media = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (NSUInteger)messageHash
{
    return self.hash;
}

#pragma mark - Setter

-(void)setDeliveryStatus:(TMMessageDeliveryStatus)deliveryStatus {
    _deliveryStatus = deliveryStatus;
    if(_type == MESSAGETTYPE_PHOTO) {
        TMPhotoMediaItem *photoMediaItem = _media;
        photoMediaItem.deliveryStatus = deliveryStatus;
    }
}
-(void)setType:(TMMessageType)type {
    _type = type;
}

-(void)setMetadata:(NSData*)metadata {
    NSDictionary *metadataDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:metadata];
    if(metadataDictionary && [metadataDictionary isKindOfClass:[NSDictionary class]]) {
        self.metadataDictionary = metadataDictionary;
        self.type = [self getMessageTypeEnumFromString:self.metadataDictionary[@"message_type"]];
        
        if(self.type == MESSAGETTYPE_PHOTO) {
            NSDictionary *urlDict = self.metadataDictionary[@"urls"];
            self.photoshareContext = [[TMPhotoShareContext alloc] initWithParams:urlDict senderId:self.senderId receiverId:self.receiverId];
            self.photoshareContext.photoUploadProgressKey = _randomId;
            self.imageSource = self.metadataDictionary[@"imagesource"];
            //check if download or upload in progress and set state
        }
        else if([self isImageLinkMessage]) {
            self.imageLinkContext = [[TMImageLinkContext alloc] initWithMetaParams:self.metadataDictionary];
        }
        else if(self.type == MESSAGETTYPE_SHARE_EVENT) {
            self.shareEventContext = [[TMShareEventContext alloc] init];
            self.shareEventContext.eventId = self.metadataDictionary[@"event_id"];
            self.shareEventContext.eventName = self.metadataDictionary[@"event_name"];
            self.shareEventContext.eventImgURL = self.metadataDictionary[@"event_image_url"];
        }
    }
}

-(void)updateMetadataFromDictioanary:(NSDictionary*)metadataDictioanry {
    self.metadataDictionary = metadataDictioanry;
    NSDictionary *urlDict = self.metadataDictionary[@"urls"];
    self.photoshareContext = [[TMPhotoShareContext alloc] initWithParams:urlDict senderId:self.senderId receiverId:self.receiverId];
    self.photoshareContext.photoUploadProgressKey = _randomId;
    self.imageSource = self.metadataDictionary[@"imagesource"];
}

#pragma mark - Getter

-(id<TMMessageMediaData>)media {
    if(!_media) {
        _media = [self getMediaObjectForType:_type];
    }
    return _media;
}

-(NSInteger)loggedInUserId {
    NSInteger userId = [[TMUserSession sharedInstance].user.userId integerValue];
    return userId;
}
-(NSInteger)matchId {
    NSInteger userId = [self loggedInUserId];
    if(self.receiverId == userId) {
        return self.senderId;
    }
    else {
        return self.receiverId;
    }
}
-(NSString*)tempMessageId {
    return self.randomId;
}
-(NSString*)uid {
    if(self.randomId) {
        return self.randomId;
    }
    else {
        return [NSString stringWithFormat:@"%ld",(long)self.messageId];
    }
}

//-(NSString*)uid {
//    if(self.source == SOCKET) {
//        if(self.randomId) {
//            return self.randomId;
//        }
//        else {
//            return [NSString stringWithFormat:@"%ld",(long)self.messageId];
//        }
//    }
//    else {
//        if(self.messageId) {
//            return [NSString stringWithFormat:@"%ld",(long)self.messageId];
//        }
//        else {
//            return self.randomId;
//        }
//    }
//}
-(BOOL)isOutgoingMessage {
    _isOutgoingMessage =  (self.senderType == MESSAGESENDER_ME) ? 1 : 0;
    return _isOutgoingMessage;
}
-(BOOL)isMediaMessage {
    if(self.type != MESSAGETTYPE_TEXT) {
        return true;
    }
    return false;
}
-(TMMessageSender)senderType {
    if(self.senderId != [self matchId]) {
        return MESSAGESENDER_ME;
    }
    else {
        return MESSAGESENDER_MATCH;
    }
}

#pragma mark - Public Methods

-(void)cacheSendingImage {
    [self.photoshareContext cacheImageData:self.imageData];
}
-(NSData*)sentImageData {
    NSData *data = nil;
    if(self.imageData) {
        data = self.imageData;
    }
    else {
        UIImage *cachedImage = [self.photoshareContext getCachedSentImage];
        data = UIImageJPEGRepresentation(cachedImage, 1);
    }
    return data;
}
-(NSString*)datespotId {
    return self.dealContext.datespotId;
}
-(NSString*)dealId {
    //
    id dealId = self.dealContext.dealId;
    if(dealId == nil || ([dealId isNULLString]) ) {
        return nil;
    }
    return self.dealContext.dealId;
}
-(NSString*)datespotAddress {
    return self.dealContext.datespotAddress;
}
-(NSString*)datespotImageURLString {
    return self.dealContext.messageImageURL;
}
-(NSString*)shareEventImageURLString {
    return self.shareEventContext.eventImgURL;
}
-(void)setDatespotId:(NSString*)datespotId {
    if(!self.dealContext) {
        self.dealContext = [[TMDealMessageContext alloc] init];
    }
    self.dealContext.datespotId = datespotId;
}
-(void)setDealId:(NSString*)dealId {
    if(!self.dealContext) {
        self.dealContext = [[TMDealMessageContext alloc] init];
    }
    self.dealContext.dealId = dealId;
}
-(void)setDatespotAddress:(NSString*)datespotAddress {
    if(!self.dealContext) {
        self.dealContext = [[TMDealMessageContext alloc] init];
    }
    self.dealContext.datespotAddress = datespotAddress;
}
-(void)setDatespotImage:(NSString*)datespotImage {
    if(!self.dealContext) {
        self.dealContext = [[TMDealMessageContext alloc] init];
    }
    self.dealContext.messageImageURL = datespotImage;
}
-(void)updateStateFromSentMessageResponse:(TMMessage*)message {
    self.deliveryStatus = message.deliveryStatus;
    self.timestamp = message.timestamp;
    self.dealContext.dealId = message.dealContext.dealId;
}
-(void)registerNotificationForUid {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDeliveryStatus:) name:self.uid object:nil];
}
-(void)updateDeliveryStatus:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    TMMessage *message = userInfo[@"msg"];
    [self updateStateFromSentMessageResponse:message];
    if((message.type == MESSAGETTYPE_PHOTO) && (message.deliveryStatus != SENDING)) {
        [self removePhotoUploadProgress];
    }
}

-(NSAttributedString*)attributedText {
    NSDictionary *attDict  = @{ NSFontAttributeName : [UIFont preferredFontForTextStyle:UIFontTextStyleBody], NSForegroundColorAttributeName : [UIColor blackColor] };
    
    NSAttributedString *attstr =  [[NSAttributedString alloc] initWithData:[self.text dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:&attDict error:nil];
    
    return attstr;
}
-(NSString*)simpleText {
    NSString *text = @"";
    if(self.text && ![self.text isKindOfClass:[NSNull class]]) {
        text = [self.text trimBrTag];
    }
    
    return text;
}
-(NSString*)messageTypeString {
    NSString *messageType = @"TEXT";
    if(self.type == MESSAGETTYPE_TEXT) {
        messageType = @"TEXT";
    }
    else if(self.type == MESSAGETTYPE_STICKER) {
        messageType = @"STICKER";
    }
    else if (self.type == MESSAGETTYPE_QUIZ_MESSAGE) {
        messageType = @"QUIZ_MESSAGE";
    }
    else if (self.type == MESSAGETTYPE_QUIZ_NUDGE_SINGLE) {
        messageType = @"NUDGE_SINGLE";
    }
    else if (self.type == MESSAGETTYPE_QUIZ_NUDGE_DOUBLE) {
        messageType = @"NUDGE_DOUBLE";
    }
    else if(self.type == MESSAGETTYPE_CD_ASK) {
        messageType = @"CD_ASK";
    }
    else if(self.type == MESSAGETTYPE_CD_VOUCHER) {
        messageType = @"CD_VOUCHER";
    }
    else if(self.type == MESSAGETTYPE_PHOTO) {
        messageType = @"IMAGE";
    }
    else if(self.type == MESSAGETTYPE_SHARE_EVENT) {
        messageType = @"SHARE_EVENT";
    }

    return messageType;
}
-(NSString*)messageIdAsString {
    if(self.messageId != 0) {
        return [NSString stringWithFormat:@"%ld",(long)self.messageId];
    }
    else {
        return [self tempMessageId];
    }
}
-(NSString*)conversationListMessage {
    NSString *conversationMessage = nil;
    if(self.type == MESSAGETTYPE_STICKER) {
        if(self.senderType == MESSAGESENDER_ME) {
            conversationMessage = @"You've sent a sticker!";
        }
        else {
            conversationMessage = @"You've received a sticker!";
        }
    }
    else if(self.type == MESSAGETTYPE_PHOTO) {
        if(self.senderType == MESSAGESENDER_ME) {
            conversationMessage = @"You've sent an image!";
        }
        else {
            conversationMessage = @"You've received an image!";
        }
    }
    else {
        conversationMessage = self.text;
    }
    return conversationMessage;
}
-(NSDictionary*)socketConnectionParams {
    NSString *receiverId = nil;
    receiverId = (self.isAdmin) ? @"admin" : [NSString stringWithFormat:@"%ld",(long)self.receiverId];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:8];
    params[@"receiver_id"] = receiverId;
    params[@"msg"] = self.text;
    params[@"message_type"] = [self messageTypeString];
    params[@"unique_id"] = self.randomId;
    params[@"is_admin"] = @(self.isAdmin);
    
    ///////
    if(self.sparkAcceptanceMessageHash) {
        params[@"hash"] = self.sparkAcceptanceMessageHash;
        params[@"update_spark"] = @"true";
    }
    if(self.quizId) {
        params[@"quiz_id"] = self.quizId;
    }
    
    ///////
    if([self isCuratedDealMessage]) {
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        metadataParams[@"message"] = self.text;
        metadataParams[@"date_spot_id"] = self.dealContext.datespotId;
        metadataParams[@"address"] = self.dealContext.datespotAddress;
        metadataParams[@"message_type"] = [self messageTypeString];
        metadataParams[@"image_uri"] = self.dealContext.messageImageURL;
        if(self.type == MESSAGETTYPE_CD_VOUCHER) {
            metadataParams[@"deal_id"] = self.dealContext.dealId;
            metadataParams[@"deal_status"] = @"accepted";
        }
        params[@"metadata"] = metadataParams;
    }
    else if([self isPhotoMessage]) {
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        metadataParams[@"message"] = self.text;
        metadataParams[@"message_type"] = [self messageTypeString];
        metadataParams[@"urls"] = self.metadataDictionary[@"urls"];
        params[@"metadata"] = metadataParams;
    }
    else if([self isShareEventMessage]) {
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        metadataParams[@"event_id"] = self.shareEventContext.eventId;
        metadataParams[@"event_name"] = self.shareEventContext.eventName;
        metadataParams[@"event_image_url"] = self.shareEventContext.eventImgURL;
         metadataParams[@"message_type"] = [self messageTypeString];
        params[@"metadata"] = metadataParams;
    }
    return params;
}
-(NSDictionary*)socketTrackingParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:8];
    params[@"unique_id"] = self.randomId;
    return params;
}
-(NSString*)socketTrackingStatus:(NSString*)status {
    NSMutableString *result = [NSMutableString stringWithCapacity:16];
    if(self.isFailedMessageRetry) {
        [result appendString:[NSString stringWithFormat:@"%@_",@"manual"]];
        
        if(self.sentFailRetryCount == 0) {
            [result appendString:[NSString stringWithFormat:@"retry--%@_1",status]];
        }
        else if(self.sentFailRetryCount == 1) {
            [result appendString:[NSString stringWithFormat:@"retry--%@_2",status]];
        }
    }
    else {
        if(self.sentFailRetryCount == 0) {
            [result appendString:[NSString stringWithFormat:@"%@_1",status]];
        }
        else if(self.sentFailRetryCount == 1) {
            [result appendString:[NSString stringWithFormat:@"%@_2",status]];
        }
    }
    
    return result;
}
-(NSDictionary*)pollingConnectionParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:8];
    params[@"msg"] = self.text;
    params[@"type"] = @"send_msg";
    params[@"message_type"] = [self messageTypeString];
    params[@"unique_id"] = self.randomId;
    params[@"is_admin"] = @(self.isAdmin);
    params[@"rc"] = @(self.sentFailRetryCount);
    
    ///////
    if(self.sparkAcceptanceMessageHash) {
        params[@"update_spark"] = @"true";
        params[@"hash"] = self.sparkAcceptanceMessageHash;
    }
    if(self.quizId) {
        params[@"quiz_id"] = self.quizId;
    }
    
    //////
    if([self isCuratedDealMessage]) {
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        metadataParams[@"message"] = self.text;
        metadataParams[@"date_spot_id"] = self.dealContext.datespotId;
        metadataParams[@"address"] = self.dealContext.datespotAddress;
        metadataParams[@"message_type"] = [self messageTypeString];
        metadataParams[@"image_uri"] = self.dealContext.messageImageURL;
        if(self.type == MESSAGETTYPE_CD_VOUCHER) {
            metadataParams[@"deal_id"] = self.dealContext.dealId;
            metadataParams[@"deal_status"] = @"accepted";
        }
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:metadataParams options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        params[@"metadata"] = jsonString;
    }
    else if([self isPhotoMessage]) {
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        //metadataParams[@"message"] = self.text;
        metadataParams[@"message_type"] = [self messageTypeString];
        metadataParams[@"urls"] = self.metadataDictionary[@"urls"];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:metadataParams options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        params[@"metadata"] = jsonString;
    }
    else if([self isShareEventMessage]) {
        NSMutableDictionary *metadataParams = [NSMutableDictionary dictionaryWithCapacity:4];
        metadataParams[@"event_id"] = self.shareEventContext.eventId;
        metadataParams[@"event_name"] = self.shareEventContext.eventName;
        metadataParams[@"event_image_url"] = self.shareEventContext.eventImgURL;
        metadataParams[@"message_type"] = [self messageTypeString];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:metadataParams options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        params[@"metadata"] = jsonString;
    }
    
    return params;
}
-(NSDictionary*)chatReadParams {
    NSDictionary *params = @{@"sender_id":@(self.matchId),
                             @"last_seen_msg_id":@(self.messageId),
                             @"last_seen_msg_tstamp":self.timestamp,
                             @"unique_id":self.uid};
    return params;
}
-(NSDictionary*)metaDataDictionary {
    return _metadataDictionary;
}
-(BOOL)isQuizMessage {
    if((self.type == MESSAGETTYPE_QUIZ_MESSAGE) ||
       (self.type == MESSAGETTYPE_QUIZ_NUDGE_SINGLE) ||
       (self.type == MESSAGETTYPE_QUIZ_NUDGE_DOUBLE)) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isCuratedDealMessage {
    if( (self.type == MESSAGETTYPE_CD_ASK) || (self.type == MESSAGETTYPE_CD_VOUCHER) ) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isPhotoMessage {
    if(self.type == MESSAGETTYPE_PHOTO) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isShareEventMessage {
    if(self.type == MESSAGETTYPE_SHARE_EVENT) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isImageLinkMessage {
    if(self.type == MESSAGETTYPE_IMAGELINK) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isPhotoUploaded {
    BOOL status = FALSE;
    if(self.photoshareContext) {
        status = (self.photoshareContext.jpegImageURLString) ? TRUE : FALSE;
    }
    return status;
}
-(BOOL)isCurrentConversationMessage {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    if(userSession.isUserOnOneToOneMessaging && userSession.currentMatchIdForOneToOneMessaging.integerValue == self.matchId) {
        return TRUE;
    }
    return FALSE;
}
-(void)showPhotoUploadProgress {
    TMPhotoMediaItem *photoMediaItem = self.media;
    [photoMediaItem startPhotoUploadProgressTracking];
}
-(void)removePhotoUploadProgress {
    TMPhotoMediaItem *photoMediaItem = self.media;
    [photoMediaItem stopPhotoUploadProgressTracking];
}

//-(NSString*)messageImageCachePath {
//    NSString *imageCachePath = [NSString stringWithFormat:@".images/conversation/%ld-%ld",(long)self.loggedInUserId,(long)self.matchId];
//    return imageCachePath;
//}
//-(NSString*)messageImageName {
//    TMPhotoShareContext *context = [self getPhotoShareContext];
//    return context.imageName;
//}

#pragma mark - Internal Methods
-(NSString*)getMessageText:(NSDictionary*)messageData {
    NSString *text = [messageData objectForKey:@"msg"];
    return [self getTrimmedText:text];
}
-(NSInteger)getReceiverId:(NSDictionary*)messageData {
    NSInteger receiverId;
    id receiverIdObj = [messageData objectForKey:@"receiver_id"];
    if([receiverIdObj isKindOfClass:[NSString class]] && [receiverIdObj isEqualToString:@"admin"]) {
        receiverId = ADMIN_USERID;
    }
    else {
        receiverId = [messageData[@"receiver_id"] integerValue];
    }
    return receiverId;
}
-(NSInteger)getSenderId:(NSDictionary*)messageData {
    NSInteger senderId;
    id senderIdObj = [messageData objectForKey:@"sender_id"];
    if([senderIdObj isKindOfClass:[NSString class]] && [senderIdObj isEqualToString:@"admin"]) {
        senderId = ADMIN_USERID;
    }
    else {
        senderId = [messageData[@"sender_id"] integerValue];
    }
    return senderId;
}
-(TMMessageType)getMessageType:(NSDictionary*)messageData {
    //set default type
    TMMessageType type = MESSAGETTYPE_TEXT;
    
    //process json response
    NSString *msgtypeObj = [messageData objectForKey:@"message_type"];
    if(msgtypeObj && [msgtypeObj isKindOfClass:[NSString class]]) {
        if([msgtypeObj isEqualToString:@"text"] || [msgtypeObj isEqualToString:@"TEXT"]) {
            type = MESSAGETTYPE_TEXT;
        }
        else if([msgtypeObj isEqualToString:@"STICKER"]) {
            type = MESSAGETTYPE_STICKER;
        }
        else if([msgtypeObj isEqualToString:@"QUIZ_MESSAGE"]) {
            type = MESSAGETTYPE_QUIZ_MESSAGE;
        }
        else if([msgtypeObj isEqualToString:@"NUDGE_SINGLE"]) {
            type = MESSAGETTYPE_QUIZ_NUDGE_SINGLE;
        }
        else if([msgtypeObj isEqualToString:@"NUDGE_DOUBLE"]) {
            type = MESSAGETTYPE_QUIZ_NUDGE_DOUBLE;
        }
        else if([msgtypeObj isEqualToString:@"CD_ASK"]) {
            type = MESSAGETTYPE_CD_ASK;
        }
        else if([msgtypeObj isEqualToString:@"CD_VOUCHER"]) {
            type = MESSAGETTYPE_CD_VOUCHER;
        }
        else if([msgtypeObj isEqualToString:@"IMAGE"]) {
            type = MESSAGETTYPE_PHOTO;
        }
        else if([msgtypeObj isEqualToString:@"IMAGE_LINK"]) {
            type = MESSAGETTYPE_IMAGELINK;
        }
        else if([msgtypeObj isEqualToString:@"SHARE_EVENT"]) {
            type = MESSAGETTYPE_SHARE_EVENT;
        }
    }
    return type;
}
-(TMMessageType)getMessageTypeEnumFromString:(NSString *)messageType {
    TMMessageType type = MESSAGETTYPE_TEXT;
    if([messageType isEqualToString:@"text"] || [messageType isEqualToString:@"TEXT"]) {
        type = MESSAGETTYPE_TEXT;
    }
    else if([messageType isEqualToString:@"STICKER"]) {
        type = MESSAGETTYPE_STICKER;
    }
    else if([messageType isEqualToString:@"QUIZ_MESSAGE"]) {
        type = MESSAGETTYPE_QUIZ_MESSAGE;
    }
    else if([messageType isEqualToString:@"NUDGE_SINGLE"]) {
        type = MESSAGETTYPE_QUIZ_NUDGE_SINGLE;
    }
    else if([messageType isEqualToString:@"NUDGE_DOUBLE"]) {
        type = MESSAGETTYPE_QUIZ_NUDGE_DOUBLE;
    }
    else if([messageType isEqualToString:@"CD_ASK"]) {
        type = MESSAGETTYPE_CD_ASK;
    }
    else if([messageType isEqualToString:@"CD_VOUCHER"]) {
        type = MESSAGETTYPE_CD_VOUCHER;
    }
    else if([messageType isEqualToString:@"IMAGE"]) {
        type = MESSAGETTYPE_PHOTO;
    }
    else if([messageType isEqualToString:@"IMAGE_LINK"]) {
        type = MESSAGETTYPE_IMAGELINK;
    }
    else if([messageType isEqualToString:@"SHARE_EVENT"]) {
        type = MESSAGETTYPE_SHARE_EVENT;
    }
    return type;
}
-(NSString*)generateUniqueId {
    NSDate *date = [NSDate date];
    unsigned long long ts = (long long)([date timeIntervalSince1970] * 1000.0);
    
    NSInteger randomNumber = arc4random_uniform(10000);
    //@"userid_matchid_timestamp_randomnumber"
    NSString *uid = [NSString stringWithFormat:@"%ld%ld%llu%ld",(long)self.loggedInUserId,(long)self.receiverId,ts,(long)randomNumber];
    return uid;
}
-(NSString*)getTrimmedText:(NSString*)unTrimmedText {
    NSString *ret = nil;
    if(unTrimmedText && ![unTrimmedText isKindOfClass:[NSNull class]]) {
        ret = [unTrimmedText trimBrTag];
    }
    return ret;
}

-(id<TMMessageMediaData>)getMediaObjectForType:(TMMessageType)type {
    id<TMMessageMediaData> media = nil;
    if(type == MESSAGETTYPE_STICKER) {
        TMStickerMediaItem *mediaItem = [[TMStickerMediaItem alloc] initWithSticker:self.sticker];
        media = mediaItem;
    }
    else if (type == MESSAGETTYPE_QUIZ_MESSAGE) {
        TMQuizMediaItem *quizMediaItem = [[TMQuizMediaItem alloc] initWithQuizMessage:_text isOutgoingMessage:self.isOutgoingMessage];
        media = quizMediaItem;
    }
    else if (type == MESSAGETTYPE_QUIZ_NUDGE_SINGLE) {
        TMNudgeMediaItem *nudgeMediaItem = [[TMNudgeMediaItem alloc] initWithQuizIconURLString:@"" quizMessage:_text quizType:MESSAGETTYPE_QUIZ_NUDGE_SINGLE];
        media = nudgeMediaItem;
    }
    else if (type == MESSAGETTYPE_QUIZ_NUDGE_DOUBLE) {
        TMNudgeMediaItem *nudgeMediaItem = [[TMNudgeMediaItem alloc] initWithQuizIconURLString:@"" quizMessage:_text quizType:MESSAGETTYPE_QUIZ_NUDGE_DOUBLE];
        media = nudgeMediaItem;
    }
    else if (type == MESSAGETTYPE_CD_ASK || type == MESSAGETTYPE_CD_VOUCHER) {
        BOOL isFailedMessage = (self.deliveryStatus == SENDING_FAILED) ? TRUE : FALSE;
        TMCuratedDealMediaItem *curatedDealMediaItem = [[TMCuratedDealMediaItem alloc] initWithMessageType:type failedMessage:isFailedMessage];
        media = curatedDealMediaItem;
    }
    else if(type == MESSAGETTYPE_PHOTO) {
        TMPhotoMediaItem *photoMediaItem = [[TMPhotoMediaItem alloc] initWithContext:self.photoshareContext
                                                           withMessageDeliveryStatus:self.deliveryStatus];
        photoMediaItem.matchId = self.matchId;
        media = photoMediaItem;
    }
    else if(type == MESSAGETTYPE_IMAGELINK) {
        TMImageLinkMediaItem *imageLinkMediaItem = [[TMImageLinkMediaItem alloc] init];
        media = imageLinkMediaItem;
    }
    else if (type == MESSAGETTYPE_SHARE_EVENT) {
        BOOL isFailedMessage = (self.deliveryStatus == SENDING_FAILED) ? TRUE : FALSE;
        TMShareEventMediaItem *shareEventMediaItem = [[TMShareEventMediaItem alloc] initWithMessageType:type failedMessage:isFailedMessage];
        media = shareEventMediaItem;
    }
    return media;
}

-(NSDictionary*)getInsertCacheParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *quizId = [NSString stringWithFormat:@"%@",self.quizId];
    NSString *galleryId = [NSString stringWithFormat:@"%@",self.sticker.galleryID];
    NSString *stickerId = [NSString stringWithFormat:@"%@",self.sticker.stickerID];
    NSString *datespotId = [NSString stringWithFormat:@"%@",self.dealContext.datespotId];
    NSString *dealId = [NSString stringWithFormat:@"%@",self.dealContext.dealId];
    NSString *randomId = [NSString stringWithFormat:@"%@",self.randomId];
    NSData *metadata = (self.metadataDictionary) ? [NSKeyedArchiver archivedDataWithRootObject:self.metadataDictionary] : [NSData data];
    params[@"matchid"] = @(self.matchId);
    params[@"userid"] = @(self.loggedInUserId);
    params[@"randomid"] = randomId;
    params[@"uid"] = self.uid;
    params[@"messageid"] =  self.messageIdAsString;
    params[@"senderid"] = @(self.senderId);
    params[@"receiverid"] = @(self.receiverId);
    params[@"text"] = self.text;
    params[@"msgtype"] = @(self.type);
    params[@"deliverystatus"] = @(self.deliveryStatus);
    params[@"msgts"] = self.timestamp;
    params[@"xts"] = self.timestamp;
    params[@"msgsender"] = @(self.senderType);
    params[@"source"] = @(self.source);
    params[@"quizid"] = quizId;
    params[@"gallery_id"] = galleryId;
    params[@"sticker_id"] = stickerId;
    params[@"datespotid"] = datespotId;
    params[@"dealid"] = dealId;
    params[@"metadata"] = metadata;
    return params;
}
-(NSDictionary*)getUpdateCacheParamsWithTimestampStatus:(BOOL)status {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *randomId = [NSString stringWithFormat:@"%@",self.randomId];
    NSData *metadata = (self.metadataDictionary) ? [NSKeyedArchiver archivedDataWithRootObject:self.metadataDictionary] : [NSData data];
    params[@"randomid"] = randomId;
    params[@"uid"] = self.uid;
    params[@"messageid"] =  self.messageIdAsString;
    params[@"senderid"] = @(self.senderId);
    params[@"receiverid"] = @(self.receiverId);
    params[@"text"] = self.text;
    params[@"deliverystatus"] = @(self.deliveryStatus);
    params[@"msgtype"] = @(self.type);
    if(status) {
        params[@"msgts"] = self.timestamp;
        params[@"xts"] = self.timestamp;
    }
    if(self.source == POLLING) {
        params[@"messageid1"] = @(self.messageId);
    }
    else {
        params[@"uid1"] = self.uid;
    }
    params[@"matchid"] = @(self.matchId);
    NSString *datespotId = [NSString stringWithFormat:@"%@",self.dealContext.datespotId];
    params[@"datespotid"] = datespotId;
    NSString *dealId = [NSString stringWithFormat:@"%@",self.dealContext.dealId];
    params[@"dealid"] = dealId;
    params[@"metadata"] = metadata;
    return params;
}
-(NSDictionary*)getMetadataUpdateCacheParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *randomId = [NSString stringWithFormat:@"%@",self.randomId];
    NSData *metadata = (self.metadataDictionary) ? [NSKeyedArchiver archivedDataWithRootObject:self.metadataDictionary] : [NSData data];
    params[@"metadata"] = metadata;
    params[@"randomid"] = randomId;
    params[@"matchid"] = @(self.matchId);
    return params;
}
-(NSDictionary*)datespotUpdateQueryParmas {
    return [self.dealContext updateQueryParams];
}
-(NSDictionary*)datespotInsertQueryParmas {
    return [self.dealContext insertQueryParams];
}
-(void)downloadFullImage {
    TMPhotoMediaItem *photoMediaItem = self.media;
    [photoMediaItem downloadFullImage];
}
-(NSString*)imageLinkURLString {
    return self.imageLinkContext.imageURLString;
}
-(NSString*)imageLinkLandingURLString {
    return self.imageLinkContext.landingURLString;
}

#pragma mark - NSObject

- (BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    
    TMMessage *aMessage = (TMMessage *)object;
    if (self.isMediaMessage != aMessage.isMediaMessage) {
        return NO;
    }
    
    BOOL hasEqualContent = self.isMediaMessage ? [self.media isEqual:aMessage.media] : [self.text isEqualToString:aMessage.text];
    NSString *aMessageSenderId = [NSString stringWithFormat:@"%ld",(long)aMessage.senderId];
    NSString *senderId = [NSString stringWithFormat:@"%ld",(long)self.senderId];
    return [senderId isEqualToString:aMessageSenderId]
    && ([self.timestamp isEqualToString:aMessage.timestamp])
    && hasEqualContent;
}

- (NSUInteger)hash
{
    NSUInteger contentHash = self.isMediaMessage ? [self.media mediaHash] : self.text.hash;
    return @(self.senderId).hash ^ self.timestamp.hash ^ contentHash;
}

//- (NSString *)description
//{
//    return [NSString stringWithFormat:@"<%@: senderId=%ld, date=%@, isMediaMessage=%@, text=%@, media=%@, uid=%@, self:%@>",
//            [self class], (long)self.senderId, self.timestamp, @(self.isMediaMessage), self.text, self.media, _uid, self];
//}

- (id)debugQuickLookObject
{
    return [self.media mediaView] ?: [self.media mediaPlaceholderView];
}

@end

//
//  TMMessage.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMMessageData.h"
#import "TMSticker.h"
#import "TMEnums.h"

@class TMDealMessageContext;
@class TMPhotoShareContext;
@class TMImageLinkContext;
@class TMShareEventContext;

@interface TMMessage : NSObject <TMMessageData>

@property (copy, nonatomic, readonly) id<TMMessageMediaData> media;

@property(nonatomic,assign) NSInteger senderId;
@property(nonatomic,assign) NSInteger receiverId;
@property(nonatomic,assign) NSInteger messageId;
@property(nonatomic,assign) NSInteger matchId;
@property(nonatomic,assign) NSInteger loggedInUserId;
@property(nonatomic,assign) NSInteger sentFailRetryCount;

@property(nonatomic,assign) BOOL isAdmin;
@property(nonatomic,assign) BOOL isOutgoingMessage;
@property(nonatomic,assign) BOOL isMediaMessage;
@property(nonatomic,assign) BOOL showTopLabel;
@property(nonatomic,assign) BOOL isFailedMessageRetry;
@property(nonatomic,assign) BOOL isBlocked;
@property(nonatomic,assign) BOOL isMessageSeen;
@property(nonatomic,assign) BOOL isSparkAcceptanceMessage;

@property(nonatomic,assign) TMMessageType type;
@property(nonatomic,assign) TMMessageSender senderType;
@property(nonatomic,assign) TMMessageDeliveryStatus deliveryStatus;
@property(nonatomic,assign) TMSendMessageStatus sendingStatus;
@property(nonatomic,assign) TMMessageSource source;

@property(nonatomic,copy) NSString* fullConversationLink;
@property(nonatomic,copy) NSString *text;
@property(nonatomic,copy) NSString *timestamp;
@property(nonatomic,copy) NSString *randomId;
@property(nonatomic,copy) NSString *uid;
@property(nonatomic,copy) NSString *quizId;
@property(nonatomic,copy) NSString *imageSource;
@property(nonatomic,copy) NSString* sparkAcceptanceMessageHash;

@property(nonatomic,strong) NSDictionary *metadataDictionary;
@property(nonatomic,strong) TMSticker* sticker;
@property(nonatomic,strong) TMPhotoShareContext *photoshareContext;
@property(nonatomic,strong) TMShareEventContext *shareEventContext;

-(instancetype)initPhotoMessageWithImageData:(NSData*)imageData
                                  receiverId:(NSInteger)receiverId
                                 imageSource:(NSString*)imageSource;

-(instancetype)initWithText:(NSString *)text
                       type:(TMMessageType)messageType
                 receiverId:(NSInteger)receiverId
                  sparkHash:(NSString*)sparkHash;

- (instancetype)initSparkSendMessageWithText:(NSString*)text
                                   messageId:(NSInteger)messageId
                                  receiverId:(NSInteger)receiverId
                                    senderId:(NSInteger)senderId
                                   messageTs:(NSString*)messageTs;

- (instancetype)initWithText:(NSString *)text
                        type:(TMMessageType)messageType
                  receiverId:(NSInteger)receiverId
                      quizId:(NSString*)quizId fullConversationLink:(NSString*) fullConverstionLink;

- (instancetype)initWithSticker:(TMSticker*)sticker
                           text:(NSString*) text
                     receiverId:(NSInteger)receiverId
                      sparkHash:(NSString*)sparkHash;

-(instancetype)initWithBadWordMessageDictionary:(NSDictionary *)dict;

-(instancetype)initWithDealMessageContext:(TMDealMessageContext*)dealMessageContext
                          withMessageType:(TMMessageType)messageType
                           withReceiverId:(NSInteger)receiverId;

-(instancetype)initPhotoMessageWithMetaParams:(NSDictionary*)metaParams
                                   receiverId:(NSInteger)receiverId
                                     randomId:(NSString*)randomId;

-(instancetype)initWithMessageDictionary:(NSDictionary*)dict;

-(instancetype)initWithSocketParamDict:(NSDictionary*)dict;

-(instancetype)initWithShareEventContext:(TMShareEventContext *)shareEventContext
                         withMessageType:(TMMessageType)messageType
                           withReceiverId:(NSInteger)receiverId;

//-(instancetype)initPhotoMessageWithMetaParams:(NSDictionary*)metaParams receiverId:(NSInteger)receiverId;
//-(instancetype)initWithImageData:(NSData*)imageData receiverId:(NSInteger)receiverId;

-(void)setMetadata:(NSData*)metadata;

-(void)registerNotificationForUid;

-(BOOL)isOutgoingMessage;
-(NSAttributedString*)attributedText;
-(NSString*)simpleText;
-(NSString*)messageTypeString;
-(NSString*)messageIdAsString;
-(NSDictionary*)socketConnectionParams;
-(NSDictionary*)pollingConnectionParams;
-(NSDictionary*)socketTrackingParams;
-(NSDictionary*)chatReadParams;
-(NSString*)tempMessageId;
-(BOOL)isQuizMessage;
-(NSString*)socketTrackingStatus:(NSString*)status;
-(NSString*)conversationListMessage;
-(NSDictionary*)getInsertCacheParams;
-(NSDictionary*)getUpdateCacheParamsWithTimestampStatus:(BOOL)status;
-(NSDictionary*)getMetadataUpdateCacheParams;
-(BOOL)isCurrentConversationMessage;
-(BOOL)isCuratedDealMessage;
-(BOOL)isPhotoMessage;
-(BOOL)isImageLinkMessage;
-(BOOL)isPhotoUploaded;
-(NSString*)datespotId;
-(NSString*)dealId;
-(NSString*)datespotAddress;
-(NSString*)datespotImageURLString;
-(void)setDatespotId:(NSString*)datespotId;
-(void)setDealId:(NSString*)dealId;
-(void)setDatespotImage:(NSString*)datespotImage;
-(void)setDatespotAddress:(NSString*)datespotAddress;
-(void)updateStateFromSentMessageResponse:(TMMessage*)message;
-(NSDictionary*)datespotUpdateQueryParmas;
-(NSDictionary*)datespotInsertQueryParmas;
-(NSData*)sentImageData;
-(void)cacheSendingImage;
-(NSString*)imageLinkURLString;
-(NSString*)imageLinkLandingURLString;
-(void)showPhotoUploadProgress;
-(void)removePhotoUploadProgress;
-(void)updateMetadataFromDictioanary:(NSDictionary*)metadataDictioanry;
-(void)downloadFullImage;
-(BOOL)isShareEventMessage;
-(NSString*)shareEventImageURLString;

@end


//
//  TMMessageMediaData.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMEnums.h"

@protocol TMMessageMediaData <NSObject>

- (UIView *)mediaView;

- (CGSize)mediaViewDisplaySize;

- (UIView *)mediaPlaceholderView;

- (NSUInteger)mediaHash;

@property (assign, nonatomic) BOOL appliesMediaViewMaskAsOutgoing;

-(void)setImageURLString:(NSString*)imageURLString;

@optional

-(void)setTitleText:(NSString*)titleText;
-(void)setTitleText:(NSString*)titleText subTitleText:(NSString*)subTitle;

-(void)setIsFlare:(BOOL)isFlare;

-(void)setMessageSender:(TMMessageSender)sender;

-(void)setNudgeBackgroundColor:(UIColor*)color;

-(void)updateUIState;

-(void)setImageLinkFromURLString:(NSString*)URLString;

@end

//
//  TMNudgeMediaItem.h
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMediaItem.h"
#import "TMEnums.h"

@interface TMNudgeMediaItem : TMMediaItem <TMMessageMediaData>

- (instancetype)initWithQuizIconURLString:(NSString*)imageURLString quizMessage:(NSString*)quizMessage quizType:(int) quizType;
-(void)updateMask;
@end

//
//  TMImageLinkMediaItem.h
//  TrulyMadly
//
//  Created by Ankit on 30/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMediaItem.h"

@interface TMImageLinkMediaItem : TMMediaItem <TMMessageMediaData>

-(void)setImageLinkFromURLString:(NSString*)URLString;

@end

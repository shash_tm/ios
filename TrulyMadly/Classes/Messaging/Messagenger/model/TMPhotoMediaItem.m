//
//  TMPhotoMediaItem.m
//  TrulyMadly
//
//  Created by Ankit on 14/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMPhotoMediaItem.h"
#import "TMPhotoMessageView.h"
#import "TMMessagesMediaViewBubbleImageMasker.h"
#import "TMImageDownloader.h"
#import "NSString+TMAdditions.h"
#import "TMPhotoShareContext.h"
#import "UIImageView+XLProgressIndicator.h"
#import "TMMessagingController.h"
#import "TMAnalytics.h"
#import "TMNotificationHeaders.h"


@interface TMPhotoMediaItem ()

@property(strong,nonatomic)TMPhotoMessageView *messageView;
@property(strong,nonatomic)TMPhotoShareContext *photoshareContext;
@property(assign,nonatomic)CGSize size;

@end


@implementation TMPhotoMediaItem

-(instancetype)initWithContext:(TMPhotoShareContext*)photoshareContext withMessageDeliveryStatus:(TMMessageDeliveryStatus)status {
    self = [super init];
    if(self) {
        self.deliveryStatus = status;
        self.photoshareContext = photoshareContext;
        self.messageView = [[TMPhotoMessageView alloc] initWithFrame:CGRectZero];
        self.size = CGSizeMake(220, 200);
    }
    return self;
}

- (void)dealloc
{
    self.messageView = nil;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    self.messageView = nil;
}

- (CGSize)mediaViewDisplaySize
{
    return self.size;
}

-(void)setMatchId:(NSInteger)matchId {
    _matchId = matchId;
    [self.messageView setMatchId:matchId];
}

-(void)updateUIState {
    if(self.appliesMediaViewMaskAsOutgoing) {
        UIImage *image = [self.photoshareContext getCachedSentImage];
        if(image) {
            [self.messageView setupViewWithFullImage:image];
            if(self.deliveryStatus == SENDING) {
                [self startPhotoUploadProgressTracking];
            }
        }
        else if((image == nil) && (self.deliveryStatus == SENT)){
            //download image
            [self.photoshareContext downloadAndCacheMainImage];
        }
    }
    else { //incoming message
        UIImage *jpegImage = [self.photoshareContext getCachedJpegImage];
        if(jpegImage) {
            [self.messageView setMainImage:jpegImage];
        }
        else {
            //show progress bar
            if(self.photoshareContext.downloadInProgress) {//is main image downloading
                
            }
            else { //show thumbnail image
                [self.messageView setupThumbnailViewWithPhotoShareContext:self.photoshareContext];
            }
        }
    }
}

-(void)startPhotoUploadProgressTracking {
    NSURLSessionDataTask *task = [[TMMessagingController sharedController]
                                  photoUploadTaskForKey:self.photoshareContext.photoUploadProgressKey];
    if((task != nil) && (self.deliveryStatus == SENDING)){
        [self.messageView setupViewForPhotoUploadProgress:task];
    }
}
-(void)stopPhotoUploadProgressTracking {
    [self.messageView resetViewForPhotoUploadProgress];
}

#pragma mark - Getters

-(TMPhotoMessageView*)messageView {
    if(!_messageView) {
        _messageView = [[TMPhotoMessageView alloc] initWithFrame:CGRectZero];
    }
    return _messageView;
}
#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    self.messageView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
    return self.messageView;
}

-(void)updateMask {
    if (!self.isMaskCreated) {
        self.isMaskCreated = TRUE;
        [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:self.messageView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
    }
}

-(void)downloadFullImage {
    if(![[TMMessagingController sharedController] isPhotoDownloadInProgressForURLString:self.photoshareContext.jpegImageURLString]) {
        self.photoshareContext.downloadInProgress = TRUE;
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = [UIColor whiteColor];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [[TMMessagingController sharedController] addImageDownloader:imageView forURLString:self.photoshareContext.jpegImageSize];
        
        [self.messageView setupViewForFullImageDownload:imageView];
        
        NSURL *url = [NSURL URLWithString:self.photoshareContext.jpegImageURLString];
        UIImage *image = [self.photoshareContext getThumbnailImage];
        
        NSDate* startDate = [NSDate date];
        NSMutableDictionary *eventInfoParams = [NSMutableDictionary dictionary];
        eventInfoParams[@"size"] = self.photoshareContext.jpegImageSize;
        eventInfoParams[@"matchid"] = @(self.matchId);
        NSMutableDictionary *eventParams = [NSMutableDictionary dictionary];
        eventParams[@"startDate"] = startDate;
        
        [imageView setImageWithProgressIndicatorAndURL:url
                                      placeholderImage:image
                                  imageDownloadDidFail:^(NSString* errorString,BOOL isResourceAvailableOnServer) {
         
              [self.messageView setupThumbnailViewWithPhotoShareContext:self.photoshareContext];
              [[TMMessagingController sharedController] removeImageDownloaderForURLString:self.photoshareContext.jpegImageURLString];
              self.photoshareContext.downloadInProgress = FALSE;
                                          
              [self postImageDownloadFailNotificationForMatchId:self.matchId imageStatus:isResourceAvailableOnServer];
                                          
              NSDate* endDate = [NSDate date];
              eventParams[@"endDate"] = endDate;
              if(errorString) {
                  eventInfoParams[@"reason"] = errorString;
              }
              [self trackPhotoDownloadEventAction:@"download_image"
                                         withStatus:@"fail"
                                         withParams:eventParams
                                withEventInfoParams:eventInfoParams];
                                      
        }imageDidAppearBlock:^(UIImageView *imageView) {
         
            self.photoshareContext.downloadInProgress = FALSE;
            [self.photoshareContext cacheFullJpegImage:imageView.image];
            [[TMMessagingController sharedController] removeImageDownloaderForURLString:self.photoshareContext.jpegImageURLString];
            NSDate* endDate = [NSDate date];
            eventParams[@"endDate"] = endDate;
            [self trackPhotoDownloadEventAction:@"download_image"
                                     withStatus:@"success"
                                     withParams:eventParams
                            withEventInfoParams:eventInfoParams];
        }];
    }
}


#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.messageView.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.messageView, @(self.appliesMediaViewMaskAsOutgoing)];
}

-(void)trackPhotoDownloadEventAction:(NSString*)eventAction
                        withStatus:(NSString*)status
                        withParams:(NSDictionary*)eventParams
               withEventInfoParams:(NSDictionary*)eventInfoParams{
    
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"photo_share";
    eventDictionary[@"eventAction"] = eventAction;
    eventDictionary[@"label"] = status;
    eventDictionary[@"status"] = status;
    if(eventParams) {
        [eventDictionary addEntriesFromDictionary:eventParams];
    }
    if(eventInfoParams) {
        eventDictionary[@"event_info"] = eventInfoParams;
    }
    
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}

-(void)postImageDownloadFailNotificationForMatchId:(NSInteger)matchId imageStatus:(BOOL)imageStatus {
    NSDictionary *userInfo = NULL;
   
    
    if(imageStatus) {
        userInfo = @{@"matchid":@(matchId),
                     @"msg":@"Oops! Failed to download entire image. Please try again"};
    }
    else {
        userInfo = @{@"matchid":@(matchId),
                     @"msg":@"Oops! The image you're trying to access is not available anymore."};
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:TMJPEGIMAGEDOWNLOADFAIL_NOTIFICATION object:nil userInfo:userInfo];
}

@end

//
//  TMNudgeMediaItem.m
//  TrulyMadly
//
//  Created by Ankit on 03/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNudgeMediaItem.h"
#import "TMNudgeMessageView.h"
#import "TMMessagesMediaViewBubbleImageMasker.h"
#import "UIColor+TMColorAdditions.h"


@interface TMNudgeMediaItem ()

@property (strong, nonatomic) TMNudgeMessageView *nudgeMessageView;
@property (assign, nonatomic) CGSize size;

@end

@implementation TMNudgeMediaItem

- (instancetype)initWithQuizIconURLString:(NSString*)imageURLString quizMessage:(NSString*)quizMessage quizType:(int) quizType {
    self = [super init];
    if(self) {
        self.size = CGSizeMake(200.0f, 150);
        self.nudgeMessageView = [[TMNudgeMessageView alloc] initWithFrame:CGRectMake(0, 0, self.size.width, self.size.height)];
        self.nudgeMessageView.backgroundColor = self.appliesMediaViewMaskAsOutgoing?[UIColor likeunSelectedColor]:[UIColor likeselectedColor];
        if (quizType == MESSAGETTYPE_QUIZ_NUDGE_SINGLE) {
            [self.nudgeMessageView setActionLabelText:@"Let's Play!"];
        }
        else if (quizType == MESSAGETTYPE_QUIZ_NUDGE_DOUBLE) {
            [self.nudgeMessageView setActionLabelText:@"View Answers!"];
        }
    }
    return self;
}

- (void)dealloc
{
    _nudgeMessageView = nil;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    _nudgeMessageView = nil;
}

- (CGSize)mediaViewDisplaySize
{
    return self.size;
}

#pragma mark - Setters

//- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
//{
//    [super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
//    _nudgeMessageView = nil;
//}

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    self.nudgeMessageView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
    return self.nudgeMessageView;
}
-(void)updateMask {
    if (!self.isMaskCreated) {
        self.isMaskCreated = TRUE;
        [TMMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:self.nudgeMessageView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
    }
}
-(void)setImageURLString:(NSString*)imageURLString {
    [self.nudgeMessageView setIconImageURLString:imageURLString];
}

-(void)setTitleText:(NSString*)titleText {
    [self.nudgeMessageView setTitleText:titleText];
}

-(void)setActionText:(NSString*)actionText {
    [self.nudgeMessageView setActionLabelText:actionText];
}

- (void) setIsFlare:(BOOL)isFlare {
    [self.nudgeMessageView setIsFlare:isFlare];
}

- (void) setMessageSender:(TMMessageSender) sender {
    [self.nudgeMessageView setMessageSender:sender];
}

- (void) setNudgeBackgroundColor:(UIColor*) color {
    [self.nudgeMessageView setBackgroundColor:color];
}

#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.nudgeMessageView.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.nudgeMessageView, @(self.appliesMediaViewMaskAsOutgoing)];
}


@end

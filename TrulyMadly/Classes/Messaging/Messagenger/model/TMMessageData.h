//
//  TMMessageData.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMMessageMediaData.h"


@protocol TMMessageData <NSObject>

@required

- (NSString *)senderId;
- (NSString *)timestamp;
- (BOOL)isMediaMessage;
- (BOOL) isOutgoingMessage;
- (NSUInteger)messageHash;

@optional

- (NSString *)text;

- (id<TMMessageMediaData>)media;

@end

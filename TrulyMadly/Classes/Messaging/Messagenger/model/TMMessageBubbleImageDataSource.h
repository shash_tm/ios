//
//  TMMessageBubbleImageDataSource.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TMMessageBubbleImageDataSource <NSObject>

- (UIImage *)messageBubbleImage;

@end

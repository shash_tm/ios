//
//  TMShareEventMediaItem.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/04/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMediaItem.h"

@interface TMShareEventMediaItem : TMMediaItem <TMMessageMediaData>

-(instancetype)initWithMessageType:(TMMessageType)type failedMessage:(BOOL)isFailedMessage;

@end

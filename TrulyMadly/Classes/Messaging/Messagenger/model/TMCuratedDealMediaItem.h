//
//  TMCuratedDealMediaItem.h
//  TrulyMadly
//
//  Created by Ankit on 30/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMMediaItem.h"

@interface TMCuratedDealMediaItem : TMMediaItem <TMMessageMediaData>

-(instancetype)initWithMessageType:(TMMessageType)type failedMessage:(BOOL)isFailedMessage;

@end

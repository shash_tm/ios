//
//  TMImageLinkContext.h
//  TrulyMadly
//
//  Created by Ankit on 30/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMImageLinkContext : NSObject

@property(nonatomic,strong)NSString *imageURLString;
@property(nonatomic,strong)NSString *landingURLString;

-(instancetype)initWithMetaParams:(NSDictionary*)metaParams;

@end

//
//  TMMediaItem.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 01/07/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMMessageMediaData.h"

@interface TMMediaItem : NSObject <TMMessageMediaData>

@property(assign,nonatomic) BOOL isMaskCreated;

-(void)clearCachedMediaViews;
-(void)updateMask;

@end

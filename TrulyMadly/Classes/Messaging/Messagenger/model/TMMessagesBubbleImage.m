//
//  TMMessagesBubbleImage.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesBubbleImage.h"

@implementation TMMessagesBubbleImage

- (instancetype)initWithMessageBubbleImage:(UIImage *)image {
    NSParameterAssert(image != nil);
    
    self = [super init];
    if (self) {
        _messageBubbleImage = image;
    }
    return self;
}

- (id)init
{
    NSAssert(NO, @"%s is not a valid initializer for %@. Use %@ instead.",
             __PRETTY_FUNCTION__, [self class], NSStringFromSelector(@selector(initWithMessageBubbleImage:)));
    return nil;
}

@end

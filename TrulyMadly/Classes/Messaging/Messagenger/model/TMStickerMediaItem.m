//
//  TMStickerMediaItem.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 01/07/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMStickerMediaItem.h"
#import "TMStickerImageView.h"

@interface TMStickerMediaItem ()

@property (strong, nonatomic) TMStickerImageView *stickerView;
@property (strong, nonatomic) TMSticker *sticker;

@end

@implementation TMStickerMediaItem

- (instancetype)initWithSticker:(TMSticker*) sticker {
    self = [super init];
    if(self) {
        self.sticker = sticker;
    }
    return self;
}

- (void)dealloc
{
    _stickerView = nil;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    _stickerView = nil;
}

- (CGSize)mediaViewDisplaySize
{
    return CGSizeMake(140.0f, 150.0f);
}

#pragma mark - Setters

//- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
//{
//    
//    //[super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
//    _imageView = nil;
//}

#pragma mark - Getters

-(TMStickerImageView*)stickerView {
    if(!_stickerView) {
        _stickerView = [[TMStickerImageView alloc] initWithFrame:CGRectZero];
        _stickerView.sticker = self.sticker;
    }
    return _stickerView;
}
#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    self.stickerView.frame = CGRectMake(0, 0, 140, 150);
    return self.stickerView;
}

#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.stickerView.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.stickerView, @(self.appliesMediaViewMaskAsOutgoing)];
}


@end

//
//  TMMessagesCollectionViewFlowLayout.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMMessagesCollectionView;

@interface TMMessagesCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (readonly, nonatomic) TMMessagesCollectionView *collectionView;

/**
 *  Returns the width of items in the layout.
 */
@property (readonly, nonatomic) CGFloat itemWidth;

/**
 *  The font used to display the body a text message in the message bubble of each
 *  `JSQMessagesCollectionViewCell` in the collectionView.
 *
 *  @discussion The default value is the preferred system font for `UIFontTextStyleBody`. This value must not be `nil`.
 */
@property (strong, nonatomic) UIFont *messageBubbleFont;

/**
 *  The maximum number of items that the layout should keep in its cache of layout information.
 *
 *  @discussion The default value is `200`. A limit of `0` means no limit. This is not a strict limit.
 */
@property (assign, nonatomic) NSUInteger cacheLimit;

/**
 *  Computes and returns the size of the `messageBubbleImageView` property of a `JSQMessagesCollectionViewCell`
 *  at the specified indexPath. The returned size contains the required dimensions to display the entire message contents.
 *  Note, this is *not* the entire cell, but only its message bubble.
 *
 *  @param indexPath The index path of the item to be displayed.
 *
 *  @return The size of the message bubble for the item displayed at indexPath.
 */
- (CGSize)messageBubbleSizeForItemAtIndexPath:(NSIndexPath *)indexPath;

/**
 *  Computes and returns the size of the item specified by indexPath.
 *
 *  @param indexPath The index path of the item to be displayed.
 *
 *  @return The size of the item displayed at indexPath.
 */
- (CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath;


@end

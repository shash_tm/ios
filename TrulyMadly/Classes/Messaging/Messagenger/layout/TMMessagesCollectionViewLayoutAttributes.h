//
//  TMMessagesCollectionViewLayoutAttributes.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMMessagesCollectionViewLayoutAttributes : UICollectionViewLayoutAttributes <NSCopying>

@property (assign, nonatomic) NSInteger cellIndex;

@property (strong, nonatomic) UIFont *messageBubbleFont;

@property (assign, nonatomic) CGFloat messageBubbleContainerViewWidth;

@property (assign, nonatomic) UIEdgeInsets textViewTextContainerInsets;

@property (assign, nonatomic) UIEdgeInsets textViewFrameInsets;

@property (assign, nonatomic) CGSize sentFailedIndicatorViewSize;

@property (assign, nonatomic) CGFloat cellTopLabelHeight;

@property (assign, nonatomic) CGFloat cellBottomLabelHeight;



@end

//
//  TMMessagesCollectionViewFlowLayout.m
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import "TMMessagesCollectionViewFlowLayout.h"
#import "TMMessagesCollectionViewLayoutAttributes.h"
#import "TMMessagesCollectionViewFlowLayoutInvalidationContex.h"
#import "TMMessagesCollectionView.h"
#import "TMMessageData.h"
#import "TMLog.h"


@interface TMMessagesCollectionViewFlowLayout ()

@property (strong, nonatomic) NSCache *messageBubbleCache;
@property (assign, nonatomic, readonly) NSUInteger bubbleImageAssetWidth;
@property (assign, nonatomic) CGFloat messageBubbleLeftRightMargin;
@property (assign, nonatomic) UIEdgeInsets messageBubbleTextViewFrameInsets;
@property (assign, nonatomic) UIEdgeInsets messageBubbleTextViewTextContainerInsets;
@property (assign, nonatomic) CGSize sentFailedMessageViewSize;

- (void)configureFlowLayout;
- (void)didReceiveApplicationMemoryWarningNotification:(NSNotification *)notification;
- (void)resetLayout;
- (void)configureMessageCellLayoutAttributes:(TMMessagesCollectionViewLayoutAttributes *)layoutAttributes;
- (CGSize)sentFailedIndicatorViewSizeForIndexPath:(NSIndexPath *)indexPath;

@end


@implementation TMMessagesCollectionViewFlowLayout

@dynamic collectionView;

#pragma mark - Initialization

- (void)configureFlowLayout
{
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.sectionInset = UIEdgeInsetsMake(10.0f, 4.0f, 10.0f, 4.0f);
    self.minimumLineSpacing = 4.0f;
    
    //_bubbleImageAssetWidth = [UIImage jsq_bubbleCompactImage].size.width;
    
    _messageBubbleCache = [NSCache new];
    _messageBubbleCache.name = @"TMMessagesCollectionViewFlowLayout.messageBubbleCache";
    _messageBubbleCache.countLimit = 500;
    
    _messageBubbleFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        _messageBubbleLeftRightMargin = 240.0f;
    }
    else {
        _messageBubbleLeftRightMargin = 50.0f;
    }
    
    _messageBubbleTextViewFrameInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 6.0f);
    _messageBubbleTextViewTextContainerInsets = UIEdgeInsetsMake(7.0f, 14.0f, 7.0f, 14.0f);
    _sentFailedMessageViewSize = CGSizeZero;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveApplicationMemoryWarningNotification:)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification
                                               object:nil];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureFlowLayout];
    }
    return self;
}

+ (Class)layoutAttributesClass
{
    return [TMMessagesCollectionViewLayoutAttributes class];
}

+ (Class)invalidationContextClass
{
    return [TMMessagesCollectionViewFlowLayoutInvalidationContex class];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _messageBubbleFont = nil;
    
    [_messageBubbleCache removeAllObjects];
    _messageBubbleCache = nil;
}

#pragma mark - Setters

- (void)setMessageBubbleFont:(UIFont *)messageBubbleFont
{
    if ([_messageBubbleFont isEqual:messageBubbleFont]) {
        return;
    }
    
    NSParameterAssert(messageBubbleFont != nil);
    _messageBubbleFont = messageBubbleFont;
    [self invalidateLayoutWithContext:[TMMessagesCollectionViewFlowLayoutInvalidationContex context]];
}

- (void)setMessageBubbleLeftRightMargin:(CGFloat)messageBubbleLeftRightMargin
{
    NSParameterAssert(messageBubbleLeftRightMargin >= 0.0f);
    _messageBubbleLeftRightMargin = ceilf(messageBubbleLeftRightMargin);
    [self invalidateLayoutWithContext:[TMMessagesCollectionViewFlowLayoutInvalidationContex context]];
}

- (void)setMessageBubbleTextViewTextContainerInsets:(UIEdgeInsets)messageBubbleTextContainerInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(_messageBubbleTextViewTextContainerInsets, messageBubbleTextContainerInsets)) {
        return;
    }
    
    _messageBubbleTextViewTextContainerInsets = messageBubbleTextContainerInsets;
    [self invalidateLayoutWithContext:[TMMessagesCollectionViewFlowLayoutInvalidationContex context]];
}

//-(void)setSentFailedMessageViewSize:(CGSize)sentFailedMessageViewSize {
//    if (CGSizeEqualToSize(_sentFailedMessageViewSize, sentFailedMessageViewSize)) {
//        return;
//    }
//    
//    _sentFailedMessageViewSize = sentFailedMessageViewSize;
//    [self invalidateLayoutWithContext:[TMMessagesCollectionViewFlowLayoutInvalidationContex context]];
//}

- (void)setCacheLimit:(NSUInteger)cacheLimit
{
    self.messageBubbleCache.countLimit = cacheLimit;
}

#pragma mark - Getters

- (CGFloat)itemWidth
{
    return CGRectGetWidth(self.collectionView.frame) - self.sectionInset.left - self.sectionInset.right;
}

- (NSUInteger)cacheLimit
{
    return self.messageBubbleCache.countLimit;
}

#pragma mark - Notifications

- (void)didReceiveApplicationMemoryWarningNotification:(NSNotification *)notification
{
    [self resetLayout];
}

#pragma mark - Collection view flow layout

- (void)invalidateLayoutWithContext:(TMMessagesCollectionViewFlowLayoutInvalidationContex *)context
{
    if (context.invalidateDataSourceCounts) {
        context.invalidateFlowLayoutAttributes = YES;
        context.invalidateFlowLayoutDelegateMetrics = YES;
    }
    
    if (context.invalidateFlowLayoutMessagesCache) {
        [self resetLayout];
    }
    
    [super invalidateLayoutWithContext:context];
}

- (void)prepareLayout
{
    [super prepareLayout];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *attributesInRect = [super layoutAttributesForElementsInRect:rect];
    
    [attributesInRect enumerateObjectsUsingBlock:^(TMMessagesCollectionViewLayoutAttributes *attributesItem, NSUInteger idx, BOOL *stop) {
        if (attributesItem.representedElementCategory == UICollectionElementCategoryCell) {
            [self configureMessageCellLayoutAttributes:attributesItem];
        }
        else {
            attributesItem.zIndex = -1;
        }
    }];
    
    return attributesInRect;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMMessagesCollectionViewLayoutAttributes *customAttributes = (TMMessagesCollectionViewLayoutAttributes *)[super layoutAttributesForItemAtIndexPath:indexPath];
    
    if (customAttributes.representedElementCategory == UICollectionElementCategoryCell) {
        [self configureMessageCellLayoutAttributes:customAttributes];
    }
    
    return customAttributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    CGRect oldBounds = self.collectionView.bounds;
    if (CGRectGetWidth(newBounds) != CGRectGetWidth(oldBounds)) {
        return YES;
    }
    
    return NO;
}

- (void)prepareForCollectionViewUpdates:(NSArray *)updateItems
{
    [super prepareForCollectionViewUpdates:updateItems];
    
    [updateItems enumerateObjectsUsingBlock:^(UICollectionViewUpdateItem *updateItem, NSUInteger index, BOOL *stop) {
        if (updateItem.updateAction == UICollectionUpdateActionInsert) {
            CGFloat collectionViewHeight = CGRectGetHeight(self.collectionView.bounds);
            
            TMMessagesCollectionViewLayoutAttributes *attributes = [TMMessagesCollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:updateItem.indexPathAfterUpdate];
            
            if (attributes.representedElementCategory == UICollectionElementCategoryCell) {
                [self configureMessageCellLayoutAttributes:attributes];
            }
            
            attributes.frame = CGRectMake(0.0f,
                                          collectionViewHeight + CGRectGetHeight(attributes.frame),
                                          CGRectGetWidth(attributes.frame),
                                          CGRectGetHeight(attributes.frame));
        }
    }];
}

#pragma mark - Invalidation utilities

- (void)resetLayout
{
    TMLOG(@"Resetting Messages Cache");
    [self.messageBubbleCache removeAllObjects];
}

#pragma mark - Message cell layout utilities

- (CGSize)messageBubbleSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id<TMMessageData> messageItem = [self.collectionView.dataSource collectionView:self.collectionView messageDataForItemAtIndexPath:indexPath];
    //NSLog(@"Message:%@ atIndex:%d",messageItem.text,indexPath.row);
    NSValue *cachedSize = [self.messageBubbleCache objectForKey:@([messageItem messageHash])];
    if (cachedSize != nil) {
        //TMLOG(@"Cached Final Size:%@",NSStringFromCGSize([cachedSize CGSizeValue]));
        return [cachedSize CGSizeValue];
    }
    
    CGSize finalSize = CGSizeZero;
    
    if ([messageItem isMediaMessage]) {
        finalSize = [[messageItem media] mediaViewDisplaySize];
        if(CGSizeEqualToSize(finalSize, CGSizeZero)) {
            TMLOG(@"Zero");
        }
    }
    else {
        CGSize sentFailedIndicatorSize = [self sentFailedIndicatorViewSizeForIndexPath:indexPath];
        //self.sentFailedMessageViewSize = sentFailedIndicatorSize;
        
        //  from the cell xibs, there is a 2 point space between avatar and bubble
        CGFloat spacingBetweenAvatarAndBubble = 2.0f;
        CGFloat horizontalContainerInsets = self.messageBubbleTextViewTextContainerInsets.left + self.messageBubbleTextViewTextContainerInsets.right;
        CGFloat horizontalFrameInsets = self.messageBubbleTextViewFrameInsets.left + self.messageBubbleTextViewFrameInsets.right;
        
        CGFloat horizontalInsetsTotal = horizontalContainerInsets + horizontalFrameInsets + spacingBetweenAvatarAndBubble;
        CGFloat maximumTextWidth = self.itemWidth - sentFailedIndicatorSize.width - self.messageBubbleLeftRightMargin - horizontalInsetsTotal;
        
        CGRect stringRect = [[messageItem text] boundingRectWithSize:CGSizeMake(maximumTextWidth, CGFLOAT_MAX)
                                                             options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                          attributes:@{ NSFontAttributeName : self.messageBubbleFont }
                                                             context:nil];
        
        CGSize stringSize = CGRectIntegral(stringRect).size;
        
        CGFloat verticalContainerInsets = self.messageBubbleTextViewTextContainerInsets.top + self.messageBubbleTextViewTextContainerInsets.bottom;
        CGFloat verticalFrameInsets = self.messageBubbleTextViewFrameInsets.top + self.messageBubbleTextViewFrameInsets.bottom;
        
        //  add extra 2 points of space, because `boundingRectWithSize:` is slightly off
        //  not sure why. magix. (shrug) if you know, submit a PR
        CGFloat verticalInsets = verticalContainerInsets + verticalFrameInsets + 2.0f;
        
        //  same as above, an extra 2 points of magix
        CGFloat finalWidth = MAX(stringSize.width + horizontalInsetsTotal, self.bubbleImageAssetWidth) + 2.0f;
        
        finalSize = CGSizeMake(finalWidth, stringSize.height + verticalInsets);
        
    }
    
    //NSLog(@"Final Size:%@ for Text:%@",NSStringFromCGSize(finalSize),messageItem.text);
    [self.messageBubbleCache setObject:[NSValue valueWithCGSize:finalSize] forKey:@([messageItem messageHash])];
    
    return finalSize;
}

- (CGSize)sentFailedIndicatorViewSizeForIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = [self.collectionView.dataSource collectionView:self.collectionView sentFailedIndicatorSizeAtIndexPath:indexPath];
    return size;
}

- (CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize messageBubbleSize = [self messageBubbleSizeForItemAtIndexPath:indexPath];
    TMMessagesCollectionViewLayoutAttributes *attributes = (TMMessagesCollectionViewLayoutAttributes *)[self layoutAttributesForItemAtIndexPath:indexPath];
    
    CGFloat finalHeight = messageBubbleSize.height;
    finalHeight += attributes.cellTopLabelHeight;
    finalHeight += attributes.cellBottomLabelHeight;
    
    return CGSizeMake(self.itemWidth, ceilf(finalHeight));
}

- (void)configureMessageCellLayoutAttributes:(TMMessagesCollectionViewLayoutAttributes *)layoutAttributes
{
    NSIndexPath *indexPath = layoutAttributes.indexPath;
    
    CGSize messageBubbleSize = [self messageBubbleSizeForItemAtIndexPath:indexPath];
    if(messageBubbleSize.width == 0) {
        TMLOG(@"Zero Width");
    }
    layoutAttributes.messageBubbleContainerViewWidth = messageBubbleSize.width;
    
    layoutAttributes.textViewFrameInsets = self.messageBubbleTextViewFrameInsets;
    
    layoutAttributes.textViewTextContainerInsets = self.messageBubbleTextViewTextContainerInsets;
    
    layoutAttributes.messageBubbleFont = self.messageBubbleFont;
    
    layoutAttributes.sentFailedIndicatorViewSize = [self.collectionView.delegate collectionView:self.collectionView
                                                                                         layout:self
                                                               sizeForSentFailedMessageIndicatorViewAtIndexPath:indexPath];
    
    layoutAttributes.cellTopLabelHeight = [self.collectionView.delegate collectionView:self.collectionView
                                                                                layout:self
                                                      heightForCellTopLabelAtIndexPath:indexPath];
    
    layoutAttributes.cellBottomLabelHeight = [self.collectionView.delegate collectionView:self.collectionView
                                                                                   layout:self
                                                      heightForCellBottomLabelAtIndexPath:indexPath];
}

@end

//
//  TMMessagesCollectionViewFlowLayoutInvalidationContex.h
//  AutoLayoutCollectionViewDemo
//
//  Created by Ankit on 29/06/15.
//  Copyright (c) 2015 TrulyMadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMMessagesCollectionViewFlowLayoutInvalidationContex : UICollectionViewFlowLayoutInvalidationContext

@property (nonatomic, assign) BOOL invalidateFlowLayoutMessagesCache;

+ (instancetype)context;

@end

//
//  TMSocketSession.h
//  TrulyMadly
//
//  Created by Ankit on 20/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMSocketSession : NSObject

+(void)setSocketConnectionParameterFromDictionary:(NSDictionary*)socketParameters;

+(NSString*)socketURLString;
+(NSString*)socketIPString;
+(NSString*)abPrefixString;
+(BOOL)forceWebSocket;
+(NSInteger)socketConnectionTimeout;
+(NSInteger)getMissedMessageTimeout;
+(NSInteger)chatsentTimeout;
+(void)clearAllCachedData;

+(void)markClearChatAPIStatusAsComplete;
+(BOOL)isClearChatAPIStatusMarkedAsComplete;

@end

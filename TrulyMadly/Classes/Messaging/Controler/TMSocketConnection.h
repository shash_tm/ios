//
//  TMSocketConnection.h
//  TrulyMadly
//
//  Created by Ankit on 06/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMChatConnection.h"
#import "TMEnums.h"

@class TMMessage;
@class TMSocketConnectionConfig;

@interface TMSocketConnection : NSObject <TMChatConnection>

@property(nonatomic,weak)id<TMSocketConnectionDelegate> delegate;
@property(nonatomic,assign)BOOL isConnectionInProgress;

//-(instancetype)initWithConnectionCounter:(NSInteger)counter
//                    withSuffixIdentifier:(NSString*)suffixIdentifier;
-(instancetype)initWithConnectionConfig:(TMSocketConnectionConfig*)connectionConfig;

-(BOOL)isConnected;
-(void)connect;
-(void)close;
-(void)getMissedMessages:(NSDictionary*)params;
-(void)getFirstTimeMessages:(NSDictionary*)params;
-(void)getFirstPhotoShareTimeMessages:(NSDictionary*)params;
-(void)sendMessage:(TMMessage*)message;
-(void)sendMessages:(NSArray*)messages;
-(void)getChatMetaData:(NSString*)matchId;
-(void)getUserMetaData:(NSString*)matchId;
-(void)fireChatRead:(TMMessage*)message;
-(void)fireMutualMatchChatRead:(NSDictionary*)params;
-(void)fireChatTypingWithParams:(NSDictionary*) params;
-(void)printSid;
-(void)trackConnectionErrorStatus:(NSString*)status;
-(void)trackConnectionPingStatus:(BOOL)status;

@end

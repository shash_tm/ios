//
//  TMPollingConnection.m
//  TrulyMadly
//
//  Created by Ankit on 06/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPollingConnection.h"
#import "TMMessageManager.h"
#import "TMMessage.h"
#import "TMError.h"
#import "TMLog.h"
#import "TMMessageResponse.h"
#import "TMAnalytics.h"


#define POLLING_TIMER_INTERVAL   5
@interface TMPollingConnection ()

@property(nonatomic,strong)NSString *pollingURLString;
@property(nonatomic,weak)NSTimer *messagePollingTimer;
@property(nonatomic,assign)BOOL isPollInProgress;
@property(nonatomic,assign)BOOL isSendMessageInProgress;

@end


@implementation TMPollingConnection

@synthesize connectionURL;

-(TMMessageManager*)messageManager {
    if(!_messageManager) {
        _messageManager = [[TMMessageManager alloc] init];
    }
    return _messageManager;
}

-(BOOL)isNetworkReachable {
    return self.messageManager.isNetworkReachable;
}

-(void)configurePollingURL:(NSString*)URLString {
    NSParameterAssert(URLString != nil);
    self.pollingURLString = URLString;
    self.connected = TRUE;
}
-(void)connect {
    [self startPolling];
}
-(void)disconnect {
    TMLOG(@"disconnect polling");
    self.pollingURLString = nil;
    [self stopPolling];
    self.connected = FALSE;
}
-(void)getDoodleLinkWithURLString:(NSString*)URLString {
    [self.messageManager doodleLinkFromURLString:URLString response:^(NSString *doodleLink) {
        [self.delegate pollingDidReceiveDoodleLink:doodleLink];
    }];
}
-(void)sendMessage:(TMMessage*)message {
    TMLOG(@"P:Send Message With Parmas:%@ URL:%@ FCL:%@",[message pollingConnectionParams],self.pollingURLString,message.fullConversationLink);
    NSString *urlString = nil;
    if(message.fullConversationLink) {
        urlString = message.fullConversationLink;
    }
    else {
        urlString = self.pollingURLString;
    }
    
    if(urlString) {
        [self trackSendMessageWithStatus:@"sent"];
        self.isSendMessageInProgress = TRUE;
        [self.messageManager sendMessage:urlString
                                  params:[message pollingConnectionParams]
                            withResponse:^(NSDictionary *responseDict, TMError *error) {
                                self.isSendMessageInProgress = FALSE;
                                if(responseDict) {
                                    [self trackSendMessageWithStatus:@"ack_received"];
                                    TMLOG(@"Send Message Response:%@",responseDict);
                                    BOOL isBlocked = FALSE;
                                    NSDictionary *dataDict = responseDict[@"data"];
                                    if(dataDict) {
                                        isBlocked = [dataDict[@"is_blocked"] boolValue];
                                    }
                                    if(isBlocked) {
                                        [self.delegate pollingDidReceiveBlockUserResponse];
                                    }
                                    else if([responseDict[@"error"] boolValue] == TRUE) {
                                        TMMessage *badMessage = [[TMMessage alloc] initWithBadWordMessageDictionary:responseDict];
                                        [self.delegate pollingDidReceiveSentMesageResponseWithBadWords:badMessage];
                                    }
                                    else {
                                        [self.delegate pollingDidReceiveSentMesageResponse:responseDict];
                                    }
                                }
                                else {
                                    [self trackSendMessageWithStatus:@"ack_failed"];
                                    TMLOG(@"Send Message Error:%@",error);
                                    message.sentFailRetryCount = message.sentFailRetryCount + 1;
                                    [self.delegate pollingDidFailToReceiveSentMessageResponse:message withError:error];
                                }
                            }];
    }
}
-(void)blockUser:(NSString*)reason withURL:(NSString*)URLString {
    [self.messageManager blockUserWithReason:reason withURL:URLString response:^(BOOL status) {
        [self.delegate pollingDidReceiveBlockUserResponse:status];
    }];
}
-(void)checkNetworkStatusWithPing {
    [self.messageManager pingForNetworkStatus:^(BOOL status) {
        [self.delegate pollingDidReceiveNetworkPingWithStatus:status];
    }];
}
-(void)fetchConversationListData:(void(^)(NSArray *result))resultBlock {
    [self.messageManager fetchConversationList:^(NSArray *conversationList, TMError *error) {
        if(conversationList) {
            resultBlock(conversationList);
        }
        else {
            resultBlock(nil);
        }
    }];
}

#pragma mark - Internal Methods

-(void)startPolling {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self fetchNewMessages];
        
        //schedule timer for next polling
        self.messagePollingTimer = [NSTimer scheduledTimerWithTimeInterval:POLLING_TIMER_INTERVAL
                                                                    target:self
                                                                  selector:@selector(fetchNewMessages)
                                                                  userInfo:nil
                                                                   repeats:YES];
    });
}
-(void)stopPolling {
    if(self.messagePollingTimer) {
        [self invalidatePollingTimer];
        self.messagePollingTimer = nil;
        self.isPollInProgress = FALSE;
    }
}

-(void)invalidatePollingTimer {
    if(self.messagePollingTimer){
        [self.messagePollingTimer invalidate];
        self.messagePollingTimer = nil;
    }
}
-(void)fetchNewMessages {
    TMLOG(@"PU:%@ ISPOLINPROGRESS:%d ISCACHEUPINPROGRESS:%d ISSENDMESSAGEINPROGRESS:%d",self.pollingURLString,self.isPollInProgress,
                                                                                        self.cacheUpdateInProgress,self.isSendMessageInProgress);
    
    if((self.pollingURLString) && (!self.isPollInProgress) && (!self.cacheUpdateInProgress) && (!self.isSendMessageInProgress)) {
        self.isPollInProgress = TRUE;
        [self getMessage];
    }
    else {
        TMLOG(@"Can not execute fetchNewMessages");
    }
}
-(void)getMessage {
    [self.messageManager getChatMessages:self.pollingURLString
                                  params:[self.delegate paramsForMissedMessagesRequest]
                                response:^(TMMessageResponse *response, TMError *tmError) {
        self.isPollInProgress = FALSE;
        if(response && (!self.isSendMessageInProgress)) {
            [self.delegate pollingDidReceiveUnreadMessages:response];
        }
        else {
            TMLOG(@"Error in fetching Missed Messages using polling:%@",tmError);
        }
    }];
}


-(void)trackSendMessageWithStatus:(NSString*)status {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"socket";
    eventDictionary[@"eventAction"] = @"chat_sent_polling";
    eventDictionary[@"label"] = status;
    eventDictionary[@"GA"] = @"true";
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}
@end

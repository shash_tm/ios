//
//  TMSocketConnectionConfig.m
//  TrulyMadly
//
//  Created by Ankit on 17/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMSocketConnectionConfig.h"
#import "TMSocketSession.h"


@interface TMSocketConnectionConfig ()

@end

@implementation TMSocketConnectionConfig

-(instancetype)init {
    self = [super init];
    if(self) {
        self.suffixIdentifier = nil;
        self.isErrorMode = FALSE;
        self.connectionCounter = 0;
    }
    return self;
}
-(BOOL)forceWebSocket {
    BOOL status = [TMSocketSession forceWebSocket];
    return status;
}
-(NSString*)connectionURLString {
    NSString *URLString = nil;
    if(self.isErrorMode) {
        URLString = [TMSocketSession socketIPString];
    }
    else {
        URLString = [TMSocketSession socketURLString];
    }
    //NSParameterAssert(URLString == nil);
    return URLString;
}

-(void)setConnectionParamForErrorReason:(SocketConnectionFailReason)errorReason {
    if(errorReason == ACKTIMEOUT) {
        self.suffixIdentifier = @"ack_timed_out";
    }
    else if(errorReason == ERROR) {
        self.suffixIdentifier = @"error";
        self.isErrorMode = TRUE;
    }
}

@end

//
//  TMCacheBlockOperation.h
//  TrulyMadly
//
//  Created by Ankit on 03/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMChatContext;

@interface TMCacheBlockOperation : NSBlockOperation

//@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,strong,readonly)TMChatContext *chatContext;


-(instancetype)initWithMatchId:(NSNumber*)matchId;
-(void)setContext:(TMChatContext*)context;

@end

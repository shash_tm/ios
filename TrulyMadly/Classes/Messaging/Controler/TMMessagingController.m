//
//  TMMessagingController.m
//  TrulyMadly
//
//  Created by Ankit on 15/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessagingController.h"
#import "TMSocketConnection.h"
#import "TMPollingConnection.h"
#import "TMChatCacheController.h"
#import "TMChatConnectionSetting.h"
#import "TMQuizManager.h"
#import "TMSocketTracking.h"
#import "TMStickerManager.h"
#import "TMChatSeenContext.h"
#import "TMMessageResponse.h"
#import "TMChatContext.h"
#import "TMUserSession.h"
#import "TMChatUser.h"
#import "TMMessage.h"
#import "TMTimestampFormatter.h"
#import "TMDataStore.h"
#import "TMCommonUtil.h"
#import "TMConversation.h"
#import "TMNotificationHeaders.h"
#import "TMMessageManager.h"
#import "TMSocketSession.h"
#import "TMSocketConnectionConfig.h"
#import "TMDataStore+TMAdAddtions.h"
#import "TMStickerManager.h"
#import "TMImageDownloader.h"
#import "TMImageUploader.h"
#import "TMPhotoShareContext.h"
#import "TMFileManager.h"
#import "TMSparkManager.h"
#import "TMSpark.h"
#import "TMError.h"
#import "TMEnums.h"
#import "TMLog.h"


#define CONTINOUS_SEND_MESSAGE_FAIL_MAXLIMIT  1
#define MESSAGE_FAIL_RETRY_MAXLIMIT           2

@interface TMMessagingController ()<TMSocketConnectionDelegate,TMPollingConnectionDelegate>

@property(nonatomic,strong)TMSocketConnection *socketConnection;
@property(nonatomic,strong)TMPollingConnection *pollingConnection;
@property(nonatomic,strong)TMChatCacheController *cacheController;
@property(nonatomic,strong)TMChatContext *chatContext;
@property(nonatomic,strong)TMChatConnectionSetting *connectionSetting;
@property(nonatomic,strong)NSMutableDictionary *receiveMessageTempCache;
@property(nonatomic,strong)NSMutableDictionary *photoUploaderCache;
@property(nonatomic,strong)NSMutableDictionary *photoDownloaderCache;
@property(nonatomic,strong)NSMutableArray *sendingQueue;
@property(nonatomic,strong)NSString *lastChatFetchIdentfier;
@property(nonatomic,strong)NSString *chatURLString;
@property(nonatomic,assign)NSInteger sentMessageFailCounter;
@property(nonatomic,assign)BOOL sendingQueueInProgress;
@property(nonatomic,assign)BOOL isFirstPollingRequestInProgress;
@property(nonatomic,assign)BOOL pingRequestStatus;
@property(nonatomic,assign)SocketConnectionFailReason socketConnectionFailureReason;
@property(nonatomic,weak)NSTimer *chatSendTimer;
@property(nonatomic,strong)TMStickerManager* stickerManager;
@property(nonatomic,strong)TMSparkManager *sparkManager;
@property(nonatomic,weak)NSTimer *sparkTimer;

@end

@implementation TMMessagingController

#pragma mark - Initializer
#pragma mark -


-(instancetype)init {
    self = [super init];
    if(self) {
        self.connectionSetting = [[TMChatConnectionSetting alloc] init];
        [self registerNotification];
        [self updateQuiz];
    }
    return self;
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.sendingQueue removeAllObjects];
    [self resetMessageSendingTimer];
}

-(TMPollingConnection*)pollingConnection {
    if(!_pollingConnection) {
        _pollingConnection = [[TMPollingConnection alloc] init];
        _pollingConnection.delegate = self;
    }
    return _pollingConnection;
}
-(TMChatCacheController*)cacheController {
    if(!_cacheController) {
        _cacheController = [[TMChatCacheController alloc] init];
    }
    return _cacheController;
}
-(NSMutableArray*)sendingQueue {
    if(!_sendingQueue) {
        _sendingQueue = [NSMutableArray arrayWithCapacity:8];
    }
    return _sendingQueue;
}
-(NSMutableDictionary*)receiveMessageTempCache {
    if(!_receiveMessageTempCache) {
        _receiveMessageTempCache = [NSMutableDictionary dictionaryWithCapacity:8];
    }
    return _receiveMessageTempCache;
}
-(NSMutableDictionary*)photoUploaderCache {
    if(!_photoUploaderCache) {
        _photoUploaderCache = [NSMutableDictionary dictionaryWithCapacity:4];
    }
    return _photoUploaderCache;
}
-(NSMutableDictionary*)photoDownloaderCache {
    if(!_photoDownloaderCache) {
        _photoDownloaderCache = [NSMutableDictionary dictionaryWithCapacity:4];
    }
    return _photoDownloaderCache;
}
-(TMSparkManager*)sparkManager {
    if(!_sparkManager) {
        _sparkManager = [[TMSparkManager alloc] init];
    }
    return _sparkManager;
}
-(void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChanged:) name:TMNETWORKCHANGE_NOTIFICATION object:nil];
}

#pragma mark - Public Methods
#pragma mark -

+(instancetype)sharedController {
    static TMMessagingController *messagingController = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        messagingController = [[TMMessagingController alloc] init];
    });
    return messagingController;
}

-(void)setupChatWithContext:(TMChatContext*)context conversationURLString:(NSString*)URLString {
    NSParameterAssert(URLString != nil);
    self.chatContext = context;
    self.chatURLString = URLString;
    [self updateChatSessionStatus:TRUE];
    [self configureCacheForConversation];
    [self configureConnection];
}
-(void)resetChatContext {
    self.chatContext = nil;
    self.chatURLString = nil;
    self.lastChatFetchIdentfier = nil;
    [self updateChatSessionStatus:FALSE];
    if(self.receiveMessageTempCache) {
        [self.receiveMessageTempCache removeAllObjects];
        self.receiveMessageTempCache = nil;
    }
    if([self.pollingConnection connected]) {
        [self executePollingDisConnect];
    }
}
-(void)setChatSessionActive:(BOOL)isActive {
    [self updateChatSessionStatus:isActive];
}
-(void)connectChat {
    [self.connectionSetting setupConnectionSettings];
    [self configureConnection];
    [self resetConversationList];
}
-(void)disconnectChat {
    TMLOG(@"disconnect chat");
    [self resetConnection];
}
-(void)sendMessage:(TMMessage*)message {
    if([self canConnect] && (self.connectionSetting.activeChatConnection != CHATCONNECTION_NA)) {
        BOOL canSend = TRUE;
        if(([self.connectionSetting isSocketConnectionActive]) && (self.connectionSetting.socketConnectionState != Connected)) {
            canSend = FALSE;
        }

        if(canSend) {
            [self updateWillSendMessageEvent:message];
            if(!message.isFailedMessageRetry) {
                [self cacheSendMessage:message];
            }
            
            //as in case of photo
            if([message isPhotoMessage]){
                if([message isPhotoUploaded]) {
                    [self executeSendMessage:message];
                }
                else {
                    [self uploadImageForMessage:message];
                }
            }
            else {
                [self executeSendMessage:message];
            }
        }
        else {
            [self updateWillNotSendMessageEvent:message];
        }
    }
    else {
        [self updateWillNotSendMessageEvent:message];
    }
}
-(void)sendMessages:(NSArray*)messages {
    for (int i=0; i<messages.count; i++) {
        TMMessage* message = messages[i];
        [self sendMessage:message];
    }
}
-(void)deleteMessage:(TMMessage*)message {
    [self.cacheController deleteMessage:message];
    //if photo share message then delete cached image
    if([message isPhotoMessage]) {
        [message.photoshareContext deleteCachedImage];
    }
}

-(void)fireChatRead:(TMMessage*)message {
    if(message.matchId == self.chatContext.matchId) {
        [self executeChatReadEmit:message];
    }
}
-(void)fireMutualMatchChatRead:(NSInteger)matchId {
    NSDictionary *params = @{@"sender_id":@(matchId)};
    [self executeSocketEmit:EMIT_MUTUALMATCHCHATREAD withParmas:params];
}
-(void)fireChatTyping {
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)self.chatContext.matchId],@"receiver_id",nil];
    
    [self executeSocketChatTypingwithParams:params];
}
-(void)blockUserWithReason:(NSString*)blockReason {
    [self.pollingConnection blockUser:blockReason withURL:self.chatURLString];
}
-(void)clearChatTillMessage:(TMMessage*)message withURLString:(NSString*)URLString response:(void(^)(BOOL status))resultBlock {
    __block NSInteger matchId = self.chatContext.matchId;
    __block NSInteger userId = self.chatContext.loggedInUserId;
    [self.pollingConnection.messageManager deleteConversation:message.messageId withURL:URLString response:^(BOOL status) {
        if(status) {
            TMChatContext *context = [[TMChatContext alloc] init];
            context.loggedInUserId = userId;
            context.matchId = matchId;
            [self.cacheController deleteMessageWithContext:context tillTimestamp:message.timestamp];
            [self removeCachedSharedPhotosAtPath:[NSString stringWithFormat:@"%@-%@",@(userId),@(matchId)]];
            [self postSocketConnectionNotificationWithStatus];
        }
        resultBlock(status);
    }];
}
-(void)fetchAndCacheDeletedConversationData {
    [self.pollingConnection.messageManager getDeletedConversationListWihtResponse:^(NSArray *deletedConversationList) {
        [TMSocketSession markClearChatAPIStatusAsComplete];
        for (int i=0; i<deletedConversationList.count; i++) {
            NSDictionary *dictionary = deletedConversationList[i];
            TMChatContext *chatContext = [[TMChatContext alloc] init];
            NSInteger userId1 = [dictionary[@"user_id_1"] integerValue];
            NSInteger userId2 = [dictionary[@"user_id_2"] integerValue];
            if([[TMUserSession sharedInstance].user.userId integerValue] == userId1) {
                chatContext.loggedInUserId = userId1;
                chatContext.matchId = userId2;
            }
            else {
                chatContext.loggedInUserId = userId2;
                chatContext.matchId = userId1;
            }
            NSString *timestamp = dictionary[@"time_stamp"];
            [self.cacheController updateDeleteConversationMetadata:chatContext tillTimestamp:timestamp];
        }
    }];
}
-(void)fetchAndUpdateUnreadConversationListCount:(void(^)(BOOL status))resultBlock {
    if([self canConnect]) {
        __block BOOL firstRequestStatus = (![TMDataStore retrieveBoolforKey:@"firstconvlistreq_status"]);
        
        [self.pollingConnection fetchConversationListData:^(NSArray *result) {
            if(result && [result isKindOfClass:[NSArray class]] && result.count) {
                if(firstRequestStatus) {
                    firstRequestStatus = FALSE;
                    [TMDataStore setObject:[NSNumber numberWithBool:TRUE] forKey:@"firstconvlistreq_status"];
                }
                [self saveConversationList:result];
                [self showUnreadConversationBlooper:^(BOOL status) {
                    resultBlock(status);
                }];
            }
        }];
    }
}
-(void)showUnreadConversationBlooper:(void(^)(BOOL status))resultBlock {
    [self.cacheController getCachedUnreadConversationCount:^(NSInteger unreadConversation) {
        BOOL status = (unreadConversation) ? TRUE : FALSE;
        resultBlock(status);
    }];
}
-(NSString*)getCurrentMatchId {
    NSString *currentmatchId = nil;
    if(self.chatContext) {
        currentmatchId = [NSString stringWithFormat:@"%ld",(long)self.chatContext.matchId];
    }
    return currentmatchId;
}
-(void)getConversationData:(void(^)(NSDictionary *conversationData))resultBlock {
    [self.cacheController getCachedConversationList:^(NSDictionary *coversationData) {
        resultBlock(coversationData);
    }];
}
-(void)getConversationDataForShare:(void(^)(NSDictionary *conversationData))resultBlock {
    [self.cacheController getCachedConversationListForShare:^(NSDictionary *coversationData) {
        resultBlock(coversationData);
    }];
}
-(void)saveConversationList:(NSArray *)conversationList {
    [self.cacheController cacheConversationListData:conversationList];
}
-(void)saveConversationList:(NSArray*)conversationList newConversationData:(void(^)(NSDictionary *conversationData))resultBlock {
    [self.cacheController cacheConversationListData:conversationList updatedData:^(NSDictionary *coversationData) {
        resultBlock(coversationData);
    }];
}
-(void)executeBlockUserCall:(NSString*)URLString {
    [self.pollingConnection.messageManager sendBlockUserCall:URLString];
}
-(void)markConversationAsBlockedAndShown:(TMChatContext*)chatContext {
    if(chatContext) {
        [self removeCachedSharedPhotosAtPath:[NSString stringWithFormat:@"%@-%@",@(chatContext.loggedInUserId),@(chatContext.matchId)]];
        [self.cacheController updateConversationAsBlockedShown:chatContext];
    }
}
-(void)archiveConversation:(TMConversation*)conversationMessage {
    [self.cacheController archiveChatConversation:conversationMessage];
}
-(void)markConversationAsRead:(TMChatContext*)chatContext {
    if(chatContext) {
        [self.cacheController markConversationAsRead:chatContext];
    }
}
-(void)cancelPhotoMessgaesInSendingStateAndMarkAsFailed {
    NSArray *keys = [self.photoUploaderCache allKeys];
    for (NSString *key in keys) {
        TMImageUploader *imageUploader = self.photoUploaderCache[key];
        [imageUploader cancelUploadTask];
        imageUploader = nil;
        [self.photoUploaderCache removeObjectForKey:key];
    }
    [self.cacheController markPhotoMessgaesInSendingStateAsFailed];
}
-(void)markConversationsAsUnRead:(NSArray*)unreadConversations {
    if(unreadConversations.count && [unreadConversations isKindOfClass:[NSArray class]]) {
        for (int i=0; i<unreadConversations.count; i++) {
            TMChatContext *context = [[TMChatContext alloc] init];
            NSInteger matchId = [unreadConversations[i] integerValue];
            context.matchId = matchId;
            context.loggedInUserId = [[TMUserSession sharedInstance].user.userId integerValue];
            [self.cacheController markConversationAsUnRead:context];
        }
    }
}
-(void)setDealIconVisibilityStatus:(BOOL)iconVisibilityStatus {
    //update cache for deal status
    [TMDataStore setBool:iconVisibilityStatus forKey:@"com.deal.icon"];
}
-(void)setDealIconClickStatus:(BOOL)clickStatus withMatch:(NSString*)matchId {
    //delegate deal status
    //update cache for deal status
    TMDealState dealState = (clickStatus) ? DealStateEnabled : DealStateEnabledWithAlert;
    [self.cacheController setDealState:dealState withMatchId:matchId];
    if(self.chatContext && self.chatContext.matchId == [matchId integerValue]) {
        [self.delegate didReceiveDealStateUpdate:dealState];
    }
}
-(BOOL)canShowDealButton {
    return FALSE; //[TMDataStore retrieveBoolforKey:@"com.deal.icon"];
}
-(BOOL)canPhotoShareButton {
    return [TMDataStore retrieveBoolforKey:@"com.photoshare.status"];
}
-(BOOL)canPhotoShareButtonToMsTm {
    return [TMDataStore retrieveBoolforKey:@"com.photosharetomstm.status"];
}
-(void)deleteAllMessageContent {
    [self.cacheController deleteAllMessageContent];
    [TMDataStore removeObjectforKey:@"create_miss_tm"];
    [self removeCachedSharedPhotosAtPath:nil];
}
-(BOOL)currentNetworkConnectionStatus {
    return [self canConnect];
}
-(void)isMatchMsTm:(NSInteger)matchId result:(void(^)(BOOL isMatchTm))resultBlock {
    [self.cacheController getMatchWithId:matchId result:^(TMChatUser *chatUser) {
        BOOL status = FALSE;
        if(chatUser.isMsTM) {
            status = TRUE;
        }
        resultBlock(status);
    }];
}
-(BOOL)isSenderIdMsTm:(NSInteger)matchId {
    NSString *msTMID = [TMDataStore retrieveObjectforKey:@"com.mstm.userid"];
    if(msTMID.integerValue == matchId) {
        return TRUE;
    }
    return FALSE;
}
-(void)uploadImageForMessage:(TMMessage*)message {
    
    TMImageUploader *imageUploader = [self.photoUploaderCache objectForKey:message.randomId];
    if(!imageUploader) {
        imageUploader = [[TMImageUploader alloc] init];
        NSDictionary *params = @{@"action":@"upload_image",@"match_id":@(message.matchId)};
        NSData *data = [message sentImageData];//[context getCacheImageDataAtPath:context.fullImageCachePath];
        [imageUploader uploadImageData:data withIdentifier:message.randomId withImageSource:message.imageSource withParams:params response:^(NSDictionary *responseDictionary, TMError *error) {
            if(responseDictionary && !error) {
                NSDictionary *metaParams = responseDictionary[@"urls"];
                NSMutableDictionary *updatedMetaParmas = [NSMutableDictionary dictionaryWithDictionary:metaParams];
                [updatedMetaParmas setObject:imageUploader.keyIdentifier forKey:@"imagename"];
                NSInteger matchId = [imageUploader.inputParams[@"match_id"] integerValue];
                
                //need to pass randomid
                TMMessage *message = [[TMMessage alloc] initPhotoMessageWithMetaParams:updatedMetaParmas
                                                                            receiverId:matchId
                                                                              randomId:imageUploader.keyIdentifier];
                //update cache with new metaparams
                //send message
                [self updateMetaParamsForMessage:message];
                [self executeSendMessage:message];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(self.chatContext && self.chatContext.matchId == message.matchId) {
                        if([self.delegate respondsToSelector:@selector(didCompletePhotoUploadForMessage:)]) {
                            [self.delegate didCompletePhotoUploadForMessage:message];
                        }
                    }
                });
            }
            else {
                //update metaparams and mark message as fail
                message.deliveryStatus = SENDING_FAILED;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(self.chatContext && self.chatContext.matchId == message.matchId) {
                        [self.delegate message:message sendingFailedWithError:nil];
                    }
                });
                [self updateCacheForSendMessageResponse:message updateConnectionIdentifier:FALSE];
            }
            [self.photoUploaderCache removeObjectForKey:imageUploader.keyIdentifier];
        }];
        
        [self.photoUploaderCache setObject:imageUploader forKey:message.randomId];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self.chatContext && self.chatContext.matchId == message.matchId) {
                if([self.delegate respondsToSelector:@selector(willStartPhotoUploadForMessage:)]) {
                    [self.delegate willStartPhotoUploadForMessage:message];
                }
            }
        });
    }
}
-(NSURLSessionDataTask*)photoUploadTaskForKey:(NSString*)keyIdentifier {
    TMImageUploader *imageUploader = [self.photoUploaderCache objectForKey:keyIdentifier];
    return imageUploader.uploadTask;
}
-(void)cancelPhotoUploadMessage:(TMMessage*)message {
    TMImageUploader *imageUploader = [self.photoUploaderCache objectForKey:message.randomId];
    [imageUploader cancelUploadTask];
    imageUploader = nil;
    [self.photoUploaderCache removeObjectForKey:message.randomId];
    message.deliveryStatus = SENDING_FAILED;
    [self updateCacheForSendMessageResponse:message updateConnectionIdentifier:FALSE];
    
    dispatch_async(dispatch_get_main_queue(), ^{
            if([self.delegate respondsToSelector:@selector(didFailPhotoUploadForMessage:)]) {
                [self.delegate didFailPhotoUploadForMessage:message];
            }
            [self.delegate message:message sendingFailedWithError:nil];
    });
}
-(BOOL)isPhotoDownloadInProgressForURLString:(NSString*)URLString {
    UIImageView *imageView = [self.photoDownloaderCache objectForKey:URLString];
    if(imageView) {
        return TRUE;
    }
    return FALSE;
}
-(void)removeImageDownloaderForURLString:(NSString*)URLString {
    [self.photoDownloaderCache removeObjectForKey:URLString];
}
-(void)addImageDownloader:(UIImageView*)imageView forURLString:(NSString*)URLString {
    [self.photoDownloaderCache setObject:imageView forKey:URLString];
}
-(void)fetchAndCacheDoodleLinkForUser:(TMChatUser*)chatUser {
    if(chatUser.doodleLink) {
        if(self.delegate && [self.delegate respondsToSelector:@selector(didReceiveDoodleLink:)]) {
            [self.delegate didReceiveDoodleLink:chatUser.doodleLink];
        }
    }
    else {
        if(self.chatURLString) {
            [self.pollingConnection getDoodleLinkWithURLString:self.chatURLString];
        }
    }
}

#pragma mark SPARK
#pragma mark -

-(void)sendSparkMessage:(NSString*)sparkMessage
                toMatch:(NSString*)matchId
              sparkHash:(NSString*)sparkHash
isHasLikedBeforeProfile:(BOOL)hasLikedBeforeProfile
               response:(void(^)(BOOL status,NSString* errorDescription))responseBlock {
    
    NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
    queryParams[@"receiver_id"] = matchId;
    queryParams[@"unique_id"] = @"gfjvgfjv1424";
    queryParams[@"spark_hash"] = sparkHash;
    queryParams[@"action"] = @"send_spark";
    queryParams[@"message"] = sparkMessage;
    queryParams[@"has_liked_before"] = hasLikedBeforeProfile ? @"true" : @"false";
    
    [self.sparkManager sendSparkWithParams:queryParams with:^(NSDictionary *response, TMError *error) {
        if(response){
            NSDictionary *messageDictionary = response[@"message"];
            if(messageDictionary && [messageDictionary isKindOfClass:[NSDictionary class]]) {
                TMMessage *sparkMessage = [[TMMessage alloc] initSparkSendMessageWithText:messageDictionary[@"msg"]
                                                                                messageId:[messageDictionary[@"msg_id"] integerValue]
                                                                               receiverId:[messageDictionary[@"receiver_id"] integerValue]
                                                                                 senderId:[messageDictionary[@"sender_id"] integerValue]
                                                                                messageTs:messageDictionary[@"time"]];
                [self.cacheController cacheNewUnreadMessages:sparkMessage];
            }
            NSString *strCount = response[@"sparks_left"];
            TMUserSession *userSession = [TMUserSession sharedInstance];
            [userSession updateAvailableSparkCount:strCount];

            responseBlock(TRUE,nil);
        }else {
            responseBlock(FALSE,error.errorMessage);
        }
    }];
}
-(void)getActiveSpark:(void(^)(TMSpark *spark,NSInteger totalCachedSpark))sparkBLock {
    if(![[TMUserSession sharedInstance] isSparkEnabled]) {
        sparkBLock(nil,0);
        return;
    }
    [self.cacheController getCachedActiveSpark:^(TMSpark *spark, NSInteger cachedSparkCount) {
        TMLOG(@"Available cached spark:%ld",(long)cachedSparkCount);
        sparkBLock(spark,cachedSparkCount);
        if(cachedSparkCount < 2) {
            [self fetchSparkData];
        }
    }];
}
-(void)fetchSparkData {
    if([[TMUserSession sharedInstance] isSparkEnabled] && self.sparkManager.isNetworkReachable) {
        [self.sparkManager fetchSparkProfilesData:^(NSArray *sparkList, TMError *error) {
            if(sparkList.count && !error) {
                [self.cacheController cacheSparkList:sparkList];
            }
        }];
    }
}
-(void)updateSparkStatusAsSeen:(NSInteger)matchId hash:(NSString*)sparkHash didUpdateSparkStatus:(void(^)(BOOL status))statusBlock {
    NSDictionary *params = @{@"sender_id":@(matchId),@"hash":sparkHash};
    
    [self.sparkManager updateSparkAction:@"seen" params:params response:^(BOOL status, NSInteger senderId) {
        statusBlock(status);
        if(status) {
            TMChatContext *context = [[TMChatContext alloc] initWithLoggedInUser];
            context.matchId = senderId;
            NSString *time = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
            [self.cacheController updateSparkAsSeen:context withStartTime:time];
        }
    }];
}
-(void)startSparkTimer {
    self.sparkTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(sparkTimerAction) userInfo:nil repeats:TRUE];
}
-(void)stopSparkTimer {
    [self.sparkTimer invalidate];
    self.sparkTimer = nil;
}
-(void)sparkTimerAction {
    [self.conversationDelegate didSparkProgressTimerComplete];
    //[[NSNotificationCenter defaultCenter] postNotificationName:TMSPARKTIMER_NOTIFICATION object:nil];
}

//for marking as seen, rejected
-(void)deleteSpark:(NSInteger)matchId hash:(NSString*)sparkHash {
    NSDictionary *params = @{@"sender_id":@(matchId),@"hash":sparkHash};
    [self.sparkManager updateSparkAction:@"rejected" params:params response:^(BOOL status, NSInteger senderId) {
        if(status){
            TMChatContext *context = [[TMChatContext alloc] initWithLoggedInUser];
            context.matchId = senderId;
            [self.cacheController deleteSparkWithChatContext:context];
            [[NSNotificationCenter defaultCenter] postNotificationName:TMSPARK_REFRESH_NOTIFICATION object:nil];
        }
    }];
}

#pragma mark - Chat Connection Methods
#pragma mark - SETUP

-(void)configureConnection {
    TMLOG(@"configureConnection");
    if([self canConnect]) {
        if([self.connectionSetting isSocketEnabled]) {
            if([self.connectionSetting isSocketConnectionActive] && (self.connectionSetting.socketConnectionState == Connected) ) {
                if(([self canExecuteSocketEmit])) {
                    [self executeGetChatMetadataEmit];
                }
            }
            else {
                [self configureSocketConnection];
            }
        }
        else {
            [self configurePollingConnection];
            //in case socket is disabled from match call response
            if(self.socketConnection.isConnected) {
                [self resetSocketConnection];
            }
        }
    }
}
-(void)configureSocketConnection {
    TMLOG(@"configureSocketConnection");
    if(!self.socketConnection) {
        self.connectionSetting.activeChatConnection = CHATCONNECTION_SOCKET;
        self.connectionSetting.socketConnectionState = Connecting;
        TMSocketConnectionConfig *connectionConfig = [[TMSocketConnectionConfig alloc] init];
        if([self.connectionSetting isDefaultSocketConnectionCounter]) {
            [connectionConfig setConnectionParamForErrorReason:self.socketConnectionFailureReason];
        }
        connectionConfig.connectionCounter = self.connectionSetting.socketConnectionCounter++;
        
        self.socketConnection = [[TMSocketConnection alloc] initWithConnectionConfig:connectionConfig];
        self.socketConnection.delegate = self;
        [self executeSocketConnect];
        [self.pollingConnection checkNetworkStatusWithPing];
        self.showConnectingIndicator = TRUE;
        [self updateNetworkStatus:TRUE];
        [self postSocketConnectionNotificationWithStatus:TRUE];
        [self postDisableSendActiontNotificationWithStatus:TRUE];
    }
}
-(void)configurePollingConnection {
    if(self.chatContext) {
        TMLOG(@"configuring Polling");
        [self getLastCachedPollingConnectionIdentifier];
        [self executePollingConnect];
    }
}
-(void)resetConnection {
    if([self.connectionSetting isSocketConnectionActive]) {
        [self resetSocketConnection];
    }
    else {
        [self executePollingDisConnect];
    }
    self.connectionSetting.activeChatConnection = CHATCONNECTION_NA;
}
-(void)configureSendMessageTimer {
    if(!self.chatSendTimer) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!self.chatSendTimer) {
                self.chatSendTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(sendMessageTimer) userInfo:nil repeats:TRUE];
            }
        });
    }
}
-(void)resetMessageSendingTimer {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.chatSendTimer) {
            [self.chatSendTimer invalidate];
            self.chatSendTimer = nil;
        }
    });
}
-(void)resetSocketConnection {
    TMLOG(@"resetSocketConnection");
    self.connectionSetting.socketConnectionState = Disconnected;
    [self.socketConnection close];
    self.socketConnection.delegate = nil;
    self.socketConnection = nil;
}
-(BOOL)canExecuteSocketEmit {
    BOOL canEmit = FALSE;
    if([self.socketConnection isConnected] && [self.connectionSetting isSocketConnectionComplete]) {
        canEmit = TRUE;
    }
    return canEmit;
}

#pragma mark - SEND MESSAGE
-(void)sendMessageTimer {
    if(!self.sendingQueueInProgress) {
        self.sendingQueueInProgress = TRUE;
        for (int i=0; i<self.sendingQueue.count; i++) {
            TMMessage *message = [self.sendingQueue objectAtIndex:i];
            if([self canConnect]) {
                if(self.connectionSetting.activeChatConnection == CHATCONNECTION_SOCKET) {
                    if([self canExecuteSocketEmit]) {
                        [self executeSendMessageEmit:message];
                        [self.sendingQueue removeObjectAtIndex:i];
                    }
                }
                else {
                    [self executeSendMessageFromPollingConnection:message];
                    [self.sendingQueue removeObjectAtIndex:i];
                }
            }
            else {
                [self resetMessageSendingTimer];
                break;
            }
        }
        
        if(!self.sendingQueue.count) {
            TMLOG(@"message sending queue emptied");
            [self resetMessageSendingTimer];
        }
        self.sendingQueueInProgress = FALSE;
    }
}

-(void)executeSendMessage:(TMMessage*)message {
    if(self.connectionSetting.activeChatConnection == CHATCONNECTION_SOCKET) {
        if([self canExecuteSocketEmit]) {
            [self executeSendMessageEmit:message];
        }
        else {
            TMLOG(@"executeSendMessage:else:");
            [self.sendingQueue addObject:message];
        }
    }
    else {
        [self executeSendMessageFromPollingConnection:message];
    }
}

#pragma mark SOCKET
-(void)executeGetFirstTimeMessagesWithTimestamp:(NSString*)timestamp {
    NSDictionary *params = @{@"timestamp":timestamp};
    [self executeSocketEmit:EMIT_GETFIRSTTIMEMESSAGES withParmas:params];
}
-(void)executeGetFirstTimePhotoShareMessagesWithTimestamp:(NSString*)timestamp {
    NSDictionary *params = @{@"timestamp":timestamp};
    [self executeSocketEmit:EMIT_GETFIRSTTIMEPSMESSAGES withParmas:params];
}
-(void)executeSocketConnect {
    TMLOG(@"executeSocketConnect");
    [self executeSocketEmit:EMIT_CONNECT withParmas:nil];
}
-(void)executeSocketDisConnect {
    [self executeSocketEmit:EMIT_DISCONNECT withParmas:nil];
}
-(void)executeGetMissedMessagesEmit {
    [self.cacheController getLastChatFetchRequestTimestamp:^(NSString *timestamp) {
        NSDictionary *params = @{@"timestamp":timestamp};
        [self executeSocketEmit:EMIT_GETMISSEDMESSAGES withParmas:params];
    }];
}
-(void)executeSendMessageEmit:(TMMessage*)message {
    [self executeSocketEmit:EMIT_SENDMESSAGE withParmas:message];
}
-(void)executeChatReadEmit:(TMMessage*)message {
    [self executeSocketEmit:EMIT_CHATREAD withParmas:message];
}
-(void)executeGetChatMetadataEmit {
    if(self.chatContext) {
        [self executeSocketEmit:EMIT_GETCHATMETADATA withParmas:@(self.chatContext.matchId)];
    }
}
-(void)executeGetMatchMetadataEmit {
    if(self.chatContext) {
        [self executeSocketEmit:EMIT_GETMATCHMETADATA withParmas:@(self.chatContext.matchId)];
    }
}
-(void)executeSocketChatTypingwithParams:(NSDictionary*) params {
    if (self.chatContext) {
        [self executeSocketEmit:EMIT_CHATTYPING withParmas:params];
    }
}
-(void)executeSocketEmit:(TMSocketEmitType)emitType withParmas:(id)params {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (emitType) {
            case EMIT_CONNECT:
                [self.socketConnection connect];
                break;
                
            case EMIT_DISCONNECT:
                [self.socketConnection close];
                break;
                
            case EMIT_GETMISSEDMESSAGES:
                [self.socketConnection getMissedMessages:params];
                break;
                
            case EMIT_SENDMESSAGE:
                [self.socketConnection sendMessage:params];
                break;
                
            case EMIT_CHATREAD:
                [self.socketConnection fireChatRead:params];
                break;
                
            case EMIT_GETCHATMETADATA:
                [self.socketConnection getChatMetaData:params];
                break;
                
            case EMIT_GETMATCHMETADATA:
                [self.socketConnection getUserMetaData:params];
                break;
                
           case EMIT_CHATTYPING:
                [self.socketConnection fireChatTypingWithParams:params];
                break;
    
            case EMIT_GETFIRSTTIMEMESSAGES:
                [self.socketConnection getFirstTimeMessages:params];
                break;
                
            case EMIT_GETFIRSTTIMEPSMESSAGES:
                [self.socketConnection getFirstPhotoShareTimeMessages:params];
                break;
                
            case EMIT_MUTUALMATCHCHATREAD:
                [self.socketConnection fireMutualMatchChatRead:params];
                break;
                
            default:
                break;
        }
    });
}

#pragma mark POLLING

-(void)executePollingConnect {
    self.connectionSetting.activeChatConnection = CHATCONNECTION_POLLING;
    [self.pollingConnection configurePollingURL:self.chatURLString];
    [self.pollingConnection connect];
    if([self canConnect]) {
        [self postDisableSendActiontNotificationWithStatus:FALSE];
        //for showing indicator in first polling request
        if(!self.isFirstPollingRequestInProgress) {
            self.isFirstPollingRequestInProgress = TRUE;
            if(self.delegate && [self.delegate respondsToSelector:@selector(willMakeFirstPollingRequest)]) {
                [self.delegate willMakeFirstPollingRequest];
            }
        }
    }
}
-(void)executePollingDisConnect {
    [self.pollingConnection disconnect];
    //self.pollingConnection.delegate = nil;
    //self.pollingConnection = nil;
}
-(void)executeSendMessageFromPollingConnection:(TMMessage*)message {
    if([self.pollingConnection connected]) {
        [self.pollingConnection sendMessage:message];
    }
    else if(message.fullConversationLink) {
        [self.pollingConnection configurePollingURL:message.fullConversationLink];
        [self.pollingConnection sendMessage:message];
    }
}

#pragma mark - Socket Connection Delegate Handling
#pragma mark -

-(void)socketDidConnect; {
    TMLOG(@"Socket Connected");
    [self.connectionSetting updateSocketConnectionCounterToMax];
    [self executeGetMissedMessagesEmit];
}
-(void)socketDidDisConnectManually:(BOOL)status withError:(SocketConnectionFailReason)connectionFailureReason {
    TMLOG(@"Got DIsconnect Delegate:%@",self.socketConnection);
    if(connectionFailureReason == ACKTIMEOUT) {
        //track ping success/failure
        [self.socketConnection trackConnectionPingStatus:self.pingRequestStatus];
    }
    self.socketConnectionFailureReason = connectionFailureReason;
    self.connectionSetting.activeChatConnection = CHATCONNECTION_NA;
    [self resetSocketConnection];
    [self resetMessageSendingTimer];
    [self.sendingQueue removeAllObjects];
    //mark all messages ad faield
    [self invalidateAndUpdateCachedMessages];
    
    if([self.connectionSetting isSocketConnectionCounterAvailable]) {
        if([self canConnect]) {
            [self configureSocketConnection];
        }
    }
    else {
        self.showConnectingIndicator = FALSE;
        [self postSocketConnectionNotificationWithStatus:FALSE];
        [self.connectionSetting resetSocketConnectionCounter];
        
        //if ping is successful then move to polling
        if(self.pingRequestStatus == TRUE && self.chatContext) {
            TMLOG(@"chat context available so swithcing to polling");
            [self updateLastCachedPollingConnectionIdentifierFromSocket];
            [self executePollingConnect];
            [self.connectionSetting disableSocketConnectionOnConnectionFail];
            
            if(self.sendingQueue.count) {
                [self configureSendMessageTimer];
            }
            if(!status) {
                [self trackMovingToPollingFromSocket];
            }
        }
        else {
            TMLOG(@"cannot swithcing to polling");
            [self updateNetworkStatus:FALSE];
            //show no network status bar
            [self postDisableSendActiontNotificationWithStatus:TRUE];
        }
    }
}
-(void)socketDidReceiveUnreadMessages:(NSArray*)messages {
    self.showConnectingIndicator = FALSE;
    [self postSocketConnectionNotificationWithStatus:FALSE];
    [self postDisableSendActiontNotificationWithStatus:FALSE];
    self.connectionSetting.socketConnectionState = Connected;
    [self.connectionSetting resetSocketConnectionCounter];
    
    if(messages) {
        [self cacheMissedMessages:messages fromSource:SOCKET];
        [self updateOldDealMessages];
        [self updateOldPhotoShareMessages];
    }
    
    if(self.sendingQueue.count) {
        [self configureSendMessageTimer];
    }
    
    [self executeGetChatMetadataEmit];
    [self executeGetMatchMetadataEmit];
    
    if(self.pollingConnection.connected) {
        TMLOG(@"Polling connected on getmissed messaged completion so disconnecting");
        [self.pollingConnection disconnect];
    }
}
-(void)socketDidFailToReceiveUnreadMessages:(TMError*)error {
    [self executeGetMissedMessagesEmit];
}
-(void)socketDidReceiveNewMesage:(NSDictionary*)messageDict {
    //to fix issue of multiple message
    TMMessage *message = [[TMMessage alloc] initWithMessageDictionary:messageDict];
    TMMessage *cachedReceivedMessage = [self.receiveMessageTempCache objectForKey:message.uid];
    if(!cachedReceivedMessage) {
        [self.receiveMessageTempCache setObject:message forKey:message.uid];
        
        if(self.chatContext.matchId == message.senderId) {
            [self updateNewMessage:message];
        }
        
        ///cache new message
        [self cacheNewUnreadMessage:message];
    }
}
-(void)socketDidReceiveSentMesageResponse:(TMMessage*)message; {
    if(message) {
        //TMLOG(@"Received Sent Message Response Successfully:%d",message.deliveryStatus);
        if(self.sentMessageFailCounter) {
            self.sentMessageFailCounter = 0;
        }
        if(message.isSparkAcceptanceMessage) {
            [self updateSparkAsAcceptedMessage:message];
        }
        if(self.chatContext.matchId == message.receiverId) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate didSendMessage:message];
            });
        }
        
        [self updateCacheForSendMessageResponse:message updateConnectionIdentifier:TRUE];
    }
}
-(void)socketDidFailedToReceiveSentMesageResponse:(TMMessage*)message {
    self.sentMessageFailCounter++;
    //max retry count reached or max cache time reached
    if(message.sentFailRetryCount == MESSAGE_FAIL_RETRY_MAXLIMIT) {
        TMLOG(@"Sent Message Fail: Max Limit Reached");
        message.deliveryStatus = SENDING_FAILED;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate message:message sendingFailedWithError:nil];
        });
        
        [self updateCacheForSendMessageResponse:message updateConnectionIdentifier:FALSE];
    }
    else {
        TMLOG(@"Sent Message Fail: Adding To Sending Queue");
        [self.sendingQueue addObject:message];
    }
    
    if(self.sentMessageFailCounter == CONTINOUS_SEND_MESSAGE_FAIL_MAXLIMIT) {
        TMLOG(@"Continous Sent Message Fail: Max Limit Reached");
        self.sentMessageFailCounter = 0;
        [self.socketConnection.delegate socketDidDisConnectManually:TRUE withError:MANUALFAIL];
    }
}
-(void)socketDidReceiveSentMesageResponseWithBadWords:(TMMessage*)message {
    //delete message form db
    [self deleteMessage:message];
    [self.cacheController getCachedMessagesWithChatContext:self.chatContext result:^(NSArray *result, TMChatContext *context) {
        if(self.chatContext.matchId == context.matchId) {
            [self updateCacheMessages:result];
        }
    }];
    [self.delegate messageSendingFailedWithBadWordsResponse:message];
}
-(void)socketDidReceiveUserMetadata:(NSDictionary*)userMetadata {
    TMLOG(@"user metadata:%@",userMetadata);
    TMChatUser *chatUser = [[TMChatUser alloc] initWithSocketResponse:userMetadata];
    [self updateCacheForUserMetadataResponse:chatUser];
    [self updateChatUserMetaData:chatUser];
}
-(void)socketDidReceiveChatReadListener:(NSDictionary*)chatMetadata {
    TMChatSeenContext *context = [[TMChatSeenContext alloc] initWithChatReadListenerData:chatMetadata];
    [self updateChatSeenMetaData:context];
    [self cacheChatSentMetaData:context];
}
-(void)socketDidReceiveChatMetadata:(NSDictionary*)chatMetadata {
    //TODO check response and complete
    TMChatSeenContext *context = [[TMChatSeenContext alloc] initWithChatSeenData:chatMetadata];
    [self updateChatSeenMetaData:context];
    [self cacheChatSentMetaData:context];
}
-(void)socketDidReceiveBlockUserResponse {
    [self.delegate didReceiveBlockUserResponse];
}
-(void)socketDidReceiveChatReadEmitResponse:(NSDictionary*)chatReadDictioanry {
    NSInteger matchId = [chatReadDictioanry[@"sender_id"] integerValue];
    NSInteger loggedInUserId = [chatReadDictioanry[@"receiver_id"] integerValue];
    if(self.chatContext.matchId == matchId && self.chatContext.loggedInUserId == loggedInUserId) {
        [self markConversationAsRead:self.chatContext];
        [self postSocketConnectionNotificationWithStatus];
    }
}
-(void)socketDidReceiveQuizStatusUpdateResponse:(NSString*)matchId {
    [self.delegate didReceiveQuizStatusUpdate:matchId];
}
-(void)socketDidReceiveChatTypingListenerForMatchID:(NSString*)matchID {
    if(self.chatContext.matchId == [matchID integerValue]) {
        [self.delegate didReceiveChatTypingEmit:matchID];
    }
}
-(void)socketDidReceiveFirsttimeMessages:(NSArray*)messages {
    [self cacheMissedMessages:messages fromSource:SOCKET];
    [TMDataStore setBool:TRUE forKey:@"com.deal.oldmessageupdate"];
}
-(void)socketDidReceiveFirsttimePhotoShareMessages:(NSArray*)messages {
    [self cacheMissedMessages:messages fromSource:SOCKET];
    [TMDataStore setBool:TRUE forKey:@"com.deal.oldphotosharemessageupdate"];
}

#pragma mark - Polling Connection Handling
#pragma mark -

-(void)pollingDidReceiveUnreadMessages:(TMMessageResponse*)messagesResponse {
    ////
    if(self.isFirstPollingRequestInProgress) {
        [self markConversationAsRead:self.chatContext];
        self.isFirstPollingRequestInProgress = FALSE;
        if(self.delegate && [self.delegate respondsToSelector:@selector(didCompleteFirstPollingRequest)]) {
            [self.delegate didCompleteFirstPollingRequest];
        }
    }
    
    NSArray *messages = messagesResponse.msgList;
    if(messages.count) {
        self.pollingConnection.cacheUpdateInProgress = TRUE;
        [self cacheMissedMessages:messages fromSource:POLLING];
    }
    
    //tmchatuser
    [self updateCacheForUserMetadataResponse:messagesResponse.chatUserData];
    [self updateChatUserMetaData:messagesResponse.chatUserData];
    
    TMChatSeenContext *context = messagesResponse.chatSeenContext;
    [self updateChatSeenMetaData:context];
    [self cacheChatSentMetaData:context];
}
-(void)pollingDidReceiveSentMesageResponse:(NSDictionary*)messageDict {
    TMMessage *message = [[TMMessage alloc] initWithMessageDictionary:messageDict];
    [self updateCacheForSendMessageResponse:message updateConnectionIdentifier:TRUE];
    if(message.isSparkAcceptanceMessage) {
        [self updateSparkAsAcceptedMessage:message];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate didSendMessage:message];
    });
}
-(void)pollingDidFailToReceiveSentMessageResponse:(TMMessage *)message withError:(TMError *)error {
    message.sentFailRetryCount = message.sentFailRetryCount + 1;
    
    if(message.sentFailRetryCount == MESSAGE_FAIL_RETRY_MAXLIMIT) {
        TMLOG(@"P:Sent Message Fail: Max Limit Reached");
        message.deliveryStatus = SENDING_FAILED;
        [self updateCacheForSendMessageResponse:message updateConnectionIdentifier:FALSE];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate message:message sendingFailedWithError:error];
        });
    }
    else {
        TMLOG(@"P:Sent Message Fail: Adding To Sending Queue");
        [self.sendingQueue addObject:message];
        [self configureSendMessageTimer];
    }
}
-(NSDictionary*)paramsForMissedMessagesRequest {
    if(self.lastChatFetchIdentfier) {
        return @{@"msg_id":self.lastChatFetchIdentfier};
    }
    else {
        return nil;
    }
}
-(void)pollingDidReceiveBlockUserResponse:(BOOL)response {
    if(self.delegate && [self.delegate respondsToSelector:@selector(userBlockedWithStatus:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate userBlockedWithStatus:response];
        });
    }
}
-(void)pollingDidReceiveSentMesageResponseWithBadWords:(TMMessage*)message {
    [self deleteMessage:message];
    [self.cacheController getCachedMessagesWithChatContext:self.chatContext result:^(NSArray *result, TMChatContext *context) {
        if(self.chatContext.matchId == context.matchId) {
            [self updateCacheMessages:result];
        }
    }];
    [self.delegate messageSendingFailedWithBadWordsResponse:message];
}
-(void)pollingDidReceiveBlockUserResponse {
    [self.delegate didReceiveBlockUserResponse];
}
-(void)pollingDidReceiveNetworkPingWithStatus:(BOOL)status {
    TMLOG(@"#### Ping Status:%d #####",status);
    self.pingRequestStatus = status;
}
-(void)pollingDidReceiveDoodleLink:(NSString*)doodleLink {
    //cache doodle link
    if(self.chatContext) {
        [self.cacheController saveDoodleLink:doodleLink forMatchId:self.chatContext.matchId];
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(didReceiveDoodleLink:)]) {
            [self.delegate didReceiveDoodleLink:doodleLink];
        }
    }
}

#pragma mark - Chat Cache Handling
#pragma mark -

-(void)cacheSendMessage:(TMMessage*)message {
    [self.cacheController cacheOutgoingMessage:message];
    //cache image if photo share message
    if(message.type == MESSAGETTYPE_PHOTO) {
        [message cacheSendingImage];
    }
}

#pragma mark - Internal Methods
#pragma mark -

-(BOOL)canConnect {
    BOOL status = [self.pollingConnection isNetworkReachable];
    return status;
}
-(BOOL)canUpdateResultWithContext:(TMChatContext*)context {
    if(self.chatContext  && (self.chatContext.matchId == context.matchId)) {
        return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)configureCacheForConversation {
   [self.cacheController getCachedMessagesWithChatContext:self.chatContext result:^(NSArray *result, TMChatContext *context) {
        if(self.chatContext.matchId == context.matchId) {
            [self updateCacheMessages:result];
            
            //fire chat read for last message
            if(result.count) {
                TMMessage *message = result[result.count-1];;
                if(message && self.chatContext && [self canExecuteSocketEmit]) {
                    [self fireChatRead:message];
                }
            }
            else {
                [self fireMutualMatchChatRead:context.matchId];
            }
        }
    }];
   
    [self.cacheController getChatSentContextData:self.chatContext result:^(TMChatSeenContext *seencontext, TMChatContext *context) {
        if(context.matchId == self.chatContext.matchId) {
            [self updateChatSeenMetaData:seencontext];
        }
    }];
    
    [self.cacheController getChatUserData:self.chatContext result:^(TMChatUser *chatUser, BOOL needUpdate, TMChatContext *context) {
        if(self.chatContext.matchId == context.matchId) {
            [self updateChatUserMetaData:chatUser];
        }
        
        ///need to check active connection also
        if(needUpdate && [self canExecuteSocketEmit] && self.chatContext) {
            [self executeGetMatchMetadataEmit];
        }
    }];
}
-(void)cacheMissedMessages:(NSArray*)messages fromSource:(TMMessageSource)messageSource {
    if(messages.count) {
        [self.cacheController cacheMessages:messages
                                withContext:self.chatContext
                                fromSourece:messageSource
                         didCompleteCaching:^(NSArray *cachedMessages, TMChatContext *context) {
                             
                             TMLOG(@"cacheMissedMessages---didCompleteCaching");
                             if([self canUpdateResultWithContext:context]) {
                                 //sent to ui
                                 [self updateCacheMessages:cachedMessages];
                             }
                             
                             if(messages.count) {
                                 id messageObj = messages[messages.count-1];
                                 TMMessage *message = nil;
                                 if([messageObj isKindOfClass:[NSDictionary class]]) {
                                     NSDictionary *dict = (NSDictionary*)messageObj;
                                     message = [[TMMessage alloc] initWithMessageDictionary:dict];
                                 }
                                 else {
                                     message = (TMMessage*)messageObj;
                                 }
                                 
                                 //check if its msg or dict
                                 [self updateCachedConnectionIdentifier:message];
                                 
                                 if(message && self.chatContext && [self canExecuteSocketEmit]) {
                                     [self fireChatRead:message];
                                 }
                             }
                         }];
    }
}
-(void)cacheNewUnreadMessage:(TMMessage*)message {
    [self.cacheController cacheNewUnreadMessages:message];
    if(message && self.connectionSetting.socketConnectionState == Connected) {
        [self updateCachedConnectionIdentifier:message];
    }
}
-(void)cacheChatSentMetaData:(TMChatSeenContext*)chatSeenContext {
    [self.cacheController cacheChatMetadata:chatSeenContext];
}
-(void)updateCacheForSendMessageResponse:(TMMessage*)message updateConnectionIdentifier:(BOOL)update {
    [self.cacheController updateMessage:message needToUpdateConnectionIdentifier:update];
}
-(void)updateMetaParamsForMessage:(TMMessage*)message {
    [self.cacheController updateMetaParamsForMessage:message];
}
-(void)updateCacheForUserMetadataResponse:(TMChatUser*)chatUser {
    if(self.chatContext && chatUser.userId == self.chatContext.matchId) {
        [self.cacheController cacheChatUserMetadata:chatUser chatContext:self.chatContext];
    }
}
-(void)updateCachedConnectionIdentifier:(TMMessage*)message {
    if(self.connectionSetting.activeChatConnection == CHATCONNECTION_SOCKET) {
        [self.cacheController updateSocketConnectionIdentifier:message];
    }
    else {
        TMLOG(@"Active Connection Polling:%ld",(long)self.connectionSetting.activeChatConnection);
        [self.cacheController updatePollingConnectionIdentifier:message];
        if(self.chatContext) {
            [self getLastCachedPollingConnectionIdentifier];
        }
    }
}
-(void)updateLastCachedPollingConnectionIdentifierFromSocket {
    self.pollingConnection.cacheUpdateInProgress = TRUE;
    [self.cacheController getLastFetchedMessage:self.chatContext result:^(TMMessage *message, TMChatContext *context) {
        TMLOG(@"Last fecthced message id:%ld",(long)message.messageId);
        self.lastChatFetchIdentfier = [NSString stringWithFormat:@"%ld",(long)message.messageId];
        self.pollingConnection.cacheUpdateInProgress = FALSE;
    }];
}
-(void)getLastCachedPollingConnectionIdentifier {
    self.pollingConnection.cacheUpdateInProgress = TRUE;
    [self.cacheController getLastFetchedMessageId:self.chatContext result:^(NSString *messageId, TMChatContext *context) {
        TMLOG(@"Got Messageid:%@",messageId);
        self.lastChatFetchIdentfier = messageId;
        self.pollingConnection.cacheUpdateInProgress = FALSE;
    }];
}
-(void)invalidateAndUpdateCachedMessages {
    if(self.chatContext) {
        [self.cacheController markSendingMessagesAsFailedForContext:self.chatContext];
        
        [self.cacheController getCachedMessagesWithChatContext:self.chatContext result:^(NSArray *result, TMChatContext *context) {
            if(self.chatContext.matchId == context.matchId) {
                [self updateCacheMessages:result];
            }
        }];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
-(void)updateWillSendMessageEvent:(TMMessage*)message {
    if(self.chatContext.matchId == message.matchId) {
        [self.delegate willSendMessage:message];
    }
}
-(void)updateWillNotSendMessageEvent:(TMMessage*)message {
    if(self.chatContext.matchId == message.matchId) {
        [self.delegate willNotSendMessage:message withError:nil];
    }
}
-(void)updateChatSessionStatus:(BOOL)status {
    BOOL isUserOnOneToOneMessaging = FALSE;
    NSNumber *matchId = nil;
    if(status) {
        isUserOnOneToOneMessaging = TRUE;
        matchId = @(self.chatContext.matchId);
    }
    TMUserSession *userSession = [TMUserSession sharedInstance];
    userSession.isUserOnOneToOneMessaging = isUserOnOneToOneMessaging;
    userSession.currentMatchIdForOneToOneMessaging = matchId;
}
-(void)updateCacheMessages:(NSArray*)messages {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate didReceiveCachedMessages:messages];
    });
}
-(void)updateNewMessage:(TMMessage*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate didReceiveNewMesages:@[(message)]];
    });
    if(message && self.chatContext) {
        if(message.matchId == self.chatContext.matchId) {
            [self fireChatRead:message];
        }
    }
}
-(void)updateChatUserMetaData:(TMChatUser*)chatUser {
    if(chatUser.userId == self.chatContext.matchId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate didReceiveChatUserData:chatUser];
        });
    }
}
-(void)updateChatSeenMetaData:(TMChatSeenContext*)chatseencontext {
    if(chatseencontext.matchId == self.chatContext.matchId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate didReceiveChatSeenData:chatseencontext];
        });
    }
}
-(void)updateNetworkStatus:(BOOL)networkStatus {
    [self.delegate networkChangedWithStatus:networkStatus];
}
////////////////////////////////////////////////////////////////////////////////////////////

-(void)networkChanged:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    BOOL isNetworkReachable = [[userInfo objectForKey:@"network_status"] boolValue];
    TMLOG(@"Network Changed:Reachcable Status:%d",isNetworkReachable);
    [self updateNetworkStatus:isNetworkReachable];
    BOOL needToDisableSendActionButtons = (!isNetworkReachable);
    [self postDisableSendActiontNotificationWithStatus:needToDisableSendActionButtons];
    
    if(!isNetworkReachable) {
        //to hide connecting status if there is no network
        [self postSocketConnectionNotificationWithStatus:FALSE];
    }
    
    if(isNetworkReachable) {
        if([self.connectionSetting isSocketEnabled]) {
            BOOL canConnectSocket = TRUE;
            if(([self.connectionSetting isSocketConnectionActive]) && (self.connectionSetting.socketConnectionState != Disconnected)) {
                canConnectSocket = FALSE;
            }
            if(canConnectSocket) {
                NSString *userId = [TMUserSession sharedInstance].user.userId;
                if(userId) {
                    [self configureSocketConnection];
                }
            }
        }
    }
}

////////////////////
#pragma mark Deal -
-(void)updateOldDealMessages {
    BOOL isOldDealMessagesUpdated = [TMDataStore retrieveBoolforKey:@"com.deal.oldmessageupdate"];
    if(!isOldDealMessagesUpdated) {
        [self.cacheController getTsForDealMessageStoredAsText:^(NSString *timestamp) {
            if(timestamp) {
                //call getmissed messages over socket
                [self executeGetFirstTimeMessagesWithTimestamp:timestamp];
            }
            else {
                [TMDataStore setBool:TRUE forKey:@"com.deal.oldmessageupdate"];
            }
        }];
    }
}
-(void)updateOldPhotoShareMessages {
    BOOL isOldMessagesUpdated = [TMDataStore retrieveBoolforKey:@"com.deal.oldphotosharemessageupdate"];
    if(!isOldMessagesUpdated) {
        [self.cacheController getTsForPhotoShareMessageStoredAsText:^(NSString *timestamp) {
            if(timestamp) {
                //call getmissed messages over socket
                [self executeGetFirstTimePhotoShareMessagesWithTimestamp:timestamp];
            }
            else {
                [TMDataStore setBool:TRUE forKey:@"com.deal.oldphotosharemessageupdate"];
            }
        }];
    }
}
-(void)resetConversationList {
    BOOL isConversationListReset = [TMDataStore retrieveBoolforKey:@"com.deal.oldphotoshareconvreset"];
    if(!isConversationListReset) {
        [TMDataStore removeObjectforKey:@"rowupdatedts"];
        [self.cacheController deleteConversation];
        [TMDataStore setBool:TRUE forKey:@"com.deal.oldphotoshareconvreset"];
    }
}

#pragma mark Sticker

- (void) makeStickerAPITRequestWithGalleryRefreshBlock:(void(^)()) galleryRefreshBlock stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock allGalleriesRefreshBlock:(void(^)()) allGalleriesRefreshBlock failureBlock:(void(^)())failureBlock
{
    [self.stickerManager makeStickerAPITRequestWithGalleryRefreshBlock:galleryRefreshBlock stickerRefreshBlock:stickerRefreshBlock allGalleriesRefreshBlock:allGalleriesRefreshBlock failureBlock:failureBlock];
}

- (TMStickerManager*) stickerManager {
    if (!_stickerManager) {
        _stickerManager = [TMStickerManager sharedStickerManager];
    }
    return _stickerManager;
}

- (NSArray*) getAllStickerGalleries {
    return [self.stickerManager getAllStickerGalleries];
}

- (NSArray*) stickersForGalleryID:(NSUInteger) galleryID {
    return [self.stickerManager getAllStickerForStickerGalleryID:[NSString stringWithFormat:@"%lu",(unsigned long)galleryID]];
}

- (NSArray*) stickersForTimestampGallery {
    return [self.stickerManager getStickerBasedOnTimestamp];
}

- (UIImage*) getImageForSticker:(TMSticker*) sticker {
    return [self.stickerManager getImageForSticker:sticker];
}

- (void) storeHDImageForSticker:(TMSticker* ) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock {
    [self.stickerManager storeHDImageForSticker:sticker stickerRefreshBlock:stickerRefreshBlock];
}
- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID {
    return [self.stickerManager updateTimestampForStickerWithStickerID:stickerID galleryID:galleryID];
}
- (NSArray*) getFirstStickerGallery {
    
    NSArray* allGalleries = [self.stickerManager getAllStickerGalleries];
    
   return (allGalleries.count)?([NSArray arrayWithObject:[allGalleries firstObject]]):(nil);
}
- (NSArray*) getAllGalleryStickers {
    
    NSMutableArray* allGalleryStickerMutableArray;
    
    NSArray* allGalleryArray = [self.stickerManager getAllStickerGalleries];
    
    for (TMSticker* sticker in allGalleryArray) {
        NSString* galleryID = sticker.galleryID;
        
        NSArray* stickerArray = [self.stickerManager getAllStickerForStickerGalleryID:galleryID];
        
        if (!allGalleryStickerMutableArray) {
            allGalleryStickerMutableArray = [[NSMutableArray alloc] init];
        }
        [allGalleryStickerMutableArray addObject:@{galleryID:stickerArray}];
    }
    
    return [allGalleryStickerMutableArray copy];
}

#pragma mark Quiz -
-(void)updateQuiz {
    TMQuizManager* quizManager = [[TMQuizManager alloc] init];
    
    NSArray* dbArray = [quizManager fetchQuizFromDBForLatestVersion:YES];
    
    if (!dbArray || dbArray.count == 0) {
        [quizManager getAllAvailableQuizData];
    }
}

- (void)saveQuizWithQuizID:(NSString*) quizID answersWithDictionary:(NSDictionary*) quizAnswerDictionary
{
    if (quizID && quizAnswerDictionary) {
        [[[TMQuizManager alloc] init] saveQuizWithID:quizID withMatchID:[NSString stringWithFormat:@"%ld",(long)[[TMMessagingController sharedController] getCurrentMatchId]] withQuestionAnswerDictionary:quizAnswerDictionary];
    }
}

#pragma mark Spark Handling -

-(void)updateSparkAsAcceptedMessage:(TMMessage*)message {
    //delete spark
    TMChatContext *context = [[TMChatContext alloc] initWithLoggedInUser];
    context.matchId = message.matchId;
    [self.cacheController deleteSparkWithChatContext:context];
//    TMConversation *sparkConversation = [[TMConversation alloc] init];
//    [self.cacheController cacheSparkConversationData:sparkConversation];
    //refresh spark and conversation list
    [[NSNotificationCenter defaultCenter] postNotificationName:TMSPARKACCEPTANCE_REFRESH_NOTIFICATION object:nil];
}


//////////////////////////
-(void)trackMovingToPollingFromSocket {
    [TMSocketTracking trackMovingToPollingFromSocketEvent];
}

-(void)postSocketConnectionNotificationWithStatus:(BOOL)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:TMSOCKETCONNECTION_NOTIFICATION
                                                        object:nil
                                                      userInfo:@{@"connectionstatus":[NSNumber numberWithBool:status]}];
}

-(void)postDisableSendActiontNotificationWithStatus:(BOOL)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:TMDISABLESENDACTION_NOTIFICATION
                                                        object:nil
                                                      userInfo:@{@"connectionstatus":[NSNumber numberWithBool:status]}];
}
-(void)postSocketConnectionNotificationWithStatus {
    [[NSNotificationCenter defaultCenter] postNotificationName:TMNEWMESSAGE_NOTIFICATION
                                                        object:nil
                                                      userInfo:nil];
}


-(BOOL)didNumberSave
{
    if([TMDataStore containsObjectWithUserIDForKey:@"user_number"]) {
        BOOL status = [[TMDataStore retrieveObjectWithUserIDForKey:@"user_number"] boolValue];
        return status;
    }
    return false;
}

-(void)removeCachedSharedPhotosAtPath:(NSString*)path {
    NSString *initialPath = @".images/conversation";
    if(path) {
        initialPath = [NSString stringWithFormat:@"%@/%@",initialPath,path];
    }
    
    [TMFileManager removeFileAtPath:initialPath];
}

@end


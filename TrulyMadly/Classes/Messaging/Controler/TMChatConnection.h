//
//  TMChatConnection.h
//  TrulyMadly
//
//  Created by Ankit on 06/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@class TMError;
@class TMMessage;
@class TMMessageResponse;


@protocol TMChatConnection <NSObject>

@property(nonatomic,strong)NSString *connectionURL;
//@property(nonatomic,strong)NSNumber *lastMessageIdentifier;

@end

@protocol TMSocketConnectionDelegate <NSObject>

////////
-(void)socketDidConnect;
-(void)socketDidDisConnectManually:(BOOL)status withError:(SocketConnectionFailReason)connectionFailureReason;
-(void)socketDidReceiveUnreadMessages:(NSArray*)messages;
-(void)socketDidFailToReceiveUnreadMessages:(TMError*)error;
-(void)socketDidReceiveFirsttimeMessages:(NSArray*)messages;
-(void)socketDidReceiveFirsttimePhotoShareMessages:(NSArray*)messages;
-(void)socketDidReceiveNewMesage:(NSDictionary*)message;
-(void)socketDidReceiveSentMesageResponse:(TMMessage*)message;
-(void)socketDidReceiveSentMesageResponseWithBadWords:(TMMessage*)message;
-(void)socketDidFailedToReceiveSentMesageResponse:(TMMessage*)message;
-(void)socketDidReceiveUserMetadata:(NSDictionary*)userMetadata;
-(void)socketDidReceiveChatMetadata:(NSDictionary*)chatMetadata;
-(void)socketDidReceiveChatReadListener:(NSDictionary*)chatMetadata;
-(void)socketDidReceiveBlockUserResponse;
-(void)socketDidReceiveChatReadEmitResponse:(NSDictionary*)chatReadDictioanry;
-(void)socketDidReceiveQuizStatusUpdateResponse:(NSString*)matchId;
-(void)socketDidReceiveChatTypingListenerForMatchID:(NSString*)matchID;

@end


@protocol TMPollingConnectionDelegate <NSObject>

//////////
-(void)pollingDidReceiveUnreadMessages:(TMMessageResponse*)messagesResponse;

/////////
-(void)pollingDidReceiveBlockUserResponse;
-(void)pollingDidReceiveSentMesageResponse:(NSDictionary*)messageDict;
-(void)pollingDidFailToReceiveSentMessageResponse:(TMMessage*)message withError:(TMError*)error;;
-(void)pollingDidReceiveBlockUserResponse:(BOOL)response;
//-(void)pollingDidReceiveConversationListResponse:(NSArray*)conversationList;
-(NSDictionary*)paramsForMissedMessagesRequest;
-(void)pollingDidReceiveSentMesageResponseWithBadWords:(TMMessage*)message;
-(void)pollingDidReceiveNetworkPingWithStatus:(BOOL)status;
-(void)pollingDidReceiveDoodleLink:(NSString*)doodleLink;

@end

//
//  TMPollingConnection.h
//  TrulyMadly
//
//  Created by Ankit on 06/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMChatConnection.h"

@class TMMessageManager;

@interface TMPollingConnection : NSObject <TMChatConnection>

@property(nonatomic,strong)TMMessageManager *messageManager;
@property(nonatomic,weak)id<TMPollingConnectionDelegate> delegate;
@property(nonatomic,assign)BOOL cacheUpdateInProgress;
@property(nonatomic,assign)BOOL connected;

-(void)configurePollingURL:(NSString*)URLString;
-(BOOL)isNetworkReachable;
-(void)connect;
-(void)disconnect;
-(void)sendMessage:(TMMessage*)message;
-(void)blockUser:(NSString*)reason withURL:(NSString*)URLString;
-(void)checkNetworkStatusWithPing;
-(void)getDoodleLinkWithURLString:(NSString*)URLString;
-(void)fetchConversationListData:(void(^)(NSArray *result))resultBlock;

@end

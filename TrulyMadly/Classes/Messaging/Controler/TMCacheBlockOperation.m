//
//  TMCacheBlockOperation.m
//  TrulyMadly
//
//  Created by Ankit on 03/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCacheBlockOperation.h"
#import "TMLog.h"
#import "TMChatContext.h"
#import "TMUserSession.h"

@interface TMCacheBlockOperation ()

@property(nonatomic,strong)TMChatContext *chatContext;

@end


@implementation TMCacheBlockOperation

-(instancetype)initWithMatchId:(NSNumber*)matchId {
    self = [super init];
    if(self) {
        NSParameterAssert(matchId != nil);
        //self.matchId = matchId.integerValue;
//        //self.chatContext.matchId = self.matchId;
//        self.chatContext.loggedInUserId = [TMUserSession sharedInstance].user.userId.integerValue;
    }
    return self;
}

-(void)setContext:(TMChatContext*)context {
    if(context) {
        self.chatContext = [[TMChatContext alloc] init];
        self.chatContext.matchId = context.matchId;
        self.chatContext.loggedInUserId = context.loggedInUserId;
    }
}

-(void)dealloc {
    TMLOG(@"TMCacheBlockOperation --- dealloc");
    self.chatContext = nil;
}

@end


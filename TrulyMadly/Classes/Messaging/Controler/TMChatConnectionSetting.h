//
//  TMSocketConnectionSetting.h
//  TrulyMadly
//
//  Created by Ankit on 19/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

typedef NS_ENUM(NSInteger, TMSocketConnectionState)  {
    Connected,
    Connecting,
    Disconnected
};


@interface TMChatConnectionSetting : NSObject

@property(nonatomic,assign)TMActiveChatConnection activeChatConnection;
@property(nonatomic,assign)TMSocketConnectionState socketConnectionState;
@property(nonatomic,assign)NSUInteger socketConnectionCounter;

-(void)setupConnectionSettings;
-(BOOL)isSocketEnabled;
-(void)disableSocketConnectionOnConnectionFail;
-(BOOL)isSocketConnectionActive;
-(BOOL)isSocketConnectionComplete;
-(void)updateSocketConnectionCounterToMax;
-(BOOL)isSocketConnectionCounterAvailable;
-(BOOL)isDefaultSocketConnectionCounter;
-(void)resetSocketConnectionCounter;

@end

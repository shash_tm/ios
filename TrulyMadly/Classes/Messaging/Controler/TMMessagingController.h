//
//  TMMessagingController.h
//  TrulyMadly
//
//  Created by Ankit on 15/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMSticker.h"
#import "TMEnums.h"

@protocol TMMessagingControllerDelegate;
@protocol TMConversationControllerDelegate;

@class TMChatSeenContext;
@class TMChatContext;
@class TMChatUser;
@class TMMessage;
@class TMError;
@class TMConversation;
@class TMSpark;

@interface TMMessagingController : NSObject

@property(nonatomic,weak)id<TMMessagingControllerDelegate> delegate;
@property(nonatomic,weak)id<TMConversationControllerDelegate> conversationDelegate;
@property(nonatomic,assign)BOOL showConnectingIndicator;


+(instancetype)sharedController;
-(BOOL)canConnect;
-(void)setupChatWithContext:(TMChatContext*)context conversationURLString:(NSString*)URLString;
-(void)resetChatContext;
-(void)setChatSessionActive:(BOOL)isActive;
-(void)connectChat;
-(void)disconnectChat;
-(void)sendMessage:(TMMessage*)message;
-(void)sendMessages:(NSArray*)messages;
-(void)deleteMessage:(TMMessage*)message;
-(void)fireChatRead:(TMMessage*)message;
-(void)fireMutualMatchChatRead:(NSInteger)matchId;
-(void)fireChatTyping;
-(void)blockUserWithReason:(NSString*)blockReason;
-(NSString*)getCurrentMatchId;
-(void)getConversationData:(void(^)(NSDictionary *conversationData))resultBlock;
-(void)saveConversationList:(NSArray*)conversationList newConversationData:(void(^)(NSDictionary *conversationData))resultBlock;
-(void)markConversationAsBlockedAndShown:(TMChatContext*)chatContext;
-(void)markConversationAsRead:(TMChatContext*)chatContext;
-(void)markConversationsAsUnRead:(NSArray*)unreadConversations;
-(void)archiveConversation:(TMConversation*)conversationMessage;
-(BOOL)currentNetworkConnectionStatus;
-(void)deleteAllMessageContent;
-(void)fetchAndCacheDeletedConversationData;
-(void)clearChatTillMessage:(TMMessage*)message withURLString:(NSString*)URLString response:(void(^)(BOOL status))resultBlock;
-(void)executeBlockUserCall:(NSString*)URLString;
-(void)isMatchMsTm:(NSInteger)matchId result:(void(^)(BOOL isMatchTm))resultBlock;
-(void)setDealIconVisibilityStatus:(BOOL)iconVisibilityStatus;
-(void)setDealIconClickStatus:(BOOL)clickStatus withMatch:(NSString*)matchId;
-(BOOL)canPhotoShareButton;
-(BOOL)canPhotoShareButtonToMsTm;
-(BOOL)canShowDealButton;
-(BOOL)didNumberSave;
-(BOOL)isSenderIdMsTm:(NSInteger)matchId;
-(void)getConversationDataForShare:(void(^)(NSDictionary *conversationData))resultBlock;
-(void)postDisableSendActiontNotificationWithStatus:(BOOL)status;
-(void)cancelPhotoMessgaesInSendingStateAndMarkAsFailed;
-(void)removeImageDownloaderForURLString:(NSString*)URLString;
-(void)addImageDownloader:(UIImageView*)imageView forURLString:(NSString*)URLString;
-(BOOL)isPhotoDownloadInProgressForURLString:(NSString*)URLString;
-(void)fetchAndCacheDoodleLinkForUser:(TMChatUser*)chatUser;
-(void)sendSparkMessage:(NSString*)sparkMessage
                toMatch:(NSString*)matchId
              sparkHash:(NSString*)sparkHash
isHasLikedBeforeProfile:(BOOL)hasLikedBeforeProfile
               response:(void(^)(BOOL status,NSString* errorDescription))responseBlock;

//sticker methods
- (void) makeStickerAPITRequestWithGalleryRefreshBlock:(void(^)()) galleryRefreshBlock stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock allGalleriesRefreshBlock:(void(^)()) allGalleriesRefreshBlock failureBlock:(void(^)())failureBlock;
- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID;
- (void) storeHDImageForSticker:(TMSticker*) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock;
- (UIImage*) getImageForSticker:(TMSticker*) sticker;
- (NSArray*) stickersForTimestampGallery;
- (NSArray*) stickersForGalleryID:(NSUInteger) galleryID;
- (NSArray*) getAllStickerGalleries;
- (NSArray*) getFirstStickerGallery;
- (NSArray*) getAllGalleryStickers;/* array of NSDictionaries */
-(NSURLSessionDataTask*)photoUploadTaskForKey:(NSString*)keyIdentifier;
-(void)cancelPhotoUploadMessage:(TMMessage*)message;

-(void)fetchAndUpdateUnreadConversationListCount:(void(^)(BOOL status))resultBlock;
-(void)showUnreadConversationBlooper:(void(^)(BOOL status))resultBlock;

-(void)getActiveSpark:(void(^)(TMSpark *spark,NSInteger totalAvailableSpark))sparkBLock;
-(void)fetchSparkData;
-(void)deleteSpark:(NSInteger)matchId hash:(NSString*)sparkHash;
-(void)updateSparkStatusAsSeen:(NSInteger)matchId hash:(NSString*)sparkHash didUpdateSparkStatus:(void(^)(BOOL status))statusBlock;
-(void)stopSparkTimer;
-(void)startSparkTimer;

@end

@protocol TMMessagingControllerDelegate <NSObject>

//-(void)didReceiveCachedMessages:(NSArray*)messages withResultType:(TMMessageCacheResultType)type;
-(void)didReceiveCachedMessages:(NSArray*)messages;
-(void)invalidateAndReloadCachedMessages:(NSArray*)messages;
-(void)didReceiveNewMesages:(NSArray*)messages;
-(void)willSendMessage:(TMMessage*)message;
-(void)willNotSendMessage:(TMMessage*)message withError:(TMError*)error;
-(void)didSendMessage:(TMMessage*)message;
-(void)message:(TMMessage*)message sendingFailedWithError:(TMError*)error;

@optional
-(void)messageSendingFailedWithBadWordsResponse:(TMMessage*)message;
-(void)didReceiveBlockUserResponse;
-(void)didReceiveChatSeenData:(TMChatSeenContext*)chatSeenContext;
-(void)didReceiveChatUserData:(TMChatUser*)chatUser;
-(void)userBlockedWithStatus:(BOOL)status;
-(void)networkChangedWithStatus:(BOOL)status;
-(void)willMakeFirstPollingRequest;
-(void)didCompleteFirstPollingRequest;
-(void)didReceiveChatTypingEmit:(NSString*)matchId;
-(void)didReceiveQuizStatusUpdate:(NSString*)matchId;
-(void)didReceiveDealStateUpdate:(TMDealState)status;
-(void)willStartPhotoUploadForMessage:(TMMessage *)message;
-(void)didCompletePhotoUploadForMessage:(TMMessage*)message;
-(void)didFailPhotoUploadForMessage:(TMMessage*)message;
-(void)didReceiveDoodleLink:(NSString*)doodleLink;

@end

@protocol TMConversationControllerDelegate <NSObject>

-(void)didSparkProgressTimerComplete;

@end



//
//  TMImageUploader.m
//  TrulyMadly
//
//  Created by Ankit on 17/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMImageUploader.h"
#import "TMRequest.h"
#import "TMAPIClient.h"
#import "TMAPIConstants.h"
#import "TMError.h"
#import "TMEnums.h"
#import "TMAnalytics.h"


@interface TMImageUploader ()

@property(nonatomic,assign)NSInteger imageSize;

@property(nonatomic,strong)NSMutableDictionary *eventInfoParams;
@property(nonatomic,strong)NSMutableDictionary *eventParams;

@end

@implementation TMImageUploader

-(void)uploadImageData:(NSData*)imageData
        withIdentifier:(NSString*)identifier
            withImageSource:(NSString*)imageSource
            withParams:(NSDictionary*)params
              response:(PhotoShareUploadBlock)responseBlock {
    
    self.keyIdentifier = identifier;
    self.inputParams = params;
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SHAREPHOTO_UPLOAD];
    [request setPostRequestParameters:params];
    
    NSDate* startDate = [NSDate date];
    self.eventInfoParams = [NSMutableDictionary dictionary];
    self.eventInfoParams[@"Source"] = imageSource;
    self.eventInfoParams[@"Matchid "] = params[@"match_id"];
    self.eventInfoParams[@"Iscompressed "] = @(TRUE);
    self.eventInfoParams[@"size"] = [NSString stringWithFormat:@"%.2lu",imageData.length/1024];
    
    self.eventParams = [NSMutableDictionary dictionary];
    _eventParams[@"startDate"] = startDate;
    ///add tracking
    self.uploadTask = [self uploadDataAPI:imageData withRequest:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    
                    NSDate* endDate = [NSDate date];
                    self.eventParams[@"endDate"] = endDate;
                    [self trackPhotoUploadEventAction:@"upload"
                                           withStatus:@"success"
                                           withParams:self.eventParams
                                  withEventInfoParams:self.eventInfoParams];
                    [self sendUploadPhotoResponse:responseObject withError:nil inBlock:^(NSDictionary *responseDictionary, TMError *error) {
                        responseBlock(responseDictionary,error);
                    }];
                    
                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                    NSDate* endDate = [NSDate date];
                    self.eventParams[@"endDate"] = endDate;
                    NSString *failureReason = [self uploadFailureDescription:error];
                    if(failureReason) {
                        self.eventInfoParams[@"reason"] = failureReason;
                    }
                    [self trackPhotoUploadEventAction:@"upload"
                                           withStatus:@"fail"
                                           withParams:self.eventParams
                                  withEventInfoParams:self.eventInfoParams];
                    [self sendUploadPhotoResponse:nil withError:error inBlock:^(NSDictionary *responseDictionary, TMError *error) {
                        responseBlock(responseDictionary,error);
                    }];
                }];
};

-(void)cancelUploadTask {
    NSDate* endDate = [NSDate date];
    _eventParams[@"endDate"] = endDate;
    [self trackPhotoUploadEventAction:@"upload"
                           withStatus:@"cancel"
                           withParams:self.eventParams
                  withEventInfoParams:self.eventInfoParams];
    [self.uploadTask cancel];
    self.uploadTask = nil;
}
-(NSString*)uploadFailureDescription:(NSError*)error {
    NSString *failureReason = error.localizedDescription;
    NSDictionary *userInfo = [error userInfo];
    NSString *underlyingErrorKey = @"NSUnderlyingError";
    NSError *underlyingError = userInfo[underlyingErrorKey];
    NSString *locatoizedDescription = underlyingError.userInfo[@"NSLocalizedDescription"];
    if(locatoizedDescription) {
        failureReason = locatoizedDescription;
    }
    return failureReason;
}
-(NSURLSessionDataTask*)uploadDataAPI:(NSData*)imgData
                          withRequest:(TMRequest*)request
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    
    NSURLSessionDataTask *task = [apiMgr POST:request.urlString parameters:[request httpPostRequestParameters] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imgData name:@"file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"______TMBaseManager Response Description_____%@\n______________",responseObject);
        success(task,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //NSLog(@"Error:%@",error);
        failure(task,error);
    }];
    
    return task;
}

-(void)sendUploadPhotoResponse:(NSDictionary*)responseDictionary withError:(NSError*)error inBlock:(PhotoShareUploadBlock)block {
    
    if(responseDictionary && !error) {
        NSInteger responseCode= [responseDictionary[@"responseCode"] integerValue];
        if((responseDictionary) && (responseCode == 200)) {
            block(responseDictionary,nil);
        }
        else if(responseCode == TMRESPONSECODE_LOGOUT) {
            TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
            block(nil,tmerror);
        }
        else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
            TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
            block(nil,tmerror);
        }
    }
    else {
        TMError *tmerror = [[TMError alloc] initWithError:error];
        block(nil,tmerror);
    }
}

-(void)trackPhotoUploadEventAction:(NSString*)eventAction
                        withStatus:(NSString*)status
                        withParams:(NSDictionary*)eventParams
               withEventInfoParams:(NSDictionary*)eventInfoParams{
    
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"photo_share";
    eventDictionary[@"eventAction"] = eventAction;
    eventDictionary[@"label"] = status;
    eventDictionary[@"status"] = status;
    if(eventParams) {
        [eventDictionary addEntriesFromDictionary:eventParams];
    }
    if(eventInfoParams) {
        eventDictionary[@"event_info"] = eventInfoParams;
    }
    
     [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}

@end

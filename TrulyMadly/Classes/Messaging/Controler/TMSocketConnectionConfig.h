//
//  TMSocketConnectionConfig.h
//  TrulyMadly
//
//  Created by Ankit on 17/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@interface TMSocketConnectionConfig : NSObject

@property(nonatomic,assign)NSInteger connectionCounter;
@property(nonatomic,strong)NSString *suffixIdentifier;
@property(nonatomic,assign)BOOL isErrorMode;

-(BOOL)forceWebSocket;
-(NSString*)connectionURLString;
-(void)setConnectionParamForErrorReason:(SocketConnectionFailReason)errorReason;

@end

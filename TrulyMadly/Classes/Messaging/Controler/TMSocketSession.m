//
//  TMSocketSession.m
//  TrulyMadly
//
//  Created by Ankit on 20/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMSocketSession.h"
#import "TMBuildConfig.h"
#import "TMDataStore.h"
#import "NSString+TMAdditions.h"


#define SOCKET_CONNECTION_TIMEOUT 10
#define SOCKET_GETMISSEDMESSAGES_TIMEOUT 10
#define SOCKET_CHATSENT_TIMEOUT 10
#define SOCKET_CONNECTION_WEBSOCKETONLY 0

@implementation TMSocketSession

+(NSString*)socketURLString {
    NSString *socketEndPoint = TM_SOCKET_ENDPOINT;
    NSString *cachedSocketEndPoint = [TMDataStore retrieveObjectforKey:@"com.chat.cd"];
    if(cachedSocketEndPoint) {
        socketEndPoint = cachedSocketEndPoint;
    }
    return socketEndPoint;
}
+(NSString*)socketIPString {
    NSString *socketIP = TM_SOCKET_ENDPOINT;
    NSArray *cachedSocketIPs = [TMDataStore retrieveObjectforKey:@"com.chat.sip"];
    NSInteger index = 0;
    if(cachedSocketIPs && cachedSocketIPs.count) {
        if(cachedSocketIPs.count > 1) {
            int randNum = arc4random() % 4;
            if((randNum+1)%2 == 0) {
                index = 1;
            }
        }
        socketIP = cachedSocketIPs[index];
    }

    //NSLog(@"Socket Ip:%@ at Index Number:%ld",socketIP,index);
    return socketIP;
}
+(NSString*)abPrefixString {
    NSString *abPrefix = nil;
    NSString *cachedABPrefix = [TMDataStore retrieveObjectforKey:@"com.chat.abprefix"];
    if(cachedABPrefix) {
        abPrefix = cachedABPrefix;
    }
    return abPrefix;
}
+(BOOL)forceWebSocket {
    BOOL forceWebSocketStatus = TRUE;
    BOOL cachedStatus = [TMDataStore retrieveBoolforKey:@"com.chat.fws"];
    if(cachedStatus) {
        forceWebSocketStatus = cachedStatus;
    }
    return forceWebSocketStatus;
}
+(NSInteger)socketConnectionTimeout {
    NSInteger connectionTimeout = 10;
    NSNumber *cachedConnectionTimeout = [TMDataStore retrieveObjectforKey:@"com.chat.cto"];
    if(cachedConnectionTimeout) {
        connectionTimeout = [cachedConnectionTimeout integerValue];
    }
    return connectionTimeout;
}
+(NSInteger)getMissedMessageTimeout {
    NSInteger getmissedmessageTimeout = 10;
    NSNumber *cachedGetmissedmessageTimeout = [TMDataStore retrieveObjectforKey:@"com.chat.gmmto"];
    if(cachedGetmissedmessageTimeout) {
        getmissedmessageTimeout = [cachedGetmissedmessageTimeout integerValue];
    }
    return getmissedmessageTimeout;
}
+(NSInteger)chatsentTimeout {
    NSInteger chatSentTimeout = 10;
    NSNumber *cachedChatSentTimeout = [TMDataStore retrieveObjectforKey:@"com.chat.csto"];
    if(cachedChatSentTimeout) {
        chatSentTimeout = [cachedChatSentTimeout integerValue];
    }
    return chatSentTimeout;
}
+(void)setSocketConnectionParameterFromDictionary:(NSDictionary*)socketParameters {
    if(socketParameters && [socketParameters isKindOfClass:[NSDictionary class]]) {
        //socet endpoint
        NSString *chatDomain = TM_SOCKET_ENDPOINT;
        if(socketParameters[@"chat_domain"]) {
            chatDomain = socketParameters[@"chat_domain"];
        }
        if((chatDomain != nil) && [chatDomain isKindOfClass:[NSString class]] && (![chatDomain isNULLString])) {
            [TMDataStore setObject:chatDomain forKey:[NSString stringWithFormat:@"com.chat.cd"]];
        }
        
        //socket ip
        NSString *socketIP = socketParameters[@"chat_ip_list"];
        if(socketIP) {
            NSArray *ips = [socketIP componentsSeparatedByString:@","];
            [TMDataStore setObject:ips forKey:@"com.chat.sip"];
        }
        //connection timeout
        NSNumber *connectionTimeout = [NSNumber numberWithInt:10];
        if(socketParameters[@"connection_timeout"]) {
            connectionTimeout = socketParameters[@"connection_timeout"];
        }
        [TMDataStore setObject:connectionTimeout forKey:@"com.chat.cto"];
        
        //getmissged message timeout
        NSNumber *getmissedmessageTimeout = [NSNumber numberWithInt:10];
        if(socketParameters[@"get_missed_messages_timeout"]) {
            getmissedmessageTimeout = socketParameters[@"get_missed_messages_timeout"];
        }
        [TMDataStore setObject:getmissedmessageTimeout forKey:@"com.chat.gmmto"];
        
        //chat sent timeout
        NSNumber *chatSentTimeout = [NSNumber numberWithInt:10];
        if(socketParameters[@"chat_send_timeout"]) {
            chatSentTimeout = socketParameters[@"chat_send_timeout"];
        }
        [TMDataStore setObject:chatSentTimeout forKey:@"com.chat.csto"];
        
        //ab prefix
        NSString *abPrefix = nil;
        if(socketParameters[@"ab_prefix"]) {
            abPrefix = socketParameters[@"ab_prefix"];
        }
        if((abPrefix != nil) && [abPrefix isKindOfClass:[NSString class]] && (![abPrefix isNULLString])) {
            [TMDataStore setObject:abPrefix forKey:@"com.chat.abprefix"];
        }
        
        //force websocket
        //
//        BOOL webSocketOnly = FALSE;
//        if([socketParameters[@"websocket_only"] boolValue] == TRUE) {
//            webSocketOnly = TRUE;
//        }
        [TMDataStore setObject:chatSentTimeout forKey:@"com.chat.fws"];
     }
}

+(void)clearAllCachedData {
    [TMDataStore removeObjectforKey:@"com.chat.cd"];
    [TMDataStore removeObjectforKey:@"com.chat.cto"];
    [TMDataStore removeObjectforKey:@"om.chat.gmmto"];
    [TMDataStore removeObjectforKey:@"com.chat.csto"];
    [TMDataStore removeObjectforKey:@"com.chat.abprefix"];
}

+(void)markClearChatAPIStatusAsComplete {
    [TMDataStore setBool:TRUE forKey:@"clearchatapi"];
}

+(BOOL)isClearChatAPIStatusMarkedAsComplete {
    if([TMDataStore retrieveBoolforKey:@"clearchatapi"]) {
        return TRUE;
    }
    return FALSE;
}
@end

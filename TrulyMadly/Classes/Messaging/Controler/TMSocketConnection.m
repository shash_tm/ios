
//
//  TMSocketConnection.m
//  TrulyMadly
//
//  Created by Ankit on 06/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMSocketConnection.h"
#import "TMUserSession.h"
#import "TMSwiftHeader.h"
#import "TMCommonUtil.h"
#import "TMCookie.h"
#import "TMMessage.h"
#import "TMLog.h"
#import "TMAnalytics.h"
#import "TMUserSession.h"
#import "TMSocketTracking.h"
#import "TMSocketSession.h"
#import "TMSocketConnectionConfig.h"

//@import SocketIO;

#define SOCKET_RECONNECTATTEMPT  0
#define GETMISSEDMESSAGES_MAXRETRYATTEMPT  2

@interface TMSocketConnection ()

@property(nonatomic,strong)SocketIOClient *socketClient;
@property(nonatomic,strong)NSMutableDictionary *sendMessageDictioanry;
@property(nonatomic,strong)NSMutableDictionary *retryMessageTrackDictioanry;
@property(nonatomic,strong)TMSocketConnectionConfig *connectionConfig;
@property(nonatomic,assign)NSInteger connectionCounter;
@property(nonatomic,assign)NSInteger connectionRetryCounter;
@property(nonatomic,assign)NSUInteger getMissedMessagesCounter;
@property(nonatomic,assign)BOOL connectionErrorStatus;
@property(nonatomic,strong)NSString* suffixIdentifier;

@end

@implementation TMSocketConnection 

@synthesize connectionURL;

-(instancetype)init {
    self = [super init];
    if(self) {
        TMLOG(@"1. Creating Socket Conneciton:%@",self);
    }
    return self;
}
-(instancetype)initWithConnectionConfig:(TMSocketConnectionConfig*)connectionConfig {
    self = [super init];
    if(self) {
        TMLOG(@"2. Creating Socket Conneciton:%@",self);
        self.connectionConfig = connectionConfig;
        self.isConnectionInProgress = FALSE;
        self.connectionCounter = connectionConfig.connectionCounter;
        self.connectionRetryCounter = 0;
        self.getMissedMessagesCounter = 1;
        self.connectionErrorStatus = FALSE;
        self.suffixIdentifier = connectionConfig.suffixIdentifier;
    }
    return self;
}
-(void)initSocketClient {
    if(!self.socketClient) {
        NSDictionary *config = @{@"reconnects":[NSNumber numberWithBool:true],
                                  @"reconnectAttempts":[NSNumber numberWithInt:SOCKET_RECONNECTATTEMPT],
                                  @"reconnectWait":[NSNumber numberWithInt:2],
                                  @"log":[NSNumber numberWithBool:FALSE],
                                  @"connectParams":[TMCookie socketConnectionCookieDictionary],
                                  @"forceWebsockets":@([self.connectionConfig forceWebSocket])
                                  };
        TMLOG(@"creating socket client:%@ with params:%@",self,config);
        NSString *socketURLString = [self.connectionConfig connectionURLString];
        NSURL *socketURL = [NSURL URLWithString:socketURLString];
        TMLOG(@"SOCKETURL:%@",socketURLString);
        SocketIOClient *socketClient = [[SocketIOClient alloc] initWithSocketURL:socketURL config:config];
        self.socketClient = socketClient;
        
        [self configureSocketEvents];
    }
}

-(void)dealloc {
    TMLOG(@"TMSocketConnection --- dealloc:%@",self);
    [self disconnectSocket];
}

#pragma mark - Socket Event Handling
#pragma mark -

-(void)configureSocketEvents {
    /////////////////
    [self.socketClient on:@"connect" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"connect ack: connect:%@ SocketId:%@ self:%@",ack,self.socketClient.sid,self);
        if(self.socketClient.sid) {
            [self trackConnectionStatus:@"ack_received" needToTrackTime:TRUE];
            self.connectionRetryCounter = 0;
            self.connectionCounter = 1;
            self.isConnectionInProgress = FALSE;
            
            [self.delegate socketDidConnect];
        }
    }];
    [self.socketClient on:@"disconnect" callback:^(NSArray * data, SocketAckEmitter* ack) {
        NSString *error = @"unknown error";
        if(data.count) {
            error = data[0];
        }
        TMLOG(@"on disconnect with error:%@ self:%@",error,self);
        [self trackDisconnectionWithStatus:error];
        [self.delegate socketDidDisConnectManually:FALSE withError:ERROR];
    }];
    [self.socketClient on:@"error" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"on error:%@ SocketId:%@ self:%@",data,self.socketClient.sid,self);
        TMLOG(@"Socket Connection Status:%ld",(long)self.socketClient.status);
        
        NSString *error = @"unknown error";
        if(data.count) {
            error = data[0];
        }
        if(self.isConnectionInProgress) {
            [self trackConnectionErrorStatus:error];
        }
        else {
            [self trackErrorEventsStatus:error];
        }
    }];
    [self.socketClient on:@"reconnect" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"on reconnect:%@ SocketId:%@",data,self.socketClient.sid);
    }];
    [self.socketClient on:@"reconnectAttempt" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"on reconnectAttempt:%@ SocketId:%@ self:%@",data,self.socketClient.sid,self);
        [self trackConnectionStatus:@"ack_timed_out" needToTrackTime:TRUE];
        self.connectionRetryCounter += 1;
        [self trackConnectionStatus:@"sent" needToTrackTime:FALSE];
    }];
    [self.socketClient on:@"chat_received" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"chat_received:%@ socketid:%@ self:%@",data,self.socketClient.sid,self);
        NSInteger index = data.count ? 0 : -1;
        
        if(index != -1) {
            NSDictionary *response = data[index];
            [self.delegate socketDidReceiveNewMesage:response];
        }
        
        [self trackChatReceivedEventStatus:@"received"];
    }];
    [self.socketClient on:@"chat_read_listener" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"chat_read_listener:%@ socketid:%@ self:%@",data,self.socketClient.sid,self);
        NSInteger index = data.count ? 0 : -1;
        BOOL isFailed = FALSE;
        if(index != -1) {
            id response = data[index];
            if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"NO ACK"]) {
                isFailed = TRUE;
            }
            else {
                isFailed = FALSE;
            }
        }
        
        if(!isFailed) {
            [self.delegate socketDidReceiveChatReadListener:data[index]];
        }
        [self trackChatReadListenerEventStatus:@"received"];
    }];
    [self.socketClient on:@"QUIZ_REFRESH" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"QUIZ_REFRESH ack: connect:%@ SocketId:%@ self:%@ data:%@",ack,self.socketClient.sid,self,data);
        if(self.socketClient.sid) {
            NSInteger index = data.count ? 0 : -1;
            
            if(index != -1) {
                NSDictionary *response = data[index];
                NSString *matchId = response[@"sender_id"];
                [self.delegate socketDidReceiveQuizStatusUpdateResponse:matchId];
            }
        }
    }];
    [self.socketClient on:@"chat_typing_listener" callback:^(NSArray * data, SocketAckEmitter* ack) {
        TMLOG(@"chat_typing_listener ack: connect:%@ SocketId:%@ self:%@ data:%@",ack,self.socketClient.sid,self,data);
        if(self.socketClient.sid) {
            NSInteger index = data.count ? 0 : -1;
            
            if(index != -1) {
                NSDictionary *response = data[index];
                NSString *matchId = ([response[@"sender_id"] isValidObject])?response[@"sender_id"] :nil;
                [self.delegate socketDidReceiveChatTypingListenerForMatchID:matchId];
                TMLOG(@"typing indicator : did receive chat typing emit with match id :: %@",matchId);
            }
        }
    }];
}

#pragma mark - Public Methods
#pragma mark -

-(BOOL)isConnected {
    if((self.socketClient.status==SocketIOClientStatusConnected) && (!self.isConnectionInProgress)) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}
-(void)connect {
    self.sendMessageDictioanry = [NSMutableDictionary dictionaryWithCapacity:4];
    self.retryMessageTrackDictioanry = [NSMutableDictionary dictionaryWithCapacity:4];
    self.isConnectionInProgress = TRUE;
    [self connectSocket];
}
-(void)close {
    TMLOG(@"TMSocketConnection---closing socket:%@",self);
    [self disconnectSocket];
}

-(void)getMissedMessages:(NSDictionary*)params {
    if([self canEmit]) {
        NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:params];
        [requestParams setObject:[self connectionUniqueId] forKey:@"unique_id"];
        TMLOG(@"Fetching List of Missed Messages With Params:%@",requestParams);
        TMLOG(@"calling get_missed_messages params:%@ socketid:%@ self:%@",params,self.socketClient.sid,self);
        //TMLOG(@"isConnected:%d connecting:%d reconnecting:%d closed:%d",self.socketClient.connected,self.socketClient.connecting,self.socketClient.reconnecting,self.socketClient.closed);
        [self.sendMessageDictioanry setObject:[NSDate date] forKey:@"getmissedmessage"];
        [self getMissedMessagesWithParams:requestParams];
    }
    else {
        TMLOG(@"Cannot emit get missged messages Hence disconnecting");
        [self.delegate socketDidDisConnectManually:TRUE withError:MANUALFAIL];
    }
}
-(void)getFirstTimeMessages:(NSDictionary*)params {
    if([self canEmit]) {
        [self.socketClient emitWithAck:@"get_missed_messages" with:@[params]](15, ^(NSArray* data) {
            TMLOG(@"Got ack For Firsttime Messages:%@ SocketId:%@ self:%@",data,self.socketClient.sid,self);
            
            NSArray *missedMessages = nil;
            NSInteger index = data.count ? 0 : -1;
            BOOL isFailed = FALSE;
            if(index != -1) {
                id response = data[index];
                isFailed = [self isNoAckResponse:response];
            }
            
            if(!isFailed) {
                missedMessages = data[index];
                [self.delegate socketDidReceiveFirsttimeMessages:missedMessages];
            }
        });
    }
}
-(void)getFirstPhotoShareTimeMessages:(NSDictionary*)params {
    if([self canEmit]) {
        [self.socketClient emitWithAck:@"get_missed_messages" with:@[params]](15, ^(NSArray* data){
            TMLOG(@"Got ack For Firsttime Messages:%@ SocketId:%@ self:%@",data,self.socketClient.sid,self);
            
            NSArray *missedMessages = nil;
            NSInteger index = data.count ? 0 : -1;
            BOOL isFailed = FALSE;
            if(index != -1) {
                id response = data[index];
                isFailed = [self isNoAckResponse:response];
            }
            
            if(!isFailed) {
                missedMessages = data[index];
                [self.delegate socketDidReceiveFirsttimePhotoShareMessages:missedMessages];
            }
        });
    }
}
-(void)sendMessage:(TMMessage*)message {
    if([self canEmit]) {
        TMLOG(@"isConnected:%d",[self isSocketConnected]);
        [self.sendMessageDictioanry setObject:[NSDate date] forKey:message.socketConnectionParams[@"unique_id"]];
        if(message.isFailedMessageRetry) {
            [self.retryMessageTrackDictioanry setObject:message forKeyedSubscript:message.socketConnectionParams[@"unique_id"]];
        }
        [self trackSendMessagesStatus:[message socketTrackingStatus:@"sent"] withParams:[message socketTrackingParams] needToTrackTime:FALSE];
        [self sendMessageWithParams:message.socketConnectionParams];
    }
    else {
        TMLOG(@"send message failed as socket not connected:%d params:%@ socketid:%@",[self isSocketConnected],message.socketConnectionParams,self.socketClient.sid);
        [self.delegate socketDidDisConnectManually:TRUE withError:MANUALFAIL];
    }
}
-(void)sendMessages:(NSArray*)messages {
    if([self canEmit]) {
        ////////////////
        TMLOG(@"isConnected:%d",[self isSocketConnected]);
        TMLOG(@"sending messages:%ld socketid:%@",(long)messages.count,self.socketClient.sid);
    }
    else {
        TMLOG(@"message list sending failed:%ld socketid:%@ self:%@",(long)messages.count,self.socketClient.sid,self);
        [self.delegate socketDidDisConnectManually:TRUE withError:MANUALFAIL];
    }
}
-(void)getUserMetaData:(NSString*)matchId {
    if([self canEmit]) {
        TMLOG(@"calling get_user_metadata matchid:%@ socketid:%@",matchId,self.socketClient.sid);
        [self getUserMetaDataWithParams:@{@"match_ids":@[matchId]}];
    }
    else {
        TMLOG(@"Not connected : getUserMetaData");
        //[self.delegate socketDidDisConnect];
    }
}

-(void)getChatMetaData:(NSString*)matchId {
    if([self canEmit]) {
        TMLOG(@"calling get_chat_metadata matchid:%@ socketid:%@",matchId,self.socketClient.sid);
        NSDictionary *dict = @{@"match_ids":@[matchId]};
        [self getChatMetaDataWithParams:dict];
    }
    else {
        TMLOG(@"Not connected : getChatMetaData");
        //[self.delegate socketDidDisConnect];
    }
}
-(void)fireChatRead:(TMMessage*)message {
    if([self canEmit]) {
        TMLOG(@"firing chat read with socketid:%@",self.socketClient.sid);
        [self fireChatReadWithParams:[message chatReadParams]];
    }
    else {
        TMLOG(@"Not connected : fireChatRead");
        TMLOG(@"cannot fire chat read as socket not connected");
    }
}
-(void)fireMutualMatchChatRead:(NSDictionary*)params {
    if([self canEmit]) {
        TMLOG(@"firing mutual match chat read with socketid:%@",self.socketClient.sid);
        [self trackChatReadStatus:@"sent"];
        [self.socketClient emitWithAck:@"chat_read" with:@[params]](60, ^(NSArray* data){
            TMLOG(@"fireMutualMatchChatRead data:%@ onsicketid:%@",data,self.socketClient.sid);
            NSInteger index = data.count ? 0 : -1;
            BOOL isFailed = FALSE;
            if(index != -1) {
                id response = data[index];
                isFailed = [self isNoAckResponse:response];
            }
            
            NSString *status;
            if(!isFailed) {
                status = @"ack_received";
                NSDictionary *chatReadDictionary = data[index];
                if(chatReadDictionary && [chatReadDictionary isKindOfClass:[NSDictionary class]]) {
                    [self.delegate socketDidReceiveChatReadEmitResponse:chatReadDictionary];
                }
            }
            else {
                status = @"ack_timed_out";
            }
            [self trackChatReadStatus:status];
        });
    }
    else {
        TMLOG(@"Not connected : fireChatRead");
        TMLOG(@"cannot fire mutual match chat read as socket not connected");
    }
}
- (void)fireChatTypingWithParams:(NSDictionary*) params {
    if ([self canEmit]) {
        [self.socketClient emitWithAck:@"chat_typing" with:@[params]](60, ^(NSArray* data){
            TMLOG(@"typing indicator : fire chat typing emit with params :: %@",params);
        });
    }
}

-(void)printSid {
    TMLOG(@"SID:%@",self.socketClient.sid);
}

#pragma mark - Socket Connection Handling
#pragma mark -

-(void)connectSocket {
    [self initSocketClient];
    if((![self isSocketConnected])) {
        TMLOG(@"Sockket Not Connected Hence connecting socket:%@",self);
        [self trackConnectionStatus:@"sent" needToTrackTime:FALSE];
        [self.sendMessageDictioanry setObject:[NSDate date] forKey:@"connection"];
        NSInteger timeout = [TMSocketSession socketConnectionTimeout];
        TMLOG(@"Socket Conneciton Timeout:%ld",(long)timeout);
        [self.socketClient connectWithTimeoutAfter:timeout withHandler:^{
            TMLOG(@"socket connect timeout:%@",self);
            [self trackConnectionStatus:@"ack_timed_out" needToTrackTime:TRUE];
            [self.delegate socketDidDisConnectManually:FALSE withError:ACKTIMEOUT];
        }];
    }
    else {
        TMLOG(@"Socket connected Hence not connecting again:%@",self);
    }
}
-(void)disconnectSocket {
    TMLOG(@"disconnectSocket:%@",self);
    if(self.sendMessageDictioanry) {
        [self.sendMessageDictioanry removeAllObjects];
        self.sendMessageDictioanry = nil;
    }
    if(self.retryMessageTrackDictioanry) {
        [self.retryMessageTrackDictioanry removeAllObjects];
        self.retryMessageTrackDictioanry = nil;
    }
    if(self.socketClient) {
        TMLOG(@"disconnecting socket fast");
        [self.socketClient removeAllHandlers];
        [self.socketClient disconnect];
        self.socketClient = nil;
    }
}

-(BOOL)isSocketConnected {
    if(self.socketClient.status == SocketIOClientStatusConnected){
        return TRUE;
    }
    return FALSE;
}

-(BOOL)isSocketDisconnected {
    if((self.socketClient.status == SocketIOClientStatusConnected) || (self.socketClient.status == SocketIOClientStatusConnected) || (self.socketClient.status == SocketIOClientStatusConnected)){
        return TRUE;
    }
    return FALSE;
}

#pragma mark - Emit Handling
#pragma mark -

-(void)getMissedMessagesWithParams:(NSDictionary*)params {
    [self trackMissedMessagesStatus:@"sent" needToTrackTime:FALSE];
    NSInteger timeout = [TMSocketSession getMissedMessageTimeout];
    [self.socketClient emitWithAck:@"get_missed_messages" with:@[params]](timeout, ^(NSArray* data){
        TMLOG(@"Got ack For Missed Messages:%@ SocketId:%@ self:%@",data,self.socketClient.sid,self);
        
        NSArray *missedMessages = nil;
        NSInteger index = data.count ? 0 : -1;
        BOOL isFailed = FALSE;
        if(index != -1) {
            id response = data[index];
            isFailed = [self isNoAckResponse:response];
        }
        
        if(!isFailed) {
            [self trackMissedMessagesStatus:@"ack_received" needToTrackTime:TRUE];
            self.getMissedMessagesCounter = 0;
            missedMessages = data[index];
            [self.delegate socketDidReceiveUnreadMessages:missedMessages];
        }
        else {
            [self trackMissedMessagesStatus:@"ack_timed_out" needToTrackTime:TRUE];
            if(self.getMissedMessagesCounter < GETMISSEDMESSAGES_MAXRETRYATTEMPT) {
                self.getMissedMessagesCounter++;
                [self.delegate socketDidFailToReceiveUnreadMessages:nil];
            }
            else {
                [self trackDisconnectionWithStatus:@"failed_get_missed_messages-switching_to_polling"];
                [self.delegate socketDidDisConnectManually:TRUE withError:MANUALFAIL];
            }
        }
    });
}

-(void)sendMessageWithParams:(NSDictionary*)params {
    TMLOG(@"Sock:Send Message With Param:%@ self:%@",params,self);
    NSInteger timeout = [TMSocketSession chatsentTimeout];
    [self.socketClient emitWithAck:@"chat_sent" with:@[params]](timeout, ^(NSArray* data){
        TMLOG(@"Got ack:%@ For Send Message with parms:%@ SocketId:%@ self:%@",data,params,self.socketClient.sid,self);
        TMMessage *message = nil;
        NSInteger index = data.count ? 0 : -1;
        BOOL isFailed = FALSE;
        if(index != -1) {
            id response = data[index];
            isFailed = [self isNoAckResponse:response];
        }
        
        if(!isFailed) {
            NSDictionary *dict = data[index];
            if([dict isValidObject]) {
                id error = dict[@"error"];
                if(error) {
                    if( ([error isKindOfClass:[NSNumber class]]) && ([error boolValue]==1) ) {
                        message = [[TMMessage alloc] initWithBadWordMessageDictionary:dict];
                        [self.delegate socketDidReceiveSentMesageResponseWithBadWords:message];
                    }
                    else if( ([error isKindOfClass:[NSString class]]) && ([error isEqualToString:@"blocked"]) ) {
                        [self.delegate socketDidReceiveBlockUserResponse];
                    }
                }
                else {
                    message = [[TMMessage alloc] initWithMessageDictionary:dict];
                    TMMessage *trackedMessage = [self.retryMessageTrackDictioanry objectForKey:message.uid];
                    if(trackedMessage && trackedMessage.isFailedMessageRetry) {
                        message.isFailedMessageRetry = trackedMessage.isFailedMessageRetry;
                        [self.retryMessageTrackDictioanry removeObjectForKey:message.uid];
                    }
                    [self trackSendMessagesStatus:[message socketTrackingStatus:@"ack_received"] withParams:[message socketTrackingParams] needToTrackTime:TRUE];
                    [self.delegate socketDidReceiveSentMesageResponse:message];
                }
            }
        }
        else {
            //create error object
            message = [[TMMessage alloc] initWithSocketParamDict:params];
            TMMessage *trackedMessage = [self.retryMessageTrackDictioanry objectForKey:message.uid];
            if(trackedMessage && trackedMessage.isFailedMessageRetry) {
                message.isFailedMessageRetry = trackedMessage.isFailedMessageRetry;
                [self.retryMessageTrackDictioanry removeObjectForKey:message.uid];
            }
            [self trackSendMessagesStatus:[message socketTrackingStatus:@"ack_timed_out"] withParams:[message socketTrackingParams] needToTrackTime:TRUE];
            [self trackDisconnectionWithStatus:@"failed_chat_sent-restarting"];
            message.sentFailRetryCount = message.sentFailRetryCount + 1;
            [self.delegate socketDidFailedToReceiveSentMesageResponse:message];
        }
    });
}

-(void)getUserMetaDataWithParams:(NSDictionary*)params {
    [self trackGetUserMetaDataStatus:@"sent"];
    [self.socketClient emitWithAck:@"get_user_metadata" with:@[params]](60, ^(NSArray* data){
        TMLOG(@"Got ack User Metadata:%@ socketid:%@ self:%@",data,self.socketClient.sid,self);
        NSInteger index = data.count ? 0 : -1;
        BOOL isFailed = FALSE;
        if(index != -1) {
            id response = data[index];
            isFailed = [self isNoAckResponse:response];
        }
        
        NSString *status = nil;
        if(!isFailed) {
            //
            id obj = data[index];
            if([obj isKindOfClass:[NSArray class]]) {
                NSArray *arr = (NSArray*)obj;
                if(arr.count){
                    NSDictionary *dict = arr[0];
                    [self.delegate socketDidReceiveUserMetadata:dict];
                    status = @"ack_received";
                }
                else {
                    status = @"ack_timed_out";
                }
            }
            else {
                status = @"ack_timed_out";
            }
        }
        else {
            status = @"ack_timed_out";
        }
        
        [self trackGetUserMetaDataStatus:status];
    });
}

-(void)getChatMetaDataWithParams:(NSDictionary*)params {
    [self trackGetChatMetaDataStatus:@"sent"];
    [self.socketClient emitWithAck:@"get_chat_metadata" with:@[params]](60, ^(NSArray* data){
        TMLOG(@"Got ack for Chat Metadata:%@ socketid:%@ self:%@",data,self.socketClient.sid,self);
        NSInteger index = data.count ? 0 : -1;
        BOOL isFailed = FALSE;
        if(index != -1) {
            id response = data[index];
            isFailed = [self isNoAckResponse:response];
        }
        
        NSString *status = nil;
        if(!isFailed) {
            NSArray *arr = data[index];
            if(arr.count > 0) {
                [self.delegate socketDidReceiveChatMetadata:arr[index]];
                status = @"ack_received";
            }
            else {
                status = @"ack_timed_out";
            }
            //track success
        }
        else {
            status = @"ack_timed_out";
        }
        
        [self trackGetChatMetaDataStatus:status];
    });
}
-(void)fireChatReadWithParams:(NSDictionary*)params {
    TMLOG(@"Fire CHat read With Param:%@",params);
    [self trackChatReadStatus:@"sent"];
    [self.socketClient emitWithAck:@"chat_read" with:@[params]](60, ^(NSArray* data){
        TMLOG(@"fireChatRead data:%@ onsicketid:%@",data,self.socketClient.sid);
        NSInteger index = data.count ? 0 : -1;
        BOOL isFailed = FALSE;
        if(index != -1) {
            id response = data[index];
            isFailed = [self isNoAckResponse:response];
        }
        
        NSString *status;
        if(!isFailed) {
            status = @"ack_received";
            NSDictionary *chatReadDictionary = data[index];
            [self.delegate socketDidReceiveChatReadEmitResponse:chatReadDictionary];
        }
        else {
            status = @"ack_timed_out";
        }
        [self trackChatReadStatus:status];
    });
}
-(NSString*)connectionUniqueId {
    NSDate *date = [NSDate date];
    NSTimeInterval ts = [date timeIntervalSince1970];
    NSInteger randomNumber = arc4random_uniform(10000);
    //@"userid_timestamp_randomnumber"
    NSString *uid = [NSString stringWithFormat:@"%ld_%ld_%ld",(long)[self loggedInUserId],(long)ts,(long)randomNumber];
    return uid;
}

-(NSInteger)loggedInUserId {
    NSInteger userId = [[TMUserSession sharedInstance].user.userId integerValue];
    return userId;
}

-(BOOL)canEmit {
    if([self isSocketConnected]) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}
-(BOOL)isNoAckResponse:(id)response {
    BOOL status = FALSE;
    if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"NO ACK"]) {
        status = TRUE;
    }
    else {
        status = FALSE;
    }
    return status;
}

////////////////////////////////////////////////////////////////////////////

#pragma mark Tracking -

-(void)trackConnectionPingStatus:(BOOL)status {
    [self trackEventAction:@"connection"
           withEventStatus:[self pingEventStatusWithCurrentConnectionCounter:status]
       withEventInfoParams:nil
           withEventParams:nil
                 sendToAll:FALSE];
}
-(void)trackConnectionStatus:(NSString*)status needToTrackTime:(BOOL)trackTime {
    NSMutableDictionary *trackParams = nil;
    if(trackTime) {
        NSString *startTime = [self.sendMessageDictioanry objectForKey:@"connection"];
        if(startTime) {
            trackParams = [NSMutableDictionary dictionaryWithCapacity:2];
            trackParams[@"startDate"] = startTime;
            trackParams[@"endDate"] = [NSDate date];
            [self.sendMessageDictioanry removeObjectForKey:@"connection"];
        }
    }
    [self trackEventAction:@"connection"
           withEventStatus:[self connectionEventStatusWithCurrentConnectionCounter:status]
       withEventInfoParams:nil
           withEventParams:trackParams
                 sendToAll:FALSE];
}
-(void)trackConnectionErrorStatus:(NSString*)status {
    [self trackEventAction:@"connection"
           withEventStatus:status
       withEventInfoParams:nil
           withEventParams:nil
                 sendToAll:TRUE];
    
    //track error count
    [self trackEventAction:@"connection"
           withEventStatus:[self connectionEventErrorStatusWithCurrentConnectionCounter]
       withEventInfoParams:nil
           withEventParams:nil
                 sendToAll:TRUE];
    
}
-(void)trackDisconnectionWithStatus:(NSString*)status {
    [self trackEventAction:@"disconnect"
           withEventStatus:status
       withEventInfoParams:nil
           withEventParams:nil
                 sendToAll:TRUE];
}
-(void)trackMissedMessagesStatus:(NSString*)status needToTrackTime:(BOOL)trackTime {
    if(self.socketClient.sid) {
        NSMutableDictionary *trackParams = nil;
        if(trackTime) {
            NSString *startTime = [self.sendMessageDictioanry objectForKey:@"getmissedmessage"];
            if(startTime) {
                trackParams = [NSMutableDictionary dictionaryWithCapacity:2];
                trackParams[@"startDate"] = startTime;
                trackParams[@"endDate"] = [NSDate date];
                [self.sendMessageDictioanry removeObjectForKey:@"getmissedmessage"];
            }
        }
        
        [self trackEventAction:@"get_missed_messages"
               withEventStatus:[self getMissedMessagesEventStatusWithCurrentCounter:status]
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:trackParams
                     sendToAll:FALSE];
    }
}
-(void)trackSendMessagesStatus:(NSString*)status withParams:(NSDictionary*)params
               needToTrackTime:(BOOL)trackTime {
    if(self.socketClient.sid) {
        NSMutableDictionary *eventInfoParams = [NSMutableDictionary dictionaryWithCapacity:4];
        eventInfoParams[@"socketID"] = self.socketClient.sid;
        
        NSMutableDictionary *trackParams = [NSMutableDictionary dictionaryWithCapacity:4];
        [trackParams addEntriesFromDictionary:params];
        if(trackTime) {
            if([self.sendMessageDictioanry objectForKey:params[@"unique_id"]]) {
                trackParams[@"startDate"] = [self.sendMessageDictioanry objectForKey:params[@"unique_id"]];
                trackParams[@"endDate"] = [NSDate date];
                [self.sendMessageDictioanry removeObjectForKey:params[@"unique_id"]];
            }
        }
        
        [self trackEventAction:@"chat_sent"
               withEventStatus:status
           withEventInfoParams:eventInfoParams
               withEventParams:trackParams
                     sendToAll:TRUE];
    }
}
-(void)trackGetUserMetaDataStatus:(NSString*)status {
    if(self.socketClient.sid) {
        [self trackEventAction:@"get_user_metadata"
               withEventStatus:status
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:nil
                     sendToAll:FALSE];
    }
}
-(void)trackGetChatMetaDataStatus:(NSString*)status {
    if(self.socketClient.sid) {
        [self trackEventAction:@"get_chat_metadata"
               withEventStatus:status
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:nil
                     sendToAll:FALSE];
    }
}
-(void)trackChatReadStatus:(NSString*)status {
    if(self.socketClient.sid) {
        [self trackEventAction:@"chat_read"
               withEventStatus:status
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:nil
                     sendToAll:FALSE];
    }
}
-(void)trackChatReadListenerEventStatus:(NSString*)status {
    if(self.socketClient.sid) {
        [self trackEventAction:@"chat_read_listener"
               withEventStatus:status
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:nil
                     sendToAll:FALSE];
    }
}
-(void)trackChatReceivedEventStatus:(NSString*)status {
    if(self.socketClient.sid) {
        [self trackEventAction:@"chat_received"
               withEventStatus:status
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:nil
                     sendToAll:FALSE];
    }
}
-(void)trackErrorEventsStatus:(NSString*)status {
    if(self.socketClient.sid) {
        [self trackEventAction:@"error_socket"
               withEventStatus:status
           withEventInfoParams:@{@"socketID":self.socketClient.sid}
               withEventParams:nil
                     sendToAll:TRUE];
    }
}
-(NSString*)connectionEventStatusWithCurrentConnectionCounter:(NSString*)status {
    NSString *connectionStatus = [NSString stringWithFormat:@"%@_%ld",status,(long)(self.connectionRetryCounter+self.connectionCounter)];
    if(self.suffixIdentifier) {
        connectionStatus = [NSString stringWithFormat:@"%@_%@",connectionStatus,self.suffixIdentifier];
    }
    TMLOG(@"____Connection Status:%@",connectionStatus);
    return connectionStatus;
}
-(NSString*)connectionEventErrorStatusWithCurrentConnectionCounter {
    NSString *connectionStatus = [NSString stringWithFormat:@"%@_%ld",@"error",(long)(self.connectionRetryCounter+self.connectionCounter)];
    if(self.suffixIdentifier) {
        connectionStatus = [NSString stringWithFormat:@"%@_%@",connectionStatus,self.suffixIdentifier];
    }
    TMLOG(@"Connection Error Status:%@",connectionStatus);
    return connectionStatus;
}
-(NSString*)pingEventStatusWithCurrentConnectionCounter:(BOOL)pingStatus {
    NSString *pingStatusString = (pingStatus) ? @"success" : @"fail";
    NSInteger counter = (self.connectionRetryCounter+self.connectionCounter);
    NSString *status = [NSString stringWithFormat:@"%@_%@_%ld",@"ping",pingStatusString,(long)counter];
    //NSString *connectionStatus = [NSString stringWithFormat:@"%@_%ld",status,(long)(self.connectionRetryCounter+self.connectionCounter)];
    if(self.suffixIdentifier) {
        status = [NSString stringWithFormat:@"%@_%@",status,self.suffixIdentifier];
    }
    TMLOG(@"____Ping Status:%@",status);
    return status;
}
-(NSString*)getMissedMessagesEventStatusWithCurrentCounter:(NSString*)status {
    NSString *connectionStatus = [NSString stringWithFormat:@"%@_%ld",status,(long)self.getMissedMessagesCounter];
    TMLOG(@"____getmissedmessage Status:%@",connectionStatus);
    return connectionStatus;
}

-(void)trackEventAction:(NSString*)eventAction
        withEventStatus:(NSString*)status
    withEventInfoParams:(NSDictionary*)params
        withEventParams:(NSDictionary*)eventParams
              sendToAll:(BOOL)sendToAll {
    
    [TMSocketTracking trackSocketEvent:eventAction
                          status:status
                     eventInfoParams:params
                           eventParams:eventParams
                       sendToAll:sendToAll];
}

@end

//
//  TMImageUploader.h
//  TrulyMadly
//
//  Created by Ankit on 17/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMError;

typedef void(^PhotoShareUploadBlock)(NSDictionary *responseDictionary, TMError *error);

@interface TMImageUploader : NSObject

@property(nonatomic,strong)NSDictionary *inputParams;
@property(nonatomic,strong)NSString *keyIdentifier;
@property(nonatomic,strong)NSURLSessionDataTask *uploadTask;

-(void)uploadImageData:(NSData*)imageData
        withIdentifier:(NSString*)identifier
       withImageSource:(NSString*)imageSource
            withParams:(NSDictionary*)params
              response:(PhotoShareUploadBlock)responseBlock;

-(void)cancelUploadTask;

@end

//
//  TMSocketConnectionSetting.m
//  TrulyMadly
//
//  Created by Ankit on 19/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMChatConnectionSetting.h"
#import "TMTimestampFormatter.h"
#import "TMDataStore.h"
#import "TMUserSession.h"
#import "TMLog.h"


#define SOCKET_CONNECTION_MAX_ATTEMPT                            2
#define SOCKET_CONNECTION_COUNTER_INIT_INDEX                     1

@interface TMChatConnectionSetting ()

@property(nonatomic,assign)BOOL isSocketConnectionComplete;
@property(nonatomic,assign)BOOL isSocketConnectionEnabled;

@end

@implementation TMChatConnectionSetting

-(instancetype)init {
    self = [super init];
    if(self) {
        [self setupConnectionSettings];
        self.socketConnectionCounter = SOCKET_CONNECTION_COUNTER_INIT_INDEX;
        self.socketConnectionState = Disconnected;
        self.activeChatConnection = CHATCONNECTION_NA;
    }
    return self;
}

-(void)setupConnectionSettings {
    BOOL status = FALSE;
    TMActiveChatConnection connectionType = [[TMUserSession sharedInstance] chatConnection];
    BOOL timerStatus = [self isSocketConnectionFailureTimerCompleted];
    if(connectionType == CHATCONNECTION_SOCKET  && timerStatus) {
        status = TRUE;
    }
    self.isSocketConnectionEnabled = status;
}
-(BOOL)isSocketEnabled {
//    return self.isSocketConnectionEnabled;
    BOOL status = FALSE;
    TMActiveChatConnection connectionType = [[TMUserSession sharedInstance] chatConnection];
    BOOL timerStatus = [self isSocketConnectionFailureTimerCompleted];
    if(connectionType == CHATCONNECTION_SOCKET  && timerStatus) {
        status = TRUE;
    }
    return status;
}
-(void)disableSocketConnectionOnConnectionFail {
    NSDate *date = [NSDate date];
    [TMDataStore setObject:date forKey:@"socket_to_polling"];
    self.isSocketConnectionEnabled = FALSE;
}
-(BOOL)isSocketConnectionActive {
    if((self.activeChatConnection == CHATCONNECTION_SOCKET)) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isSocketConnectionComplete {
    if(self.socketConnectionState == Connected) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isSocketConnectionCounterAvailable {
    TMLOG(@"1.currentconnectioncounter:%ld",(long)self.socketConnectionCounter);
    if(self.socketConnectionCounter <= SOCKET_CONNECTION_MAX_ATTEMPT) {
        TMLOG(@"2.currentconnectioncounter:%ld",(long)self.socketConnectionCounter);
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isDefaultSocketConnectionCounter {
    if(self.socketConnectionCounter == SOCKET_CONNECTION_MAX_ATTEMPT) {
        return TRUE;
    }
    return FALSE;
}
-(void)updateSocketConnectionCounterToMax {
    self.socketConnectionCounter = SOCKET_CONNECTION_MAX_ATTEMPT+1;
}
-(void)resetSocketConnectionCounter {
    self.socketConnectionCounter = SOCKET_CONNECTION_COUNTER_INIT_INDEX;
}
-(BOOL)isPollingConnectionEnabled {
    TMActiveChatConnection connectionType = [[TMUserSession sharedInstance] chatConnection];
    if(connectionType == CHATCONNECTION_POLLING) {
        return TRUE;    
    }
    return FALSE;
}
-(BOOL)isSocketConnectionFailureTimerCompleted {
    BOOL status = TRUE;
    NSDate *cachedDate = [TMDataStore retrieveObjectforKey:@"socket_to_polling"];
    if(cachedDate) {
        NSDate *date = [NSDate date];
        TMTimestampFormatter *tsformatter = [TMTimestampFormatter sharedFormatter];
        NSInteger mins = [tsformatter minsBetweenDate:cachedDate andDate:date];
        if(mins < 1) {
            status = FALSE;
        }
    }
    return status;
}

@end

//
//  TMMessagesBubbleImageFactory.h
//  TrulyMadly
//
//  Created by Ankit on 28/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMMessagesBubbleImage.h"

@interface TMMessagesBubbleImageFactory : NSObject

- (instancetype)init;

- (instancetype)initWithBubbleImage:(UIImage *)bubbleImage capInsets:(UIEdgeInsets)capInsets;

- (TMMessagesBubbleImage *)outgoingMessagesBubbleImageWithColor:(UIColor *)color;


- (TMMessagesBubbleImage *)incomingMessagesBubbleImageWithColor:(UIColor *)color;

- (TMMessagesBubbleImage *)voucherMessagesBubbleImage;

@end

//
//  TMMessagesMediaViewBubbleImageMasker.m
//  TrulyMadly
//
//  Created by Ankit on 06/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessagesMediaViewBubbleImageMasker.h"
#import "TMMessagesBubbleImageFactory.h"
#import "TMMessagesBubbleImage.h"
#import "UIColor+TMColorAdditions.h"

@interface TMMessagesMediaViewBubbleImageMasker ()

@property (strong, nonatomic) TMMessagesBubbleImageFactory *bubbleImageFactory;

@end

@implementation TMMessagesMediaViewBubbleImageMasker

-(instancetype)init {
    self = [super init];
    if(self) {
        self.bubbleImageFactory = [[TMMessagesBubbleImageFactory alloc] init];
    }
    return self;
}
+(void)applyBubbleImageMaskToMediaView:(UIView *)mediaView isOutgoing:(BOOL)isOutgoing
{
    TMMessagesMediaViewBubbleImageMasker *masker = [[TMMessagesMediaViewBubbleImageMasker alloc] init];
    
    if (isOutgoing) {
        [masker applyOutgoingBubbleImageMaskToMediaView:mediaView];
    }
    else {
        [masker applyIncomingBubbleImageMaskToMediaView:mediaView];
    }
}
+(void)applyBubbleImageMaskToVoucherView:(UIView*)mediaView {
    TMMessagesMediaViewBubbleImageMasker *masker = [[TMMessagesMediaViewBubbleImageMasker alloc] init];
    [masker applyImageMaskToVoucherView:mediaView];
}

#pragma mark - View masking

-(void)applyOutgoingBubbleImageMaskToMediaView:(UIView*)mediaView {
    TMMessagesBubbleImage *bubbleImageData = [self.bubbleImageFactory outgoingMessagesBubbleImageWithColor:[UIColor outgoingMessageColor]];
    [self maskView:mediaView withImage:[bubbleImageData messageBubbleImage]];
}
-(void)applyIncomingBubbleImageMaskToMediaView:(UIView*)mediaView {
    TMMessagesBubbleImage *bubbleImageData = [self.bubbleImageFactory incomingMessagesBubbleImageWithColor:[UIColor incomingMessageColor]];
    [self maskView:mediaView withImage:[bubbleImageData messageBubbleImage]];
}
-(void)applyImageMaskToVoucherView:(UIView*)mediaView {
    TMMessagesBubbleImage *bubbleImageData = [self.bubbleImageFactory voucherMessagesBubbleImage];
    [self maskView:mediaView withImage:[bubbleImageData messageBubbleImage]];
}
-(void)maskView:(UIView*)view withImage:(UIImage*)image {
    if(view == nil) {
        //NSLog(@"view is nill");
    }
    NSParameterAssert(view != nil);
    NSParameterAssert(image != nil);
    
    UIImageView *imageViewMask = [[UIImageView alloc] initWithImage:image];
    imageViewMask.frame = CGRectInset(view.frame, 2.0f, 2.0f);
    
    view.layer.mask = imageViewMask.layer;
}


@end

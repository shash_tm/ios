//
//  TMTimestampFormatter.m
//  TrulyMadly
//
//  Created by Ankit on 22/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMTimestampFormatter.h"

@interface TMTimestampFormatter ()

@property (nonatomic,strong) NSDictionary *dateTextAttributes;
@property (nonatomic,strong) NSDateFormatter *dateFormatter;
@property (nonatomic,strong) NSDateFormatter *absoluteDateFormatter;
@property (nonatomic,strong) NSDateFormatter *shortDateFormatter;

@end



@implementation TMTimestampFormatter

#pragma mark - Initialization

+ (TMTimestampFormatter *)sharedFormatter
{
    static TMTimestampFormatter *_sharedFormatter = nil;
    
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        _sharedFormatter = [[TMTimestampFormatter alloc] init];
    });
    
    return _sharedFormatter;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setLocale:[NSLocale currentLocale]];
        [_dateFormatter setDoesRelativeDateFormatting:YES];
        
        _absoluteDateFormatter = [[NSDateFormatter alloc] init];
        [_absoluteDateFormatter setLocale:[NSLocale currentLocale]];
        [_absoluteDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [_absoluteDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        
        _shortDateFormatter = [[NSDateFormatter alloc] init];
        [_shortDateFormatter setLocale:[NSLocale currentLocale]];
        [_shortDateFormatter setDateFormat:@"MMM dd, h:mm a"];
        

        UIColor *color = [UIColor lightGrayColor];
        NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        _dateTextAttributes = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:12.0f],
                                 NSForegroundColorAttributeName : color,
                                 NSParagraphStyleAttributeName : paragraphStyle };
    }
    return self;
}

- (void)dealloc
{
    self.dateFormatter = nil;
    self.absoluteDateFormatter = nil;
    self.shortDateFormatter = nil;
    self.dateTextAttributes = nil;
}

#pragma mark - Formatter

-(NSDate*)absoulteDateFromString:(NSString*)dateString {
    NSDate *date = [self.absoluteDateFormatter dateFromString:dateString];
    return date;
}
-(NSDate*)shortDateFromString:(NSString*)dateString {
    NSDate *date = [self.shortDateFormatter dateFromString:dateString];
    return date;
}
-(NSString*)timestampForDate:(NSDate*)date {
    if (!date) {
        return nil;
    }
    
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    return [self.dateFormatter stringFromDate:date];
}

-(NSAttributedString*)attributedDateStringForDate:(NSDate*)date {
    if (!date) {
        return nil;
    }
    
    NSString *relativeDate = [self relativeDateForDate:date];
    NSMutableAttributedString *timestamp = [[NSMutableAttributedString alloc] initWithString:relativeDate
                                                                                  attributes:self.dateTextAttributes];
    return timestamp;
}
-(NSAttributedString*)attributedTimestampForDate:(NSDate *)date byAppendingDeliveryStatus:(NSString*)deliveryStatus {
    
    if (!date) {
        return nil;
    }

    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] init];
    
    NSAttributedString *deliveryStatusAttributedString = nil;
    if(deliveryStatus) {
        deliveryStatusAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",deliveryStatus]
                                                                         attributes:[self deliveryStatusTextAttributes]];
        [attributedText appendAttributedString:deliveryStatusAttributedString];
    }
    
    NSString *time = [self timeForDate:date];
    NSAttributedString *timestamp = [[NSAttributedString alloc] initWithString:time
                                                                           attributes:[self timeTextAttributes]];
    [attributedText appendAttributedString:timestamp];
    
    return attributedText;
}

-(NSString*)timeForDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    
    [self.dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    return [self.dateFormatter stringFromDate:date];
}

- (NSString*)absoluteTimestampForDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    return [self.absoluteDateFormatter stringFromDate:date];
}
- (NSString*)shortTimestampForDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    return [self.shortDateFormatter stringFromDate:date];
}
-(NSString*)relativeDateForDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    return [self.dateFormatter stringFromDate:date];
}

-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

-(NSInteger)minsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitMinute startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitMinute startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitMinute
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference minute];
}
-(NSInteger)secsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitMinute startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitMinute startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitSecond
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference second];
}

-(NSString*)currentTimestampString {
    NSDate *date = [NSDate date];
    NSString *dateString = [self absoluteTimestampForDate:date];
    return dateString;
}

-(BOOL)istimestamp:(NSString*)timestamp1 greatetThanTimestamp:(NSString*)timestamp2 {
    BOOL status = FALSE;
    NSDate *date1 = [self absoulteDateFromString:timestamp1];
    NSDate *date2 = [self absoulteDateFromString:timestamp2];
    if(date1 && date2) {
        NSComparisonResult result = [date2 compare:date1];
        if(result==NSOrderedAscending) {
            status = TRUE;
        }
    }
    else if(date2 == nil) {
        status = TRUE;
    }
    return status;
}
-(BOOL)istimestamp:(NSString*)timestamp1 greatetThanEqualToTimestamp:(NSString*)timestamp2 {
    BOOL status = FALSE;
    NSDate *date1 = [self absoulteDateFromString:timestamp1];
    NSDate *date2 = [self absoulteDateFromString:timestamp2];
    if(date1 && date2) {
        NSComparisonResult result = [date2 compare:date1];
        if( (result == NSOrderedAscending) || (result == NSOrderedSame) ) {
            status = TRUE;
        }
    }
    else if(date2 == nil) {
        status = TRUE;
    }
    return status;
}

-(NSDictionary*)deliveryStatusTextAttributes {
    NSDictionary *textAttributes = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:12.0f],
                                      NSForegroundColorAttributeName : [UIColor grayColor] };
    return textAttributes;
}
-(NSDictionary*)timeTextAttributes {
    UIColor *color = [UIColor lightGrayColor];
    NSDictionary *textAttributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:12.0f],
                                      NSForegroundColorAttributeName : color };
    return textAttributes;
}

@end

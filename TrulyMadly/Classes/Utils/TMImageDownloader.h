//
//  TMImageDownloader.h
//  TrulyMadly
//
//  Created by Ankit on 07/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString * const TMImageDownloadCompleteNotification;
FOUNDATION_EXPORT NSString * const TMImageDownloadFailNotification;
FOUNDATION_EXPORT NSString * const TMDownloadedImage;
FOUNDATION_EXPORT NSString * const TMImageDownloadUrl;



@interface TMImageDownloader : NSObject

+(instancetype)sharedImageDownloader;
-(UIImage*)imageAtPath:(NSString*)imagePath;
-(NSData*)imageDataAtPath:(NSString*)imagePath;
-(void)downloadImageFromUrl:(NSURL*)imageURL;
-(void)downloadAndCacheImageFromUrl:(NSString*)imageURLString;
-(void)deRegisterImageUrl:(NSURL*)imageURL;
-(void)clearCachedImage:(NSURL*)imageURL;
-(void)clearCachedImageAtPath:(NSString*)imagePath;
-(void)downloadImageFromUrl:(NSURL*)imageURL cacheAtPath:(NSString*)path;
-(void)cacheImageData:(NSData*)imageData atPath:(NSString*)path withName:(NSString*)imageName;
@end

//
//  TMActivityIndicator.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 23/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMActivityIndicator: TMBaseManager {
   
    var activityIndicator: UIActivityIndicatorView!
    
    override init() {
        super.init()
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        self.activityIndicator.color = UIColor(red: 0.463 , green: 0.463 , blue: 0.463 , alpha: 1.0)
    }
    
    func addActivity() -> UIActivityIndicatorView {
        return self.activityIndicator
    }
    
}

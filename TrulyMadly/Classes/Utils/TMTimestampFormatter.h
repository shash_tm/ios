//
//  TMTimestampFormatter.h
//  TrulyMadly
//
//  Created by Ankit on 22/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMTimestampFormatter : NSObject

/**
 *  Returns the shared timestamp formatter object.
 *
 *  @return The shared timestamp formatter object.
 */
+ (TMTimestampFormatter *)sharedFormatter;

-(NSDate*)absoulteDateFromString:(NSString*)dateString;

-(NSDate*)shortDateFromString:(NSString*)dateString;

-(NSString*)timestampForDate:(NSDate*)date;

-(NSAttributedString*)attributedTimestampForDate:(NSDate*)date byAppendingDeliveryStatus:(NSString*)deliveryStatus;

-(NSAttributedString*)attributedDateStringForDate:(NSDate*)date;

-(NSString*)timeForDate:(NSDate*)date;

-(NSString*)absoluteTimestampForDate:(NSDate*)date;

-(NSString*)shortTimestampForDate:(NSDate*)date;

-(NSString*)relativeDateForDate:(NSDate*)date;

-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

-(NSInteger)minsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

-(NSInteger)secsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

-(BOOL)istimestamp:(NSString*)timestamp1 greatetThanTimestamp:(NSString*)timestamp2;

-(BOOL)istimestamp:(NSString*)timestamp1 greatetThanEqualToTimestamp:(NSString*)timestamp2;

@end

//
//  TMCommonUtil.h
//  TrulyMadly
//
//  Created by Ankit on 07/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCommonUtil : NSObject

+(NSString*)deviceOSVersion;
+(NSString*)vendorId;
+(NSString*)buildNumberString;
+(NSString*)appVersionString;
+(BOOL)isCurrentDeviceBeforeiOS8;
@end

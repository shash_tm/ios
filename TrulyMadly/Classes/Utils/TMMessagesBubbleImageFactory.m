//
//  TMMessagesBubbleImageFactory.m
//  TrulyMadly
//
//  Created by Ankit on 28/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessagesBubbleImageFactory.h"
#import "UIImage+TMAdditions.h"



@interface TMMessagesBubbleImageFactory ()

@property (strong, nonatomic, readonly) UIImage *bubbleImage;
@property (assign, nonatomic, readonly) UIEdgeInsets capInsets;

- (UIEdgeInsets)centerPointEdgeInsetsForImageSize:(CGSize)bubbleImageSize;

- (TMMessagesBubbleImage *)messagesBubbleImageWithColor:(UIColor *)color flippedForIncoming:(BOOL)flippedForIncoming;

- (UIImage *)horizontallyFlippedImageFromImage:(UIImage *)image;

- (UIImage *)stretchableImageFromImage:(UIImage *)image withCapInsets:(UIEdgeInsets)capInsets;

@end

@implementation TMMessagesBubbleImageFactory

- (instancetype)initWithBubbleImage:(UIImage *)bubbleImage capInsets:(UIEdgeInsets)capInsets
{
    NSParameterAssert(bubbleImage != nil);
    
    self = [super init];
    if (self) {
        _bubbleImage = bubbleImage;
        
        if (UIEdgeInsetsEqualToEdgeInsets(capInsets, UIEdgeInsetsZero)) {
            _capInsets = [self centerPointEdgeInsetsForImageSize:bubbleImage.size];
        }
        else {
            _capInsets = capInsets;
        }
    }
    return self;
}

- (instancetype)init
{
    return [self initWithBubbleImage:[UIImage bubbleCompactImage] capInsets:UIEdgeInsetsZero];
}

- (void)dealloc
{
    _bubbleImage = nil;
}

#pragma mark - Public

- (TMMessagesBubbleImage *)outgoingMessagesBubbleImageWithColor:(UIColor *)color
{
    return [self messagesBubbleImageWithColor:color flippedForIncoming:NO];
}

- (TMMessagesBubbleImage *)incomingMessagesBubbleImageWithColor:(UIColor *)color
{
    return [self messagesBubbleImageWithColor:color flippedForIncoming:YES];
}

- (TMMessagesBubbleImage *)voucherMessagesBubbleImage
{
    return [self messagesVoucherImage];
}

#pragma mark - Private

- (UIEdgeInsets)centerPointEdgeInsetsForImageSize:(CGSize)bubbleImageSize
{
    // make image stretchable from center point
    CGPoint center = CGPointMake(bubbleImageSize.width / 2.0f, bubbleImageSize.height / 2.0f);
    return UIEdgeInsetsMake(center.y, center.x, center.y, center.x);
}

- (TMMessagesBubbleImage *)messagesBubbleImageWithColor:(UIColor *)color flippedForIncoming:(BOOL)flippedForIncoming
{
    NSParameterAssert(color != nil);
    
    UIImage *normalBubble = [self.bubbleImage imageMaskedWithColor:color];
    
    if (flippedForIncoming) {
        normalBubble = [self horizontallyFlippedImageFromImage:normalBubble];
    }
    
    normalBubble = [self stretchableImageFromImage:normalBubble withCapInsets:self.capInsets];
  
    
    return [[TMMessagesBubbleImage alloc] initWithMessageBubbleImage:normalBubble];
}
- (TMMessagesBubbleImage *)messagesVoucherImage
{
    //NSParameterAssert(color != nil);
    
    //UIImage *normalBubble = [self.bubbleImage imageMaskedWithColor:color];
    
    UIImage *voucherImage = [UIImage imageNamed:@"vouchermask"];
    UIImage *normalBubble = [self stretchableImageFromImage:voucherImage withCapInsets:self.capInsets];
    return [[TMMessagesBubbleImage alloc] initWithMessageBubbleImage:normalBubble];
}

- (UIImage *)horizontallyFlippedImageFromImage:(UIImage *)image
{
    return [UIImage imageWithCGImage:image.CGImage
                               scale:image.scale
                         orientation:UIImageOrientationUpMirrored];
}

- (UIImage *)stretchableImageFromImage:(UIImage *)image withCapInsets:(UIEdgeInsets)capInsets
{
    return [image resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
}


@end

//
//  TMCommonUtil.m
//  TrulyMadly
//
//  Created by Ankit on 07/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCommonUtil.h"
#import <UIKit/UIKit.h>
#import "TMDataStore.h"


@implementation TMCommonUtil

+(NSString*)deviceOSVersion {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    return systemVersion;
}

+(NSString*)vendorId {
    NSString *cachedVendorId = [TMDataStore retrieveObjectforKey:@"vendorid"];
    if(!cachedVendorId) {
        NSString *vendorId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [TMDataStore setObject:vendorId forKey:@"vendorid"];
        cachedVendorId = vendorId;
    }
//    if(cachedVendorId != vendorId) {
//        //need to track change of id to server
//        [TMDataStore setObject:vendorId forKey:@"vendorid"];
//        cachedVendorId = vendorId;
//    }
    return cachedVendorId;
}

//interanl version string
+(NSString*)buildNumberString {
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    return appBuildString;
}

//public app cversion
+(NSString*)appVersionString {
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return appVersionString;
}

+(BOOL)isCurrentDeviceBeforeiOS8
{
    // iOS < 8.0
    return [[UIDevice currentDevice].systemVersion compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending;
}

@end

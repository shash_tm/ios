//
//  TMStringUtil.h
//  TrulyMadly
//
//  Created by Ankit Jain on 11/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>


@interface TMStringUtil : NSObject

+(CGRect)textRectForString:(NSDictionary*)text;

+(CGSize)contentSizeForStringArray:(NSArray*)contents

                    withAttributes:(NSDictionary*)textAttributes;

+(NSString*)stringByReplacingInvalidContent:(NSString*)text;

@end

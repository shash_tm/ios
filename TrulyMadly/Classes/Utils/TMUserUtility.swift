//
//  TMUserUtility.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 03/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import Foundation

class TMUserUtility{
    
    func getIndex(from:NSArray,object:AnyObject)->Int{
        for i in (0 ..< from.count){
            let index = i
            if((from[i] as AnyObject).isEqual(object)){
                return index
            }
        }
        return -1
    }
    
    func getIndexForIndustry(key:String,from:NSArray)->Int{
        for i in (0 ..< from.count){
            let index = i
            let industry = from[i] as! NSDictionary
            let industryKey = industry["key"] as! String
            if(industryKey == key){
                return index
            }
        }
        return -1
    }
    
    func getIndexForCity(key: String, from:NSArray) -> Int {
        for i in (0 ..< from.count){
            let index = i
            let cities = from[i] as! NSDictionary
            let cityId = cities["city_id"] as! String
            if(cityId == key){
                return index
            }
        }
        return -1
    }
}

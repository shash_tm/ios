/**
 / Source based on https://github.com/swissmanu/MACircleProgressIndicator
 */

#import "XLCircleProgressIndicator.h"

#define kXLCircleProgressIndicatorDefaultStrokeProgressColor      [UIColor whiteColor]
#define kXLCircleProgressIndicatorDefaultStrokeRemainingColor     [[UIColor whiteColor] colorWithAlphaComponent:0.3f]
#define kXLCircleProgressIndicatorDefaultStrokeWidthRatio         0.06
#define kXLCircleProgressIndicatorDefaultSize                     50.f
#define kXLCircleProgressIndicatorDefaultStrokeWidth              -1.0

@interface XLCircleProgressIndicator ()

@property(nonatomic,strong)UIImageView *bgImageView;

@end

@implementation XLCircleProgressIndicator

@synthesize strokeWidth          = _strokeWidth;
@synthesize strokeProgressColor  = _strokeProgressColor;
@synthesize strokeRemainingColor = _strokeRemainingColor;
@synthesize strokeWidthRatio     = _strokeWidthRatio;
@synthesize progressValue        = _progressValue;
@synthesize minimumSize          = _minimumSize;


-(id)initWithFrame:(CGRect)frame showCancelButton:(BOOL)showCancelButton {
    self = [super initWithFrame:frame];
    if (self) {
        //self.backgroundColor = [UIColor redColor];
        if(showCancelButton) {
            self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
            self.bgImageView.image = [UIImage imageNamed:@"photouploadcancel"];
            self.bgImageView.backgroundColor = [UIColor clearColor];
            self.bgImageView.clipsToBounds = TRUE;
            [self addSubview:self.bgImageView];
        }
    
        [self setDefaultValues];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaultValues];
    }
    return self;
}
-(void)setDefaultValues {
    self.backgroundColor    = [UIColor clearColor];
    _strokeProgressColor    = kXLCircleProgressIndicatorDefaultStrokeProgressColor;
    _strokeRemainingColor   = kXLCircleProgressIndicatorDefaultStrokeRemainingColor;
    _strokeWidthRatio       = kXLCircleProgressIndicatorDefaultStrokeWidthRatio;
    _minimumSize            = kXLCircleProgressIndicatorDefaultSize;
    _strokeWidth            = kXLCircleProgressIndicatorDefaultStrokeWidth;
}


#pragma mark - Properties

-(void)setProgressValue:(float)progressValue {
    if (progressValue < 0.0) progressValue = 0.0;
    if (progressValue > 1.0) progressValue = 1.0;
    _progressValue = progressValue;
    [self setNeedsDisplay];
}

-(void)setStrokeWidth:(CGFloat)strokeWidth {
    _strokeWidthRatio = -1.0;
    _strokeWidth = strokeWidth;
    [self setNeedsDisplay];
}

-(void)setStrokeWidthRatio:(CGFloat)strokeWidthRatio {
    _strokeWidth = -1.0;
    _strokeWidthRatio = strokeWidthRatio;
    [self setNeedsDisplay];
}

-(void)setStrokeProgressColor:(UIColor *)strokeProgressColor{
    _strokeProgressColor = strokeProgressColor;
    [self setNeedsDisplay];
}

-(void)setStrokeRemainingColor:(UIColor *)strokeRemainingColor{
    _strokeRemainingColor = strokeRemainingColor;
    [self setNeedsDisplay];
}

- (void)setMinimumSize:(CGFloat)minimumSize
{
    _minimumSize = minimumSize;
    [self setNeedsDisplay];
}

#pragma mark - draw progress indicator view

-(void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    float minSize =  MIN(self.minimumSize, CGRectGetHeight(self.bounds));
    float lineWidth = _strokeWidth;
    if (lineWidth == -1.0) lineWidth = minSize * _strokeWidthRatio;
    float radius = (minSize - lineWidth) / 2.0;
    
    if(self.bgImageView.superview){
        self.bgImageView.frame = CGRectMake(0, 0,
                                            self.minimumSize-lineWidth-4,
                                            self.minimumSize-lineWidth-4);
        self.bgImageView.center = center;
    }
    
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, center.x, center.y);
    CGContextRotateCTM(context, -M_PI * 0.5);
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextBeginPath(context);
    CGContextAddArc(context, 0, 0, radius, 0, 2 * M_PI, 0);
    CGContextSetStrokeColorWithColor(context, _strokeRemainingColor.CGColor);
    CGContextStrokePath(context);
    CGContextBeginPath(context);
    CGContextAddArc(context, 0, 0, radius, 0, M_PI * (_progressValue * 2), 0);
    CGContextSetStrokeColorWithColor(context, _strokeProgressColor.CGColor);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

@end


@implementation TMCircleProgressIndicator

-(id)initWithFrame:(CGRect)frame showCancelButton:(BOOL)showCancelButton {
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat minimumSize = kXLCircleProgressIndicatorDefaultSize;
        float lineWidth = kXLCircleProgressIndicatorDefaultStrokeWidth;
        CGPoint center = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                         0,
                                                                         minimumSize-lineWidth-4,
                                                                         minimumSize-lineWidth-4)];
        self.bgImageView.backgroundColor = [UIColor blackColor];
        self.bgImageView.alpha = 0.2;
        self.bgImageView.clipsToBounds = TRUE;
        self.bgImageView.center = center;
        self.bgImageView.layer.cornerRadius = self.bgImageView.frame.size.width/2;
        [self addSubview:self.bgImageView];
            
        self.progressView = [[XLCircleProgressIndicator alloc] initWithFrame:self.bounds showCancelButton:showCancelButton];
        [self addSubview:self.progressView];
    }
    return self;
}

-(void)setProgressValue:(float)progressValue {
    [self.progressView setProgressValue:progressValue];
}

@end
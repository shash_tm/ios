//
//  TMMessagesMediaViewBubbleImageMasker.h
//  TrulyMadly
//
//  Created by Ankit on 06/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMMessagesMediaViewBubbleImageMasker : NSObject

+ (void)applyBubbleImageMaskToMediaView:(UIView *)mediaView isOutgoing:(BOOL)isOutgoing;
+(void)applyBubbleImageMaskToVoucherView:(UIView*)mediaView;

@end

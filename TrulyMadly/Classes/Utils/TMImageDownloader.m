//
//  TMImageDownloader.m
//  TrulyMadly
//
//  Created by Ankit on 07/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMImageDownloader.h"
#import "UIImageView+AFNetworking.h"
#import "TMImageDownload.h"
#import "TMAnalytics.h"
#import "TMFileManager.h"

NSString * const TMImageDownloadCompleteNotification = @"TMImageDownloadCompleteNotification";
NSString * const TMImageDownloadFailNotification = @"TMImageDownloadFailNotification";
NSString * const TMDownloadedImage = @"TMDownloadedImage";
NSString * const TMImageDownloadUrl = @"TMImageDownloadUrl";

@interface TMImageDownloader ()<TMImageDownloadDelegate>

@property(nonatomic,strong)NSMutableDictionary *imageDownloadInProgress;

@end

@implementation TMImageDownloader

-(instancetype)init {
    self = [super init];
    if(self) {
        self.imageDownloadInProgress = [NSMutableDictionary dictionary];
    }
    return self;
}
+(instancetype)sharedImageDownloader {
    static TMImageDownloader *imageDownloader = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        imageDownloader = [[TMImageDownloader alloc] init];
    });
    return imageDownloader;
}
-(UIImage*)imageAtPath:(NSString*)imagePath {
    NSData *imageData = [TMFileManager getFileAtPath1:imagePath];
    UIImage *cachedImage = nil;
    if(imageData) {
        cachedImage = [UIImage imageWithData:imageData];
    }
    return cachedImage;
}
-(NSData*)imageDataAtPath:(NSString*)imagePath {
    NSData *imageData = [TMFileManager getFileAtPath1:imagePath];
    return imageData;
}
-(void)cacheImageData:(NSData*)imageData atPath:(NSString*)path withName:(NSString*)imageName {
    [TMFileManager cacheData:imageData atPath:path withFileName:imageName];
}

-(void)downloadImageFromUrl:(NSURL*)imageURL cacheAtPath:(NSString*)path {
    //if image download already not in progress
    if( imageURL && (![self.imageDownloadInProgress objectForKey:imageURL.absoluteString]) ) {
        [self registerImageUrl:imageURL withDelegateHandler:self cachingPath:path];
    }
}
-(void)downloadAndCacheImageFromUrl:(NSString*)imageURLString {
    NSURL *imageURL = [NSURL URLWithString:imageURLString];
    //if image download already not in progress
    if( imageURL && (![self.imageDownloadInProgress objectForKey:imageURL.absoluteString]) ) {
        [self registerImageUrl:imageURL withDelegateHandler:self cachingPath:nil];
    }
}
-(void)downloadImageFromUrl:(NSURL*)imageURL {
    //if alreeady not in progress
    if(![self.imageDownloadInProgress objectForKey:imageURL.absoluteString]) {
        [self registerImageUrl:imageURL withDelegateHandler:self cachingPath:nil];
    }
}
-(void)registerImageUrl:(NSURL*)imageURL withDelegateHandler:(id)delegate cachingPath:(NSString*)path {
    //NSParameterAssert(imageURL != nil);
    TMImageDownload *imgDownload = [[TMImageDownload alloc] init];
    imgDownload.delegate = delegate;
    imgDownload.startDate = [NSDate date];
    imgDownload.cachingPath = path;
    [self.imageDownloadInProgress setObject:imgDownload forKey:imageURL.absoluteString];
    [imgDownload getImageWithURL:imageURL];
}
-(void)deRegisterImageUrl:(NSURL*)imageURL {
    TMImageDownload *imgDownload = [self.imageDownloadInProgress objectForKey:imageURL.absoluteString];
    if(imgDownload) {
        imgDownload.delegate = nil;
        [imgDownload cancelImageRequestOperation];
        [self.imageDownloadInProgress removeObjectForKey:imageURL.absoluteString];
    }
}
-(void)clearCachedImage:(NSURL*)imageURL {
    TMImageDownload *imgDownload = [[TMImageDownload alloc] init];
    [imgDownload removeCachedImageDownloadedFromURL:imageURL];
    imgDownload = nil;
}
-(void)clearCachedImageAtPath:(NSString*)imagePath {
    if(imagePath) {
        TMImageDownload *imgDownload = [[TMImageDownload alloc] init];
        [imgDownload removeCachedImageDownloadedFromPath:imagePath];
        imgDownload = nil;
    }
}
-(void)didCompleteImageDownload:(UIImage *)imageDownloaded fromUrl:(NSURL *)imageURL {
    TMImageDownload *imgDownload = [self.imageDownloadInProgress objectForKey:imageURL.absoluteString];
    if(imgDownload) {
        [self fetchImagesTracking:imgDownload.startDate status:@"success"];
    }
    [self deRegisterImageUrl:imageURL];
    NSDictionary *userInfo = NULL;
    NSString *notification = NULL;
    
    if(imageDownloaded) {
        userInfo = @{TMDownloadedImage:imageDownloaded,
                     TMImageDownloadUrl:imageURL};
        notification = TMImageDownloadCompleteNotification;
    }
    else {
        userInfo = @{TMImageDownloadUrl:imageURL};
        notification = TMImageDownloadFailNotification;
    }
    
    [self fireImageDownloadNotification:notification
                                fromURL:imageURL
                           withUserInfo:userInfo];
}

-(void)didFailToDownloadImageFromUrl:(NSURL *)imageURL {
    TMImageDownload *imgDownload = [self.imageDownloadInProgress objectForKey:imageURL.absoluteString];
    if(imgDownload) {
        [self fetchImagesTracking:imgDownload.startDate status:@"error"];
    }
    NSDictionary *userInfo = @{TMImageDownloadUrl:imageURL};
    [self fireImageDownloadNotification:TMImageDownloadFailNotification
                                fromURL:imageURL
                           withUserInfo:userInfo];
}


-(void)fireImageDownloadNotification:(NSString*)notification
                             fromURL:(NSURL*)imageURL
                        withUserInfo:(NSDictionary*)userInfoDictionary {
    [self deRegisterImageUrl:imageURL];
    [[NSNotificationCenter defaultCenter] postNotificationName:notification
                                                        object:nil
                                                      userInfo:userInfoDictionary];
}

-(void)fetchImagesTracking:(NSDate*)startDate status:(NSString*)status {
    NSDate* endDate = [NSDate date];
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"matches" forKey:@"eventCategory"];
    [eventDict setObject:@"fetching_images" forKey:@"eventAction"];
    [eventDict setObject:startDate  forKey:@"startDate"];
    [eventDict setObject:endDate  forKey:@"endDate"];
    [eventDict setObject:status  forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}

@end


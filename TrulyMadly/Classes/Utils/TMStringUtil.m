//
//  TMStringUtil.m
//  TrulyMadly
//
//  Created by Ankit Jain on 11/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMStringUtil.h"

@implementation TMStringUtil

+(NSString*)stringByReplacingInvalidContent:(NSString*)text {
    if((![text isKindOfClass:[NSString class]])){
        return @"";
    }
    return text;
}

+(CGRect)textRectForString:(NSDictionary*)textInfoDictionary {
    
    CGFloat maxWidth = [[textInfoDictionary objectForKey:@"kmaxwidth"] floatValue];
    CGFloat maxHeight = ([textInfoDictionary objectForKey:@"kmaxheight"]) ? [[textInfoDictionary objectForKey:@"kmaxheight"] floatValue] : NSUIntegerMax;
    
    CGSize constraint = CGSizeMake(maxWidth,maxHeight);
    UIFont *font = [textInfoDictionary objectForKey:@"kfont"];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    NSString *text = [textInfoDictionary objectForKey:@"ktext"];
    CGRect rect = [text boundingRectWithSize:constraint
                                     options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                     attributes:attributes
                                     context:nil];
    
    //NSLog(@"TMStringUtil---text:%@---rect:%@",text,NSStringFromCGRect(rect));
    
    return rect;
}

+(CGSize)contentSizeForStringArray:(NSArray*)contents

                    withAttributes:(NSDictionary*)textAttributes{
    if(!contents){
        assert(@"text array nil");
    }
    CGFloat xPosition = ([textAttributes objectForKey:@"xPos"]) ? [[textAttributes objectForKey:@"xPos"] floatValue] : 0;
    CGFloat xPos = xPosition;//([textAttributes objectForKey:@"xPos"]) ? [[textAttributes objectForKey:@"xPos"] floatValue] : 0;
    CGFloat yPos = 0;
    CGFloat width = 0;
    CGFloat height = 0;
    CGFloat prevHeight = 0;
    //CGFloat contentHeight = 0;
    
    CGFloat maxWidth = ([textAttributes objectForKey:@"kmaxwidth"]) ? [[textAttributes objectForKey:@"kmaxwidth"] floatValue] : 0;
    CGFloat comaSpace = ([textAttributes objectForKey:@"cspace"]) ? [[textAttributes objectForKey:@"cspace"] floatValue] : 0;
    UIFont *font = [textAttributes objectForKey:@"kfont"];
    
    CGFloat interimSpace = ([textAttributes objectForKey:@"kinterimspace"]) ? [[textAttributes objectForKey:@"kinterimspace"] floatValue]: 0;
    
    for (int i=0; i<contents.count;i++){
        NSString *content = [contents objectAtIndex:i];
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:maxWidth], @"kmaxwidth", content, @"ktext", nil];
        width = [TMStringUtil textRectForString:dict].size.width;
        height = [TMStringUtil textRectForString:dict].size.height;
      
        if( (contents.count != 1) &&  (i<contents.count-1)) {
            comaSpace = ([textAttributes objectForKey:@"cspace"]) ? [[textAttributes objectForKey:@"cspace"] floatValue] : 0;
        }
        else {
            comaSpace = 0;
        }
        
        //xPos = xPos + ceilf(width) + comaSpace;
        CGFloat textLayoutRectXMax = xPos + width + comaSpace;
        if(textLayoutRectXMax > maxWidth) {
            xPos = xPosition;
            CGFloat verticalSpace = (i < (contents.count-1)) ? interimSpace: 0;
            yPos = yPos + prevHeight + verticalSpace;
        }
        
        prevHeight = height;
        
        xPos = xPos + width + comaSpace;
    }
    
    CGSize finalsize = CGSizeMake(maxWidth, (yPos + height));
    return finalsize;
}





@end

//
//  TMUserDataCacheController.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMError;
@class TMEditPreference;

typedef void (^_Nonnull LoginSuccessBlock)(void);
typedef void (^_Nonnull StatusBlock)(void);

@interface TMUserDataController : NSObject

//-(void)fetchAndUpdateUserBasicDataFromURL:(NSURL*)userdataURL;
//-(void)validateUserWithLoginParams:(NSDictionary* _Nonnull)loginParams withResponse:(void(^_Nonnull)(TMError* _Nullable error))loginBlock;

-(void)getUserBasicDataFromURLString:(NSString*_Nonnull)dataURLString
                                with:(void(^_Nonnull)(BOOL status, TMError* _Nullable error))responseBlock;

-(void)getUserStateWithResponse:(void(^_Nonnull)(TMError* _Nullable error))responseBlock;

-(void)validateUserWithLoginParams:(NSDictionary* _Nonnull)loginParams
           didSuccessfullyValidate:(LoginSuccessBlock)loginSuccessBlock
              didCompleteLoginStep:(void(^_Nonnull)(TMError* _Nullable error))completionBlock;

-(void)getPreferenceDataWithResponse:(void(^_Nonnull)(TMEditPreference  * _Nullable preference, TMError  * _Nullable error))responseBlock;

-(void)getAvailableCityList:(void (^_Nonnull)(NSArray * _Nonnull cityList,NSArray * _Nonnull popularCityList))responseBlock;
-(void)getStateList:(void(^_Nonnull)(NSArray* _Nonnull stateList))responseBlock;
-(void)getPopularCityList:(void(^_Nonnull)(NSArray* _Nonnull popularCityList))responseBlock;
-(void)getCityList:(void(^_Nonnull)(NSArray* _Nonnull cityList))responseBlock;
-(void)getDegreeData:(void(^_Nonnull)(NSArray* _Nonnull degreeList))responseBlock;
-(NSArray*_Nonnull)getHeightData;

@end

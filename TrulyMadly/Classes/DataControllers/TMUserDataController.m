//
//  TMUserDataCacheController.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMUserDataController.h"
#import "TMUserDataManager.h"
#import "TMUserDataCacheController.h"
#import "TMEditPreferenceManager.h"
#import "TMEditPreference.h"
#import "TMError.h"
#import "TMLog.h"

@interface TMUserDataController ()

@property(nonatomic,strong)TMUserDataManager* userDataManager;
@property(nonatomic,strong)TMEditPreferenceManager* editPrefManager;
@property(nonatomic,strong)TMUserDataCacheController* userDataCacheController;

@end

@implementation TMUserDataController

-(TMUserDataManager*)userDataManager {
    if(!_userDataManager) {
        _userDataManager = [[TMUserDataManager alloc] init];
    }
    return _userDataManager;
}
-(TMEditPreferenceManager*)editPrefManager {
    if(!_editPrefManager) {
        _editPrefManager = [[TMEditPreferenceManager alloc] init];
    }
    return _editPrefManager;
}
-(TMUserDataCacheController*)userDataCacheController {
    if(!_userDataCacheController) {
        _userDataCacheController = [[TMUserDataCacheController alloc] init];
    }
    return _userDataCacheController;
}
-(void)getUserStateWithResponse:(void(^_Nonnull)(TMError* _Nullable error))responseBlock {
    
    if(self.userDataManager.isNetworkReachable) {
        [self.userDataManager getUserDataWithResponse:^(NSDictionary *response, TMError *error) {
            if(response) {
                NSString *queryString = response[@"basics_data"][@"url"];
                [self getUserBasicDataFromURLString:queryString with:^(BOOL status, TMError * _Nullable error) {
                    TMLOG(@"executing login step completion block");
                    responseBlock(error);
                }];
            }
            else {
                responseBlock(error);
            }
        }];
    }
    else {
        TMError *tmError = [[TMError alloc] initWithNoNetworkErrorCode];
        responseBlock(tmError);
    }
}
-(void)validateUserWithLoginParams:(NSDictionary* _Nonnull)loginParams
           didSuccessfullyValidate:(LoginSuccessBlock)loginSuccessBlock
              didCompleteLoginStep:(void(^_Nonnull)(TMError* _Nullable error))completionBlock {
    
    if(self.userDataManager.isNetworkReachable) {
        [self.userDataManager validateLogin:loginParams with:^(NSDictionary *response, TMError *error) {
            if(response) {
                loginSuccessBlock();
                
                NSString *queryString = response[@"basics_data"][@"url"];
                [self getUserBasicDataFromURLString:queryString with:^(BOOL status, TMError * _Nullable error) {
                    TMLOG(@"executing login step completion block");
                    completionBlock(error);
                }];
            }
            else {
                completionBlock(error);
            }
        }];
    }
    else {
        TMError *tmError = [[TMError alloc] initWithNoNetworkErrorCode];
        completionBlock(tmError);
    }
}
-(void)getPreferenceDataWithResponse:(void(^_Nonnull)(TMEditPreference  * _Nullable preference, TMError  * _Nullable error))responseBlock {
    self.editPrefManager.trackEventDictionary = [self getEditPreferenceTrackingParams];
    [self.editPrefManager getUserPreferenceDataWithResponse:^(TMEditPreference *preference, TMError *error) {
        if(preference) {
            NSString *queryString = preference.basicDataURLString;
            [self getUserBasicDataFromURLString:queryString with:^(BOOL status, TMError * _Nullable error) {
                responseBlock(preference,error);
            }];
        }
        else {
            responseBlock(preference,error);
        }
    }];
}
-(void)getUserBasicDataFromURLString:(NSString*_Nonnull)dataURLString
                                with:(void(^_Nonnull)(BOOL status, TMError* _Nullable error))responseBlock {
    
    BOOL isUserBasicDataURLValid = [self.userDataCacheController validateUserBasicDataURLString:dataURLString];
    if(isUserBasicDataURLValid) {
        TMLOG(@"user basic data cached for URL:%@",dataURLString);
        responseBlock(true,nil);
    }else {
        TMLOG(@"fetch basic data from server as url is changed");
        if(self.userDataManager.isNetworkReachable) {
            [self.userDataManager fetchBasicDataFromURLString:dataURLString withResponse:^(NSDictionary *reponseData, TMError *error) {
                if(reponseData && !error) {
                    TMLOG(@"sucessfully fetched user basic data");
                    [self.userDataCacheController cacheUserBasicData:reponseData withURLString:dataURLString didCacheData:^{
                        responseBlock(TRUE,nil);
                    }];
                }
                else {
                    responseBlock(FALSE,error);
                }
            }];
        }
        else {
            TMError *tmError = [[TMError alloc] initWithNoNetworkErrorCode];
            responseBlock(FALSE,tmError);
        }
    }
}

-(NSArray*)getHeightDataList {
    NSMutableArray *dataList = [[NSMutableArray alloc] init];
    NSArray *keys, *values;
    keys = [NSArray arrayWithObjects:@"4-0", @"4-1", @"4-2", @"4-3", @"4-4", @"4-5", @"4-6", @"4-7", @"4-8", @"4-9", @"4-10", @"4-11",
            @"5-0", @"5-1", @"5-2", @"5-3", @"5-4", @"5-5", @"5-6", @"5-7", @"5-8", @"5-9", @"5-10", @"5-11",
            @"6-0", @"6-1", @"6-2", @"6-3", @"6-4", @"6-5", @"6-6", @"6-7", @"6-8", @"6-9", @"6-10", @"6-11",
            @"7-0", @"7-1", @"7-2", @"7-3", @"7-4", @"7-5", @"7-6", @"7-7", @"7-8", @"7-9", @"7-10", @"7-11", nil];
    values = [NSArray arrayWithObjects:@"4 Feet 0 Inch", @"4 Feet 1 Inch", @"4 Feet 2 Inch", @"4 Feet 3 Inch", @"4 Feet 4 Inch", @"4 Feet 5 Inch", @"4 Feet 6 Inch", @"4 Feet 7 Inch", @"4 Feet 8 Inch", @"4 Feet 9 Inch", @"4 Feet 10 Inch", @"4 Feet 11 Inch",
              @"5 Feet 0 Inch", @"5 Feet 1 Inch", @"5 Feet 2 Inch", @"5 Feet 3 Inch", @"5 Feet 4 Inch", @"5 Feet 5 Inch", @"5 Feet 6 Inch", @"5 Feet 7 Inch", @"5 Feet 8 Inch", @"5 Feet 9 Inch", @"5 Feet 10 Inch", @"5 Feet 11 Inch",
              @"6 Feet 0 Inch", @"6 Feet 1 Inch", @"6 Feet 2 Inch", @"6 Feet 3 Inch", @"6 Feet 4 Inch", @"6 Feet 5 Inch", @"6 Feet 6 Inch", @"6 Feet 7 Inch", @"6 Feet 8 Inch", @"6 Feet 9 Inch", @"6 Feet 10 Inch", @"6 Feet 11 Inch",
              @"7 Feet 0 Inch", @"7 Feet 1 Inch", @"7 Feet 2 Inch", @"7 Feet 3 Inch", @"7 Feet 4 Inch", @"7 Feet 5 Inch", @"7 Feet 6 Inch", @"7 Feet 7 Inch", @"7 Feet 8 Inch", @"7 Feet 9 Inch", @"7 Feet 10 Inch", @"7 Feet 11 Inch",nil];
    for (int i = 0; i < [keys count]; i++) {
        NSDictionary* heightData = @{@"key":keys[i],@"value":values[i]};
        [dataList addObject:heightData];
    }
    return dataList;
}
-(NSArray*_Nonnull)getHeightData {
    return [self getHeightDataList];
}
-(void)getAvailableCityList:(void (^)(NSArray * _Nonnull cityList,NSArray * _Nonnull popularCityList))responseBlock {
    [self.userDataCacheController getAvailableCachedCityList:^(NSArray *popularCityList, NSArray *cityList) {
        responseBlock(cityList,popularCityList);
    }];
}
-(void)getPopularCityList:(void(^_Nonnull)(NSArray* _Nonnull popularCityList))responseBlock {
    [self.userDataCacheController getCachedPopularCityList:^(NSArray *popularCityList) {
        responseBlock(popularCityList);
    }];
}
-(void)getStateList:(void(^_Nonnull)(NSArray* _Nonnull stateList))responseBlock {
    [self.userDataCacheController getCachedStateList:^(NSArray *stateList) {
        responseBlock(stateList);
    }];
}
-(void)getCityList:(void(^_Nonnull)(NSArray* _Nonnull cityList))responseBlock {
    [self.userDataCacheController getCachedCityList:^(NSArray *cityList) {
        responseBlock(cityList);
    }];
}
-(void)getDegreeData:(void(^_Nonnull)(NSArray* _Nonnull degreeList))responseBlock {
    [self.userDataCacheController getCachedDegreeList:^(NSArray *cityList) {
        responseBlock(cityList);
    }];
}

-(NSMutableDictionary*)getEditPreferenceTrackingParams {
    NSMutableDictionary* trackingParams = [NSMutableDictionary dictionary];
    trackingParams[@"screenName"] = @"TMEditPreferenceViewController";
    trackingParams[@"eventCategory"] = @"edit_preference";
    trackingParams[@"eventAction"] = @"page_load";
    return trackingParams;
}

@end



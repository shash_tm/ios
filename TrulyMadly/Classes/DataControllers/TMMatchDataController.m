//
//  TMMatchDataController.m
//  TrulyMadly
//
//  Created by Ankit on 25/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>

#import "TMMatchDataController.h"
#import "TMMatchCacheController.h"
#import "TMMatchManager.h"
#import "TMImageDownloader.h"
#import "TMTimestampFormatter.h"

#import "TMProfile.h"
#import "TMSystemMessage.h"
#import "TMMatchResponse.h"
#import "TMError.h"

#import "TMUserSession.h"
#import "TMDataStore.h"
#import "TMLog.h"
#import "TMDataStore+TMAdAddtions.h"



#define KMATCHREQ_BATCHCOUNT @"com.matchreq.batchcount"

@interface TMMatchDataController ()

@property(nonatomic,strong)TMMatchManager *matchManager;
@property(nonatomic,strong)TMMatchCacheController *cacheController;
@property(nonatomic,strong)TMMatchResponse *matchResponse;

@property(nonatomic,copy)RequestStartBlock requestStartBlock;
@property(nonatomic,copy)RequestStartBlock requestLoadingBlock;

@property(nonatomic,strong)NSDictionary *nudgeDataDictionary;
@property(nonatomic,strong)NSDictionary *fbMutualConnections;
@property(nonatomic,assign)NSInteger matchCounter;
@property(nonatomic,assign)BOOL matchDataLoadInProgress;
@property(nonatomic,assign)BOOL isMatchDataFetchedFromCache;
@property(nonatomic,assign)BOOL isFirstMatchActionAfterSecondBatchCall;
@end

@implementation TMMatchDataController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.matchDataLoadInProgress = FALSE;
    }
    return self;
}

-(TMMatchManager*)matchManager {
    if(_matchManager == nil) {
        _matchManager = [[TMMatchManager alloc] init];
    }
    return _matchManager;
}
-(TMMatchCacheController*)cacheController {
    if(_cacheController == nil) {
        _cacheController = [[TMMatchCacheController alloc] init];
    }
    return _cacheController;
}

#pragma mark- Match Data
-(NSInteger)getAvailableMatcheCount {
    return [self.matchResponse profileCount];
}
-(BOOL)isMatchesAvailabe {
    BOOL status = ([self.matchResponse profileCount]) ? TRUE : FALSE;
    return status;
}
-(TMProfile*)getMatchProfile {
    self.isFirstMatchActionAfterSecondBatchCall = FALSE;
    TMProfile *matchProfile = [self matchProfileAtIndex:self.matchCounter++];
    return matchProfile;
}
//SeventyNine Profile Ad Changes
-(TMProfile*) getMatchAdProfile {
    self.isFirstMatchActionAfterSecondBatchCall = FALSE;
    TMProfile *matchProfile = [self.matchResponse matchProfileForAd];
    return matchProfile;
}
-(void)getMatchDataWithForceRefresh:(BOOL)forceRefresh
                   willStartRequest:(RequestStartBlock)requestStartBlock
                  didStartRequest:(RequestLoadingBlock)requestLoadingBlock
                        profileData:(void (^)(TMProfile* profile))success
                              error:(void (^)(TMError* error))failure {
    
    if(!self.matchDataLoadInProgress) {
        self.matchDataLoadInProgress = TRUE;
        self.requestStartBlock = requestStartBlock;
        self.requestLoadingBlock = requestLoadingBlock;
        
        [self getMatchDataWithForceRefresh:forceRefresh response:^(TMProfile* profile,TMError* error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.matchDataLoadInProgress = FALSE;
                ///check if batchcount=2 call is completed or not
                [self checkAndProcessSecondBatchCall];
                
                //////
                if(!error) {
                    success(profile);
                }
                else {
                    failure(error);
                }
            });
        }];
    }
}
-(void)getMatchDataWithForceRefresh:(BOOL)forceRefresh response:(void(^)(TMProfile* profile,TMError* error))responseBlock {
    self.matchResponse = nil;
    self.nudgeDataDictionary = nil;
    self.fbMutualConnections = nil;
    self.matchCounter = 0;
    self.isMatchDataFetchedFromCache = FALSE;
    self.requestStartBlock();
   
    [self getMatchResponseDictionaryWithForceRefresh:forceRefresh response:^(NSDictionary *matchResponse) {
        if(matchResponse) {
            self.matchResponse = [[TMMatchResponse alloc] initWithDictionary:matchResponse];
            self.nudgeDataDictionary = self.matchResponse.nudgeDict;
            
            TMProfile *profile = [self getMatchProfile];
            responseBlock(profile,nil);
        }
    } error:^(TMError *error) {
        responseBlock(nil,error);
    }];
}
-(void)getMatchResponseDictionaryWithForceRefresh:(BOOL)forceRefresh
                                         response:(void(^)(NSDictionary* matchResponse))successBlock
                                            error:(void(^)(TMError *error))errorBlock {
    if(forceRefresh) {
        [self getMatchDataFromServerWithResponse:^(NSDictionary *matchResponse) {
            successBlock(matchResponse);
        } error:^(TMError *error) {
            errorBlock(error);
        }];
    }
    else {
        [self.cacheController getCachedMatchResponse:^(NSMutableDictionary *cachedMatchResponse) {
            if(cachedMatchResponse) {
                self.isMatchDataFetchedFromCache = TRUE;
                successBlock(cachedMatchResponse);
            }
            else {
                [self getMatchDataFromServerWithResponse:^(NSDictionary *matchResponse) {
                    successBlock(matchResponse);
                } error:^(TMError *error) {
                    errorBlock(error);
                }];
            }
        }];
    }
}
-(void)getMatchDataFromServerWithResponse:(void(^)(NSDictionary* matchResponse))successBlock
                                    error:(void(^)(TMError *error))errorBlock {
    [self.cacheController invalidateCache];
    [self resetBatchCount];
    NSDictionary *params = @{@"batchCount":@(1)};
    [self getMatchDataWithParams:params
                callLoadingBlock:TRUE
                    responseData:^(NSDictionary *responseDictionary) {
        
        successBlock(responseDictionary);
        
    }error:^(TMError *error) {
        errorBlock(error);
    }];
}
-(void)checkAndProcessSecondBatchCall {
    NSInteger batchCount = [self getBatchCount];
    if(batchCount == 1) {
        TMLOG(@"Executing batch count 2 call for matches");
        NSDictionary *params = @{@"batchCount":@(2),@"fetch_all_matches":@"true"};
        [self getMatchDataWithParams:params
                    callLoadingBlock:FALSE
                        responseData:^(NSDictionary *responseDictionary) {
            [self updateBatchCount];
            [self.cacheController getCachedMatchResponse:^(NSMutableDictionary *cachedMatchResponse) {
                self.matchCounter = 0;
                self.isFirstMatchActionAfterSecondBatchCall = TRUE;
                if(cachedMatchResponse && [cachedMatchResponse isKindOfClass:[NSDictionary class]]) {
                    self.matchResponse = [[TMMatchResponse alloc] initWithDictionary:cachedMatchResponse];
                }
            }];

        } error:^(TMError *error) {
            //do nothing
        }];
    }
}
-(NSInteger)getBatchCount {
    NSNumber *batchCount = [TMDataStore retrieveObjectforKey:KMATCHREQ_BATCHCOUNT];
    if(batchCount) {
        return [batchCount integerValue];
    }
    return 1;
}
-(void)updateBatchCount {
    NSInteger batchCount = [self getBatchCount];
   [TMDataStore setObject:[NSNumber numberWithInteger:(++batchCount)] forKey:KMATCHREQ_BATCHCOUNT];
}
-(void)resetBatchCount {
    [TMDataStore setObject:@(1) forKey:KMATCHREQ_BATCHCOUNT];
}
-(void)updateSparkCountFromMatchResponse:(NSDictionary*)matchResponseDictionary {
    NSString *sparkCount = matchResponseDictionary[@"sparkCountersLeft"];
    if(sparkCount && (![sparkCount isKindOfClass:[NSNull class]])) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        [userSession updateAvailableSparkCount:sparkCount];
    }
}
-(void)updateSelectDataFromMatchResponse:(NSDictionary*)matchResponseDictionary {
    NSDictionary *myData = matchResponseDictionary[@"my_data"];
    NSDictionary *selectData = myData[@"my_select"];
    [[TMUserSession sharedInstance] setMySelectStatus:selectData];
}
-(void)cacheMatchResponse:(NSDictionary*)responseDictionary {
    NSMutableDictionary *mutableResponse = [responseDictionary mutableCopy];
    NSArray *matchData = mutableResponse[@"data"];
    [mutableResponse removeObjectForKey:@"data"];
    [self.cacheController cacheMatchData:matchData cacheMetaData:mutableResponse];
}
-(void)getMatchDataWithParams:(NSDictionary*)params
             callLoadingBlock:(BOOL)callLoadingBlock
                 responseData:(void(^)(NSDictionary *responseDictionary))result
                        error:(void(^)(TMError *))failure {
    
    if(self.matchManager.isNetworkReachable) {
        
        if(callLoadingBlock && self.requestLoadingBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.requestLoadingBlock();
                self.requestLoadingBlock = nil;
            });
        }

        self.matchManager.trackEventDictionary = [self matchNetworkRequestTrackingParams];
        [self.matchManager getMatchesWithParams:params result:^(NSDictionary *responseDictionary, TMError *error) {
            if(responseDictionary) {
                [self updateSparkCountFromMatchResponse:responseDictionary];
                [self updateSelectDataFromMatchResponse:responseDictionary];
                [self cacheMatchResponse:responseDictionary];
                result(responseDictionary);
            }
            else {
                failure(error);
            }
        }];
    }
    else {
        TMError *error = [[TMError alloc] init];
        error.errorCode = TMERRORCODE_NONETWORK;
        failure(error);
    }
}
-(void)sendUserActionOnMatchProfile:(NSString *)matchId
                          actionURL:(NSString*)actionURL
                  moveToNextProfile:(void(^)(BOOL moveToNextProfile))block
                           response:(void(^)(NSDictionary *mutualData))success
                              error:(void(^)(TMError* error))failure {
    
    if(self.matchManager.isNetworkReachable) {
        block(TRUE);
        if(matchId != nil) {
           [self markMatchProfileAsShown:matchId.integerValue];
        }
        if(self.isFirstMatchActionAfterSecondBatchCall) {
            [self.cacheController getCachedMatchResponse:^(NSMutableDictionary *cachedMatchResponse) {
                if(cachedMatchResponse && [cachedMatchResponse isKindOfClass:[NSDictionary class]]) {
                    self.matchResponse = [[TMMatchResponse alloc] initWithDictionary:cachedMatchResponse];
                }
                else {
                    [self.matchResponse removerAllProfiles];
                }
            }];
        }
        if(actionURL != nil) {
            [self.matchManager sendMatchWithUrl:actionURL completionBlock:^(NSDictionary *mutualData, TMError *error) {
                //  success(mutualData);
            }];
        }
        
    }
    else {
        TMError *error = [[TMError alloc] init];
        error.errorCode = TMERRORCODE_NONETWORK;
        failure(error);
    }
}
-(void)markProfileAsShownForSendSparkActionWithMatchId:(NSInteger)matchId {
    [self markMatchProfileAsShown:matchId];
    if(self.isFirstMatchActionAfterSecondBatchCall) {
        [self.cacheController getCachedMatchResponse:^(NSMutableDictionary *cachedMatchResponse) {
            if(cachedMatchResponse && [cachedMatchResponse isKindOfClass:[NSDictionary class]]) {
                self.matchResponse = [[TMMatchResponse alloc] initWithDictionary:cachedMatchResponse];
            }
            else {
                [self.matchResponse removerAllProfiles];
            }
        }];
    }
}
-(void)updateFBAccessToken:(void(^)(BOOL isTokenExpired))responseBlock {
    BOOL canMakeRequest = TRUE;
    NSString *key = @"com.match.fbtokenupdate";
    NSDate *lastReqTime = [TMDataStore retrieveObjectforKey:key];
    if(lastReqTime) {
        NSDate *date = [NSDate date];
        NSTimeInterval mins = [[TMTimestampFormatter sharedFormatter] minsBetweenDate:lastReqTime andDate:date];
        NSInteger hours = (mins/60);
        if(hours < 24) {
            canMakeRequest = FALSE;
        }
    }
    
    if(canMakeRequest) {
        FBAccessTokenData *accessTokenData = [[FBSession activeSession] accessTokenData];
        NSString *fbToken = [accessTokenData accessToken];
        if(fbToken) {
            [self.matchManager sendFBAccesToken:fbToken completionBlock:^(BOOL isDeviceTokenExpired) {
                [TMDataStore setObject:[NSDate date] forKey:key];
                responseBlock(isDeviceTokenExpired);
            }];
        }
    }
}
-(void)markMatchProfileAsShown:(NSInteger)matchId {
    [self.cacheController markMatchAsShown:matchId];
}
-(void)clearCacheData {
    [self.cacheController invalidateCache];
}
-(void)updateOptimizationParams:(NSDictionary*)params {
    
    NSNumber *loadMorePoint = params[@"secondBatchTriggerPoint"];
    if (loadMorePoint) {
        [TMDataStore setObject:loadMorePoint forKey:@"kloadmorecount"];
    }
    
    NSNumber *appCacheExpiry = params[@"appCacheExpiryTime"];
    if(appCacheExpiry) {
        [TMDataStore setObject:appCacheExpiry forKey:@"cacheexp"];
    }
}
-(void)cacheMatchesProfileImages {
    int maxLimit  = (int)self.matchCounter+3;
    for (int i=(int)self.matchCounter+1; i<maxLimit; i++) {
        TMProfile *profile = [self matchProfileAtIndex:i];
        if(profile) {
            TMImageDownloader *imageDownloader = [TMImageDownloader sharedImageDownloader];
            [imageDownloader downloadAndCacheImageFromUrl:profile.basicInfo.imageUrlString];
        }
    }
}

#pragma mark- User Data
-(BOOL)isValidProfilePic {
    BOOL profilePicStatus = self.matchResponse.isValidProfilePic;
    return profilePicStatus;
}
-(BOOL)hasPhotos {
    BOOL profilePicStatus = self.matchResponse.hasPhotos;
    return profilePicStatus;
}
-(BOOL)hasLikedInThePast {
    BOOL profilePicStatus = self.matchResponse.hasLikedInThePast;
    return profilePicStatus;
}

#pragma mark- User Nudge Data
-(BOOL)isNudgeAvailable {
    if(self.matchResponse.isNudgeAvailable && self.matchResponse.nudgeDict != nil) {
        return TRUE;
    }
    return FALSE;
}
-(NSString*)nudgeKeyForCurrentMatch {
    NSNumber *key = [NSNumber numberWithInteger:self.matchCounter];

    if ([self.nudgeDataDictionary objectForKey:key]) {
        id msg = self.nudgeDataDictionary[key];
        if(![msg isKindOfClass:[NSNull class]]) {
            return self.nudgeDataDictionary[key];
        }
        return @"";
    }
    return @"";
}

#pragma mark- System Messages
-(TMSystemMessageLinkType)systemMessageLinkTypeForFirstTile {
    TMSystemMessageLinkType systemMsgLinkType = [self.matchResponse.systemMessage getSystemMessageLinkTypeForFirstTile];
    return systemMsgLinkType;
    
}
-(TMSystemMessageLinkType)systemMessageTypeForLastTileLink {
    TMSystemMessageLinkType systemMsgLinkType = [self.matchResponse.systemMessage getSystemMessageTypeForLastTileLink];
    return systemMsgLinkType;
}
-(BOOL)isFirstTileAvailable {
    BOOL status = self.matchResponse.systemMessage.isFirstTileAvailable;
    return status;
}
-(NSString*)getFirstSystemTileTitle {
    return [self.matchResponse.systemMessage getFirstTileTitle];
}
-(NSString*)getLastTileSystemMessage {
    return self.matchResponse.systemMessage.lastTileMessage;
}
-(NSString*)getLikeActionSystemMessage {
    return [self.matchResponse.systemMessage getLikeActionMessage];
}
-(BOOL)isLastSystemTileVisible {
    if (self.matchCounter <= [self.matchResponse profileCount]) {
        return FALSE;
    }
    return TRUE;
}

#pragma mark - Female Rejected Users Timestamp calculation method
-(BOOL)isToastRequiredForFemaleProfileVisibility {
    if ([[TMUserSession sharedInstance].user isUserFemale] && ([TMDataStore containsObjectForKey:@"is_discovery_on"] && ![TMDataStore retrieveBoolforKey:@"is_discovery_on"])) {
        return  [self performPhotoFlowTimestampCalculation];
    }
    return NO;
}

//SeventyNine Ad Changes
- (BOOL)canShowInterstitialAd {
    
    BOOL showInterstitialAd = NO;
    
    NSInteger matchCount, currentProfileIndex;
    matchCount = [self getAvailableMatcheCount];
    currentProfileIndex = self.matchCounter;
    if(currentProfileIndex > matchCount) {
        showInterstitialAd = YES;
    }
    return showInterstitialAd;
}

#pragma mark- Internal Methods
-(TMProfile*)matchProfileAtIndex:(NSInteger)index {
    TMProfile *profile = [self.matchResponse profileForIndex:index];
    return profile;
}
-(void)sendUserAttributeToMoEngage {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    [userSession sendUserAttributeToMoEngage];
}
-(NSMutableDictionary*)matchNetworkRequestTrackingParams {
    NSMutableDictionary *trackParams = [[NSMutableDictionary alloc] init];
    trackParams[@"screenName"] = @"TMMatchesViewController";
    trackParams[@"eventCategory"] = @"matches";
    trackParams[@"eventAction"] = @"page_load";
    return trackParams;
}
-(BOOL)performPhotoFlowTimestampCalculation {
    // show toast message for the profile discovery
    if (![TMDataStore retrieveBoolforKey:@"is_discovery_on"]) {
        //profile discovery is off
        
        BOOL isProfileDiscoveryToastToBeShown = NO;
        
        //check for timestamp
        if (![TMDataStore retrieveObjectforKey:PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP]) {
            
            isProfileDiscoveryToastToBeShown = YES;
        }
        else {
            //timestamp present
            //compare the time difference
            
            int requiredDayDifference = 3;
            
            if ([[NSDate date] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSinceReferenceDate:[[TMDataStore retrieveObjectforKey:PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP] doubleValue]]] > requiredDayDifference*24*60*60) {
                
                isProfileDiscoveryToastToBeShown = YES;
            }
        }
        if (isProfileDiscoveryToastToBeShown) {
            //update timestamp
            //store it
            NSTimeInterval currentTimeInterval = [NSDate timeIntervalSinceReferenceDate];
            [TMDataStore setObject:[NSNumber numberWithDouble:currentTimeInterval] forKey:PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP];
            [TMDataStore synchronize];
            
            return isProfileDiscoveryToastToBeShown;
        }
    }
    return NO;
}

-(BOOL)canExecuteLoadMoreRequest {
    BOOL status = FALSE;
    NSInteger matchCount = [self getAvailableMatcheCount];
    NSInteger triggerValue = [self loadMoreTriggerValue];
    TMLOG(@"MatchCount:%ld MatchCounter:%ld LoadMoreCount:%ld",(long)matchCount,(long)self.matchCounter,(long)triggerValue);
    TMLOG(@"HasMoreMatches:%d",self.matchResponse.hasMoreMatches);
    if( (self.matchCounter > (matchCount - triggerValue) ) && self.matchResponse.hasMoreMatches ) {
        status = TRUE;
    }
    
    return status;
}
-(NSInteger)loadMoreTriggerValue {
    NSInteger loadMoreTriigerPoint = 10;
    NSNumber*loadMoreCount = [TMDataStore retrieveObjectforKey:@"kloadmorecount"];
    if(loadMoreCount) {
        loadMoreTriigerPoint = loadMoreCount.integerValue;;
    }
    return loadMoreTriigerPoint;
}

@end

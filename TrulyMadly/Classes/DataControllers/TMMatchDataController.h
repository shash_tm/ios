//
//  TMMatchDataController.h
//  TrulyMadly
//
//  Created by Ankit on 25/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@class TMProfile;
@class TMError;

typedef void (^RequestStartBlock)(void);
typedef void (^RequestLoadingBlock)(void);

@interface TMMatchDataController : NSObject

@property(nonatomic,strong,readonly)NSDictionary *nudgeDataDictionary;

-(NSInteger)getAvailableMatcheCount;
-(BOOL)isMatchesAvailabe;
-(TMProfile*)getMatchProfile;

-(void)getMatchDataWithForceRefresh:(BOOL)forceRefresh
                   willStartRequest:(RequestStartBlock)requestStartBlock
                    didStartRequest:(RequestLoadingBlock)requestLoadingBlock
                        profileData:(void (^)(TMProfile* profile))success
                              error:(void (^)(TMError* error))failure;

-(void)sendUserActionOnMatchProfile:(NSString *)matchId
                          actionURL:(NSString*)actionURL
                  moveToNextProfile:(void(^)(BOOL moveToNextProfile))block
                           response:(void(^)(NSDictionary *mutualData))success
                              error:(void(^)(TMError* error))failure;
-(void)markProfileAsShownForSendSparkActionWithMatchId:(NSInteger)matchId;
-(void)markMatchProfileAsShown:(NSInteger)matchId;
-(void)clearCacheData;
-(void)updateOptimizationParams:(NSDictionary*)params;
-(void)cacheMatchesProfileImages;

-(BOOL)isValidProfilePic;
-(BOOL)hasPhotos;
-(BOOL)hasLikedInThePast;

-(BOOL)isNudgeAvailable;
-(NSString*)nudgeKeyForCurrentMatch;

-(TMSystemMessageLinkType)systemMessageLinkTypeForFirstTile;
-(TMSystemMessageLinkType)systemMessageTypeForLastTileLink;
-(BOOL)isFirstTileAvailable;
-(NSString*)getFirstSystemTileTitle;
-(NSString*)getLastTileSystemMessage;
-(NSString*)getLikeActionSystemMessage;
-(BOOL)isLastSystemTileVisible;

-(BOOL)isToastRequiredForFemaleProfileVisibility;
-(void)updateFBAccessToken:(void(^)(BOOL isTokenExpired))responseBlock;

//SeventyNine Ad changes
- (BOOL)canShowInterstitialAd;
- (TMProfile*) getMatchAdProfile;
@end

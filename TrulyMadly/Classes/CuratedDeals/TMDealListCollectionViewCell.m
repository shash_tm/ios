//
//  TMDealListCollectionViewCell.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 28/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDealListCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"
#import "TMProfileInterest.h"
#import "TMProfileImageView.h"

//observations from the specs
//screen width : 710 ; image width : 680
//1) price label margin from right :: 20 --> margin from top :: 10
//2) pricle label is right aligned --> width in snapshot is 65 --> make it maxmimum of 100
//3) introducing a margin of 72 dp in between name label and price label
//4) name label length in snapshot is 400 --> max till left margin
//5) left margin for the name label is 20
//6) bottom margin between the name label and the location label is 20
//7) length of location label in snapshot is 250 --> max to be 80% of the name label
//8) cell height is 380 --> overall screen height is 1020

@interface TMDealListCollectionViewCell ()

@property(nonatomic, strong) TMProfileImageView* profileImageView;
@property(nonatomic, strong) UIImageView* missTMImageView;
@property(nonatomic, strong) UILabel* nameLabel;
@property(nonatomic, strong) UILabel* locationLabel;

//for internal purposes only
@property(nonatomic, assign) CGFloat priceNameLocationAndHashTagScrollViewLabelSideMargin;
@property(nonatomic, assign) CGFloat maxNameLabelWidth;

//@property(nonatomic, strong) UIImageView* spotImageView;
//@property(nonatomic, strong) CAGradientLayer *gradient;

@end

@implementation TMDealListCollectionViewCell

#define PRICE_LABEL_TAG 101
#define NAME_LABEL_TAG 111
#define LOCATION_LABEL_TAG 121
#define SCROLLVIEW_TAG 131
#define HASHTAG_BUTTON_TAG 141
#define IMAGEVIEW_TAG 151
#define PLACEHOLDER_VIEW_TAG 161

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

- (id)initWithFrame:(CGRect)aRect {
    self = [super initWithFrame:aRect];
    if (self) {
        [self initializeUIComponents];
        [self initializeAlignmentAttributes];
    }
    return self;
}


/**
 * Initializes the UI component
 * to be called only once in the cell's lifetime
 */
- (void) initializeUIComponents {
    //initialize all labels and UI properties
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.tag = NAME_LABEL_TAG;
    self.nameLabel.font = IS_IPHONE_6_PLUS?([UIFont boldSystemFontOfSize:22]):((IS_IPHONE_6)?([UIFont boldSystemFontOfSize:21]):(([UIScreen mainScreen].bounds.size.height > 500)?([UIFont boldSystemFontOfSize:20]):([UIFont boldSystemFontOfSize:19])));
    [self addSubview:self.nameLabel];
    
    self.locationLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.numberOfLines = 0;
    self.locationLabel.textColor = [UIColor whiteColor];
    self.locationLabel.tag = LOCATION_LABEL_TAG;
    self.locationLabel.font = [UIFont systemFontOfSize:13];
    self.locationLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.locationLabel];
    
    self.profileImageView = [[TMProfileImageView alloc] initWithFrame:self.bounds];
    self.profileImageView.clipsToBounds = YES;
    [self.profileImageView setImageContentMode:UIViewContentModeScaleAspectFill];
    [self addSubview:self.profileImageView];
    
    self.missTMImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.missTMImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.missTMImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.missTMImageView];
}

/**
 * Initializes the alignment attributes
 * should be called only once in the cell's lifetime
 */
- (void) initializeAlignmentAttributes {
    //initializes all alignment attributes
    self.priceNameLocationAndHashTagScrollViewLabelSideMargin = 8;
    self.maxNameLabelWidth = 0;
}

//custom setter to update the UI
- (void) setDateSpot:(TMDateSpot *)dateSpot {
    _dateSpot = dateSpot;
    
    //update the UI for the new date spot
    [self updateUIComponents];
}


/**
 * Updates the UI according to the new datespot object
 */
- (void) updateUIComponents {
    
    BOOL isMissTMRecommendation = NO;
    
    //update the miss TM Recommendation status
    [self updateMissRecommendation:isMissTMRecommendation];
    
    //add the name label
    [self updateNameLabelWithMissRecommendation:isMissTMRecommendation];
    
    //update the location label
    [self updateLocationLabelWithMissTMRecommendation:isMissTMRecommendation];
    
    //update the background image view
    [self updateBackgroundImageView];
}


/**
 * Updates the miss TM view
 */
- (void) updateMissRecommendation:(BOOL) isMissTMRecommendation {
    if (isMissTMRecommendation) {
        CGFloat missTMImageSide = (9.0/35.0)*self.bounds.size.height;
        self.missTMImageView.frame = CGRectMake(self.bounds.size.width/2 - missTMImageSide/2, (13.0/70.0)*self.bounds.size.height, missTMImageSide, missTMImageSide);
    }
    else {
        self.missTMImageView.frame = CGRectZero;
    }
}


/**
 * Updates the name label UI
 */
- (void) updateNameLabelWithMissRecommendation:(BOOL) isMissTMRecommendation {
    
    if (self.dateSpot.header.combinedName && self.dateSpot.header.combinedName.length) {
        if (isMissTMRecommendation) {
            NSString* combinedNameString = self.dateSpot.header.combinedName;
            
            UIFont* font = self.nameLabel.font;
            
            self.maxNameLabelWidth = self.bounds.size.width*9.5/10;
            
            CGSize nameLabelRectSize = [combinedNameString boundingRectWithConstraintSize:CGSizeMake(self.maxNameLabelWidth, CGFLOAT_MAX) attributeDictionary:@{NSFontAttributeName:font}].size;
            
            CGFloat yCoordinate = (self.missTMImageView.frame.origin.y + self.missTMImageView.frame.size.height + 16 > (12.0/35.0)*self.bounds.size.height)? (self.missTMImageView.frame.origin.y + self.missTMImageView.frame.size.height + 16):((12.0/35.0)*self.bounds.size.height);
            
            self.nameLabel.frame = CGRectMake(self.bounds.size.width/2 - nameLabelRectSize.width/2, yCoordinate, nameLabelRectSize.width, nameLabelRectSize.height);
            self.nameLabel.text = combinedNameString;
        }
        else {
            NSString* combinedNameString = self.dateSpot.header.combinedName;
            
            UIFont* font = self.nameLabel.font;
            
            self.maxNameLabelWidth = self.bounds.size.width*9.5/10;
            
            CGSize nameLabelRectSize = [combinedNameString boundingRectWithConstraintSize:CGSizeMake(self.maxNameLabelWidth, CGFLOAT_MAX) attributeDictionary:@{NSFontAttributeName:font}].size;
            
            CGFloat yCoordinate = (self.missTMImageView.frame.origin.y + self.missTMImageView.frame.size.height + 16> (12.0/35.0)*self.bounds.size.height)? (self.missTMImageView.frame.origin.y + self.missTMImageView.frame.size.height + 16):((12.0/35.0)*self.bounds.size.height);
            
            self.nameLabel.frame = CGRectMake(self.bounds.size.width/2 - nameLabelRectSize.width/2, yCoordinate, nameLabelRectSize.width, nameLabelRectSize.height);
            self.nameLabel.text = combinedNameString;
        }
    }
    else {
        self.nameLabel.text = @"";
    }
}


/**
 * Updates the location label UI
 */
- (void) updateLocationLabelWithMissTMRecommendation:(BOOL) isMissTMRecommendation {
    if (self.dateSpot.header.location && self.dateSpot.header.location.length) {
        if (isMissTMRecommendation) {
            NSString* locationString = self.dateSpot.header.location;
            
            CGFloat maxLocationLabelWidth = (self.maxNameLabelWidth > 0)?((self.maxNameLabelWidth*4)/5):((self.bounds.size.width*4)/5);
            
            CGSize locationLabelRectSize = [locationString boundingRectWithConstraintSize:CGSizeMake(maxLocationLabelWidth, CGFLOAT_MAX) attributeDictionary:@{NSFontAttributeName:self.locationLabel.font}].size;
            
            CGFloat yCoordinate = (self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + 16 > (5.0/7.0)*self.bounds.size.height)? (self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + 16):((5.0/7.0)*self.bounds.size.height);
            
            self.locationLabel.frame = CGRectMake(self.bounds.size.width/2 - locationLabelRectSize.width/2, yCoordinate, locationLabelRectSize.width, locationLabelRectSize.height);
            self.locationLabel.text = locationString;
        }
        else {
            NSString* locationString = self.dateSpot.header.location;
            
            CGFloat maxLocationLabelWidth = (self.maxNameLabelWidth > 0)?((self.maxNameLabelWidth*4)/5):((self.bounds.size.width*4)/5);
            
            CGSize locationLabelRectSize = [locationString boundingRectWithConstraintSize:CGSizeMake(maxLocationLabelWidth, CGFLOAT_MAX) attributeDictionary:@{NSFontAttributeName:self.locationLabel.font}].size;
            
            CGFloat yCoordinate = (self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + 16 > (18.0/35.0)*self.bounds.size.height)? (self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + 16):((18.0/35.0)*self.bounds.size.height);
            
            self.locationLabel.frame = CGRectMake(self.bounds.size.width/2 - locationLabelRectSize.width/2, yCoordinate, locationLabelRectSize.width, locationLabelRectSize.height);
            self.locationLabel.text = locationString;
        }
    }
    else {
        self.locationLabel.text = @"";
    }
}


/**
 * Updates the background Image UI
 */
- (void)updateBackgroundImageView {
    if (self.dateSpot.header.imageURL) {
        NSString* imageURLString = self.dateSpot.header.imageURL;
        if (imageURLString) {
            NSString *lastPathComponent = [imageURLString lastPathComponent];
            self.profileImageView.frame = self.bounds;
            [self.profileImageView setDefaultImage:[UIImage imageNamed:@"deal_image_placeholder"]];
            [self.profileImageView setImageFromURLStringWithBackgroundGradient:imageURLString isDarkCentered:YES];
            [self.profileImageView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
            [self sendSubviewToBack:self.profileImageView];
        }
    }
}


@end

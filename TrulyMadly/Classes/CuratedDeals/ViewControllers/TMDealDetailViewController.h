//
//  TMDealDetailViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

@class TMCuratedDealContext;
#import "TMBaseViewController.h"
#import "TMDealListViewController.h"

@interface TMDealDetailViewController : TMBaseViewController

@property (nonatomic, weak) id <TMDealListViewControllerDelegate> dealDetailViewControllerDelegate;
-(instancetype)initWithDealContext:(TMCuratedDealContext *)dealContext;

@end
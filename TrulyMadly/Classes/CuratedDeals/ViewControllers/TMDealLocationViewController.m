//
//  TMDealLocationViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMDealLocationViewController.h"
#import "TMDealContactCollectionViewCell.h"
#import "TMDateContact.h"


@interface TMDealLocationViewController()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIAlertViewDelegate>

@property(nonatomic, strong)TMDateContact *dateContact;
@property(nonatomic, strong)UICollectionView *collectionView;

@end

@implementation TMDealLocationViewController

-(instancetype)initWithDateContact:(TMDateContact *)dateContactObj headerText:(NSString *)headerText
{
    self = [super init];
    if(self) {
        self.dateContact = dateContactObj;
        self.title = headerText;
        return self;
    }
    return nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:false animated: false];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self configureUIState];
    [self configureCollectionView];
}

-(void)dealloc
{
    //NSLog(@"deal multiple location dealloc called");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configureUIState {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self addLeftBarButton];
}

-(void)addLeftBarButton {
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer , leftButton] animated:true];
    
}

-(void)backButtonPressed {
    self.collectionView = nil;
    [self.navigationController popViewControllerAnimated:true];
}

-(void)configureCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(2.0f, 10.0f, 2.0f, 10.0f);
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,54.0,0.0);
    self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
    
    [self.collectionView registerClass:[TMDealContactCollectionViewCell class] forCellWithReuseIdentifier:@"contacts"];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
}

- (void) viewDidLayoutSubviews {
    //update the frame for the deal list collection view
    self.collectionView.frame = self.view.bounds;
}

#pragma mark - Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dateContact.locationArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMDealContactCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"contacts" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    [cell configureDealContactForLocation:self.dateContact index:indexPath.row];
    cell.delegate = self.cellDelegate;
    return cell;
}

# pragma mark - collection view flow layout delegate methods

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewFlowLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIEdgeInsets contentInset = UIEdgeInsetsMake(18, 0, 18, 0);
    CGFloat height = [self contentHeightForItemAtIndexPath:indexPath layout:collectionViewLayout];
    height = height + contentInset.top;
    height = height + contentInset.bottom;
    
    return CGSizeMake([self cellWidth:collectionViewLayout], ceilf(height));
}

- (CGFloat)contentHeightForItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewFlowLayout *)collectionViewLayout
{
    CGFloat maxWidth = [self cellWidth:collectionViewLayout];
    CGFloat height = 0;
    height = [self.dateContact contactContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth] index:indexPath.row];
    return height;
}

-(CGFloat)cellWidth:(UICollectionViewFlowLayout *)collectionViewLayout
{
    return self.view.bounds.size.width-collectionViewLayout.sectionInset.left-collectionViewLayout.sectionInset.right;
}

-(CGFloat)baseIconWidth:(CGFloat)maxWidth {
    CGFloat iconWidth = (maxWidth - (15*4))/5;
    return iconWidth;
}

@end

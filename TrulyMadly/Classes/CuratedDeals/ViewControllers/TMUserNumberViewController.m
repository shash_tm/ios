//
//  TMUserNumberViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMUserNumberViewController.h"
#import "NSString+TMAdditions.h"
#import "TMCuratedDealsManager.h"
#import "TMErrorMessages.h"
#import "TMAnalytics.h"
#import "TMDataStore+TMAdAddtions.h"

@interface TMUserNumberViewController ()<UITextFieldDelegate, UIAlertViewDelegate>

@property(nonatomic, strong)NSString *phNumber;
@property(nonatomic, strong)UIScrollView *scrollView;
@property(nonatomic, assign)BOOL isNumberAvail;
@property(nonatomic, strong)UITextField *phoneTextField;
@property(nonatomic, strong)UIButton *sendButton;
@property(nonatomic, strong)TMCuratedDealsManager *dealManager;
@property(nonatomic, strong)UIActivityIndicatorView *activityLoader;

@end

@implementation TMUserNumberViewController

#define space_between_two_component 50

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        self.isNumberAvail = false;
        return self;
    }
    return nil;
}

-(instancetype)initWithNumber:(NSString *)number
{
    self = [super init];
    if(self)
    {
        self.isNumberAvail = true;
        self.phNumber = number;
        return self;
    }
    return nil;
}

//-(void)setDateSpotId:(NSString *)dateSpotId
//{
//    self.dateSpotId = dateSpotId;
//}

-(TMCuratedDealsManager*)dealManager {
    if(_dealManager == nil) {
        _dealManager = [[TMCuratedDealsManager alloc] init];
    }
    return _dealManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView* dealListLoaderBackgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    dealListLoaderBackgroundImageView.image = [UIImage imageNamed:@"date_list_loader_background"];
    [self.view addSubview:dealListLoaderBackgroundImageView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.view addGestureRecognizer:tapGesture];
    
    // initialize the UI Component
    [self configureUI];
    
    [self registerNotifcations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [self deRegisterNotifications];
    self.dealManager = nil;
}

-(void)configureUI {
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    
    NSString *text = @"Please share your phone number so that we can send a copy of your voucher!";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
    CGSize constrintSize = CGSizeMake(self.view.bounds.size.width-60, NSUIntegerMax);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    CGFloat height = rect.size.height;
    
    CGFloat yPos = (self.view.bounds.size.height-(height+space_between_two_component+44+space_between_two_component+44))/2;
    CGFloat xPos = (self.view.bounds.size.width-rect.size.width)/2;
    
    UILabel *phLbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
    phLbl.text = text;
    phLbl.font = [UIFont systemFontOfSize:15];
    phLbl.textAlignment = NSTextAlignmentCenter;
    phLbl.textColor = [UIColor whiteColor];
    phLbl.numberOfLines = 0;
    [phLbl sizeToFit];
    [self.scrollView addSubview:phLbl];
    
    xPos = 30;
    
    self.phoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(xPos, yPos+phLbl.frame.size.height+space_between_two_component, self.scrollView.frame.size.width-60, 44)];
    self.phoneTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.phoneTextField.delegate = self;
    self.phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneTextField.font = [UIFont systemFontOfSize:28.0];
    self.phoneTextField.textColor = [UIColor whiteColor];
    [self.phoneTextField addTarget:self action:@selector(didPhoneChanged) forControlEvents:UIControlEventEditingChanged];
    [self.scrollView addSubview:self.phoneTextField];
    
    //phone image view
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(xPos, self.phoneTextField.frame.origin.y+20, 30, 20)];
    self.phoneTextField.leftView = paddingView;
    self.phoneTextField.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *phoneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, self.phoneTextField.frame.origin.y+10, 25, 25)];
    [phoneImageView setImage:[UIImage imageNamed:@"call_without_circle"]];
    [self.scrollView addSubview:phoneImageView];
    
    
    //phoneSeparator
    UIView *phoneSeparator = [[UIView alloc] initWithFrame:CGRectMake(xPos, self.phoneTextField.frame.origin.y+40, self.phoneTextField.frame.size.width, 0.5)];
    phoneSeparator.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:phoneSeparator];
    
    yPos = phoneSeparator.frame.origin.y + phoneSeparator.frame.size.height + space_between_two_component;
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.sendButton.frame = CGRectMake(xPos, yPos, self.scrollView.frame.size.width-60, 44);
    self.sendButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [self.sendButton setTitle:@"Confirm" forState:UIControlStateNormal];
    self.sendButton.layer.cornerRadius = 6;
    [self.sendButton addTarget:self action:@selector(didSendClick) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.sendButton];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, yPos+self.sendButton.frame.size.height+15);
    
    if(self.isNumberAvail)
    {
        self.phoneTextField.text = self.phNumber;
        [self enableButton];
    }else {
        [self disableButton];
    }

}

-(BOOL)prefersStatusBarHidden {
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)tapAction
{
    if(self.phoneTextField) {
        [self.phoneTextField resignFirstResponder];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    if(newLength <= 10) {
        return true;
    }
    return false;
}

-(void)didPhoneChanged
{
    if([self.phoneTextField.text length] == 10){
        self.phNumber = self.phoneTextField.text;
        [self enableButton];
    }
    else {
        [self disableButton];
    }
}

-(void)didSendClick
{
    [self tapAction];
    
    NSString *msg = [NSString stringWithFormat:@"%@ %@%@",@"Receive your voucher code on",self.phNumber,@"?"];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
    [alertView addButtonWithTitle:@"OK"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([buttonTitle isEqualToString:@"OK"]) {
        [self sendNumberToServer];
    }
}

-(void)sendNumberToServer
{
    __weak TMUserNumberViewController *weakSelf = self;

    if([self.dealManager isNetworkReachable])
    {
        NSDictionary *params = @{@"action":@"save_number", @"phone_number":self.phNumber, @"deal_id":self.deal_id};
        
        [self configureViewForDataLoadingStart];
        
        [self.dealManager setUserPhoneNumberForCuratedDeals:params responseBlock:^(BOOL status, TMError *error) {
            
            [weakSelf configureViewForDataLoadingFinish];
            
            if(status)
            {
                [weakSelf setUserNumberSaveKey];
                
                [weakSelf trackEventData];
                
                [weakSelf dismissViewControllerAnimated:true completion:^{
                    [weakSelf.userNumberControllerDelegate didDismissUserNumberController];
                }];
            }
            else {
                [weakSelf showAlertWithTitle:TM_UNKNOWN_MSG msg:@"Problem in updating the number." withDelegate:nil];
            }
        }];
    }
    else {
        [weakSelf showAlertWithTitle:TM_INTERNET_NOTAVAILABLE_MSG msg:@"Problem in updating the number." withDelegate:nil];
    }
}

-(void)configureViewForDataLoadingStart
{
    self.view.userInteractionEnabled = false;
    self.scrollView.userInteractionEnabled = false;
    [self disableButton];
    [self addLoaderForUpdate];
    [self.activityLoader startAnimating];
}

-(void)configureViewForDataLoadingFinish
{
    self.view.userInteractionEnabled = true;
    self.scrollView.userInteractionEnabled = true;
    [self enableButton];
    if(self.activityLoader) {
        [self.activityLoader stopAnimating];
        [self.activityLoader removeFromSuperview];
    }
}

-(void)addLoaderForUpdate
{
    self.activityLoader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityLoader.color = [UIColor colorWithRed:0.463 green:0.463 blue:0.463 alpha:1.0];
    [self.scrollView addSubview:self.activityLoader];
    CGRect frm = self.activityLoader.frame;
    frm.origin.x = self.sendButton.frame.origin.x+80;
    frm.origin.y = self.sendButton.frame.origin.y+(self.sendButton.frame.size.height-self.activityLoader.frame.size.height)/2;
    self.activityLoader.frame = frm;
    [self.scrollView bringSubviewToFront:self.activityLoader];
}

-(void)enableButton {
    self.sendButton.enabled = true;
    [self.sendButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    [self.sendButton setBackgroundColor:[UIColor colorWithRed:1.0 green: 0.702 blue: 0.725 alpha: 1.0]];
}

-(void)disableButton {
    self.sendButton.enabled = false;
    [self.sendButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.sendButton setBackgroundColor:[UIColor colorWithRed:0.902 green: 0.906 blue: 0.91 alpha: 1.0]];
}

-(void)registerNotifcations {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

-(void)deRegisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    CGRect activeButtonRect = self.sendButton.frame;
    CGPoint activeButtonOrigin = activeButtonRect.origin;
    
    if(!CGRectContainsPoint(aRect, activeButtonOrigin)) {
        [self.scrollView scrollRectToVisible:activeButtonRect animated:true];
    }
}

-(void)keyboardDidHide:(NSNotification*)notification {
    UIEdgeInsets contentInset = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInset;
    self.scrollView.scrollIndicatorInsets = contentInset;
}

-(void)trackEventData
{
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMUserNumberViewController" forKey:@"screenName"];
    [eventDict setObject:@"datespots" forKey:@"eventCategory"];
    [eventDict setObject:@"number_saved" forKey:@"eventAction"];
    [eventDict setObject:self.dateSpotId forKey:@"label"];
    [eventDict setObject:self.dateSpotId forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}

-(void)setUserNumberSaveKey
{
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithBool:TRUE] key:@"user_number"];
}


@end

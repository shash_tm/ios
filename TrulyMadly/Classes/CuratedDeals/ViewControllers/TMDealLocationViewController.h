//
//  TMDealLocationViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 11/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"
#import "TMDealContactCollectionViewCell.h"

@class TMDateContact;

@interface TMDealLocationViewController : TMBaseViewController

@property(nonatomic,weak)id<TMDealContactCollectionViewCellDelegate>cellDelegate;
-(instancetype)initWithDateContact:(TMDateContact *)dateContactObj headerText:(NSString *)headerText;

@end

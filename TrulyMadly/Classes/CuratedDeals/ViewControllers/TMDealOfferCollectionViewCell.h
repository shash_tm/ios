//
//  TMDealOfferCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

@class TMDateOffer;
#import <UIKit/UIKit.h>

@protocol TMDealOfferCollectionViewCellDelegate;

@interface TMDealOfferCollectionViewCell : UICollectionViewCell

@property(nonatomic, assign)BOOL isCellConfigured;
@property(nonatomic,weak)id<TMDealOfferCollectionViewCellDelegate>delegate;

-(void)configureDealWithOffer:(TMDateOffer *)dealOffer;

@end

@protocol TMDealOfferCollectionViewCellDelegate <NSObject>

@optional
-(void)didMenuTap:(NSArray *)menuImages currentIndex:(NSUInteger)currentIndex;

@end
//
//  TMDealDetailViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDealDetailViewController.h"
#import "TMCuratedDealsManager.h"
#import "TMSwiftHeader.h"
#import "TMLog.h"
#import "TMDateSpot.h"
#import "TMDateContact.h"
#import "TMProfileCollectionViewCellLike.h"
#import "TMProfilePhotoHeaderView.h"
#import "TMFullScreenPhotoViewController.h"
#import "TMDealTermsNConditionCollectionViewCell.h"
#import "TMDealContactCollectionViewCell.h"
#import "TMDealOfferCollectionViewCell.h"
#import "TMCuratedDealContext.h"
#import "TMDataStore+TMAdAddtions.h"
#import "TMDealMessageContext.h"
#import "TMUserNumberViewController.h"
#import "TMDealLocationViewController.h"


#define VOUCHER_TUTORIAL @"voucher_tutorial"

@interface TMDealDetailViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, TMProfilePhotoHeaderViewDelegate, UIAlertViewDelegate, TMDealContactCollectionViewCellDelegate, TMDealOfferCollectionViewCellDelegate, TMUserNumberViewControllerDelegate>

@property(nonatomic, strong)TMCuratedDealsManager *dealManager;
@property(nonatomic, strong)TMDateSpot *dealDetailObj;
@property(nonatomic, strong)UICollectionView *collectionView;
@property(nonatomic, strong)NSString *dealStatus;
@property(nonatomic, strong)NSString *dealButtonText;
@property(nonatomic, strong)TMCuratedDealContext *dealContext;
@property(nonatomic, strong)UIView *voucherTutorial;
@property(nonatomic, strong)NSString *phoneNumber;

@end

@implementation TMDealDetailViewController

-(instancetype)initWithDealContext:(TMCuratedDealContext *)dealContext
{
    self = [super init];
    if(self) {
        self.dealContext = dealContext;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Datespot";
    [self.navigationController setNavigationBarHidden:false animated: false];
    //[self.navigationItem setHidesBackButton:true];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self configureUIState];
    [self loadDealDetailData];
}

-(void)dealloc
{
    //NSLog(@"deal detail dealloc called");
    self.dealManager = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(TMCuratedDealsManager*)dealManager {
    if(_dealManager == nil) {
        _dealManager = [[TMCuratedDealsManager alloc] init];
    }
    return _dealManager;
}

-(void)configureUIState {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self addLeftBarButton];
}

-(void)addLeftBarButton {
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -12;

    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
   
    [self.navigationItem setLeftBarButtonItems:@[negativeSpacer , leftButton] animated:true];

}

-(void)backButtonPressed {
    self.dealManager = nil;
    self.collectionView = nil;
    if(self.dealContext.fromChat) {
        [self dismissViewControllerAnimated:TRUE completion:nil];
    }
    else {
        [self.navigationController popViewControllerAnimated:true];
    }
}

-(void)loadDealDetailData {
    
    if([self.dealManager isNetworkReachable]) {
        NSDictionary *params = @{@"datespot_id":self.dealContext.dateSpotId, @"match_id":self.dealContext.matchId};
        if(self.dealContext.dealId) {
            params = @{@"datespot_id":self.dealContext.dateSpotId, @"match_id":self.dealContext.matchId, @"deal_id":self.dealContext.dealId};
        }
        
        [self configureViewForDataLoadingStart];
        [self.dealManager getDealsDetailData:params responseBlock:^(TMDateSpot *dealDetailResponse, TMError *error) {
            
            [self configureViewForDataLoadingFinish];
            
            if(dealDetailResponse != nil) {
                self.dealDetailObj = dealDetailResponse;
                self.dealStatus = self.dealDetailObj.dateStatus;
                self.dealButtonText = self.dealDetailObj.dateButtonText;
                [self configureCollectionViewWithDealData];
            }
            else {
                //error case
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:false];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewAtError];
                }
                else {
                    //unknwon error
                    [self showRetryViewAtUnknownError];
                }
            }
        }];
    }
    else {
         [self showRetryViewAtError];
    }
}

-(void)showRetryViewAtUnknownError {
    if(!self.retryView.superview) {
        [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
        [self.retryView.retryButton addTarget:self action:@selector(loadDealDetailData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)showRetryViewAtError {
    if(!self.retryView.superview) {
        [self showRetryView];
        [self.retryView.retryButton addTarget:self action:@selector(loadDealDetailData) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)configureViewForDataLoadingStart {
    
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading..."];
}

-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
    TMLOG(@"configureViewForDataLoadingFinish");
    if(self.retryView.superview) {
        TMLOG(@"removing retry view");
        [self removeRetryView];
    }
}

-(void)configureCollectionViewWithDealData
{
    [self resetCollectionView];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(2.0f, 10.0f, 2.0f, 10.0f);
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height) collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.contentInset=UIEdgeInsetsMake(0.0,0.0,70.0,0.0);
    self.collectionView.scrollIndicatorInsets=UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
    
    [self.collectionView registerClass:[TMDealOfferCollectionViewCell class] forCellWithReuseIdentifier:@"offer"];
    [self.collectionView registerClass:[TMProfileCollectionViewCellLike class] forCellWithReuseIdentifier:[TMProfileCollectionViewCellLike cellReuseIdentifier]];
    [self.collectionView registerClass:[TMDealTermsNConditionCollectionViewCell class] forCellWithReuseIdentifier:@"conditions"];
    [self.collectionView registerClass:[TMDealContactCollectionViewCell class] forCellWithReuseIdentifier:@"contacts"];
    
    [self.collectionView registerClass:[TMProfilePhotoHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
    withReuseIdentifier:[TMProfilePhotoHeaderView headerReuseIdentifier]];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    
    [self addButton];
    
    [self presentUserNumberViewController];
    
}

-(void)addButton {
    
    UIView *actionView = [[UIView alloc] initWithFrame:CGRectMake(30, self.view.frame.size.height-64
                                                                            , self.view.frame.size.width-60, 54)];
    actionView.backgroundColor = [UIColor likeColor];//[UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
    actionView.layer.cornerRadius = 25;
    [self.view addSubview:actionView];

    
    UILabel *buttonText = [[UILabel alloc] initWithFrame:CGRectZero];
    buttonText.textColor = [UIColor whiteColor];
    buttonText.backgroundColor = [UIColor clearColor];
    buttonText.textAlignment = NSTextAlignmentCenter;
    buttonText.font = [UIFont systemFontOfSize:16];
    buttonText.text = self.dealButtonText;
    [actionView addSubview:buttonText];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(200, 44);
    CGRect rect = [self.dealButtonText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    buttonText.frame = CGRectMake((CGRectGetWidth(actionView.frame) - rect.size.width)/2, (CGRectGetHeight(actionView.frame) - rect.size.height)/2, rect.size.width, rect.size.height);
    
    
    if([self.dealStatus isEqualToString:@"lets_go"] || [self.dealStatus isEqualToString:@"ask_him"] || [self.dealStatus isEqualToString: @"awaiting_response"] || [self.dealStatus isEqualToString:@"ask_her"]) {
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectZero];
        icon.backgroundColor = [UIColor clearColor];
        [actionView addSubview:icon];
        
        if([self.dealStatus isEqualToString:@"awaiting_response"]) {
            icon.image = [UIImage imageNamed:@"awaiting_response"];
            actionView.backgroundColor = [UIColor colorWithRed:0.749 green:0.749 blue:0.749 alpha:1.0];
            icon.frame = CGRectMake(34, 14, 15, 18);
            
            [self trackEventData:@"awaiting_response"];

        }else {
            if([self.dealStatus isEqualToString:@"lets_go"]){
                [self trackEventData:@"lets_go_viewed"];
            }
            else if([self.dealStatus isEqualToString:@"ask_him"] || [self.dealStatus isEqualToString:@"ask_her"]){
                [self trackEventData:@"suggest_viewed"];
            }
            icon.image = [UIImage imageNamed:@"ask"];
            icon.frame = CGRectMake(34, 14, 18, 16);
        }
        
        
        CGFloat totalWidth = (CGRectGetWidth(icon.bounds) + 10 +CGRectGetWidth(buttonText.bounds));
        CGFloat xPos = (CGRectGetWidth(actionView.bounds) - totalWidth)/2;
        
        icon.frame = CGRectMake(xPos,
                                (actionView.frame.size.height - icon.frame.size.height)/2,
                                icon.frame.size.width,
                                icon.frame.size.height);
        buttonText.frame = CGRectMake(CGRectGetMaxX(icon.frame)+10,
                                      (CGRectGetHeight(actionView.frame) - CGRectGetHeight(buttonText.frame))/2,
                                      buttonText.frame.size.width,
                                      buttonText.frame.size.height);
        
    }
    
    else if ([self.dealStatus isEqualToString: @"voucher"]) {
        actionView.backgroundColor = [UIColor colorWithRed:0.165 green:0.804 blue:0.722 alpha:1.0];
        if(!self.dealDetailObj.getNumber) {
            [self trackEventData:@"voucher_code_viewed"];
            if(![self didShownVoucherTutorial]) {
                [self showVoucherTutorial];
            }
        }
    }
    
    if([self.dealStatus isEqualToString:@"lets_go"] || [self.dealStatus isEqualToString:@"ask_him"] || [self.dealStatus isEqualToString:@"ask_her"]) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [actionView addGestureRecognizer:tapGesture];
    }
}

-(void)presentUserNumberViewController
{
    if(self.dealDetailObj.getNumber) {
        TMUserNumberViewController *usrNumCon;
        
        if(self.dealDetailObj.userNumber) {
            usrNumCon = [[TMUserNumberViewController alloc] initWithNumber:self.dealDetailObj.userNumber];
        }
        else {
            usrNumCon = [[TMUserNumberViewController alloc] init];
        }
        
        if(usrNumCon) {
            usrNumCon.dateSpotId = self.dealDetailObj.dateSpotId;
            usrNumCon.deal_id = (self.dealContext.dealId) ? self.dealContext.dealId : @"";
            usrNumCon.userNumberControllerDelegate = self;
            [self presentViewController:usrNumCon animated:true completion:nil];
        }
    }
}

-(void)resetCollectionView {
    [self.collectionView removeFromSuperview];
    self.collectionView = nil;
}

#pragma mark - Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.dealDetailObj sectionCount];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dealDetailObj rowCountForSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        // hashtag
        TMProfileCollectionViewCellLike *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMProfileCollectionViewCellLike cellReuseIdentifier] forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        [cell configureDealHashtag:self.dealDetailObj.hashtag];
        return cell;
    }
    else if(indexPath.section == 1) {
        // offer
        TMDealOfferCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"offer" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        [cell configureDealWithOffer:self.dealDetailObj.offer];
        cell.delegate = self;
        return cell;
    }
    else if (indexPath.section == 2) {
        // contacts
        TMDealContactCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"contacts" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        [cell configureDealContact:self.dealDetailObj.contacts];
        cell.delegate = self;
        return cell;
    }
    else if(indexPath.section == 3) {
        // terms and condition
        TMDealTermsNConditionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"conditions" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        [cell configureDealConditions:self.dealDetailObj.conditions];
        return cell;
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        TMProfilePhotoHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                            withReuseIdentifier:[TMProfilePhotoHeaderView headerReuseIdentifier]                                                                            forIndexPath:indexPath];
        headerView.backgroundColor = [UIColor clearColor];
        headerView.delegate = self;
        [headerView configureHeader:self.dealDetailObj.header isDealProfile:true];
        return headerView;
    }
    return nil;
}

# pragma mark - collection view flow layout delegate methods

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewFlowLayout*)collectionViewLayout
                  referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section == 0) {
        return CGSizeMake([self cellWidth:collectionViewLayout], [UIScreen mainScreen].bounds.size.width);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewFlowLayout *)collectionViewLayout
                  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIEdgeInsets contentInset = UIEdgeInsetsMake(18, 0, 13, 0);
    CGFloat height = [self contentHeightForItemAtIndexPath:indexPath layout:collectionViewLayout];
    height = height + contentInset.top;
    height = height + contentInset.bottom;
    
   return CGSizeMake([self cellWidth:collectionViewLayout], ceilf(height));
}

- (CGFloat)contentHeightForItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewFlowLayout *)collectionViewLayout
{
    CGFloat height = 0;
    height = [self.dealDetailObj contentLayoutHeight:indexPath.section row:indexPath.row maxWidth:[self cellWidth:collectionViewLayout]];
    return height;
}

-(CGFloat)cellWidth:(UICollectionViewFlowLayout *)collectionViewLayout
{
    return self.view.bounds.size.width-collectionViewLayout.sectionInset.left-collectionViewLayout.sectionInset.right;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == 0) {
        return UIEdgeInsetsMake(13, 0, 13, 0);
    }
    else if (section == 1) {
        return UIEdgeInsetsMake(0, 0, 15, 0);
    }
    return UIEdgeInsetsMake(0, 0, 11, 0);
}

#pragma mark TMProfilePhotoHeader delegate method

-(void)didTapPhotoHeader:(NSArray *)images withCurrentIndex:(NSInteger)currentIndex
{
    TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] initWithDealProfile:true];
    fullScreenViewCon.currentIndex = currentIndex;
    [self.navigationController pushViewController:fullScreenViewCon animated:YES];
    [fullScreenViewCon loadDateSpotImagesFromArray:images];
}

#pragma mark TMDealLocationViewControllerDelegate Method

-(void)didPhoneClick:(NSString *)phoneNumber
{
    self.phoneNumber = phoneNumber;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:self.phoneNumber message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
    
    [alertView addButtonWithTitle:@"Call"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

-(void)didViewMoreClick
{
    // Push TMLocationViewController
    TMDealLocationViewController *locViewCon = [[TMDealLocationViewController alloc] initWithDateContact:self.dealDetailObj.contacts headerText:self.dealDetailObj.header.name];
    locViewCon.cellDelegate = self;
    [self.navigationController pushViewController:locViewCon animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([buttonTitle isEqualToString:@"Get Voucher"] || [buttonTitle isEqualToString:@"Let\'s Go!"]) {
        TMDealMessageContext *context = [[TMDealMessageContext alloc] init];
        context.datespotId = self.dealDetailObj.dateSpotId;
        context.dealId = self.dealContext.dealId;
        [context setDatespotDisplayName:(self.dealDetailObj.contacts.locationArr.count == 1)?([NSString stringWithFormat:@"%@ %@",self.dealDetailObj.header.name,self.dealDetailObj.header.location.capitalizedString]):(self.dealDetailObj.header.name)];
        context.datespotAddress = self.dealDetailObj.header.location;
        context.messageImageURL = self.dealDetailObj.communicationImage;
        context.datespotFriendlyName = self.dealDetailObj.header.friendlyName;
        
        /////////////
        if([self.dealStatus isEqualToString:@"lets_go"]) {
            context.dealMessageType = MESSAGETTYPE_CD_VOUCHER;
            [self.dealDetailViewControllerDelegate didClickLetsGoForDateSpot:context];
            [self trackEventData:@"lets_go_confirm"];
        }
        else if([self.dealStatus isEqualToString:@"ask_him"] || [self.dealStatus isEqualToString:@"ask_her"]) {
            context.dealMessageType = MESSAGETTYPE_CD_ASK;
            [self.dealDetailViewControllerDelegate didClickForRequestForDateSpot:context];
            [self trackEventData:@"suggest_confirm"];
        }
    }
    else if([buttonTitle isEqualToString:@"Call"]){
        [self trackEventData:@"phone_called"];
        NSString *callString = [NSString stringWithFormat:@"%@%@",@"tel://",self.phoneNumber];
        NSString* webStrURL = [callString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:webStrURL];
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)didMenuTap:(NSArray *)menuImages currentIndex:(NSUInteger)currentIndex
{
    TMFullScreenPhotoViewController *fullScreenViewCon = [[TMFullScreenPhotoViewController alloc] initWithDealMenuAndProfile:true isDealMenu:true];
    fullScreenViewCon.currentIndex = currentIndex;
    [self.navigationController pushViewController:fullScreenViewCon animated:YES];
    [fullScreenViewCon loadImgArrayWithImages:menuImages];
    
    [self trackEventData:@"menu_viewed"];
}

-(void)tapAction
{
    NSString *msg = @"";
    NSString *title = @"";
    if([self.dealStatus isEqualToString:@"lets_go"]) {
        //title = @"Let\'s Go";
        title = @"Get Voucher";
        //msg = @"Are you sure about this datespot?";
        msg = @"Get voucher for this datespot?";
        [self trackEventData:@"lets_go_clicked"];
    }
    else if([self.dealStatus isEqualToString:@"ask_him"]) {
        title = @"Let\'s Go!";
        msg = [NSString stringWithFormat:@"Do you want to meet him at %@%@",self.dealDetailObj.header.name,@"?"];
        [self trackEventData:@"suggest_clicked"];
    }
    else {
        title = @"Let\'s Go!";
        msg = [NSString stringWithFormat:@"Do you want to meet her at %@%@",self.dealDetailObj.header.name,@"?"];
        [self trackEventData:@"suggest_clicked"];
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
    [alertView addButtonWithTitle:title];

    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

// voucher tutorial code

-(void)showVoucherTutorial {

    self.voucherTutorial = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-54)];
    self.voucherTutorial.backgroundColor = [UIColor blackColor];
    self.voucherTutorial.alpha = 0.9;
    [self.view addSubview:self.voucherTutorial];
    [self.view bringSubviewToFront:self.voucherTutorial];
    
    NSString *text = @"Please share this voucher \n code at the venue before \n placing your order.";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:18]};
    CGSize constrintSize = CGSizeMake(self.view.bounds.size.width-60, NSUIntegerMax);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
     UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-20)/2, self.voucherTutorial.frame.size.height-80, 20, 20)];
    circleView.layer.cornerRadius = circleView.frame.size.width/2;
    circleView.backgroundColor = [UIColor whiteColor];
    [self.voucherTutorial addSubview:circleView];
    
    UIView *verView = [[UIView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-3)/2, circleView.frame.origin.y - self.view.bounds.size.height/6 , 3, self.view.bounds.size.height/6)];
    verView.backgroundColor = [UIColor whiteColor];
    [self.voucherTutorial addSubview:verView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-rect.size.width)/2, verView.frame.origin.y - rect.size.height - 15, rect.size.width, rect.size.height)];
    lbl.text = text;
    lbl.font = [UIFont systemFontOfSize:18];
    lbl.textColor = [UIColor whiteColor];
    lbl.numberOfLines = 0;
    lbl.textAlignment = NSTextAlignmentCenter;
    [lbl sizeToFit];
    [self.voucherTutorial addSubview:lbl];
    
    // animate the whole component
    [UIView animateWithDuration:1.0
                          delay: 0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect newFrame = lbl.frame;
                         newFrame.origin.y +=  60;
                         lbl.frame = newFrame;
                         
                         newFrame = verView.frame;
                         newFrame.origin.y +=  60;
                         verView.frame = newFrame;
                         
                         newFrame = circleView.frame;
                         newFrame.origin.y +=  60;
                         circleView.frame = newFrame;
                         
                     }
                     completion:^(BOOL finished){
    
                     }];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tutorialAction)];
    [self.voucherTutorial addGestureRecognizer:tapGesture];
    
    [self setVoucherTutorialKey];
}

-(void)tutorialAction
{
    if(self.voucherTutorial) {
        [self.voucherTutorial removeFromSuperview];
        self.voucherTutorial = nil;
    }
}

-(BOOL)didShownVoucherTutorial
{
    if([TMDataStore containsObjectWithUserIDForKey:VOUCHER_TUTORIAL]) {
        BOOL status = [[TMDataStore retrieveObjectWithUserIDForKey:VOUCHER_TUTORIAL] boolValue];
        return status;
    }
    return false;
}

-(void)setVoucherTutorialKey
{
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithBool:TRUE] key:VOUCHER_TUTORIAL];
}

// track event function
-(void)trackEventData:(NSString*)action
{
    NSString *status = (self.dealContext.dealId) ? self.dealContext.dealId : self.dealDetailObj.dateSpotId;
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMDealDetailViewController" forKey:@"screenName"];
    [eventDict setObject:@"datespots" forKey:@"eventCategory"];
    [eventDict setObject:action forKey:@"eventAction"];
    [eventDict setObject:self.dealDetailObj.dateSpotId forKey:@"label"];
    [eventDict setObject:status forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}

#pragma Mark - TMUserNumberViewControllerDelegate Method

-(void)didDismissUserNumberController
{
    if([self.dealStatus isEqualToString:@"voucher"])
    {
        [self trackEventData:@"voucher_code_viewed"];
        if(![self didShownVoucherTutorial]) {
            [self showVoucherTutorial];
        }
    }
}

@end
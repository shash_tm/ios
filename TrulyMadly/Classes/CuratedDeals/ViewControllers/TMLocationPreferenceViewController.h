//
//  TMLocationPreferenceViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@protocol TMLocationPreferenceViewControllerDelegate <NSObject>

@optional
- (void) didClickForApply:(NSArray*)zoneIdArr;
- (void) didClickForApplyWithGeo:(NSDictionary *)geoData;

@end

@interface TMLocationPreferenceViewController : TMBaseViewController

@property (nonatomic, weak) id <TMLocationPreferenceViewControllerDelegate> locationPreferenceDelegate;

-(instancetype)initWithGeo:(BOOL)geoFlag;
-(void)initWithZoneArray:(NSMutableArray *)zoneArray cityName:(NSString *)cityName;
-(void)defaultState;

@end

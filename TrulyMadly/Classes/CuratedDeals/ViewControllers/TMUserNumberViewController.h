//
//  TMUserNumberViewController.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseViewController.h"

@protocol TMUserNumberViewControllerDelegate <NSObject>

-(void)didDismissUserNumberController;

@end


@interface TMUserNumberViewController : TMBaseViewController

@property (nonatomic, weak) id <TMUserNumberViewControllerDelegate> userNumberControllerDelegate;
@property(nonatomic, strong)NSString *dateSpotId;
@property(nonatomic, strong)NSString *deal_id;

-(instancetype)init;
-(instancetype)initWithNumber:(NSString *)number;

@end

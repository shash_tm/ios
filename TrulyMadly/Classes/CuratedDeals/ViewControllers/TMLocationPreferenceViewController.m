//
//  TMLocationPreferenceViewController.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 08/02/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMLocationPreferenceViewController.h"
#import <CoreLocation/CoreLocation.h>

#define headerHeight 64.0
#define cityHeight   46.0
#define nearByHeight 46.0
#define slidingWidth 60.0
#define selectColor  [UIColor colorWithRed:0.922 green:0.282 blue:0.506 alpha:1]
#define deSelectColor [UIColor colorWithRed:0.498 green:0.498 blue:0.498 alpha:1]

@interface TMLocationPreferenceViewController()<UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIAlertViewDelegate>

@property(nonatomic, assign) BOOL showGeo;
@property(nonatomic, strong) UIImageView *bgImgView;
@property(nonatomic, strong) UITableView *zoneView;
@property(nonatomic, strong) NSMutableArray *zoneArray;
@property(nonatomic, assign) BOOL isDoesNotMatterSelected;
@property(nonatomic, strong) UITableViewCell *zeroCell;
@property(nonatomic, strong) UILabel *cityLabel;
@property(nonatomic, strong) UIImageView *nearImageView;
@property(nonatomic, assign) BOOL isCurrentLocAvail;
@property(nonatomic, strong) NSString *latitude;
@property(nonatomic, strong) NSString *longitude;

@property(nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation TMLocationPreferenceViewController

-(instancetype)initWithGeo:(BOOL)geoFlag
{
    self = [super init];
    if(self){
        self.showGeo = geoFlag;
    }
    return self;
}

-(void)initWithZoneArray:(NSMutableArray *)zoneArray cityName:(NSString *)cityName
{
    if(self.cityLabel){
        self.cityLabel.text = cityName;
    }
    //self.isCurrentLocAvail = false;
    self.zoneArray = [[NSMutableArray alloc] init];
    self.zoneArray = zoneArray;
    [self.zoneView reloadData];
    if(self.zoneView.contentSize.height > self.view.bounds.size.height-(headerHeight+cityHeight)-98){
        self.zoneView.scrollEnabled = true;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    [self addBGImage];
    [self addHeader];
    if(self.showGeo) {
        [self addNearByView];
    }
    [self addCity:@""];
    [self addZonesInTableView];
    [self addContinueButton];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
}

-(void)addBGImage
{
    self.bgImgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.bgImgView.image = [UIImage imageNamed:@"map_background"];
    [self.view addSubview:self.bgImgView];
}

-(void)addHeader
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(slidingWidth, 10, self.view.bounds.size.width-slidingWidth, headerHeight)];
    [self.view addSubview:headerView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, (headerHeight-16)/2, self.view.bounds.size.width-slidingWidth, 16)];
    lbl.text = @"Location Preferences";
    lbl.font = [UIFont systemFontOfSize:16.0];
    [headerView addSubview:lbl];
}

-(void)addNearByView {
    UIView *nearByView = [[UIView alloc] initWithFrame:CGRectMake(slidingWidth, headerHeight, self.view.bounds.size.width-slidingWidth, nearByHeight)];
    nearByView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [self.view addSubview:nearByView];
    
    UILabel *nearByLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, (nearByHeight-18)/2, self.view.bounds.size.width-slidingWidth, 18)];
    nearByLbl.text = @"Nearby";
    nearByLbl.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    nearByLbl.textColor = deSelectColor;
    [nearByView addSubview:nearByLbl];
    
    CGFloat xPos = self.view.bounds.size.width-slidingWidth-20-10;
    CGFloat yPos = (nearByHeight-20)/2;
    
    self.nearImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, 20, 20)];
    self.nearImageView.image = [UIImage imageNamed:@"deselect_loc"];
    [nearByView addSubview:self.nearImageView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkCurrentLocation)];
    [nearByView addGestureRecognizer:tapGesture];

}

-(void)addCity:(NSString *)cityName
{
    CGFloat yPos = headerHeight;
    if(self.showGeo){
        yPos = headerHeight+nearByHeight;
    }
    UIView *cityView = [[UIView alloc] initWithFrame:CGRectMake(slidingWidth, yPos, self.view.bounds.size.width-slidingWidth, cityHeight)];
    //cityView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [self.view addSubview:cityView];
    
    self.cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, (cityHeight-16)/2, self.view.bounds.size.width-slidingWidth, 16)];
    self.cityLabel.text = cityName;
    self.cityLabel.font = [UIFont systemFontOfSize:16.0];
    self.cityLabel.textColor = [UIColor blackColor];
    [cityView addSubview:self.cityLabel];
}

-(void)addZonesInTableView
{
    //self.zoneView = [[UITableView alloc] initWithFrame:CGRectMake(35+slidingWidth, headerHeight+cityHeight, self.view.bounds.size.width-40-slidingWidth, self.view.bounds.size.height-98-(headerHeight+cityHeight)) style:UITableViewStylePlain];
    CGFloat yPos = headerHeight+cityHeight;
    if(self.showGeo){
        yPos = headerHeight+cityHeight+nearByHeight;
    }
    self.zoneView = [[UITableView alloc] initWithFrame:CGRectMake(slidingWidth, yPos, self.view.bounds.size.width-slidingWidth, self.view.bounds.size.height-98-(yPos)) style:UITableViewStylePlain];
    [self.zoneView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"uiText"];
    self.zoneView.tintColor = selectColor;
    self.zoneView.delegate = self;
    self.zoneView.dataSource = self;
    self.zoneView.scrollEnabled = false;
    [self.zoneView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.zoneView];
    self.zoneView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];//[UIColor clearColor];
}

-(void)addContinueButton
{
    UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeSystem];
    continueButton.frame = CGRectMake(30+slidingWidth, self.view.bounds.size.height-78, self.view.bounds.size.width-60-slidingWidth, 44);
    continueButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [continueButton setTitle:@"Apply" forState:UIControlStateNormal];
    continueButton.layer.cornerRadius = 6;
    [continueButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
    [continueButton setBackgroundColor:[UIColor colorWithRed:1.0 green: 0.702 blue: 0.725 alpha: 1.0]];
    [continueButton addTarget:self action:@selector(didApplyClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:continueButton];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    //NSLog(@"deal location pref dealloc called");
    self.locationManager = nil;
    self.latitude = nil;
    self.longitude = nil;
    self.isCurrentLocAvail = false;
}

-(void)defaultState {
    self.isCurrentLocAvail = false;
    self.nearImageView.image = [UIImage imageNamed:@"deselect_loc"];
}

-(void)didApplyClick
{
    if(self.isCurrentLocAvail) {
        if(self.locationPreferenceDelegate) {
            NSDictionary *geoData = @{@"lat":self.latitude,@"lon":self.longitude};
            [self.locationPreferenceDelegate didClickForApplyWithGeo:geoData];
        }
    }else {
        NSMutableArray *zoneIds = [[NSMutableArray alloc] init];
        if(self.locationPreferenceDelegate){
            for (int i=0 ; i<self.zoneArray.count; i++) {
                if([[self.zoneArray[i] objectForKey:@"selected"] boolValue]){
                    [zoneIds addObject:[self.zoneArray[i] objectForKey:@"zone_id"]];
                }
            }
            NSArray *zoneIdArr = [[NSArray alloc] initWithArray:zoneIds];
            [self.locationPreferenceDelegate didClickForApply:zoneIdArr];
        }
    }
}

#pragma mark UITableView DataSource and Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.zoneArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uiText"];
    
    cell.textLabel.text = self.zoneArray[indexPath.row][@"name"];
    if(!self.isCurrentLocAvail && [self.zoneArray[indexPath.row][@"selected"] boolValue])
    {   if(indexPath.row == 0){ self.isDoesNotMatterSelected = true; }
        cell.textLabel.textColor = selectColor;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.textLabel.textColor = deSelectColor;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row == 0){
        self.zeroCell = cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.zoneView cellForRowAtIndexPath:indexPath];
    if(indexPath.row == 0)
    {
        self.isDoesNotMatterSelected = !self.isDoesNotMatterSelected;
        if(self.isDoesNotMatterSelected){
            [self deSelectAllZones];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.textLabel.textColor = selectColor;
            [self toggleNearByOption];
        }
        else {
            cell.textLabel.textColor = deSelectColor;
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self updateDictionaryAtIndex:0 status:self.isDoesNotMatterSelected];
            [self updateDoesNotMatterCellOnDeSelect];
        }
    }
    else {
        BOOL selected = [self.zoneArray[indexPath.row][@"selected"] boolValue];
        
        [self updateDictionaryAtIndex:(int)indexPath.row status:!selected];
        
        if(self.isDoesNotMatterSelected){
            [self deselectZeroCell];
        }
        
        if(selected){
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.textLabel.textColor = deSelectColor;
            [self updateDoesNotMatterCellOnDeSelect];
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.textLabel.textColor = selectColor;
            //[self updateDoesNotMatterCellOnSelect];
            [self toggleNearByOption];
        }
    }
}

-(void)toggleNearByOption {
    if(self.isCurrentLocAvail) {
        self.isCurrentLocAvail = false;
        self.nearImageView.image = [UIImage imageNamed:@"deselect_loc"];
    }
}

-(void)updateDoesNotMatterCellOnDeSelect
{
    BOOL selectZeroCell = true;
    for (int i=1; i<self.zoneArray.count; i++) {
        BOOL selected = [self.zoneArray[i][@"selected"] boolValue];
        if(selected){
            selectZeroCell = false;
        }
    }
    if(selectZeroCell){
        [self selectZeroCell];
    }
}

-(void)updateDoesNotMatterCellOnSelect
{
    BOOL selectZeroCell = true;
    for (int i=1; i<self.zoneArray.count; i++) {
        BOOL selected = [self.zoneArray[i][@"selected"] boolValue];
        if(!selected){
            selectZeroCell = false;
        }
    }
    if(selectZeroCell){
        [self deSelectAllZones];
        [self selectZeroCell];
    }
}


-(void)selectZeroCell
{
    self.isDoesNotMatterSelected = true;
    self.zeroCell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.zeroCell.textLabel.textColor = selectColor;
    [self updateDictionaryAtIndex:0 status:self.isDoesNotMatterSelected];
}

-(void)deselectZeroCell
{
    self.isDoesNotMatterSelected = false;
    self.zeroCell.accessoryType = UITableViewCellAccessoryNone;
    self.zeroCell.textLabel.textColor = deSelectColor;
    [self updateDictionaryAtIndex:0 status:self.isDoesNotMatterSelected];
}

-(void)updateDictionaryAtIndex:(int)index status:(BOOL)status
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.zoneArray[index]];
    dict[@"selected"] = [NSNumber numberWithBool:status];
    self.zoneArray[index] = dict;
}

-(void)deSelectAllZones
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
   
    // deselect visible cells
    NSArray *visibleCell = [self.zoneView visibleCells];
    for (int i=0; i<visibleCell.count; i++) {
        cell = visibleCell[i];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.textColor = deSelectColor;
    }
    // update zone array
    for (int i =0; i<self.zoneArray.count; i++) {
       NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.zoneArray[i]];
        if(i==0){
            dict[@"selected"] = [NSNumber numberWithBool:true];
        }else{
            dict[@"selected"] = [NSNumber numberWithBool:false];
        }
        self.zoneArray[i] = dict;
    }
}

-(void)checkCurrentLocation {
    if(!self.isCurrentLocAvail) {
        // determine whether location services are available
        if([CLLocationManager locationServicesEnabled]) {
            //NSLog(@"location services enabled");
            //NSLog(@"authorize app status %d",[CLLocationManager authorizationStatus]);
            if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
                // show alert to redirect to settings and location app
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"LOCATION SERVICES DISABLED" message:@"Please turn on location services in\nSettings->Privacy->Location Services." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
                    alertView.tag = 110391;
                    [alertView show];
                });
            }else{
                if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
                {
                    [self.locationManager requestWhenInUseAuthorization];
                }
                [self.locationManager startUpdatingLocation];
            }
        }else {
            //NSLog(@"location services not enabled");
            // show alert to redirect to settings
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"LOCATION SERVICES DISABLED" message:@"Please turn on location services in\nSettings->Privacy->Location Services." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
                alertView.tag = 110390;
                [alertView show];
            });
        }
    }else{
        // does not matter use case
        self.isCurrentLocAvail = false;
        self.isDoesNotMatterSelected = true;
        [self deSelectAllZones];
        [self selectZeroCell];
        self.nearImageView.image = [UIImage imageNamed:@"deselect_loc"];
        [self didApplyClick];
    }
}

#pragma mark - AlertView delegate method

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 110390) {
        // open the settings
        if(buttonIndex == 1) {
            // ios version > 8
            if(&UIApplicationOpenSettingsURLString != nil) {
                [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }
    }
    else if(alertView.tag == 110391) {
        // open teh app in settings
        // ios version > 8
        if(buttonIndex == 1) {
            if(&UIApplicationOpenSettingsURLString != nil) {
                [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }
    }
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
    });
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *currentLocation = [locations objectAtIndex:0];
    if(currentLocation != nil) {
        self.latitude = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];
        self.longitude = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];
        //NSLog(@"new location longitude: %.8f", currentLocation.coordinate.longitude);
        //NSLog(@"new location lattitude: %.8f", currentLocation.coordinate.latitude);
        [self.locationManager stopUpdatingLocation];
        self.isCurrentLocAvail = true;
        self.isDoesNotMatterSelected = false;
        [self deSelectAllZones];
        self.nearImageView.image = [UIImage imageNamed:@"select_loc"];
        [self didApplyClick];
    }
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    NSLog(@"didUpdateToLocation: %@", newLocation);
//    CLLocation *currentLocation = newLocation;
//    
//    if(currentLocation != nil) {
//        self.latitude = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];
//        self.longitude = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];
//        NSLog(@"new location longitude: %.8f", currentLocation.coordinate.longitude);
//        NSLog(@"new location lattitude: %.8f", currentLocation.coordinate.latitude);
//        [self.locationManager stopUpdatingLocation];
//        self.isCurrentLocAvail = true;
//        self.isDoesNotMatterSelected = false;
//        [self deSelectAllZones];
//        self.nearImageView.image = [UIImage imageNamed:@"select_loc"];
//    }
//    
//}

@end

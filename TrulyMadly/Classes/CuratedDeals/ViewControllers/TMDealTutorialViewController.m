//
//  TMDealTutorialViewController.m
//  TrulyMadly
//
//  Created by Ankit on 15/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMDealTutorialViewController.h"
#import "TMAnalytics.h"

@interface TMDealTutorialViewController ()

@property(nonatomic,strong)UIImageView *bgImageView;

@end

@implementation TMDealTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.bgImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    UIImage *image = [UIImage imageNamed:@"dealtutorial@2x.jpg"];
    self.bgImageView.image = image;
    [self.view addSubview:self.bgImageView];
    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
//    [self.view addGestureRecognizer:tapGesture];
    
    //
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(CGRectGetWidth(self.view.frame)-40, 20, 34, 34);
    [button setImage:[UIImage imageNamed:@"Cancel"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    [self trackTutorial];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tapAction {
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}
-(void)buttonAction {
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

-(void)trackTutorial {
    NSMutableDictionary* eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMDealTutorialViewController" forKey:@"screenName"];
    [eventDict setObject:@"datespots" forKey:@"eventCategory"];
    [eventDict setObject:@"tutorial" forKey:@"eventAction"];
    [eventDict setObject:@"tutorial_viewed" forKey:@"label"];
    [eventDict setObject:@"tutorial_viewed" forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
}
@end

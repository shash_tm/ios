//
//  TMDealContactCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDealContactCollectionViewCell.h"
#import "TMDateContact.h"
#import "NSString+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"

@interface TMDealContactCollectionViewCell()

@property(nonatomic, strong)TMDateContact *dateContact;
@property(nonatomic, strong)UIImageView *callImgView;
@property(nonatomic, strong)UIImageView *addressImgView;
@property(nonatomic, strong)UIButton *callButton;
@property(nonatomic, strong)UILabel *callLabel;
@property(nonatomic, strong)UILabel *addressLabel;
@property(nonatomic, strong)UIImageView *seperatorImgView;
@property(nonatomic, assign)NSInteger index;
@property(nonatomic, strong)UIButton *viewMoreButton;

@end

@implementation TMDealContactCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self addChildrenViews];
    }
    return self;
}

-(void)addChildrenViews
{
    self.seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,1.0)];
    self.seperatorImgView.backgroundColor = [UIColor separatorColor];
    [self.contentView addSubview:self.seperatorImgView];
    
    self.callImgView  = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.callImgView.image = [UIImage imageNamed:@"call"];
    [self.contentView addSubview:self.callImgView];
    
    self.callButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.callButton.frame = CGRectZero;
    [self.callButton addTarget:self action:@selector(didPhoneClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.callButton];
    
    self.callLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.callLabel.textColor = [UIColor blackColor];
    self.callLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:self.callLabel];
    
    self.addressImgView  = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.addressImgView.image = [UIImage imageNamed:@"Location_icon"];
    [self.contentView addSubview:self.addressImgView];
    
    self.addressLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.addressLabel.textColor = [UIColor blackColor];
    self.addressLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:self.addressLabel];
    
}

-(void)configureDealContactForLocation:(TMDateContact *)dealContact index:(NSInteger)index
{
    self.dateContact = dealContact;
    self.index = index;

    if(index == 0){
        self.seperatorImgView.frame = CGRectZero;
    }else {
        self.seperatorImgView.frame = CGRectMake(0, 0,self.frame.size.width,2);
    }
    
    if([dealContact isPhoneAvailable:index]){
        [self configurePhoneNumber];
    }
    if([dealContact isAddressAvailable:index]){
        [self configureAddress];
    }
}

-(void)configurePhoneNumber
{
    CGFloat xPos = 0;
    CGFloat yPos = 22;

    self.callImgView.frame = CGRectMake(0, yPos, [self.dateContact iconWidth], [self.dateContact iconWidth]);
    
    xPos = [self.dateContact iconWidth]+self.dateContact.spaceBetweenIconAndText;
    
    NSString *phoneNumber = self.dateContact.locationArr[self.index][@"phone_number"];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGSize constrintSize = CGSizeMake(self.frame.size.width-xPos-10, NSUIntegerMax);
    CGRect rect = [phoneNumber boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    self.callButton.frame = CGRectMake(0, yPos + fabs((self.callImgView.frame.size.height-30)/2), xPos + rect.size.width + 50, 30);
    
    self.callLabel.frame = CGRectMake(xPos, yPos + fabs((self.callImgView.frame.size.height-rect.size.height)/2), rect.size.width, rect.size.height);
    self.callLabel.text = phoneNumber;
    self.callLabel.numberOfLines = 0;
    [self.callLabel sizeToFit];
}

-(void)configureAddress
{
    CGFloat xPos = 0;
    CGFloat yPos = 22;
    
    yPos = yPos + self.callImgView.frame.size.height + self.dateContact.spaceBetweenTwoSection;
    xPos = [self.dateContact iconWidth]+self.dateContact.spaceBetweenIconAndText;
    
    self.addressImgView.frame = CGRectMake(0, yPos,[self.dateContact iconWidth], [self.dateContact iconWidth]);
    
    NSString *address = self.dateContact.locationArr[self.index][@"address"];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGSize constrintSize = CGSizeMake(self.frame.size.width-xPos, NSUIntegerMax);
    CGRect rect = [address boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    if(rect.size.height <= [self.dateContact iconWidth]) {
        yPos = yPos + ([self.dateContact iconWidth]-rect.size.height)/2;
    }
    
    self.addressLabel.frame = CGRectMake(xPos, yPos, rect.size.width, rect.size.height);
    self.addressLabel.text = address;
    self.addressLabel.numberOfLines = 0;
    [self.addressLabel sizeToFit];
}

-(void)configureDealContact:(TMDateContact *)dealContact {
    
    self.index = 0;
    self.dateContact = dealContact;
    CGFloat yPos = 22;
    CGFloat xPos = 0;
        
    if(dealContact.isMultipleLocationAvail) {
        
        self.addressImgView.frame = CGRectMake(0, yPos,[self.dateContact iconWidth], [self.dateContact iconWidth]);
        
        xPos = [self.dateContact iconWidth]+self.dateContact.spaceBetweenIconAndText;
        NSString *text = [self.dateContact getMultipleLocationText];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(self.frame.size.width-xPos, NSUIntegerMax);
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            
        if(rect.size.height <= [dealContact iconWidth]) {
            yPos = yPos + ([dealContact iconWidth]-rect.size.height)/2;
        }
        
        self.addressLabel.frame = CGRectMake(xPos, yPos, rect.size.width, rect.size.height);
        self.addressLabel.text = text;
        self.addressLabel.numberOfLines = 0;
        [self.addressLabel sizeToFit];
        
        if(!self.viewMoreButton){
            self.viewMoreButton = [UIButton buttonWithType:UIButtonTypeSystem];
            self.viewMoreButton.frame = CGRectMake(xPos, yPos+rect.size.height+10, 60, 30);
            [self.viewMoreButton setTitle:@"View all" forState:UIControlStateNormal];
            [self.viewMoreButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            self.viewMoreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [self.viewMoreButton addTarget:self action:@selector(didViewMoreClick) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:self.viewMoreButton];
        }
    }
    else {
        if([self.dateContact isPhoneAvailable:0]){
            [self configurePhoneNumber];
        }
        if([self.dateContact isAddressAvailable:0]){
            [self configureAddress];
        }
    }
}

-(void)didPhoneClick
{
    if(self.delegate){
        NSString *phoneNumber = self.dateContact.locationArr[self.index][@"phone_number"];
        [self.delegate didPhoneClick:phoneNumber];
    }
}

-(void)didViewMoreClick
{
    if(self.delegate){
        [self.delegate didViewMoreClick];
    }
}

@end

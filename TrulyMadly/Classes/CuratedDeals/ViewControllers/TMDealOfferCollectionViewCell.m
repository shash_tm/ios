//
//  TMDealOfferCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDealOfferCollectionViewCell.h"
#import "TMDateOffer.h"
#import "NSString+TMAdditions.h"
#import "TMProfileImageView.h"
#import "UIColor+TMColorAdditions.h"

@interface TMDealOfferCollectionViewCell()

@property(nonatomic, strong)TMDateOffer *dealOffer;
@property(nonatomic, strong)NSArray *menuImages;

@end

@implementation TMDealOfferCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        
    }
    return self;
}

-(void)configureDealWithOffer:(TMDateOffer *)dealOffer
{
    if(!self.isCellConfigured)
    {
        self.isCellConfigured = true;
        CGFloat xPos = 0;
        CGFloat yPos = 22;
    
        CGFloat offerYPos = 0;
    
        UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                  0,
                                                                                  self.frame.size.width,
                                                                                  1.0)];
        seperatorImgView.backgroundColor = [UIColor separatorColor];
        [self.contentView addSubview:seperatorImgView];
    
    
        UIImageView *offerImgView  = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos,[dealOffer iconWidth], [dealOffer iconWidth])];
        offerImgView.image = [UIImage imageNamed:@"datelicious_icon"];
        [self.contentView addSubview:offerImgView];
    
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
        CGSize constrintSize = CGSizeMake(self.frame.size.width, NSUIntegerMax);
        CGRect rect = [[dealOffer offerAndRecommendationHeaderText] boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
        xPos = [dealOffer iconWidth]+dealOffer.spaceBetweenIconAndText;
    
//    if(!dealOffer.is_inclusive && rect.size.height < [dealOffer iconWidth]) {
//        yPos = yPos + ([dealOffer iconWidth] - rect.size.height)/2;
//    }
    
        UILabel *headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
        headerLbl.text = [dealOffer offerAndRecommendationHeaderText];
        headerLbl.font = [UIFont systemFontOfSize:15];
        headerLbl.textColor = [UIColor blackColor];
        [self.contentView addSubview:headerLbl];
    
        offerYPos = headerLbl.frame.origin.y+ headerLbl.frame.size.height + 5;

        if(dealOffer.isPriceAvailable) {
        
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
            CGSize constrintSize = CGSizeMake(self.frame.size.width/2, NSUIntegerMax);
            CGRect rect = [dealOffer.amount boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
            CGFloat xPos = self.frame.size.width-rect.size.width-5;
            CGFloat yPos = offerImgView.frame.origin.y;
        
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
            lbl.text = dealOffer.amount;
            lbl.font = [UIFont systemFontOfSize:15];
            lbl.textColor = [UIColor blackColor];
            [self.contentView addSubview:lbl];
            
            // rupee image
            xPos = xPos-5-(rect.size.height/2);
            UIImageView *rupeeImgView  = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos+3.5, rect.size.height/2, rect.size.height-5)];
            rupeeImgView.image = [UIImage imageNamed:@"rupee_black"];
            [self.contentView addSubview:rupeeImgView];
    
        }
        if(dealOffer.isPercentageAvailable) {
            NSString *text = [dealOffer.amount stringByAppendingString:@" %"];
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
            CGSize constrintSize = CGSizeMake(self.frame.size.width/2, NSUIntegerMax);
            CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
            CGFloat xPos = self.frame.size.width-rect.size.width-5;
            CGFloat yPos = offerImgView.frame.origin.y;
        
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
            lbl.text = text;
            lbl.font = [UIFont systemFontOfSize:15];
            lbl.textColor = [UIColor blackColor];
            [self.contentView addSubview:lbl];
        }
    
//    if(dealOffer.is_inclusive) {
//        CGFloat xPos = headerLbl.frame.origin.x;
//        CGFloat yPos = headerLbl.frame.origin.y+ headerLbl.frame.size.height;
//        
//        attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:11]};
//        rect = [dealOffer.is_inclusive boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
//        
//        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
//        lbl.text = dealOffer.is_inclusive;
//        lbl.font = [UIFont systemFontOfSize:11];
//        lbl.textColor = [UIColor blackColor];
//        [self.contentView addSubview:lbl];
//        
//        offerYPos = yPos+ lbl.frame.size.height + 5;
//    }
//    
//    if (dealOffer.is_per_couple) {
//        
//        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:11]};
//        CGSize constrintSize = CGSizeMake(self.frame.size.width/2, NSUIntegerMax);
//        CGRect rect = [dealOffer.is_per_couple boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
//        
//        CGFloat xPos = self.frame.size.width-rect.size.width-5;
//        CGFloat yPos = headerLbl.frame.origin.y+ headerLbl.frame.size.height;
//
//        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, rect.size.width, rect.size.height)];
//        lbl.text = dealOffer.is_per_couple;
//        lbl.font = [UIFont systemFontOfSize:11];
//        lbl.textColor = [UIColor blackColor];
//        [self.contentView addSubview:lbl];
//
//        offerYPos = yPos+ lbl.frame.size.height + 5;
//    }
    
        //offer
        xPos = headerLbl.frame.origin.x;

        attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        constrintSize = CGSizeMake(self.frame.size.width-xPos, NSUIntegerMax);
        rect = [dealOffer.offer boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
        UILabel *offerLbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, offerYPos, rect.size.width, rect.size.height)];
        offerLbl.text = dealOffer.offer;
        offerLbl.font = [UIFont systemFontOfSize:14];
        offerLbl.textColor = [UIColor colorWithRed:0.498 green:0.498 blue:0.498 alpha:1];//[UIColor lightGrayColor];
        offerLbl.numberOfLines = 0;
        [offerLbl sizeToFit];
        [self.contentView addSubview:offerLbl];
        
        offerYPos = offerYPos + offerLbl.frame.size.height + dealOffer.spaceBetweenTwoSameSection;
    
    // menu
        if(dealOffer.isMenuAvailable) {
            
            xPos = headerLbl.frame.origin.x;
            // show menu
            self.menuImages = dealOffer.menuImages;
        
            for (int i = 0; i<dealOffer.menuImages.count; i++) {
                if(xPos+dealOffer.menuHeight <= self.frame.size.width) {
                    TMProfileImageView *imgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                                               offerYPos,
                                                                                               dealOffer.menuHeight,
                                                                                               dealOffer.menuHeight+10)];
                    
                    [imgView setImageContentMode:UIViewContentModeScaleAspectFill];
                    imgView.scaleImage = true;
                    [imgView setImageFromURLString:[dealOffer.menuImages objectAtIndex:i] isDeal:false];
                    [self.contentView addSubview:imgView];
                
                    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeSystem];
                    menuButton.frame = imgView.frame;
                    menuButton.tag = i;
                    [menuButton addTarget:self action:@selector(didMenuClick:) forControlEvents:UIControlEventTouchUpInside];
                    [self.contentView addSubview:menuButton];
                
                    xPos = xPos + dealOffer.menuHeight + 10;
                }
            }
            offerYPos = offerYPos + dealOffer.menuHeight + dealOffer.spaceBetweenOfferAndRecommendationSection;
        }else {
            offerYPos = offerYPos;
        }
    
        // recommendation section
        if(dealOffer.isRecommendationAvailable) {
            xPos = 0;
            offerYPos = offerYPos + 12;
            UIImageView *recImgView  = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, offerYPos,[dealOffer iconWidth], [dealOffer iconWidth])];
            recImgView.image = [UIImage imageNamed:@"miss_tm_deal"];
            [self.contentView addSubview:recImgView];
    
            xPos = headerLbl.frame.origin.x;
    
            attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
            constrintSize = CGSizeMake(self.frame.size.width-xPos-10, NSUIntegerMax);
            rect = [dealOffer.recommendation boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
            if(rect.size.height <= [dealOffer iconWidth]) {
                offerYPos = offerYPos+([dealOffer iconWidth]-rect.size.height)/2;
            }
        
            UILabel *recLbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos, offerYPos, rect.size.width, rect.size.height)];
            recLbl.text = dealOffer.recommendation;
            recLbl.font = [UIFont systemFontOfSize:15];
            recLbl.textColor = [UIColor blackColor];
            recLbl.numberOfLines = 0;
            [recLbl sizeToFit];
            [self.contentView addSubview:recLbl];
        }
    }
}

-(void)didMenuClick:(UIButton *)button
{
    if(self.delegate) {
        [self.delegate didMenuTap:self.menuImages currentIndex:button.tag];
    }
}

@end

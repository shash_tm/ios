//
//  TMDealContactCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

@class TMDateContact;
#import <UIKit/UIKit.h>

@protocol TMDealContactCollectionViewCellDelegate;

@interface TMDealContactCollectionViewCell : UICollectionViewCell

@property(nonatomic,weak)id<TMDealContactCollectionViewCellDelegate>delegate;

-(void)configureDealContact:(TMDateContact *)dealContact;
-(void)configureDealContactForLocation:(TMDateContact *)dealContact index:(NSInteger)index;

@end

@protocol TMDealContactCollectionViewCellDelegate <NSObject>

@optional
-(void)didPhoneClick:(NSString *)phoneNumber;
-(void)didViewMoreClick;

@end

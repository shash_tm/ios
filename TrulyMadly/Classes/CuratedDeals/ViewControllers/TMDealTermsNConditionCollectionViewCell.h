//
//  TMDealTermsNConditionCollectionViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMDateCondition;
@interface TMDealTermsNConditionCollectionViewCell : UICollectionViewCell

@property(nonatomic, assign)BOOL isCellConfigured;
@property(nonatomic, strong)UILabel *headerTitle;

-(void)configureDealConditions:(TMDateCondition *)dealCondition;

@end

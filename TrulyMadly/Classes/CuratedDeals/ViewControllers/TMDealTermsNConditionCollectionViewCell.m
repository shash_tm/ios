//
//  TMDealTermsNConditionCollectionViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 25/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDealTermsNConditionCollectionViewCell.h"
#import "TMDateCondition.h"
#import "NSString+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"

@interface TMDealTermsNConditionCollectionViewCell()

@property(nonatomic, strong)TMDateCondition *dateCondition;

@end

@implementation TMDealTermsNConditionCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}

-(void)addHeader
{
    CGRect frame = self.frame;
    CGFloat yPos = 22.0;
    
    UIImageView *seperatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                  0,
                                                                                  frame.size.width,
                                                                                  1.0)];
    seperatorImgView.backgroundColor = [UIColor separatorColor];
    [self.contentView addSubview:seperatorImgView];
    
    UIImageView *imgIconView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                        yPos,
                                                                        [self.dateCondition getIconWidth],
                                                                        [self.dateCondition getIconWidth])];
    imgIconView.image = [UIImage imageNamed:@"deal_tc"];
    [self.contentView addSubview:imgIconView];

    self.headerTitle = [[UILabel alloc] initWithFrame:CGRectMake([self.dateCondition getIconWidth]+self.dateCondition.spaceBetweenIconAndText,
                                                                 yPos,
                                                                 frame.size.width,
                                                                 20)];
    self.headerTitle.font = [UIFont systemFontOfSize:15];
    self.headerTitle.textColor = [UIColor blackColor];
    self.headerTitle.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.headerTitle];
    
    NSString *text = [self.dateCondition getTermsHeaderText];
    self.headerTitle.text = text;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 20);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGRect rext = self.headerTitle.frame;
    rext.size.width = rect.size.width;
    rext.origin.y = imgIconView.frame.origin.y;//+(imgIconView.frame.size.height-rect.size.height)/2;
    self.headerTitle.frame = rext;
}

-(void)configureDealConditions:(TMDateCondition *)dealCondition {
    if(!self.isCellConfigured){
        self.isCellConfigured = true;
        self.dateCondition = dealCondition;
        [self addHeader];
        
        CGRect frame = self.frame;
        CGFloat xPos = [self.dateCondition getIconWidth]+self.dateCondition.spaceBetweenIconAndText;
        CGFloat yPos = self.headerTitle.frame.origin.y+self.headerTitle.frame.size.height+5;
        for (int i=0; i<self.dateCondition.termsNCondition.count; i++) {
            
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
            CGSize constrintSize = CGSizeMake(frame.size.width-xPos-10, NSUIntegerMax);
            CGRect rect = [self.dateCondition.termsNCondition[i] boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
            
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xPos+15, yPos, rect.size.width, rect.size.height)];
            lbl.text = self.dateCondition.termsNCondition[i];
            lbl.textColor = [UIColor colorWithRed:0.498 green:0.498 blue:0.498 alpha:1];//[UIColor lightGrayColor];
            lbl.font = [UIFont systemFontOfSize:14];
            lbl.numberOfLines = 0;
            [lbl sizeToFit];
            [self.contentView addSubview:lbl];
            
            UIImageView *boldImgView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos+7, 5, 5)];
            boldImgView.image = [UIImage imageNamed:@"bullets"];
            [self.contentView addSubview:boldImgView];
            
            yPos = yPos+rect.size.height+self.dateCondition.spaceBetweenTwoConditions;
        }
    }
}

@end

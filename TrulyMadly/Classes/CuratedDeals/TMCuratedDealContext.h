//
//  TMCuratedDealContext.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCuratedDealContext : NSObject

@property(nonatomic ,strong)NSString *dateSpotId;
@property(nonatomic ,strong)NSString *dealId;
@property(nonatomic ,strong)NSString *matchId;
@property(nonatomic ,assign)BOOL fromChat;

@end

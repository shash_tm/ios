//
//  TMCuratedDealsManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;
@class TMDateSpot;

typedef void (^DealDetailBlock)(TMDateSpot *dealDetailResponse, TMError *error);
typedef void (^DealListBlock)(NSDictionary* dealListDict, TMError *error);
typedef void (^UserPhoneBlock)(BOOL status, TMError *error);

@interface TMCuratedDealsManager : TMBaseManager

-(void)getDealsDetailData:(NSDictionary *)queryString responseBlock:(DealDetailBlock)responseBlock;

-(void)getDealsListWithResponseBlock:(NSDictionary *)queryString responseBlock:(DealListBlock) responseBlock;

-(void)setUserPhoneNumberForCuratedDeals:(NSDictionary *)queryString responseBlock:(UserPhoneBlock)responseBlock;

@end

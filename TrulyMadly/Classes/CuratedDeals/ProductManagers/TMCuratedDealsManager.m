//
//  TMCuratedDealsManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 24/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMCuratedDealsManager.h"
#import "TrulyMadly-Swift.h"
#import "TMLog.h"
#import "TMDateSpot.h"

@implementation TMCuratedDealsManager

#define KEY_DATES_LIST @"dates"
#define KEY_ZONES_LIST @"zones"

-(void)getDealsDetailData:(NSDictionary *)queryString responseBlock:(DealDetailBlock)responseBlock
{
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_DEAL_DETAIL_PATH];

    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                id data = [response data];
                if(![data isKindOfClass:[NSNull class]]) {
                    TMDateSpot *dealDetailResponse = [[TMDateSpot alloc] initWithDateData:[response response]];
                    responseBlock(dealDetailResponse, nil);
                }else {
                    responseBlock(nil, nil);
                }
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                
                responseBlock(nil,tmError);
            }
        }
        else {
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            
            responseBlock(nil,tmError);
        }
    }];

}

-(void)getDealsListWithResponseBlock:(NSDictionary *)queryString responseBlock:(DealListBlock) responseBlock
{
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_DEAL_LIST_API];
    
    [request setPostRequestParameters:queryString];

    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
            
                NSDictionary *dateListDictionary = [response responseDictionary];
                
                NSMutableArray* dateSpotArray = [[NSMutableArray alloc] init];
                
                //parse the date list response to get the profile info
                if (dateListDictionary && [dateListDictionary isKindOfClass:[NSDictionary class]]) {
                    NSArray* dates = ([dateListDictionary valueForKey:KEY_DATES_LIST] && [[dateListDictionary valueForKey:KEY_DATES_LIST] isKindOfClass:[NSArray class]])?[dateListDictionary valueForKey:KEY_DATES_LIST]:nil;
                    for (int index = 0; index < dates.count; index++) {
                        if ([[dates objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                            NSDictionary* dateInfoDictionary = [dates objectAtIndex:index];
                             TMDateSpot *dateSpot = [[TMDateSpot alloc] initWithDateListData:dateInfoDictionary];
                            [dateSpotArray addObject:dateSpot];
                        }
                    }
                }
                
                NSArray *zoneList = ([dateListDictionary valueForKey:KEY_ZONES_LIST] && [[dateListDictionary valueForKey:KEY_ZONES_LIST] isKindOfClass:[NSArray class]])?[dateListDictionary valueForKey:KEY_ZONES_LIST]:[[NSArray alloc] init];
                
                NSString *cityName = ([dateListDictionary valueForKey:@"city_name"] && [[dateListDictionary valueForKey:@"city_name"] isKindOfClass:[NSString class]])?[dateListDictionary valueForKey:@"city_name"]:@"";
                
                NSDictionary* responseDict = [[NSDictionary alloc] initWithObjectsAndKeys:dateSpotArray,@"dealList",zoneList,@"zoneList",cityName,@"cityName", nil];
                //responseBlock(dateSpotArray, nil);
                responseBlock(responseDict,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                
                responseBlock(nil,tmError);
            }
        }
        else {
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            
            responseBlock(nil,tmError);
        }
    }];
}

-(void)setUserPhoneNumberForCuratedDeals:(NSDictionary *)queryString responseBlock:(UserPhoneBlock)responseBlock
{
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_DEAL_LIST_API];
    
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                responseBlock(true,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                responseBlock(false,tmError);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = (NSString*)[response error];
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                responseBlock(false,tmError);
            }
        }
        else {
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            responseBlock(false,tmError);
        }
    }];

}

@end

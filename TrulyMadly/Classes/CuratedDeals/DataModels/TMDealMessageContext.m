//
//  TMDealMessageContext.m
//  TrulyMadly
//
//  Created by Ankit on 04/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMDealMessageContext.h"
#import "TMUserSession.h"

@interface TMDealMessageContext()

@property(nonatomic,strong)NSString* messageString;
@property(nonatomic,strong)NSString* datespotDisplayName;

@end


@implementation TMDealMessageContext

- (void) setDatespotDisplayName:(NSString *)datespotDisplayName {
    _datespotDisplayName = datespotDisplayName;
}
-(void)setDealMessageType:(TMMessageType)dealMessageType {
    NSString *text;
    if(dealMessageType == MESSAGETTYPE_CD_ASK) {
        text = [NSString stringWithFormat:@"Hey! Want to meet at %@?",self.datespotDisplayName];
  }
  else {
      text = [NSString stringWithFormat:@"%@ %@%@",@"Yay! Looks like you both have agreed to meet at",self.datespotDisplayName,@". Enjoy!"];
    }
    self.messageText = text;
}

-(NSDictionary*)insertQueryParams {
    NSString *userId = [TMUserSession sharedInstance].user.userId;
    NSString *imageURL = [NSString stringWithFormat:@"%@",self.messageImageURL];
    NSDictionary *params = @{@"userid":userId,@"datespotid":self.datespotId,@"address":self.datespotAddress,@"imageurl":imageURL};
    return params;
}
-(NSDictionary*)updateQueryParams {
    NSDictionary *params = @{@"address":self.datespotAddress,@"imageurl":self.messageImageURL,@"datespotid":self.datespotId};
    return params;
}
@end

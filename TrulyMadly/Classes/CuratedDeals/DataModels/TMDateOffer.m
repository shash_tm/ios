//
//  TMDateOffer.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDateOffer.h"
#import "NSObject+TMAdditions.h"
#import "NSString+TMAdditions.h"

@interface TMDateOffer()

@property(nonatomic,assign)CGFloat contentYPos;
@property(nonatomic,assign)CGFloat contentMaxWidth;
@property(nonatomic,assign)CGFloat offerSectionHeight;
@property(nonatomic,assign)CGFloat recommendationSectionHeight;

@end

@implementation TMDateOffer

-(instancetype)initWithDateData:(NSDictionary *)data
{
    self = [super init];
    if(self) {
        [self setDefaults];
        
        id menuImages = data[@"menu_images"];
        if([menuImages isValidObject]) {
            self.menuImages = data[@"menu_images"];
            if([self.menuImages count]>0){
                self.isMenuAvailable = true;
            }else {
                self.isMenuAvailable = false;
            }
        }
        
        id recommendation = data[@"recommendation"];
        if([recommendation isValidObject]) {
            self.recommendation = data[@"recommendation"];
            self.isRecommendationAvailable = true;
        }else {
            self.isRecommendationAvailable = false;
        }
        
        id offer = data[@"offer"];
        if([offer isValidObject]) {
            self.offer = data[@"offer"];
        }
        
        id amountType = data[@"amount_type"];
        if([amountType isValidObject]) {
            if([data[@"amount_type"] isEqualToString:@"price"]) {
                self.isPriceAvailable = true;
                self.isPercentageAvailable = false;
            }else if([data[@"amount_type"] isEqualToString:@"percentage"]) {
                self.isPriceAvailable = false;
                self.isPercentageAvailable = true;
            }
        }
        
        id amountValue = data[@"amount_value"];
        if([amountValue isValidObject]) {
            self.amount = data[@"amount_value"];
        }
        
        id is_per_couple = data[@"is_per_couple"];
        if([is_per_couple isValidObject]) {
            self.is_per_couple = data[@"is_per_couple"];
        }
        
        id is_inclusive = data[@"is_inclusive"];
        if([is_inclusive isValidObject]) {
            self.is_inclusive = data[@"is_inclusive"];
        }
        if (data[@"deal_type"] && [data[@"deal_type"] isValidObject]) {
            self.dealType = data[@"deal_type"];
        }
    }
    return self;
}

-(void)setDefaults {
    self.spaceBetweenIconAndText = 10.0;
    self.spaceBetweenTwoSameSection = 8.0;
    self.spaceBetweenOfferAndRecommendationSection = 15.0;
    self.offerSectionHeight = 0;
    self.recommendationSectionHeight = 0;
    self.contentMaxWidth = 0;
    self.menuHeight = 50;
}

-(NSString *)offerAndRecommendationHeaderText
{
    return @"Datelicious Offer";
}

-(CGFloat)getTextMinXPos {
    return (self.iconWidth + self.spaceBetweenIconAndText);
}

-(CGFloat)offerAndRecommendationContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width
{
    self.contentYPos = 0;
    self.iconWidth = ((width*40)/100);
    self.contentMaxWidth = maxWidth - (self.iconWidth + self.spaceBetweenIconAndText);
    CGFloat height = [self contentHeight:maxWidth];
    
    return height;
}

-(CGFloat)contentHeight:(CGFloat)maxWidth {
    CGFloat contentHeight = 0;
    self.offerSectionHeight = [self getOfferContentHeight];
    
    ///////recommendation section////////////////
    self.recommendationSectionHeight = [self getRecommendationHeight];
    
    ////////total height///////
    if(self.recommendationSectionHeight > 0) {
        if(self.recommendationSectionHeight > self.iconWidth) {
            contentHeight = self.offerSectionHeight + self.spaceBetweenOfferAndRecommendationSection + self.recommendationSectionHeight;
        }
        else {
            contentHeight = self.offerSectionHeight + self.spaceBetweenOfferAndRecommendationSection + self.iconWidth;
        }
    }else {
        contentHeight = self.offerSectionHeight;
    }
    
    return contentHeight;
}

-(CGFloat)getOfferContentHeight
{
    CGFloat offerContentTempHeight = 0;
    
    CGFloat yPos = self.contentYPos;
    CGFloat height = 0;
    
    CGSize size = [self sizeForContent:[self offerAndRecommendationHeaderText] font:[UIFont systemFontOfSize:15.0]];
    height = size.height;
//    if(self.is_inclusive) {
//        size = [self sizeForContent:self.is_inclusive font:[UIFont systemFontOfSize:11.0]];
//        height = height+size.height;
//    }
    
//    if(height > self.iconWidth) {
//        yPos = yPos + height;
//    }
//    else {
//        yPos = yPos + self.iconWidth;
//    }
    
    yPos = height;
    
    size = [self sizeForContent:self.offer font:[UIFont systemFontOfSize:14.0]];
    height = size.height;
    
    yPos = yPos+height+self.spaceBetweenTwoSameSection;
    
    if(yPos > self.iconWidth) {
        yPos = yPos;
    }
    else {
        yPos = self.iconWidth;
    }

    
    if(self.isMenuAvailable) {
        yPos = yPos + self.menuHeight + self.spaceBetweenTwoSameSection;
    }
    
    offerContentTempHeight = yPos;
    
    return offerContentTempHeight;
}

-(CGFloat)getRecommendationHeight
{
    CGFloat recommendContentTempHeight = 0;
    if(self.isRecommendationAvailable){
        recommendContentTempHeight = [self sizeForContent:self.recommendation font:[UIFont systemFontOfSize:15.0]].height;
    }
    return recommendContentTempHeight;

}

-(CGSize)sizeForContent:(NSString*)content font:(UIFont*)font {
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth, NSUIntegerMax);
    CGRect rect = [content boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}

@end

//
//  TMDateContact.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDateContact.h"
#import "NSObject+TMAdditions.h"
#import "NSString+TMAdditions.h"

@interface TMDateContact()

@property(nonatomic, assign)CGFloat contentMaxWidth;

@end

@implementation TMDateContact

-(instancetype)initWithData:(NSArray *)locationArray
{
    self = [super init];
    if(self){
        [self setDefaults];
        
        self.locationArr = [[NSArray alloc] initWithArray:locationArray];
        
        if(self.locationArr.count>1) {
            self.isMultipleLocationAvail = true;
        }
        else {
            self.isMultipleLocationAvail = false;
        }
    }
    
    return self;
}

-(void)setDefaults
{
    self.spaceBetweenIconAndText = 10.0;
    self.spaceBetweenTwoSection = 15.0;
    
}

-(BOOL)isPhoneAvailable:(NSInteger)index
{
    NSDictionary *data = [[NSDictionary alloc] initWithDictionary:self.locationArr[index]];
    id phoneNumber = data[@"phone_number"];
    if([phoneNumber isValidObject]){
        return true;
    }
    return false;
}

-(BOOL)isAddressAvailable:(NSInteger)index
{
    NSDictionary *data = [[NSDictionary alloc] initWithDictionary:self.locationArr[index]];
    id address = data[@"address"];
    if([address isValidObject]){
        return true;
    }
    return false;
}

-(NSString *)getMultipleLocationText
{
    NSString *text;
    text = [NSString stringWithFormat:@"%lu %@",(unsigned long)self.locationArr.count,@"Locations"];
    return text;
}

-(CGFloat)contactContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width index:(NSInteger)index
{
    CGFloat contactHeight = 0;
    
    self.iconWidth = ((width*40)/100);
    self.contentMaxWidth = maxWidth - (self.iconWidth + self.spaceBetweenIconAndText);
    
    if([self isPhoneAvailable:index]) {
        contactHeight = self.iconWidth;
    }
    if([self isAddressAvailable:index]) {
        CGFloat height = [self sizeForContent:self.locationArr[index][@"address"]].height;
        if(height > self.iconWidth) {
            contactHeight = contactHeight + self.spaceBetweenTwoSection + height;
        }else {
            contactHeight = contactHeight + self.spaceBetweenTwoSection + self.iconWidth;
        }
    }
    return contactHeight;
}

-(CGFloat)contactContentHeightForDetailPage:(CGFloat)maxWidth iconWidth:(CGFloat)width
{
    CGFloat contactHeight = 0;
    
    self.iconWidth = ((width*40)/100);
    self.contentMaxWidth = maxWidth - (self.iconWidth + self.spaceBetweenIconAndText);
    
    if(self.isMultipleLocationAvail){
        contactHeight = self.iconWidth + 30.0;
    }
    else {
        // do calculate each value
        if([self isPhoneAvailable:0]) {
            contactHeight = self.iconWidth;
        }
        if([self isAddressAvailable:0]) {
            CGFloat height = [self sizeForContent:self.locationArr[0][@"address"]].height;
            if(height > self.iconWidth) {
                contactHeight = contactHeight + self.spaceBetweenTwoSection + height;
            }else {
                contactHeight = contactHeight + self.spaceBetweenTwoSection + self.iconWidth;
            }
        }
    }
    return contactHeight;
}

-(CGSize)sizeForContent:(NSString*)content {
    NSDictionary *attributes = @{NSFontAttributeName:[self contentDescriptionFont]};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth, NSUIntegerMax);
    CGRect rect = [content boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}

-(UIFont*)contentDescriptionFont {
    return [UIFont systemFontOfSize:15];
}

@end
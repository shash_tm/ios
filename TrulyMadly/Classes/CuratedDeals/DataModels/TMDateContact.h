//
//  TMDateContact.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMDateContact : NSObject

@property(nonatomic, assign)CGFloat spaceBetweenTwoSection;
@property(nonatomic, assign)BOOL isMultipleLocationAvail;
@property(nonatomic, strong)NSArray *locationArr;

@property(nonatomic,assign)CGFloat iconWidth;
@property(nonatomic, assign)CGFloat spaceBetweenIconAndText;

-(instancetype)initWithData:(NSArray *)locationArray;
-(CGFloat)contactContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width index:(NSInteger)index;
-(CGFloat)contactContentHeightForDetailPage:(CGFloat)maxWidth iconWidth:(CGFloat)width;
-(NSString *)getMultipleLocationText;
-(BOOL)isPhoneAvailable:(NSInteger)index;
-(BOOL)isAddressAvailable:(NSInteger)index;

@end

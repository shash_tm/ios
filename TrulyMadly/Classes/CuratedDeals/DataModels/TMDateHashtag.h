//
//  TMDateHashtag.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMDateHashtag : NSObject

@property(nonatomic,strong)NSMutableArray* hashtag;
@property(nonatomic,assign)CGFloat likeViewHeight;
@property(nonatomic,assign)UIEdgeInsets likeViewInset;
@property(nonatomic,assign)CGFloat likeViewHorizontalMargin;
@property(nonatomic,assign)CGFloat likeViewNewLineVerticalMargin;

-(instancetype)initWithHashtag:(NSArray *)hashtag;
-(CGFloat)hashtagContentHeight:(CGFloat)maxWidth;

@end

//
//  TMDateOffer.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMDateOffer : NSObject

@property(nonatomic, assign)BOOL isMenuAvailable;
@property(nonatomic, assign)BOOL isRecommendationAvailable;
@property(nonatomic, assign)BOOL isPriceAvailable;
@property(nonatomic, assign)BOOL isPercentageAvailable;

@property(nonatomic, strong)NSString *is_per_couple;
@property(nonatomic, strong)NSString *is_inclusive;
@property(nonatomic, strong)NSArray *menuImages;
@property(nonatomic, strong)NSString *offer;
@property(nonatomic, strong)NSString *recommendation;
@property(nonatomic, strong)NSString *amount;
@property(nonatomic, strong)NSString *dealType;

@property(nonatomic,assign)CGFloat iconWidth;
@property(nonatomic,assign)CGFloat menuHeight;

@property(nonatomic,assign)CGFloat spaceBetweenIconAndText;
@property(nonatomic,assign)CGFloat spaceBetweenTwoSameSection;
@property(nonatomic,assign)CGFloat spaceBetweenOfferAndRecommendationSection;

-(instancetype)initWithDateData:(NSDictionary *)data;
-(NSString*)offerAndRecommendationHeaderText;
-(CGFloat)offerAndRecommendationContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width;

@end

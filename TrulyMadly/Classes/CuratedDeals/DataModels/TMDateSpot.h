//
//  TMDateSpot.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMDateHeader.h"
#import "TMDateOffer.h"
#import "TMDateHashtag.h"

@class TMDateContact;
@class TMDateCondition;

@interface TMDateSpot : NSObject

@property(nonatomic, strong)NSString *dateSpotId;
@property(nonatomic, strong)NSString *dateStatus;
@property(nonatomic, strong)NSString *dateButtonText;
@property(nonatomic, strong)TMDateHeader *header;
@property(nonatomic, strong)TMDateHashtag *hashtag;
@property(nonatomic, strong)TMDateOffer *offer;
@property(nonatomic, strong)TMDateContact *contacts;
@property(nonatomic, strong)TMDateCondition *conditions;
@property(nonatomic, assign)int pricing;
@property(nonatomic, strong)NSString *communicationImage;
@property(nonatomic, assign)BOOL getNumber;
@property(nonatomic, strong)NSString *userNumber;

-(instancetype)initWithDateData:(NSDictionary *)dealData;
-(instancetype)initWithDateListData:(NSDictionary *)dateData;
-(NSInteger)sectionCount;
-(NSInteger)rowCountForSection:(NSInteger)section;
-(CGFloat)contentLayoutHeight:(NSInteger)section row:(NSInteger)row maxWidth:(CGFloat)maxWidth;
-(NSString*)messageText;

@end

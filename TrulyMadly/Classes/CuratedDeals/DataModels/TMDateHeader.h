//
//  TMDateHeader.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMDateHeader : NSObject

@property(nonatomic, strong)NSString *combinedName;
@property(nonatomic, strong)NSString *friendlyName;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSString *location;
@property(nonatomic, strong)NSArray *images;
@property(nonatomic, strong)NSString *imageURL;
@property(nonatomic, strong)NSString *date;

-(instancetype)initWithData:(NSDictionary *)data;

@end

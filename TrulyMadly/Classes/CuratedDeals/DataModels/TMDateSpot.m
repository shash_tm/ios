//
//  TMDateSpot.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDateSpot.h"
#import "TMDateHeader.h"
#import "TMDateHashtag.h"
#import "TMDateOffer.h"
#import "TMDateContact.h"
#import "TMDateCondition.h"
#import "NSObject+TMAdditions.h"

@implementation TMDateSpot

#define KEY_DATESPOT_ID @"datespot_id"
#define KEY_DATE_HASHTAGS @"hashtags"


-(instancetype)initWithDateData:(NSDictionary *)dateData {
    self = [super init];
    if(self){
        [self configureProfileWithDictionary:dateData];
    }
    return self;
}

-(instancetype)initWithDateListData:(NSDictionary *)dateData {
    self = [super init];
    if(self){
        [self configureProfileWithListDictionary:dateData];
    }
    return self;
}

-(void)configureProfileWithListDictionary:(NSDictionary *)dateData
{
     self.dateSpotId = (dateData[@"datespot_id"])?dateData[@"datespot_id"]:nil;
     self.dateStatus = (dateData[@"deal_status"])?dateData[@"deal_status"]:nil;
     self.header = [[TMDateHeader alloc] initWithData:dateData];
     self.hashtag = (dateData[@"hashtags"])?[[TMDateHashtag alloc] initWithHashtag:dateData[@"hashtags"]]:nil;
     self.offer = [[TMDateOffer alloc] initWithDateData:dateData];
     //self.contacts = [[TMDateContact alloc] initWithData:dateData];
     self.conditions = (dateData[@"terms_and_conditions"])?[[TMDateCondition alloc] initWithCondition:dateData[@"terms_and_conditions"]]:nil;
    self.pricing = (dateData[@"pricing"] && [dateData[@"pricing"] isValidObject])?[dateData[@"pricing"] intValue]:0;
}
-(void)configureProfileWithDictionary:(NSDictionary *)dateData
{
    NSDictionary *responseData = dateData[@"data"];
    NSDictionary *deal_details = dateData[@"deal_details"];
    
    self.dateSpotId = responseData[@"datespot_id"];
    self.dateStatus = deal_details[@"deal_status"];
    self.dateButtonText = deal_details[@"deal_text"];
    self.header = [[TMDateHeader alloc] initWithData:responseData];
    self.hashtag = [[TMDateHashtag alloc] initWithHashtag:responseData[@"hashtags"]];
    self.offer = [[TMDateOffer alloc] initWithDateData:responseData];
    self.contacts = (responseData[@"locations"] && [responseData[@"locations"] isKindOfClass:[NSArray class]])?[[TMDateContact alloc] initWithData:responseData[@"locations"]]:nil;
    if([responseData[@"terms_and_conditions"] isValidObject]) {
        self.conditions = [[TMDateCondition alloc] initWithCondition:responseData[@"terms_and_conditions"]];
    }
    if([responseData[@"communication_image"] isValidObject]) {
        self.communicationImage = responseData[@"communication_image"];
    }
    
    if(dateData[@"phone_details"]) {
        if([dateData[@"phone_details"] isValidObject]) {
            NSDictionary *phoneDetails = dateData[@"phone_details"];
            self.getNumber = [[phoneDetails objectForKey:@"get_number"] boolValue];
            if(phoneDetails[@"existing_no"])
            {
                if([phoneDetails[@"existing_no"] isValidObject]) {
                    self.userNumber = phoneDetails[@"existing_no"];
                }
            }
        }
    }
}


-(NSInteger)sectionCount
{
    if(self.conditions) {
        return 4;
    }
    return 3;
}

-(NSInteger)rowCountForSection:(NSInteger)section {
    return 1;
}

-(CGFloat)contentLayoutHeight:(NSInteger)section row:(NSInteger)row maxWidth:(CGFloat)maxWidth   {
    CGFloat height = 0;
    switch (section) {
        case 0://hashtag
            height = [self.hashtag hashtagContentHeight:maxWidth];
            break;
        case 1://OfferNRecommendation
            height = [self.offer offerAndRecommendationContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
            break;
        case 2://Contact Details
            height = [self.contacts contactContentHeightForDetailPage:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
            break;
        case 3://Conditions
            height = [self.conditions conditionContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
            break;
        default:
            break;
    }
    
    return height;
}

-(CGFloat)baseIconWidth:(CGFloat)maxWidth {
    CGFloat iconWidth = (maxWidth - (15*4))/5;
    return iconWidth;
}

-(NSString*)messageText {
    return @"Hey, want to meet at Lodi - the garden restauranr?";
}

@end
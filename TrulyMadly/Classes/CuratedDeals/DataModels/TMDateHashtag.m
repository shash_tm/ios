//
//  TMDateHashtag.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDateHashtag.h"
#import "TMProfileInterest.h"
#import "NSObject+TMAdditions.h"

@implementation TMDateHashtag

-(instancetype)initWithHashtag:(NSArray *)hashtag
{
    self = [super init];
    if(self){
        [self setDefaults];
        self.hashtag = [NSMutableArray arrayWithCapacity:16];
        
        NSInteger index = 0;
        if([hashtag isValidObject]) { //to prevent anc check for nil crash in swift
            for (NSString *interest in hashtag) {
                //check for common like
                BOOL isCommonLike = false;
                TMProfileInterest *profileInterest = [[TMProfileInterest alloc] initWithTag:interest isCommonLike:isCommonLike];
                [self.hashtag insertObject:profileInterest atIndex:index];
                index++;
            }
        }
    }
    return self;
}

-(void)setDefaults {
    self.likeViewHeight = 28;
    self.likeViewInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.likeViewHorizontalMargin = 5;
    self.likeViewNewLineVerticalMargin = 10;
}

-(CGFloat)hashtagContentHeight:(CGFloat)maxWidth {
    CGFloat contentHeight = 0;
    CGFloat xPos = 0;
    CGFloat yPos = 0;
    for (int i=0; i<self.hashtag.count;i++) {
        TMProfileInterest *profileInterest = [self.hashtag objectAtIndex:i];
        CGFloat likeViewWidth = [profileInterest tagSizeForMaximumWidth:maxWidth].width + self.likeViewInset.left+self.likeViewInset.right;
        
        if((xPos+likeViewWidth) > maxWidth) {
            xPos = 0;
            yPos = yPos + self.likeViewHeight + self.likeViewNewLineVerticalMargin;
        }
        xPos = xPos + likeViewWidth + self.likeViewHorizontalMargin;
    }
    contentHeight = yPos + self.likeViewHeight;
    
    return contentHeight;
}

@end

//
//  TMDateCondition.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDateCondition.h"
#import "NSString+TMAdditions.h"

@interface TMDateCondition()

@property(nonatomic, assign)CGFloat contentMaxWidth;

@end

@implementation TMDateCondition

-(instancetype)initWithCondition:(NSArray *)conditions
{
    self = [super init];
    if(self) {
        self.termsNCondition = conditions;
        [self setDefaults];
    }
    return self;
}

-(NSString *)getTermsHeaderText
{
    return @"How It Works";
}

-(void)setDefaults
{
    self.spaceBetweenIconAndText = 10.0;
    self.spaceBetweenTwoConditions = 12.0;
    
}

-(CGFloat)getIconWidth
{
    return self.iconWidth;
}

-(CGFloat)conditionContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width
{
    CGFloat conditionsTmpHeight = 0;
    
    self.iconWidth = ((width*40)/100);
    self.contentMaxWidth = maxWidth - (self.iconWidth + self.spaceBetweenIconAndText);
    
    for (int i=0; i<self.termsNCondition.count; i++){
        conditionsTmpHeight = conditionsTmpHeight + self.spaceBetweenTwoConditions + [self sizeForContent:self.termsNCondition[i]].height;
    }
    
    conditionsTmpHeight = conditionsTmpHeight+self.spaceBetweenTwoConditions + 30;
    
    return conditionsTmpHeight;
}

-(CGSize)sizeForContent:(NSString*)content {
    NSDictionary *attributes = @{NSFontAttributeName:[self contentDescriptionFont]};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth, NSUIntegerMax);
    CGRect rect = [content boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}

-(UIFont*)contentDescriptionFont {
    return [UIFont systemFontOfSize:14];
}

@end

//
//  TMDateHeader.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDateHeader.h"
#import "NSObject+TMAdditions.h"

@implementation TMDateHeader

-(instancetype)initWithData:(NSDictionary *)data
{
    self = [super init];
    if(self){
        self.friendlyName = @"";
        self.name = @"";
        self.location = @"";
        
//        if(data[@"friendly_name"]) {
//            id friendly_name = data[@"friendly_name"];
//            if([friendly_name isValidObject]) {
//                self.friendlyName = data[@"friendly_name"];
//            }
//        }
        
        if(data[@"name"]) {
            id name = data[@"name"];
            if([name isValidObject]) {
                self.name = data[@"name"];
            }
        }
        
//        if(data[@"location"]) {
//            id location = data[@"location"];
//            id multipleLoc = data[@"location_count"];
//            if([location isValidObject]) {
//                if([multipleLoc isValidObject] && [data[@"location_count"] integerValue]>1){
//                    self.location = (data[@"multiple_locations_text"]) ? data[@"multiple_locations_text"] : @"";
//                }else {
//                    self.location = data[@"location"];
//                }
//            }
//        }
        
        if (data[@"locations"] && [data[@"locations"] isKindOfClass:[NSArray class]]) {
            NSArray *loc = data[@"locations"];
            if(loc.count>1){
                self.location = (data[@"multiple_locations_text"]) ? data[@"multiple_locations_text"] : @"";
            }else if(loc.count>0){
                self.location = loc[0][@"location"];
            }else{
                self.location = @"";
            }
        }else {
            self.location = @"";
        }
        
        if(data[@"images"]) {
            id images = data[@"images"];
            if([images isValidObject]) {
                self.images = [[NSArray alloc] initWithArray:data[@"images"]];
            }else {
                self.images = [[NSArray alloc] init];
            }
        }
        if (data[@"image"]) {
             self.imageURL = data[@"image"];
        }
        self.combinedName = self.name;//[[self.friendlyName stringByAppendingString:@" at "] stringByAppendingString:self.name];
        
        // for event only
        self.date = (data[@"pretty_date"] && !([data[@"pretty_date"] isKindOfClass:[NSNull class]])) ? data[@"pretty_date"] :@"";
        if(![self.date isEqualToString:@""]) {
            self.location = [NSString stringWithFormat:@"%@, %@",self.date,self.location];
        }
    }
    return self;
}

@end

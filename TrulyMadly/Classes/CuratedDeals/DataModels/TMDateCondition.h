//
//  TMDateCondition.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 22/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMDateCondition : NSObject

@property(nonatomic, strong)NSArray *termsNCondition;

@property(nonatomic,assign)CGFloat iconWidth;
@property(nonatomic,assign)CGFloat spaceBetweenTwoConditions;
@property(nonatomic, assign)CGFloat spaceBetweenIconAndText;

-(instancetype)initWithCondition:(NSArray *)conditions;
-(CGFloat)conditionContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width;
-(NSString *)getTermsHeaderText;
-(CGFloat)getIconWidth;

@end

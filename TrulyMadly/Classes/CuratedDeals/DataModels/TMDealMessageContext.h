//
//  TMDealMessageContext.h
//  TrulyMadly
//
//  Created by Ankit on 04/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@interface TMDealMessageContext : NSObject

@property(nonatomic,strong)NSString *dealId;
@property(nonatomic,strong)NSString *datespotId;
@property(nonatomic,strong)NSString *matchId;
@property(nonatomic,strong)NSString *datespotAddress;
@property(nonatomic,strong)NSString *datespotFriendlyName;
@property(nonatomic,strong)NSString *messageImageURL;
@property(nonatomic,strong)NSString *messageText;
@property(nonatomic,assign)TMMessageType dealMessageType;

- (void) setDatespotDisplayName:(NSString *)datespotDisplayName;

-(NSDictionary*)insertQueryParams;
-(NSDictionary*)updateQueryParams;

@end

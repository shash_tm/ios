//
//  TMDealListCollectionViewCell.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 28/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMDateSpot.h"

@interface TMDealListCollectionViewCell : UICollectionViewCell

//date spot property
@property (nonatomic, strong) TMDateSpot* dateSpot;

@end

//
//  TMScoreView.m
//  TrulyMadly
//
//  Created by Ankit Jain on 27/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMScoreView.h"
#import "TMStringUtil.h"

//kimgStr
//ktext
//ksttext
//kfont
//kstfont
//ktextcol
//ksttextcol
//kwidthOffset
//kheightoffset


@interface TMScoreView ()

@property(nonatomic,strong)UILabel *titleText;
@property(nonatomic,strong)UILabel *subtitleText;
@property(nonatomic,strong)UILabel *headerText;
@property(nonatomic,strong)UIImageView *imgView;
@property(nonatomic,strong)UIImageView *centerImgView;
@property(nonatomic,strong)UIButton *btn;

@property(nonatomic,assign)BOOL state;


@end

@implementation TMScoreView

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.imgView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imgView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.imgView];
        
        self.centerImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.centerImgView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.centerImgView];
        
        self.titleText = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleText.backgroundColor = [UIColor clearColor];
        self.titleText.numberOfLines = 0;
        self.titleText.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleText];
        
        self.subtitleText = [[UILabel alloc] initWithFrame:CGRectZero];
        self.subtitleText.backgroundColor = [UIColor clearColor];
        [self addSubview:self.subtitleText];
        
        self.headerText = [[UILabel alloc] initWithFrame:CGRectZero];
        self.headerText.backgroundColor = [UIColor clearColor];
        [self addSubview:self.headerText];
    }
    return self;
}

-(void)setEnableUserIneraction:(BOOL)enableUserIneraction {
    _enableUserIneraction = enableUserIneraction;
    
    if(enableUserIneraction) {
        
        self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn.backgroundColor = [UIColor clearColor];
        self.btn.frame = self.imgView.frame;
        [self.btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btn];
    }
}

-(void)setTitleTextWithAttributeDictionary:(NSDictionary*)attributeDict {
    
    UIColor *color = [attributeDict objectForKey:@"tcolor"];
    UIFont *font = [attributeDict objectForKey:@"kfont"];
    NSString *text = [attributeDict objectForKey:@"ktext"];
    CGFloat widthOffset = ([attributeDict objectForKey:@"kwidthOffset"]) ? [[attributeDict objectForKey:@"kwidthOffset"] floatValue] : 0;
    
    //CGFloat heightOffset = ([attributeDict objectForKey:@"kheightoffset"]) ? [[attributeDict objectForKey:@"kheightoffset"] floatValue] : 0;
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:self.frame.size.width-widthOffset], @"kmaxwidth", [NSNumber numberWithFloat:40], @"kmaxheight", text, @"ktext", nil];
    CGSize size = [TMStringUtil textRectForString:dict].size;
    
    self.titleText.frame = CGRectMake((self.bounds.size.width - size.width)/2,
                                      (self.bounds.size.height - size.height)/2,
                                      size.width,
                                      size.height);
    self.titleText.font = font;
    self.titleText.textColor = [attributeDict objectForKey:@"ktextcol"];
    self.titleText.text = text;
    if(color) {
        self.titleText.textColor = color;
    }
    //////
    text = [attributeDict objectForKey:@"ksttext"];
    if(text) {
        font = [attributeDict objectForKey:@"kstfont"];
        
        dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:self.frame.size.width], @"kmaxwidth", [NSNumber numberWithFloat:40], @"kmaxheight", text, @"ktext", nil];
        size = [TMStringUtil textRectForString:dict].size;
        CGFloat subTitleYPos = self.titleText.frame.origin.y + self.titleText.frame.size.width + 2;
        self.subtitleText.frame = CGRectMake(0,
                                             subTitleYPos,
                                             self.bounds.size.width,
                                             size.height);
        self.subtitleText.font = font;
        self.subtitleText.textColor = [UIColor grayColor];//[attributeDict objectForKey:@"ksttextcol"];
        self.subtitleText.text = text;
        self.subtitleText.textAlignment = NSTextAlignmentCenter;
    }
    
}

-(void)setTitleImageWithAttributeDictionary:(NSDictionary*)attributeDict {
    NSString *imgStr = [attributeDict objectForKey:@"ktitleimgStr"];
    UIImage *img = [UIImage imageNamed:imgStr];
    self.centerImgView.image = img;
    //CGSize imgSize = img.size;
    CGFloat width = self.bounds.size.width * 0.20;
    CGFloat height = self.bounds.size.width * 0.15;
    CGFloat xPos = (self.bounds.size.width - width)/2;
    CGFloat yPos = (self.bounds.size.height - height)/2;
    self.centerImgView.frame = CGRectMake(xPos,
                                          yPos,
                                          width,
                                          height);
}

-(void)setHeadeTextWithAttributeDictionary:(NSDictionary*)attributeDict {
    
    UIFont *font = [attributeDict objectForKey:@"kfont"];
    NSString *text = [attributeDict objectForKey:@"ktext"];
    UIColor *textColor = ([attributeDict objectForKey:@"ktextcol"]) ? [attributeDict objectForKey:@"ktextcol"] : [UIColor blackColor];
    CGFloat heightOffset = ([attributeDict objectForKey:@"kheightoffset"]) ? [[attributeDict objectForKey:@"kheightoffset"] floatValue] : 0;
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:font, @"kfont", [NSNumber numberWithFloat:self.frame.size.width], @"kmaxwidth", [NSNumber numberWithInt:20], @"kmaxheight",text, @"ktext", nil];
    CGSize size = [TMStringUtil textRectForString:dict].size;
    /////////////////////////////////////////////////////
    //added as a hack to acoomodate "on Adaptability" text in 320 width devices
    //add x posiotion offset
    //so x = -xPosOffset
    //width = width - xPosOffset
    /////////////////////////////////////////////////////
    CGFloat xPosOffet = 1;
    self.headerText.frame = CGRectMake(-xPosOffet,
                                      0,
                                      self.bounds.size.width+xPosOffet,
                                      floorf(size.height));
    //NSLog(@"Frame1:%@",NSStringFromCGRect(self.headerText.frame));
    self.headerText.font = font;
    self.headerText.textColor = textColor;
    self.headerText.textAlignment = NSTextAlignmentCenter;
    self.headerText.text = text;

    ///
    self.imgView.frame = CGRectMake(self.bounds.origin.x, 25, self.bounds.size.width, self.bounds.size.width);
    
    CGFloat updatedLabelHeight = 0.0f;
    
    if(self.subtitleText.text) {
        updatedLabelHeight = ((self.imgView.frame.size.height - self.titleText.frame.size.height)/2) + 25 - heightOffset;
    }
    else {
        updatedLabelHeight = ((self.imgView.frame.size.height - self.titleText.frame.size.height)/2) + 25;
    }
    
    CGRect rect = CGRectMake(((self.imgView.frame.size.width - self.titleText.frame.size.width)/2),
                             updatedLabelHeight,
                             self.titleText.frame.size.width,
                             self.titleText.frame.size.height);

    self.titleText.frame = rect;
    
    updatedLabelHeight = self.titleText.frame.origin.y + self.titleText.frame.size.height;
    rect = CGRectMake(self.subtitleText.frame.origin.x,
                      updatedLabelHeight,
                      self.subtitleText.frame.size.width,
                      self.subtitleText.frame.size.height-1);//hack to bring height diff between title and subltitle view
    
    self.subtitleText.frame = rect;
    
    if(self.enableUserIneraction) {
        self.btn.frame = self.imgView.frame;
    }
    
    if(self.centerImgView.image) {
        CGFloat width = self.imgView.frame.size.width * 0.20;
        CGFloat height = self.imgView.frame.size.height * 0.15;
        CGFloat xPos = (self.imgView.frame.size.width - width)/2;
        CGFloat yPos = (self.imgView.frame.size.height - height)/2;
        self.centerImgView.frame = CGRectMake(xPos,
                                              yPos+25,
                                              width,
                                              height);
    }
   
}

-(void)setImageWithAttributeDictionary:(NSDictionary*)attributeDict {
    
    NSString *imgStr = [attributeDict objectForKey:@"kimgStr"];
    self.imgView.image = [UIImage imageNamed:imgStr];
}

-(void)btnAction:(UIButton*)btn {
    
    //common cell active
    if([self.headerText.text isEqualToString:@"On Values"]) {
        [self.delegate scoreViewDidClickWithKey:self.headerText.text];
    }
    else if([self.headerText.text isEqualToString:@"On Adaptability"]) {
        [self.delegate scoreViewDidClickWithKey:self.headerText.text];
    }
    else if(self.state) {
        self.state = FALSE;
        self.subtitleText.text = @"Tap";
        [self.delegate scoreViewDidClickWithState:self.state];
    }
    else {
        self.state = TRUE;
        self.subtitleText.text = @"Hide";
        [self.delegate scoreViewDidClickWithState:self.state];
    }
}

@end

//
//  TMQuizIntroScreenView.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 23/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMQuizIntroScreenView.h"
#import "UIImageView+AFNetworking.h"
#import "TMActivityIndicatorView.h"
#import "TMRetryView.h"


typedef BOOL (^quizQuestionsFetchStateBlock)();
typedef void (^startButtonPressActionBlock)(BOOL quizQuestionsDownloaded);
typedef void (^quizRetryActionBlock)();

#define ACTIVITY_INDICATOR_BACKGROUND_VIEW_TAG 1234321

@interface TMQuizIntroScreenView ()

@property (weak, nonatomic) IBOutlet UIImageView *quizIntroImageView;
@property (weak, nonatomic) IBOutlet UIButton *quizIntroStartButton;
@property (weak, nonatomic) IBOutlet UILabel *quizIntroTitleLabel;
@property (strong, nonatomic) quizQuestionsFetchStateBlock currentQuizQuestionsFetchStateBlock;
@property (strong, nonatomic) startButtonPressActionBlock currentStartButtonPressActionBlock;
@property (strong, nonatomic) quizRetryActionBlock currentRetryActionBlock;
@property (weak, nonatomic) IBOutlet UIView *quizIntroInfoContainerView;
@property (nonatomic) TMActivityIndicatorView* activityIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel *playNowLabel;
@property (weak, nonatomic) IBOutlet UILabel *quizDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *quizDescriptionTextView;
@property (strong, nonatomic) TMRetryView* retryView;
@property (weak, nonatomic) IBOutlet UILabel *sponsoredText;



@end

@implementation TMQuizIntroScreenView

- (void) awakeFromNib
{
    [super awakeFromNib];
    //add observer for decide action failure handling
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quizActionFailureReceived:) name:@"quizDecideActionFailed" object:nil];
}

- (IBAction)startButtonPressed:(UIButton *)sender {
    
    if (self.currentQuizQuestionsFetchStateBlock()) {
        // quiz questions not fetched yet
        //show activity indicator
         [self showActivityIndicatorViewWithMessage:@"Ready, Set, Quiz..."];
        
        //a block to notify delegate about the button press
        self.currentStartButtonPressActionBlock(NO);
    }
    else {
        //a block for the action to display quiz questions
        self.currentStartButtonPressActionBlock(YES);
    }
}

- (void) fillQuizIntroDataWithQuizName:(NSString*) quizName quizImageURLString:(NSString*) quizImageURLString quizDetailTextString:(NSString*) quizDetailTextString quizSponsoredText:(NSString*) sponsoredText withQuizQuestionsFetchStateBlock:(BOOL(^)()) quizQuestionsFetchStateBlock startButtonPressBlock:(void(^)(BOOL quizQuestionsDownloaded)) startButtonPressBlock retryActionBlock:(void(^)(void)) quizRetryActionBlock
{
//    self.quizIntroInfoContainerView.layer.cornerRadius = 6;
//    self.quizIntroInfoContainerView.layer.borderWidth = 1;
//    self.quizIntroInfoContainerView.layer.borderColor = [UIColor colorWithRed:90.0/255.0 green:136.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    
    [self.quizIntroImageView setImageWithURL:[NSURL URLWithString:quizImageURLString] placeholderImage:[UIImage imageNamed:@"Quiz_Placeholder"]];
    self.quizIntroTitleLabel.text = quizName;
    self.quizDescriptionTextView.text = quizDetailTextString;
    self.sponsoredText.text = sponsoredText;
    self.currentQuizQuestionsFetchStateBlock = quizQuestionsFetchStateBlock;
    self.currentStartButtonPressActionBlock = startButtonPressBlock;
    self.currentRetryActionBlock = quizRetryActionBlock;
}

-(void) showActivityIndicatorViewWithMessage:(NSString*)message
{
    //hide the activity indicator view
    [self hideActivityIndicatorView];
    
    //hide the retry view
    [self removeRetryView];
    
    self.quizIntroStartButton.hidden = YES;
    self.playNowLabel.hidden = YES;
    self.quizDescriptionTextView.hidden = YES;
    
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] init];
    self.activityIndicatorView.titleLable.textColor = [UIColor grayColor];
    [self addSubview:self.activityIndicatorView];
    self.activityIndicatorView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + self.frame.size.height/8);
    self.activityIndicatorView.titleText = message;
    [self.activityIndicatorView startAnimating];
}

/**
 * hides the activity indicator view
 */
-(void)hideActivityIndicatorView {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    while ([self viewWithTag:ACTIVITY_INDICATOR_BACKGROUND_VIEW_TAG]) {
        [[self viewWithTag:ACTIVITY_INDICATOR_BACKGROUND_VIEW_TAG] removeFromSuperview];
    }
    self.activityIndicatorView = nil;
    self.quizIntroStartButton.hidden = NO;
    self.playNowLabel.hidden = NO;
}


#pragma mark - TMRetryView methods

-(void)showRetryViewWithMessage:(NSString*) failureMessage {
    
    //hide the activity indicator view
    [self hideActivityIndicatorView];
    
    //hide the retry action view
    [self removeRetryView];
    
    self.retryView = [[TMRetryView alloc] initWithFrame:self.frame message:failureMessage];
    [self.retryView.retryButton addTarget:self action:@selector(quizRetryAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.retryView];
}

-(void)removeRetryView {
    [self.retryView removeFromSuperview];
    self.retryView = nil;
}

- (void) quizActionFailureReceived:(NSNotification*) failureNotification
{
    NSString* failureMessage = @"Quiz action failed.";
    
    if ([failureNotification.object isKindOfClass:[NSString class]] && [failureNotification.object isEqualToString:@"networkFailure"]) {
        failureMessage = @"No internet connectivity.";
    }
    
    if (self.activityIndicatorView) {
        [self showRetryViewWithMessage:failureMessage];
    }
}

- (void) quizRetryAction {
    [self removeRetryView];
    //show activity indicator
    [self showActivityIndicatorViewWithMessage:@"Ready, Set, Quiz..."];
    
    self.currentRetryActionBlock();
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

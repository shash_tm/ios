//
//  TMScoreView.h
//  TrulyMadly
//
//  Created by Ankit Jain on 27/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  TMScoreViewDelegate;

@interface TMScoreView : UIView

@property(nonatomic,weak)id<TMScoreViewDelegate> delegate;
@property(nonatomic,assign)BOOL enableUserIneraction;

-(void)setTitleTextWithAttributeDictionary:(NSDictionary*)attributeDict;
-(void)setHeadeTextWithAttributeDictionary:(NSDictionary*)attributeDict;
-(void)setImageWithAttributeDictionary:(NSDictionary*)attributeDict;
-(void)setTitleImageWithAttributeDictionary:(NSDictionary*)attributeDict;
@end

@protocol  TMScoreViewDelegate <NSObject>

-(void)scoreViewDidClickWithState:(BOOL)state;
-(void)scoreViewDidClickWithKey:(NSString*)key;

@end
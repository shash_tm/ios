//
//  TMCustomAlertView.h
//  TMCustomAlertView
//
//  Created by Abhijeet Mishra on 21/07/15.
//  Copyright (c) 2015 Abhijeet Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMPhotoAlertViewDelegate <NSObject>

@optional

- (void) actionButtonPressed;

- (void) cancelButtonPressed;

@end

@interface TMPhotoAlertView : UIView


@property (nonatomic, weak) id<TMPhotoAlertViewDelegate> delegate;


/**
 * Shows the custom alert view with the message
 * @param message for the alert view
 * @param parent view for the alert
 * @param frame for the alert
 * @param delegate for the alert
 */
+ (void) customAlertViewWithMessage:(NSString*) message parentView:(UIView*) parentView delegate:(id<TMPhotoAlertViewDelegate>) delegate;


/**
 * Removes the custom alert view with the message
 * @param base view for the alert
 */
+ (void) removeCustomAlertViewFromView:(UIView*) baseView;

@end

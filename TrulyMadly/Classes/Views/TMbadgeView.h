//
//  TMbadgeView.h
//  TrulyMadly
//
//  Created by Ankit on 08/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMBadgeView : UIView

@property(nonatomic,strong)UIButton *badgeButton;

-(instancetype)initWithFrame:(CGRect)frame
                   withImage:(UIImage*)image
              withImageFrame:(CGRect)imageFrame;

-(void)setBadgeNotificationStatus:(BOOL)status;

-(void)setBadgeNotificationStatusForLocation:(BOOL)status;

-(void)setBadgeNotificationStatusForMenu:(BOOL)status;

-(void)setCounter:(NSString *)counter;

@end

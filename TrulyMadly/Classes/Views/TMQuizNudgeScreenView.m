//
//  TMQuizNudgeScreenView.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 07/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMQuizNudgeScreenView.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)

#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

//#define SLIDING_MENU_HEIGHT (IS_IPHONE_6?44:(IS_IPHONE_6_PLUS?51:44))

@interface TMQuizNudgeScreenView ()

@property (strong, nonatomic) UIView* titleView;
@property (strong, nonatomic) UIView* messageView;
@property (strong, nonatomic) UIView* buttonBaseView;
@property (weak, nonatomic) id<TMQuizNudgeScreenDelegate> delegate;
@property (strong, nonatomic) NSString* currentMatchName;
@property (strong, nonatomic) NSString* quizIconURLString;
@property (strong, nonatomic) NSString *sponsoredImageURLString;
@property (strong, nonatomic) NSString *quizSuccessMessage;
@property (strong, nonatomic) NSMutableString *quizSuccessNudge;

@end

@implementation TMQuizNudgeScreenView

- (instancetype) initWithFrame:(CGRect)frame delegate:(id<TMQuizNudgeScreenDelegate>) delegate forMatchName:(NSString*) matchName quizIconURLString:(NSString*) quizIconURLString sponsoredImageURLString:(NSString*) sponsoredImageURL quizSuccessMessage:(NSString*) successMessage quizSuccessNudge:(NSString*) successNudge {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.delegate = delegate;
        if(matchName == nil) {
            matchName = @"";
        }
        self.currentMatchName = matchName;
        self.quizIconURLString = quizIconURLString;
        self.sponsoredImageURLString = sponsoredImageURL;
        self.quizSuccessMessage = successMessage;
        if(successNudge != nil) {
            self.quizSuccessNudge = [NSMutableString stringWithString:successNudge];
        }
        [self createSubViews];
    }
    return self;
}


- (void) createSubViews
{
    //create the quiz logo
    UIImageView* logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width*3/8, self.bounds.size.height/8, self.bounds.size.width/4, self.bounds.size.width/4)];
    logoImageView.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height*2.5/8);
    logoImageView.clipsToBounds = YES;
    logoImageView.layer.cornerRadius = logoImageView.bounds.size.width/2;
    logoImageView.backgroundColor = [UIColor clearColor];
    [logoImageView setImageWithURL:[NSURL URLWithString:self.quizIconURLString]];
    [self addSubview:logoImageView];
    
    int titleViewHeight = (self.bounds.size.height/8 > 40)? 40:self.bounds.size.height;
    
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, titleViewHeight)];
    self.titleView.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2 - self.bounds.size.height/20);
    self.titleView.backgroundColor = [UIColor clearColor];
    [self createTitleViewSubViews];
    [self addSubview:self.titleView];
    
    self.messageView = [[UIView alloc] initWithFrame:CGRectMake(0, self.titleView.frame.origin.y + self.titleView.bounds.size.height, self.bounds.size.width, self.bounds.size.height/5)];
    self.messageView.backgroundColor = [UIColor clearColor];
    [self createMessageViewSubViews];
    [self addSubview:self.messageView];
    
    UIImageView *sponsoredImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.messageView.frame.origin.y + self.messageView.bounds.size.height , self.bounds.size.width, self.bounds.size.width/4)];
    [sponsoredImageView setImageWithURL:[NSURL URLWithString:self.sponsoredImageURLString]];
    sponsoredImageView.backgroundColor = [UIColor clearColor];
    sponsoredImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:sponsoredImageView];
    
    int buttonHeight = (self.bounds.size.height > 60)?60:self.bounds.size.height;
    
    self.buttonBaseView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - buttonHeight, self.bounds.size.width, buttonHeight)];
    self.buttonBaseView.backgroundColor = [UIColor clearColor];
    [self createControlButtons];
    [self addSubview:self.buttonBaseView];
}

- (void) createTitleViewSubViews
{
    NSDictionary *attributes;
    CGSize constraintSize;
    CGRect frame;
    
    UIFont *titleLabelFont = (IS_IPHONE_6?[UIFont fontWithName:@"HelveticaNeue" size:24.0]:(IS_IPHONE_6_PLUS?[UIFont fontWithName:@"HelveticaNeue" size:26.0]:[UIFont fontWithName:@"HelveticaNeue" size:20.0]));
    attributes = @{NSFontAttributeName:titleLabelFont};
    constraintSize = CGSizeMake(self.titleView.bounds.size.width*8/10, 200);
    if(self.quizSuccessMessage == nil) {
        self.quizSuccessMessage = @"Awesome, you are done!";
    }
    frame = [self.quizSuccessMessage boundingRectWithConstraintSize:constraintSize attributeDictionary:attributes];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.titleView.bounds.origin.x + self.titleView.bounds.size.width/10, self.titleView.bounds.origin.y + self.titleView.bounds.size.height/5, self.titleView.bounds.size.width*8/10, CGRectGetHeight(frame))];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.center = CGPointMake(self.bounds.size.width/2, titleLabel.center.y);
    titleLabel.font = titleLabelFont;
    titleLabel.numberOfLines = 0;
    titleLabel.text = self.quizSuccessMessage;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor darkGrayColor];
    [self.titleView addSubview:titleLabel];
}

- (void) createMessageViewSubViews
{
    UILabel* messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.messageView.bounds.origin.x + self.messageView.bounds.size.width/10, self.messageView.bounds.size.height/8, self.messageView.bounds.size.width*8/10, self.messageView.bounds.size.height)];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.font = (IS_IPHONE_6?[UIFont fontWithName:@"HelveticaNeue" size:18.0]:(IS_IPHONE_6_PLUS?[UIFont fontWithName:@"HelveticaNeue" size:18.0]:[UIFont fontWithName:@"HelveticaNeue" size:16.0]));
    messageLabel.textColor = [UIColor colorWithRed:(76.0/255.0) green:(76.0/255.0) blue:(76.0/255.0) alpha:1.0];
    if(self.quizSuccessNudge == nil) {
       self.quizSuccessNudge = [NSMutableString stringWithString:[NSString stringWithFormat:@"Would you like to nudge %@?",self.currentMatchName]];
    }else {
        self.quizSuccessNudge = [[self.quizSuccessNudge stringByReplacingOccurrencesOfString:@"_name" withString:self.currentMatchName] mutableCopy];
    }
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.text = self.quizSuccessNudge;
    [self.messageView addSubview:messageLabel];
}


- (void) createControlButtons
{
    UIButton* skipButton = [[UIButton alloc] initWithFrame:CGRectMake(self.buttonBaseView.bounds.origin.x, self.buttonBaseView.bounds.origin.y, self.buttonBaseView.bounds.size.width/2, self.buttonBaseView.bounds.size.height)];
    [skipButton setTitle:@"Skip" forState:UIControlStateNormal];
    skipButton.backgroundColor = [UIColor colorWithRed:172.0/255.0 green:198.0/255.0 blue:209.0/255.0 alpha:1.0];
    [skipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    skipButton.titleLabel.font = (IS_IPHONE_6?[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0]:(IS_IPHONE_6_PLUS?[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0]:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0]));
    [skipButton addTarget:self action:@selector(skipButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBaseView addSubview:skipButton];
    
    UIButton* nudgeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.buttonBaseView.bounds.origin.x + skipButton.bounds.origin.x + skipButton.bounds.size.width, self.buttonBaseView.bounds.origin.y, self.buttonBaseView.bounds.size.width/2, self.buttonBaseView.bounds.size.height)];
    [nudgeButton setTitle:@"Nudge" forState:UIControlStateNormal];
    nudgeButton.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:179.0/255.0 blue:177.0/255.0 alpha:1.0];
    nudgeButton.titleLabel.font = (IS_IPHONE_6?[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0]:(IS_IPHONE_6_PLUS?[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0]:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0]));
    [nudgeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nudgeButton addTarget:self action:@selector(nudgeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBaseView addSubview:nudgeButton];
}

- (void) skipButtonPressed:(UIButton*) skipButton
{
    [self.delegate skipButtonPressed];
    [self removeNudgeScreenView];
}

- (void) nudgeButtonPressed:(UIButton*) nudgeButton
{
    [self.delegate nudgeButtonPressed];
    [self removeNudgeScreenView];
}

- (void) removeNudgeScreenView
{
    [self removeFromSuperview];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

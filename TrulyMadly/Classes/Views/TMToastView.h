//
//  TMToastView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TMToastPresentationFromTop,
    TMToastPresentationFromBottom,
    TMToastPresentationFromLeft,
    TMToastPresentationFromRight
} TMToastPresentationDirection;

@interface TMToastView : UIView

@property (strong, nonatomic) NSString *text;

+(void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection) presentationDirection;

+(void)showToastInParentViewWithAlertIcon: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection) presentationDirection;

+(void)showToastInParentViewWithMissTM :(UIView *)parentView withText:(NSString *)text withDuration:(float)duration presentationDirection:(TMToastPresentationDirection) presentationDirection;

+(void)showToastInParentViewWithEditProfileEnabled: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection) presentationDirection;

+(void)showToastInParentViewForMutualEvent:(UIView *)parentView withMutualEventData:(NSArray *)mutualEventData withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection)presentationDirection;

+(void)showToastInParentViewForPhotoFlow:(UIView *)parentView imageName:(NSString *)imageName
                             textMessage:(NSString *)textMessage  withDuaration:(float)duration
                   presentationDirection:(TMToastPresentationDirection)presentationDirection
                         removeAfterTime:(BOOL)removeAfterTime;

@end

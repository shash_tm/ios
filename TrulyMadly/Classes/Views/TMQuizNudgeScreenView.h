//
//  TMQuizNudgeScreenView.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 07/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMQuizNudgeScreenDelegate <NSObject>

- (void) nudgeButtonPressed;
- (void) skipButtonPressed;

@end

@interface TMQuizNudgeScreenView : UIView

- (instancetype) initWithFrame:(CGRect)frame delegate:(id<TMQuizNudgeScreenDelegate>) delegate forMatchName:(NSString*) matchName quizIconURLString:(NSString*) quizIconURLString sponsoredImageURLString:(NSString*) sponsoredImageURL quizSuccessMessage:(NSString*) successMessage quizSuccessNudge:(NSString*) successNudge;

@end

//
//  TMProfileVisibilityAlertView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 18/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMProfileVisibilityAlertViewDelegate;

@interface TMProfileVisibilityAlertView : UIView

@property(nonatomic,weak)id<TMProfileVisibilityAlertViewDelegate> alertViewDelegate;

-(instancetype)initWithMessage:(NSString *)msg frame:(CGRect)frame fromLike:(BOOL)fromLike;

@end

@protocol TMProfileVisibilityAlertViewDelegate <NSObject>

-(void)didRemoveView:(BOOL)fromLike;
-(void)didNotNowClick;

@end
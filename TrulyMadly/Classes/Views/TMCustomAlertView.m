//
//  TMCustomAlertView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMCustomAlertView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMRateView.h"
#import "NSString+TMAdditions.h"

@interface TMCustomAlertView()<TMRateViewDelegate>

@property (nonatomic, strong)NSNumber *rate;
@property (nonatomic, strong)UIView *alertBodyView;
@property (nonatomic, strong)TMRateView *rateView;

@end

@implementation TMCustomAlertView

-(instancetype)init {
    self = [super init];
    return self;
}

-(void)initAlertWithMessageAndRateView:(NSString *)message parentView:(UIView*) parentView delegate:(id<TMCustomAlertViewDelegate>)delegate buttonTitle:(NSString *)buttonTitle
{
    self.frame = parentView.frame;
    self.delegate = delegate;
    [parentView addSubview:self];
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.frame];
    bgView.backgroundColor = [UIColor alertBackgroundColor];
    bgView.alpha = 0.50;
    [self addSubview:bgView];
    
    self.alertBodyView = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-272)/2, (self.frame.size.height-180)/2, 272, 176)];
    self.alertBodyView.backgroundColor = [UIColor whiteColor];
    self.alertBodyView.alpha = 1.0;
    self.alertBodyView.layer.cornerRadius = 8;
    [self addSubview:self.alertBodyView];
    
    [self addAlertBodyWithMessage:message buttonTitle:buttonTitle];
}

-(void)addAlertBodyWithMessage:(NSString *)message buttonTitle:(NSString *)buttonTitle
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0]};
    CGSize constrintSize = CGSizeMake(self.alertBodyView.frame.size.width-25, self.alertBodyView.frame.size.height);
    CGRect rect = [message boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGFloat height = 20+15+15+1+44+20+rect.size.height;
    if(height > 176) {
        CGRect alertFrame = self.alertBodyView.frame;
        alertFrame.size.height = height;
        self.alertBodyView.frame = alertFrame;
    }
    
    // uilabel with message
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((self.alertBodyView.frame.size.width-rect.size.width)/2, 20, rect.size.width, rect.size.height)];
    label.text = message;
    label.font = [UIFont systemFontOfSize:14.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    [label sizeToFit];
    label.backgroundColor = [UIColor clearColor];
    [self.alertBodyView addSubview:label];
    
    //rate view
    self.rateView = [[TMRateView alloc] initWithFrame:CGRectMake(20, label.frame.origin.y+label.frame.size.height+15, self.alertBodyView.frame.size.width-40, 22) fullStar:[UIImage imageNamed:@"StarFull"] emptyStar:[UIImage imageNamed:@"StarEmpty"]];
    self.rateView.padding = 20;
    self.rateView.alignment = RateViewAlignmentCenter;
    self.rateView.editable = YES;
    self.rateView.delegate = self;
    self.rateView.backgroundColor = [UIColor clearColor];
    [self.alertBodyView addSubview:self.rateView];
    
    //horizontal separator view
    UIView *hView = [[UIView alloc] initWithFrame:CGRectMake(0, self.alertBodyView.frame.size.height-45, self.alertBodyView.frame.size.width, 0.5)];
    hView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.25];//[UIColor lightGrayColor];
    [self.alertBodyView addSubview:hView];
    
    //vertical separator view
    UIView *vView = [[UIView alloc] initWithFrame:CGRectMake((self.alertBodyView.frame.size.width-0.5)/2, hView.frame.origin.y+hView.frame.size.height, 0.5, 44)];
    vView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.25];//[UIColor lightGrayColor];
    [self.alertBodyView addSubview:vView];
    
    //cancel button
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancelButton.frame = CGRectMake(0, hView.frame.origin.y+hView.frame.size.height, vView.frame.origin.x, 44);
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18.0];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(didCancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.alertBodyView addSubview:cancelButton];
    
    //Other button
    UIButton *otherButton = [UIButton buttonWithType:UIButtonTypeSystem];
    otherButton.frame = CGRectMake(vView.frame.origin.x+vView.frame.size.width, hView.frame.origin.y+hView.frame.size.height, vView.frame.origin.x+1, vView.frame.size.height);
    otherButton.titleLabel.font = [UIFont systemFontOfSize:18.0];
    [otherButton setTitle:buttonTitle forState:UIControlStateNormal];
    [otherButton addTarget:self action:@selector(didActionButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.alertBodyView addSubview:otherButton];

}

-(void)didCancelButtonPressed
{
    if ([self.delegate respondsToSelector:@selector(cancelButtonPressed)]) {
        [self.delegate cancelButtonPressed];
    }
    [self dismissCustomAlertViewUI];
}

- (void)didActionButtonPressed
{
    if ([self.delegate respondsToSelector:@selector(actionButtonPressed:)]) {
        [self.delegate actionButtonPressed:self.rate];
    }
    [self dismissCustomAlertViewUI];
}

#pragma mark - Dismiss UI method

- (void) dismissCustomAlertViewUI
{
    self.delegate = nil;
    [self removeFromSuperview];
}

#pragma mark - TMRateViewDelegate

- (void)rateView:(TMRateView *)rateView changedToNewRate:(NSNumber *)rate {
    self.rate = rate;
    //self.rateLabel.text = [NSString stringWithFormat:@"Rate: %d", rate.intValue];
}



@end

//
//  TMProfileVisibilityView.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 05/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMProfileVisibilityViewDelegate;

@interface TMProfileVisibilityView : UIView

@property(nonatomic,weak)id<TMProfileVisibilityViewDelegate> delegate;

-(instancetype)initWithFrame:(CGRect)frame showPrivacy:(BOOL)showPrivacy;
-(void)showOptionsToUploadPhoto;
-(void)showLoader;
-(void)fillImage:(UIImage*)image;
-(void)sendImageToServer:(UIImage*)image;

@end

@protocol TMProfileVisibilityViewDelegate <NSObject>

-(void)didCancelVisibilityNudge:(BOOL)showPrivacy;
-(void)didPhotoClick;
-(void)didUploadPhotoSuccessfully;
-(void)addPhotoEditorWithImage:(UIImage*)image;

@end
//
//  TMProfileVisibilityView.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 05/10/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMProfileVisibilityView.h"
#import "TMPhotoManager.h"
#import "TMUserSession.h"
#import "TMSettingsManager.h"
#import "TMDataStore.h"
#import "TMToastView.h"
#import "UIImage+TMAdditions.h"
#import "NSString+TMAdditions.h"
#import "TMAnalytics.h"
#import "DZNPhotoEditorViewController.h"

#define IS_PHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_PHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568.0)

@interface TMProfileVisibilityView ()<UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIView* profileVisibilityActionView;
@property (nonatomic, strong) UIView* profileVisbilityControlView;
@property (nonatomic, strong) UIButton* closeButton;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;
@property (nonatomic, assign) BOOL didShowActionSheet;
@property (nonatomic, strong) UIView *actionView;
@property (nonatomic, assign) BOOL isPhotoSelected;
@property (nonatomic, assign) BOOL isEveryoneSelected;
@property (nonatomic, assign) BOOL isOnlyMyLikeSelected;
@property (nonatomic, strong) UITableView *controlView;
@property (nonatomic, assign) BOOL isPhotoUploading;
@property (nonatomic, strong) UILabel *profileControlMessageLabel;
@property (nonatomic, strong) NSMutableDictionary* eventDict;
@property (nonatomic, assign) BOOL showPrivacy;
@property (nonatomic, assign) BOOL showPhotoAction;

@property(nonatomic, strong)CAShapeLayer *border;

@end

@implementation TMProfileVisibilityView


- (instancetype) initWithFrame:(CGRect)frame showPrivacy:(BOOL)showPrivacy
{
    if (self = [super initWithFrame:frame]) {
        self.showPrivacy = showPrivacy;
        self.showPhotoAction = !showPrivacy;
        self.backgroundColor = [UIColor whiteColor];
        [self createProfileVisiblityScreenSubviews];
    }
    return self;
}

- (void) createProfileVisiblityScreenSubviews
{
    self.didShowActionSheet = false;
    self.isPhotoSelected = false;
    self.isEveryoneSelected = false;
    self.isOnlyMyLikeSelected = false;
    self.isPhotoUploading = false;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
    [self addSubview:self.scrollView];
    
    //layout the profile visibility action view
    self.profileVisibilityActionView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.bounds.size.width-20, self.bounds.size.width-20)];
    self.profileVisibilityActionView.backgroundColor = [UIColor colorWithRed:0.973 green:0.973 blue:0.973 alpha:1.0];
    _border = [CAShapeLayer layer];
    _border.strokeColor = [UIColor colorWithRed:0.573 green:0.698 blue:0.835 alpha:1.0].CGColor;
    _border.fillColor = nil;
    _border.lineDashPattern = @[@4, @2];
    [self.profileVisibilityActionView.layer addSublayer:_border];
    
    [self createProfileVisibilityActionViewSubViews];
    [self.scrollView addSubview:self.profileVisibilityActionView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.profileVisibilityActionView addGestureRecognizer:tapGesture];
    
    //layout the profile visibility control view
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.bounds.size.width-20, NSUIntegerMax);
    CGRect rect = [@"Who can see my pic?" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    self.profileVisbilityControlView = [[UIView alloc] initWithFrame:CGRectMake(10, self.profileVisibilityActionView.frame.origin.y+self.profileVisibilityActionView.frame.size.height+12, self.bounds.size.width-20, 88+rect.size.height+10)];
    self.profileVisbilityControlView.backgroundColor = [UIColor colorWithRed:0.973 green:0.973 blue:0.973 alpha:1.0];
    self.profileVisbilityControlView.layer.cornerRadius = 4;
    
    [self createProfileVisibilieyControlViewSubviews];
    [self.scrollView addSubview:self.profileVisbilityControlView];
    
    //layout the profile visibility close button
    CGFloat yPos = self.profileVisbilityControlView.frame.origin.y+self.profileVisbilityControlView.frame.size.height;
    CGFloat remainingHeight = self.frame.size.height-yPos;
    CGFloat yClose = (IS_PHONE_4 || IS_PHONE_5) ? yPos+20 : yPos+(remainingHeight-44)/2;
    self.closeButton = [[UIButton alloc] initWithFrame:CGRectMake(10, yClose, self.bounds.size.width-20, 44)];
    [self.closeButton setTitleColor:[UIColor colorWithRed:0 green:0.357 blue:0.847 alpha:1.0] forState:UIControlStateNormal];
    [self.closeButton setTitle:@"Skip" forState:UIControlStateNormal];
    self.closeButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    self.closeButton.backgroundColor = [UIColor whiteColor];
    self.closeButton.layer.cornerRadius = 6;
    [self.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.closeButton];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, yClose+50);
    
}


-(void)layoutSubviews
{
    _border.path = [UIBezierPath bezierPathWithRect:self.profileVisibilityActionView.bounds].CGPath;
    _border.frame = self.profileVisibilityActionView.bounds;
    
    // open action sheet
    if(self.showPhotoAction){
        self.showPhotoAction = false;
        [self tapAction];
    }
}


- (void) createProfileVisibilityActionViewSubViews
{
    //create the action button
    
    int profileVisibilityActionButtonSide = self.profileVisibilityActionView.bounds.size.width*0.4;
    
    UIFont *font = [UIFont systemFontOfSize:20];
    
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.profileVisibilityActionView.bounds.size.width, NSUIntegerMax);
    CGRect rect = [@"Add Profile Pic" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGFloat xPos = (self.profileVisibilityActionView.frame.size.width - profileVisibilityActionButtonSide)/2;
    CGFloat yPos = (self.profileVisibilityActionView.frame.size.height-(profileVisibilityActionButtonSide + rect.size.height+5))/2;
    
    self.actionView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, profileVisibilityActionButtonSide, profileVisibilityActionButtonSide)];
    self.actionView.backgroundColor = [UIColor clearColor];
    [self.profileVisibilityActionView addSubview:self.actionView];
    
    UIImageView *cameraImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.actionView.frame.size.width, self.actionView.frame.size.height)];
    cameraImage.image = [UIImage imageNamed:@"camera"];
    cameraImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.actionView addSubview:cameraImage];
    
    //text
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.actionView.frame.origin.x+(self.actionView.frame.size.width-rect.size.width)/2, self.actionView.frame.origin.y+self.actionView.frame.size.height+5, rect.size.width, rect.size.height)];
    self.textLabel.text = @"Add Profile Pic";
    self.textLabel.font = font;
    self.textLabel.textColor = [UIColor colorWithRed:0.573 green:0.698 blue:0.835 alpha:1.0];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    [self.profileVisibilityActionView addSubview:self.textLabel];
    
}

-(void)tapAction {
    if( !(self.didShowActionSheet || self.isPhotoUploading) ) {
        [self showOptionsToUploadPhoto];
    }
}

-(void)showOptionsToUploadPhoto
{
    [self.delegate didPhotoClick];
}

- (void) createProfileVisibilieyControlViewSubviews
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0];
    
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.bounds.size.width-20, NSUIntegerMax);
    CGRect rect = [@"Who can see my pic?" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    //create the label
    self.profileControlMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, self.profileVisbilityControlView.bounds.size.width, rect.size.height)];
    self.profileControlMessageLabel.font = font;
    self.profileControlMessageLabel.text = @"Who can see my pic?";
    self.profileControlMessageLabel.backgroundColor = [UIColor clearColor];
    if (!self.isPhotoSelected) {
        self.profileControlMessageLabel.textColor = [UIColor colorWithRed:0.733 green:0.733 blue:0.733 alpha:1.0];
    }else {
        self.profileControlMessageLabel.textColor = [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1.0];
    }
    [self.profileVisbilityControlView addSubview:self.profileControlMessageLabel];
    
    // add the table view
    self.controlView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.profileControlMessageLabel.frame.origin.y+self.profileControlMessageLabel.frame.size.height+5, self.profileVisbilityControlView.bounds.size.width, 87) style:UITableViewStylePlain];
    [self.controlView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"uiText"];
    self.controlView.scrollEnabled = false;
    self.controlView.tintColor = [UIColor colorWithRed:49.0/255.0 green:112.0/255.0 blue:253.0/255.0 alpha:1.0];
    self.controlView.delegate = self;
    self.controlView.dataSource = self;
    [self.controlView setSeparatorColor:[UIColor colorWithRed:0.925 green:0.925 blue:0.925 alpha:1.0]];
    [self.profileVisbilityControlView addSubview:self.controlView];
    self.controlView.backgroundColor = [UIColor colorWithRed:0.973 green:0.973 blue:0.973 alpha:1.0];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uiText"];
    
    if(indexPath.row == 0) {
        cell.textLabel.text = @"All my matches";
        cell.backgroundColor = [UIColor clearColor];
        if(self.isEveryoneSelected) {
            cell.imageView.image = [UIImage imageNamed:@"all_matches"];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else {
            cell.imageView.image = [UIImage imageNamed:@"all_matches_disabled"];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else {
        cell.textLabel.text = @"Only the matches I \'like\'";
        cell.backgroundColor = [UIColor clearColor];
        if(self.isOnlyMyLikeSelected) {
            cell.imageView.image = [UIImage imageNamed:@"only_my_matches"];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.imageView.image = [UIImage imageNamed:@"only_my_matches_disabled"];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    if (!self.isPhotoSelected) {
        cell.textLabel.textColor = [UIColor colorWithRed:0.733 green:0.733 blue:0.733 alpha:1.0];
    }else {
        cell.textLabel.textColor = [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1.0];
    }
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isPhotoSelected) {
        if(indexPath.row == 0) {
            if(!self.isEveryoneSelected) {
                self.isOnlyMyLikeSelected = false;
                self.isEveryoneSelected = true;
                [tableView reloadData];
            }
        }
        else {
            if(!self.isOnlyMyLikeSelected) {
                self.isEveryoneSelected = false;
                self.isOnlyMyLikeSelected = true;
                [tableView reloadData];
                if(self.isOnlyMyLikeSelected) {
                    [TMToastView showToastInParentView:self withText:@"Your profile is now visible only to the ones you like. You can change this in your Settings." withDuaration:3.0 presentationDirection:TMToastPresentationFromBottom];
                }
            }
        }
    }
    else {
        // show nudge
        [TMToastView showToastInParentView:self withText:@"To change your profile visibility setting, upload a picture first." withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
    }
}

- (void) closeButtonPressed:(UIButton*) closeButtonPressed
{
    //TODO:: close button action
    if(self.isEveryoneSelected || self.isOnlyMyLikeSelected) {
        [self updateProfileVisibility];
    }
    else {
        [self.delegate didCancelVisibilityNudge:self.showPrivacy];
    }
}

-(void)showLoader {
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:20]};
    CGSize constrintSize = CGSizeMake(self.profileVisibilityActionView.bounds.size.width, NSUIntegerMax);
    CGRect rect = [@"Uploading Profile Pic" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    self.textLabel.text = @"Uploading Profile Pic";
    self.actionView.hidden = true;
    if(self.activityIndicator == nil) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhite];
    }
    
    self.activityIndicator.color = [UIColor colorWithRed:0.463 green:0.463 blue:0.463 alpha:1.0];
    [self.profileVisibilityActionView addSubview:self.activityIndicator];
    
    CGRect activityFrame = self.activityIndicator.frame;
    activityFrame.size.width = 20;
    activityFrame.size.height = 20;
    activityFrame.origin.x = (self.profileVisibilityActionView.frame.size.width-activityFrame.size.width)/2;
    activityFrame.origin.y = (self.profileVisibilityActionView.frame.size.height-(activityFrame.size.height+23))/2;
    self.activityIndicator.frame = activityFrame;
    self.activityIndicator.backgroundColor = [UIColor clearColor];
    [self.profileVisibilityActionView bringSubviewToFront:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
    
    CGRect textLabelFrame = self.textLabel.frame;
    textLabelFrame.origin.x = self.actionView.frame.origin.x+(self.actionView.frame.size.width-rect.size.width)/2;
    textLabelFrame.origin.y = self.activityIndicator.frame.origin.y + self.activityIndicator.frame.size.height+5;
    textLabelFrame.size.width = rect.size.width;
    self.textLabel.frame = textLabelFrame;

}

-(void)fillImage:(UIImage *)image {
    
    self.isPhotoSelected = true;
    self.didShowActionSheet = true;
    
    [TMUserSession sharedInstance].user.isFemaleProfileRejected = NO;

    [self.border removeFromSuperlayer];

    self.closeButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
    [self.closeButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
    [self.closeButton setTitle:@"Done" forState:UIControlStateNormal];
    
    self.profileControlMessageLabel.textColor = [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1.0];
    
    if(self.activityIndicator) {
        [self.activityIndicator removeFromSuperview];
        self.activityIndicator = nil;
    }
    
    for (UIView *view in self.profileVisibilityActionView.subviews) {
        [view removeFromSuperview];
    }
    
    UIImageView *fullImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.profileVisibilityActionView.frame.size.width, self.profileVisibilityActionView.frame.size.height)];
    [self.profileVisibilityActionView addSubview:fullImage];
    [self.profileVisibilityActionView bringSubviewToFront:fullImage];
    
    fullImage.contentMode = UIViewContentModeScaleAspectFill;
    UIImage *downloadedImage = [image scaleImageToSize:CGSizeMake(fullImage.frame.size.width, fullImage.frame.size.height)];
    fullImage.image = downloadedImage;
    fullImage.clipsToBounds = YES;
    
    self.isEveryoneSelected = true;
    
    [self.controlView reloadData];
    
    [self trackEvent:@"photo_upload"];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.delegate addPhotoEditorWithImage:image];
    }];
    
}

-(void)sendImageToServer:(UIImage*)image {
    self.didShowActionSheet = false;
    TMPhotoManager *photoMgr = [[TMPhotoManager alloc] init];
    
    if([photoMgr isNetworkReachable]) {
        
        [self showLoader];
        
        self.closeButton.enabled = false;
        self.controlView.userInteractionEnabled = false;
        
        self.isPhotoUploading = true;
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
       // NSLog(@"Edited File size is : %.2f KB",(float)imageData.length/1024.0f);
        CGFloat size = (float)imageData.length/1024.0f;
        if(size >= 1024) {
            imageData = UIImageJPEGRepresentation(image, 0.5);
           // NSLog(@"Edited File size after compress is : %.2f KB",(float)imageData.length/1024.0f);
        }

        [self.closeButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
        
        [photoMgr uploadPhoto:imageData
                     response:^(TMPhotoResponse *photoResponse, TMError *error) {
                         self.closeButton.enabled = true;
                         self.controlView.userInteractionEnabled = true;
                         self.isPhotoUploading = false;
                         
                         if(photoResponse) {
                             [self.delegate didUploadPhotoSuccessfully];
                             [self fillImage:image];
                         }
                         else {
                             
                             [self.closeButton setTitleColor:[UIColor colorWithRed:0 green:0.357 blue:0.847 alpha:1.0] forState:UIControlStateNormal];
                             
                             if(self.activityIndicator) {
                                 [self.activityIndicator removeFromSuperview];
                                 self.activityIndicator = nil;
                             }
                             self.didShowActionSheet = false;
                             self.actionView.hidden = false;
                             
                             NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:20]};
                             CGSize constrintSize = CGSizeMake(self.profileVisibilityActionView.bounds.size.width, NSUIntegerMax);
                             CGRect rect = [@"Add Profile Pic" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
                             
                             self.textLabel.text = @"Add Profile Pic";
                             
                             CGRect textLabelFrame = self.textLabel.frame;
                             textLabelFrame.origin.x = self.actionView.frame.origin.x+(self.actionView.frame.size.width-rect.size.width)/2;
                             textLabelFrame.origin.y = self.actionView.frame.origin.y + self.actionView.frame.size.height+5;
                             textLabelFrame.size.width = rect.size.width;
                             self.textLabel.frame = textLabelFrame;
                         }
                     }];
    }
    else {
        self.isPhotoUploading = false;
        [TMToastView showToastInParentView:self withText:@"Could not upload due to connectivity issue." withDuaration:4.0 presentationDirection:TMToastPresentationFromBottom];
    }
}

-(void)addLoader {
    if(self.activityIndicator == nil) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhite];
    }
    
    self.activityIndicator.color = [UIColor colorWithRed:0.463 green:0.463 blue:0.463 alpha:1.0];
    [self addSubview:self.activityIndicator];
    CGRect activityFrame = self.activityIndicator.frame;
    activityFrame.size.width = 20;
    activityFrame.size.height = 20;
    activityFrame.origin.x = (self.closeButton.frame.size.width-(20+90+5))/2;
    activityFrame.origin.y = (self.closeButton.frame.origin.y+11);
    self.activityIndicator.frame = activityFrame;
    self.activityIndicator.backgroundColor = [UIColor clearColor];
    [self bringSubviewToFront:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
    
    self.closeButton.backgroundColor = [UIColor colorWithRed:0.902 green:0.906 blue:0.91 alpha:1.0];
    [self.closeButton setTitleColor:[UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0] forState:UIControlStateNormal];
    [self.closeButton setTitle:@"Updating" forState:UIControlStateNormal];
    
}

-(void)updateProfileVisibility {
    BOOL isDiscoveryOn = false;
    if(self.isEveryoneSelected) {
        isDiscoveryOn = false;
    }else if(self.isOnlyMyLikeSelected) {
        isDiscoveryOn = true;
    }
    TMSettingsManager *settingsMngr = [[TMSettingsManager alloc] init];
    
    if([settingsMngr isNetworkReachable]) {
        NSMutableDictionary *queryString = [NSMutableDictionary dictionaryWithCapacity:16];
        NSNumber *boolNumber = [NSNumber numberWithBool:isDiscoveryOn];
        [queryString setObject:boolNumber forKey:@"is_discovery_on"];
        
        self.closeButton.enabled = false;
        self.controlView.userInteractionEnabled = false;
        
        [self addLoader];
        
        [settingsMngr updateProfileDiscovery:queryString with:^(BOOL status, TMError *error) {
            self.closeButton.enabled = true;
            self.controlView.userInteractionEnabled = true;
            
            if(self.activityIndicator) {
                [self.activityIndicator removeFromSuperview];
                self.activityIndicator = nil;
            }
            
            if(status) {
                if(!isDiscoveryOn) {
                    if([TMDataStore containsObjectForKey:PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP]) {
                        NSTimeInterval currentTimeInterval = [NSDate timeIntervalSinceReferenceDate];
                        NSNumber *myDouble = [NSNumber numberWithDouble:currentTimeInterval];
                        [TMDataStore setObject:myDouble forKey:PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP];
                        [TMDataStore synchronize];
                    }
                }
                [TMDataStore setBool:!isDiscoveryOn forKey:@"is_discovery_on"];
                [TMDataStore synchronize];
                
                [self.delegate didCancelVisibilityNudge:false];
            }
            else {
                self.closeButton.backgroundColor = [UIColor colorWithRed:1.0 green:0.702 blue:0.725 alpha:1.0];
                [self.closeButton setTitleColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
                [self.closeButton setTitle:@"Done" forState:UIControlStateNormal];
                
                [self showAlertWithTitle:@"Oops! Couldn't update due to connectivity issues. Want to try again?" msg:@"" withDelegate:self];
            }
        }];
    }else {
        [self showAlertWithTitle:@"Oops! Couldn't update due to connectivity issues. Want to try again?" msg:@"" withDelegate:self];
    }
}


-(void)showAlertWithTitle:(NSString*)title
                      msg:(NSString*)msg
             withDelegate:(id)delegate  {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:@"No" otherButtonTitles:nil, nil];
        
        [alertView addButtonWithTitle:@"Yes"];
        
        [alertView show];
    });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    if([buttonTitle isEqualToString:@"No"]) {
        [self.delegate didCancelVisibilityNudge:false];
    }
}

-(void)trackEvent:(NSString *)event_type
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMProfileVisibilityView" forKey:@"screenName"];
    [eventDict setObject:@"photo_flow" forKey:@"eventCategory"];
    [eventDict setObject:event_type forKey:@"eventAction"];
    [eventDict setObject:@"success" forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}

@end
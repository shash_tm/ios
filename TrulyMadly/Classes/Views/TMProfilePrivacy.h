//
//  TMProfilePrivacy.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 12/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMProfilePrivacyDelegate;

@interface TMProfilePrivacy : UIView

@property(nonatomic,weak)id<TMProfilePrivacyDelegate> delegate;

@end

@protocol TMProfilePrivacyDelegate <NSObject>

-(void)didAddProfileClick;
-(void)didSkipClick;

@end

//
//  TMProfileVisibilityAlertView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 18/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfileVisibilityAlertView.h"
#import "NSString+TMAdditions.h"

@interface TMProfileVisibilityAlertView ()

@property(nonatomic, strong)NSString *message;
@property(nonatomic, assign)BOOL fromLike;
//@property(nonatomic, strong)NSTimer *autoCompleteTimer;

@end

@implementation TMProfileVisibilityAlertView

-(instancetype)initWithMessage:(NSString *)msg frame:(CGRect)frame fromLike:(BOOL)fromLike
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self.fromLike = fromLike;
        self.message = msg;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapGesture];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tapAction) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
       // self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(tapAction) userInfo:nil repeats:NO];
        
        [self configureUI];
        
        return self;
    }
    return nil;
}

-(void)configureUI
{
    CGFloat imgTopMargin = 14;  CGFloat spaceBetweenImgAndText = 16;   CGFloat lineHeight = 0.4; CGFloat lineWidth = 1.0;
    CGFloat imgWidth = 50;
    CGFloat width = self.frame.size.width*0.67;
    
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(width-30, NSUIntegerMax);
    CGRect rect = [self.message boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGFloat height = imgTopMargin+imgWidth+spaceBetweenImgAndText+rect.size.height+spaceBetweenImgAndText+lineHeight+44;//imgWidth/2+25+rect.size.height;
    CGFloat yPos = (self.frame.size.height-height)/2;
    
    UIView *popView = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-width)/2, yPos, width, height)];
    popView.backgroundColor = [UIColor whiteColor];
    popView.layer.cornerRadius = 8;
    [self addSubview:popView];
    
    UIImageView *alertImageView = [[UIImageView alloc] initWithFrame:CGRectMake((width-imgWidth)/2, imgTopMargin, imgWidth, imgWidth)];
    alertImageView.image = [UIImage imageNamed:@"warning"];
    [popView addSubview:alertImageView];
    
    UILabel *textLbl = [[UILabel alloc] initWithFrame:CGRectMake((width-rect.size.width)/2, imgTopMargin+imgWidth+spaceBetweenImgAndText-1 , rect.size.width, rect.size.height)];
    textLbl.text = self.message;
    textLbl.textAlignment = NSTextAlignmentCenter;
    textLbl.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];//[UIColor colorWithRed:0.416 green:0.58 blue:0.757 alpha:1];
    textLbl.font = font;
    textLbl.numberOfLines = 0;
    [textLbl sizeToFit];
    [popView addSubview:textLbl];
    
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, textLbl.frame.origin.y+rect.size.height+spaceBetweenImgAndText, width, lineHeight)];
    horizontalLine.backgroundColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1.0];
    [popView addSubview:horizontalLine];
    
    if(self.fromLike){
        UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake((width-lineWidth)/2, horizontalLine.frame.origin.y+horizontalLine.frame.size.height, lineWidth, 44)];
        verticalLine.backgroundColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1.0];
        [popView addSubview:verticalLine];
        
        UIButton *notNowButton = [[UIButton alloc] initWithFrame:CGRectMake(0, horizontalLine.frame.origin.y+horizontalLine.frame.size.height, (width-lineWidth)/2, 44)];
        [notNowButton setTitleColor:[UIColor colorWithRed:0 green:0.541 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [notNowButton setTitle:@"Not Now" forState:UIControlStateNormal];
        notNowButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        notNowButton.backgroundColor = [UIColor clearColor];
        [notNowButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [popView addSubview:notNowButton];

        UIButton *gotItButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verticalLine.frame), horizontalLine.frame.origin.y+horizontalLine.frame.size.height, (width-lineWidth)/2, 44)];
        [gotItButton setTitleColor:[UIColor colorWithRed:0 green:0.541 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [gotItButton setTitle:@"Go Ahead" forState:UIControlStateNormal];
        gotItButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        gotItButton.backgroundColor = [UIColor clearColor];
        [gotItButton addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
        [popView addSubview:gotItButton];
        
    }else {
        UIButton *gotItButton = [[UIButton alloc] initWithFrame:CGRectMake(0, horizontalLine.frame.origin.y+horizontalLine.frame.size.height, width, 44)];
        [gotItButton setTitleColor:[UIColor colorWithRed:0 green:0.541 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [gotItButton setTitle:@"Got it!" forState:UIControlStateNormal];
        gotItButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        gotItButton.backgroundColor = [UIColor clearColor];
        [gotItButton addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
        [popView addSubview:gotItButton];
    }

}

-(void)tapAction
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    if(self.alertViewDelegate){
        [self.alertViewDelegate didRemoveView:self.fromLike];
    }
}

-(void)cancelAction {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    if(self.alertViewDelegate){
        [self.alertViewDelegate didNotNowClick];
    }
}


@end
//
//  TMCustomAlertView.m
//  TMCustomAlertView
//
//  Created by Abhijeet Mishra on 21/07/15.
//  Copyright (c) 2015 Abhijeet Mishra. All rights reserved.
//

#import "TMPhotoAlertView.h"
#import "TMAnalytics.h"

@interface TMPhotoAlertView ()

@property (weak, nonatomic) IBOutlet UILabel *topViewMessageLabel;
@property (weak, nonatomic) IBOutlet UIView *alertBodyView;
@property (strong, nonatomic) NSMutableDictionary* eventDict;

@end

@implementation TMPhotoAlertView


+ (void) customAlertViewWithMessage:(NSString*) message parentView:(UIView*) parentView delegate:(id<TMPhotoAlertViewDelegate>) delegate
{
    TMPhotoAlertView* customAlertView = (TMPhotoAlertView*)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    customAlertView.frame = parentView.frame;
    customAlertView.topViewMessageLabel.text = message;
    customAlertView.delegate = delegate;
    [parentView addSubview:customAlertView];
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        customAlertView.alertBodyView.transform = CGAffineTransformMakeScale(1.1, 1.1);
    } completion:^(BOOL completion){
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            customAlertView.alertBodyView.transform = CGAffineTransformMakeScale(1, 1);
        } completion:nil];
    }];
}

#pragma mark - TMPhotoAlertView removal methods

+ (void) removeCustomAlertViewFromView:(UIView*) baseView
{
    NSArray* subViews = baseView.subviews;
    
    for (int index = 0; index < subViews.count; index++) {
        if ([[subViews objectAtIndex:index] isKindOfClass:[TMPhotoAlertView class]]) {
            [[subViews objectAtIndex:index] removeFromSuperview];
        }
    }
}


#pragma mark - IBActions

- (IBAction)cancelButtonPressed:(UIButton*) sender {
    if ([self.delegate respondsToSelector:@selector(cancelButtonPressed)]) {
        [self.delegate cancelButtonPressed];
    }
    
    [self dismissCustomAlertViewUI];
}


- (IBAction)actionButtonPressed:(id) sender {
    if ([self.delegate respondsToSelector:@selector(actionButtonPressed)]) {
        [self.delegate actionButtonPressed];
    }
    
    [self dismissCustomAlertViewUI];
}

#pragma mark - Dismiss UI method

- (void) dismissCustomAlertViewUI
{
     self.delegate = nil;
    [self removeFromSuperview];
}

@end

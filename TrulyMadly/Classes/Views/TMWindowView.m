//
//  TMWindowView.m
//  TrulyMadly
//
//  Created by Ankit on 27/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMWindowView.h"

@implementation TMWindowView

- (instancetype)init {
    self = [super init];
    if (self) {
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        [keyWindow addSubview:self];
        
        self.backgroundColor = [UIColor clearColor];
        self.frame = keyWindow.bounds;
        
        [self attachBackgroudView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapActionOnWindowView)];
        [self.backgroundView addGestureRecognizer:tapGesture];
    }
    return self;
}
-(void)attachBackgroudView {
    self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
    self.backgroundView.backgroundColor = [UIColor blackColor];
    self.backgroundView.alpha = 0.6;
    [self addSubview:self.backgroundView];
}

-(void)deinit {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeFromSuperview];
    [self.backgroundView removeFromSuperview];
    self.backgroundView = nil;
}

-(void)tapActionOnWindowView {
    [self.delegate didDissmissWindowView];
}

@end

//
//  TMCustomActivity.h
//  TrulyMadly
//
//  Created by Ankit on 28/09/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCustomActivity : UIActivity

@property(nonatomic,strong)NSString *text;
@property(nonatomic,strong)NSString *link;

- (instancetype)initWithText:(NSString*)text link:(NSString*)link;

@end

//
//  TMNavigationImageView.m
//  TrulyMadly
//
//  Created by Ankit on 22/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNavigationImageView.h"
#import "UIImageView+AFNetworking.h"
#import "TMStringUtil.h"

#define PROFILEIMAGE_WIDTH  36
#define TITLELABEL_HEIGHT   20
#define XPOS_OffSET_BETWEEN_IMAGE_AND_LABEL   6


@interface TMNavigationImageView ()

@property(nonatomic,strong)UIView *matchContainerView;
@property(nonatomic,strong)UIView *connectionContainerView;
@property(nonatomic,strong)UIImageView *profileImageView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicatorView;
@property(nonatomic,strong)UILabel *connectionLabel;
@property(nonatomic,assign)BOOL isDataLoaded;
@property(nonatomic,assign)BOOL isConnecting;
@property(nonatomic,assign)CGRect infoRect;

@end

@implementation TMNavigationImageView

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if(self) {
        self.isDataLoaded = FALSE;
        self.isConnecting = FALSE;
        self.frame = CGRectZero;
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor clearColor];
        self.profileImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.profileImageView.image = [UIImage imageNamed:@"ProfileBlank"];
        self.profileImageView.layer.cornerRadius = PROFILEIMAGE_WIDTH/2;
        self.profileImageView.clipsToBounds = YES;
        self.profileImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.profileImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.backgroundColor = [UIColor clearColor];

        
        UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapgesture];
    }
    return self;
}

-(UIView*)connectionContainerView {
    if(!_connectionContainerView) {
        _connectionContainerView = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:_connectionContainerView];
    }
    return _connectionContainerView;
}
-(UIActivityIndicatorView*)activityIndicatorView {
    if(!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.frame = CGRectMake(0, 0, PROFILEIMAGE_WIDTH, PROFILEIMAGE_WIDTH);
        _activityIndicatorView.hidesWhenStopped = TRUE;
        [self.connectionContainerView addSubview:_activityIndicatorView];
    }
    return _activityIndicatorView;
}

-(UILabel*)connectionLabel {
    if(!_connectionLabel) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
        [dict setObject:[UIFont systemFontOfSize:14] forKey:@"kfont"];
        [dict setObject:[NSNumber numberWithFloat:NSUIntegerMax] forKey:@"kmaxwidth"];
        [dict setObject:@"Connecting" forKey:@"ktext"];
        
        CGSize finalSize = [TMStringUtil textRectForString:dict].size;
        _connectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(38,
                                                                     (PROFILEIMAGE_WIDTH-finalSize.height)/2,
                                                                     finalSize.width,
                                                                     finalSize.height)];
        _connectionLabel.font = [UIFont systemFontOfSize:14];
        _connectionLabel.text = @"Connecting";
        [self.connectionContainerView addSubview:_connectionLabel];
    }
    return _connectionLabel;
}
//self.frame = CGRectMake(0, 0, 90, PROFILEIMAGE_WIDTH);
-(void)setViewForConnectionModeWithText:(BOOL)showText {
    self.clipsToBounds = FALSE;
    self.isConnecting = TRUE;
    self.connectionContainerView.frame = CGRectMake(0, 0, 90, PROFILEIMAGE_WIDTH);
    if(showText) {
        self.connectionLabel.text = @"Connecting";
    }
    else {
        self.connectionLabel.text = nil;
        CGFloat xPos = (CGRectGetWidth(self.connectionContainerView.frame) - CGRectGetWidth(self.activityIndicatorView.frame))/2;
        //self.connectionContainerView.backgroundColor = [UIColor redColor];
       // NSLog(@"Self frame:%@",NSStringFromCGRect(self.frame));
        //NSLog(@"Self superview frame:%@",NSStringFromCGRect(self.superview.frame));
        self.activityIndicatorView.frame = CGRectMake(xPos+10,
                                                      CGRectGetMinY(self.activityIndicatorView.frame),
                                                      CGRectGetWidth(self.activityIndicatorView.frame),
                                                      CGRectGetWidth(self.activityIndicatorView.frame));
    }
    [self.activityIndicatorView startAnimating];
    
    self.profileImageView.hidden = TRUE;
    self.titleLabel.hidden = TRUE;
}
//self.frame = self.infoRect;
-(void)resetViewForConnectionMode {
    self.clipsToBounds = TRUE;
    self.isConnecting = FALSE;
    
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView = nil;
    [self.connectionLabel removeFromSuperview];
    self.connectionLabel = nil;
    [self.connectionContainerView removeFromSuperview];
    self.connectionContainerView = nil;
    
    self.profileImageView.hidden = FALSE;
    self.titleLabel.hidden = FALSE;
}

-(void)setTitleText:(NSString*)nameText withImageUrl:(NSString*)imageurl {
    if(!self.isDataLoaded) {
        self.isDataLoaded = TRUE;
        [self.profileImageView setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"ProfileBlank"]];
        
        //update frame
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
        [dict setObject:self.titleLabel.font forKey:@"kfont"];
        [dict setObject:[NSNumber numberWithFloat:NSUIntegerMax] forKey:@"kmaxwidth"];
        [dict setObject:nameText forKey:@"ktext"];
        CGSize finalSize = [TMStringUtil textRectForString:dict].size;
        
        //
        self.profileImageView.frame = CGRectMake(0,
                                                 0,
                                                 PROFILEIMAGE_WIDTH,
                                                 PROFILEIMAGE_WIDTH);
        
        //update label frame and text
        CGFloat xPosOffsetBetweenImageAndLabel = 6;
        CGFloat labelXPos = CGRectGetMaxX(self.profileImageView.frame) + xPosOffsetBetweenImageAndLabel;
        CGFloat labelHeight = 20;
        CGFloat labelWidth = finalSize.width;
        CGFloat labelYPos = (PROFILEIMAGE_WIDTH - labelHeight)/2;
        self.titleLabel.frame = CGRectMake(labelXPos,
                                           labelYPos,
                                           labelWidth,
                                           labelHeight);
        self.titleLabel.text = nameText;
        
        
        //update self frame
        CGFloat width =  CGRectGetWidth(self.profileImageView.frame) + CGRectGetWidth(self.titleLabel.frame) + xPosOffsetBetweenImageAndLabel;
        self.frame = CGRectMake(0, 0, width, PROFILEIMAGE_WIDTH);
        self.infoRect = self.frame;
        
        if(!self.titleLabel.superview) {
            [self addSubview:self.titleLabel];
        }
    }
}

-(void)tapAction {
    if(!self.isConnecting) {
        [self.delegate didTapToNavigationView];
    }
}

@end

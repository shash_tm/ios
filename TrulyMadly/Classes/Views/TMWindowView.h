//
//  TMWindowView.h
//  TrulyMadly
//
//  Created by Ankit on 27/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMWindowViewDelegate;

@interface TMWindowView : UIView

@property(nonatomic,weak)id<TMWindowViewDelegate> delegate;
@property(nonatomic,strong)UIView *backgroundView;

-(void)deinit;

@end

@protocol TMWindowViewDelegate <NSObject>

-(void)didDissmissWindowView;

@end
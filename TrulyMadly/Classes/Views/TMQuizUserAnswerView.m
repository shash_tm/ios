//
//  TMQuizUserAnswerCollectionView.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizUserAnswerView.h"
#import "TMQuizUserOptionTableViewCell.h"
#import "TMQuizUserOptionImageTableViewCell.h"
#import "TMQuizUserOptionSingleImageTableViewCell.h"
#import "TMQuizCommonAnswerTableViewCell.h"
#import "UserAnswerQuestionView.h"
#import "TMMessagesInputToolbar.h"
#import "UIImageView+AFNetworking.h"
#import "QuizQuestionDomainObject.h"
#import "TMMessagesToolbarContentView.h"
#import "TMQuizCommonQuestionTableViewCell.h"
#import "NSString+TMAdditions.h"
#import "TMAnalytics.h"

#define USER_ANSWER_OPTION_TABLE_VIEW_CELL @"userAnswerOptionTableViewCell"
#define USER_IMAGE_ANSWER_OPTION_TABLE_VIEW_CELL @"userImageAnswerOptionTableViewCell"
#define USER_IMAGE_SINGLE_ANSWER_OPTION_TABLE_VIEW_CELL @"userSingleImageAnswerOptionTableViewCell"
#define USER_ANSWER_QUESTION_TABLE_VIEW @"userAnswerQuestionView"
#define USER_COMMON_ANSWER_TABLE_VIEW_CELL @"userCommonAnswerTableViewCell"
#define USER_COMMON_QUESTION_TABLE_VIEW_CELL @"userCommonQuestionTableViewCell"

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)



@interface TMQuizUserAnswerView () <UITableViewDataSource, UITableViewDelegate, TMMessagesInputToolbarDelegate, UITextViewDelegate,UITextViewDelegate,TMKeyboardControllerDelegate>

//UI elements
@property (weak, nonatomic) IBOutlet UILabel *quizNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *quizImageView;
@property (weak, nonatomic) IBOutlet UITableView *userAnswerTableView;
@property (weak, nonatomic) IBOutlet TMMessagesInputToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIView *quizDescriptionView;
@property(nonatomic,strong) TMKeyboardController *keyboardController;

@property (weak, nonatomic) IBOutlet UIView *gradientBaseView;

//data and state objects
@property (strong, nonatomic) NSString* quizID;
@property (strong, nonatomic) NSString* quizName;
@property (assign, nonatomic) BOOL isFlare;
@property (strong, nonatomic) NSArray* questions;

//flare UI objects
@property (weak, nonatomic) IBOutlet UIView *flareBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *flareImageView;


//animatable keyboard objects
@property (weak, nonatomic) IBOutlet UIView *staticButtonBaseView;
@property (weak, nonatomic) IBOutlet UIButton *staticConversationButton;
@property (weak, nonatomic) IBOutlet UIButton *staticShareButton;

//total common answer count
@property (nonatomic, assign) int commonAnswerCount;

//tracking info
@property (nonatomic, strong) NSMutableDictionary* eventDict;

@end

@implementation TMQuizUserAnswerView

- (void) awakeFromNib {
    
    // Initialization code
   
    self.flareBackgroundView.layer.cornerRadius = self.flareBackgroundView.bounds.size.width/2;
    self.clipsToBounds = YES;
    
    self.toolBar.hidden = YES;
    self.staticButtonBaseView.hidden = NO;
    
    //add both the title and image to the static conversation button
    self.staticConversationButton.titleEdgeInsets = UIEdgeInsetsMake(0, IS_IPHONE_6_PLUS?4:IS_IPHONE_6?8:12, 0, 0);
    
    UIImageView* staticConversationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.staticConversationButton.bounds.size.height/3, self.staticConversationButton.bounds.size.height/3)];
    staticConversationImageView.image = [UIImage imageNamed:@"conversation_toolbar"];
    staticConversationImageView.contentMode = UIViewContentModeScaleAspectFit;
    staticConversationImageView.center = CGPointMake(([UIScreen mainScreen].bounds.size.height > 500)?self.staticConversationButton.bounds.size.width*2.3/5:self.staticConversationButton.bounds.size.width*2/5
                                                     , self.staticConversationButton.bounds.size.height/2);
    [self.staticConversationButton addSubview:staticConversationImageView];
    
    //add both the title and image to the static share button
    self.staticConversationButton.titleEdgeInsets = UIEdgeInsetsMake(0, IS_IPHONE_6_PLUS?40:IS_IPHONE_6?44:48, 0, 0);
    
    UIImageView* staticShareImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.staticShareButton.bounds.size.height/2, self.staticShareButton.bounds.size.height/2)];
    staticShareImageView.image = [UIImage imageNamed:@"sharescore"];
    staticShareImageView.contentMode = UIViewContentModeScaleAspectFit;
    staticShareImageView.center = CGPointMake(([UIScreen mainScreen].bounds.size.height > 500)?self.staticShareButton.bounds.size.width*4/10:self.staticShareButton.bounds.size.width*3.5/10
                                              , self.staticShareButton.bounds.size.height*4.5/10);
    [self.staticShareButton addSubview:staticShareImageView];
    
    self.staticShareButton.titleEdgeInsets = UIEdgeInsetsMake(0, IS_IPHONE_6_PLUS?40:IS_IPHONE_6?44:48, 0, 0);
    
    
    //add observer for keyboard hide notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willReceiveKeyboardDidHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


/**
 * Perform customizations for toolbar
 */
- (void) setUpToolBar
{
    [self.toolBar setUpToolBarForCommonAnswersWithFrame:self.bounds];
    self.toolBar.backgroundColor = [UIColor clearColor];
    self.toolBar.clipsToBounds = YES;
    self.toolBar.delegate = self;
    self.toolBar.isSendOptionEnabled = YES;
    self.toolBar.contentView.textView.placeHolder = NSLocalizedStringFromTable(@"Start your conversation", @"JSQMessages", @"Placeholder text for the message input text view");
    self.toolBar.contentView.textView.delegate = self;
    [self.toolBar toggleSendButtonEnabled];
    [self.toolBar setupCommonAnswerActionButtonWithMediaButtonStatus:YES];
    
    //setting the keyboard controller of delegate
    self.keyboardController = [[TMKeyboardController alloc] initWithTextView:self.toolBar.contentView.textView contextView:self panGestureRecognizer:self.userAnswerTableView.panGestureRecognizer tapGestureRecognizer:nil delegate:self];
    [self.keyboardController beginListeningForKeyboard];
}

#pragma mark - TMMessagesInputToolbarDelegate delegate methods

- (void)messagesInputToolbar:(TMMessagesInputToolbar*) toolbar
      didPressRightBarButton:(UIButton *)sender
{
    //send the message
    //temp disabling to prevent crashing
   [self.quizController sendCommonAnswerWithMessageText:self.toolBar.contentView.textView.text quizID:self.quizID isFlare:self.isFlare];
    
    //dismiss the keyboard
    [self.toolBar.contentView.textView resignFirstResponder];
    
    //dismiss the self view
    [self.delegate commonAnswerMessageSendingButtonPressed];
}

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
       didPressLeftBarButton:(UIButton *)sender
{
    
}

- (void)messagesInputToolbar:(TMMessagesInputToolbar *)toolbar
   didPressLeftQuizBarButton:(UIButton *)sender
{
    
}

- (void)messagesInputToolbar:(TMMessagesInputToolbar*) toolbar
 didPressRightShareBarButton:(UIButton*) sender
{
    [self shareScore];
}

- (IBAction)showKeyboardButtonPressed:(id)sender {
    self.toolBar.hidden = NO;
    
    //self.toolBar.transform = CGAffineTransformMakeScale(0, 0);
    
  //  int finalShareButtonWidth = self.bounds.size.width/5;
    
    self.toolBar.frame = CGRectMake(self.toolBar.frame.origin.x, self.toolBar.frame.origin.y, self.bounds.size.width/2, self.toolBar.bounds.size.height);
   
    CGFloat originalWidth = self.staticShareButton.bounds.size.width;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.toolBar.frame = CGRectMake(self.toolBar.frame.origin.x, self.toolBar.frame.origin.y, self.bounds.size.width, self.toolBar.bounds.size.height);
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.staticShareButton.frame = CGRectMake(self.bounds.size.width - originalWidth*3/5, self.staticShareButton.bounds.origin.y, originalWidth*3/5, self.staticShareButton.bounds.size.height);
        //  animatableView.transform = CGAffineTransformIdentity;
        //        self.dynamicShareButton.frame = CGRectMake(self.bounds.size.width - finalShareButtonWidth, self.dynamicShareButton.frame.origin.y, finalShareButtonWidth, self.dynamicShareButton.bounds.size.height);
        //        self.dynamicConversationView.frame = CGRectMake(self.dynamicConversationView.frame.origin.x, self.dynamicConversationView.frame.origin.y, self.bounds.size.width - finalShareButtonWidth, self.dynamicConversationView.bounds.size.height);
    } completion:^(BOOL completed){
        [self.staticShareButton setTitle:nil forState:UIControlStateNormal];
        [self.staticShareButton setBackgroundColor:[UIColor colorWithRed:252.0/255.0 green:179.0/255.0 blue:184.0/255.0 alpha:1.0]];
        [UIView animateWithDuration:0.2 animations:^{
            self.staticShareButton.frame = CGRectMake(self.bounds.size.width - originalWidth*2/5, self.staticShareButton.bounds.origin.y, originalWidth*2/5, self.staticShareButton.bounds.size.height);
        } completion:^(BOOL completed){
            self.staticShareButton.hidden = YES;
            self.staticConversationButton.hidden = YES;
        }];
    }];
}

- (void) fillUserAnswerDetailsWithQuizID:(NSString*) quizID quizName:(NSString*) quizName quizImageURL:(NSString*) quizImageURL quizQuestionList:(NSArray*) questionList isFlare:(BOOL) isFlare
{
    //setting the delegate and datasource
    self.userAnswerTableView.delegate = self;
    self.userAnswerTableView.dataSource = self;
    self.userAnswerTableView.showsVerticalScrollIndicator = NO;
    self.userAnswerTableView.showsHorizontalScrollIndicator = NO;
    
    self.flareBackgroundView.hidden = !isFlare;
    self.flareImageView.hidden = !isFlare;
    
    self.quizName = quizName;
    
    //setting the quiz title
    [self createTitleViewWithQuizName:self.quizName andQuizImageViewURL:quizImageURL];
    
    //saving quiz ID data
    self.quizID = quizID;
    
    //saving the quiz questions
    self.questions = questionList;
    
    //set up the profile image view
    self.quizImageView.layer.cornerRadius = self.quizImageView.bounds.size.width/2;
    self.quizImageView.clipsToBounds = YES;
    
    //register table view cells
    [self registerTableViewCells];
    
    //calculate the common answer score
    [self calculateCommonAnswerScore];
}

- (void) calculateCommonAnswerScore
{
    NSArray* questionsArray = self.questions;
    
    for (int index = 0; index < questionsArray.count; index++) {
        QuizOptionDomainObject* userAnswer = ((QuizQuestionDomainObject*)[self.questions objectAtIndex:index]).userAnswer;
        
        QuizOptionDomainObject* matchAnswer = ((QuizQuestionDomainObject*)[self.questions objectAtIndex:index]).matchAnswer;
        
        if ((userAnswer.optionText.length && matchAnswer.optionText.length && [userAnswer.optionText isEqualToString:matchAnswer.optionText]) || (userAnswer.optionImageURL.length && matchAnswer.optionImageURL.length && [userAnswer.optionImageURL isEqualToString:matchAnswer.optionImageURL])) {
            self.commonAnswerCount++;
        }
    }
}

- (void) registerTableViewCells
{
    [self.userAnswerTableView registerClass:[TMQuizUserOptionTableViewCell class] forCellReuseIdentifier:USER_ANSWER_OPTION_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerClass:[TMQuizUserOptionImageTableViewCell class] forCellReuseIdentifier:USER_IMAGE_ANSWER_OPTION_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerClass:[TMQuizUserOptionSingleImageTableViewCell class] forCellReuseIdentifier:USER_IMAGE_SINGLE_ANSWER_OPTION_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerClass:[TMQuizCommonAnswerTableViewCell class] forCellReuseIdentifier:USER_COMMON_ANSWER_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerClass:[TMQuizCommonQuestionTableViewCell class] forCellReuseIdentifier:USER_COMMON_QUESTION_TABLE_VIEW_CELL];
    
    [self.userAnswerTableView registerNib:[UINib nibWithNibName:@"TMQuizUserOptionTableViewCell" bundle:nil] forCellReuseIdentifier:USER_ANSWER_OPTION_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerNib:[UINib nibWithNibName:@"TMQuizUserOptionImageTableViewCell" bundle:nil] forCellReuseIdentifier:USER_IMAGE_ANSWER_OPTION_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerNib:[UINib nibWithNibName:@"TMQuizUserOptionSingleImageTableViewCell" bundle:nil] forCellReuseIdentifier:USER_IMAGE_SINGLE_ANSWER_OPTION_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerNib:[UINib nibWithNibName:@"TMQuizCommonAnswerTableViewCell" bundle:nil] forCellReuseIdentifier:USER_COMMON_ANSWER_TABLE_VIEW_CELL];
    [self.userAnswerTableView registerNib:[UINib nibWithNibName:@"TMQuizCommonQuestionTableViewCell" bundle:nil] forCellReuseIdentifier:USER_COMMON_QUESTION_TABLE_VIEW_CELL];
}

- (void) createTitleViewWithQuizName:(NSString*) quizName andQuizImageViewURL:(NSString*) quizImageURL
{
    //setting the quiz name
    self.quizNameLabel.text = quizName;
    [self.quizNameLabel sizeToFit];
    
    //setting the quiz image
    self.quizImageView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin
                                             | UIViewAutoresizingFlexibleHeight
                                             | UIViewAutoresizingFlexibleLeftMargin
                                             | UIViewAutoresizingFlexibleRightMargin
                                             | UIViewAutoresizingFlexibleTopMargin
                                             | UIViewAutoresizingFlexibleWidth);
    self.quizImageView.clipsToBounds = YES;
    self.quizImageView.layer.cornerRadius = self.quizImageView.bounds.size.height/2;
    self.quizImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.quizImageView setImageWithURL:[NSURL URLWithString:quizImageURL]];
}


/**
 * get the option type for the current question
 * @return either imageAll type or remaining type
 */
- (optionType) getOptionTypeForQuestion:(QuizQuestionDomainObject*) questionDomainObject
{
    QuizOptionDomainObject* userOption = questionDomainObject.userAnswer;
    
    QuizOptionDomainObject* matchOption = questionDomainObject.matchAnswer;
    
    if ((userOption.optionText && userOption.optionText.length > 0) || (matchOption.optionText && matchOption.optionText.length > 0)) {
        //option type text present
        return optionTypeOthers;
    }
    else {
        return optionTypeAllImages;
    }
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.questions.count*2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    //return self.questions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row%2 == 0) {
        
        QuizQuestionDomainObject* questionDomainObject = [self.questions objectAtIndex:indexPath.row/2];
        
        CGFloat labelWidth = IS_IPHONE_6_PLUS?(328):(IS_IPHONE_6?300:270);
        
        CGRect requiredSize = [questionDomainObject.questionText boundingRectWithConstraintSize:CGSizeMake(labelWidth, CGFLOAT_MAX) attributeDictionary:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}];
         return requiredSize.size.height + 16;
    }
    else {
        
        BOOL isTextOption = NO;
        
        int questionNo = (int)(indexPath.row/2);
        
        if (((QuizQuestionDomainObject*)[self.questions objectAtIndex:questionNo]).userAnswer.optionText && ((QuizQuestionDomainObject*)[self.questions objectAtIndex:questionNo]).userAnswer.optionText.length) {
            isTextOption = YES;
        }
        
        if (isTextOption) {
            return 144;
        }
        else {
            //image option
            return IS_IPHONE_6_PLUS?220:(IS_IPHONE_6?200:([UIScreen mainScreen].bounds.size.height > 500)?180:180);
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == self.questions.count*2 - 1) {
        return 0;
    }
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == self.questions.count - 1) {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
    else {
        UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 60)];
        footerView.backgroundColor = [UIColor whiteColor];
        return footerView;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIColor *colorOne = [UIColor colorWithRed:172.0/255.0 green:198.0/255.0 blue:229.0/255.0 alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:255.0/255.0 green:179.0/255.0 blue:185.0/255.0 alpha:1.0];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.frame = self.gradientBaseView.bounds;
    [self.gradientBaseView.layer insertSublayer:headerLayer atIndex:0];

    int questionNo = (int)(indexPath.row/2);
    
    if (indexPath.row%2 == 0) {
        //show question
        TMQuizCommonQuestionTableViewCell* commonQuestionTableViewCell = [self.userAnswerTableView dequeueReusableCellWithIdentifier:USER_COMMON_QUESTION_TABLE_VIEW_CELL];
        [commonQuestionTableViewCell fillQuestionDetailsForQuestionObject:((QuizQuestionDomainObject*)[self.questions objectAtIndex:questionNo]) andQuestionNumber:(int)(questionNo + 1)];
        commonQuestionTableViewCell.backgroundColor = [UIColor clearColor];
        return commonQuestionTableViewCell;
    }
    else {
        //show answer
        
        QuizOptionDomainObject* userAnswer = ((QuizQuestionDomainObject*)[self.questions objectAtIndex:questionNo]).userAnswer;
        
        QuizOptionDomainObject* matchAnswer = ((QuizQuestionDomainObject*)[self.questions objectAtIndex:questionNo]).matchAnswer;
        
        TMQuizCommonAnswerTableViewCell* commonAnswerTableViewCell = [self.userAnswerTableView dequeueReusableCellWithIdentifier:USER_COMMON_ANSWER_TABLE_VIEW_CELL];
        [commonAnswerTableViewCell updateCommonAnswerInfoForUserAnswer:userAnswer matchAnswer:matchAnswer userProfilePicURL:[self.delegate getUserProfilePic] matchProfilePicURL:[self.delegate getCurrentMatchProfilePic]];
        commonAnswerTableViewCell.backgroundColor = [UIColor clearColor];
        return commonAnswerTableViewCell;
    }
}

#pragma mark - TMKeyboardControllerDelegate delegate methods

- (void)keyboardController:(TMKeyboardController*)keyboardController keyboardDidChangeFrame:(CGRect)keyboardFrame
{
    //adjust the common answer screen UI
    if (![self.toolBar.contentView.textView isFirstResponder]) {
        return;
    }
    CGFloat heightFromBottom = CGRectGetMinY(keyboardFrame) - CGRectGetHeight(self.toolBar.frame);
    if(self.keyboardController.isKeyboardScrolling) {
        heightFromBottom = heightFromBottom - 54;
    }
    
    
    heightFromBottom = MIN(self.userAnswerTableView.frame.size.height, heightFromBottom);
    
    [self setToolbarBottomPosition:heightFromBottom];
}

- (void)keyboardControllerKeyboardWillStopPanning:(TMKeyboardController*)keyboardController
{
    //do nothing
}

- (void)keyboardControllerKeyboardDidHide:(TMKeyboardController*)keyboardController
{
    if (![self.toolBar.contentView.textView isFirstResponder]) {
        return;
    }
    [self.toolBar.contentView.textView resignFirstResponder];
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    CGFloat heightFromBottom = CGRectGetMaxY(maxUsableFrame) - CGRectGetHeight(self.toolBar.frame);
    [self setToolbarBottomPosition:heightFromBottom];}

-(void)setToolbarBottomPosition:(CGFloat)bottonYPositin {
    
    self.toolBar.frame = CGRectMake(self.toolBar.frame.origin.x,
                                         bottonYPositin,
                                         self.toolBar.frame.size.width,
                                         self.toolBar.frame.size.height);
    
    [self updateTableViewInsets];
}

- (void)setTableViewInsetsTopValue:(CGFloat)top bottomValue:(CGFloat)bottom
{
    //TMLOG(@"setCollectionViewInsetsTopValue:%f",bottom);
    UIEdgeInsets insets = UIEdgeInsetsZero;
    insets = UIEdgeInsetsMake(top, 0.0f, bottom, 0.0f);
    self.userAnswerTableView.contentInset = insets;
    self.userAnswerTableView.scrollIndicatorInsets = insets;
}

-(void)updateTableViewInsets {
    CGRect maxUsableFrame = [self getMaxUsableFrame];
    [self setTableViewInsetsTopValue:0 bottomValue:CGRectGetMaxY(maxUsableFrame)-CGRectGetMinY(self.toolBar.frame)-54];
   // [self scrollToBottomAnimated:false hideCollectionVewWhileScrolling:false];
}

- (void)scrollToBottomAnimated:(BOOL)animated hideCollectionVewWhileScrolling:(BOOL)hidden
{
    //TMLOG(@":_______scrollToBottomAnimated1");
    if ([self.userAnswerTableView numberOfSections] == 0) {
        return;
    }
    
    NSInteger rows = [self.userAnswerTableView numberOfRowsInSection:0];
    
    if (rows == 0) {
        return;
    }
    
    if(hidden) {
        self.userAnswerTableView.hidden = hidden;
    }
    CGFloat tableViewContentHeight = [self.userAnswerTableView.delegate tableView:self.userAnswerTableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    BOOL isContentTooSmall = (tableViewContentHeight < self.userAnswerTableView.bounds.size.height);
    
    if (isContentTooSmall) {
        //  workaround for the first few messages not scrolling
        //  when the collection view content size is too small, `scrollToItemAtIndexPath:` doesn't work properly
        //  this seems to be a UIKit bug, see #256 on GitHub
        [self.userAnswerTableView scrollRectToVisible:CGRectMake(0.0, tableViewContentHeight - 1.0f, 1.0f, 1.0f)
                                        animated:animated];
        return;
    }
    
    //  workaround for really long messages not scrolling
    //  if last message is too long, use scroll position bottom for better appearance, else use top
    //  possibly a UIKit bug, see #480 on GitHub
    NSUInteger finalRow = MAX(0, [self.userAnswerTableView numberOfRowsInSection:0] - 1);
    NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
    CGSize finalCellSize = CGSizeMake(self.userAnswerTableView.bounds.size.width, [self.userAnswerTableView cellForRowAtIndexPath:finalIndexPath].bounds.size.height);//[self.userAnswerTableView.collectionViewLayout sizeForItemAtIndexPath:finalIndexPath];
    
    CGFloat maxHeightForVisibleMessage = CGRectGetHeight(self.userAnswerTableView.bounds) - self.userAnswerTableView.contentInset.top - 54;
    
    UITableViewScrollPosition scrollPosition = (finalCellSize.height > maxHeightForVisibleMessage) ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    //TMLOG(@":_______scrollToBottomAnimated2");
    [self.userAnswerTableView scrollToRowAtIndexPath:finalIndexPath atScrollPosition:scrollPosition animated:animated];
    // [self.userAnswerTableView scrollToItemAtIndexPath:finalIndexPath
                                //atScrollPosition:scrollPosition
                                  //      animated:animated];
    //TMLOG(@":_______scrollToBottomAnimated3");
}

-(CGRect)getMaxUsableFrame {
    CGFloat topBarHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    CGFloat totalHeight = CGRectGetMaxY(self.frame);
    
    CGFloat maxUsableHeight = totalHeight - topBarHeight;
    CGRect rect = CGRectMake(CGRectGetMinX(self.bounds),
                             CGRectGetMinY(self.bounds),
                             CGRectGetWidth(self.bounds),
                             maxUsableHeight);
    return rect;
}

#pragma mark - UITextView delegate methods

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView != self.toolBar.contentView.textView) {
        return;
    }
    
    [self.toolBar toggleSendButtonEnabled];
}

#pragma mark - Keyboard dismiss tap gesture callback method

- (void) screenTapped:(UITapGestureRecognizer*) tapGestureRecognizer
{
    [self.toolBar.contentView.textView resignFirstResponder];
}

- (void) shareScore {
    
    self.eventDict = [[NSMutableDictionary alloc] init];
    
    NSString *action = (self.isFlare) ? @"flare_send_nudge_common_answers" : @"send_nudge_common_answers";
    //adding event tracking for nudge action
    [self.eventDict setObject:@"TMQuizGalleryView" forKey:@"screenName"];
    [self.eventDict setObject:self.quizID forKey:@"label"];
    [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
    [self.eventDict setObject:self.quizID forKey:@"status"];
    [self.eventDict setObject:action forKey:@"eventAction"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
    
    //sending 1 to 1 nudge for now
    [self.quizController performOneToOneNudgeWithQuizID:self.quizID quizName:self.quizName];
    // [self.quizController sendCommonAnswerWithMessageText:[NSString stringWithFormat:@"Hey our score is %d/%d :)",self.commonAnswerCount,(int)self.questions.count] quizID:self.quizID];
    
    //dismiss the keyboard
    [self.toolBar.contentView.textView resignFirstResponder];
    
    //dismiss the self view
    [self.delegate commonAnswerMessageSendingButtonPressed];
}

- (IBAction)shareScoreTapped:(id)sender {
    [self shareScore];
}

#pragma mark - UIKeyboard notification callback method
- (void)willReceiveKeyboardDidHideNotification:(NSNotification *)notification
{
    self.toolBar.frame = CGRectMake(self.toolBar.frame.origin.x,
                                    self.toolBar.frame.origin.y + self.toolBar.bounds.size.height + 8,
                                    self.toolBar.frame.size.width,
                                    self.toolBar.frame.size.height);
}

- (void) dealloc
{
    [self.keyboardController endListeningForKeyboard];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

//
//  TMNavigationImageView.h
//  TrulyMadly
//
//  Created by Ankit on 22/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMNavigationImageViewDeleagte;

@interface TMNavigationImageView : UIView

@property(nonatomic,weak)id<TMNavigationImageViewDeleagte> delegate;

-(void)setTitleText:(NSString*)nameText withImageUrl:(NSString*)imageurl;
-(void)setViewForConnectionModeWithText:(BOOL)showText;
-(void)resetViewForConnectionMode;

@end

@protocol TMNavigationImageViewDeleagte <NSObject>

-(void)didTapToNavigationView;

@end

//
//  TMbadgeView.m
//  TrulyMadly
//
//  Created by Ankit on 08/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBadgeView.h"

@implementation TMBadgeView

-(instancetype)initWithFrame:(CGRect)frame
                   withImage:(UIImage*)image
              withImageFrame:(CGRect)imageFrame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.badgeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.badgeButton.frame = frame;
        self.badgeButton.backgroundColor = [UIColor clearColor];
        [self addSubview:self.badgeButton];
        
        UIImageView *imgview = [[UIImageView alloc] initWithFrame:imageFrame];
        imgview.backgroundColor = [UIColor clearColor];
        //imgview.contentMode = UIViewContentModeScaleToFill;
        imgview.image = image;
        [self addSubview:imgview];
    }
    
    return self;
}

-(void)setBadgeNotificationStatus:(BOOL)status {
    if(status) {
        UIImageView *imgview = (UIImageView*)[self viewWithTag:41000];
        if(!imgview) {
            imgview = [[UIImageView alloc] initWithFrame:CGRectMake(32, 8, 12, 12)];
            imgview.tag = 41000;
            [self addSubview:imgview];
        }
        imgview.image = [UIImage imageNamed:@"notification"];
    }
    else {
        UIImageView *imgview = (UIImageView*)[self viewWithTag:41000];
        imgview.image = nil;
        [imgview removeFromSuperview];
        imgview = nil;
    }
}

-(void)setBadgeNotificationStatusForMenu:(BOOL)status {
    if(status) {
        UIImageView *imgview = (UIImageView*)[self viewWithTag:410002];
        if(!imgview) {
            imgview = [[UIImageView alloc] initWithFrame:CGRectMake(28, 5, 10, 10)];
            imgview.tag = 410002;
            [self addSubview:imgview];
        }
        imgview.image = [UIImage imageNamed:@"notification"];
    }
    else {
        UIImageView *imgview = (UIImageView*)[self viewWithTag:410002];
        imgview.image = nil;
        [imgview removeFromSuperview];
        imgview = nil;
    }
}

-(void)setBadgeNotificationStatusForLocation:(BOOL)status {
    if(status) {
        UIImageView *imgview = (UIImageView*)[self viewWithTag:410001];
        if(!imgview) {
            imgview = [[UIImageView alloc] initWithFrame:CGRectMake(34, 10, 10, 10)];
            imgview.tag = 410001;
            [self addSubview:imgview];
        }
        imgview.image = [UIImage imageNamed:@"notification"];
    }
    else {
        UIImageView *imgview = (UIImageView*)[self viewWithTag:410001];
        imgview.image = nil;
        [imgview removeFromSuperview];
        imgview = nil;
    }
}

-(void)setCounter:(NSString *)counter
{
    if(counter && ![counter isEqualToString:@"0"]){
        UILabel *lbl = (UILabel *)[self viewWithTag:110390];
        if(!lbl){
            lbl = [[UILabel alloc] initWithFrame:CGRectMake(36, 7, 15, 15)];
            lbl.tag = 110390;
            lbl.layer.masksToBounds = YES;
            lbl.layer.cornerRadius = lbl.frame.size.width/2;
            [self addSubview:lbl];
        }
        lbl.text = counter;
        lbl.textColor = [UIColor whiteColor];
        lbl.font = [UIFont systemFontOfSize:11.0];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor orangeColor];
    }else {
        UILabel *lbl = (UILabel *)[self viewWithTag:110390];
        [lbl removeFromSuperview];
        lbl = nil;
    }
}

@end

//
//  TMNoNetworkBar.m
//  TrulyMadly
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNoNetworkBar.h"
#import "UIColor+TMColorAdditions.h"

@interface TMNoNetworkBar ()

@end

@implementation TMNoNetworkBar

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor noNetworkIndicatorViewColor];
        self.infoLabel = [[UILabel alloc] initWithFrame:self.bounds];
        self.infoLabel.textColor = [UIColor whiteColor];
        self.infoLabel.font = [UIFont systemFontOfSize:14];
        self.infoLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.infoLabel];
    }
    return self;
}

-(void)showWithWidth:(CGFloat)width {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.frame = CGRectMake(0, 0, width, 30);
                         self.infoLabel.frame = self.bounds;
                     }
                     completion:^(BOOL finished){
                         self.infoLabel.text = @"No Internet Connectivity";
                     }];
}

-(void)hide {
    self.infoLabel.text = @"Connecting...";
}

@end

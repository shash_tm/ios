//
//  TMRetryView.m
//  TrulyMadly
//
//  Created by Ankit on 31/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMRetryView.h"
#import "UIColor+TMColorAdditions.h"
#import "NSString+TMAdditions.h"


@implementation TMRetryView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.retryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.retryButton.layer.cornerRadius = 10;
        [self.retryButton setTitle:@"Retry" forState:UIControlStateNormal];
        self.retryButton.backgroundColor = [UIColor activeButtonColor];
        self.retryButton.frame = CGRectMake((self.frame.size.width-100)/2,
                                            (self.frame.size.height-30)/2,
                                            100,
                                            44);
        [self addSubview:self.retryButton];
        
        self.retryTextLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width-200)/2,
                                                                        (self.frame.size.height-110)/2,
                                                                        200,
                                                                        30)];
        self.retryTextLabel.textAlignment = NSTextAlignmentCenter;
        self.retryTextLabel.textColor = [UIColor darkTextColor];
        self.retryTextLabel.backgroundColor = [UIColor clearColor];
        self.retryTextLabel.text = @"No Internet Connectivity";
        self.retryTextLabel.font = [UIFont systemFontOfSize:14.0];
        [self addSubview:self.retryTextLabel];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect) frame message:(NSString*) message
{
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.retryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.retryButton.layer.cornerRadius = 10;
        [self.retryButton setTitle:@"Retry" forState:UIControlStateNormal];
        self.retryButton.backgroundColor = [UIColor activeButtonColor];
        self.retryButton.frame = CGRectMake((self.frame.size.width-100)/2,
                                            (self.frame.size.height-30)/2,
                                            100,
                                            44);
        [self addSubview:self.retryButton];
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0]};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds)-40, 25);
        CGRect rect = [message boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];

        self.retryTextLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width-rect.size.width)/2,
                                                                        (self.frame.size.height-110)/2,
                                                                        rect.size.width,
                                                                        rect.size.height)];
        //self.retryTextLabel.center = CGPointMake(self.center.x, self.center.y - 80);
        self.retryTextLabel.textAlignment = NSTextAlignmentCenter;
        self.retryTextLabel.textColor = [UIColor darkTextColor];
        self.retryTextLabel.backgroundColor = [UIColor clearColor];
        self.retryTextLabel.text = message;
        self.retryTextLabel.numberOfLines = 0;
        self.retryTextLabel.font = [UIFont systemFontOfSize:14.0];
        [self.retryTextLabel sizeToFit];
        [self addSubview:self.retryTextLabel];
    }
    return self;
}

@end

//
//  TMRetryView.h
//  TrulyMadly
//
//  Created by Ankit on 31/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMRetryView : UIView

@property(nonatomic,strong)UILabel *retryTextLabel;
@property(nonatomic,strong)UIButton *retryButton;

- (instancetype) initWithFrame:(CGRect) frame message:(NSString*) message;

@end

//
//  TMSystemMessageView.h
//  TrulyMadly
//
//  Created by Ankit on 11/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEnums.h"

@protocol TMSystemMessageViewDelegate;

@interface TMSystemMessageView : UIView

@property(nonatomic,weak)id<TMSystemMessageViewDelegate>delegate;

-(instancetype)initWithFrame:(CGRect)frame withSystemMessageLink:(TMSystemMessageLinkType)systemMsgLink;

-(void)setTitleText:(NSString*)text;
-(void)setButtonTitleText:(NSString*)text;
-(void)setButtonHidden;
-(void)showInviteFriendThanksMessage;

@end

@protocol TMSystemMessageViewDelegate <NSObject>

-(void)handleActionOnSystemMessageLinkType:(TMSystemMessageLinkType)systemMsgLink;

@end

//
//  TMCustomAlertView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 28/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMCustomAlertViewDelegate <NSObject>

@optional
- (void) actionButtonPressed:(NSNumber *)rateScore;

- (void) cancelButtonPressed;

@end

@interface TMCustomAlertView : UIView

@property (nonatomic, weak) id<TMCustomAlertViewDelegate> delegate;

-(void)initAlertWithMessageAndRateView: (NSString *)message parentView:(UIView *)parentView delegate:(id<TMCustomAlertViewDelegate>)delegate buttonTitle:(NSString *)buttonTitle;

@end

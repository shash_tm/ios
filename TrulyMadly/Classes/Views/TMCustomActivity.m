//
//  TMCustomActivity.m
//  TrulyMadly
//
//  Created by Ankit on 28/09/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCustomActivity.h"

@implementation TMCustomActivity


- (instancetype)initWithText:(NSString*)text link:(NSString*)link
{
    self = [super init];
    if (self) {
        self.text = text;
        self.link = link;
    }
    return self;
}

-(NSString*)activityType {
    return @"whatsapp.com";
}

-(NSString*)activityTitle {
    return @"WhatsApp";
}

-(UIImage*)activityImage {
    return [UIImage imageNamed:@"WhatsApp"];
}

-(BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    return TRUE;
}

-(UIActivityCategory)activityCategory {
    return UIActivityCategoryShare;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    
}

- (void)performActivity {
    NSString *text = [self.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *link = [self.link stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *replaced = [link stringByReplacingOccurrencesOfString:@"=" withString:@"%3D" options:NSLiteralSearch range:NSMakeRange(0, link.length)];
    
    NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@%@",text,replaced];
    
    NSURL *whatsappURL = [[NSURL alloc] initWithString:url];
    
    UIApplication *application = [UIApplication sharedApplication];
    if([application canOpenURL:whatsappURL]) {
        [application openURL:whatsappURL];
    }
    else {
        [self showAlert:@"" msg:@"There was an error connecting to whatsApp. Please make sure that it is installed."];
    }
}

-(void)activityDidFinish:(BOOL)completed {
    
}

-(void)showAlert:(NSString*)title msg:(NSString*)msg {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    });
}



@end

//
//  TMQuizUserAnswerCollectionView.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMKeyboardController.h"
#import "TMQuizController.h"

@protocol TMQuizAnswerViewDelegate <NSObject>

- (void) commonAnswerMessageSendingButtonPressed;

- (NSString*) getCurrentMatchProfilePic;

- (NSString*) getUserProfilePic;

- (NSString*) getUserName;

- (NSString*) getCurrentMatchName;


@end

@interface TMQuizUserAnswerView : UIView

@property (nonatomic, strong) id<TMQuizAnswerViewDelegate> delegate;

@property (nonatomic, weak) TMQuizController* quizController;

- (void) fillUserAnswerDetailsWithQuizID:(NSString*) quizID quizName:(NSString*) quizName quizImageURL:(NSString*) quizImageURL quizQuestionList:(NSArray*) questionList isFlare:(BOOL) isFlare;
- (void) setUpToolBar;

@end

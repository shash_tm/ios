//
//  TMCircleView.m
//  TrulyMadly
//
//  Created by Ankit on 21/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCircleWiget.h"

#define kStartAngle -M_PI_2


@interface TMCircleWiget ()

@property (nonatomic, strong) CAShapeLayer *backgroundLayer;
@property (nonatomic, strong) CAShapeLayer *circle;
@property (nonatomic) CGPoint centerPoint;
@property (nonatomic) CGFloat percent;
@property (nonatomic) CGFloat radius;
@property (nonatomic) CGFloat lineWidth;
@property (nonatomic) BOOL clockwise;

@end

@implementation TMCircleWiget

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundLayer = [CAShapeLayer layer];
        [self.layer addSublayer:self.backgroundLayer];
        
        self.circle = [CAShapeLayer layer];
        [self.layer addSublayer:self.circle];
    }
    return self;
}

- (void)drawCircleWithPercent:(CGFloat)percent
                    lineWidth:(CGFloat)lineWidth
                    clockwise:(BOOL)clockwise
                  strokeColor:(UIColor *)strokeColor {
    
    self.percent = percent;
    self.lineWidth = lineWidth;
    self.clockwise = clockwise;
    
    CGFloat min = MIN(self.frame.size.width, self.frame.size.height);
    self.radius = (min - lineWidth)  / 2;
    self.centerPoint = CGPointMake(self.frame.size.width / 2 - self.radius, self.frame.size.height / 2 - self.radius);
    
    [self setupBackgroundLayerWithFillColor];
    [self setupCircleLayerWithStrokeColor:strokeColor];
}

- (void)setupBackgroundLayerWithFillColor {
    
    self.backgroundLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.radius, self.radius) radius:self.radius startAngle:kStartAngle endAngle:2*M_PI clockwise:self.clockwise].CGPath;
    
    // Center the shape in self.view
    self.backgroundLayer.position = self.centerPoint;
    
    // Configure the apperence of the circle
    self.backgroundLayer.fillColor = [UIColor clearColor].CGColor;
    self.backgroundLayer.strokeColor = [UIColor clearColor].CGColor;
    self.backgroundLayer.lineWidth = self.lineWidth;
    self.backgroundLayer.lineCap = kCALineCapRound;
    self.backgroundLayer.rasterizationScale = 2 * [UIScreen mainScreen].scale;
    self.backgroundLayer.shouldRasterize = YES;
    
}

- (void)setupCircleLayerWithStrokeColor:(UIColor *)strokeColor {
    // Set up the shape of the circle
    
    CGFloat endAngle = [self calculateToValueWithPercent:self.percent];
    
    // Make a circular shape
    self.circle.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.radius, self.radius) radius:self.radius startAngle:kStartAngle endAngle:endAngle clockwise:self.clockwise].CGPath;
    
    // Center the shape in self.view
    
    self.circle.position = self.centerPoint;
    
    // Configure the apperence of the circle
    self.circle.fillColor = [UIColor clearColor].CGColor;
    self.circle.strokeColor = strokeColor.CGColor;
    self.circle.lineWidth = self.lineWidth;
    self.circle.lineCap = kCALineCapRound;
    self.circle.shouldRasterize = YES;
    self.circle.rasterizationScale = 2 * [UIScreen mainScreen].scale;
    
}

- (CGFloat)calculateToValueWithPercent:(CGFloat)percent {
    return (kStartAngle + (percent * 2 * M_PI) / 100);
}

@end

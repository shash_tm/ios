//
//  TMCircleView.h
//  TrulyMadly
//
//  Created by Ankit on 21/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCircleWiget : UIView

- (void)drawCircleWithPercent:(CGFloat)percent
                    lineWidth:(CGFloat)lineWidth
                    clockwise:(BOOL)clockwise
                  strokeColor:(UIColor *)strokeColor;

@end

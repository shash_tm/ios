//
//  TMActivityIndicatorView.h
//  TrulyMadly
//
//  Created by Ankit on 09/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TMActivityIndicatorView : UIView

@property(nonatomic,strong)NSString *titleText;

/**
 Starts the animation of the activity indicator.
 */
- (void)startAnimating;

/**
 Stops the animation of the acitivity indciator.
 */
- (void)stopAnimating;

@property(nonatomic,strong)UILabel *titleLable;

@end




//
//  TMToastView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMToastView.h"
#import "TMLog.h"
#import "TMStringUtil.h"
#import "UIImageView+AFNetworking.h"

#define IS_PHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_PHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568.0)

@interface TMToastView ()
@property (strong, nonatomic, readonly) UILabel *textLabel;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (nonatomic,assign)BOOL isAlertIconShown;
@property (nonatomic,assign)BOOL isMissTMShown;
@property (nonatomic, assign) TMToastPresentationDirection presentationDirection;
@property (nonatomic, strong) UIColor *textColor;
@property(nonatomic, strong) UIImageView *secondEventView;
@end

@implementation TMToastView
@synthesize textLabel = _textLabel;
@synthesize imageView = _imageView;

float const ToastHeight = 50.0f;
float const ToastGap = 10.0f;
float yMissPos = 10.0f;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(UILabel *)textLabel
{
    if (!_textLabel) {
        if(self.isMissTMShown) {
            _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, yMissPos, self.frame.size.width - 80.0, self.frame.size.height - 20.0)];
            _textLabel.textAlignment = NSTextAlignmentLeft;
            _textLabel.font = [UIFont boldSystemFontOfSize:13.0];
        }
        else {
            _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, self.frame.size.width - 10.0, self.frame.size.height - 10.0)];
            _textLabel.textAlignment = NSTextAlignmentCenter;
            _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _textLabel.numberOfLines = 2;
            _textLabel.font = [UIFont systemFontOfSize:13.0];
        }
        
        _textLabel.textColor = self.textColor;
        _textLabel.backgroundColor = [UIColor clearColor];
        
        if(self.isAlertIconShown) {
            _textLabel.textAlignment = NSTextAlignmentCenter;
            //_textLabel.textAlignment = NSTextAlignmentLeft;
            _textLabel.font = [UIFont systemFontOfSize:15.0];

        }
        [self addSubview:_textLabel];
        
    }
    return _textLabel;
}

-(UIImageView *)imageView
{
    if(!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0,16.0,21.0,18.0)];
        _imageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_imageView];
    }
    return _imageView;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textLabel.text = text;
    if(self.isMissTMShown) {
        self.textLabel.numberOfLines = 0;
        [self.textLabel sizeToFit];
    }
}

+(void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection) presentationDirection;
{
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            toastsAlreadyInParent = 1;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    TMLOG(@"parent frame is %@",NSStringFromCGRect(parentFrame));
    
    //float yOrigin = parentFrame.size.height - (70.0 + ToastHeight * toastsAlreadyInParent + ToastGap * toastsAlreadyInParent);
    float yOrigin = 0;
    if (presentationDirection == TMToastPresentationFromTop) {
        if(parentFrame.origin.y == 0) {
            yOrigin = 64.0;
        }
    }
    else {
        //presentation from bottom
        yOrigin = parentView.bounds.origin.y + parentView.bounds.size.height;
    }
    CGRect selfFrame = CGRectMake(20.0, yOrigin, parentFrame.size.width - 40.0, ToastHeight);
    TMLOG(@"self frame is %@",NSStringFromCGRect(selfFrame));
    if(toastsAlreadyInParent == 0) {
        TMToastView *toast = [[TMToastView alloc] initWithFrame:selfFrame];
    
        toast.backgroundColor = [UIColor darkGrayColor];
        toast.alpha = 0.0f;
        toast.layer.cornerRadius = 4.0;
        toast.textColor = [UIColor whiteColor];
        toast.text = text;
    
        toast.presentationDirection = presentationDirection;
        
        [parentView addSubview:toast];
    
        [UIView animateWithDuration:0.4 animations:^{
            toast.alpha = 1.0f;
            toast.textLabel.alpha = 0.9f;
            
            if (presentationDirection == TMToastPresentationFromTop) {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin+10.0,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin - 60.0 - CGRectGetHeight(toast.frame),CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
        }completion:^(BOOL finished) {
            if(finished){
            
            }
        }];
        [toast performSelector:@selector(hideSelf:) withObject:toast afterDelay:duration];
    
}
}


+(void)showToastInParentViewWithAlertIcon: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection) presentationDirection;
{
    
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            toastsAlreadyInParent = 1;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    TMLOG(@"parent frame is %@",NSStringFromCGRect(parentFrame));
    
    //float yOrigin = parentFrame.size.height - (70.0 + ToastHeight * toastsAlreadyInParent + ToastGap * toastsAlreadyInParent);
    float yOrigin = 0;
    if (presentationDirection == TMToastPresentationFromTop) {
        if(parentFrame.origin.y == 0) {
            yOrigin = 64.0;
        }
    }
    else {
        //presentation from bottom
        yOrigin = parentView.bounds.origin.y + parentView.bounds.size.height;
    }
    CGRect selfFrame = CGRectMake(0.0, yOrigin, parentFrame.size.width, ToastHeight);
    TMLOG(@"self frame is %@",NSStringFromCGRect(selfFrame));
    if(toastsAlreadyInParent == 0) {
        TMToastView *toast = [[TMToastView alloc] initWithFrame:selfFrame];
        toast.isAlertIconShown = true;
        toast.backgroundColor = [UIColor colorWithRed:0.35 green:0.57 blue:0.81 alpha:1.0];
        toast.alpha = 0.0f;
        toast.textColor = [UIColor whiteColor];
        toast.text = text;
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0], @"kfont", [NSNumber numberWithFloat:parentFrame.size.width], @"kmaxwidth", text, @"ktext", nil];
        CGRect rect = [TMStringUtil textRectForString:dict];
        
        CGRect imageFrame = toast.imageView.frame;
        imageFrame.origin.x = (parentFrame.size.width-rect.size.width)/2 + rect.size.width - 5.0;
        toast.imageView.frame = imageFrame;
        toast.imageView.image = [UIImage imageNamed:@"Alert"];

        
        toast.presentationDirection = presentationDirection;
        
        [parentView addSubview:toast];
        
        [UIView animateWithDuration:0.4 animations:^{
            toast.alpha = 1.0f;
            toast.textLabel.alpha = 0.9f;
            
            if (presentationDirection == TMToastPresentationFromTop) {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin - 60.0 - CGRectGetHeight(toast.frame),CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
        }completion:^(BOOL finished) {
            if(finished){
                
            }
        }];
        [toast performSelector:@selector(hideSelf:) withObject:toast afterDelay:duration];
        
    }
}

+(void)showToastInParentViewWithMissTM:(UIView *)parentView withText:(NSString *)text withDuration:(float)duration presentationDirection:(TMToastPresentationDirection)presentationDirection
{
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            toastsAlreadyInParent = 1;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    TMLOG(@"parent frame is %@",NSStringFromCGRect(parentFrame));
    
    float yOrigin = 0;
    float xOrigin = 20.0;
    float toastWidth = parentFrame.size.width - 40.0;
    
    if (presentationDirection == TMToastPresentationFromTop) {
        if(parentFrame.origin.y == 0) {
            yOrigin = 64.0;
        }
    }
    else if(presentationDirection == TMToastPresentationFromLeft) {
        toastWidth = parentFrame.size.width*0.78;
        xOrigin = parentView.bounds.origin.x - toastWidth;
        yOrigin = parentView.bounds.origin.y + 25;
    }
    else {
        //presentation from bottom
        yOrigin = parentView.bounds.origin.y + parentView.bounds.size.height;
    }
    
    CGRect selfFrame = CGRectMake(xOrigin, yOrigin, toastWidth, 70);
    
    TMLOG(@"self frame is %@",NSStringFromCGRect(selfFrame));
    
    if(toastsAlreadyInParent == 0) {
        TMToastView *toast = [[TMToastView alloc] initWithFrame:selfFrame];
        
        toast.isMissTMShown = true;
        toast.backgroundColor = [UIColor whiteColor];
        toast.alpha = 0.0f;
        toast.textColor = [UIColor blackColor];
        toast.layer.cornerRadius = 4.0;
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:13.0], @"kfont", [NSNumber numberWithFloat:toastWidth-70], @"kmaxwidth", text, @"ktext", nil];
        CGRect rect = [TMStringUtil textRectForString:dict];
        yMissPos = (selfFrame.size.height - rect.size.height)/2;
        
        toast.text = text;
        
        CGRect imageFrame = toast.imageView.frame;
        imageFrame.origin.x = CGRectGetWidth(toast.frame)-60.0;
        imageFrame.origin.y = 10.0;
        imageFrame.size.width = 50.0;
        imageFrame.size.height = 50.0;
        toast.imageView.frame = imageFrame;
        toast.imageView.image = [UIImage imageNamed:@"MissTMNudge"];
        
        toast.presentationDirection = presentationDirection;
        
        [parentView addSubview:toast];
        
        [UIView animateWithDuration:0.4 animations:^{
            toast.alpha = 0.85f;
            toast.textLabel.alpha = 0.9f;
            
            if (presentationDirection == TMToastPresentationFromTop) {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin+10.0,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else if(presentationDirection == TMToastPresentationFromLeft) {
                toast.frame = CGRectMake(xOrigin+CGRectGetWidth(toast.frame)+10, yOrigin ,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else {
                toast.frame = CGRectMake(toast.frame.origin.x, yOrigin - 60.0 - CGRectGetHeight(toast.frame),CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
        }completion:^(BOOL finished) {
            if(finished){
                
            }
        }];
        [toast performSelector:@selector(hideSelf:) withObject:toast afterDelay:duration];
        
    }

}

- (void)hideSelf:(TMToastView*)toastView
{
    
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 0.0;
        self.textLabel.alpha = 0.0;
        if (toastView.presentationDirection == TMToastPresentationFromTop) {
            toastView.frame = CGRectMake(toastView.frame.origin.x,0.0,CGRectGetWidth(toastView.frame),CGRectGetHeight(toastView.frame));
        }
        else if (toastView.presentationDirection == TMToastPresentationFromLeft) {
            toastView.frame = CGRectMake(toastView.frame.origin.x-CGRectGetWidth(toastView.frame)-10,toastView.frame.origin.y,CGRectGetWidth(toastView.frame),CGRectGetHeight(toastView.frame));
        }
        else {
            toastView.frame = CGRectMake(toastView.frame.origin.x,toastView.frame.origin.y + CGRectGetHeight(toastView.frame) + 60.0,CGRectGetWidth(toastView.frame),CGRectGetHeight(toastView.frame));
        }
    }completion:^(BOOL finished) {
        if(finished){
            [self removeFromSuperview];
        }
    }];
}

+(void)showToastInParentViewWithEditProfileEnabled:(UIView *)parentView withText:(NSString *)text withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection)presentationDirection
{
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            toastsAlreadyInParent = 1;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    TMLOG(@"parent frame is %@",NSStringFromCGRect(parentFrame));
    
    float yOrigin = 0;
    if (presentationDirection == TMToastPresentationFromTop) {
//        if(parentFrame.origin.y == 0) {
//            yOrigin = 64.0;
//        }
    }
    else {
        //presentation from bottom
        yOrigin = parentView.bounds.origin.y + parentView.bounds.size.height;
    }
    CGRect selfFrame = CGRectMake(20.0, yOrigin, parentFrame.size.width - 40.0, ToastHeight);
    TMLOG(@"self frame is %@",NSStringFromCGRect(selfFrame));
    if(toastsAlreadyInParent == 0) {
        TMToastView *toast = [[TMToastView alloc] initWithFrame:selfFrame];
        
        toast.backgroundColor = [UIColor darkGrayColor];
        toast.alpha = 0.0f;
        toast.layer.cornerRadius = 4.0;
        toast.textColor = [UIColor whiteColor];
        toast.text = text;
        
        toast.presentationDirection = presentationDirection;
        
        [parentView addSubview:toast];
        
        [UIView animateWithDuration:0.4 animations:^{
            toast.alpha = 1.0f;
            toast.textLabel.alpha = 0.9f;
            
            if (presentationDirection == TMToastPresentationFromTop) {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin+10.0,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin - 60.0 - CGRectGetHeight(toast.frame),CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
        }completion:^(BOOL finished) {
            if(finished){
                
            }
        }];
        [toast performSelector:@selector(hideSelf:) withObject:toast afterDelay:duration];
        
    }

}

+(void)showToastInParentViewForMutualEvent:(UIView *)parentView withMutualEventData:(NSArray *)mutualEventData withDuaration:(float)duration presentationDirection:(TMToastPresentationDirection)presentationDirection{
    
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            toastsAlreadyInParent = 1;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    TMLOG(@"parent frame is %@",NSStringFromCGRect(parentFrame));
    
    float yOrigin = 0;
    float xOrigin = 20.0;
    float toastWidth = parentFrame.size.width - 40.0;
    
    CGFloat eventWidth = 30;
    
    NSString *text = @"You both are following similar experiences.";
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], @"kfont", [NSNumber numberWithFloat:toastWidth-70], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    if(presentationDirection == TMToastPresentationFromLeft) {
        int count = (mutualEventData.count > 2) ? 3 : (int)mutualEventData.count;
        toastWidth = 8 + 10 + 10 + eventWidth*(count) - (count-1)*10 + rect.size.width;//parentFrame.size.width*0.9;
        xOrigin = parentView.bounds.origin.x - toastWidth;
        yOrigin = parentView.bounds.origin.y + 20;
    }
    else {
        //presentation from bottom
        yOrigin = parentView.bounds.origin.y + parentView.bounds.size.height;
    }
    
    CGRect selfFrame = CGRectMake(xOrigin, yOrigin, toastWidth, 40);
    
    TMLOG(@"self frame is %@",NSStringFromCGRect(selfFrame));
    
    if(toastsAlreadyInParent == 0) {
        TMToastView *toast = [[TMToastView alloc] initWithFrame:selfFrame];
        
        toast.backgroundColor = [UIColor whiteColor];
        toast.alpha = 0.95f;
        toast.layer.cornerRadius = 20.0;
        toast.textColor = [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1.0];
        
        // view for mutual event images
        CGFloat xPos = 10;
        CGFloat yPos = (40-eventWidth)/2;
        for (int i=0; i<mutualEventData.count; i++) {
            if(i > 2){
                break;
            }
            
            if(i==1) {
                toast.secondEventView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, eventWidth, eventWidth)];
                toast.secondEventView.backgroundColor = [UIColor clearColor];
                toast.secondEventView.layer.cornerRadius = eventWidth/2;
                toast.secondEventView.layer.masksToBounds = YES;
                toast.secondEventView.layer.borderWidth = 1.5;
                toast.secondEventView.layer.borderColor = [UIColor whiteColor].CGColor;
                [toast.secondEventView setImageWithURL:[[NSURL alloc] initWithString:[mutualEventData[i] objectForKey:@"image"]] placeholderImage:nil];
                [toast addSubview:toast.secondEventView];
            }else {
                UIImageView *picView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, eventWidth, eventWidth)];
                picView.backgroundColor = [UIColor clearColor];
                picView.layer.cornerRadius = eventWidth/2;
                picView.layer.masksToBounds = YES;
                picView.layer.borderWidth = 1.5;
                picView.layer.borderColor = [UIColor whiteColor].CGColor;
                [picView setImageWithURL:[[NSURL alloc] initWithString:[mutualEventData[i] objectForKey:@"image"]] placeholderImage:nil];
                [toast addSubview:picView];
            }
            
            if(i==2){
                [toast bringSubviewToFront:toast.secondEventView];
            }
            
            xPos = xPos + eventWidth - 10;
        }
        
        
        toast.text = text;
    
        toast.textLabel.frame = CGRectMake(10+eventWidth*mutualEventData.count+8-(10*(mutualEventData.count-1)), (toast.frame.size.height - rect.size.height)/2, rect.size.width, rect.size.height);
        toast.textLabel.font = [UIFont systemFontOfSize:14.0];
        toast.textLabel.textAlignment = NSTextAlignmentLeft;
        toast.presentationDirection = presentationDirection;
        
        [parentView addSubview:toast];
        
        [UIView animateWithDuration:0.4 animations:^{
            toast.alpha = 0.85f;
            toast.textLabel.alpha = 0.9f;
            
            if (presentationDirection == TMToastPresentationFromTop) {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin+10.0,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else if(presentationDirection == TMToastPresentationFromLeft) {
                toast.frame = CGRectMake(xOrigin+CGRectGetWidth(toast.frame)+(parentFrame.size.width-toastWidth)/2, yOrigin ,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else {
                toast.frame = CGRectMake(toast.frame.origin.x, yOrigin - 60.0 - CGRectGetHeight(toast.frame),CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
        }completion:^(BOOL finished) {
            if(finished){
                
            }
        }];
        
        //[toast performSelector:@selector(hideSelf:) withObject:toast afterDelay:duration];
        
    }

}

+(void)showToastInParentViewForPhotoFlow:(UIView *)parentView imageName:(NSString *)imageName
                             textMessage:(NSString *)textMessage  withDuaration:(float)duration
                   presentationDirection:(TMToastPresentationDirection)presentationDirection
                         removeAfterTime:(BOOL)removeAfterTime
{
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[TMToastView class]])
        {
            toastsAlreadyInParent = 1;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    TMLOG(@"parent frame is %@",NSStringFromCGRect(parentFrame));
    
    float yOrigin = 0;
    float xOrigin = 20.0;
    float toastWidth = parentFrame.size.width - 40.0;
    
    CGFloat iconWidth = 40;
    
    NSString *text = textMessage;
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], @"kfont", [NSNumber numberWithFloat:toastWidth-iconWidth-26], @"kmaxwidth", text, @"ktext", nil];
    CGRect rect = [TMStringUtil textRectForString:dict];
    
    if(presentationDirection == TMToastPresentationFromLeft) {
        toastWidth = 8+iconWidth+10+rect.size.width+8;
        xOrigin = parentView.bounds.origin.x - toastWidth;
        yOrigin = parentView.bounds.origin.y + 20;
    }
    else {
        //presentation from bottom
        yOrigin = parentView.bounds.origin.y + parentView.bounds.size.height;
    }
    
    CGRect selfFrame = CGRectMake(xOrigin, yOrigin, toastWidth, 50);
    
    TMLOG(@"self frame is %@",NSStringFromCGRect(selfFrame));
    
    if(toastsAlreadyInParent == 0) {
        TMToastView *toast = [[TMToastView alloc] initWithFrame:selfFrame];
        
        toast.backgroundColor = [UIColor whiteColor];
        toast.alpha = 0.95f;
        toast.layer.cornerRadius = 10.0;
        toast.textColor = [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1.0];
        toast.text = text;
        
        CGRect imageFrame = toast.imageView.frame;
        imageFrame.origin.x = 8.0;
        imageFrame.origin.y = 5.0;
        imageFrame.size.width = iconWidth;
        imageFrame.size.height = iconWidth;
        toast.imageView.frame = imageFrame;
        toast.imageView.image = [UIImage imageNamed:imageName];
        
        toast.textLabel.frame = CGRectMake(8+iconWidth+10, (toast.frame.size.height - rect.size.height)/2, rect.size.width, rect.size.height);
        toast.textLabel.font = (IS_PHONE_4 || IS_PHONE_5) ? [UIFont systemFontOfSize:12.0] : [UIFont systemFontOfSize:14.0];
        toast.textLabel.textAlignment = NSTextAlignmentLeft;
        toast.presentationDirection = presentationDirection;
        
        [parentView addSubview:toast];
        
        [UIView animateWithDuration:0.4 animations:^{
            toast.alpha = 0.85f;
            toast.textLabel.alpha = 0.9f;
            
            if (presentationDirection == TMToastPresentationFromTop) {
                toast.frame = CGRectMake(toast.frame.origin.x,yOrigin+10.0,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else if(presentationDirection == TMToastPresentationFromLeft) {
                toast.frame = CGRectMake(xOrigin+CGRectGetWidth(toast.frame)+(parentFrame.size.width-toastWidth)/2, yOrigin ,CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
            else {
                toast.frame = CGRectMake(toast.frame.origin.x, yOrigin - 60.0 - CGRectGetHeight(toast.frame),CGRectGetWidth(toast.frame),CGRectGetHeight(toast.frame));
            }
        }completion:^(BOOL finished) {
            if(finished){
                
            }
        }];
        
        if(removeAfterTime) {
            [toast performSelector:@selector(hideSelf:) withObject:toast afterDelay:duration];
        }
        
    }

}

@end
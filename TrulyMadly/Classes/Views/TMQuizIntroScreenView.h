//
//  TMQuizIntroScreenView.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 23/09/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMQuizIntroScreenView : UIView

- (void) fillQuizIntroDataWithQuizName:(NSString*) quizName quizImageURLString:(NSString*) quizImageURLString quizDetailTextString:(NSString*) quizDetailTextString quizSponsoredText:(NSString*) sponsoredText withQuizQuestionsFetchStateBlock:(BOOL(^)()) quizQuestionsFetchStateBlock startButtonPressBlock:(void(^)(BOOL quizQuestionsDownloaded)) startButtonPressBlock retryActionBlock:(void(^)(void)) quizRetryActionBlock;

@end

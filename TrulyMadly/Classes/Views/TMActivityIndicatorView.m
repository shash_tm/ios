//
//  TMActivityIndicatorView.m
//  TrulyMadly
//
//  Created by Ankit on 09/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMActivityIndicatorView.h"
#import "TMStringUtil.h"
#import "UIColor+TMColorAdditions.h"


@interface TMActivityIndicatorView ()

//@property(nonatomic,strong)UILabel *titleLable;

@property(nonatomic,strong)UIView *circleContainerView;

/** The number of circle indicators. */
@property (readwrite, nonatomic) NSUInteger numberOfCircles;

/** The spacing between circles. */
@property (readwrite, nonatomic) CGFloat internalSpacing;

/** The radius of each circle. */
@property (readwrite, nonatomic) CGFloat radius;

/** The base animation delay of each circle. */
@property (readwrite, nonatomic) CGFloat delay;

/** The base animation duration of each circle*/
@property (readwrite, nonatomic) CGFloat duration;

/** The default color of each circle. */
@property (strong, nonatomic) UIColor *defaultColor;

/** An indicator whether the activity indicator view is animating. */
@property (readwrite, nonatomic) BOOL isAnimating;

/** Header label frame based on text size **/
@property (nonatomic,assign) CGRect headerLabelFrame;


/**
 Sets up default values
 */
- (void)setupDefaults;

/**
 Adds circles.
 */
- (void)addCircles;

/**
 Removes circles.
 */
- (void)removeCircles;

/**
 Adjusts self's frame.
 */
- (void)adjustFrame;

/**
 Creates the circle view.
 @param radius The radius of the circle.
 @param color The background color of the circle.
 @param positionX The x-position of the circle in the contentView.
 @return The circle view.
 */
- (UIView *)createCircleWithRadius:(CGFloat)radius color:(UIColor *)color positionX:(CGFloat)x;

/**
 Creates the animation of the circle.
 @param duration The duration of the animation.
 @param delay The delay of the animation
 @return The animation of the circle.
 */
- (CABasicAnimation *)createAnimationWithDuration:(CGFloat)duration delay:(CGFloat)delay;

@end


@implementation TMActivityIndicatorView

#pragma mark -
#pragma mark - Initializations

- (id)init {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self setupDefaults];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setupDefaults];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

#pragma mark -
#pragma mark - Private Methods

- (void)setupDefaults {
    self.backgroundColor = [UIColor clearColor];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.circleContainerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.circleContainerView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.circleContainerView];
    self.titleLable = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLable.textAlignment = NSTextAlignmentCenter;
    self.titleLable.textColor = [UIColor blackColor];//[UIColor lightGrayColor];
    self.titleLable.font = [UIFont systemFontOfSize:14];
    self.titleLable.numberOfLines = 0;
    self.titleLable.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.titleLable];
    
    self.numberOfCircles = 3;
    self.internalSpacing = 3;
    self.radius = 10;
    self.delay = 0.2;
    self.duration = 0.7;
    self.defaultColor = [UIColor lightGrayColor];
    self.headerLabelFrame = CGRectZero;
    
}

- (UIView *)createCircleWithRadius:(CGFloat)radius
                             color:(UIColor *)color
                         positionX:(CGFloat)x {
    UIView *circle = [[UIView alloc] initWithFrame:CGRectMake(x, 0, radius * 2, radius * 2)];
    circle.backgroundColor = color;
    circle.layer.cornerRadius = radius;
    circle.translatesAutoresizingMaskIntoConstraints = NO;
    return circle;
}

- (CABasicAnimation *)createAnimationWithDuration:(CGFloat)duration delay:(CGFloat)delay {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    anim.delegate = self;
    anim.fromValue = [NSNumber numberWithFloat:0.0f];
    anim.toValue = [NSNumber numberWithFloat:1.0f];
    anim.autoreverses = YES;
    anim.duration = duration;
    anim.removedOnCompletion = NO;
    anim.beginTime = CACurrentMediaTime()+delay;
    anim.repeatCount = INFINITY;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return anim;
}

- (void)addCircles {
    for (NSUInteger i = 0; i < self.numberOfCircles; i++) {
        UIColor *color = [self circleBackgroundColorAtIndex:i];
        UIView *circle = [self createCircleWithRadius:self.radius
                                                color:(color == nil) ? self.defaultColor : color
                                            positionX:(i * ((2 * self.radius) + self.internalSpacing))];
        [circle setTransform:CGAffineTransformMakeScale(0, 0)];
        [circle.layer addAnimation:[self createAnimationWithDuration:self.duration delay:(i * self.delay)] forKey:@"scale"];
        [self.circleContainerView addSubview:circle];
    }
}

- (void)removeCircles {
    [self.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
}

- (void)adjustFrame {
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGRect frame = CGRectZero;
    CGFloat labelHeight = 40;
    CGFloat width = (self.numberOfCircles * ((2 * self.radius) + self.internalSpacing)) - self.internalSpacing;
    CGFloat height = (self.radius * 2);
    CGFloat totalHeight = height + labelHeight;
    CGFloat xPos = 0;
    CGFloat yPos = ((screenRect.size.height - totalHeight)/2) - height;
    
    //////
    frame = CGRectMake(xPos,
                       yPos,
                       screenRect.size.width,
                       totalHeight);
    self.frame = frame;
    
    ////////
    self.circleContainerView.frame = CGRectMake((frame.size.width - width)/2,
                                                0,
                                                width,
                                                height);
    //NSLog(@"CCVF:%@",NSStringFromCGRect(self.circleContainerView.frame));
    ///////
    self.titleLable.frame = CGRectMake(0,
                                       height,
                                       screenRect.size.width,
                                       40);
    
}

- (UIColor *)circleBackgroundColorAtIndex:(NSUInteger)index {
    UIColor *color = NULL;
    if(index == 0){
        color= [UIColor hideColor];//[UIColor colorWithRed:249/255.0f green:178/255.0f blue:183/255.0f alpha:0.95];
    }
    else if(index == 1) {
        color= [UIColor sparkColor];//[UIColor colorWithRed:214/255.0f green:189/255.0f blue:207/255.0f alpha:0.95];
    }
    else {
        color= [UIColor likeColor];//[UIColor colorWithRed:172/255.0f green:198/255.0f blue:229/255.0f alpha:0.95];
    }

    return color; //UIColorFromRGB(0xff3333);
}

#pragma mark -
#pragma mark - Public Methods

- (void)startAnimating {
    if (!self.isAnimating) {
        [self addCircles];
        self.hidden = NO;
        self.isAnimating = YES;
    }
}

- (void)stopAnimating {
    if (self.isAnimating) {
        [self removeCircles];
        self.hidden = YES;
        self.isAnimating = NO;
    }
}

-(void)setTitleText:(NSString *)titleText {
    _titleText = titleText;
    self.titleLable.text = titleText;
}

#pragma mark -
#pragma mark - Custom Setters and Getters

- (void)setNumberOfCircles:(NSUInteger)numberOfCircles {
    _numberOfCircles = numberOfCircles;
    [self adjustFrame];
}

- (void)setRadius:(CGFloat)radius {
    _radius = radius;
    [self adjustFrame];
}

- (void)setInternalSpacing:(CGFloat)internalSpacing {
    _internalSpacing = internalSpacing;
    [self adjustFrame];
}


@end

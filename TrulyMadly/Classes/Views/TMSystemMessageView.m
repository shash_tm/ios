//
//  TMSystemMessageView.m
//  TrulyMadly
//
//  Created by Ankit on 11/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMSystemMessageView.h"
#import "TMStringUtil.h"
#import "NSString+TMAdditions.h"
#import "TMEnums.h"

@interface TMSystemMessageView ()

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIButton *btn;
@property(nonatomic,assign)TMSystemMessageLinkType systemMsgLinkType;

@end

@implementation TMSystemMessageView

-(instancetype)initWithFrame:(CGRect)frame withSystemMessageLink:(TMSystemMessageLinkType)systemMsgLink{
    self = [super initWithFrame:frame];
    if(self) {
        self.systemMsgLinkType = systemMsgLink;
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,
                                                                    (self.frame.size.height-40)/2,
                                                                    self.frame.size.width-40,
                                                                    40)];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 0;
        [self addSubview:self.titleLabel];
        
        self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn.frame = CGRectMake(20,
                                    (self.titleLabel.frame.origin.y+self.titleLabel.frame.size.height)+20,
                                    self.frame.size.width-40,
                                    44);
        self.btn.backgroundColor = [UIColor colorWithRed:249/255.0f green:161/255.0f blue:171/255.0f alpha:1.0];
        [self.btn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btn];
    }
    
    return self;
}

-(void)setTitleText:(NSString*)text {
    self.titleLabel.text = text;
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], @"kfont", [NSNumber numberWithFloat:self.titleLabel.frame.size.width], @"kmaxwidth", text, @"ktext", nil];
    
    CGSize contentSize = [TMStringUtil textRectForString:dict].size;
    
    CGRect rect = self.titleLabel.frame;
    self.titleLabel.frame = CGRectMake(rect.origin.x,
                                       ((self.frame.size.height-contentSize.height)/2)-30,
                                       rect.size.width,
                                       contentSize.height);
    
    self.btn.frame = CGRectMake(self.btn.frame.origin.x,
                               (self.titleLabel.frame.origin.y+self.titleLabel.frame.size.height)+20,
                                self.btn.frame.size.width,
                                self.btn.frame.size.height);
}

-(void)setButtonTitleText:(NSString*)text {
    self.btn.hidden = FALSE;
    [self.btn setTitle:text forState:UIControlStateNormal];
}

-(void)setButtonHidden {
    self.btn.hidden = TRUE;
}

-(void)btnAction {
    //NSLog(@"btnAction");
    [self.delegate handleActionOnSystemMessageLinkType:self.systemMsgLinkType];
}

-(void)showInviteFriendThanksMessage {
    NSString *text = @"Your invites have been sent";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16]};
    CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.bounds), 20);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGFloat xPos = (CGRectGetWidth(self.frame) - rect.size.width)/2;
    CGFloat yPos = CGRectGetMaxY(self.btn.frame)+15;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPos,
                                                               yPos,
                                                               rect.size.width,
                                                               rect.size.height)];
    label.font = [UIFont systemFontOfSize:16];
    label.text = text;
    [self addSubview:label];
    
    
    [UIView animateWithDuration:1.5
                          delay: 0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         label.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [label removeFromSuperview];
                     }];
    
}

@end

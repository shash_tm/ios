//
//  TMProfilePrivacy.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 12/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfilePrivacy.h"
#import "NSString+TMAdditions.h"

#define margin 17
@implementation TMProfilePrivacy

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.alpha = 0.95;
        [self configureUI];
    }
    return self;
}

-(void)configureUI
{
    CGFloat height = 0;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    view.backgroundColor = [UIColor clearColor];
    [self addSubview:view];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(view.frame.size.width-20, NSUIntegerMax);
    CGRect rect = [@"Don\'t worry, we\'ve got your back!" boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UILabel *mainLbl = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width-rect.size.width)/2, 0, rect.size.width, rect.size.height)];
    mainLbl.text = @"Don\'t worry, we\'ve got your back!";
    mainLbl.font = font;
    mainLbl.textColor = [UIColor blackColor];
    [view addSubview:mainLbl];
    
    height = height + rect.size.height;
    
    UIView *biggerLine = [[UIView alloc] initWithFrame:CGRectMake(20, mainLbl.frame.origin.y+mainLbl.frame.size.height+24, view.frame.size.width-40, 0.75)];
    biggerLine.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [view addSubview:biggerLine];
    
    height = height + margin + biggerLine.frame.size.height;
    // public view
    
    UIImageView *publicImageView = [[UIImageView alloc] initWithFrame:CGRectMake(mainLbl.frame.origin.x, biggerLine.frame.origin.y+biggerLine.frame.size.height+margin, 30, 30)];
    publicImageView.image = [UIImage imageNamed:@"private"];
    [view addSubview:publicImageView];
    
    font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    attributes = @{NSFontAttributeName:font};
    constrintSize = CGSizeMake(rect.size.width-20, NSUIntegerMax);//CGSizeMake(biggerLine.frame.size.width-(publicImageView.frame.origin.x)-publicImageView.frame.size.height, NSUIntegerMax);
    rect = [@"Your pics will never be made public." boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    CGFloat yPos = (rect.size.height<publicImageView.frame.size.width) ? publicImageView.frame.origin.y + (publicImageView.frame.size.height-rect.size.height)/2 : publicImageView.frame.origin.y;
    
    UILabel *publicLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(publicImageView.frame)+10, yPos, rect.size.width, rect.size.height)];
    publicLbl.text = @"Your pics will never be made public.";
    publicLbl.textColor = [UIColor colorWithRed:0.267 green:0.267 blue:0.267 alpha:1.0];
    publicLbl.font = font;
    publicLbl.numberOfLines = 0;
    [publicLbl sizeToFit];
    [view addSubview:publicLbl];
    
    yPos = publicImageView.frame.origin.y;
    if(rect.size.height<publicImageView.frame.size.width) {
        yPos = yPos+ publicImageView.frame.size.height + margin;
        height = height + margin + publicImageView.frame.size.height;
    }else {
        yPos = yPos+ rect.size.height + margin;
        height = height + margin + rect.size.height;
    }
    UIView *smallLine = [[UIView alloc] initWithFrame:CGRectMake(publicImageView.frame.origin.x, yPos, self.bounds.size.width-2*publicImageView.frame.origin.x, 0.75)];
    smallLine.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [view addSubview:smallLine];
    
    
    height = height + margin + smallLine.frame.size.height;
    // delete view
    
    UIImageView *deleteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(smallLine.frame.origin.x, smallLine.frame.origin.y+smallLine.frame.size.height+margin, 30, 30)];
    deleteImageView.image = [UIImage imageNamed:@"delete"];
    [view addSubview:deleteImageView];
    
    rect = [@"All your pics are permanently deleted when you delete your account." boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    yPos = (rect.size.height<deleteImageView.frame.size.width) ? deleteImageView.frame.origin.y + (deleteImageView.frame.size.height-rect.size.height)/2 : deleteImageView.frame.origin.y;
    
    UILabel *deleteLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(deleteImageView.frame)+10, yPos, rect.size.width, rect.size.height)];
    deleteLbl.text = @"All your pics are permanently deleted when you delete your account.";
    deleteLbl.textColor = [UIColor colorWithRed:0.267 green:0.267 blue:0.267 alpha:1.0];
    deleteLbl.font = font;
    deleteLbl.numberOfLines = 0;
    [deleteLbl sizeToFit];
    [view addSubview:deleteLbl];
    
    yPos = deleteImageView.frame.origin.y;
    if(rect.size.height<deleteImageView.frame.size.width) {
        yPos = yPos+ deleteImageView.frame.size.height + margin;
        height = height + margin + deleteImageView.frame.size.height;
    }else {
        yPos = yPos+ rect.size.height + margin;
        height = height + margin + rect.size.height;
    }
    
    UIView *biggerLine1 = [[UIView alloc] initWithFrame:CGRectMake(20, yPos, view.frame.size.width-40, 0.75)];
    biggerLine1.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [view addSubview:biggerLine1];
    
    height = height + margin + biggerLine1.frame.size.height;
    
    // add profile pic button
    
    UIButton *profileButton = [[UIButton alloc] initWithFrame:CGRectMake(biggerLine1.frame.origin.x+(biggerLine1.frame.size.width-155)/2, biggerLine1.frame.origin.y+biggerLine1.frame.size.height+28, 155, 44)];
    [profileButton setTitleColor:[UIColor colorWithRed:0.855 green:0.267 blue:0.478 alpha:1] forState:UIControlStateNormal];
    [profileButton setTitle:@"Add Profile Pic" forState:UIControlStateNormal];
    profileButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    profileButton.backgroundColor = [UIColor whiteColor];
    profileButton.layer.cornerRadius = 6;
    profileButton.layer.borderWidth = 1;
    profileButton.layer.borderColor = [UIColor colorWithRed:0.855 green:0.267 blue:0.478 alpha:1].CGColor;
    [profileButton addTarget:self action:@selector(addProfilePressed) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:profileButton];
    
    height = height + margin + profileButton.frame.size.height;
    
    // skip button
    UIButton *skipButton = [[UIButton alloc] initWithFrame:CGRectMake(biggerLine1.frame.origin.x+(biggerLine1.frame.size.width-100)/2, profileButton.frame.origin.y+profileButton.frame.size.height+margin, 100, 44)];
    [skipButton setTitleColor:[UIColor colorWithRed:0.855 green:0.267 blue:0.478 alpha:1] forState:UIControlStateNormal];
    [skipButton setTitle:@"No Thanks" forState:UIControlStateNormal];
    skipButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    skipButton.backgroundColor = [UIColor whiteColor];
    [skipButton addTarget:self action:@selector(skipButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:skipButton];
    
    height = height + margin + skipButton.frame.size.height;
    
    CGRect frm = view.frame;
    frm.origin.y = (self.bounds.size.height-height)/2;
    frm.size.height = height;
    view.frame = frm;
}

-(void)addProfilePressed
{
    if(self.delegate){
        [self.delegate didAddProfileClick];
    }
}

-(void)skipButtonPressed
{
    if(self.delegate){
        [self.delegate didSkipClick];
    }
}

@end

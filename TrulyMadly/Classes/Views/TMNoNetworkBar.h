//
//  TMNoNetworkBar.h
//  TrulyMadly
//
//  Created by Ankit on 24/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMNoNetworkBar : UIView

@property(nonatomic,strong)UILabel *infoLabel;

-(void)showWithWidth:(CGFloat)width;
-(void)hide;

@end

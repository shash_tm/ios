//
//  TMUserHashTagView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMUserHashTagViewDelegate;

@interface TMUserHashTagView : UIView

@property(nonatomic,weak)id<TMUserHashTagViewDelegate> hashTagDelegate;

-(instancetype)initWithFrame:(CGRect)frame withTitleText:(NSString*)text showCloseImage:(BOOL)showCloseImage isSelected:(BOOL)isSelected;

@end

@protocol TMUserHashTagViewDelegate <NSObject>

-(void)didTapOnHashTag:(NSString *)hashtag;

@end
//
//  TMUserHashTagView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/01/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMUserHashTagView.h"

#define IS_PHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_PHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568.0)

@implementation TMUserHashTagView

-(instancetype)initWithFrame:(CGRect)frame withTitleText:(NSString*)text showCloseImage:(BOOL)showCloseImage isSelected:(BOOL)isSelected{
    
    self = [super initWithFrame:frame];
    
    if(self) {
        
        self.backgroundColor = (isSelected) ? [UIColor colorWithRed:179.0/255.0 green:207.0/255.0 blue:238.0/255.0 alpha:1.0] : [UIColor clearColor];
        self.layer.cornerRadius = 15;
        self.layer.borderWidth = (isSelected) ? 0 : 1;
        self.layer.borderColor = [UIColor colorWithRed:179.0/255.0 green:207.0/255.0 blue:238.0/255.0 alpha:1.0].CGColor;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [self addGestureRecognizer:tapGesture];
        
        CGFloat widthOffset = 30;
        if(!showCloseImage){
            widthOffset = 10;
        }
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.bounds.size.width-widthOffset, 20)];
        lb.font = (IS_PHONE_5 || IS_PHONE_4) ? [UIFont systemFontOfSize:14.0] : [UIFont systemFontOfSize:15.0];
        lb.textColor = (isSelected) ? [UIColor whiteColor] : [UIColor colorWithRed:0.467 green:0.467 blue:0.467 alpha:1];
        lb.backgroundColor = [UIColor clearColor];
        lb.text = text;
        [self addSubview:lb];
        
        if(showCloseImage) {
            CGFloat imgWidth = (IS_PHONE_5 || IS_PHONE_4) ? 12 : 15;
            UIImageView *cancelImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-imgWidth-5,
                                                                                   (self.frame.size.height - imgWidth)/2,
                                                                                   imgWidth,
                                                                                   imgWidth)];
            cancelImgView.backgroundColor = [UIColor clearColor];
            cancelImgView.image = [UIImage imageNamed:@"remove_circle"];
            [self addSubview:cancelImgView];
        }
        
    }
    return self;
}

-(void)tapAction:(UITapGestureRecognizer *)sender{
    if(self.hashTagDelegate) {
        NSString *hashtag = @"";
        
        NSArray *subviews = [self subviews];
        for (int i=0; i<subviews.count; i++) {
            id subview = [subviews objectAtIndex:i];
            if([subview isKindOfClass:[UILabel class]]){
                UILabel *lbl = [subviews objectAtIndex:i];
                hashtag = lbl.text;
                break;
            }
        }
        [self.hashTagDelegate didTapOnHashTag:hashtag];
    }
}

@end
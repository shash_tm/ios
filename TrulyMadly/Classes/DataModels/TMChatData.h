//
//  TMChatData.h
//  TrulyMadly
//
//  Created by Ankit on 10/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TMChatData <NSObject>

@property(nonatomic,assign)NSUInteger userId;
@property(nonatomic,assign)NSUInteger matchId;

@end

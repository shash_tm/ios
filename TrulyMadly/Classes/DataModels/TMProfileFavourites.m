//
//  TMProfileFavourites.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileFavourites.h"
#import "TMFavouriteItem.h"
#import "TMEnums.h"

#define FAV_VIEW_BASETAG   20000
#define FAVLIST_CONTENT_HEIGHT  54

@interface TMProfileFavourites ()

@property(nonatomic,assign)BOOL activeIndexFound;

@end


@implementation TMProfileFavourites

-(instancetype)initWithfavouriteData:(NSDictionary*)favouriteData {
    self = [super init];
    if(self) {
        self.favItems = [NSMutableArray array];
        self.activeIndex = -1;
        self.activeIndexFound = false;
        self.favIconWidth = 0;
        self.favListContentHeight = FAVLIST_CONTENT_HEIGHT;
        self.isFavsEmpty = true;
        
        //create array of fav items
        NSArray *favTypes = @[@"movies",@"music",@"books",@"travel",@"other"];
        NSInteger activeIndex = 0;
        
        for(NSString *favType in favTypes) {
            TMFavouriteItem *favItem = [[TMFavouriteItem alloc] init];
            NSArray *favList = NULL;
            if(favouriteData) {
                NSString *favTypeKey = [NSString stringWithFormat:@"%@_favorites",favType];
                favList = favouriteData[favTypeKey];
                [favItem configureFavouriteItem:favList withKey:favType];
            }
            
            [self.favItems addObject:favItem];
            
            if(favItem.status) {
                if(!self.activeIndexFound) {
                    self.activeIndexFound = true;
                    self.activeIndex = activeIndex;
                }
            }else {
                self.isFavsEmpty = false;
            }
            activeIndex++;
        }
    }
    return self;
}

-(BOOL)isFavsAvailable {
    if(self.activeIndex != -1) {
        return true;
    }
    else {
        return false;
    }
}

-(NSString*)favHeaderText {
    return @"Favourites";
}

-(TMProfileCollectionViewCellType)favRowType {
    TMProfileCollectionViewCellType cellType = TMPROFILECOLLECTIONVIEWCELL_FAVOURITE;
    return cellType;
}

-(TMProfileCollectionViewCellType)favListRowType {
    TMProfileCollectionViewCellType cellType = TMPROFILECOLLECTIONVIEWCELL_FAVOURITE_LIST;
    return cellType;
}

-(CGFloat)favouriteContentHeight:(CGFloat)maxWidth withRow:(NSInteger)row  {
    CGFloat contentHeight = 0;
    if(row == 0) {
        self.favIconWidth = [self iconWidth:maxWidth];
        contentHeight = [self iconHeight];
        contentHeight += self.headerHeight;
    }
    else if(row == 1) {
        contentHeight = 64;
    }
    return contentHeight;
}

-(CGFloat)iconWidth:(CGFloat)maxWidth  {
    CGFloat iconWidth = maxWidth/5;//(maxWidth - (45*4))/5;
    return iconWidth;
}
-(CGFloat)iconHeight {
    return 48;
}
-(TMFavouriteItem*)favItemAtActiveIndex {
    TMFavouriteItem *favItem = [self.favItems objectAtIndex:self.activeIndex];
    return favItem;
}

-(NSArray*)commonFavAtIndex {
    return self.commonFav[self.activeIndex];
}

@end


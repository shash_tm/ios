//
//  TMChatContext.h
//  TrulyMadly
//
//  Created by Ankit on 12/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMChatContext : NSObject

@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,assign)NSInteger loggedInUserId;
@property(nonatomic,strong)NSString *clearChatTs;

- (instancetype)initWithLoggedInUser;


@end

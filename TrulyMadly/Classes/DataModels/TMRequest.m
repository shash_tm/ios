//
//  TMRequest.m
//  TrulyMadly
//
//  Created by Ankit on 05/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMRequest.h"
#import "TMUserSession.h"
#import "TMCommonUtil.h"

@interface TMRequest ()

@property(nonatomic,strong)NSDictionary *postRequestParamsDict;
@property(nonatomic,strong)NSDictionary *getRequestParamsDict;


@end

@implementation TMRequest

-(instancetype)initWithUrlString:(NSString*)urlString {
    self = [super init];
    if(self) {
        self.urlString = urlString;
    }
    return self;
}

-(NSDictionary*)httpHeaders {
    TMUserSession *userSession = [TMUserSession sharedInstance];
    
    NSString *cookieStr = [userSession getCookieParam];
    NSString *deviceId = [TMCommonUtil vendorId];
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    NSMutableDictionary *httpHeaderDict = [NSMutableDictionary dictionaryWithCapacity:16];
    [httpHeaderDict setObject:@"true" forKey:@"login_mobile"];
    [httpHeaderDict setObject:[TMCommonUtil buildNumberString] forKey:@"app_version_code"];
    [httpHeaderDict setObject:[TMCommonUtil appVersionString] forKey:@"app_version_name"];
    [httpHeaderDict setObject:@"iOSApp" forKey:@"source"];
    [httpHeaderDict setObject:@"iOS" forKey:@"app"];
    [httpHeaderDict setObject:deviceId forKey:@"device_id"];
    [httpHeaderDict setObject:osVersion forKey:@"osversion"];
    if(cookieStr) {
        [httpHeaderDict setObject:cookieStr forKey:@"Cookie"];
    }
    return httpHeaderDict;
}

-(void)setGetRequestParams:(NSDictionary*)getParamsDict {
    self.getRequestParamsDict = getParamsDict;
}

-(NSDictionary*)httpGetRequestParameters {
    return self.getRequestParamsDict;
}

-(void)setPostRequestParameters:(NSDictionary*)postParamsDict {
    self.postRequestParamsDict = postParamsDict;
}
-(NSDictionary*)httpPostRequestParameters {
    return self.postRequestParamsDict;
}

@end





//
//  TMStickerGallery.m
//  StickerScrollableMenu
//
//  Created by Abhijeet Mishra on 17/08/15.
//  Copyright (c) 2015 Abhijeet Mishra. All rights reserved.
//

#import "TMStickerGallery.h"

@implementation TMStickerGallery


- (instancetype) initWithGalleryID:(NSString*) galleryID iconImage:(UIImage*) iconImage
{
    
    if (self = [super init]) {
        self.galleryID = galleryID;
        self.iconImage = iconImage;
        return self;
    }
    
    return nil;
}

@end

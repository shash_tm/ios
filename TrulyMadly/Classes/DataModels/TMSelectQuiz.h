//
//  TMSelectQuiz.h
//  TrulyMadly
//
//  Created by Ankit on 02/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@interface TMSelectQuiz : NSObject

@property(nonatomic,strong,readonly)NSString *quizText;
@property(nonatomic,strong,readonly)NSString *quizImageURLString;
@property(nonatomic,assign,readonly)TMSelectQuizAnswer *quizAnswer;
@property(nonatomic,assign,readonly)NSInteger questionId;
@property(nonatomic,assign,readonly)NSInteger yesOptionId;
@property(nonatomic,assign,readonly)NSInteger noOptionId;

- (instancetype)initWithQuizDictionary:(NSDictionary*)quizDictionary;
-(void)setQuizAnswerWithYesOptionId;
-(void)setQuizAnswerWithNoOptionId;
-(TMSelectQuizAnswer)quizAnswerStatus;
-(NSString*)getSelectedOptionId;
-(void)removeSelectedOtionId;

@end

//
//  TMProfileVerification.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileVerification.h"
#import "TMVerification.h"
#import "NSString+TMAdditions.h"


@interface TMProfileVerification()

@property(nonatomic,assign)CGFloat contentMaxWidth;

@end

@implementation TMProfileVerification

-(instancetype)initWithTrustMeterDictionary:(NSDictionary*)trustMeterDict endorsements:(NSArray*)endorsements trustScores:(NSDictionary *)trustScores editMode:(BOOL)editMode {
    self = [super init];
    if(self) {
        NSString *trustScore = trustMeterDict[@"trust_score"];
        if( trustScore != nil ) {
            if(![trustScore isKindOfClass:[NSNull class]]) {
                self.trustScore = trustScore;
            }
        }
        else {
            self.trustScore = @"0";
        }
        
        self.endorsements = endorsements;
        self.trustScores = trustScores;
        self.editMode = editMode;
        ////////////////////
        TMVerification *fbVerification = [self facebookVerification:trustMeterDict];
        TMVerification *liVerification = [self linkedInProofVerification:trustMeterDict];
        TMVerification *photoIdVerification = [self idProofVerification:trustMeterDict];
        TMVerification *mnVerification = [self mobileNumberVerification:trustMeterDict];
        TMVerification *endorsementVarificaton = [self endorsementVerification];
        
        self.trustMeters = @[fbVerification,mnVerification,liVerification,photoIdVerification,endorsementVarificaton];
    }
    return self;
}


-(NSString*)verificationHeaderText {
    return @"Trust Score";
}

-(NSString*)verificationSubHeaderText {
    return @"A higher trust score could mean more matches!";//@"A higher trust score will get you more responses!";
}

-(NSString*)trustScoreImageName {
    return @"score_active";
}

-(TMVerification*)facebookVerification:(NSDictionary*)trustMeterDict  {
    
    NSString *fbConnection = trustMeterDict[@"fb_connections"];
    BOOL isFbConnectionActive = false;
    self.isFbConnected = false;
    if([fbConnection isValidObject]){
        isFbConnectionActive = true;
        self.isFbConnected = true;
    }
    
    NSString *text = NULL;
    NSString *imageName = NULL;
    
    if(isFbConnectionActive) {
        int connection = [fbConnection intValue];
        if (connection == 0) {
            text = @"";
        }else {
            text = [NSString stringWithFormat:@"%@ ",fbConnection];
        }
        //imageName = @"facebook_highlight";
        imageName = (self.editMode) ? @"facebook_profile_edit" : @"facebook_profile";
    }
    else {
        text = @"";
       // imageName = @"facebook_inactive";
        imageName = (self.editMode) ? @"facebook_profile_edit_inactive" : @"facebook_profile_inactive";
    }
    
    TMVerification *fbVerification = [[TMVerification alloc] init];
    fbVerification.state = isFbConnectionActive;
    fbVerification.text = text;
    fbVerification.imageName = imageName;
    NSString *fbScore = [NSString stringWithFormat:@"%@", self.trustScores[@"fb"]];
    fbVerification.score = [fbScore stringByAppendingString:@"%"];
    
    return fbVerification;
}
-(TMVerification*)idProofVerification:(NSDictionary*)trustMeterDict {
    
    NSString *idProof = trustMeterDict[@"id_proof"];
    BOOL isIdProofActive = false;
    
    if([idProof isValidObject]){
        isIdProofActive = true;
    }
    
    NSString *text = NULL;
    NSString *imageName = NULL;
    if(isIdProofActive) {
        text = trustMeterDict[@"id_proof_type"];
        //imageName = @"PhotoID_Highlight";
        imageName = (self.editMode) ? @"photo id_profile_edit" : @"photo id_profile";
    }
    else {
        text = @"Photo ID";
       // imageName = @"photoid_inactive";
        imageName = (self.editMode) ? @"photo id_profile_edit_inactive" : @"photo id_profile_inactive";
    }
    
    TMVerification *empVerification = [[TMVerification alloc] init];
    empVerification.state = isIdProofActive;
    empVerification.text = text;
    empVerification.imageName = imageName;
    NSString *idScore = [NSString stringWithFormat:@"%@", self.trustScores[@"photo_id"]];
    empVerification.score = [idScore stringByAppendingString:@"%"];
    
    return empVerification;
}
-(TMVerification*)linkedInProofVerification:(NSDictionary*)trustMeterDict  {
    
    NSString *linkedInConnection = trustMeterDict[@"linkedin_connections"];
    NSString *linkedDesignation = trustMeterDict[@"linkedin_designation"];
    BOOL isLinkedInActive = false;
    
    if( ([linkedInConnection isValidObject]) || ([linkedDesignation isValidObject]) ){
        isLinkedInActive = true;
    }
    
    NSString *text = NULL;
    NSString *imageName = NULL;
    if(isLinkedInActive) {
        text = [NSString stringWithFormat:@"%@ ", linkedInConnection];//((linkedDesignation != nil) ? linkedDesignation : linkedInConnection);
       // imageName = @"plinkedin_active";
        imageName = (self.editMode) ? @"linkedin_profile_edit" : @"linkedin_profile";
    }
    else {
        text = @"";
       // imageName = @"plinkedin_inactive";
        imageName = (self.editMode) ? @"linkedin_profile_edit_inactive" : @"linkedin_profile_inactive";
    }
    
    TMVerification *empVerification = [[TMVerification alloc] init];
    empVerification.state = isLinkedInActive;
    empVerification.text = text;
    empVerification.imageName = imageName;
    NSString *linkedinScore = [NSString stringWithFormat:@"%@", self.trustScores[@"linkedin"]];
    empVerification.score = [linkedinScore stringByAppendingString:@"%"];
    
    return empVerification;
}
-(TMVerification*)mobileNumberVerification:(NSDictionary*)trustMeterDict  {
    NSString *mobileNo = trustMeterDict[@"mobile_number"];
    BOOL isMobileNoActive = false;
    
    if([mobileNo isValidObject]){
        isMobileNoActive = true;
    }
    
    NSString *text = NULL;
    NSString *imageName = NULL;
    if(isMobileNoActive) {
        text = mobileNo;
       // imageName = @"Phone_Highlight";
        imageName = (self.editMode) ? @"phone_profile_edit" : @"phone_profile";
    }
    else {
        text = @"Phone Number";
       // imageName = @"phone_inactive";
        imageName = (self.editMode) ? @"phone_profile_edit_inactive" : @"phone_profile_inactive";
    }
    
    TMVerification *empVerification = [[TMVerification alloc] init];
    empVerification.state = isMobileNoActive;
    empVerification.text = text;
    empVerification.imageName = imageName;
    NSString *phoneScore = [NSString stringWithFormat:@"%@", self.trustScores[@"phone"]];
    empVerification.score = [phoneScore stringByAppendingString:@"%"];
    
    return empVerification;
}

-(TMVerification*)endorsementVerification {
    BOOL isEndorsementActive = false;
    
    if(self.endorsements != nil) {
        isEndorsementActive = true;
    }
    
    NSString *text = NULL;
    NSString *imageName = NULL;
    if(isEndorsementActive) {
        text = [NSString stringWithFormat:@"%ld ", (unsigned long)[self.endorsements count]];
        //imageName = @"endorse_highlight";
        imageName = (self.editMode) ? @"references_profile_edit" : @"references_profile";
    }
    else {
        text = @"";
       // imageName = @"endorse_inactive";
        imageName = (self.editMode) ? @"references_profile_edit_inactive" : @"references_profile_inactive";
    }
    
    TMVerification *endorseVerification = [[TMVerification alloc] init];
    endorseVerification.state = isEndorsementActive;
    endorseVerification.text = text;
    endorseVerification.imageName = imageName;
    NSString *endorseScore = [NSString stringWithFormat:@"%@", self.trustScores[@"endorsements"]];
    endorseVerification.score = [endorseScore stringByAppendingString:@"%"];
    
    return endorseVerification;
}

-(BOOL)isEndorsementVerificationAvailable {
    BOOL isEndorsementActive = false;
    
    if(self.endorsements != nil) {
        isEndorsementActive = true;
    }
    return isEndorsementActive;
}

-(CGFloat)varificationContentHeight:(CGFloat)maxWidth {
    CGFloat baseYPos = self.contentInset.top + self.headerHeight;
    CGFloat contentHeight = baseYPos;
    
    CGFloat editModeWidthOffset = (self.editMode) ? 25 : 0;
    self.contentMaxWidth = maxWidth - editModeWidthOffset;
    CGFloat subHeaderHeight = [self sizeForContent:[self verificationSubHeaderText]].height;
    contentHeight = contentHeight+ subHeaderHeight;
        
//    CGFloat heightPerVerificationCell = 26;
//    CGFloat yOffsetBetweenVerificationCell = 6;

//    for (int i=0; i<self.trustMeters.count; i++) {
//        TMVerification *verf = [self.trustMeters objectAtIndex:i];
//        if(!self.editMode && verf.state) {
//            contentHeight = contentHeight + heightPerVerificationCell;
//        }
//        else if(self.editMode) {
//            contentHeight = contentHeight + heightPerVerificationCell;
//        }
//        if(i<self.trustMeters.count-1) {
//            contentHeight = contentHeight + yOffsetBetweenVerificationCell;
//        }
//    }
//    if(self.endorsements != nil) {
//        contentHeight = contentHeight + 10 + 90;
//    }
    return contentHeight + 55;
}

-(CGSize)sizeForContent:(NSString*)content {
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:11.0]};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth, NSUIntegerMax);
    CGRect rect = [content boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}

@end


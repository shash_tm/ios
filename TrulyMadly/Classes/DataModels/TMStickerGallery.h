//
//  TMStickerGallery.h
//  StickerScrollableMenu
//
//  Created by Abhijeet Mishra on 17/08/15.
//  Copyright (c) 2015 Abhijeet Mishra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface TMStickerGallery : NSObject

@property (nonatomic, strong) NSString* galleryID;

@property (nonatomic, strong) UIImage* iconImage;

- (instancetype) initWithGalleryID:(NSString*) galleryID iconImage:(UIImage*) iconImage;

@end

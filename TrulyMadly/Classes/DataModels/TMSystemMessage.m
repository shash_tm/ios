//
//  TMSystemMessage.m
//  TrulyMadly
//
//  Created by Ankit on 14/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMSystemMessage.h"

@interface TMSystemMessage ()

@property(nonatomic,strong)NSDictionary* systemMessageDictionary;

@end

@implementation TMSystemMessage

-(instancetype)initWithSystemMessgaeDictionary:(NSDictionary*)dictionary {
    self = [super init];
    if(self) {
        self.systemMessageDictionary = dictionary;
    }
    return self;
}

-(BOOL)isFirstTileAvailable {
    BOOL isFirstTileAvailable = false;
    NSString *firstTileString = self.systemMessageDictionary[@"first_tile"];
    if( firstTileString != nil) {
        isFirstTileAvailable = true;
    }
    return isFirstTileAvailable;
}

-(NSString*)getFirstTileTitle {
    NSString* firstTileString = self.systemMessageDictionary[@"first_tile"];
    return firstTileString;
}

-(NSString*)getFirstTileLink {
    NSString *firstTileLink = self.systemMessageDictionary[@"first_tile_link"];
    return firstTileLink;
}

-(NSString*)firstTileButtonTitle {
    NSString* firstTileButtonTitle = self.systemMessageDictionary[@"first_tile_button"];
    return firstTileButtonTitle;
}

//////////

-(NSString*)lastTileMessage {
    NSString* firstTileString = self.systemMessageDictionary[@"last_tile_msg"];
    return firstTileString;
}

-(NSString*)getLastTileLink {
    NSString *firstTileLink = self.systemMessageDictionary[@"last_tile_link"];
    return firstTileLink;
}

-(TMSystemMessageLinkType)getSystemMessageLinkTypeForFirstTile {
    
    TMSystemMessageLinkType systemMessageLink = SYSTEM_MESSAGE_LINK_NONE;
    NSString* firstTileLink = [self getFirstTileLink];
    
    if(firstTileLink != nil) {
        if([firstTileLink isEqualToString:@"upload_photo"]) {
            systemMessageLink = SYSTEM_MESSAGE_LINK_TYPE_PHOTOUPLOAD;
        }
        else if([firstTileLink isEqualToString:@"show_matches"]) {
            systemMessageLink = SYSTEM_MESSAGE_LINK_SHOWMATCHES;
        }
    }
    return systemMessageLink;
}

-(TMSystemMessageLinkType)getSystemMessageTypeForLastTileLink {
    TMSystemMessageLinkType systemMessageLink = SYSTEM_MESSAGE_LINK_NONE;
    NSString* lastTileLink = [self getLastTileLink];
    
    if(lastTileLink) {
        if([lastTileLink isEqualToString:@"invite_friends"]) {
            systemMessageLink = SYSTEM_MESSAGE_TYPE_INVITEFRIEND;
        }
        else if([lastTileLink isEqualToString:@"trustbuilder"]) {
            systemMessageLink = SYSTEM_MESSAGE_LINK_TRUSTBUILDER;
        }
    }
    return systemMessageLink;
}

//////////////////
-(NSString*)getLikeActionMessage {
    NSString *likeActionMessage = self.systemMessageDictionary[@"like_hide_message"];
    return likeActionMessage;
}

-(NSDictionary*)deactivationDictionary {
    NSDictionary* deactivationDict = self.systemMessageDictionary[@"deactivation_msgs"];
    return deactivationDict;
}


@end


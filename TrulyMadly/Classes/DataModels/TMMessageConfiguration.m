//
//  TMMessageConfiguration.m
//  TrulyMadly
//
//  Created by Ankit on 27/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageConfiguration.h"
#import "TMUserSession.h"

@interface TMMessageConfiguration ()

@property(nonatomic,assign)NSInteger hostUserId;

@end

@implementation TMMessageConfiguration

-(instancetype)init {
    self = [super init];
    if(self) {
        TMUser *user = [TMUserSession sharedInstance].user;
        NSInteger userId = user.userId.integerValue;
        self.hostUserId = userId;
    }
    return self;
}

@end

//
//  TMProfileCompatibility.h
//  TrulyMadly
//
//  Created by Ankit on 24/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMProfileItem.h"

@class TMProfileLikes;
@class TMCompatibilityQuiz;

@interface TMProfileCompatibility : TMProfileItem

@property(nonatomic,strong)TMProfileLikes *commonLike;
@property(nonatomic,strong)TMCompatibilityQuiz *valueQuiz;
@property(nonatomic,strong)TMCompatibilityQuiz *adapQuiz;
@property(nonatomic,assign)CGFloat horizontalInterimMargin;
@property(nonatomic,assign)BOOL isCommonLikeActive;
@property(nonatomic,assign)CGFloat verticalTitleHeight;
@property(nonatomic,assign)CGFloat iconWidth;


-(instancetype)initWithMatchDetail:(NSDictionary*)matchDetail
                   withCommonLikes:(NSArray*)commonLikes;

-(CGFloat)compatibilityContentHeight:(CGFloat)maxWidth row:(NSInteger)row;
-(NSInteger)compabilityItemCount;
-(NSString*)compatibilityHeaderText;

-(NSString*)valueHeaderText;
-(NSString*)adaptibilityHeaderText;

@end

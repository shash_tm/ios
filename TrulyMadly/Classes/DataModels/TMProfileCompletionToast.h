//
//  TMProfileCompletionToast.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMProfileCompletionToast : NSObject

-(BOOL)willShowTrustToast;
-(BOOL)willShowIncompleteProfileToast;
-(void)setTrustCancel;
-(void)setEditProfileCancel;
-(void)setDailyParameterForTrustBuilder;
-(void)setDailyParameterForProfile;
-(BOOL)willShowPIDashboard;
-(void)setPiActivityCount;
-(void)setNudgeCount;
-(BOOL)canInterstitialAdShown;

@end

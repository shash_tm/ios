//
//  TMCompatibilityQuiz.m
//  TrulyMadly
//
//  Created by Ankit on 24/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCompatibilityQuiz.h"
#import "TMSwiftHeader.h"




@interface TMCompatibilityQuiz ()

@end

@implementation TMCompatibilityQuiz

-(instancetype)initWithQuizType:(TMQuizScoreType)quizType withQuizScore:(NSInteger)quizScore {
    self = [super init];
    if(self) {
        self.quizType = quizType;
        self.quizScore = quizScore;
    }
    return self;
}

-(NSString*)compatibilityQuizScoreText {
    NSString *text = NULL;
    TMUser *user = [TMUserSession sharedInstance].user;
    if(self.quizScore != 0) {
        if( (self.quizType == TMQUIZSCORETYPE_VALUE) && ([user isValuesQuizFilledByUser]) ) {
            text = [self textForQuizScore];
        }
        else if( (self.quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) &&
                ([user isInterPersonalQuizFilledByUser]) ) {
            text = [self textForQuizScore];
        }
        else {
            text = @"Take Quiz";
        }
    }
    else {
        if( (self.quizType == TMQUIZSCORETYPE_VALUE) && ([user isValuesQuizFilledByUser]) ) {
            text = @"Quiz Pending";
        }
        else if((self.quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) && ([user isInterPersonalQuizFilledByUser]) ) {
            text = @"Quiz Pending";
        }
        else {
            text = @"Take Quiz";
        }
    }
    return text;
}

//1 = Low   //2 = Decent  //3 = Good   //4 = Superb
-(NSString*)textForQuizScore {
    NSString* scoreText = NULL;
    if(self.quizScore == 1) {
        scoreText = @"LOW";
    }
    else if(self.quizScore == 2) {
        scoreText = @"DECENT";
    }
    else if(self.quizScore == 3) {
        scoreText = @"GOOD";
    }
    else if(self.quizScore == 4) {
        scoreText = @"SUPERB";
    }
    return scoreText;
}

-(UIFont*)titleTextFont {
    UIFont* font = NULL;
    if(self.quizScore != 0) {
        font = [UIFont systemFontOfSize:13];
    }
    else {
        font = [UIFont systemFontOfSize:12];
    }
    return font;
}

-(BOOL)isQuizFilledByUser {
    TMUser *user = [TMUserSession sharedInstance].user;
    if( (self.quizType == TMQUIZSCORETYPE_VALUE) && ([user isValuesQuizFilledByUser]) ) {
        return true;
    }
    else if((self.quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) && ([user isInterPersonalQuizFilledByUser]) ) {
        return true;
    }
    return false;
}

-(BOOL)isInteractive {
    TMUser *user = [TMUserSession sharedInstance].user;
    if( (self.quizType == TMQUIZSCORETYPE_VALUE) && ([user isValuesQuizFilledByUser]) ) {
        return false;
    }
    else if((self.quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) && ([user isInterPersonalQuizFilledByUser]) ) {
        return false;
    }
    return true;
}

-(NSString*)quizImageString {
    TMUser *user = [TMUserSession sharedInstance].user;
    NSString *imgStr = NULL;
    if(self.quizType == TMQUIZSCORETYPE_VALUE) {
        if([user isValuesQuizFilledByUser] && self.quizScore != 0) {
            imgStr = @"score_active";
        }
        else {
            imgStr = @"score_inactive";
        }
        
    }
    else if(self.quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) {
        if([user isInterPersonalQuizFilledByUser] && self.quizScore != 0) {
            imgStr = @"score_active";
        }
        else {
            imgStr = @"score_inactive";
        }
    }
    return imgStr;
}

-(NSString*)quizImageStringForEditMode {
    TMUser *user = [TMUserSession sharedInstance].user;
    NSString *imgStr = NULL;
    if(self.quizType == TMQUIZSCORETYPE_VALUE) {
        if([user isValuesQuizFilledByUser]) {
            imgStr = @"score_active";
        }
        else {
            imgStr = @"score_inactive";
        }
    }
    else if(self.quizType == TMQUIZSCORETYPE_VALUE_ADAPTABILITY) {
        if([user isInterPersonalQuizFilledByUser]) {
            imgStr = @"score_active";
        }
        else {
            imgStr = @"score_inactive";
        }
    }
    return imgStr;
}

-(UIColor*)quizTextColor {
    NSString *quizTextStr = [self compatibilityQuizScoreText];
    if([quizTextStr isEqualToString:@"Quiz Pending"]) {
        return [UIColor grayColor];
    }
    return [UIColor blackColor];
}


@end

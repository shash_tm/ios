//
//  TMProfileVerification.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileItem.h"

@interface TMProfileVerification : TMProfileItem

@property(nonatomic,strong)NSString *trustScore;
@property(nonatomic,assign)BOOL isFbConnected;
@property(nonatomic,strong)NSArray *trustMeters;
@property(nonatomic,strong)NSArray *endorsements;
@property(nonatomic,strong)NSDictionary *trustScores;          // base scores for fb, linkedin etc.

-(instancetype)initWithTrustMeterDictionary:(NSDictionary*)trustMeterDict
                               endorsements:(NSArray*)endorsements
                                trustScores:(NSDictionary*)trustScores
                                   editMode:(BOOL)editMode;

-(NSString*)verificationHeaderText;
-(NSString*)trustScoreImageName;
-(CGFloat)varificationContentHeight:(CGFloat)maxWidth;
-(BOOL)isEndorsementVerificationAvailable;
-(NSString*)verificationSubHeaderText;

@end

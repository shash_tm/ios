//
//  TMProfile.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMProfileItem.h"
#import "TMProfileBasicInfo.h"
#import "TMEnums.h"


@class TMProfileLikes;
@class TMProfileWorkAndEdu;
@class TMProfileFavourites;
@class TMProfileCompatibility;
@class TMProfileVerification;
@class TMProfileSelectInfo;
@class TMAdTrackingModel;


@interface TMProfile : TMProfileItem

@property(nonatomic,strong)TMProfileBasicInfo *basicInfo;
@property(nonatomic,strong)TMProfileLikes *likes;
@property(nonatomic,strong)TMProfileWorkAndEdu *workAndEducation;
@property(nonatomic,strong)TMProfileFavourites *favourite;
@property(nonatomic,strong)TMProfileCompatibility *compatibility;
@property(nonatomic,strong)TMProfileVerification *varification;
@property(nonatomic,strong)TMProfileSelectInfo *selectInfo;
@property(nonatomic,strong)TMAdTrackingModel *adObj;
@property(nonatomic,strong)NSDictionary *trustScores;
@property(nonatomic,strong)NSArray *mutualEventArr;
@property(nonatomic,strong)NSArray *commonAttributes;

@property(nonatomic,strong)NSString *likeUrl;
@property(nonatomic,strong)NSString *hideUrl;
@property(nonatomic,strong)NSString *matchId;
@property(nonatomic,strong)NSString *sparkHash;

@property(nonatomic,assign)BOOL isMutualEventAvail;
@property(nonatomic,assign)BOOL isPhotoVisibilityShown;
@property(nonatomic,assign)BOOL hasLikedBefore;

-(instancetype)initWithProfileData:(NSDictionary*)profileDictionary withEditMode:(BOOL)editMode trustScores:(NSDictionary*)trustScores;
-(instancetype)initWithProfileData:(NSDictionary*)profileDictionary trustScores:(NSDictionary*)trustScores;
-(NSInteger)sectionCount;
-(NSInteger)rowCountForSection:(NSInteger)section;
-(TMProfileCollectionViewCellType)cellType:(NSInteger)section row:(NSInteger)row;
-(CGFloat)contentLayoutHeight:(NSInteger)section row:(NSInteger)row maxWidth:(CGFloat)maxWidth;
-(BOOL)showSelectNudgeOnSparkAction;
-(BOOL)showSelectNudgeOnLikeAction;

//SeventyNine Profile Ad Changes
-(instancetype)initWithProfileAdData;
- (void)addLikesForProfileAd:(NSArray *)profileLikes;
@end

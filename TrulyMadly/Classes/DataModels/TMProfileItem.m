//
//  TMProfileItem.m
//  TrulyMadly
//
//  Created by Ankit on 24/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileItem.h"


@interface TMProfileItem ()

@end

@implementation TMProfileItem

-(instancetype)init {
    self = [super init];
    if(self) {
        self.contentInset = UIEdgeInsetsMake( 10, 0, 0, 10);
        self.headerHeight = 40;
        self.editMode = FALSE;
        self.isProfileAdCampaign = FALSE;
    }
    return self;
}

-(void)setEditMode:(BOOL)editMode {
    if(editMode) {
        _headerHeight = 40;
    }else {
        _headerHeight = 0;
    }
    _editMode = editMode;
}

-(CGFloat)spaceBetweenHeaderAndFirstViewComponent {
    return 10.0;
}



@end

//
//  TMProfileFavourites.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileItem.h"

@class TMFavouriteItem;

@interface TMProfileFavourites : TMProfileItem

@property(nonatomic,strong)NSMutableArray* favItems;
@property(nonatomic,assign)CGFloat favIconWidth;
@property(nonatomic,assign)NSInteger activeIndex;
@property(nonatomic,assign)NSInteger favListContentHeight;
@property(nonatomic,assign)BOOL isFavsEmpty;
@property(nonatomic,assign)BOOL isCommonFav;
@property(nonatomic,strong)NSMutableArray *commonFav;

-(instancetype)initWithfavouriteData:(NSDictionary*)favouriteData;
-(BOOL)isFavsAvailable;
-(NSString*)favHeaderText;
-(CGFloat)favouriteContentHeight:(CGFloat)maxWidth withRow:(NSInteger)row;
-(TMFavouriteItem*)favItemAtActiveIndex;
-(NSArray*)commonFavAtIndex;
-(CGFloat)iconHeight;

@end

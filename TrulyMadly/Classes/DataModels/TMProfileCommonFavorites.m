//
//  TMProfileCommonFavorites.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCommonFavorites.h"

@interface TMProfileCommonFavorites()

@property(nonatomic,strong)NSArray *commonMovies;
@property(nonatomic,strong)NSArray *commonMusic;
@property(nonatomic,strong)NSArray *commonTravel;
@property(nonatomic,strong)NSArray *commonBooks;
@property(nonatomic,strong)NSArray *commonOthers;

@end
@implementation TMProfileCommonFavorites

-(instancetype)initWithCommonfavouriteData:(NSDictionary *)favouriteData {
    self = [super init];
    if(self) {
        self.commonFav = [[NSMutableArray alloc] init];
        [self configureFavoriteData:favouriteData];
    }
    return self;
}

-(void)configureFavoriteData:(NSDictionary *)favouriteData {
    if(favouriteData) {
        if( [favouriteData[@"movies_favorites"] count]>0 || [favouriteData[@"music_favorites"] count]>0 || [favouriteData[@"travel_favorites"] count]>0 || [favouriteData[@"books_favorites"] count]>0 || [favouriteData[@"other_favorites"] count]>0 ) {
            self.hasCommonFavorites = true;
        }
        self.commonMovies = favouriteData[@"movies_favorites"];
        self.commonMusic = favouriteData[@"music_favorites"];
        self.commonTravel = favouriteData[@"travel_favorites"];
        self.commonBooks = favouriteData[@"books_favorites"];
        self.commonOthers = favouriteData[@"other_favorites"];
        
        [self.commonFav addObject:self.commonMovies];
        [self.commonFav addObject:self.commonMusic];
        [self.commonFav addObject:self.commonBooks];
        [self.commonFav addObject:self.commonTravel];
        [self.commonFav addObject:self.commonOthers];
    }
}

-(BOOL)isCommonFavoritesAvailable {
    return self.hasCommonFavorites;
}

-(NSMutableArray*)getCommonFav {
    return self.commonFav;
}

@end

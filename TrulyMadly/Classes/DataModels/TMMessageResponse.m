//
//  TMMessage.m
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageResponse.h"
#import "TMChatSeenContext.h"
#import "TMChatUser.h"

@implementation TMMessageResponse

-(instancetype)initWithResponse:(NSDictionary *)response {
    self = [super initWithResponse:response];
    if(self) {
        
        NSDictionary *dataDict = [response objectForKey:@"data"];
        NSDictionary *receiverDict = [dataDict objectForKey:@"receiver"];
        
        NSArray *msgList = [dataDict objectForKey:@"message_list"];
        if(msgList && msgList.count) {
            self.msgList = [NSMutableArray arrayWithArray:msgList];
        }
        self.userId = [[dataDict objectForKey:@"user_id"] integerValue];

        //chat user metadata
        self.chatUserData = [[TMChatUser alloc] initWithPollingResponse:receiverDict];
        
        ///chat seen context
        self.chatSeenContext = [[TMChatSeenContext alloc] init];
        self.chatSeenContext.lastSeenMessageId = receiverDict[@"last_seen_msg_id"];
        self.chatSeenContext.lastSeenMessageTimestamp = receiverDict[@"last_seen_receiver_tstamp"];
        self.chatSeenContext.matchId = [receiverDict[@"profile_id"] integerValue];
    }
    return self;
    
}

@end

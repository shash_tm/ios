//
//  TMImageDownload.h
//  TrulyMadly
//
//  Created by Ankit on 07/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Availability.h>
#import <UIKit/UIKit.h>


@protocol AFURLResponseSerialization, TMImageCache;
@protocol TMImageDownloadDelegate;

@interface TMImageDownload : NSObject

@property (nonatomic,weak) id <TMImageDownloadDelegate> delegate;
@property (nonatomic,strong) id <AFURLResponseSerialization> imageResponseSerializer;
@property(nonatomic,strong)NSDate* startDate;
@property(nonatomic,strong)NSString* cachingPath;

+(id <TMImageCache>)sharedImageCache;
+(void)setSharedImageCache:(id <TMImageCache>)imageCache;
-(void)getImageWithURL:(NSURL *)url;
-(void)cancelImageRequestOperation;
-(void)removeCachedImageDownloadedFromURL:(NSURL*)imageURL;
-(void)removeCachedImageDownloadedFromPath:(NSString*)imagePath;

@end

#pragma mark -

@protocol TMImageCache <NSObject>

- (UIImage*)cachedImageForRequest:(NSURLRequest *)request;

- (void)cacheImage:(UIImage *)image
        forRequest:(NSURLRequest *)request;

- (void)removeCachedImageForRequest:(NSURLRequest *)request;

@end

@protocol TMImageDownloadDelegate <NSObject>

-(void)didCompleteImageDownload:(UIImage*)imageDownloaded fromUrl:(NSURL*)imageUrl;
-(void)didFailToDownloadImageFromUrl:(NSURL*)imageUrl;

@end
//
//  QuizOptionDomainObject.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuizOptionDomainObject : NSObject

@property (nonatomic, strong) NSString* optionID;
@property (nonatomic, strong) NSString* optionText;
@property (nonatomic, strong) NSString* optionImageURL;

- (instancetype) initWithOptionDictionary:(NSDictionary*) optionDictionary;

@end

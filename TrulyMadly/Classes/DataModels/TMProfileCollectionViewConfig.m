//
//  TMProfileCollectionViewConfig.m
//  TrulyMadly
//
//  Created by Ankit on 04/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfileCollectionViewConfig.h"

@implementation TMProfileCollectionViewConfig

-(TMProfileCollectionViewCellType)getCellTypeForRow:(NSInteger)row {
    TMProfileCollectionViewCellType cellType;
    
    switch (self.sectionType) {
        case TMPROFILECOLLECTIONVIEWSECTION_SELECT:
            cellType = TMPROFILECOLLECTIONVIEWCELL_SELECT;
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_HASHTAG:
            cellType = TMPROFILECOLLECTIONVIEWCELL_HASHTAG;
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_WORKANDEDU:
            cellType = TMPROFILECOLLECTIONVIEWCELL_WORKANDEDU;
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_TRUSTSCORE:
            cellType = TMPROFILECOLLECTIONVIEWCELL_TRUSTSCORE;
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_FAVOURITE:
            if(row == 0){
                cellType = TMPROFILECOLLECTIONVIEWCELL_FAVOURITE;
            }
            else {
                cellType = TMPROFILECOLLECTIONVIEWCELL_FAVOURITE_LIST;
            }
            break;
        default:
            break;
    }
    return cellType;
}

@end

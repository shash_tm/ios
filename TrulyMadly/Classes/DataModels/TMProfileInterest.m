//
//  TMProfileInterest.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileInterest.h"
#import "NSString+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"

@implementation TMProfileInterest

-(instancetype)initWithTag:(NSString*)tag isCommonLike:(BOOL)commonLike {
    self = [super init];
    if(self) {
        self.tag = tag;
        self.commonLike = commonLike;
    }
    return self;
}

-(CGSize)tagSizeForMaximumWidth:(CGFloat)width {
    UIFont* font = [self fontForText];
    NSDictionary *attDict = @{NSFontAttributeName:font};
    CGSize contrintSize = CGSizeMake(width, NSUIntegerMax);
    CGRect stringRect = [self.tag boundingRectWithConstraintSize:contrintSize attributeDictionary:attDict];
    CGSize tagSize = CGSizeMake(stringRect.size.width, stringRect.size.height);
    return tagSize;
}
-(UIFont*)fontForText {
    NSString *fontFamily = (self.commonLike) ? @"HelveticaNeue-Medium" : @"HelveticaNeue";
    UIFont *font = [UIFont fontWithName:fontFamily size:14];
    return font;
}
-(UIColor*)tagColor {
    if(self.commonLike) {
        return [UIColor commonPropertyColor];
    }
    else {
        return [UIColor commonPropertyColor];
    }
}


@end







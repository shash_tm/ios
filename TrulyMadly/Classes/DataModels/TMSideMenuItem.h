//
//  TMSideMenuItem.h
//  TrulyMadly
//
//  Created by Ankit on 14/07/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TMSideMenuType) {
    PREFERENCES,
    REFER_AND_EARN,
    SPARK,
    HELPDESK,
    SETTINGS,
    SELECT,
    SCENES
};


@interface TMSideMenuItem : NSObject

@property(nonatomic,strong,readonly)NSString *titleText;
@property(nonatomic,strong,readonly)NSString *imageName;
@property(nonatomic,assign,readonly)TMSideMenuType sideMenuType;
@property(nonatomic,assign,readonly)CGSize imageViewSize;

- (instancetype)initWithSideMenuType:(TMSideMenuType)menuType;

@end

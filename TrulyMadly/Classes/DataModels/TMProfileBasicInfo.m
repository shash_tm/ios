//
//  TMProfileBasicInfo.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileBasicInfo.h"
#import "NSObject+TMAdditions.h"

@implementation TMProfileBasicInfo

-(instancetype)initWithData:(NSDictionary*)profileData isProfileAdCampaign:(BOOL)isProfileAdCampaign {
    self = [super init];
    if(self) {
        self.isProfileAdCampaign = isProfileAdCampaign;
        self.name = profileData[@"fname"];
        self.imageUrlString = profileData[@"profile_pic"];
        self.otherImages = (profileData[@"other_pics"] && [profileData[@"other_pics"] isKindOfClass:[NSArray class]]) ?  profileData[@"other_pics"] : [[NSArray alloc] init];
    
        if(!isProfileAdCampaign) {
            self.age = profileData[@"age"];
            self.city = profileData[@"city"];
            self.height = profileData[@"height_inch"];
            self.lastActivity = ([profileData[@"last_login"] isValidObject])?(profileData[@"last_login"]):nil;
            
            if(profileData[@"celeb_status"]) {
                id celebStatus = profileData[@"celeb_status"];
                if([celebStatus isValidObject]) {
                    self.isCeleb = [profileData[@"celeb_status"] boolValue];
                }
            }
            else {
                self.isCeleb = FALSE;
            }
            
            if(profileData[@"isDummySet"]) {
                id dummyStatus = profileData[@"isDummySet"];
                if([dummyStatus isValidObject]) {
                    self.isDummySet = [profileData[@"isDummySet"] boolValue];
                }
            }
            else {
                self.isDummySet = FALSE;
            }
            
            self.facebookMutualConnections = [profileData[@"fb_mf_count"] integerValue];
            NSArray *fbMutualFriendList = profileData[@"fb_mf"];
            if(fbMutualFriendList && [fbMutualFriendList isKindOfClass:[NSArray class]]) {
                self.fbMutualFriendList = fbMutualFriendList;
            }
        }
    }
    return self;
}


@end

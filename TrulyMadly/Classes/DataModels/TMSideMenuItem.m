//
//  TMSideMenuItem.m
//  TrulyMadly
//
//  Created by Ankit on 14/07/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSideMenuItem.h"

@interface TMSideMenuItem ()

@property(nonatomic,strong)NSString *titleText;
@property(nonatomic,strong)NSString *imageName;
@property(nonatomic,assign)TMSideMenuType sideMenuType;
@property(nonatomic,assign)CGSize imageViewSize;

@end

@implementation TMSideMenuItem

- (instancetype)initWithSideMenuType:(TMSideMenuType)menuType
{
    self = [super init];
    if (self) {
        self.sideMenuType = menuType;
        
        if(menuType == PREFERENCES) {
            self.titleText = @"Preferences";
            self.imageName = @"preference";
            self.imageViewSize = CGSizeMake(24, 24);
        }
        if(menuType == REFER_AND_EARN) {
            self.titleText = @"Refer & Earn";
            self.imageName = @"refer";
            self.imageViewSize = CGSizeMake(24, 24);
        }
        else if(menuType == SPARK) {
            self.titleText = @"Sparks";
            self.imageName = @"sidemenusparkIcon";
            self.imageViewSize = CGSizeMake(24, 24);
        }
        else if(menuType == HELPDESK) {
            self.titleText = @"Help Desk";
            self.imageName = @"helpdesk";
            self.imageViewSize = CGSizeMake(24, 24);
        }
        else if(menuType == SETTINGS) {
            self.titleText = @"Settings";
            self.imageName = @"settings";
            self.imageViewSize = CGSizeMake(24, 24);
        }
        else if(menuType == SELECT) {
            self.titleText = @"Select";
        }
        else if(menuType == SCENES) {
            self.imageName = @"sidemenuscenes";
            self.titleText = @"TM Mixers";
            self.imageViewSize = CGSizeMake(24, 24);
        }

    }
    return self;
}

@end

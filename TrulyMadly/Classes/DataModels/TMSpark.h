//
//  TMSpark.h
//  TrulyMadly
//
//  Created by Ankit on 02/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@class TMChatUser;
@class TMChatContext;

@interface TMSpark : NSObject

@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,strong)NSString* fullConversationLink;
@property(nonatomic,strong)NSString* message;
@property(nonatomic,assign)NSInteger messageId;
@property(nonatomic,strong)NSString* sparkHash;
@property(nonatomic,strong)NSString* sparkStartTime;
@property(nonatomic,strong)NSString* sparkCreateTime;
@property(nonatomic,assign)NSInteger sparkExpiryTime; //in seconds
@property(nonatomic,assign)BOOL isSparkSeen;
@property(nonatomic,assign)BOOL isSelectMember;

-(instancetype)initWithSparkDictionary:(NSDictionary*)sparkDictionary;
-(BOOL)isSparkExpired;
-(NSMutableDictionary*)sparkInsertQueryParams;
-(TMChatUser*)matchData;
-(TMChatContext*)chatContext;
-(NSString*)fName;
-(NSString*)age;
-(NSString*)designation;
-(NSString*)profileURLString;
-(NSString*)profileImageURLString;
-(void)updateMatchData:(TMChatUser*)matchData;
-(void)setDesignation:(NSString*)designation;
-(void)setProfileImagesURLData:(NSData*)imagesURLData;
-(void)setAge:(NSInteger)age;
-(void)setCity:(NSString*)city;
-(NSString*)sparkRemainingTimeInShortFormat;
-(NSString*)sparkExpiryRemainingTime;


@end

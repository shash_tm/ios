//
//  TMCookieData.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCookieData.h"

@implementation TMCookieData

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.register_basics_data = [[NSMutableDictionary alloc] init];
    }
    return self;
}
@end

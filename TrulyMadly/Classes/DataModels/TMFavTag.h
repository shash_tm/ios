//
//  TMFavTag.h
//  TrulyMadly
//
//  Created by Ankit on 01/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMFavTag : NSObject

@property(nonatomic,strong)NSString *value;
@property(nonatomic,strong)NSString *key;
@property(nonatomic,assign)NSInteger index;

@end

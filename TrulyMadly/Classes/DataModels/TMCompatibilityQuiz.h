//
//  TMCompatibilityQuiz.h
//  TrulyMadly
//
//  Created by Ankit on 24/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMEnums.h"

@interface TMCompatibilityQuiz : NSObject

@property(nonatomic,assign)NSInteger quizScore;
@property(nonatomic,assign)TMQuizScoreType quizType;

-(instancetype)initWithQuizType:(TMQuizScoreType)quizType withQuizScore:(NSInteger)quizScore;
-(NSString*)compatibilityQuizScoreText;
-(UIFont*)titleTextFont;
-(BOOL)isQuizFilledByUser;
-(BOOL)isInteractive;
-(NSString*)quizImageString;
-(NSString*)quizImageStringForEditMode;
-(UIColor*)quizTextColor;

@end

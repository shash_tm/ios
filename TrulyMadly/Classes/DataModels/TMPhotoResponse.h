//
//  TMPhotoResponse.h
//  TrulyMadly
//
//  Created by Ankit on 01/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMResponse.h"

@class TMPhoto;

@interface TMPhotoResponse : TMResponse

@property(nonatomic,strong)NSMutableArray *photos;
@property(nonatomic,assign)BOOL allowSkip;

-(TMPhoto*)photoForIndex:(NSInteger)index;

@end

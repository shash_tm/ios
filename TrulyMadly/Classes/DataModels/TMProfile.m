//
//  TMProfile.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfile.h"
#import "TMProfileBasicInfo.h"
#import "TMProfileLikes.h"
#import "TMProfileWorkAndEdu.h"
#import "TMProfileFavourites.h"
#import "TMProfileCompatibility.h"
#import "TMProfileVerification.h"
#import "TMProfileCommonFavorites.h"
#import "TMAdTrackingModel.h"
#import "TMProfileSelectInfo.h"
#import "TMProfileCollectionViewConfig.h"
#import "TMEnums.h"
#import "TMUserSession.h"


@interface TMProfile ()

@property(nonatomic,strong)NSMutableArray *rowIndexTypes;

@end


@implementation TMProfile

-(instancetype)initWithProfileData:(NSDictionary*)profileDictionary trustScores:(NSDictionary *)trustScores{
    self = [super init];
    if(self) {
        self.trustScores = trustScores;
        [self configureProfileWithDictionary:profileDictionary];
    }
    return self;
}

-(instancetype)initWithProfileData:(NSDictionary*)profileDictionary withEditMode:(BOOL)editMode trustScores:(NSDictionary *)trustScores{
    self = [super init];
    if(self) {
        self.trustScores = trustScores;
        self.editMode = editMode;
        [self configureProfileWithDictionary:profileDictionary];
    }
    return self;
}
//SeventyNine Profile Ad Changes
-(instancetype)initWithProfileAdData {
    self = [super init];
    if(self) {
        [self configureAdProfile];
    }
    return self;
}

- (void)configureAdProfile {
    
    NSArray *sections, *rows;
    TMProfileCollectionViewConfig *config;
    self.rowIndexTypes = [NSMutableArray arrayWithCapacity:2];
    //need to call api to get hashtags.
    config = [[TMProfileCollectionViewConfig alloc] init];
    self.isProfileAdCampaign = TRUE;
    
    sections = @[@(TMPROFILECOLLECTIONVIEWSECTION_HASHTAG)];
    rows = @[@(1)];
    config.sectionType = [sections[0] integerValue];
    config.rowCount = [rows[0] integerValue];
    [self.rowIndexTypes addObject:config];
}

- (void)addLikesForProfileAd:(NSArray *)profileLikes {
    self.likes = [[TMProfileLikes alloc] initWithProfileLikes:profileLikes withCommonHashtags:nil];
    self.likes.showHeader = YES;
}

-(void)configureProfileWithDictionary:(NSDictionary*)profileDictionary {
    self.rowIndexTypes = [NSMutableArray arrayWithCapacity:16];
    NSDictionary *profileDataDictionary = profileDictionary[@"profile_data"];
    NSArray *profileInterests = profileDataDictionary[@"interest"];
    NSArray *commonIntersets = profileDataDictionary[@"common_likes"];
    NSArray *commonHashtags = profileDataDictionary[@"common_hashTags"];
    NSDictionary *trustMeterDict = profileDataDictionary[@"trustMeter"];
    NSDictionary *favDict = profileDataDictionary[@"InterestsHobbies"];
    NSDictionary *compatibilityDict = profileDataDictionary[@"match_details"];
    NSArray *endorsements = profileDataDictionary[@"endorsementData"];
    
    self.isProfileAdCampaign = (profileDataDictionary[@"isProfileAdCampaign"]) ? [profileDataDictionary[@"isProfileAdCampaign"] boolValue] : FALSE;
    self.adObj = (profileDataDictionary[@"campaign_links"] && [profileDataDictionary[@"campaign_links"] isKindOfClass:[NSDictionary class]]) ? [[TMAdTrackingModel alloc] initWithCampaignDict:profileDataDictionary[@"campaign_links"]] : nil;
    
    id matchId = profileDictionary[@"user_id"];
    if(matchId && [matchId isKindOfClass:[NSNumber class]]) {
        self.matchId = [matchId stringValue];
    }else if(matchId && [matchId isKindOfClass:[NSString class]]) {
        self.matchId = matchId;
    }
    
    self.basicInfo = [[TMProfileBasicInfo alloc] initWithData:profileDataDictionary isProfileAdCampaign:self.isProfileAdCampaign];
    self.basicInfo.matchId = self.matchId;
    self.likes = [[TMProfileLikes alloc] initWithProfileLikes:profileInterests withCommonHashtags:commonHashtags];
    self.likes.editMode = self.editMode;
    
    self.workAndEducation = [[TMProfileWorkAndEdu alloc] initWithProfileData:profileDataDictionary]; 
    self.workAndEducation.editMode = self.editMode;
    self.workAndEducation.isSelfEdit = self.editMode;
    if (self.workAndEducation.isSelfEdit) {
        [self.workAndEducation configureWorkDataForEdit];
    }
    self.favourite = [[TMProfileFavourites alloc] initWithfavouriteData:favDict];
    self.favourite.editMode = self.editMode;
    self.favourite.isCommonFav = false;
    
    self.compatibility = [[TMProfileCompatibility alloc] initWithMatchDetail:compatibilityDict withCommonLikes:commonIntersets];
    self.compatibility.editMode = self.editMode;
    self.compatibility.commonLike.editMode = self.editMode;
    
    self.varification = [[TMProfileVerification alloc] initWithTrustMeterDictionary:trustMeterDict endorsements:endorsements trustScores:self.trustScores editMode:self.editMode];
    //self.varification.editMode = self.editMode;

    self.likeUrl = profileDataDictionary[@"like_url"];
    self.hideUrl = profileDataDictionary[@"hide_url"];
    
    if(profileDataDictionary[@"common_favourites"]) {
        NSDictionary *cfavDict = profileDataDictionary[@"common_favourites"];
        TMProfileCommonFavorites *cfav = [[TMProfileCommonFavorites alloc] initWithCommonfavouriteData:cfavDict];
        self.favourite.isCommonFav = [cfav isCommonFavoritesAvailable];
        self.favourite.commonFav = [cfav getCommonFav];
    }
    if([[TMUserSession sharedInstance] isSelectEnabled]) {
        self.selectInfo = [[TMProfileSelectInfo alloc] initWithDictionary:profileDataDictionary[@"select"]];
    }
    NSDictionary *nriDict = profileDataDictionary[@"nri"];
    self.isUserNRI = [[nriDict objectForKey:@"is_nri"] boolValue];
    
    self.basicInfo.isSelectMember = self.selectInfo.isSelectUser;
    self.likes.showHeader = self.selectInfo.isSelectUser;
    
    /*
    if(profileDataDictionary[@"mutual_events"] && [profileDataDictionary[@"mutual_events"] isKindOfClass:[NSArray class]]){
        self.mutualEventArr = profileDataDictionary[@"mutual_events"];
        if(self.mutualEventArr.count>0) {
            self.isMutualEventAvail = true;
        }else{
            self.isMutualEventAvail = false;
            self.mutualEventArr = nil;
        }
    }
    else {
        self.isMutualEventAvail = false;
        self.mutualEventArr = nil;
    }*/
    
    if(profileDataDictionary[@"photo_visibility_toast"]){
        self.isPhotoVisibilityShown = [profileDataDictionary[@"photo_visibility_toast"] boolValue];
        if(self.isPhotoVisibilityShown){
            self.isMutualEventAvail = false;
            self.mutualEventArr = nil;
        }
    }else {
        self.isPhotoVisibilityShown = false;
    }
    
    self.commonAttributes = (profileDataDictionary[@"commonalities_string"] && [profileDataDictionary[@"commonalities_string"] isKindOfClass:[NSArray class]]) ? profileDataDictionary[@"commonalities_string"] : [[NSArray alloc] init];
    
    self.sparkHash = profileDataDictionary[@"spark_hash"];
    self.hasLikedBefore = [profileDataDictionary[@"has_liked_before"] boolValue];
    
    ///
    NSArray *sections;
    NSArray *rows;
    NSArray *contentHeights;
    NSInteger favSectionRowCount = ([self.favourite isFavsAvailable]) ? 2 : 1;
    if(self.editMode){
        sections = @[@(TMPROFILECOLLECTIONVIEWSECTION_TRUSTSCORE),
                     @(TMPROFILECOLLECTIONVIEWSECTION_HASHTAG),
                     @(TMPROFILECOLLECTIONVIEWSECTION_WORKANDEDU),
                     @(TMPROFILECOLLECTIONVIEWSECTION_FAVOURITE)];
        rows = @[@(1),@(1),@(1),@(favSectionRowCount)];
        contentHeights = @[];
    }
    else if(self.isProfileAdCampaign) {
        sections = @[@(TMPROFILECOLLECTIONVIEWSECTION_HASHTAG)];
        rows = @[@(1)];
    }
    else if(self.selectInfo.isSelectUser) {
        sections = @[@(TMPROFILECOLLECTIONVIEWSECTION_SELECT),
                     @(TMPROFILECOLLECTIONVIEWSECTION_HASHTAG),
                     @(TMPROFILECOLLECTIONVIEWSECTION_WORKANDEDU),
                     @(TMPROFILECOLLECTIONVIEWSECTION_FAVOURITE)];
        rows = @[@(1),@(1),@(1),@(favSectionRowCount)];
    }
    else {
        sections = @[@(TMPROFILECOLLECTIONVIEWSECTION_HASHTAG),
                     @(TMPROFILECOLLECTIONVIEWSECTION_WORKANDEDU),
                     @(TMPROFILECOLLECTIONVIEWSECTION_FAVOURITE)];
        rows = @[@(1),@(1),@(favSectionRowCount)];
    }
    
    for (int collectionviewSection=0; collectionviewSection<sections.count; collectionviewSection++) {
        TMProfileCollectionViewConfig *config = [[TMProfileCollectionViewConfig alloc] init];
        config.sectionType = [sections[collectionviewSection] integerValue];
        config.rowCount = [rows[collectionviewSection] integerValue];
        [self.rowIndexTypes addObject:config];
    }
}

-(NSInteger)sectionCount {
    return self.rowIndexTypes.count;
}

-(NSInteger)rowCountForSection:(NSInteger)section {
    TMProfileCollectionViewConfig *config = self.rowIndexTypes[section];
    NSInteger rowCount = config.rowCount;
    return rowCount;
}

-(TMProfileCollectionViewCellType)cellType:(NSInteger)section row:(NSInteger)row {
    TMProfileCollectionViewConfig *config = self.rowIndexTypes[section];
    TMProfileCollectionViewCellType cellType = [config getCellTypeForRow:row];
    return cellType;
}

-(CGFloat)contentLayoutHeight:(NSInteger)section row:(NSInteger)row maxWidth:(CGFloat)maxWidth   {
    CGFloat height = 0;
    TMProfileCollectionViewConfig *config = self.rowIndexTypes[section];
    
    switch (config.sectionType) {
        case TMPROFILECOLLECTIONVIEWSECTION_SELECT:
            height = 80;
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_HASHTAG:
            height = [self.likes likesContentHeight:maxWidth];
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_WORKANDEDU:
            height = [self.workAndEducation workAndEducationContentHeight:maxWidth iconWidth:[self baseIconWidth:maxWidth]];
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_FAVOURITE:
            height = [self.favourite favouriteContentHeight:maxWidth withRow:row];
            break;
        case TMPROFILECOLLECTIONVIEWSECTION_TRUSTSCORE:
            height = [self.varification varificationContentHeight:maxWidth];
            break;
        default:
            break;
    }

    return height;
}

-(CGFloat)baseIconWidth:(CGFloat)maxWidth {
    CGFloat iconWidth = (maxWidth - (15*4))/5;
    return iconWidth;
}

-(BOOL)showSelectNudgeOnSparkAction {
    BOOL isSelectEnabled = [[TMUserSession sharedInstance] isSelectEnabled];
    BOOL isSelectProfile = self.selectInfo.isSelectUser;
    BOOL isSelectUser = [[TMUserSession sharedInstance].user isSelectUser];
    BOOL isSparkAllowedToSelectUser = [[TMUserSession sharedInstance] isSparkActionAllowedOnSelectProfile];
    if( isSelectEnabled && isSelectProfile && (!isSelectUser) && (!isSparkAllowedToSelectUser)) {
        return TRUE;
    }
    return FALSE;
}

-(BOOL)showSelectNudgeOnLikeAction {
    BOOL isSelectEnabled = [[TMUserSession sharedInstance] isSelectEnabled];
    BOOL isSelectProfile = self.selectInfo.isSelectUser;
    BOOL isSelectUser = [[TMUserSession sharedInstance].user isSelectUser];
    BOOL isLikeAllowedToSelectUser = [[TMUserSession sharedInstance] isLikeActionAllowedOnSelectProfile];
    if( isSelectEnabled && isSelectProfile && (!isSelectUser) && (!isLikeAllowedToSelectUser)) {
        return TRUE;
    }
    return FALSE;
}

@end


//
//  TMError.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@interface TMError : NSObject

@property(nonatomic,assign)TMApiErrorCode errorCode;
@property(nonatomic,strong)NSString *errorMessage;

-(instancetype)initWithResponseCode:(TMApiResponseCode)responseCode;
-(instancetype)initWithError:(NSError*)error;
-(instancetype)initWithNoNetworkErrorCode;

@end

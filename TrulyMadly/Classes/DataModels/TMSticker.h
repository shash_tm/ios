//
//  TMSticker.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 20/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMSticker : NSObject

@property (nonatomic, strong) NSString* stickerID;
@property (nonatomic, strong) NSString* galleryID;
@property (nonatomic, strong) NSString* updatedInVersion;
@property (nonatomic, strong) NSString* thumbnailFilePathURL;
@property (nonatomic, strong) NSString* hdFilePathURL;
@property (nonatomic, strong) NSString* thumbnailQualityImageURLString;
@property (nonatomic, strong) NSString* highQualityImageURLString;
@property (nonatomic, assign) BOOL isUpdated;
@property (nonatomic, assign) BOOL isActive;

/**
 * Initializer for sticker response dictionary
 * @param sticker info dictionary
 * @return TMSticker instance
 */
- (instancetype) initWithDictionary:(NSDictionary*) stickerInfoDictionary;


/**
 * Initializer for given gallery ID and sticker ID
 * @param gallery ID
 * @param sticker ID
 * @return TMSticker instance
 */
- (instancetype) initWithGalleryID:(NSString*) galleryID stickerID:(NSString*) stickerID;


/**
 * Initializer for a given sticker URL string
 * @param sticker URL string
 * @return TMSticker instance
 */
- (instancetype) initWithStickerURLString:(NSString*) stickerURLString ;

@end

//
//  TMMutualLikeItem.m
//  TrulyMadly
//
//  Created by Ankit on 20/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMutualLikeItem.h"





//@property(nonatomic,strong)NSString *fullConvLink;


@implementation TMMutualLikeItem

-(instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    if(self) {
        self.matchedtTimestamp = [dictionary objectForKey:@"matched_timestamp"];
        NSDictionary *profileData = [dictionary objectForKey:@"profile_data"];
        self.age = [[profileData objectForKey:@"age"] integerValue];
        self.blockedShownUrl = [profileData objectForKey:@"block_shown_url"];
        self.city = [profileData objectForKey:@"city"];
        self.fName = [profileData objectForKey:@"fname"];
        self.isBlocked = [[profileData objectForKey:@"is_blocked"] boolValue];
        self.matchedOn = [profileData objectForKey:@"matched_on"];
        self.fullConvLink = [profileData objectForKey:@"message_link"];
        self.receiverId = [[profileData objectForKey:@"profile_id"] integerValue];
        self.profilePic = [profileData objectForKey:@"profile_pic"];
    }
    return self;
}

@end

//
//  TMNotificationUpdate.h
//  TrulyMadly
//
//  Created by Ankit on 14/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMNotificationUpdate : NSObject

-(instancetype)initWithResponse:(NSDictionary*)response;

@property(nonatomic,assign)NSInteger conversationCount;
@property(nonatomic,strong)NSArray *unreadConversations;

@end

//
//  QuizDomainObject.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizInfo.h"

@interface TMQuizInfo ()

@property (nonatomic, strong, readwrite) NSString* quizID;
@property (nonatomic, strong, readwrite) NSString* quizIconURL;
@property (nonatomic, strong, readwrite) NSString* quizBannerURL;
@property (nonatomic, strong, readwrite) NSString* quizDisplayName;
@property (nonatomic, strong, readwrite) NSString* quizDescription;
@property (nonatomic, strong, readwrite) NSString* quizSponsoredText;
@property (nonatomic, strong, readwrite) NSString* quizSponsoredImageURL;
@property (nonatomic, strong, readwrite) NSString* quizSuccessMessage;
@property (nonatomic, strong, readwrite) NSString* quizSuccessNudge;
@property (nonatomic, strong, readwrite) NSArray* playedBy;
@property (nonatomic, strong, readwrite) NSArray* flareUserArray;

@end

@implementation TMQuizInfo

#define KEY_QUIZ_ID @"quiz_id"
#define KEY_QUIZ_DISPLAY_NAME @"display_name"
#define KEY_QUIZ_IMAGE @"image"
#define KEY_QUIZ_BANNER_URL @"banner_url"
#define KEY_QUIZ_PLAYED_BY_USER @"played_by"
#define KEY_QUIZ_IS_NEW @"is_new"
#define KEY_QUIZ_DESCRIPTION @"description"
#define KEY_QUIZ_SPONSORED_TEXT @"sponsored_text"
#define KEY_QUIZ_SPONSORED_IMAGE @"sponsored_image"
#define KEY_QUIZ_SUCCESS @"quiz_success_msg"
#define KEY_QUIZ_SUCCESS_NUDGE_TEXT @"quiz_success_nudge_msg"

- (instancetype) initWithDictionary:(NSDictionary*) quizInfoDictionary
{
    if(self = [super init]) {
        self.quizID = ([quizInfoDictionary valueForKey:KEY_QUIZ_ID] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_ID] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_ID]:nil;
        
        self.quizDisplayName = ([quizInfoDictionary valueForKey:KEY_QUIZ_DISPLAY_NAME] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_DISPLAY_NAME] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_DISPLAY_NAME]:nil;
        
        self.quizBannerURL = ([quizInfoDictionary valueForKey:KEY_QUIZ_BANNER_URL] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_BANNER_URL] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_BANNER_URL]:nil;
        
        self.quizIconURL = ([quizInfoDictionary valueForKey:KEY_QUIZ_IMAGE] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_IMAGE] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_IMAGE]:nil;
        
        if (([quizInfoDictionary valueForKey:KEY_QUIZ_PLAYED_BY_USER] && [[quizInfoDictionary valueForKey:KEY_QUIZ_PLAYED_BY_USER] isKindOfClass:[NSString class]])) {
          NSArray* playedByUserArray = [((NSString*)[quizInfoDictionary valueForKey:KEY_QUIZ_PLAYED_BY_USER]) componentsSeparatedByString:@","];
           
            NSMutableArray* playedByUserTempMutableArray = [[NSMutableArray alloc] init];
            NSMutableArray* flareUserMutableArray = [[NSMutableArray alloc] init];
            
            for (int index = 0; index < playedByUserArray.count; index++) {
                if ([[playedByUserArray objectAtIndex:index] isKindOfClass:[NSString class]]) {
                    NSString* playedByUser = [playedByUserArray objectAtIndex:index];
                     if ([playedByUser hasSuffix:@"+"]) {
                        playedByUser = [[playedByUser componentsSeparatedByString:@"+"] firstObject];
                        [flareUserMutableArray addObject:playedByUser];
                        [playedByUserTempMutableArray addObject:playedByUser];
                    }
                    else {
                        [playedByUserTempMutableArray addObject:playedByUser];
                    }
                }
            }
            
            playedByUserArray = [NSOrderedSet orderedSetWithArray:playedByUserArray].array;
            flareUserMutableArray = [[NSOrderedSet orderedSetWithArray:flareUserMutableArray].array mutableCopy];
            
            self.playedBy = (playedByUserArray && playedByUserArray.count)?[playedByUserTempMutableArray copy]:nil;
            self.flareUserArray = (flareUserMutableArray && flareUserMutableArray.count)?[flareUserMutableArray copy]:nil;
        }
        else {
            self.playedBy = nil;
            self.flareUserArray = nil;
        }
        
        self.isNew = ([quizInfoDictionary valueForKey:KEY_QUIZ_IS_NEW] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_IS_NEW] isKindOfClass:[NSNull class]])?[[quizInfoDictionary valueForKey:KEY_QUIZ_IS_NEW] boolValue]:NO;
        
        self.quizDescription = ([quizInfoDictionary valueForKey:KEY_QUIZ_DESCRIPTION] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_DESCRIPTION] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_DESCRIPTION]:@"";
        
        self.quizSponsoredText = ([quizInfoDictionary valueForKey:KEY_QUIZ_SPONSORED_TEXT] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_SPONSORED_TEXT] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_SPONSORED_TEXT]:@"";
        
        self.quizSponsoredImageURL = ([quizInfoDictionary valueForKey:KEY_QUIZ_SPONSORED_IMAGE] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_SPONSORED_IMAGE] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_SPONSORED_IMAGE]:nil;
        
        self.quizSuccessMessage = ([quizInfoDictionary valueForKey:KEY_QUIZ_SUCCESS] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_SUCCESS] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_SUCCESS]:nil;
        
        self.quizSuccessNudge = ([quizInfoDictionary valueForKey:KEY_QUIZ_SUCCESS_NUDGE_TEXT] && ![[quizInfoDictionary valueForKey:KEY_QUIZ_SUCCESS_NUDGE_TEXT] isKindOfClass:[NSNull class]])?[quizInfoDictionary valueForKey:KEY_QUIZ_SUCCESS_NUDGE_TEXT]:nil;
        
        return self;
    }
    return nil;
}


- (BOOL) isFlareForUserID:(NSString*) userID
{
    if (userID) {
        return [self.flareUserArray containsObject:userID];
    }
    return NO;
}

@end

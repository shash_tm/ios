//
//  TMEditPreference.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

protocol TMEditPreferenceDelegate{
    func didDataChange()
}

class TMEditPreference: NSObject , TMDataTableViewControllerDelegate {
   
    var editPreferenceData = [:] as Dictionary<String,AnyObject>
    var demo = [:] as NSDictionary
    var trait = [:] as NSDictionary
    var basicsData = [:] as NSDictionary
    var startAge: String = "18"
    var endAge: String = "70"
    var startHeight: String = "4'0\""
    var endHeight: String = "7'11\""
    var start_height: String = ""
    var end_height: String = ""
    var states = [AnyObject]()
    var statesToDisplay = [String]()
    var statesToSend = [String]()
    var cities = [AnyObject]()
    var citiesToDisplay = [String]()
    var citiesToSend = [String]()
    var citiesId = [String]()
//    var maritalStatus: String = ""
//    var degree = [AnyObject]()
//    var degreeToDisplay = [String]()
//    var degreeToSend = [String]()
//    
//    var masterStart: Int = 2
//    var masterEnd: Int = 19
//    var bachelorStart: Int = 22
//    var bachelorEnd: Int = 39
    
    var allStates:NSMutableArray!
    var allDegree:NSMutableArray!
    var allCities:NSMutableArray!
    
    var delegate:TMEditPreferenceDelegate? = nil
    
    var buildForm = TMLoadRegisterJson()
    
    init(editPreferenceData:Dictionary<String,AnyObject>) {
        super.init()
        self.editPreferenceData = editPreferenceData
        self.demo = editPreferenceData["demo"] as! NSDictionary
        self.trait = editPreferenceData["trait"] as! NSDictionary
        self.basicsData = editPreferenceData["basics_data"] as! NSDictionary
    }
    
    
    func createStateObj() {
        var stateObj = Dictionary<String,AnyObject>()
        self.allStates = buildForm.getStates()
        
        for i in (0 ..< self.allStates.count) {
            let stateData = self.allStates[i] as! NSDictionary
            stateObj["state_id"] = stateData["key"] as AnyObject?
            stateObj["name"] = stateData["value"] as AnyObject?
            stateObj["country_id"] = stateData["country_id"] as AnyObject?
            stateObj["isSelected"] = false as AnyObject?
            self.states.append(stateObj as AnyObject)
        }
    }
    
//    func createDegreeObj() {
//        self.allDegree = buildForm.getDegree()
//        
//        var degreeObj = Dictionary<String,AnyObject>()
//        for(var i=0; i<self.allDegree.count; i++) {
//            degreeObj["id"] = self.allDegree[i]["key"] as! String
//            degreeObj["name"] = self.allDegree[i]["value"] as! String
//            degreeObj["isSelected"] = false as Bool
//            self.degree.append(degreeObj)
//        }
//    }
    
//    private func initDegreeIndex() {
//       // var keys = buildForm.getDegreeKey()
//        let masters = "2-10"//keys["masters"] as! String //BaseValue.masters["key"] as! String
//        let bachelors = "11-19"//keys["bachelors"] as! String
//        
//        var tmp = masters.componentsSeparatedByString("-") as NSArray!
//        var start = tmp[0] as! String
//        var end = tmp[1] as! String
//        self.masterStart = Int(start)!
//        self.masterEnd = Int(end)!
//        
//        tmp = bachelors.componentsSeparatedByString("-") as NSArray!
//        start = tmp[0] as! String
//        end = tmp[1] as! String
//        self.bachelorStart = Int(start)!
//        self.bachelorEnd = Int(end)!
//    }
    
    func getUrl() -> String {
        return self.basicsData["url"] as! String
    }
    
    func setAge() {
        if( ((self.demo["start_age"] as AnyObject).isValidObject() == true) && (self.demo["start_age"] != nil) ) {
            self.startAge = self.demo["start_age"] as! String
        }
        else {
            self.startAge = "18";
        }
        if( ((self.demo["end_age"] as AnyObject).isValidObject() == true) && (self.demo["end_age"] != nil) ) {
            self.endAge = self.demo["end_age"] as! String
            if(self.startAge == "18" && self.endAge == "18") {
                self.endAge = "19"
            }
        }
        else {
            self.endAge = "70";
        }
    }
    
    func setHeight() {
        if( ((self.trait["start_height"] as AnyObject).isValidObject() == true) && (self.trait["start_height"] != nil) ) {
            self.start_height = self.trait["start_height"] as! String
        }else {
            self.start_height = "48";
        }
        if( ((self.trait["end_height"] as AnyObject).isValidObject() == true) && (self.trait["end_height"] != nil) ) {
            self.end_height = self.trait["end_height"] as! String
        }else {
            self.end_height = "95";
        }
        
        if(self.start_height == "48" && self.end_height == "48") {
            self.end_height = "50"
        }
        var height = self.start_height //self.trait["start_height"] as! String
        self.startHeight = self.getHeightInFeetandInch(Int(height)!)
        height = self.end_height //self.trait["end_height"] as! String
        self.endHeight = self.getHeightInFeetandInch(Int(height)!)
    }
    
    func getHeightInFeetandInch(_ height:Int) -> String {
        let feet: Int = height/12
        let inch: Int = height%12
        let heightToReturn = String(feet)+"'"+String(inch)+"\""
        
        return heightToReturn
    }
    
    func setStates() {
        if(self.states.count == 0) {
            self.createStateObj()
        }
        
        let states = self.demo["states"] as! String
        if(states != "") {
            let statesArr = states.components(separatedBy: ",") as NSArray!
            let length = (statesArr?.count)! as NSInteger
            for i in (0 ..< length) {
                let stateString = statesArr?[i] as! String
                let state = stateString.components(separatedBy: "-") as NSArray!
                let state_id = state?[1] as! String
                let tmp = self.getStateForKey(state_id)
                if(tmp != "") {
                    self.createCityObj(state_id)
                    self.statesToDisplay.append(tmp)
                }
            }
        }
    }
    
    func setCities() {
        let cities = self.demo["cities"] as! String
        if(cities != "") {
            let cityArr = cities.components(separatedBy: ",") as NSArray!
            let length = (cityArr?.count)! as NSInteger
            for i in (0 ..< length) {
                let cityString = cityArr?[i] as! String
                let city = cityString.components(separatedBy: "-") as NSArray!
                let city_id = city?[2] as! String
                let tmp = self.getCityForKey(city_id)
                if(tmp != "") {
                    self.citiesId.append(city_id)
                    self.citiesToDisplay.append(tmp)
                }

            }
        }
    }
    
    private func createCityObj(_ state_id: String) {
        var stateId: String = ""
        var cityObj = Dictionary<String,AnyObject>()
        self.allCities = buildForm.getCities()
        
        for i in (0 ..< self.allCities.count) {
            let cityData = self.allCities[i] as! NSDictionary
            stateId = cityData["state_id"] as! String
            if(stateId == state_id){
                cityObj["city_id"] = cityData["city_id"] as AnyObject?
                cityObj["state_id"] = cityData["state_id"] as AnyObject?
                cityObj["name"] = cityData["name"] as AnyObject?
                cityObj["country_id"] = cityData["country_id"] as AnyObject?
                cityObj["isSelected"] = false as AnyObject?
                self.cities.append(cityObj as AnyObject)
            }
        }
    }
    
    func getSates() {
        self.statesToDisplay = []
        self.cities = []
        for i in (0 ..< self.states.count) {
            let tmp = self.states[i] as! NSDictionary
            let isSelected = tmp["isSelected"] as! Bool
            if(isSelected) {
                self.createCityObj(tmp["state_id"] as! String)
                self.statesToDisplay.append(tmp["name"] as! String)
            }
        }
    }
    

    func getCities() {
        self.citiesToDisplay = []
        for j in (0 ..< self.citiesId.count) {
            var tmp = self.getCityForKey(self.citiesId[j] as String)
        }
        
        for i in (0 ..< self.cities.count) {
            let tmp = self.cities[i] as! NSDictionary
            let isSelected = tmp["isSelected"] as! Bool
            if(isSelected) {
                self.citiesToDisplay.append(tmp["name"] as! String)
            }
        }
    }
    
    
//    func setMaritalStatus() {
//        let status: AnyObject! = self.demo["marital_status"]
//        if(!status.isKindOfClass(NSNull)) {
//            self.maritalStatus = self.demo["marital_status"] as! String
//        } else {
//            self.maritalStatus = ""
//        }
//    }
    
//    func setHighestDegree() {
//        if(self.degree.count == 0) {
//            self.createDegreeObj()
//        }
//        
//        let degrees: AnyObject! = self.work["education"]
//        if(!degrees.isKindOfClass(NSNull)) {
//            let degreesArr = degrees.componentsSeparatedByString(",") as NSArray!
//            for (var i = 0; i<degreesArr.count; i++) {
//                let degree_id = degreesArr[i] as! String
//                let tmp = self.getDegreeForKey(degree_id, isSelected: true)
//                if(tmp != "") {
//                    self.degreeToDisplay.append(tmp)
//                }
//            }
//        }
//    }
    
    /*
        delegate for TMDataTableViewController
    */
    
    func didStateChange(key: String, index: Int) {
        if(key == "state") {
            if(index == -1) {
                self.states = []
                self.statesToDisplay = []
                self.createStateObj()
            }
            else {
                var stateObj = Dictionary<String,AnyObject>()
                let tmp = self.states[index] as! NSDictionary
                let isSelected = tmp["isSelected"] as! Bool
                stateObj["state_id"] = tmp["state_id"] as AnyObject?
                stateObj["name"] = tmp["name"] as AnyObject?
                stateObj["country_id"] = tmp["country_id"] as AnyObject?
                stateObj["isSelected"] = !isSelected as AnyObject?
                self.states[index] = stateObj as AnyObject
            }
        }
        else if(key == "city") {
            if(index == -1) {
                self.citiesToDisplay = []
                self.citiesId = []
            }
            else {
                var cityObj = Dictionary<String,AnyObject>()
                let tmp = self.cities[index] as! NSDictionary
                let isSelected = tmp["isSelected"] as! Bool
                cityObj["city_id"] = tmp["city_id"] as AnyObject?
                cityObj["state_id"] = tmp["state_id"] as AnyObject?
                cityObj["name"] = tmp["name"] as AnyObject?
                cityObj["country_id"] = tmp["country_id"] as AnyObject?
                cityObj["isSelected"] = !isSelected as AnyObject?
                self.cities[index] = cityObj as AnyObject
                
                if(isSelected) {
                    //remove city id from array
                    self.citiesId = self.citiesId.filter { $0 != cityObj["city_id"] as! String! }
                } else {
                    // add city id in array
                    self.citiesId.append(cityObj["city_id"] as! String)
                }
            }
        }
        if(self.delegate != nil) {
            self.delegate!.didDataChange()
        }
    }
    
    private func getStateForKey(_ key: String) -> String {
        var stateObj = Dictionary<String,AnyObject>()
        
        for i in (0 ..< self.states.count) {
            let tmp = self.states[i] as! NSDictionary
            if(tmp["state_id"] as! String == key) {
                stateObj["state_id"] = tmp["state_id"] as AnyObject?
                stateObj["name"] = tmp["name"] as AnyObject?
                stateObj["country_id"] = tmp["country_id"] as AnyObject?
                stateObj["isSelected"] = true as AnyObject?
                self.states[i] = stateObj as AnyObject
                return tmp["name"] as! String
            }
        }
        return ""
    }
    
    private func getCityForKey(_ key: String) -> String {
        var cityObj = Dictionary<String,AnyObject>()
        
        for i in (0 ..< self.cities.count) {
            let tmp = self.cities[i] as! NSDictionary
            if(tmp["city_id"] as! String == key) {
                cityObj["city_id"] = tmp["city_id"] as AnyObject?
                cityObj["state_id"] = tmp["state_id"] as AnyObject?
                cityObj["name"] = tmp["name"] as AnyObject?
                cityObj["country_id"] = tmp["country_id"] as AnyObject?
                cityObj["isSelected"] = true as AnyObject?
                self.cities[i] = cityObj as AnyObject
                return tmp["name"] as! String
            }
        }
        return ""
    }
    
//    private func getDegreeForKey(id: String, isSelected: Bool) -> String {
//        var degreeObj = Dictionary<String,AnyObject>()
//        
//        for(var i=0;i<self.degree.count;i++) {
//            let tmp = self.degree[i] as! NSDictionary
//            if(tmp["id"] as! String == id) {
//                degreeObj["name"] = tmp["name"] as! String
//                degreeObj["id"] = tmp["id"] as! String
//                degreeObj["isSelected"] = isSelected as Bool
//                self.degree[i] = degreeObj
//                return tmp["name"] as! String
//            }
//        }
//        return ""
//    }
    
    
    
    // delegate for marital status
    
//    func didMaritalStatusChanged(key: String) {
//        
//        self.maritalStatus = key
//        
//        if(self.delegate != nil) {
//            self.delegate!.didDataChange()
//        }
//    }
    
    // delegate for degree table view controller
    
//    func didDegreeChange(index: Int, isSelected: Bool) {
//    
//        if(index == -1) {
//            self.degree = []
//            self.degreeToDisplay = []
//            self.createDegreeObj()
//        } else if(index == -2) {  // for masters only
//            var i = self.masterStart
//            let j = self.masterEnd
//            while(i<=j) {
//                let id = String(i)
//                self.getDegreeForKey(id, isSelected: isSelected)
//                i++
//            }
//        } else if(index == -3) {  // for bachelors only
//            var i = self.bachelorStart
//            let j = self.bachelorEnd
//            while(i<=j) {
//                let id = String(i)
//                self.getDegreeForKey(id, isSelected: isSelected)
//                i++
//            }
//        }
//        else {
//            var degreeObj = Dictionary<String,AnyObject>()
//            let tmp = self.degree[index] as! NSDictionary
//            let isSelected = tmp["isSelected"] as! Bool
//            degreeObj["id"] = tmp["id"] as! String
//            degreeObj["name"] = tmp["name"] as! String
//            degreeObj["isSelected"] = !isSelected
//            self.degree[index] = degreeObj
//        }
//        
//        if(self.delegate != nil) {
//            self.delegate!.didDataChange()
//        }
//    }
    
    private func getStateToSend() -> String {
        self.statesToSend = []
        for i in (0 ..< self.states.count) {
            let tmp = self.states[i] as! NSDictionary
            let isSelected = tmp["isSelected"] as! Bool
            if(isSelected) {
                let country_id = tmp["country_id"] as! String
                let state_id = tmp["state_id"] as! String
                let stateId = country_id+"-"+state_id
                self.statesToSend.append(stateId as String)
            }
        }
        
        if(self.statesToSend.count>0) {
            return self.toStringFromArray(self.statesToSend)
        }
        return ""
    }
    
    private func getCityToSend() -> String {
        self.citiesToSend = []
        for i in (0 ..< self.cities.count) {
            let tmp = self.cities[i] as! NSDictionary
            let isSelected = tmp["isSelected"] as! Bool
            if(isSelected) {
                let country_id = tmp["country_id"] as! String
                let state_id = tmp["state_id"] as! String
                let city_id = tmp["city_id"] as! String
                let cityId = country_id+"-"+state_id+"-"+city_id
                self.citiesToSend.append(cityId as String)
            }
        }
        
        if(self.citiesToSend.count>0) {
            return self.toStringFromArray(self.citiesToSend)
        }

        return ""
    }
    
    
//    private func educationToSend() -> String {
//        self.degreeToSend = []
//        for(var i=0; i<self.degree.count; i++) {
//            let tmp = self.degree[i] as! NSDictionary
//            let isSelected = tmp["isSelected"] as! Bool
//            if(isSelected) {
//                self.degreeToSend.append(tmp["id"] as! String)
//            }
//        }
//        
//        if(self.degreeToSend.count>0) {
//            return self.toStringFromArray(self.degreeToSend)
//        }
//        
//        return ""
//    }
    
    private func toStringFromArray(_ list: Array<String>)-> String {
        
        var stringForServer = String()
        stringForServer = list.joined(separator: ",")
        
        return stringForServer
        
    }
    
    private func getCountryId() -> String {
        let AllCountries: NSArray = buildForm.getCountries()
        let countryData = AllCountries[0] as! NSDictionary
        let countryId : String = countryData["country_id"] as! String
        return countryId
    }
    
    func saveData() -> NSDictionary{
        
        var queryString = Dictionary<String,AnyObject>()
        queryString["update"] = true as Bool as AnyObject?
        queryString["login_mobile"] = true as Bool as AnyObject?
        queryString["lookingAgeStart"] = self.startAge as String as AnyObject?
        queryString["lookingAgeEnd"] = self.endAge as String as AnyObject?
        queryString["start_height"] = self.start_height as String as AnyObject?
        queryString["end_height"] = self.end_height as String as AnyObject?
        queryString["spouseCountry"] = self.getCountryId() as String as AnyObject?
        queryString["spouseState"] = self.getStateToSend() as String as AnyObject?
        queryString["spouseCity"] = self.getCityToSend() as String as AnyObject?
        //queryString["marital_status_spouse"] = self.maritalStatus as String
        //queryString["education_spouse"] = self.educationToSend() as String
        return queryString as NSDictionary
    }
    
}

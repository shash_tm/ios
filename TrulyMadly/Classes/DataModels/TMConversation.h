//
//  TMMessageConversation.h
//  TrulyMadly
//
//  Created by Ankit on 10/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@class TMChatUser;
@class TMChatContext;

@interface TMConversation : NSObject

@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,assign)NSInteger userId;

@property(nonatomic,strong)NSString *message;
@property(nonatomic,strong)NSString *timestamp;
@property(nonatomic,strong)NSString *clearChatTs;
@property(nonatomic,assign)TMMessageSource source;
@property(nonatomic,assign)TMMessageStatus status;
@property(nonatomic,assign)TMMessageSender sender;
@property(nonatomic,assign)BOOL seen;

@property(nonatomic,assign)TMConversationMessageType conversationMessageType;
@property(nonatomic,strong)NSString *conversationSortTs;
@property(nonatomic,assign)BOOL isBlockedShown;
@property(nonatomic,assign)BOOL isBlocked;
@property(nonatomic,assign)BOOL isArchived;
@property(nonatomic,assign)BOOL isMsTm;
@property(nonatomic,assign)BOOL isSparkMatch;
@property(nonatomic,assign)BOOL isSelectMember;

@property(nonatomic,strong)NSString *fName;
@property(nonatomic,strong)NSString *fullConvLink;
@property(nonatomic,strong)NSString *profileImageURL;
@property(nonatomic,strong)NSString *profileURLString;
@property(nonatomic,assign)NSInteger receiverAge;
@property(nonatomic,assign)TMDealState dealState;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
-(BOOL)isUnreadMessageConversation;
-(TMChatUser*)getChatUser;
-(TMChatContext*)getChatContext;
-(NSString*)escapedMessage;
-(NSDictionary*)getInsertQueryParams;
-(NSDictionary*)getUpdateQueryParams;
-(NSString*)messageFormattedTimestamp;

@end


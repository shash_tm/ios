//
//  TMPhoto.swift
//  TrulyMadly
//
//  Created by Chetan Bhardwaj on 06/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMPhoto: NSObject {
    
    var admnapprov:String?
    var canprofile:String?
    var isprofile:NSString?
    var fullphotoname:String?
    var photothumbnail:String?
    var photoid:String?
    var status:String?
    
    init(photoDict:NSDictionary) {
        super.init()
        self.admnapprov = photoDict["admin_approved"] as? String
        self.canprofile = photoDict["can_profile"] as? String
        self.isprofile = photoDict["is_profile"] as? NSString
        self.fullphotoname = photoDict["name"] as? String
        self.photothumbnail = photoDict["thumbnail"] as? String
        self.photoid = photoDict["photo_id"] as? String
        self.status = photoDict["status"] as? String
    }
    
    func isProfilePhoto() -> Bool {
        if(self.isprofile == "yes") {
            return true
        }
        else {
            return false
        }
    }
    
    func canSetAsProfilePhoto() -> Bool {
        if(self.canprofile?.isKind(of: NSNull.self) != true) {
            if((self.canprofile == "no") || (status=="Rejected")) {
                return false
            }
        }
        return true
    }
    
    func isPhotoRejected() -> Bool {
        if(self.status == "Rejected") {
            return true;
        }
        return false
    }
}

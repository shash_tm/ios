//
//  QuizOptionDomainObject.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "QuizOptionDomainObject.h"

#define KEY_OPTION_ID @"option_id"
#define KEY_OPTION_TEXT @"text"
#define KEY_OPTION_IMAGE_URL @"image"

@implementation QuizOptionDomainObject

- (instancetype) initWithOptionDictionary:(NSDictionary*) optionDictionary
{
    if (self = [super init]) {
        if ([optionDictionary valueForKey:KEY_OPTION_ID] && ![[optionDictionary valueForKey:KEY_OPTION_ID] isKindOfClass:[NSNull class]]) {
            self.optionID = [NSString stringWithFormat:@"%@",[optionDictionary valueForKey:KEY_OPTION_ID]];
        }
        else {
            self.optionID = @"";
        }
        if ([optionDictionary valueForKey:KEY_OPTION_TEXT] && ![[optionDictionary valueForKey:KEY_OPTION_TEXT] isKindOfClass:[NSNull class]]) {
            self.optionText = [NSString stringWithFormat:@"%@",[optionDictionary valueForKey:KEY_OPTION_TEXT]];
        }
        else {
            self.optionText = @"";
        }
        if ([optionDictionary valueForKey:KEY_OPTION_IMAGE_URL] && ![[optionDictionary valueForKey:KEY_OPTION_IMAGE_URL] isKindOfClass:[NSNull class]]) {
            self.optionImageURL = [NSString stringWithFormat:@"%@",[optionDictionary valueForKey:KEY_OPTION_IMAGE_URL]];
        }
        else {
            self.optionImageURL = @"";
        }
        return self;
    }
    return nil;
}

@end

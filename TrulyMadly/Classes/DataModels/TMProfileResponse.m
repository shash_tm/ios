//
//  TMProfileResponse.m
//  TrulyMadly
//
//  Created by Ankit on 01/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileResponse.h"
#import "TMProfile.h"
#import "TMUserSession.h"
#import "TMSystemMessage.h"

@implementation TMProfileResponse

-(instancetype)initWithResponse:(NSDictionary*)response {
    self = [super initWithResponse:response];
    if(self) {
        self.profileList = [response objectForKey:@"data"];
        NSDictionary *systemMsgDictionary = [response objectForKey:@"system_messages"];
        self.systemMessage = [[TMSystemMessage alloc] initWithSystemMessgaeDictionary:systemMsgDictionary];
        
        NSDictionary *userDataDictionary = [response objectForKey:@"my_data"];
        if (userDataDictionary) {
            TMUserSession *userSession = [TMUserSession sharedInstance];
            [userSession setUserData:userDataDictionary];
            [userSession setUserNavigationStatusAsMovedToMatches];
            
            id mutualLikes = [userDataDictionary objectForKey:@"mutual_like_count"];
            if(mutualLikes != nil) {
                self.mutualLikeCount = [mutualLikes integerValue];
            }
            
            id unreadChats = [userDataDictionary objectForKey:@"message_count"];
            if(unreadChats != nil) {
                self.unreadChatCount = [unreadChats integerValue];
            }
            
            NSDictionary *trustScores = userDataDictionary[@"trustmeter"];
            if(trustScores != nil) {
                self.trustScores = trustScores;
            }
        }
        //system flags
//        NSDictionary *systemFlags = response[@"system_flags"];
//        if(systemFlags) {
//            self.isSocketConnectionFlagEnabled = [systemFlags[@"is_socket_enabled"] boolValue];
//            self.isSocketDebugFlagEnabled = [systemFlags[@"socket_debug_flag"] boolValue];
//        }
        
        // ab nudges
        NSDictionary *abString = response[@"abString"];
        if(abString != nil) {
            NSDictionary *AestheticsAB = abString[@"AestheticsAB"];
            if(AestheticsAB != nil) {
                id aesthetics_nudge = [AestheticsAB objectForKey:@"Aesthetics_Nudge"];
                if (aesthetics_nudge != nil) {
                    NSArray *Aesthetics_Nudge = AestheticsAB[@"Aesthetics_Nudge"];
                    
                    if([Aesthetics_Nudge[0] isValidObject] && [Aesthetics_Nudge[1] isValidObject]) {
                        NSArray *nudgePoint = Aesthetics_Nudge[0];
                        NSArray *nudgeMsg = Aesthetics_Nudge[1];
                        NSMutableDictionary* nudgeDict = [[NSMutableDictionary alloc] init];
                        for(int i=0; i<nudgePoint.count; i++) {
                            NSInteger key = [nudgePoint[i] integerValue];
                            nudgeDict[[NSNumber numberWithInt:key]] = nudgeMsg[i];
                        }
                        self.nudgeDict = nudgeDict;
                        self.isNudgeAvailable = true;
                    }else {
                        self.isNudgeAvailable = false;
                        self.nudgeDict = nil;
                    }
                }else {
                    self.isNudgeAvailable = false;
                    self.nudgeDict = nil;
                }
            }
        }
        
        // photo flow parameters
        if(response[@"validProfilePic"] && [response[@"validProfilePic"] isValidObject])
        {
            self.isValidProfilePic = [response[@"validProfilePic"] boolValue];
        }
        
        if(response[@"noPhotos"] && [response[@"noPhotos"] isValidObject])
        {
            self.hasPhotos = [response[@"noPhotos"] boolValue];
        }
        
        if(response[@"likedInThePast"] && [response[@"likedInThePast"] isValidObject])
        {
            self.hasLikedInThePast = [response[@"likedInThePast"] boolValue];
        }
    }
    return self;
}

-(TMProfile*)profileForIndex:(NSInteger)index {
    TMProfile *userProfile = nil;
    if(index < self.profileList.count) {
        NSDictionary *profileDictionary = [self.profileList objectAtIndex:index];
        userProfile = [[TMProfile alloc] initWithProfileData:profileDictionary trustScores:self.trustScores];
    }
    return userProfile;
}

-(TMProfile*)profileForIndex:(NSInteger)index editMode:(BOOL)editMode {
    TMProfile *userProfile = NULL;
    if(index<self.profileList.count) {
        NSDictionary *profileDictionary = [self.profileList objectAtIndex:index];
        userProfile = [[TMProfile alloc] initWithProfileData:profileDictionary withEditMode:editMode trustScores:self.trustScores];
    }
    return userProfile;
}

-(BOOL)isProfileAvailable {
    BOOL status = false;
    if(self.profileList.count > 0) {
        status = true;
    }
    return status;
}

@end





//
//  TMMessage.h
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMResponse.h"

@class TMChatUser;
@class TMChatSeenContext;

@interface TMMessageResponse : TMResponse

@property(nonatomic,assign)NSInteger userId;
@property(nonatomic,strong)TMChatUser *chatUserData;
@property(nonatomic,strong)TMChatSeenContext *chatSeenContext;
@property(nonatomic,strong)NSMutableArray *msgList;

@property(nonatomic,strong)NSArray *blockFlags;
@property(nonatomic,strong)NSArray *blockReasons;


@end

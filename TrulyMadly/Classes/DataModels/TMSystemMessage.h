//
//  TMSystemMessage.h
//  TrulyMadly
//
//  Created by Ankit on 14/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"


@interface TMSystemMessage : NSObject

-(instancetype)initWithSystemMessgaeDictionary:(NSDictionary*)dictionary;
-(BOOL)isFirstTileAvailable;
-(NSString*)getFirstTileTitle;
-(NSString*)getFirstTileLink;
-(NSString*)firstTileButtonTitle;
-(NSString*)lastTileMessage;
-(NSString*)getLastTileLink;
-(TMSystemMessageLinkType)getSystemMessageLinkTypeForFirstTile;
-(TMSystemMessageLinkType)getSystemMessageTypeForLastTileLink;
-(NSString*)getLikeActionMessage;
-(NSDictionary*)deactivationDictionary;

@end

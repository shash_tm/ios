//
//  TMProfileItem.h
//  TrulyMadly
//
//  Created by Ankit on 24/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSObject+TMAdditions.h"

@interface TMProfileItem : NSObject

@property(nonatomic,assign)CGFloat headerHeight;
@property(nonatomic,assign)BOOL editMode;
@property(nonatomic,assign)UIEdgeInsets contentInset;
@property(nonatomic,assign)BOOL isProfileAdCampaign;
@property(nonatomic,assign)BOOL isUserNRI;

-(CGFloat)spaceBetweenHeaderAndFirstViewComponent;

@end

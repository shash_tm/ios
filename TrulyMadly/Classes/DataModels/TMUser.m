//
//  TMUser.m
//  TrulyMadly
//
//  Created by Ankit on 26/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMUser.h"
#import "TMDataStore.h"
#import "NSObject+TMAdditions.h"

@interface TMUser ()

@property(nonatomic,assign)BOOL isSelectUser;

@end

@implementation TMUser

-(instancetype)init {
    self = [super init];
    if(self) {
        self.userId = [TMDataStore retrieveObjectforKey:@"userid"];
    }
    return self;
}

-(void)setUserDefaults {
    self.userId = nil;
    self.mutualLikeCount = nil;
    self.hasuserUnlikedProfile = nil;
    self.isInterPersonalFilled = FALSE;
    self.isValuesFilled = FALSE;
    self.isFavouriteFilled = FALSE;
    self.profilePicURL = FALSE;
    [TMDataStore removeObjectforKey:@"userid"];
    [TMDataStore removeObjectforKey:@"gender"];
    [TMDataStore removeObjectforKey:@"fname"];
    [TMDataStore removeObjectforKey:@"city"];
    [TMDataStore removeObjectforKey:@"state"];
    [TMDataStore removeObjectforKey:@"age"];
    [TMDataStore removeObjectforKey:@"status"];
    [TMDataStore removeObjectforKey:@"country_code"];
}

-(void)setUserDataFromDictioanry:(NSDictionary*)userDataDictionary {
   
    NSString *userStatus = [userDataDictionary objectForKey:@"status"];
    if(userStatus && [userStatus isKindOfClass:[NSString class]]) {
        [TMDataStore setObject:userStatus forKey:@"status"];
    }
    
    if([userDataDictionary objectForKey:@"fname"]) {
         NSString *fname = [userDataDictionary objectForKey:@"fname"];
        [TMDataStore setObject:fname forKey:@"fname"];
    }
    NSString *city = [userDataDictionary objectForKey:@"city"];
    if((city) && ([city isValidObject])) {
        NSString *city = [userDataDictionary objectForKey:@"city"];
        [TMDataStore setObject:city forKey:@"city"];
    }
    NSString *state = [userDataDictionary objectForKey:@"state"];
    if((state) && [state isValidObject]) {
        [TMDataStore setObject:state forKey:@"state"];
    }
   
    NSString *gender = [userDataDictionary objectForKey:@"gender"];
    [TMDataStore setObject:gender forKey:@"gender"];
    if([userDataDictionary objectForKey:@"profile_pic"]) {
        self.profilePicURL = [userDataDictionary objectForKey:@"profile_pic_full"];
    }
    
    NSNumber *age = [userDataDictionary objectForKey:@"age"];
    if(age != nil && ([age isValidObject]) ) {
        [TMDataStore setObject:age forKey:@"age"];
    }
    
    NSNumber *country = [userDataDictionary objectForKey:@"country_code"];
    if(country != nil && ([country isValidObject])) {
        [TMDataStore setObject:country forKey:@"country_code"];
    }
    
    self.mutualLikeCount = [userDataDictionary objectForKey:@"mutual_like_count"];
    self.isInterPersonalFilled = [[userDataDictionary objectForKey:@"interpersonal_filled"] boolValue];
    self.isValuesFilled = [[userDataDictionary objectForKey:@"values_filled"] boolValue];
    self.isFavouriteFilled = [[userDataDictionary objectForKey:@"favourite_filled"] boolValue];
    
    ///
    NSString *cachedValue = [TMDataStore retrieveObjectforKey:@"hasuserunlikedprofile"];
    BOOL hasuserUnlikedProfile = false;
    if(cachedValue != nil) {
        hasuserUnlikedProfile = true;
    }
    self.hasuserUnlikedProfile = [NSNumber numberWithBool:hasuserUnlikedProfile];
    
    /////
    NSString *userId = [userDataDictionary objectForKey:@"user_id"];
    if(userId && [userId isKindOfClass:[NSString class]]) {
        self.userId = userId;
        //update cache
        [TMDataStore setObject:userId forKey:@"userid"];
    }
    if([userDataDictionary objectForKey:@"fb_designation"] && !([userDataDictionary[@"fb_designation"] isKindOfClass:[NSNull class]])){
        NSString *fbDesignation = [userDataDictionary objectForKey:@"fb_designation"];
        [TMDataStore setObject:fbDesignation forKey:@"fb_designation"];
    }
}

-(TMUserStatus)userStatus {
    TMUserStatus status;
    if([self.status isEqualToString:@"authentic"]) {
        status = AUTHENTIC;
    }
    else if([self.status isEqualToString:@"non-authentic"]) {
        status = NON_AUTHENTIC;
    }
    else if([self.status isEqualToString:@"incomplete"]) {
        status = INCOMPLETE;
    }else if([self.status isEqualToString:@"suspended"]) {
        status = SUSPENDED;
    }else if([self.status isEqualToString:@"blocked"]) {
        status = BLOCKED;
    }
    return status;
}

///////Like Action ////////////
-(BOOL)isNonAuthenticUserLikeActionRegistered {
    BOOL nonAuthenticUserLikedActionRegistered = false;
    
    NSString *cachedValue = [TMDataStore retrieveObjectforKey:@"register_noauthentic_user_like_action"];
    
    if(cachedValue != nil) {
        nonAuthenticUserLikedActionRegistered = true;
    }
    return nonAuthenticUserLikedActionRegistered;
}

-(void)registerNonAuthenticUserLikeAction {
    [TMDataStore setObject:@"true" forKey:@"register_noauthentic_user_like_action"];
}

/////// Unlike action /////
-(BOOL)isUserUnlikeActionRegisteredFromEvent {
    BOOL userUnlikedActionRegistered = false;
    
    NSString *cachedValue = [TMDataStore retrieveObjectforKey:@"register_user_unlike_action_event"];
    
    if(cachedValue != nil) {
        userUnlikedActionRegistered = true;
    }
    return userUnlikedActionRegistered;
}

-(void)registerUserUnlikeActionFromEvent {
    [TMDataStore setObject:@"true" forKey:@"register_user_unlike_action_event"];
}

-(BOOL)isUserUnlikeActionRegistered {
    BOOL userUnlikedActionRegistered = false;
    
    NSString *cachedValue = [TMDataStore retrieveObjectforKey:@"register_user_unlike_action"];
    
    if(cachedValue != nil) {
        userUnlikedActionRegistered = true;
    }
    return userUnlikedActionRegistered;
}

-(void)registerUserUnlikeAction {
    [TMDataStore setObject:@"true" forKey:@"register_user_unlike_action"];
}

-(BOOL)isInterPersonalQuizFilledByUser {
    return self.isInterPersonalFilled;
}

-(BOOL)isValuesQuizFilledByUser {
    return self.isValuesFilled;
}

////////
-(void)registerNonAuthenticUserFirstInteractionWithMatch {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"isNonAuthenticUserFirstInteractionWithMatchCompleted"];
}
-(BOOL)isNonAuthenticUserFirstInteractionedWithMatch {
    
    NSNumber *status = [TMDataStore retrieveObjectforKey:@"isNonAuthenticUserFirstInteractionWithMatchCompleted"];
    if(status.boolValue == 1) {
        return true;
    }
    return false;
}
-(NSString*)userGenderString {
    if([self isUserFemale]) {
        return @"female";
    }
    else {
         return @"male";
    }
}
-(BOOL) isUserFemale {
     if([self.gender isEqualToString:@"F"]) {
         return YES;
     }
    return NO;
}

- (NSString*) getIntentionSurveyUserIDKey {
    if (self.userId) {
        return [INTENTION_SURVEY_STATUS stringByAppendingString:self.userId];
    }
    else {
        return INTENTION_SURVEY_STATUS;
    }
}
-(BOOL)isUserCountryIndia {
    if(self.country == 113) {
        return TRUE;
    }
    return FALSE;
}
-(NSString*)getUserAttributeMoEngageKey {
    if(self.userId) {
        return [@"isSetUserAttributeMoEngage_" stringByAppendingString:self.userId];
    }
    return @"isSetUserAttributeMoEngage_";
}

-(NSString*)fName {
    return [TMDataStore retrieveObjectforKey:@"fname"];
}

-(NSString*)gender {
    return [TMDataStore retrieveObjectforKey:@"gender"];
}
-(NSString*)fbDesignation {
    NSString* designation = [TMDataStore retrieveObjectforKey:@"fb_designation"];
    return designation;
}
-(NSString*)city {
    return [TMDataStore retrieveObjectforKey:@"city"];
}

-(NSString*)state {
    return [TMDataStore retrieveObjectforKey:@"state"];
}
-(int)country {
    NSNumber *countryCode = [TMDataStore retrieveObjectforKey:@"country_code"];
    if(countryCode) {
        int country = countryCode.intValue;
        return country;
    }
    return 113;
}

-(int)age {
    NSString *ageStr = [TMDataStore retrieveObjectforKey:@"age"];
    int age = ageStr.intValue;
    return age;
}

-(NSString*)status {
    return [TMDataStore retrieveObjectforKey:@"status"];
}

-(void)setSelectStatus:(NSDictionary*)selectData {
    NSString *daysLeft = selectData[@"days_left"];
    if([daysLeft isKindOfClass:[NSString class]]) {
        [TMDataStore setObject:daysLeft forKey:@"com.select.expirydaysleft"];
    }
    else {
        [TMDataStore setObject:nil forKey:@"com.select.expirydaysleft"];
    }
    
    BOOL selectUser = [selectData[@"is_tm_select"] boolValue];
    [TMDataStore setBool:selectUser forKey:@"com.select.user"];
    
    NSString *profileCTA = selectData[@"profile_cta"];
    if(profileCTA && [profileCTA isKindOfClass:[NSString class]]) {
        [TMDataStore setObject:profileCTA forKey:@"com.select.profilecta"];
    }
    
    BOOL quizPlayed = [selectData[@"quiz_played"] boolValue];
    [TMDataStore setBool:quizPlayed forKey:@"com.select.quizplayed"];
    
    NSString *sideCTA = selectData[@"side_cta"];
    if(sideCTA && [sideCTA isKindOfClass:[NSString class]]) {
        [TMDataStore setObject:sideCTA forKey:@"com.select.sidecta"];
    }
    
    //as default text for buy free trail pack view
    NSString *profileCTAText = selectData[@"profile_cta_text"];
    if(profileCTAText && [profileCTAText isKindOfClass:[NSString class]]) {
        [TMDataStore setObject:profileCTAText forKey:@"com.select.profilectatext"];
    }
    
    [TMDataStore synchronize];
}
-(BOOL)isSelectUser {
    BOOL selectUser = [TMDataStore retrieveBoolforKey:@"com.select.user"];
    return selectUser;
}
-(NSInteger)selectExpiryDaysLeft {
    NSString *daysLeft = [TMDataStore retrieveObjectforKey:@"com.select.expirydaysleft"];
    return [daysLeft integerValue];
}
-(NSString*)selectExpiryDaysLeftText {
    NSString *daysLeft = [TMDataStore retrieveObjectforKey:@"com.select.expirydaysleft"];
    if(daysLeft  && [daysLeft isKindOfClass:[NSString class]]) {
        NSString *expiryMessage;
        if([daysLeft intValue] == 1) {
            expiryMessage = [NSString stringWithFormat:@"Expires in %@ Day",daysLeft];
        }else {
            expiryMessage = [NSString stringWithFormat:@"Expires in %@ Days",daysLeft];
        }
        return expiryMessage;
    }
    return nil;
}
-(BOOL)isQuizPlayed {
    BOOL quizPlayed = [TMDataStore retrieveBoolforKey:@"com.select.quizplayed"];
    return quizPlayed;
}
-(NSString*)selectSideMenuCta {
    NSString *sideCTA = [TMDataStore retrieveObjectforKey:@"com.select.sidecta"];
    return sideCTA;
}
-(NSString*)selectProfileCta {
    NSString *profileCTA = [TMDataStore retrieveObjectforKey:@"com.select.profilecta"];
    return profileCTA;
}
-(NSString*)selectProfileCtaText {
    NSString *profileCTA = [TMDataStore retrieveObjectforKey:@"com.select.profilectatext"];
    return profileCTA;
}

@end


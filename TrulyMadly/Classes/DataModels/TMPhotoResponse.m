//
//  TMPhotoResponse.m
//  TrulyMadly
//
//  Created by Ankit on 01/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPhotoResponse.h"
#import "TMSwiftHeader.h"

@implementation TMPhotoResponse

-(instancetype)initWithResponse:(NSDictionary*)response {
    self = [super initWithResponse:response];
    if(self) {
        self.photos = [NSMutableArray arrayWithCapacity:4];
        
        NSNumber *allowSkip = [response objectForKey:@"allowSkip"];
        self.allowSkip = [allowSkip boolValue];
        
        NSArray *photoList = [response objectForKey:@"data"];
        for (NSInteger i = photoList.count-1; i >= 0; i--) {
            NSDictionary *photoDict = [photoList objectAtIndex:i];
            NSString *isProfilePhoto = [photoDict objectForKey:@"is_profile"];
            
            if([isProfilePhoto isEqualToString:@"yes"]) {
                [self.photos insertObject:photoDict atIndex:0];
            }
            else {
                [self.photos addObject:photoDict];
            }
        }
    }
    return self;
}

-(TMPhoto*)photoForIndex:(NSInteger)index {
    
    TMPhoto *photoObj = NULL;
    if(index<self.photos.count) {
        NSDictionary *photoDict = [self.photos objectAtIndex:index];
        photoObj = [[TMPhoto alloc] initWithPhotoDict:photoDict];
    }
    return photoObj;
}



@end

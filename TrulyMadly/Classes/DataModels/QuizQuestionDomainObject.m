//
//  QuizQuestionDomainObject.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "QuizQuestionDomainObject.h"

#define KEY_QUIZ_ID @"quiz_id"
#define KEY_QUESTION_ID @"Question_id"
#define KEY_QUESTION_TEXT @"question_text"
#define KEY_QUESTION_IMAGE_URL @"question_image"
#define KEY_QUESTION_OPTIONS @"answers"
#define KEY_MATCH_ANSWER @"match_answer"
#define KEY_USER_ANSWER @"user_answer"

@implementation QuizQuestionDomainObject

- (instancetype) initWithCommonAnswersResponseDictionary:(NSDictionary*) commonAnswerDictionary
{
    if (self = [super init]) {
        if ([commonAnswerDictionary valueForKey:KEY_QUIZ_ID] && ![[commonAnswerDictionary valueForKey:KEY_QUIZ_ID] isKindOfClass:[NSNull class]]) {
            self.quizID = [NSString stringWithFormat:@"%@",[commonAnswerDictionary valueForKey:KEY_QUIZ_ID]];
        }
        if ([commonAnswerDictionary valueForKey:KEY_QUESTION_ID] && ![[commonAnswerDictionary valueForKey:KEY_QUESTION_ID] isKindOfClass:[NSNull class]]) {
            self.questionID = [NSString stringWithFormat:@"%@",[commonAnswerDictionary valueForKey:KEY_QUESTION_ID]];
        }
        if ([commonAnswerDictionary valueForKey:KEY_QUESTION_TEXT] && ![[commonAnswerDictionary valueForKey:KEY_QUESTION_TEXT] isKindOfClass:[NSNull class]]) {
            self.questionText = [NSString stringWithFormat:@"%@",[commonAnswerDictionary valueForKey:KEY_QUESTION_TEXT]];
        }
        if ([commonAnswerDictionary valueForKey:KEY_QUESTION_IMAGE_URL] && ![[commonAnswerDictionary valueForKey:KEY_QUESTION_IMAGE_URL] isKindOfClass:[NSNull class]]) {
            self.questionImageURL = [NSString stringWithFormat:@"%@",[commonAnswerDictionary valueForKey:KEY_QUESTION_IMAGE_URL]];
        }
        
        //create the options here
        if ([commonAnswerDictionary valueForKey:KEY_QUESTION_OPTIONS] && [[commonAnswerDictionary valueForKey:KEY_QUESTION_OPTIONS] isKindOfClass:[NSArray class]]) {
            
            NSArray* optionsDictionary = [commonAnswerDictionary valueForKey:KEY_QUESTION_OPTIONS];
            
            NSMutableArray* optionsObjectArray = [[NSMutableArray alloc] init];
            
            for (int index = 0; index < optionsDictionary.count; index++) {
                if ([[optionsDictionary objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                    QuizOptionDomainObject* option = [[QuizOptionDomainObject alloc] initWithOptionDictionary:[optionsDictionary objectAtIndex:index]];
                    [optionsObjectArray addObject:option];
                }
            }
            self.options = optionsObjectArray;
        }
        else {
            self.options = [[NSMutableArray alloc] init];
        }
        if ([commonAnswerDictionary valueForKey:KEY_USER_ANSWER] && ![[commonAnswerDictionary valueForKey:KEY_USER_ANSWER] isKindOfClass:[NSNull class]]) {
            QuizOptionDomainObject* userOption = [[QuizOptionDomainObject alloc] initWithOptionDictionary:[commonAnswerDictionary valueForKey:KEY_USER_ANSWER]];
            self.userAnswer = userOption;
        }
        if ([commonAnswerDictionary valueForKey:KEY_MATCH_ANSWER] && ![[commonAnswerDictionary valueForKey:KEY_MATCH_ANSWER] isKindOfClass:[NSNull class]]) {
            QuizOptionDomainObject* matchOption = [[QuizOptionDomainObject alloc] initWithOptionDictionary:[commonAnswerDictionary valueForKey:KEY_MATCH_ANSWER]];
            self.matchAnswer = matchOption;
        }
        
        //is answer common
        if ([commonAnswerDictionary valueForKey:KEY_USER_ANSWER] && ![[commonAnswerDictionary valueForKey:KEY_USER_ANSWER] isKindOfClass:[NSNull class]] && [commonAnswerDictionary valueForKey:KEY_MATCH_ANSWER] && ![[commonAnswerDictionary valueForKey:KEY_MATCH_ANSWER] isKindOfClass:[NSNull class]]) {
            //both the answers are present
            if ([self.userAnswer.optionText isEqualToString:self.matchAnswer.optionText] || [self.userAnswer.optionImageURL isEqualToString:self.matchAnswer.optionImageURL]) {
                //common answer
                self.isAnswerCommon = YES;
            }
        }
        
        return self;
    }
    return nil;
}

@end

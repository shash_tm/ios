//
//  TMEditPreference.h
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMSwiftHeader.h"

@protocol TMEditPreferenceDelegate;

@interface TMEditPreference : NSObject <TMDataTableViewControllerDelegate>

@property(nonatomic,weak)id<TMEditPreferenceDelegate> delegate;
@property(nonatomic,strong)NSMutableArray* regionToToDisplay;
@property(nonatomic,strong)NSMutableArray* statesToDisplay;
@property(nonatomic,strong)NSMutableArray* citiesToDisplay;
@property(nonatomic,strong)NSMutableArray* states;
@property(nonatomic,strong)NSMutableArray* cities;
@property(nonatomic,strong)NSMutableArray* regions;
@property(nonatomic,strong)NSString* basicDataURLString;
@property(nonatomic,strong)NSString* startAge;
@property(nonatomic,strong)NSString* endAge;
@property(nonatomic,strong)NSString* startHeight;
@property(nonatomic,strong)NSString* endHeight;
@property(nonatomic,strong)NSString* startHeightInFeetandInch;
@property(nonatomic,strong)NSString* endHeightInFeetandInch;
@property(nonatomic,assign)BOOL showOutsideIndiaProfiles;
@property(nonatomic,assign)BOOL outsideProfileSwitchEnabled;

- (instancetype)initWithEditPreferenceData:(NSDictionary*)editPreferenceData;
-(void)loadStateAndCityData;
-(void)updateLocationSelectionState;
-(NSDictionary*)getEditPrefSaveRequestfParams;
-(NSString*)getHeightInFeetandInch:(NSInteger)height;
-(NSString*)getSelectedLocationCellHeaderText;
-(NSString*)getSelectedCityText;
-(NSString*)getSelectedCityJoinedText;
-(NSString*)getSelectedStateText;
-(NSString*)getSelectedStateJoinedText;
-(NSString*)getSelectedRegionText;
-(NSString*)getSelectedRegionJoinedText;

@end


@protocol TMEditPreferenceDelegate <NSObject>

-(void)didChangePreference;

@end

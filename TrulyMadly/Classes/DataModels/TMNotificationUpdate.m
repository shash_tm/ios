//
//  TMNotificationUpdate.m
//  TrulyMadly
//
//  Created by Ankit on 14/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMNotificationUpdate.h"

@implementation TMNotificationUpdate

-(instancetype)initWithResponse:(NSDictionary*)response {
    self = [super init];
    if(self) {
        NSDictionary *counterDictionary = response[@"counters"];
        self.conversationCount = [counterDictionary[@"conversation_count"] integerValue];
        NSArray *unreadConversations = counterDictionary[@"unread_messages"];
        if(unreadConversations && [unreadConversations isKindOfClass:[NSArray class]]) {
            self.unreadConversations = unreadConversations;
        }
    }
    return self;
}

@end

//
//  TMSelectQuizResultInfo.h
//  TrulyMadly
//
//  Created by Ankit on 07/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMSelectQuizResultInfo : NSObject

@property(nonatomic,strong)NSString* questionText;
@property(nonatomic,assign)NSInteger matchOption;
@property(nonatomic,assign)NSInteger userOption;

- (instancetype)initWithQuizResultDictionary:(NSDictionary*)quizResultDictionary;
-(UIColor*)getBoundaryColorForUser;
-(UIColor*)getBoundaryColorForMatch;
-(UIColor*)connectingColor;
-(BOOL)showConnectingView;
-(UIImage*)connectingImage;
-(UIImage*)getUserActionImage;
-(UIImage*)getMatchActionImage;

@end

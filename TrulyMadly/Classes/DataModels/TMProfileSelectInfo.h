//
//  TMProfileSelectInfo.h
//  TrulyMadly
//
//  Created by Ankit on 03/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMProfileSelectInfo : NSObject

@property(nonatomic,assign,readonly)BOOL isSelectUser;
@property(nonatomic,strong,readonly)NSString* commonImageLink;
@property(nonatomic,strong,readonly)NSString* defaultText;
@property(nonatomic,strong,readonly)NSString* quotesText;

- (instancetype)initWithDictionary:(NSDictionary*)selectProfileData;
-(NSString*)getCommonText;

@end

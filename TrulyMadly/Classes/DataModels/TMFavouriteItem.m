//
//  TMFavouriteItem.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMFavouriteItem.h"

@interface TMFavouriteItem ()

@property(nonatomic,strong)NSArray *favs;
@property(nonatomic,strong)NSString *activeImageStr;
@property(nonatomic,strong)NSString *inActiveImageStr;
@property(nonatomic,strong)NSString *disabledImageStr;
@property(nonatomic,strong)NSString *commonImageStr;

@end


@implementation TMFavouriteItem

-(void)configureFavouriteItem:(NSArray*)favList withKey:(NSString*)key {
    if(favList.count > 0) {
        self.status = true;
        self.favs = favList;
    }
    
    self.activeImageStr = [NSString stringWithFormat:@"%@_highlight",key];
    self.inActiveImageStr = [NSString stringWithFormat:@"%@_inactive",key];
    self.disabledImageStr = [NSString stringWithFormat:@"%@_unavailable",key];
    self.commonImageStr = [NSString stringWithFormat:@"%@_inactive",key];
}


@end

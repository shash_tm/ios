//
//  TMMutualLikeItem.h
//  TrulyMadly
//
//  Created by Ankit on 20/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMMutualLikeItem : NSObject

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@property(nonatomic,strong)NSString *matchedtTimestamp;
@property(nonatomic,assign)NSInteger age;
@property(nonatomic,strong)NSString *blockedShownUrl;
@property(nonatomic,strong)NSString *city;
@property(nonatomic,strong)NSString *fName;
@property(nonatomic,assign)BOOL isBlocked;
@property(nonatomic,strong)NSString *matchedOn;
@property(nonatomic,strong)NSString *fullConvLink;
@property(nonatomic,assign)NSInteger receiverId;
@property(nonatomic,strong)NSString *profilePic;


@end

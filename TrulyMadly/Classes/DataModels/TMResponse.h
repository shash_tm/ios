//
//  TMResponse.h
//  TrulyMadly
//
//  Created by Ankit on 01/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMResponse : NSObject

@property(nonatomic,strong)NSDictionary *responseDictionary;
@property(nonatomic,strong)NSArray *responseArray;
@property(nonatomic,assign)NSInteger serverResponseCode;

-(instancetype)initWithResponseDictionary:(NSDictionary*)responseDictionary;
-(instancetype)initWithResponseArray:(NSArray*)responseArray;
-(instancetype)initWithResponse:(NSDictionary*)response;
-(NSInteger)responseCode;
-(id)data;
-(id)myData;
-(NSDictionary*)systemMessageDictionary;
-(NSString*)status;
-(NSDictionary*)response;
-(NSString*)error;
-(NSArray*)psychoQuestion;

@end




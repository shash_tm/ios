//
//  TMProfileLikes.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMProfileItem.h"
#import "TMEnums.h"

@interface TMProfileLikes : TMProfileItem

@property(nonatomic,strong)NSMutableArray* likes;
@property(nonatomic,strong)NSArray* commonLikes;
@property(nonatomic,assign)CGFloat likeViewHeight;
@property(nonatomic,assign)UIEdgeInsets likeViewInset;
@property(nonatomic,assign)CGFloat likeViewHorizontalMargin;
@property(nonatomic,assign)CGFloat likeViewNewLineVerticalMargin;
@property(nonatomic,assign)BOOL showHeader;


-(instancetype)initWithCommonLikes:(NSArray*)commonLikes;
-(instancetype)initWithProfileLikes:(NSArray*)profileLikes
                 withCommonHashtags:(NSArray*)commonHashtags;

-(CGFloat)likesContentHeight:(CGFloat)maxWidth;
-(NSString*)likeHeaderTextForEditMode;
-(BOOL)isCommonLikeAvailable;
-(NSString*)commonLikeCountAsText;
-(NSString*)commonLikeHeaderText;
-(NSString*)commonLikeImageString;
//-(TMProfileCollectionViewCellType)likeRowType;
-(CGFloat)getLikeViewHeight;
-(UIFont*)commonLikeTitleFont;

@end

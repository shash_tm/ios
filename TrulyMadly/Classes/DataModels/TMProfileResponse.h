//
//  TMProfileResponse.h
//  TrulyMadly
//
//  Created by Ankit on 01/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMResponse.h"

@class TMSystemMessage;
@class TMProfile;


@interface TMProfileResponse : TMResponse

@property(nonatomic,strong)NSArray *profileList;
@property(nonatomic,strong)TMSystemMessage *systemMessage;

@property(nonatomic,assign)NSInteger mutualLikeCount;
@property(nonatomic,assign)NSInteger unreadChatCount;

@property(nonatomic,assign)BOOL isSocketConnectionFlagEnabled;
@property(nonatomic,assign)BOOL isSocketDebugFlagEnabled;

@property(nonatomic,strong)NSDictionary *trustScores;
@property(nonatomic,strong)NSDictionary *nudgeDict;
@property(nonatomic,assign)BOOL isNudgeAvailable;

@property(nonatomic,assign)BOOL isValidProfilePic;
@property(nonatomic,assign)BOOL hasPhotos;
@property(nonatomic,assign)BOOL hasLikedInThePast;


-(TMProfile*)profileForIndex:(NSInteger)index;
-(TMProfile*)profileForIndex:(NSInteger)index editMode:(BOOL)editMode;  
-(BOOL)isProfileAvailable;

@end
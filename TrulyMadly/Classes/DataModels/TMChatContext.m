//
//  TMChatContext.m
//  TrulyMadly
//
//  Created by Ankit on 12/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMChatContext.h"
#import "TMUserSession.h"


@implementation TMChatContext

- (instancetype)initWithLoggedInUser {
    self = [super init];
    if (self) {
        self.loggedInUserId = [TMUserSession sharedInstance].user.userId.integerValue;
    }
    return self;
}

@end

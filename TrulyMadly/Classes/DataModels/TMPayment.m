//
//  TMPayment.m
//  TrulyMadly
//
//  Created by Ankit on 29/09/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMPayment.h"

#define SPARK_BUY_ERRORMSG @"Problem in buying spark. Please try again"

#define SELECT_BUY_ERRORMSG @"Problem in buying select. Please try again"

@interface TMPayment ()

//@property(nonatomic,assign)TMPaymentState paymentState;
//@property(nonatomic,assign)TMPaymentStatus paymentStatus;
//@property(nonatomic,assign)NSInteger sparkCount;
//@property(nonatomic,strong)NSString *packageIdentifier;
@property(nonatomic,strong)NSString *errorDisplayText;
@property(nonatomic,strong)NSString *errorTrackingText;
//@property(nonatomic,assign)BOOL showChatAssist;


@end


@implementation TMPayment

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(NSString*)errorDisplayText {
    if(self.packageType == Spark) {
        return SPARK_BUY_ERRORMSG;
    }
    else {
        return SELECT_BUY_ERRORMSG;
    }
}
-(void)setPaymentFailureReason:(NSString*)failureReason{
    self.errorTrackingText = failureReason;
}

@end

//
//  TMProfileLikes.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileLikes.h"
#import "TMProfileInterest.h"

@interface TMProfileLikes ()

@end


@implementation TMProfileLikes

-(instancetype)initWithCommonLikes:(NSArray*)commonLikes {
    self = [super init];
    if(self) {
        [self setDefaults];
        self.commonLikes = commonLikes;
    }
    return self;
}

-(instancetype)initWithProfileLikes:(NSArray*)profileLikes withCommonHashtags:(NSArray*)commonHashtags {
    self = [super init];
    if(self) {
        [self setDefaults];
        
        self.likes = [NSMutableArray arrayWithCapacity:16];
        
        NSInteger index = 0;
        if([profileLikes isValidObject]) { //to prevent anc check for nil crash in swift
            for (NSString *interest in profileLikes) {
                //check for common like
                BOOL isCommonLike = false;
                if(commonHashtags) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF Like %@",interest];
                    NSArray *filteredArray = [commonHashtags filteredArrayUsingPredicate:predicate];

                    if(filteredArray.count == 1) {
                        isCommonLike = true;
                    }
                }
                
                TMProfileInterest *profileInterest = [[TMProfileInterest alloc] initWithTag:interest isCommonLike:isCommonLike];
                [self.likes insertObject:profileInterest atIndex:index];
                index++;
            }
        }
    }
    return self;
}

-(void)setDefaults {
    self.likeViewHeight = 32;
    self.likeViewInset = UIEdgeInsetsMake(0, 12, 0, 12);
    self.likeViewHorizontalMargin = 7;
    self.likeViewNewLineVerticalMargin = 10;
}

-(CGFloat)likesContentHeight:(CGFloat)maxWidth {
    CGFloat contentHeight = 0;
    CGFloat xPos = 0;
    CGFloat yPos = 0;
    for (int i=0; i<self.likes.count;i++) {
        TMProfileInterest *profileInterest = [self.likes objectAtIndex:i];
        CGFloat likeViewWidth = [profileInterest tagSizeForMaximumWidth:maxWidth].width + self.likeViewInset.left+self.likeViewInset.right;
        
        if((xPos+likeViewWidth) > maxWidth) {
            xPos = 0;
            yPos = yPos + self.likeViewHeight + self.likeViewNewLineVerticalMargin;
        }
        xPos = xPos + likeViewWidth + self.likeViewHorizontalMargin;
    }
    contentHeight = yPos + self.likeViewHeight;
    if(self.editMode) {
        contentHeight += self.headerHeight;
    }
    return contentHeight;
}

-(NSString*)likeHeaderTextForEditMode {
    return @"My Hashtags";
}

-(BOOL)isCommonLikeAvailable {
    if(self.commonLikes.count != 0){
        return true;
    }
    return false;
}

-(NSString*)commonLikeCountAsText  {
    NSString *text = NULL;
    NSInteger count = (self.commonLikes.count) ? self.commonLikes.count : 0;
    text = [NSString stringWithFormat:@"%ld",(long)count];
    return text;
}

-(NSString*)commonLikeHeaderText {
    return @"On Interests";
}
-(UIFont*)commonLikeTitleFont {
    return [UIFont systemFontOfSize:26];
}
-(NSString*)commonLikeImageString {
    NSString *imgStr = NULL;
    if([self isCommonLikeAvailable]) {
        imgStr = @"score_active";
    }
    else {
        imgStr = @"score_inactive";
    }
    return imgStr;
}

//-(TMProfileCollectionViewCellType)likeRowType {
//    TMProfileCollectionViewCellType cellType = TMPROFILECOLLECTIONVIEWCELL_COMMONLIKELSIT;
//    return cellType;
//}

-(CGFloat)getLikeViewHeight {
    return self.likeViewHeight;
}

@end


//
//  TMMessageReceiver.m
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageReceiver.h"
#import "TMUserSession.h"


@interface TMMessageReceiver ()


@end

@implementation TMMessageReceiver

-(instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    if(self) {
    
        @try {
            self.isAdmin = [[dictionary objectForKey:@"is_admin_set"] boolValue];
            self.fName = [dictionary objectForKey:@"fname"];
            NSNumber *lastSeenMsgId = [dictionary objectForKey:@"last_seen_msg_id"];
            if(lastSeenMsgId && ![lastSeenMsgId isKindOfClass:[NSNull class]]) {
                self.lastSeenMsgId = lastSeenMsgId;
            }
            self.lastSeenReceiverTs = [dictionary objectForKey:@"last_seen_receiver_tstamp"];
            self.userId = [[dictionary objectForKey:@"profile_id"] integerValue];
            self.profileLink = [dictionary objectForKey:@"profile_link"];
            self.profilePic = [dictionary objectForKey:@"profile_pic"];
        }
        @catch (NSException *exception) {
            
        }
    }
    return self;
    
}

-(NSInteger)loggedInUserId {
    NSInteger userId = [[TMUserSession sharedInstance].user.userId integerValue];
    return userId;
}

@end

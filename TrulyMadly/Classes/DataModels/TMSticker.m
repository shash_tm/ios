//
//  TMSticker.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 20/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMSticker.h"
#import "NSObject+TMAdditions.h"
#import "TMBuildConfig.h"
#import "TMDataStore.h"
#import "TMStickerDBManager.h"

#define STICKER_ID @"id"
#define STICKER_ID_CUSTOM @"sticker_id"
#define GALLERY_ID @"gallery_id"
#define IS_ACTIVE @"active"
#define UPDATED_IN_VERSION @"updated_in_version"

#define PNG_FORMAT_EXTENSION @"png"
#define WEBP_FORMAT_EXTENSTION @"webp"

#define CURRENT_FORMAT_EXTENSION PNG_FORMAT_EXTENSION

@implementation TMSticker

/**
 * Dummy dictionary
 
 "id": "4",
  "name": "4",
  "type": "sticker",
  "active": "true",
  "gallery_id": "1",
 "default_download": "false",
  "updated_in_version": "1"
 */
- (instancetype) initWithDictionary:(NSDictionary*) stickerInfoDictionary
{
    if (self = [super init]) {
        if (stickerInfoDictionary) {
            if ([stickerInfoDictionary valueForKey:STICKER_ID]) {
                self.stickerID = ([stickerInfoDictionary valueForKey:STICKER_ID] && [[stickerInfoDictionary valueForKey:STICKER_ID] isValidObject])?[stickerInfoDictionary valueForKey:STICKER_ID]:nil;
            }
            else if ([stickerInfoDictionary valueForKey:STICKER_ID_CUSTOM]) {
                self.stickerID = ([stickerInfoDictionary valueForKey:STICKER_ID_CUSTOM] && [[stickerInfoDictionary valueForKey:STICKER_ID_CUSTOM] isValidObject])?[stickerInfoDictionary valueForKey:STICKER_ID_CUSTOM]:nil;
            }
            
            self.galleryID = ([stickerInfoDictionary valueForKey:GALLERY_ID] && [[stickerInfoDictionary valueForKey:GALLERY_ID] isValidObject])?[stickerInfoDictionary valueForKey:GALLERY_ID]:nil;
            
            //updated the file location values
            [self updateFileLocationAndURLValues];
            
            return self;
        }
    }
    return nil;
}

- (instancetype) initWithGalleryID:(NSString*) galleryID stickerID:(NSString*) stickerID {
    TMStickerDBManager* stickerDBManager = [[TMStickerDBManager alloc] init];
    if ([stickerDBManager getStickerWithStickerID:stickerID galleryID:galleryID]) {
        self = [self initWithDictionary:[stickerDBManager getStickerWithStickerID:stickerID galleryID:galleryID]];
        return self;
    }
    else {
        self = [super init];
        self.galleryID = galleryID;
        self.stickerID = stickerID;
        
        //updated the file location values
        [self updateFileLocationAndURLValues];
        
        return self;
    }
    return nil;
}

- (instancetype) initWithStickerURLString:(NSString*) stickerURLString {
    @try {
        if (stickerURLString) {
            if ([stickerURLString componentsSeparatedByString:@"/"].count) {
                //obtain the sticker id
                NSString* stickerSubstring = [[stickerURLString componentsSeparatedByString:@"/"] lastObject];
                NSString* stickerID = [[stickerSubstring componentsSeparatedByString:@"."] firstObject];
                
                if ([stickerURLString componentsSeparatedByString:@"/"].count >= 3) {
                    int count = (int)[stickerURLString componentsSeparatedByString:@"/"].count;
                    NSString* gallerySubString = [[stickerURLString componentsSeparatedByString:@"/"] objectAtIndex:count - 3];
                    
                    NSString* galleryID;
                    
                    if ([gallerySubString componentsSeparatedByString:@"_"].count > 1) {
                        galleryID = [[gallerySubString componentsSeparatedByString:@"_"] lastObject];
                    }
                    else {
                        if (self = [super init]) {
                            self.thumbnailQualityImageURLString = stickerURLString;
                            return self;
                        }
                    }
                    if (stickerID && galleryID) {
                        return [self initWithGalleryID:galleryID stickerID:stickerID];
                    }
                }
            }
        }
        return nil;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (void) updateFileLocationAndURLValues {
    if (self.galleryID && self.stickerID) {
        
        self.thumbnailFilePathURL = [NSString stringWithFormat:@"tm_%d_hdpi_%d.%@",[self.galleryID intValue],[self.stickerID intValue],CURRENT_FORMAT_EXTENSION];
        
        self.hdFilePathURL = [NSString stringWithFormat:@"tm_%d_hdpi_%d.%@",[self.galleryID intValue],[self.stickerID intValue],CURRENT_FORMAT_EXTENSION];
        
        if ([self.galleryID isEqualToString:self.stickerID]) {
            
            //create the high quality URL string
            NSString* relativeURLString = [@"images/stickers/StickersGalleries" stringByAppendingPathComponent:[[NSString stringWithFormat:@"tm_%d",[self.galleryID intValue]]  stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.%@",[self.stickerID intValue],CURRENT_FORMAT_EXTENSION]]];
            self.highQualityImageURLString = [STIKCER_BASE_URL stringByAppendingPathComponent:relativeURLString];
            
            relativeURLString = [@"images/stickers/StickersGalleries" stringByAppendingPathComponent:[[NSString stringWithFormat:@"tm_%d",[self.galleryID intValue]] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.%@",[self.stickerID intValue],CURRENT_FORMAT_EXTENSION]]];
            self.thumbnailQualityImageURLString = self.highQualityImageURLString;//[STIKCER_BASE_URL stringByAppendingPathComponent:relativeURLString];
        }
        else {
            NSString* relativeURLString = [@"images/stickers/StickersGalleries" stringByAppendingPathComponent:[[[NSString stringWithFormat:@"tm_%d",[self.galleryID intValue]] stringByAppendingPathComponent:@"thumbnail"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.%@",[self.stickerID intValue],CURRENT_FORMAT_EXTENSION]]];
            
            //create the high quality URL string
            relativeURLString = [@"images/stickers/StickersGalleries" stringByAppendingPathComponent:[[[NSString stringWithFormat:@"tm_%d",[self.galleryID intValue]] stringByAppendingPathComponent:@"hdpi"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.%@",[self.stickerID intValue],CURRENT_FORMAT_EXTENSION]]];
            self.highQualityImageURLString = [STIKCER_BASE_URL stringByAppendingPathComponent:relativeURLString];
            
            self.thumbnailQualityImageURLString = self.highQualityImageURLString;//[STIKCER_BASE_URL stringByAppendingPathComponent:relativeURLString];
        }
    }
}

@end

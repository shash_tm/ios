//
//  TMImageDownload.m
//  TrulyMadly
//
//  Created by Ankit on 07/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMImageDownload.h"
#import <objc/runtime.h>
#import "AFHTTPRequestOperation.h"
#import "TMFileManager.h"

@interface TMImageCache : NSCache <TMImageCache>

@end

@interface TMFileCache : NSObject

+(UIImage *)cachedImageAtPath:(NSString*)imagePath;
+(void)cacheImage:(UIImage *)image atPath:(NSString*)imagePath isPNG:(BOOL)isPNG;
+(void)removeCachedImageAtPath:(NSString*)imagePath;

@end

@interface TMImageDownload ()

@property (readwrite, nonatomic, strong, setter = af_setImageRequestOperation:) AFHTTPRequestOperation *af_imageRequestOperation;

@end

@implementation TMImageDownload
@dynamic imageResponseSerializer;

+ (NSOperationQueue *)af_sharedImageRequestOperationQueue {
    static NSOperationQueue *_af_sharedImageRequestOperationQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _af_sharedImageRequestOperationQueue = [[NSOperationQueue alloc] init];
        _af_sharedImageRequestOperationQueue.maxConcurrentOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount;
    });
    
    return _af_sharedImageRequestOperationQueue;
}

- (AFHTTPRequestOperation *)af_imageRequestOperation {
    return (AFHTTPRequestOperation *)objc_getAssociatedObject(self, @selector(af_imageRequestOperation));
}

- (void)af_setImageRequestOperation:(AFHTTPRequestOperation *)imageRequestOperation {
    objc_setAssociatedObject(self, @selector(af_imageRequestOperation), imageRequestOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+ (id <TMImageCache>)sharedImageCache {
    static TMImageCache *_af_defaultImageCache = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _af_defaultImageCache = [[TMImageCache alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * __unused notification) {
            [_af_defaultImageCache removeAllObjects];
        }];
    });

    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wgnu"
        return objc_getAssociatedObject(self, @selector(sharedImageCache)) ?: _af_defaultImageCache;
    #pragma clang diagnostic pop
    
}

+ (void)setSharedImageCache:(id <TMImageCache>)imageCache {
    objc_setAssociatedObject(self, @selector(sharedImageCache), imageCache, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark -

- (id <AFURLResponseSerialization>)imageResponseSerializer {
    static id <AFURLResponseSerialization> _af_defaultImageResponseSerializer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _af_defaultImageResponseSerializer = [AFImageResponseSerializer serializer];
    });
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
    return objc_getAssociatedObject(self, @selector(imageResponseSerializer)) ?: _af_defaultImageResponseSerializer;
#pragma clang diagnostic pop
}

- (void)setImageResponseSerializer:(id <AFURLResponseSerialization>)serializer {
    objc_setAssociatedObject(self, @selector(imageResponseSerializer), serializer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark -

-(void)getImageWithURL:(NSURL *)url {

    //NSLog(@"getImageWithURL:%@",url.absoluteString);
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self cancelImageRequestOperation];
    
    UIImage *cachedImage = nil;
    if(self.cachingPath) {
        cachedImage = [TMFileCache cachedImageAtPath:self.cachingPath];
    }
    else {
        cachedImage = [[[self class] sharedImageCache] cachedImageForRequest:urlRequest];
    }
    if (cachedImage) {
        //NSLog(@"__________IP: Cached Image Found _____________");
        self.af_imageRequestOperation = nil;
        [self.delegate didCompleteImageDownload:cachedImage fromUrl:url];
    } else {
        //NSLog(@"__________IP: Downloading Image ______________");
        __weak __typeof(self)weakSelf = self;
        self.af_imageRequestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        self.af_imageRequestOperation.responseSerializer = self.imageResponseSerializer;
        [self.af_imageRequestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            BOOL isPNG = false;
            if ([[urlRequest URL] isEqual:[strongSelf.af_imageRequestOperation.request URL]]) {
                NSHTTPURLResponse *response = strongSelf.af_imageRequestOperation.response;
                isPNG = ([response.MIMEType isEqualToString:@"image/png"]) ? true : false;
                UIImage *imageDownloaded = responseObject;
                [weakSelf.delegate didCompleteImageDownload:imageDownloaded fromUrl:[strongSelf.af_imageRequestOperation.request URL]];
                if (operation == strongSelf.af_imageRequestOperation){
                    strongSelf.af_imageRequestOperation = nil;
                }
            }
            
            if(weakSelf.cachingPath) {
                [TMFileCache cacheImage:responseObject atPath:weakSelf.cachingPath isPNG:isPNG];
            }
            else {
                [[[strongSelf class] sharedImageCache] cacheImage:responseObject forRequest:urlRequest];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if ([[urlRequest URL] isEqual:[strongSelf.af_imageRequestOperation.request URL]]) {
                if (operation == strongSelf.af_imageRequestOperation){
                    strongSelf.af_imageRequestOperation = nil;
                    [weakSelf.delegate didFailToDownloadImageFromUrl:url];
                }
            }
        }];
        
        [[[self class] af_sharedImageRequestOperationQueue] addOperation:self.af_imageRequestOperation];
    }
}

- (void)cancelImageRequestOperation {
    [self.af_imageRequestOperation cancel];
    self.af_imageRequestOperation = nil;
}

-(void)removeCachedImageDownloadedFromURL:(NSURL*)imageURL {
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:imageURL];
    [urlRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [[TMImageDownload sharedImageCache] removeCachedImageForRequest:urlRequest];
}
-(void)removeCachedImageDownloadedFromPath:(NSString*)imagePath {
    [TMFileCache removeCachedImageAtPath:imagePath];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

#pragma mark -

static inline NSString * AFImageCacheKeyFromURLRequest(NSURLRequest *request) {
    return [[request URL] absoluteString];
}

@implementation TMImageCache

-(UIImage *)cachedImageForRequest:(NSURLRequest *)request {
    switch ([request cachePolicy]) {
        case NSURLRequestReloadIgnoringCacheData:
        case NSURLRequestReloadIgnoringLocalAndRemoteCacheData:
            return nil;
        default:
            break;
    }
    //NSLog(@"Totoal Cache:%@",self);
    return [self objectForKey:AFImageCacheKeyFromURLRequest(request)];
}

-(void)cacheImage:(UIImage *)image
        forRequest:(NSURLRequest *)request
{
    if (image && request) {
        [self setObject:image forKey:AFImageCacheKeyFromURLRequest(request)];
    }
}

-(void)removeCachedImageForRequest:(NSURLRequest *)request
{
    if (request) {
        [self removeObjectForKey:AFImageCacheKeyFromURLRequest(request)];
    }
}

@end

@implementation TMFileCache

+(UIImage *)cachedImageAtPath:(NSString*)imagePath {
    NSData *imageData = [TMFileManager getFileAtPath1:imagePath];
    UIImage *cachedImage = nil;
    if(imageData) {
        cachedImage = [UIImage imageWithData:imageData];
    }
    return cachedImage;
}
+(void)cacheImage:(UIImage *)image
        atPath:(NSString*)imagePath
         isPNG:(BOOL)isPNG
{
    if (image && imagePath) {
        if(isPNG) {
            [TMFileManager writeFileWithData1:UIImagePNGRepresentation(image) toPath:imagePath];
        }else {
            [TMFileManager writeFileWithData1:UIImageJPEGRepresentation(image, 1) toPath:imagePath];
        }
    }
}
+(void)removeCachedImageAtPath:(NSString*)imagePath
{
    if (imagePath) {
        [TMFileManager removeFileAtPath:imagePath];
    }
}

@end

//
//  TMChatSeenContext.h
//  TrulyMadly
//
//  Created by Ankit on 03/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMChatSeenContext : NSObject

@property(nonatomic,strong)NSString *lastSeenMessageId;
@property(nonatomic,strong)NSString *lastSeenMessageTimestamp;
@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,assign)NSInteger userId;

-(instancetype)initWithChatSeenData:(NSDictionary*)data;
-(instancetype)initWithChatReadListenerData:(NSDictionary*)data;

@end

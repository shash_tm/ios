//
//  QuizQuestionDomainObject.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 17/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuizOptionDomainObject.h"

@interface QuizQuestionDomainObject : NSObject

@property (nonatomic, strong) NSString* quizID;
@property (nonatomic, strong) NSString* questionID;
@property (nonatomic, strong) NSString* questionText;
@property (nonatomic, strong) NSString* questionImageURL;
@property (nonatomic, strong) NSMutableArray* options;
@property (nonatomic, strong) NSString* userOptionID;
@property (nonatomic, strong) QuizOptionDomainObject* userAnswer;
@property (nonatomic, strong) QuizOptionDomainObject* matchAnswer;
@property (nonatomic, assign) BOOL isAnswerCommon;

- (instancetype) initWithCommonAnswersResponseDictionary:(NSDictionary*) commonAnswerDictionary;

@end

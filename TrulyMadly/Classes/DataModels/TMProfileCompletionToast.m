//
//  TMProfileCompletionToast.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCompletionToast.h"
//#import "TMDataStore.h"
#import "TMDataStore+TMAdAddtions.h"

@implementation TMProfileCompletionToast

-(void)setNudgeCount
{
    if ([TMDataStore containsObjectWithUserIDForKey:@"nudgeCount"]) {
        NSString* countStr = [TMDataStore retrieveObjectWithUserIDForKey:@"nudgeCount"];
        int count = [countStr intValue];
        count++;
        NSString* countString = [NSString stringWithFormat:@"%d",count];
        [TMDataStore setObjectWithUserIDForObject:countString key:@"nudgeCount"];
        [TMDataStore synchronize];
    }
    else {
        int count = 1;
        NSString* countString = [NSString stringWithFormat:@"%d",count];
        [TMDataStore setObjectWithUserIDForObject:countString key:@"nudgeCount"];
        [TMDataStore synchronize];
    }
}

-(BOOL)canInterstitialAdShown
{
    if ([TMDataStore containsObjectWithUserIDForKey:@"nudgeCount"]) {
        NSString *count = [TMDataStore retrieveObjectWithUserIDForKey:@"nudgeCount"];
        int nudgeCount = count.intValue;
        if(nudgeCount > 2)
        {
            return true;
        }
        else {
            return false;
        }
    }
    return true;
}

-(BOOL)willShowTrustToast {
    
    if([self isTrustShownOneDayOld]) {
        return true;
    }
//    if ([TMDataStore containsObjectForKey:@"trust_Cancel"]) {
//        NSString *count = [TMDataStore retrieveObjectforKey:@"trust_Cancel"];
//        int cancelCount = count.intValue;
//        if(cancelCount == 2) {
//            // check for week old date
//            if([self isTrustShownWeekOld]) {
//                return true;
//            }
//            return false;
//        }
//    }
    return false;
    
}

-(BOOL)willShowIncompleteProfileToast {
    
    if([self isProfileShownOneDayOld]) {
        return true;
    }
//    if ([TMDataStore containsObjectForKey:@"profile_Incomplete_Cancel"]) {
//        NSString *count = [TMDataStore retrieveObjectforKey:@"profile_Incomplete_Cancel"];
//        int cancelCount = count.intValue;
//        if(cancelCount == 2) {
//            // check for week old date
//            if([self isProfileShownOneDayOld]) {
//                return true;
//            }
//            return false;
//        }
//    }
    return false;
}

-(BOOL)isProfilePhotoApproved {
    if ([TMDataStore containsObjectForKey:@"is_profile_photo_approved"]) {
        return [TMDataStore retrieveBoolforKey:@"is_profile_photo_approved"];
    }
    return false;
}

-(BOOL)isTrustShownOneDayOld {
    if([TMDataStore containsObjectForKey:@"last_update_tstamp_trust"]) {
        NSDate *currDate = [NSDate date];
        NSDate *lastUpdatedDate = [TMDataStore retrieveObjectforKey:@"last_update_tstamp_trust"];
        NSTimeInterval secondsBetween = [currDate timeIntervalSinceDate:lastUpdatedDate];
        int noOfDays = secondsBetween/86400;
        if(noOfDays >= 1) {
            //[TMDataStore removeObjectforKey:@"trust_Cancel"];
            return true;
        }
        return false;
    }
    return true;
}

-(BOOL)isProfileShownOneDayOld {
    if([TMDataStore containsObjectForKey:@"last_update_tstamp_profile"]) {
        NSDate *currDate = [NSDate date];
        NSDate *lastUpdatedDate = [TMDataStore retrieveObjectforKey:@"last_update_tstamp_profile"];
        int secondsBetween = [currDate timeIntervalSinceDate:lastUpdatedDate];
        int noOfDays = secondsBetween/86400;
        if(noOfDays >= 1) {
            //[TMDataStore removeObjectforKey:@"profile_Incomplete_Cancel"];
            return true;
        }
        return false;
    }
    return true;
}

-(void)setDailyParameterForTrustBuilder {
    NSDate *currentDate = [NSDate date];
    [TMDataStore setObject:currentDate forKey:@"last_update_tstamp_trust"];
    [TMDataStore synchronize];
}

-(void)setDailyParameterForProfile {
    NSDate *currentDate = [NSDate date];
    [TMDataStore setObject:currentDate forKey:@"last_update_tstamp_profile"];
    [TMDataStore synchronize];
}

-(void)setTrustCancel {
     int count = 0;
     if ([TMDataStore containsObjectForKey:@"trust_Cancel"]) {
         NSString* countStr = [TMDataStore retrieveObjectforKey:@"trust_Cancel"];
         int count = [countStr intValue];
         if(count<2) {
             count++;
             NSString* countString = [NSString stringWithFormat:@"%d",count];
             [TMDataStore setObject:countString forKey:@"trust_Cancel"];
             [TMDataStore synchronize];
             if(count == 2) {
                 [self setDailyParameterForTrustBuilder];
             }
         }
     }else {
         count++;
         NSString* countString = [NSString stringWithFormat:@"%d",count];
         [TMDataStore setObject:countString forKey:@"trust_Cancel"];
         [TMDataStore synchronize];
     }
}

-(void)setEditProfileCancel {
    int count = 0;
    if ([TMDataStore containsObjectForKey:@"profile_Incomplete_Cancel"]) {
        NSString* countStr = [TMDataStore retrieveObjectforKey:@"profile_Incomplete_Cancel"];
        int count = [countStr intValue];
        if(count<2) {
            count++;
            NSString* countString = [NSString stringWithFormat:@"%d",count];
            [TMDataStore setObject:countString forKey:@"profile_Incomplete_Cancel"];
            [TMDataStore synchronize];
            if(count == 2) {
                [self setDailyParameterForProfile];
            }
        }
    }else {
        count++;
        NSString* countString = [NSString stringWithFormat:@"%d",count];
        [TMDataStore setObject:countString forKey:@"profile_Incomplete_Cancel"];
        [TMDataStore synchronize];
    }
}

-(BOOL)willShowPIDashboard {
    if ([TMDataStore containsObjectForKey:@"pi_activity_shown_count"]) {
        NSString *count = [TMDataStore retrieveObjectforKey:@"pi_activity_shown_count"];
        int cancelCount = count.intValue;
        if(cancelCount >= 2) {
            return false;
        }
        else if([TMDataStore containsObjectForKey:@"last_update_tstamp_pi_activity"]) {
            NSDate *currDate = [NSDate date];
            NSDate *lastUpdatedDate = [TMDataStore retrieveObjectforKey:@"last_update_tstamp_pi_activity"];
            int secondsBetween = [currDate timeIntervalSinceDate:lastUpdatedDate];
            int noOfDays = secondsBetween/86400;
            if(noOfDays >= 1) {
                return true;
            }
            return false;
        }
        else{
            return true;
        }
    }
    return true;
}

-(void)setPiActivityCount {
    int count = 0;
    NSDate *currentDate = [NSDate date];
    if ([TMDataStore containsObjectForKey:@"pi_activity_shown_count"]) {
        NSString* countStr = [TMDataStore retrieveObjectforKey:@"pi_activity_shown_count"];
        int count = [countStr intValue];
        if(count<2) {
            count++;
            NSString* countString = [NSString stringWithFormat:@"%d",count];
            [TMDataStore setObject:countString forKey:@"pi_activity_shown_count"];
            [TMDataStore setObject:currentDate forKey:@"last_update_tstamp_pi_activity"];
            [TMDataStore synchronize];
        }
    }else {
        count++;
        NSString* countString = [NSString stringWithFormat:@"%d",count];
        [TMDataStore setObject:countString forKey:@"pi_activity_shown_count"];
        [TMDataStore setObject:currentDate forKey:@"last_update_tstamp_pi_activity"];
        [TMDataStore synchronize];
    }
}
@end
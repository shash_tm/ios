//
//  TMUserSession.h
//  TrulyMadly
//
//  Created by Ankit on 26/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMUser.h"
 
@interface TMUserSession : NSObject

@property(nonatomic,strong)TMUser *user;
@property(nonatomic,assign)BOOL isUserProfileUpdated;
@property(nonatomic,assign)BOOL needToLoadMatchData;
@property(nonatomic,assign)BOOL isProfilePhotoUpdated;
@property(nonatomic,assign)BOOL isUserOnOneToOneMessaging;
@property(nonatomic,assign)BOOL socketEnabled;
@property(nonatomic,assign)BOOL socketDebugEnabled;
@property(nonatomic,assign)BOOL selectBuyActionOnSelectProfile;
@property(nonatomic,assign)BOOL needToLoadProfileOnSelectPayment;
@property(nonatomic,strong)NSNumber *currentMatchIdForOneToOneMessaging;
@property(nonatomic,strong)NSMutableDictionary *registerBasicsDataDictionary;

+(instancetype)sharedInstance;
-(void)setUserData:(NSDictionary*)userDataDictionary;
-(void)setUserLoginStatus:(BOOL)loginStatus;
-(BOOL)isLogin;
-(TMActiveChatConnection)chatConnection;
-(BOOL)navigateUserToMatches;
-(void)setUserNavigationStatusAsMovedToMatches;
-(NSString*)cookie;
-(NSString*)getCookieParam;
-(void)setSessionCookie;
-(BOOL)fromRegistration;
-(void)clearUserSessionData;
-(void)setRegisterCookieData:(NSMutableDictionary*)regsiteredCookieData;
-(void)deleteRegisterCookieData;
-(BOOL)isMatchActionViewAnimationShown;
-(void)registerMatchActionViewAnimation;
-(BOOL)isMutualLikeAnimationShown;
-(void)registerMutualLikeAnimation;
-(void)sendDeviceTokenForLoginEvent;
-(NSString*)currentConnectedNetwork;
-(void)setNetworkType:(TMNetworkType)connectionType;
-(void)setSocketEnabled:(BOOL)enabled withDebugFlagEnabled:(BOOL)debugFlagEnabled;
-(void)sendUserAttributeToMoEngage;

-(BOOL)isMeetUpTutorialShown;
-(void)registerMeetUpTutorialViewShown;
-(BOOL)isMeetUpIconShown;
-(void)registerMeetUpIconShown:(BOOL)flag;

-(BOOL)isVideoTutorialShown;
-(void)registerVideoTutorialShown;

-(BOOL)isFirstSessionFlagSet;
-(void)setSoftPopShownTime;
-(BOOL)isSoftUpdatePopShownMoreThanOneDay;

-(void)setAppViralityActiveEnabled:(BOOL)status;
-(BOOL)isAppViralityEnabled;

-(BOOL)canPhotoVisibilityShown;
-(void)registerPhotoVisibilityAlertForLike;
-(void)deregisterPhotoVisibilityAlertForLike;
-(BOOL)canPhotoVisibilityShownForLike;

-(BOOL)canShakePhotoIconOnChatWithMatchId:(NSString *)matchId;

-(BOOL)isReferBlooperShown;
-(void)registerReferBlooperShown;

-(void)updateAvailableSparkCount:(NSString *)sparksAvailable;
-(NSInteger)getAvailableSparkCount;
-(BOOL)isSparkEnabled;
-(void)enableSpark:(BOOL)status;
-(BOOL)canShowSlideAnimationOnSparkChat;
-(void)updateSlideAnimationOnSparkChat;

-(void)updateSceneData:(NSDictionary*)sceneData;
-(NSString*)getSceneCategoryIds;

-(BOOL)isSparkTutorialShown;
-(BOOL)canShowSparkConversationTutorial;
-(void)updateSparkConversationTutorialCounter;
-(void)updateSparkConversationTutorialStatusAsShown;
-(void)updateWhyNotSparkPopupFlags:(NSDictionary*)dictionary;
-(BOOL)isWhyNotSparkPopupEnabled;
-(NSInteger)whyNotSparkPopupCount;

-(void)setSelectStatusData:(NSDictionary*)selectData;
-(void)setMySelectStatus:(NSDictionary*)mySelectData;
-(void)setSelectQuizId:(NSString*)quizId;
-(void)setSelectEnabled:(BOOL)isSelectEnabled;
-(BOOL)isSelectEnabled;
-(BOOL)isSelectNudgeEnabled;
-(NSString*)selectNudgeContent;
-(NSInteger)selectNudgeTimeInterval;
-(BOOL)isSparkActionAllowedOnSelectProfile;
-(BOOL)isLikeActionAllowedOnSelectProfile;
-(NSString*)getActiveSelectQuizId;
-(void)setSelectQuizAsStarted;
-(void)setSelectQuizAsFinished;
-(BOOL)isSelectQuizPending;
-(void)setSelectQuizProgressState:(TMSelectQuizProgressState)progressState;
-(void)setSelectQuizPlayedIndex:(NSInteger)quizPlayedIndex;
-(NSInteger)getSelectQuizPlayedIndex;
-(TMSelectQuizProgressState)getSelectQuizProgressState;

-(void)updateLikeActionCounter:(NSInteger)likeCounter;
-(void)updateHideActionCounter:(NSInteger)hideCounter;
-(void)updateSparkActionCounter:(NSInteger)sparkCounter;
-(NSInteger)getLikeActionCounter;
-(NSInteger)getHideActionCounter;
-(NSInteger)getSparkActionCounter;
-(void)cacheSelectMatchId:(NSString*)matchId matchHash:(NSString*)hash;
-(void)resetCachedSelectMatchRefreshData;
-(NSString*)getCachedSelectedMatchIdForRefresh;
-(NSString*)getCachedSelectedHashForRefresh;
-(int)getPhoneCountryCode;
-(BOOL)canShowNRINudge;
-(void)setNRINudgeShown;
- (void)setLikeLimitForUser:(int)likeLimit;
- (int)getLikeLimitForUser;
- (int)getLimitedLikeCounter;

@end


//
//  TMSparkPackageInfo.h
//  TrulyMadly
//
//  Created by Ankit on 26/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"


@interface TMSparkPackageInfo : NSObject

@property(nonatomic,strong,readonly)NSString *packageTitle;
@property(nonatomic,strong,readonly)NSString *packageDescription;
@property(nonatomic,strong,readonly)NSString *packageTag;
@property(nonatomic,strong,readonly)NSString *packagePrice;
@property(nonatomic,strong,readonly)NSString *currencyType;
@property(nonatomic,assign,readonly)TMBuyPackTagType tagType;

- (instancetype)initWithDictionary:(NSDictionary*)packageDictionary;

@end

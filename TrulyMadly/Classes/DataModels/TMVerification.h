//
//  TMVerification.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMVerification : NSObject

@property(nonatomic,assign)BOOL state;
@property(nonatomic,strong)NSString *text;
@property(nonatomic,strong)NSString *imageName;
@property(nonatomic,strong)NSString *score;
@end

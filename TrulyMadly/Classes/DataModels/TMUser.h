//
//  TMUser.h
//  TrulyMadly
//
//  Created by Ankit on 26/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"


@interface TMUser : NSObject

@property(nonatomic,strong)NSString *userId;
@property(nonatomic,strong)NSString *profilePicURL;
@property(nonatomic,strong)NSNumber *mutualLikeCount;
@property(nonatomic,strong)NSNumber *hasuserUnlikedProfile;

@property(nonatomic,assign,readonly)BOOL isSelectUser;
@property(nonatomic,assign)BOOL isInterPersonalFilled;
@property(nonatomic,assign)BOOL isValuesFilled;
@property(nonatomic,assign)BOOL isFavouriteFilled;
@property(nonatomic,assign)BOOL isFemaleProfileRejected;
@property(nonatomic,assign)BOOL hasFemaleProfileBeenRejectedOnce;

-(int)age;
-(NSString*)fName;
-(NSString*)gender;
-(NSString*)city;
-(NSString*)state;
-(NSString*)status;
-(NSString*)fbDesignation;
-(int)country;
-(BOOL)isUserCountryIndia;
-(void)setUserDefaults;
-(void)setUserDataFromDictioanry:(NSDictionary*)userDataDictionary;
-(TMUserStatus)userStatus;
-(BOOL)isNonAuthenticUserLikeActionRegistered;
-(void)registerNonAuthenticUserLikeAction;
-(BOOL)isUserUnlikeActionRegistered;
-(void)registerUserUnlikeAction;
-(BOOL)isInterPersonalQuizFilledByUser;
-(BOOL)isValuesQuizFilledByUser;
-(void)registerNonAuthenticUserFirstInteractionWithMatch;
-(BOOL)isNonAuthenticUserFirstInteractionedWithMatch;
-(NSString*)userGenderString;
-(BOOL)isUserFemale;
-(NSString*)getIntentionSurveyUserIDKey;
-(NSString*)getUserAttributeMoEngageKey;
-(BOOL)isUserUnlikeActionRegisteredFromEvent;
-(void)registerUserUnlikeActionFromEvent;

-(void)setSelectStatus:(NSDictionary*)selectData;
-(BOOL)isSelectUser;
-(NSInteger)selectExpiryDaysLeft;
-(NSString*)selectExpiryDaysLeftText;
-(BOOL)isQuizPlayed;
-(NSString*)selectSideMenuCta;
-(NSString*)selectProfileCta;
-(NSString*)selectProfileCtaText;

@end

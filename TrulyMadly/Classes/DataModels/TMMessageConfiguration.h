//
//  TMMessageConfiguration.h
//  TrulyMadly
//
//  Created by Ankit on 27/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@class TMChatUser;

@interface TMMessageConfiguration : NSObject

@property(nonatomic,strong)TMChatUser* sparkUser;
@property(nonatomic,strong)NSString* sparkHash;

@property(nonatomic,strong)NSString* messageConversationFullLink;
@property(nonatomic,strong)NSString* fName;
@property(nonatomic,strong)NSString* profileImageURLString;
@property(nonatomic,strong)NSString* profileURLString;
@property(nonatomic,strong)NSString* clearChatTs;
@property(nonatomic,strong)NSString* sparkRemainingTime;
@property(nonatomic,assign)NSInteger sparkExpiryTimeInSecs;

@property(nonatomic,assign)NSInteger matchUserId;
@property(nonatomic,assign,readonly)NSInteger hostUserId;

@property(nonatomic,assign)BOOL isConversationHasUnreadMessage;
@property(nonatomic,assign)BOOL isMsTm;
@property(nonatomic,assign)BOOL isSparkConversation;
@property(nonatomic,assign)TMDealState dealState;

@end

//
//  TMProfileInterest.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TMProfileInterest : NSObject

@property(nonatomic,strong)NSString *tag;
@property(nonatomic,assign)BOOL commonLike;

-(instancetype)initWithTag:(NSString*)tag isCommonLike:(BOOL)commonLike;
-(CGSize)tagSizeForMaximumWidth:(CGFloat)width;
-(UIColor*)tagColor;
-(UIFont*)fontForText;

@end

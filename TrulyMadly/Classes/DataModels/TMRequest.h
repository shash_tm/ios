//
//  TMRequest.h
//  TrulyMadly
//
//  Created by Ankit on 05/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMRequest : NSObject

@property(nonatomic,strong)NSString *urlString;

-(instancetype)initWithUrlString:(NSString*)urlString;
-(NSDictionary*)httpHeaders;
-(void)setGetRequestParams:(NSDictionary*)getParamsDict;
-(NSDictionary*)httpGetRequestParameters;
-(void)setPostRequestParameters:(NSDictionary*)postParamsDict;
-(NSDictionary*)httpPostRequestParameters;

@end


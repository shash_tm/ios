//
//  TMEndorsement.swift
//  TrulyMadly
//
//  Created by Ankit on 17/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMEndorsement: NSObject {
   
    var fName:NSString?
    var imageUrl:NSString?
    var userId:NSString?
    
    init(endorsementDataDict:Dictionary<String,String>) {
        super.init()
        self.fName = endorsementDataDict["fname"] as NSString?
        self.imageUrl = endorsementDataDict["pic"] as NSString?
        self.userId = endorsementDataDict["user_id"] as NSString?
    }
}

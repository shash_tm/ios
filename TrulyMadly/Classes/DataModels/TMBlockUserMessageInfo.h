//
//  TMBlockUserMessageInfo.h
//  TrulyMadly
//
//  Created by Ankit on 16/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMBlockUserMessageInfo : NSObject

@property(nonatomic,strong)NSString *blockReason;
@property(nonatomic,assign)NSInteger blockType;

@end

//
//  TMUserSession.m
//  TrulyMadly
//
//  Created by Ankit on 26/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMUserSession.h"
#import "TMDataStore.h"
#import "TMEnums.h"
#import "TMCookie.h"
#import "TMNotification.h"
#import "AppDelegate.h"
#import "TMSocketSession.h"
#import "TMAppViralityController.h"


#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


@interface TMUserSession ()

@property(nonatomic,assign)TMActiveChatConnection chatConnectionType;
@property(nonatomic,assign)TMNetworkType currentNetworkType;

@end

@implementation TMUserSession

-(instancetype)init {
    self = [super init];
    if(self) {
        self.user = [[TMUser alloc] init];
        self.needToLoadMatchData = FALSE;
        self.registerBasicsDataDictionary = [[NSMutableDictionary alloc] init];
        [self setDefaultChatConnectionType];
    }
    return self;
}

+(instancetype)sharedInstance {
    static TMUserSession *_sharedSession = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate,^{
        _sharedSession = [[self alloc] init];
    });
    return _sharedSession;
}

-(void)setIsUserProfileUpdated:(BOOL)isUserProfileUpdated {
    _needToLoadMatchData = isUserProfileUpdated;
    [TMDataStore setBool:isUserProfileUpdated forKey:@"userprofileupdated"];
}

-(BOOL)isUserProfileUpdated {
    BOOL status = FALSE;
    status = [TMDataStore retrieveBoolforKey:@"userprofileupdated"];
    return status;
}

-(void)setUserData:(NSDictionary*)userDataDictionary {
    [self.user setUserDataFromDictioanry:userDataDictionary];
    [self sendUserdetailsToAppVirality];
}

-(void)setUserLoginStatus:(BOOL)loginStatus {
    [TMDataStore setObject:[NSNumber numberWithBool:loginStatus] forKey:@"isLoggedIn"];
    [TMDataStore synchronize];
}
-(BOOL)isLogin {
    NSNumber *loginStatus = [TMDataStore retrieveObjectforKey:@"isLoggedIn"];
    if(loginStatus != nil){
        return true;
    }else{
        return false;
    }
}
-(void)setDefaultChatConnectionType {
    NSNumber *socketConnection = [TMDataStore retrieveObjectforKey:@"socketconnection"];
    if(!socketConnection) {
        BOOL defaultStatus = ([self needToDisableSocket]) ? FALSE : TRUE;
        [TMDataStore setObject:[NSNumber numberWithBool:defaultStatus] forKey:@"socketconnection"];
        self.chatConnectionType = CHATCONNECTION_SOCKET;
    }
    else {
        if([self needToDisableSocket]) {
            [TMDataStore setObject:[NSNumber numberWithBool:FALSE] forKey:@"socketconnection"];
        }
        BOOL socketConnectionEnabled = [[TMDataStore retrieveObjectforKey:@"socketconnection"] boolValue];
        if(socketConnectionEnabled) {
            self.chatConnectionType = CHATCONNECTION_SOCKET;
        }
        else {
            self.chatConnectionType = CHATCONNECTION_POLLING;
        }
    }
}
-(void)setSocketEnabled:(BOOL)enabled withDebugFlagEnabled:(BOOL)debugFlagEnabled {
    /////
    self.socketDebugEnabled = true;
    /////
    BOOL socketEnabled;
    if(enabled) {
        socketEnabled = TRUE;
        self.chatConnectionType = CHATCONNECTION_SOCKET;
    }
    else {
        socketEnabled = FALSE;
        self.chatConnectionType = CHATCONNECTION_POLLING;
    }
    
    [TMDataStore setObject:[NSNumber numberWithBool:socketEnabled] forKey:@"socketconnection"];
}

-(TMActiveChatConnection)chatConnection {
    return self.chatConnectionType;
}
-(BOOL)navigateUserToMatches {
    NSNumber *userStatus = [TMDataStore retrieveObjectforKey:@"isUserNavigatedToMatches"];
    if((userStatus != nil) && (userStatus.boolValue)) {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)needToDisableSocket {
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    if([appVersion isEqualToString:@"240"] && SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        return TRUE;
    }
    return FALSE;
}
-(void)setUserNavigationStatusAsMovedToMatches {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"isUserNavigatedToMatches"];
}

-(NSString*)cookie {
    NSString *cookieStr = [TMCookie cookieWithKeyValueFormat];
    return cookieStr;
}

-(void)setSessionCookie {
    [TMCookie cacheTMSessionCookie];
}

-(NSString*)getCookieParam {
    NSString *tmpData = @"";

    for (NSString *key in self.registerBasicsDataDictionary) {
        tmpData = [NSString stringWithFormat:@"%@%@",tmpData,key];
        tmpData = [NSString stringWithFormat:@"%@%@",tmpData,@"="];
        tmpData = [NSString stringWithFormat:@"%@%@",tmpData,[self.registerBasicsDataDictionary objectForKey:key]];
        tmpData = [NSString stringWithFormat:@"%@%@",tmpData,@";"];
    }
    
    NSString *key = [TMCookie cookieKey];
    NSString *sessionId = [TMDataStore retrieveObjectforKey:key];
    if(sessionId) {
        tmpData = [NSString stringWithFormat:@"%@%@=%@",tmpData,key,sessionId];
    }
    
    tmpData = [tmpData stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return tmpData;
}

-(BOOL)fromRegistration {
    NSNumber *fromRegisteration = [TMDataStore retrieveObjectforKey:@"fromRegisteration"];
    if(fromRegisteration != nil) {
        return true;
    }
    else {
        return false;
    }
}

//need to reset
-(void)clearUserSessionData {
    //reset all user defaults
    [self resetApplicationUserDefaults];
    //clear current session cookie
    [self clearHttpSessionCookie];
    //
    [self.user setUserDefaults];
    [TMSocketSession clearAllCachedData];
}

-(void)resetApplicationUserDefaults {
    //reset all applicatio user defaults
    //this will be done on logout
    [TMDataStore removeObjectforKey:@"isLoggedIn"];
    [TMDataStore removeObjectforKey:@"PHPSESSID"];
    [TMDataStore removeObjectforKey:@"isUserNavigatedToMatches"];
    [TMDataStore removeObjectforKey:@"socketconnection"];
    [TMDataStore synchronize];
}

-(void)clearHttpSessionCookie {
    NSHTTPCookieStorage *cookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage;
    for (NSHTTPCookie *cookie in cookieStorage.cookies) {
        [cookieStorage deleteCookie:cookie];
    }
}

-(void)setRegisterCookieData:(NSMutableDictionary *)regsiteredCookieData {
    self.registerBasicsDataDictionary = regsiteredCookieData;
}

-(void)deleteRegisterCookieData {
    self.registerBasicsDataDictionary = nil;
}

-(BOOL)isMeetUpTutorialShown {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"meetup_tutorial_shown"] boolValue];
    return status;
}
-(BOOL)isMeetUpIconShown {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"meetup_icon_shown"] boolValue];
    return status;
}
-(void)registerMeetUpIconShown:(BOOL)flag {
    [TMDataStore setObject:[NSNumber numberWithBool:flag] forKey:@"meetup_icon_shown"];
}
-(void)registerMeetUpTutorialViewShown {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"meetup_tutorial_shown"];
}

-(void)registerVideoTutorialShown {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"video_tutorial_shown"];
}
-(BOOL)isVideoTutorialShown {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"video_tutorial_shown"] boolValue];
    return status;
}
-(BOOL)isFirstSessionFlagSet {
    BOOL status = [TMDataStore retrieveBoolforKey:@"auto_expand_trust_shown"] && [[TMDataStore retrieveObjectforKey:@"matchaction_animation"] boolValue];
    return status;
}
-(BOOL)isAppUpdatedFlagSet {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"app_updated"] boolValue];
    return status;
}
-(BOOL)isMatchActionViewAnimationShown {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"matchaction_animation"] boolValue];
    return status;
}

-(void)registerMatchActionViewAnimation {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"matchaction_animation"];
}

-(BOOL)isMutualLikeAnimationShown {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"mutuallike_animation"] boolValue];
    return status;
}

-(void)registerMutualLikeAnimation {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"mutuallike_animation"];
}

///////////////////
-(void)sendDeviceTokenForLoginEvent {
    BOOL isAppInstallSent = [[TMDataStore retrieveObjectforKey:@"appInstall"] boolValue];
    if(!isAppInstallSent) {
        [self sendTokenToServer:@"install"];
    }
    
    BOOL isTokenSent = [[TMDataStore retrieveObjectforKey:@"loginbug"] boolValue];
    if(!isTokenSent) {
        [self sendTokenToServer:@"login"];
        [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"loginbug"];
    }
}

-(void)sendUserAttributeToMoEngage {
   // BOOL isUserAttributeSent = [[TMDataStore retrieveObjectforKey:[self.user getUserAttributeMoEngageKey]] boolValue];
   // if(!isUserAttributeSent) {
        //AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        //[delegate setUserAttributeForMoEngage];
      //  [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:[self.user getUserAttributeMoEngageKey]];
    //}
}

-(void)sendTokenToServer:(NSString*)status {
    NSString *token = [TMDataStore retrieveObjectforKey:@"deviceToken"];
    if(token != nil) {
        [TMNotification sendToken:status token:token];
    }
}

-(void)setNetworkType:(TMNetworkType)connectionType {
    self.currentNetworkType = connectionType;
}
-(NSString*)currentConnectedNetwork {
    NSString *retVal = nil;
    if(self.currentNetworkType == TMNetworkTypeWiFi) {
        retVal = @"Wifi";
    }
    else if(self.currentNetworkType == TMNetworkType2G) {
        retVal = @"2G";
    }
    else if(self.currentNetworkType == TMNetworkType3G) {
        retVal = @"3G";
    }
    else if(self.currentNetworkType == TMNetworkTypeLTE) {
        retVal = @"4G";
    }
    else {
        retVal = @"Unknown";
    }
    return retVal;
}

-(void)setSoftPopShownTime {
    [TMDataStore setObject:[NSDate date] forKey:@"last_time_soft_popup_shown"];
}
-(BOOL)isSoftUpdatePopShownMoreThanOneDay {
    if ([TMDataStore containsObjectForKey:@"last_time_soft_popup_shown"]) {
        NSDate *lastTimeValue = [TMDataStore retrieveObjectforKey:@"last_time_soft_popup_shown"];
        if (lastTimeValue >= 0 && fabs([[NSDate date] timeIntervalSinceDate:lastTimeValue]) > 86400.0f) {
            [self setSoftPopShownTime];
            return true;
        }else {
            return false;
        }
    }else {
        [self setSoftPopShownTime];
        return true;
    }
    return false;
}

#pragma mark - AppViraltity
#pragma mark -

-(void)sendUserdetailsToAppVirality {
    [TMAppViralityController updateUserData];
}
-(void)setAppViralityActiveEnabled:(BOOL)status {
    [TMDataStore setBool:status forKey:@"com.appvirality.status"];
}
-(BOOL)isAppViralityEnabled {
    BOOL status = [TMDataStore retrieveBoolforKey:@"com.appvirality.status"];
    return status;
}

#pragma mark - ProfileVisibilty
-(BOOL)canPhotoVisibilityShown {
    if ([TMDataStore containsObjectForKey:[self getKeyName:@"photo_visibility_shown_count"]]) {
        NSString *count = [TMDataStore retrieveObjectforKey:[self getKeyName:@"photo_visibility_shown_count"]];
        int cancelCount = count.intValue;
        if(cancelCount >= 3) {
            return false;
        }
        else if ([TMDataStore containsObjectForKey:[self getKeyName:@"last_time_photo_visibility_shown"]]) {
            NSDate *lastTimeValue = [TMDataStore retrieveObjectforKey:[self getKeyName:@"last_time_photo_visibility_shown"]];
            if (lastTimeValue >= 0 && fabs([[NSDate date] timeIntervalSinceDate:lastTimeValue]) > 86400.0f) {
                cancelCount = cancelCount+1;
                NSString *shownCount = [NSString stringWithFormat:@"%d",cancelCount];
                [TMDataStore setObject:[NSDate date] forKey:[self getKeyName:@"last_time_photo_visibility_shown"]];
                [TMDataStore setObject:shownCount forKey:[self getKeyName:@"photo_visibility_shown_count"]];
                return true;
            }else {
                return false;
            }
        }
        else{
            cancelCount = cancelCount+1;
            NSString *shownCount = [NSString stringWithFormat:@"%d",cancelCount];
            [TMDataStore setObject:shownCount forKey:[self getKeyName:@"photo_visibility_shown_count"]];
            [TMDataStore setObject:[NSDate date] forKey:[self getKeyName:@"last_time_photo_visibility_shown"]];
            return true;
        }
    }else {
         [TMDataStore setObject:@"1" forKey:[self getKeyName:@"photo_visibility_shown_count"]];
         [TMDataStore setObject:[NSDate date] forKey:[self getKeyName:@"last_time_photo_visibility_shown"]];
        return true;
    }
    return false;
}

-(void)registerPhotoVisibilityAlertForLike {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:[self getKeyName:@"photo_visibility_shown_like"]];
}
-(void)deregisterPhotoVisibilityAlertForLike {
    [TMDataStore removeObjectforKey:[self getKeyName:@"photo_visibility_shown_like"]];
}
-(BOOL)canPhotoVisibilityShownForLike {
    BOOL status = [[TMDataStore retrieveObjectforKey:[self getKeyName:@"photo_visibility_shown_like"]] boolValue];
    return status;
}
-(NSString *)getKeyName:(NSString *)name {
    NSString *keyName = [NSString stringWithFormat:@"%@_%@",name,self.user.userId];
    return keyName;
}

-(BOOL)canShakePhotoIconOnChatWithMatchId:(NSString *)matchId {
    if([self.user isUserFemale] && self.user.isFemaleProfileRejected){
        //check if photo icon already shake with the given match id
        if([self checkPhotoIconShakeWithMatchId:matchId]){
            // check total count of shaking the icon
            if([self checkPhotoIconShakeCount]) {
                return true;
            }
        }
    }
    return false;
}

-(BOOL)checkPhotoIconShakeCount {
    if ([TMDataStore containsObjectForKey:[self getKeyName:@"photo_icon_shake_count"]]) {
        NSString *count = [TMDataStore retrieveObjectforKey:[self getKeyName:@"photo_icon_shake_count"]];
        int cancelCount = count.intValue;
        if(cancelCount >= 3) {
            return false;
        }else {
            cancelCount = cancelCount+1;
            NSString *shownCount = [NSString stringWithFormat:@"%d",cancelCount];
            [TMDataStore setObject:shownCount forKey:[self getKeyName:@"photo_icon_shake_count"]];
            return true;
        }
    }else {
        [TMDataStore setObject:@"1" forKey:[self getKeyName:@"photo_icon_shake_count"]];
        return true;
    }
    return false;
}

-(BOOL)checkPhotoIconShakeWithMatchId:(NSString *)matchId {
    NSString *key = [NSString stringWithFormat:@"%@_%@",[self getKeyName:@"photo_icon_shake_shown"],matchId];
    if ([TMDataStore containsObjectForKey:key]) {
        return false;
    }else {
        [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:key];
        return true;
    }
    return false;
}
-(BOOL)isReferBlooperShown {
    if ([TMDataStore containsObjectForKey:[self getKeyName:@"refer_blooper_shown"]]) {
        return true;
    }
    return false;
}
-(void)registerReferBlooperShown {
    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:[self getKeyName:@"refer_blooper_shown"]];
}
-(void)updateSceneData:(NSDictionary*)sceneData {
    NSString *sceneCategoryIds = sceneData[@"scenes_ab_category"];
    [TMDataStore setObject:sceneCategoryIds forKey:@"com.scene.cateoryids"];
}
-(NSString*)getSceneCategoryIds {
    NSString *sceneCategoryIds = [TMDataStore retrieveObjectforKey:@"com.scene.cateoryids"];
    return sceneCategoryIds;
}
-(void)updateAvailableSparkCount:(NSString *)sparksAvailable {
    [TMDataStore setObject:sparksAvailable forKey:[self getKeyName:@"spark_counter"]];
}
-(void)enableSpark:(BOOL)status {
    [TMDataStore setBool:status forKey:@"com.spark.status"];
}
-(BOOL)isSparkEnabled {
    BOOL status = [TMDataStore retrieveBoolforKey:@"com.spark.status"];
    return status;
}
-(BOOL)isSparkTutorialShown {
    BOOL status = [[TMDataStore retrieveObjectforKey:@"com.match.sparktutorial"] integerValue];
    [TMDataStore setBool:TRUE forKey:@"com.match.sparktutorial"];
    return status;
}
-(BOOL)canShowSlideAnimationOnSparkChat {
    NSInteger animationCounter = [[TMDataStore retrieveObjectforKey:@"com.chat.sparkanimationcounter"] integerValue];
    if(animationCounter<2){
        return TRUE;
    }
    return FALSE;
}
-(void)updateSlideAnimationOnSparkChat {
    NSString *key =  @"com.chat.sparkanimationcounter";
    NSInteger animationCounter = [[TMDataStore retrieveObjectforKey:key] integerValue];
    [TMDataStore setObject:@(++animationCounter) forKey:key];
}
-(NSInteger)getAvailableSparkCount {
    if([TMDataStore containsObjectForKey:[self getKeyName:@"spark_counter"]]) {
        NSString *str = [TMDataStore retrieveObjectforKey:[self getKeyName:@"spark_counter"]];
        NSInteger counter = [str integerValue];
        return counter;
    }
    return 0;
}
-(BOOL)canShowSparkConversationTutorial {
    NSInteger count = [[TMDataStore retrieveObjectforKey:@"com.convlist.sparktutorial"] integerValue];
    if(count < 2) {
        return TRUE;
    }
    return FALSE;;
}
-(void)updateSparkConversationTutorialCounter {
    NSInteger count = [[TMDataStore retrieveObjectforKey:@"com.convlist.sparktutorial"] integerValue];
    [TMDataStore setObject:@(++count) forKey:@"com.convlist.sparktutorial"];
}
-(void)updateSparkConversationTutorialStatusAsShown {
    //as tutorial need to be shown 2 times
    [TMDataStore setObject:@(2) forKey:@"com.convlist.sparktutorial"];
}
-(void)updateWhyNotSparkPopupFlags:(NSDictionary*)dictionary {
    NSString *count = dictionary[@"matches_count"];
    BOOL enabled = [dictionary[@"enabled"] boolValue];
    [TMDataStore setBool:enabled forKey:@"com.spark.whynotsparkpopup.enabled"];
    [TMDataStore setObject:count forKey:@"com.spark.whynotsparkpopup.count"];
}
-(BOOL)isWhyNotSparkPopupEnabled {
    BOOL enabled = [TMDataStore retrieveBoolforKey:@"com.spark.whynotsparkpopup.enabled"];
    return enabled;
}
-(NSInteger)whyNotSparkPopupCount {
    NSString* countStr = [TMDataStore retrieveObjectforKey:@"com.spark.whynotsparkpopup.count"];
    return [countStr integerValue];
}
-(void)setSelectEnabled:(BOOL)isSelectEnabled {
    [TMDataStore setBool:isSelectEnabled forKey:@"com.select.enabled"];
}
-(void)setSelectQuizId:(NSString*)quizId {
    [TMDataStore setObject:quizId forKey:@"com.select.quizid"];
}
-(void)setSelectStatusData:(NSDictionary*)selectData {
    NSDictionary *mySelect = selectData[@"my_select"];
    NSDictionary *actionNudge = selectData[@"action_nudge"];
    NSDictionary *selectNudge = selectData[@"select_nudge"];
    [self setMySelectStatus:mySelect];
    [self setSelectActionNudgeStatus:actionNudge];
    [self setSelectNudgeStatus:selectNudge];
}
-(void)setSelectActionNudgeStatus:(NSDictionary*)actionNudge {
    BOOL likeAllowed = [actionNudge[@"allow_like"] boolValue];
    BOOL sparkAllowed = [actionNudge[@"allow_spark"] boolValue];
    
    [TMDataStore setBool:likeAllowed forKey:@"com.select.likeallowed"];
    [TMDataStore setBool:sparkAllowed forKey:@"com.select.sparkallowed"];
}
-(void)setSelectNudgeStatus:(NSDictionary*)selectNudgeData {
    NSString *content = selectNudgeData[@"content"];
    BOOL enabled = [selectNudgeData[@"enabled"] boolValue];
    NSNumber *frquesncy = selectNudgeData[@"frequency"];
    [TMDataStore setObject:content forKey:@"com.select.selectnudgecontent"];
    [TMDataStore setBool:enabled forKey:@"com.select.selectnudgeenabled"];
    [TMDataStore setObject:frquesncy forKey:@"com.select.selectnudgefrequency"];
}
-(void)setMySelectStatus:(NSDictionary*)mySelectData {
    [self.user setSelectStatus:mySelectData];
}

-(BOOL)isSelectEnabled {
    BOOL status = [TMDataStore retrieveBoolforKey:@"com.select.enabled"];
    return status;
}
-(BOOL)isSelectNudgeEnabled {
    BOOL status = [TMDataStore retrieveBoolforKey:@"com.select.selectnudgeenabled"];
    return status;
}
-(NSString*)selectNudgeContent {
    NSString *content = [TMDataStore retrieveObjectforKey:@"com.select.selectnudgecontent"];
    return content;
}
-(NSInteger)selectNudgeTimeInterval {
    NSNumber *timeinterval = [TMDataStore retrieveObjectforKey:@"com.select.selectnudgefrequency"];
    return timeinterval.integerValue;
}
-(BOOL)isLikeActionAllowedOnSelectProfile {
    return [TMDataStore retrieveBoolforKey:@"com.select.likeallowed"];
}
-(BOOL)isSparkActionAllowedOnSelectProfile {
    return [TMDataStore retrieveBoolforKey:@"com.select.sparkallowed"];
}
-(NSString*)getActiveSelectQuizId{
    NSString *quizId = [TMDataStore retrieveObjectforKey:@"com.select.quizid"];
    return quizId;
}
-(void)setSelectQuizAsStarted {
    [TMDataStore setBool:TRUE forKey:@"com.select.quizpending"];
    [TMDataStore setObject:@(OnReady) forKey:@"com.select.progressstate"];
}
-(void)setSelectQuizAsFinished {
    [TMDataStore setBool:FALSE forKey:@"com.select.quizpending"];
    [TMDataStore removeObjectforKey:@"com.select.progressstate"];
    [TMDataStore synchronize];
}
-(BOOL)isSelectQuizPending; {
    BOOL status = [TMDataStore retrieveBoolforKey:@"com.select.quizpending"];
    return status;
}
-(void)setSelectQuizProgressState:(TMSelectQuizProgressState)progressState {
    [TMDataStore setObject:@(progressState) forKey:@"com.select.progressstate"];
}
-(void)setSelectQuizPlayedIndex:(NSInteger)quizPlayedIndex {
    [TMDataStore setObject:@(quizPlayedIndex) forKey:@"com.select.quizplayedindex"];
}
-(NSInteger)getSelectQuizPlayedIndex {
    NSNumber *index = [TMDataStore retrieveObjectforKey:@"com.select.quizplayedindex"];
    return index.integerValue;
}
-(TMSelectQuizProgressState)getSelectQuizProgressState {
    NSNumber *state = [TMDataStore retrieveObjectforKey:@"com.select.progressstate"];
    TMSelectQuizProgressState progressState = [state integerValue];
    return progressState;
}
-(void)updateLikeActionCounter:(NSInteger)likeCounter {
    [TMDataStore setObject:@(likeCounter) forKey:@"com.matchaction.likecount"];
    if(likeCounter) {
       [self updateLimitedLikeCounter];
    }
    
}
-(void)updateHideActionCounter:(NSInteger)hideCounter {
    [TMDataStore setObject:@(hideCounter) forKey:@"com.matchaction.hdiecount"];
}
-(void)updateSparkActionCounter:(NSInteger)sparkCounter {
    [TMDataStore setObject:@(sparkCounter) forKey:@"com.matchaction.sparkcount"];
}
-(NSInteger)getLikeActionCounter {
    NSNumber *counter = [TMDataStore retrieveObjectforKey:@"com.matchaction.likecount"];
    return [counter integerValue];
}
-(NSInteger)getHideActionCounter {
    NSNumber *counter = [TMDataStore retrieveObjectforKey:@"com.matchaction.hdiecount"];
    return [counter integerValue];
}
-(NSInteger)getSparkActionCounter {
    NSNumber *counter = [TMDataStore retrieveObjectforKey:@"com.matchaction.sparkcount"];
    return [counter integerValue];
}

-(void)cacheSelectMatchId:(NSString*)matchId matchHash:(NSString*)hash {
    [TMDataStore setObject:matchId forKey:@"com.select.matchreload.matchid"];
    [TMDataStore setObject:hash forKey:@"com.select.matchreload.hash"];
}
-(void)resetCachedSelectMatchRefreshData {
    [TMUserSession sharedInstance].needToLoadProfileOnSelectPayment = FALSE;
    [TMUserSession sharedInstance].selectBuyActionOnSelectProfile = FALSE;
    [TMDataStore removeObjectforKey:@"com.select.matchreload.matchid"];
    [TMDataStore removeObjectforKey:@"com.select.matchreload.hash"];
}
-(NSString*)getCachedSelectedMatchIdForRefresh {
    NSString *matchId = [TMDataStore retrieveObjectforKey:@"com.select.matchreload.matchid"];
    return matchId;
}
-(NSString*)getCachedSelectedHashForRefresh {
    NSString *hash = [TMDataStore retrieveObjectforKey:@"com.select.matchreload.hash"];
    return hash;
}

-(int)getPhoneCountryCode {
    return self.user.country;
}

- (BOOL)canShowNRINudge {
    BOOL showNudge = [TMDataStore retrieveBoolforKey:@"nriProfile_nudge"];
    return showNudge;
}

- (void)setNRINudgeShown {
    [TMDataStore setBool:[NSNumber numberWithBool:true] forKey:@"nriProfile_nudge"];
}

- (void)updateLimitedLikeCounter {
    int likeCounter = 0;
    if([TMDataStore containsObjectForKey:@"nriProfile_like_limit"]) {
        if([TMDataStore containsObjectForKey:@"com.matchaction.limitlike"]) {
            likeCounter = [[TMDataStore retrieveObjectforKey:@"com.matchaction.limitlike"] intValue];
        }
        [TMDataStore setObject:[NSNumber numberWithInt:++likeCounter] forKey:@"com.matchaction.limitlike"];
    }
}

- (void)setLikeLimitForUser:(int)likeLimit {
    [TMDataStore setObject:[NSNumber numberWithInt:likeLimit] forKey:@"nriProfile_like_limit"];
}

- (void)checkForLimitLikeCounterForReset {
    if([TMDataStore containsObjectForKey:@"com.matchaction.limitlike.timestamp"]) {
        NSDate *timeStamp, *currentTime;
        timeStamp = [TMDataStore retrieveObjectforKey:@"com.matchaction.limitlike.timestamp"];
        currentTime = [NSDate date];
        int timeDifference = fabs([currentTime timeIntervalSinceDate:timeStamp]);
        if( timeDifference >= 24*60*60) {
            [TMDataStore setObject:[NSDate date] forKey:@"com.matchaction.limitlike.timestamp"];
            [TMDataStore setObject:[NSNumber numberWithInt:0] forKey:@"com.matchaction.limitlike"];
        }
    }else {
        [TMDataStore setObject:[NSDate date] forKey:@"com.matchaction.limitlike.timestamp"];
    }
}

- (int)getLimitedLikeCounter {
    int likeCounter = 0;
    if([TMDataStore containsObjectForKey:@"com.matchaction.limitlike"]) {
        [self checkForLimitLikeCounterForReset];
        likeCounter = [[TMDataStore retrieveObjectforKey:@"com.matchaction.limitlike"] intValue];
    }
    return likeCounter;
}

- (int)getLikeLimitForUser {
    
    int likeLimit = -1;
    if([TMDataStore containsObjectForKey:@"nriProfile_like_limit"]) {
        likeLimit = [[TMDataStore retrieveObjectforKey:@"nriProfile_like_limit"] intValue];
    }
    return likeLimit;
}
@end

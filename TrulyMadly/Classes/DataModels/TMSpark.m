//
//  TMSpark.m
//  TrulyMadly
//
//  Created by Ankit on 02/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSpark.h"
#import "TMChatContext.h"
#import "TMChatUser.h"
#import "TMUserSession.h"
#import "TMTimestampFormatter.h"
#import "NSObject+TMAdditions.h"



@interface TMSpark ()

@property(nonatomic,strong)TMChatUser* matchData;

@end

@implementation TMSpark

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.matchData = [[TMChatUser alloc] init];
    }
    return self;
}
- (instancetype)initWithSparkDictionary:(NSDictionary*)sparkDictionary
{
    self = [super init];
    if (self) {
        self.matchId = [sparkDictionary[@"sender_id"] integerValue];
        self.fullConversationLink = sparkDictionary[@"full_conv_link"];
        self.sparkHash = sparkDictionary[@"hash"];
        self.message = sparkDictionary[@"message"];
        NSString *messageId = sparkDictionary[@"message_id"];
        if([messageId isValidObject] && [messageId isNStringObject]) {
            self.messageId = [messageId integerValue];
        }
        
        self.sparkCreateTime = sparkDictionary[@"create_date"];
        self.sparkExpiryTime = [sparkDictionary[@"expiry_time"] integerValue];
        
        NSString *statusString = sparkDictionary[@"status"];
        self.isSparkSeen = ([statusString isEqualToString:@"sent"]) ? FALSE : TRUE;
        self.isSelectMember = [sparkDictionary[@"is_select"] boolValue];
        
        //spark user data
        self.matchData = [[TMChatUser alloc] initWithSparkResponse:sparkDictionary];
        
    }
    return self;
}

-(BOOL)isSparkExpired {
    if(self.isSparkSeen) {
        NSDate *sparkStartDate = [[TMTimestampFormatter sharedFormatter] absoulteDateFromString:self.sparkStartTime];
        if(sparkStartDate) {
            NSDate *currentDate = [NSDate date];
            NSTimeInterval secs = [[TMTimestampFormatter sharedFormatter] secsBetweenDate:sparkStartDate andDate:currentDate];
            if(secs > self.sparkExpiryTime) {
                return TRUE;
            }
        }
    }
    return FALSE;
}
-(NSString*)sparkExpiryRemainingTime {
    NSInteger remainingMins = self.sparkExpiryTime/60;
    
    if(self.sparkExpiryTime && (![self.sparkStartTime isEqualToString:@""])) {
        NSDate *sparkStartDate = [[TMTimestampFormatter sharedFormatter] absoulteDateFromString:self.sparkStartTime];
        NSDate *currentDate = [NSDate date];
        NSTimeInterval mins = [[TMTimestampFormatter sharedFormatter] minsBetweenDate:sparkStartDate andDate:currentDate];
        NSInteger sparkExpiryMins = self.sparkExpiryTime/60;
        if(mins < sparkExpiryMins) {
            remainingMins = sparkExpiryMins - mins;
        }
    }
    NSString *remainingTime = [self sparkRemainingTime:remainingMins];
    return remainingTime;
}
-(NSString*)sparkRemainingTime:(NSInteger)remainingMins {
    NSString *remainingTime;
    if(remainingMins < 1) {
        remainingTime = @"00:01 min";
    }
    else if(remainingMins > 60) {
        int hour = (int)remainingMins / 60;
        int min = (int)remainingMins % (int)60;
        remainingTime = [NSString stringWithFormat:@"%d:%02d hours", hour, min];
    }
    else {
        int min = (int)remainingMins;
        remainingTime = [NSString stringWithFormat:@"%d:%ld mins",min,(long)0];
    }
    return remainingTime;
}
-(NSString*)sparkRemainingTimeInShortFormat {
    NSString *remainingTime;
    NSInteger remainingMins = self.sparkExpiryTime/60;
    
    if(self.sparkExpiryTime && (![self.sparkStartTime isEqualToString:@""])) {
        NSDate *sparkStartDate = [[TMTimestampFormatter sharedFormatter] absoulteDateFromString:self.sparkStartTime];
        NSDate *currentDate = [NSDate date];
        NSTimeInterval mins = [[TMTimestampFormatter sharedFormatter] minsBetweenDate:sparkStartDate andDate:currentDate];
        NSInteger sparkExpiryMins = self.sparkExpiryTime/60;
        if(mins < sparkExpiryMins) {
            remainingMins = sparkExpiryMins - mins;
        }
    }
    
    if(remainingMins < 1) {
        remainingTime = @"1 min";
    }
    else if(remainingMins > 60) {
        int hour = (int)remainingMins / 60;
        remainingTime = [NSString stringWithFormat:@"%d hr", hour];
    }
    else {
        int min = (int)remainingMins;
        remainingTime = [NSString stringWithFormat:@"%d mins",min];
    }
    return remainingTime;
}

-(NSMutableDictionary*)sparkInsertQueryParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:8];
    params[@"userid"] = @(self.chatContext.loggedInUserId);
    params[@"matchid"] = @(self.chatContext.matchId);
    params[@"message"] = (self.message) ? self.message : @"";
    params[@"messageid"] = @(self.messageId);
    params[@"exptime"] = @(self.sparkExpiryTime);
    params[@"hash"] = self.sparkHash;
    params[@"convlink"] = self.fullConversationLink;
    params[@"seen"] = [NSNumber numberWithInteger:self.isSparkSeen];
    NSString* startTime  = @"";
    if(self.isSparkSeen) {
        startTime = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:[NSDate date]];
    }
    params[@"starttime"] = startTime; //cuuent time ? empty string as spark is not seen till now
    params[@"createdate"] = self.sparkCreateTime;
    NSArray* profileImages = self.matchData.profileImages;
    NSData *metadata = (profileImages) ? [NSKeyedArchiver archivedDataWithRootObject:profileImages] : [NSData data];
    params[@"imageurls"] = metadata;
    params[@"selectmember"] = @(self.isSelectMember);
    return params;
}
-(void)setProfileImagesURLData:(NSData*)imagesURLData {
    NSArray *profileImages = [NSKeyedUnarchiver unarchiveObjectWithData:imagesURLData];
    if(profileImages && [profileImages isKindOfClass:[NSArray class]]) {
        self.matchData.profileImages = profileImages;
    }
}
-(void)setDesignation:(NSString*)designation {
    self.matchData.designation = designation;
}
-(void)setAge:(NSInteger)age {
    self.matchData.age = age;
}
-(void)setCity:(NSString*)city {
    self.matchData.city = city;
}
-(NSString*)fName {
    return self.matchData.fName;
}
-(NSString*)age {
    return [NSString stringWithFormat:@"%ld",(long)self.matchData.age];
}
-(NSString*)designation {
    return self.matchData.designation;
}
-(NSString*)profileURLString {
    return self.matchData.profileLinkURLString;
}
-(NSString*)profileImageURLString {
    return self.matchData.profileImageURLString;
}
-(TMChatUser*)matchData {
    return _matchData;
}
-(void)updateMatchData:(TMChatUser*)matchData {
    self.matchData.isMsTM = FALSE;
    self.matchData.isSparkMatch = TRUE;
    self.matchData.userId = matchData.userId;
    self.matchData.age = matchData.age;
    self.matchData.fName = matchData.fName;
    self.matchData.userId = matchData.userId;
    self.matchData.city = matchData.city;
    self.matchData.designation = matchData.designation;
    self.matchData.profileLinkURLString = matchData.profileLinkURLString;
    self.matchData.profileImageURLString = matchData.profileImageURLString;
}
-(TMChatContext*)chatContext {
    TMChatContext *chatContext = [[TMChatContext alloc] init];
    chatContext.matchId = self.matchId;
    chatContext.loggedInUserId = [[TMUserSession sharedInstance].user.userId integerValue];
    return chatContext;
}

- (CGFloat)calculateProgressForValue:(CGFloat)value start:(CGFloat)start end:(CGFloat)end
{
    CGFloat diff = (value - start);
    CGFloat scope = (end - start);
    CGFloat progress;
    
    if(diff != 0.0) {
        progress = diff / scope;
    } else {
        progress = 0.0f;
    }
    
    return progress*100;
}

@end

//
//  TMChatUser.m
//  TrulyMadly
//
//  Created by Ankit on 03/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMChatUser.h"
#import "NSObject+TMAdditions.h"

@implementation TMChatUser

-(instancetype)initWithSocketResponse:(NSDictionary*)chatUserDataDictionary {
    self = [super init];
    if(self) {
        self.userId = [chatUserDataDictionary[@"profile_id"] integerValue];
        self.fName = chatUserDataDictionary[@"fname"];
        self.profileLinkURLString = chatUserDataDictionary[@"profile_link"];
        self.profileImageURLString = chatUserDataDictionary[@"profile_pic"];
        self.isMsTM = [chatUserDataDictionary[@"is_miss_tm"] boolValue];
    }
    return self;
}

-(instancetype)initWithPollingResponse:(NSDictionary*)chatUserDataDictionary {
    self = [super init];
    if(self) {
        self.userId = [chatUserDataDictionary[@"profile_id"] integerValue];
        self.fName = chatUserDataDictionary[@"fname"];
        self.profileLinkURLString = chatUserDataDictionary[@"profile_link"];
        self.profileImageURLString = chatUserDataDictionary[@"profile_pic"];
        self.isMsTM = [chatUserDataDictionary[@"is_miss_tm"] boolValue];
    }
    return self;
}

-(instancetype)initWithSparkResponse:(NSDictionary*)sparkDictionary {
    self = [super init];
    if(self) {
        self.userId = [sparkDictionary[@"sender_id"] integerValue];
        self.fName = sparkDictionary[@"fname"];
        self.profileLinkURLString = sparkDictionary[@"profile_link"];
        self.profileImageURLString = sparkDictionary[@"profile_pic"];
        self.profileImages = sparkDictionary[@"images"];
        self.designation = sparkDictionary[@"designation"];
        self.city = sparkDictionary[@"city"];
        self.age = [sparkDictionary[@"age"] integerValue];
        self.isMsTM = FALSE;
        self.isSparkMatch = TRUE;
    }
    return self;
}

-(NSArray*)getCombinedProfileImages {
    NSMutableArray *combinedProfiles = [NSMutableArray array];
    [combinedProfiles addObject:self.profileImageURLString];
    
    if(self.profileImages.count) {
        if(self.profileImages.count == 1) {
            NSString *imageURLString = self.profileImages[0];
            if([imageURLString isValidObject] && [imageURLString isNStringObject]) {
                [combinedProfiles insertObject:[self.profileImages objectAtIndex:0] atIndex:0];
            }
        }
        else if(self.profileImages.count >= 2) {
            NSString *imageURLString = self.profileImages[0];
            if([imageURLString isValidObject] && [imageURLString isNStringObject]) {
                [combinedProfiles insertObject:imageURLString atIndex:0];
            }
            
            imageURLString = self.profileImages[1];
            if([imageURLString isValidObject] && [imageURLString isNStringObject]) {
                [combinedProfiles insertObject:imageURLString atIndex:2];
            }
        }
    }
    return combinedProfiles;
}

@end

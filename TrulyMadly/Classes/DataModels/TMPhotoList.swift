//
//  TMPhotoList.swift
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMPhotoList: NSObject {
   
    var photoList:NSMutableArray = []
    
    init(photoList:NSArray) {
        super.init()
        //let resultPredicate = NSPredicate(format: "is_profile == %@", "yes")
        var i = photoList.count-1
        for _ in (0 ..< photoList.count) {
        //for (var i=photoList.count-1; i>=0; i -= 1) {
            let photoDict = photoList.object(at: i) as! NSDictionary
            let isProfilePhoto = photoDict.object(forKey: "is_profile") as! NSString
            
            if(isProfilePhoto == "yes"){
                self.photoList.insert(photoDict, at: 0)
            }
            else {
                self.photoList.add(photoDict)
            }
            
            i = i+1
        }
    }
    
    func photoForIndex(_ index:Int) -> TMPhoto? {
        
        var photoObj:TMPhoto?
        if(index<self.photoList.count) {
            let photoDict = self.photoList[index] as! NSDictionary
            photoObj = TMPhoto.init(photoDict:photoDict)
        }
        
        return photoObj
    }
}

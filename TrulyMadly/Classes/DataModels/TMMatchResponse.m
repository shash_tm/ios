//
//  TMMatchResponse.m
//  TrulyMadly
//
//  Created by Ankit on 02/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMatchResponse.h"
#import "TMProfile.h"
#import "TMUserSession.h"
#import "TMSystemMessage.h"


@interface TMMatchResponse ()

@property(nonatomic,strong)NSMutableArray *profileList;

@end


@implementation TMMatchResponse

-(instancetype)initWithDictionary:(NSDictionary*)response {
    self = [super init];
    if(self) {
        NSArray *profiles = [response objectForKey:@"data"];
        self.profileList = [NSMutableArray array];
        [self.profileList addObjectsFromArray:profiles];
        
        NSDictionary *systemMsgDictionary = [response objectForKey:@"system_messages"];
        self.systemMessage = [[TMSystemMessage alloc] initWithSystemMessgaeDictionary:systemMsgDictionary];
        
        NSDictionary *userDataDictionary = [response objectForKey:@"my_data"];
        if (userDataDictionary) {
            TMUserSession *userSession = [TMUserSession sharedInstance];
            [userSession setUserData:userDataDictionary];
            [userSession setUserNavigationStatusAsMovedToMatches];
            
            id mutualLikes = [userDataDictionary objectForKey:@"mutual_like_count"];
            if(mutualLikes != nil) {
                self.mutualLikeCount = [mutualLikes integerValue];
            }
            
            id unreadChats = [userDataDictionary objectForKey:@"message_count"];
            if(unreadChats != nil) {
                self.unreadChatCount = [unreadChats integerValue];
            }
            
            NSDictionary *trustScores = userDataDictionary[@"trustmeter"];
            if(trustScores != nil) {
                self.trustScores = trustScores;
            }
        }
        
        //system flags
        NSDictionary *systemFlags = response[@"system_flags"];
        if(systemFlags) {
            self.isSocketConnectionFlagEnabled = [systemFlags[@"is_socket_enabled"] boolValue];
            self.isSocketDebugFlagEnabled = [systemFlags[@"socket_debug_flag"] boolValue];
            TMUserSession *session = [TMUserSession sharedInstance];
            [session setSocketEnabled:self.isSocketConnectionFlagEnabled
                 withDebugFlagEnabled:self.isSocketDebugFlagEnabled];
        }
        
        // ab nudges
        NSDictionary *abString = response[@"abString"];
        if(abString != nil) {
            NSDictionary *AestheticsAB = abString[@"AestheticsAB"];
            if(AestheticsAB != nil) {
                id aesthetics_nudge = [AestheticsAB objectForKey:@"Aesthetics_Nudge"];
                if (aesthetics_nudge != nil) {
                    NSArray *Aesthetics_Nudge = AestheticsAB[@"Aesthetics_Nudge"];
                    
                    if([Aesthetics_Nudge[0] isValidObject] && [Aesthetics_Nudge[1] isValidObject]) {
                        NSArray *nudgePoint = Aesthetics_Nudge[0];
                        NSArray *nudgeMsg = Aesthetics_Nudge[1];
                        NSMutableDictionary* nudgeDict = [[NSMutableDictionary alloc] init];
                        for(int i=0; i<nudgePoint.count; i++) {
                            NSInteger key = [nudgePoint[i] integerValue];
                            nudgeDict[[NSNumber numberWithInteger:key]] = nudgeMsg[i];
                        }
                        self.nudgeDict = nudgeDict;
                        self.isNudgeAvailable = true;
                    }else {
                        self.isNudgeAvailable = false;
                        self.nudgeDict = nil;
                    }
                }else {
                    self.isNudgeAvailable = false;
                    self.nudgeDict = nil;
                }
            }
        }
        
        // photo flow parameters
        if(response[@"validProfilePic"] && [response[@"validProfilePic"] isValidObject])
        {
            self.isValidProfilePic = [response[@"validProfilePic"] boolValue];
        }
        
        if(response[@"noPhotos"] && [response[@"noPhotos"] isValidObject])
        {
            self.hasPhotos = [response[@"noPhotos"] boolValue];
        }
        
        if(response[@"likedInThePast"] && [response[@"likedInThePast"] isValidObject])
        {
            self.hasLikedInThePast = [response[@"likedInThePast"] boolValue];
        }
        
        self.hasMoreMatches = [response[@"hasMoreRecommendations"] boolValue];
        self.isNewRecommendation = [response[@"isNewRecommendation"] boolValue];
    }
    return self;
}
-(void)removerAllProfiles {
    [self.profileList removeAllObjects];
}
-(void)removerProfile:(TMProfile*)profile matchingIndex:(NSInteger)matchingIndex{
    NSInteger index = [self.profileList indexOfObject:profile];
    if(index > matchingIndex) {
        [self.profileList removeObjectAtIndex:matchingIndex];
    }
}
-(TMProfile*)profileForIndex:(NSInteger)index {
    TMProfile *userProfile = nil;
    if(index < self.profileList.count) {
        NSDictionary *profileDictionary = [self.profileList objectAtIndex:index];
        userProfile = [[TMProfile alloc] initWithProfileData:profileDictionary trustScores:self.trustScores];
    }
    return userProfile;
}
-(TMProfile*)profileForIndex:(NSInteger)index editMode:(BOOL)editMode {
    TMProfile *userProfile = NULL;
    if(index<self.profileList.count) {
        NSDictionary *profileDictionary = [self.profileList objectAtIndex:index];
        userProfile = [[TMProfile alloc] initWithProfileData:profileDictionary withEditMode:editMode trustScores:self.trustScores];
    }
    return userProfile;
}

//SeventyNine profile Ad changes
- (TMProfile*) matchProfileForAd {
    TMProfile *adProfile = [[TMProfile alloc] initWithProfileAdData];
    return adProfile;
}
-(BOOL)isProfileAvailable {
    BOOL status = false;
    if(self.profileList.count > 0) {
        status = true;
    }
    return status;
}
-(NSInteger)profileCount {
    return self.profileList.count;
}

@end

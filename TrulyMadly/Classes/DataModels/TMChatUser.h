//
//  TMChatUser.h
//  TrulyMadly
//
//  Created by Ankit on 03/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMChatUser : NSObject

@property(nonatomic,assign)BOOL isMsTM;
@property(nonatomic,assign)BOOL isSparkMatch;
//@property(nonatomic,assign)BOOL isSelectMember;
@property(nonatomic,assign)NSInteger userId;
@property(nonatomic,assign)NSInteger age;
@property(nonatomic,strong)NSString *fName;
@property(nonatomic,strong)NSString *city;
@property(nonatomic,strong)NSString *designation;
@property(nonatomic,strong)NSString *profileLinkURLString;
@property(nonatomic,strong)NSString *profileImageURLString;
@property(nonatomic,strong)NSString *cacheTs;
@property(nonatomic,strong)NSString *doodleLink;
@property(nonatomic,strong)NSArray *profileImages;

-(instancetype)initWithSocketResponse:(NSDictionary*)chatUserDataDictionary;
-(instancetype)initWithPollingResponse:(NSDictionary*)chatUserDataDictionary;
-(instancetype)initWithSparkResponse:(NSDictionary*)sparkDictionary;
-(NSArray*)getCombinedProfileImages;

@end

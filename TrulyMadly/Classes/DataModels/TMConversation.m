//
//  TMMessageConversation.m
//  TrulyMadly
//
//  Created by Ankit on 10/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMConversation.h"
#import "TMSwiftHeader.h"
#import "TMChatContext.h"
#import "TMChatUser.h"
#import "TMTimestampFormatter.h"


@interface TMConversation ()

@property(nonatomic,strong)NSString *messageFormattedTimestamp;

@end

@implementation TMConversation

-(instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    if(self) {
        /////match data
        self.receiverAge = [[dictionary objectForKey:@"age"] integerValue];
        self.fName = [dictionary objectForKey:@"fname"];
        self.fullConvLink = [dictionary objectForKey:@"full_conv_link"];
        self.profileImageURL = [dictionary objectForKey:@"profile_pic"];
        self.profileURLString = [dictionary objectForKey:@"profile_link"];
        ///message data
        ////message
        NSInteger senderId = [dictionary[@"sender_id"] integerValue];
        NSInteger receiverId = [dictionary[@"receiver_id"] integerValue];
        if(self.userId == senderId) {
            self.matchId = receiverId;
            self.sender = MESSAGESENDER_ME;
        }
        else {
            self.matchId = senderId;
            self.sender = MESSAGESENDER_MATCH;
        }
        
        NSString *msgStr = [dictionary objectForKey:@"message"];
        if(msgStr && [msgStr isKindOfClass:[NSString class]]) {
            self.message = msgStr;
        }
        else {
            self.message = @"";
        }
        self.seen = [[dictionary objectForKey:@"seen"] boolValue];
        self.timestamp = [dictionary objectForKey:@"timestamp"];
        self.conversationSortTs = dictionary[@"row_updated_tstamp"];
        
        ////////
        NSString *convMessageType = dictionary[@"msg_type"];
        if(convMessageType && [convMessageType isKindOfClass:[NSString class]]) {
            if([convMessageType isEqualToString:@"MUTUAL_LIKE"]) {
                self.conversationMessageType = CONVERSATIONMESSAGE_MUTUALLIKE;
                if(!self.message) {
                    self.message = @"has liked you too. Say Hello!";
                }
            }
            else if([convMessageType isEqualToString:@"IMAGE"]) {
                self.conversationMessageType = CONVERSATIONMESSAGE_IMAGE;
                if(self.sender == MESSAGESENDER_ME) {
                    self.message = @"You've sent an image!";
                }
                else {
                    self.message = @"You've received an image!";
                }
            }
            else {
                self.conversationMessageType = CONVERSATIONMESSAGE_TEXT;
            }
        }
        
        /////conversation data
        ////blocked by
        NSString *blockedByStr = dictionary[@"blocked_by"];
        if(blockedByStr && [blockedByStr isKindOfClass:[NSString class]]) {
            self.isBlocked = TRUE;
        }
        else {
            self.isBlocked = FALSE;
        }
        /////is block shown
        id isBlockShown = dictionary[@"is_blocked_shown"];
        if(isBlockShown && [isBlockShown isKindOfClass:[NSString class]]) {
            self.isBlockedShown = [isBlockShown boolValue];
        }
        else {
            self.isBlockedShown = FALSE;
        }
       
        self.isMsTm = [dictionary[@"is_miss_tm"] boolValue];
        
        self.isSparkMatch = [dictionary[@"is_spark"] boolValue];
        
        self.dealState = DealStateDisabled;
        
        self.isSelectMember = [dictionary[@"is_select_match"] boolValue];
    }
    return self;
}

-(NSInteger)userId {
    TMUser *user = [TMUserSession sharedInstance].user;
    return [user.userId integerValue];
}
-(BOOL)isUnreadMessageConversation {
    if( (!self.seen) && (self.sender == MESSAGESENDER_MATCH) ) {
        return TRUE;
    }
    else if((self.conversationMessageType == CONVERSATIONMESSAGE_MUTUALLIKE) && (!self.seen)) {
        return TRUE;
    }
    return false;
}

-(TMChatUser*)getChatUser {
    TMChatUser *chatUser = [[TMChatUser alloc] init];
    chatUser.userId = self.matchId;
    chatUser.age = self.receiverAge;
    chatUser.fName = self.fName;
    chatUser.profileImageURLString = self.profileImageURL;
    chatUser.profileLinkURLString = self.profileURLString;
    chatUser.isMsTM = self.isMsTm;
    chatUser.isSparkMatch = self.isSparkMatch;
    return chatUser;
}
-(TMChatContext*)getChatContext {
    TMChatContext *chatContext = [[TMChatContext alloc] init];
    chatContext.matchId = self.matchId;
    chatContext.loggedInUserId = self.userId;
    return chatContext;
}
-(NSString*)escapedMessage {
//    NSString *message = [_message stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *message = [_message stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    return message;
}
-(NSString*)messageFormattedTimestamp {
    _messageFormattedTimestamp = @"";
    if( (_messageFormattedTimestamp != nil) && (![self.message isEqualToString:@""]) ) {
        NSDate *date = [[TMTimestampFormatter sharedFormatter] absoulteDateFromString:self.timestamp];
        _messageFormattedTimestamp = [[TMTimestampFormatter sharedFormatter] shortTimestampForDate:date];
    }
    return _messageFormattedTimestamp;
}

-(NSDictionary*)getInsertQueryParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"userid"] = @(self.userId);
    params[@"matchid"] = @(self.matchId);
    params[@"message"] = self.message;
    params[@"msgts"] = self.timestamp;
    params[@"messagesender"] = @(self.sender);
    params[@"messagesource"] = @(self.source);
    params[@"messagestatus"] = @(self.status);
    params[@"seen"] = @(self.seen);
    params[@"convmessagetype"] =  @(self.conversationMessageType);
    params[@"convlistsortts"] = self.conversationSortTs;
    params[@"convlink"] = self.fullConvLink;
    params[@"blocked"] = @(self.isBlocked);
    params[@"blockshown"] = @(self.isBlockedShown);
    params[@"archive"] = @(self.isArchived);
    params[@"isselectmember"] = @(self.isSelectMember);
    return params;
}
-(NSDictionary*)getUpdateQueryParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"userid"] = @(self.userId);
    params[@"matchid"] = @(self.matchId);
    params[@"message"] = self.message;
    params[@"msgts"] = self.timestamp;
    params[@"messagesender"] = @(self.sender);
    params[@"messagesource"] = @(self.source);
    params[@"messagestatus"] = @(self.status);
    params[@"seen"] = @(self.seen);
    params[@"convmessagetype"] =  @(self.conversationMessageType);
    params[@"convlistsortts"] = self.conversationSortTs;
    params[@"convlink"] = self.fullConvLink;
    params[@"blocked"] = @(self.isBlocked);
    params[@"blockshown"] = @(self.isBlockedShown);
    params[@"archive"] = @(self.isArchived);
    params[@"matchidw"] = @(self.matchId);
    params[@"useridw"] = @(self.userId);
    params[@"isselectmember"] = @(self.isSelectMember);
    return params;
}

@end


//
//  TMMatchResponse.h
//  TrulyMadly
//
//  Created by Ankit on 02/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMSystemMessage;
@class TMProfile;

@interface TMMatchResponse : NSObject

//@property(nonatomic,strong,readonly)NSMutableArray *profileList;
@property(nonatomic,strong)TMSystemMessage *systemMessage;

@property(nonatomic,assign)NSInteger mutualLikeCount;
@property(nonatomic,assign)NSInteger unreadChatCount;

@property(nonatomic,assign)BOOL isSocketConnectionFlagEnabled;
@property(nonatomic,assign)BOOL isSocketDebugFlagEnabled;

@property(nonatomic,strong)NSDictionary *trustScores;
@property(nonatomic,strong)NSDictionary *nudgeDict;
@property(nonatomic,assign)BOOL isNudgeAvailable;

@property(nonatomic,assign)BOOL isValidProfilePic;
@property(nonatomic,assign)BOOL hasPhotos;
@property(nonatomic,assign)BOOL hasLikedInThePast;
@property(nonatomic,assign)BOOL hasMoreMatches;
@property(nonatomic,assign)BOOL isNewRecommendation;


-(instancetype)initWithDictionary:(NSDictionary*)response;
-(TMProfile*)profileForIndex:(NSInteger)index;
-(TMProfile*)profileForIndex:(NSInteger)index editMode:(BOOL)editMode;
-(BOOL)isProfileAvailable;
-(NSInteger)profileCount;
-(void)removerProfile:(TMProfile*)profile matchingIndex:(NSInteger)matchingIndex;
-(void)removerAllProfiles;

//SeventyNine Profile Ad Changes
- (TMProfile*)matchProfileForAd;
@end

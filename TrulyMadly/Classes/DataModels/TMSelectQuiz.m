//
//  TMSelectQuiz.m
//  TrulyMadly
//
//  Created by Ankit on 02/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuiz.h"
#import "TMDataStore.h"

@interface TMSelectQuiz ()

@property(nonatomic,strong)NSString *quizText;
@property(nonatomic,strong)NSString *quizImageURLString;
@property(nonatomic,assign)TMSelectQuizAnswer *quizAnswer;
@property(nonatomic,assign)NSInteger questionId;
@property(nonatomic,assign)NSInteger yesOptionId;
@property(nonatomic,assign)NSInteger noOptionId;

@end


@implementation TMSelectQuiz

- (instancetype)initWithQuizDictionary:(NSDictionary*)quizDictionary
{
    self = [super init];
    if (self) {
        self.quizAnswer = NotAnswered;
        self.questionId = [quizDictionary[@"question_id"] integerValue];
        self.quizImageURLString = quizDictionary[@"question_image"];
        self.quizText = quizDictionary[@"question_text"];
        
        NSArray *options = quizDictionary[@"options"];
        if(options.count == 2) {
            NSDictionary *nooption = options[0];
            NSDictionary *yesoption = options[1];
            self.noOptionId =  [nooption[@"option_id"] integerValue];
            self.yesOptionId = [yesoption[@"option_id"] integerValue];
        }
        
        /*for (NSDictionary *option in options) {
            NSString *optionText = option[@"option_text"];
            if([optionText isEqualToString:@"No"]) {
                self.noOptionId = [option[@"option_id"] integerValue];
            }
            else if([optionText isEqualToString:@"Yes"]) {
                self.yesOptionId = [option[@"option_id"] integerValue];
            }
        }*/
    }
    return self;
}

-(void)setQuizAnswerWithYesOptionId {
    [self setQuizAnswerWithOptionId:self.yesOptionId];
}
-(void)setQuizAnswerWithNoOptionId {
    [self setQuizAnswerWithOptionId:self.noOptionId];
}
-(void)setQuizAnswerWithOptionId:(NSInteger)optionId {
    NSString *key = [self getSelectionQuizOptionCacheKey];
    ////////////////
    //NSString *alreadySelectedOptionId = [self getSelectedOptionId];
    //NSLog(@"Already Selected option:%@ forKey:%@",alreadySelectedOptionId,key);
    ////////////////
    //NSLog(@"seting optionid:%ld forKey:%@",(long)optionId,key);
    [TMDataStore setObject:[NSString stringWithFormat:@"%ld",optionId]
                    forKey:key];
}
-(NSString*)getSelectedOptionId {
    NSString *key = [self getSelectionQuizOptionCacheKey];
    NSString *status = [TMDataStore retrieveObjectforKey:key];
    return status;
}
-(void)removeSelectedOtionId {
    NSString *key = [self getSelectionQuizOptionCacheKey];
    [TMDataStore removeObjectforKey:key];
}
-(TMSelectQuizAnswer)quizAnswerStatus {
    NSString *key = [self getSelectionQuizOptionCacheKey];
    NSString *status = [TMDataStore retrieveObjectforKey:key];
    //NSLog(@"optionid:%@ forkey:%@",status,key);
    if(!status) {
        return NotAnswered;
    }
    else if([status integerValue] == self.yesOptionId) {
        return Yes;
    }
    else if([status integerValue] == self.noOptionId) {
        return No;
    }
    else {
        return NotAnswered;
    }
}

-(NSString*)getSelectionQuizOptionCacheKey {
    NSString *key = [NSString stringWithFormat:@"com.select.quiz.optionkey.%ld",(long)self.questionId];
    return key;
}
@end

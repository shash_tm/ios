//
//  TMProfileBasicInfo.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMProfileBasicInfo : NSObject

@property(nonatomic,strong)NSArray *fbMutualFriendList;
@property(nonatomic,strong)NSString *matchId;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *imageUrlString;
@property(nonatomic,strong)NSArray *otherImages;
@property(nonatomic,strong)NSString *age;
@property(nonatomic,strong)NSString *city;
@property(nonatomic,strong)NSString *height;
@property(nonatomic,strong)NSString *lastActivity;
@property(nonatomic,assign)NSInteger facebookMutualConnections;
@property(nonatomic,assign)BOOL hasChildren;
@property(nonatomic,assign)BOOL isCeleb;
@property(nonatomic,assign)BOOL isDummySet;
@property(nonatomic,assign)BOOL isProfileAdCampaign;
@property(nonatomic,assign)BOOL isSelectMember;

-(instancetype)initWithData:(NSDictionary*)profileData isProfileAdCampaign:(BOOL)isProfileAdCampaign;

@end

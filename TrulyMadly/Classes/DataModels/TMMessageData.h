//
//  TMMessageData.h
//  TrulyMadly
//
//  Created by Ankit on 28/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TMMessageData <NSObject>

@required

- (NSInteger)senderId;
- (NSDate*)date;
- (BOOL)isMediaMessage;

@optional

- (NSUInteger)hash;
-(BOOL)isOutgoingMessage;
- (NSString *)text;

@end

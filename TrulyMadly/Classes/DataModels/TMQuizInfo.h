//
//  QuizDomainObject.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMQuizInfo : NSObject

@property (nonatomic, strong, readonly) NSString* quizID;
@property (nonatomic, strong, readonly) NSString* quizIconURL;
@property (nonatomic, strong, readonly) NSString* quizBannerURL;
@property (nonatomic, strong, readonly) NSString* quizDisplayName;
@property (nonatomic, strong, readonly) NSString* quizDescription;
@property (nonatomic, strong, readonly) NSString* quizSponsoredText;
@property (nonatomic, strong, readonly) NSString* quizSponsoredImageURL;
@property (nonatomic, strong, readonly) NSString* quizSuccessMessage;
@property (nonatomic, strong, readonly) NSString* quizSuccessNudge;
@property (nonatomic, strong, readonly) NSArray* playedBy;
@property (nonatomic, strong, readonly) NSArray* flareUserArray;
@property (nonatomic, readwrite) BOOL isNew;

- (instancetype) initWithDictionary:(NSDictionary*) quizInfoDictionary;
- (BOOL) isFlareForUserID:(NSString*) userID;

@end

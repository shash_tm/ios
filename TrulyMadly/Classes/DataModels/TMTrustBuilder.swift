//
//  TMTrustBuilder.swift
//  TrulyMadly
//
//  Created by Rajesh Singh on 15/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMTrustBuilder: NSObject {
   
    //trust score
    var fbScore:Int!
    var linkedinScore:Int!
    var mobileScore:Int!
    var idScore:Int!
    var empScore:Int!
    var endorseScore: Int!
    
    //linkedin verified data
    var linkedinDesignation:String?
    var linkedinConnection:String?
    var linkedinMsg:String = "We won't post on your profile."
    var linkedinError: Bool = false
    
    //facebook verified data
    var fbConnection:String!
    var fbMessages:NSDictionary!
    var TMFBdiff:NSDictionary!
    var fbMismatchReason:String!
    var fbMsg:String!
    var fbError: Bool = false
    
    var phoneNumber:String?
    var number_of_trials:Int = 0
    var phoneError: Bool = false
    
    var idType:String?
    var idRejectReason:String?
    var idMsg:String = "We just want to make sure who you are. Your documents will be deleted after verification."
    var idMessages:NSDictionary!
    var id_diff_data:NSDictionary!
    var idProofStatus:String =  ""
    var idError: Bool = false
    
    var isFbVerified:Bool = false
    var isPhoneVerified:Bool = false
    var isLinkedinVerified:Bool = false
    var isIdVerified:Bool = false
    var isempVerified:Bool = false
    var companyName:String?
    
    var empType:String?
    var empRejectReason:String?
    var empMsg:String?
    var empMessages:NSDictionary?
    var empProofStatus:String?
    
    var endorseLink: String = ""
    var endorsementMsg: String = ""
    var endorsementSuccessMsg: String = ""
    var endorseData: NSArray = [AnyObject]() as NSArray
    var endorseCount: Int = 2
    var isEndorseVerified: Bool = false
    
    var trustScore:Int!
    
    var status:String?
    
    var thresold:Int!
    
    var phoneRejectedMsg:String = "We'll call you to confirm your number. That's all. We won't share it with anyone."
    
    var trustbuilderData = [:] as Dictionary<String,AnyObject>

    var trustPoints: [AnyHashable: Any] = [:]
    var facebookObj: [AnyHashable: Any] = [:]
    var linkedinObj: [AnyHashable: Any] = [:]
    var phoneObj: [AnyHashable: Any] = [:]
    var idProofObj: [AnyHashable: Any] = [:]
    var endorsementObj: [AnyHashable: Any] = [:]
    var introMessages: [AnyHashable: Any] = [:]
    
    init(trustbuilderData:Dictionary<String,AnyObject>) {
        super.init()
        
        self.trustbuilderData = trustbuilderData
        self.status = self.trustbuilderData["status"] as? String
        
        let trust: AnyObject! = self.trustbuilderData["trustScore"] as AnyObject!
        if(!(trust is NSNull)) {
            if let trust = self.trustbuilderData["trustScore"] as? String{
                self.trustScore = Int(trust)
            }else {
                self.trustScore = self.trustbuilderData["trustScore"] as! Int
            }
        }else {
            self.trustScore = 0
        }
        
        self.trustPoints = self.trustbuilderData["trustPoints"] as! [AnyHashable: Any]
        self.facebookObj = self.trustbuilderData["facebook"] as! [AnyHashable: Any]
        self.linkedinObj = self.trustbuilderData["linkedin"] as! [AnyHashable: Any]
        self.phoneObj = self.trustbuilderData["phone"] as! [AnyHashable: Any]
        self.idProofObj = self.trustbuilderData["idProof"] as! [AnyHashable: Any]
        self.introMessages = self.trustbuilderData["messages"] as! [AnyHashable: Any]
        self.endorsementObj = self.trustbuilderData["endorsementData"] as! [AnyHashable: Any]
        
        self.setTrustPoints()
        self.setFacebookBlock()
        self.setLinkedinBlock()
        self.setIdBlock()
        self.setPhoneBlock()
        self.setEndorsementBlock()
    }
    
    func getTrustScore()->Int {
        return self.trustScore!
    }
    
    private func setPhoneBlock() {
        let verified = self.phoneObj["isVerified"] as AnyObject
        let isVerified = verified as? Bool
        if(isVerified == true){
            self.phoneError = false
            self.isPhoneVerified = true
            self.phoneNumber = self.phoneObj["phone_number"] as? String
        }else{
            self.isPhoneVerified = false
            if let trials = self.phoneObj["number_of_trials"] as? String {
                if(trials != "null") {
                    self.number_of_trials = Int(trials)!
                }
            }
            if(self.number_of_trials >= 3) {
                self.phoneError = true
                self.phoneRejectedMsg = self.phoneObj["rejected_message"] as! String
            }
        }
    }
    
    private func setTrustPoints(){
        self.fbScore = self.trustPoints["fb"] as! Int
        self.linkedinScore = self.trustPoints["linkedin"] as! Int
        self.mobileScore = self.trustPoints["phone"] as! Int
        self.idScore = self.trustPoints["photo_id"] as! Int
        self.empScore = self.trustPoints["employment"] as! Int
        self.endorseScore = self.trustPoints["endorsements"] as! Int
        self.thresold = self.trustPoints["threshold"] as! Int
    }
    
    private func setFacebookBlock() {
        print(self.facebookObj)
        let verified = self.facebookObj["isVerified"] as AnyObject
        let isVerified = verified as? Bool
        if(isVerified == true){
            self.fbError = false
            self.isFbVerified = true
            self.fbConnection = self.facebookObj["fbConnection"] as! String
        }else{
            self.isFbVerified = false
            self.fbMessages = self.facebookObj["fb_messages"] as? NSDictionary
            let mismatchReason = self.facebookObj["fb_mismatch"] as? String
            if(mismatchReason != nil){
                self.fbError = true
                self.fbMismatchReason = mismatchReason
                if let diff = self.facebookObj["TMFBdiff"] as? NSDictionary{
                    self.TMFBdiff = diff
                }
                self.fbMsg = self.fbMismatchError(mismatchReason!)
            }
        }
    }
    
    func updateFacebookBlock(_ facebookResponse:NSDictionary!) {
        
        if let parseJson = facebookResponse{
            if let status = parseJson["status"] as? String{
                if(status == "success"){
                    self.fbError = false
                    self.trustScore = self.trustScore + self.fbScore
                    if let connections = parseJson["connections"] as? String{
                        self.fbConnection = connections
                    }else{
                        let connections = parseJson["connections"] as! Int
                        self.fbConnection = String(connections)
                    }
                    self.isFbVerified = true
                }else if(status == "mismatch"){
                    self.fbError = true
                    self.fbMismatchReason = parseJson["error"] as! String
                    self.TMFBdiff = parseJson["diff"] as! NSDictionary
                    self.fbMsg = self.fbMismatchError(parseJson["error"] as! String)
                }else if(status == "fail" || status == "refresh"){
                    self.fbError = true
                    self.fbMsg = parseJson["error"] as! String
                }
            }
        }
        
    }
    
    private func setIdBlock() {
        let verified = self.idProofObj["isVerified"] as AnyObject
        let isVerified = verified as? Bool
        if(isVerified == true){
            self.idError = false
            self.isIdVerified = true
            self.idType = self.idProofObj["isProofType"] as? String
        }else{
            self.isIdVerified = false
            if let idStatus = self.idProofObj["status"] as? String{
                self.idProofStatus = idStatus
                self.idMessages = self.idProofObj["id_messages"] as? NSDictionary
                let idMessages: NSDictionary = self.idProofObj["id_messages"] as! NSDictionary
                self.id_diff_data = self.idProofObj["id_diff_data"] as? NSDictionary
                let id_diff_data: NSDictionary = self.idProofObj["id_diff_data"] as! NSDictionary
                self.idType = self.idProofObj["isProofType"] as? String
                if(idStatus == "under_moderation"){
                    self.idMsg = idMessages["moderation"] as! String
                    self.idMsg = self.idMsg.replacingOccurrences(of: "idType", with: self.idType!, options: NSString.CompareOptions.literal, range: nil)
                }else if(idStatus == "rejected"){
                    self.idError = true
                    self.idRejectReason = self.idProofObj["isRejectReason"] as? String
                    if(self.idRejectReason == "not_clear") {
                        self.idMsg = idMessages["not_clear"] as! String
                    }else if(self.idRejectReason == "password_required") {
                        self.idMsg = idMessages["pwd_reqired"] as! String
                    }else {
                        //self.idMsg = self.idMismatchError(self.idRejectReason!)
                       // self.idMsg = self.idMismatchError(self.idRejectReason! , idMessages:idMessages, id_diff_data:id_diff_data)
                        self.idMsg = self.idMisMatchError(self.idRejectReason!, idMessages: idMessages, id_diff_data: id_diff_data)
                    }
                }else if(idStatus == "fail") {
                    self.idError = true
                    self.idMsg = "Please get in touch with our customer care."
                }
            }
        }

    }
    
    private func setLinkedinBlock() {
        let verified = self.linkedinObj["isVerified"] as AnyObject
        let isVerified = verified as? Bool
        if(isVerified == true){
            self.isLinkedinVerified = true
            self.linkedinDesignation = self.linkedinObj["linkdesignation"] as? String
            self.linkedinConnection = self.linkedinObj["linkconnection"] as? String
        }else{
            self.isLinkedinVerified = false
        }
    }
    
    private func fbMismatchError(_ mismatchReason:String)->String{
        var msg = ""
        
        if(mismatchReason == "Age mismatch"){
            msg = self.fbMessages["age_mismatch"] as! String
            let tmpFb: AnyObject = self.TMFBdiff["fbAge"]! as AnyObject
            let tmpTM: AnyObject = self.TMFBdiff["TMage"]! as AnyObject
            if( (!(tmpFb is NSNull)) && (!(tmpTM is NSNull)) ) {
                msg = msg.replacingOccurrences(of: "tmAge", with: self.TMFBdiff["TMage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbAge", with: self.TMFBdiff["fbAge"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }else if(mismatchReason == "Name mismatch"){
            msg = self.fbMessages["name_mismatch"] as! String
            let tmpFb: AnyObject = self.TMFBdiff["fbName"]! as AnyObject
            let tmpTM: AnyObject = self.TMFBdiff["TMname"]! as AnyObject
            if( (!(tmpFb is NSNull)) && (!(tmpTM is NSNull)) ) {
                msg = msg.replacingOccurrences(of: "tmName", with: self.TMFBdiff["TMname"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbName", with: self.TMFBdiff["fbName"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }else if(mismatchReason == "Name and Age mismatch"){
            msg = self.fbMessages["name_age_mismatch"] as! String
            let tmpFbName: AnyObject = self.TMFBdiff["fbName"]! as AnyObject
            let tmpTMName: AnyObject = self.TMFBdiff["TMname"]! as AnyObject
            let tmpFb: AnyObject = self.TMFBdiff["fbAge"]! as AnyObject
            let tmpTM: AnyObject = self.TMFBdiff["TMage"]! as AnyObject
            if( (!(tmpFbName is NSNull)) && (!(tmpTMName is NSNull)) && (!(tmpFb is NSNull))
                && (!(tmpTM is NSNull)) ) {
                msg = msg.replacingOccurrences(of: "tmAge", with: self.TMFBdiff["TMage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbAge", with: self.TMFBdiff["fbAge"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "tmName", with: self.TMFBdiff["TMname"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbName", with: self.TMFBdiff["fbName"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }
        
//        if(mismatchReason == "Age mismatch"){
//            msg = self.fbMessages["age_mismatch"] as String
//            msg = msg.stringByReplacingOccurrencesOfString("tmAge", withString: self.TMFBdiff["TMage"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//            msg = msg.stringByReplacingOccurrencesOfString("fbAge", withString: self.TMFBdiff["fbAge"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//        }else if(mismatchReason == "Name mismatch"){
//            msg = self.fbMessages["name_mismatch"] as String
//            msg = msg.stringByReplacingOccurrencesOfString("tmName", withString: self.TMFBdiff["TMname"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//            msg = msg.stringByReplacingOccurrencesOfString("fbName", withString: self.TMFBdiff["fbName"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//        }else if(mismatchReason == "Name and Age mismatch"){
//            msg = self.fbMessages["name_age_mismatch"] as String
//            msg = msg.stringByReplacingOccurrencesOfString("tmAge", withString: self.TMFBdiff["TMage"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//            msg = msg.stringByReplacingOccurrencesOfString("fbAge", withString: self.TMFBdiff["fbAge"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//            msg = msg.stringByReplacingOccurrencesOfString("tmName", withString: self.TMFBdiff["TMname"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//            msg = msg.stringByReplacingOccurrencesOfString("fbName", withString: self.TMFBdiff["fbName"] as String, options: NSStringCompareOptions.LiteralSearch, range: nil)
//        }
        return msg
    }
    
    private func idMisMatchError(_ rejectReason:String, idMessages:NSDictionary, id_diff_data: NSDictionary) -> String
    {
        var msg:String = ""
        
        if(rejectReason == "age_mismatch"){
            msg = idMessages["age_mismatch"] as! String
            let tmpID: AnyObject = id_diff_data["IDage"]! as AnyObject
            let tmpTM: AnyObject = id_diff_data["TMage"]! as AnyObject
            if( (!(tmpID is NSNull)) && (!(tmpTM is NSNull)) ) {
                msg = msg.replacingOccurrences(of: "tmAge", with: id_diff_data["TMage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbAge", with: id_diff_data["IDage"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }else if(rejectReason == "name_mismatch"){
            msg = idMessages["name_mismatch"] as! String
            let tmpID: AnyObject = id_diff_data["IDname"]! as AnyObject
            let tmpTM: AnyObject = id_diff_data["TMname"]! as AnyObject
            if( (!(tmpID is NSNull)) && (!(tmpTM is NSNull)) ) {
                msg = msg.replacingOccurrences(of: "tmName", with: id_diff_data["TMname"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbName", with: id_diff_data["IDname"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }else if(rejectReason == "name_age_mismatch"){
            msg = idMessages["name_age_mismatch"] as! String
            let tmpFbName: AnyObject = id_diff_data["IDname"]! as AnyObject
            let tmpTMName: AnyObject = id_diff_data["TMname"]! as AnyObject
            let tmpFb: AnyObject = id_diff_data["IDage"]! as AnyObject
            let tmpTM: AnyObject = id_diff_data["TMage"]! as AnyObject
            
            if( self.isValidObject(param: tmpFbName) &&
                self.isValidObject(param: tmpTMName) &&
                self.isValidObject(param: tmpFb) &&
                self.isValidObject(param: tmpTM)) {
            
                msg = msg.replacingOccurrences(of: "tmAge", with: id_diff_data["TMage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbAge", with: id_diff_data["IDage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "tmName", with: id_diff_data["TMname"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbName", with: id_diff_data["IDname"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }
        return msg
    }
    
    private func idMismatchError(_ rejectReason:String)->String{
        var msg:String = ""
        
        if(rejectReason == "age_mismatch"){
            msg = self.idMessages["age_mismatch"] as! String
            let tmpID: AnyObject = self.id_diff_data["IDage"]! as AnyObject
            let tmpTM: AnyObject = self.id_diff_data["TMage"]! as AnyObject
            if( self.isValidObject(param: tmpID) && self.isValidObject(param: tmpTM) ) {
                msg = msg.replacingOccurrences(of: "tmAge", with: self.id_diff_data["TMage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbAge", with: self.id_diff_data["IDage"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }else if(rejectReason == "name_mismatch"){
            msg = self.idMessages["name_mismatch"] as! String
            let tmpID: AnyObject = self.id_diff_data["IDname"]! as AnyObject
            let tmpTM: AnyObject = self.id_diff_data["TMname"]! as AnyObject
            if( self.isValidObject(param: tmpID) && self.isValidObject(param: tmpTM) ) {
                msg = msg.replacingOccurrences(of: "tmName", with: self.id_diff_data["TMname"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbName", with: self.id_diff_data["IDname"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }else if(rejectReason == "name_age_mismatch"){
            msg = self.idMessages["name_age_mismatch"] as! String
            let tmpFbName: AnyObject = self.id_diff_data["IDname"]! as AnyObject
            let tmpTMName: AnyObject = self.id_diff_data["TMname"]! as AnyObject
            let tmpFb: AnyObject = self.id_diff_data["IDage"]! as AnyObject
            let tmpTM: AnyObject = self.id_diff_data["TMage"]! as AnyObject
            if(self.isValidObject(param: tmpFbName)
                && self.isValidObject(param: tmpTMName)
                && self.isValidObject(param: tmpFb)
                && self.isValidObject(param: tmpTM)) {
                msg = msg.replacingOccurrences(of: "tmAge", with: self.id_diff_data["TMage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbAge", with: self.id_diff_data["IDage"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "tmName", with: self.id_diff_data["TMname"] as! String, options: NSString.CompareOptions.literal, range: nil)
                msg = msg.replacingOccurrences(of: "fbName", with: self.id_diff_data["IDname"] as! String, options: NSString.CompareOptions.literal, range: nil)
            }
        }
        return msg
    }

    private func setEndorsementBlock() {
        self.endorseLink = self.endorsementObj["link"] as! String
        self.endorsementMsg = self.endorsementObj["endorsement_message"] as! String
        self.endorsementSuccessMsg = self.endorsementObj["endorsement_success_message"] as! String
        self.endorseData = self.endorsementObj["data"] as! NSArray
        self.endorseCount = self.endorsementObj["endorsement_threshold_count"] as! Int
        self.isEndorseVerified = self.endorsementObj["isVerified"] as! Bool
        
    }
    
    private func isValidObject(param:AnyObject) -> Bool {
        if(param is NSNull) {
            return false
        }
        return true
    }
    
}

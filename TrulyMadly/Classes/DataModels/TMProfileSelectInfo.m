//
//  TMProfileSelectInfo.m
//  TrulyMadly
//
//  Created by Ankit on 03/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMProfileSelectInfo.h"

@interface TMProfileSelectInfo ()

@property(nonatomic,assign)BOOL isSelectUser;
@property(nonatomic,strong)NSString* commonImageLink;
@property(nonatomic,strong)NSString* commonText;
@property(nonatomic,strong)NSString* defaultText;
@property(nonatomic,strong)NSString* quotesText;

@end


@implementation TMProfileSelectInfo

- (instancetype)initWithDictionary:(NSDictionary*)selectProfileData
{
    self = [super init];
    if (self) {
        if(selectProfileData && [selectProfileData isKindOfClass:[NSDictionary class]]) {
            self.isSelectUser = [selectProfileData[@"is_tm_select"] boolValue];
            self.commonImageLink = selectProfileData[@"common_image"];
            self.commonText = selectProfileData[@"common_string"];
            self.quotesText = selectProfileData[@"quote"];
        }
    }
    return self;
}

-(NSString*)defaultText {
    return @"Join TrulyMadly Select to see how compatible you are!";
}

-(NSString*)getCommonText {
    if(self.commonText && [self.commonText isKindOfClass:[NSString class]]) {
        return self.commonText;
    }
    else {
        return @"What an interesting match! Time to find out more about each other.";
    }
}
@end

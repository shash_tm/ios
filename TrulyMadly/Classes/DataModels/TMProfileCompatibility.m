//
//  TMProfileCompatibility.m
//  TrulyMadly
//
//  Created by Ankit on 24/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileCompatibility.h"
#import "TMCompatibilityQuiz.h"
#import "TMProfileLikes.h"
#import "TMEnums.h"


@interface TMProfileCompatibility ()

@property(nonatomic,assign)CGFloat commonLikeContentHeight;



@end

@implementation TMProfileCompatibility

-(instancetype)initWithMatchDetail:(NSDictionary*)matchDetail withCommonLikes:(NSArray*)commonLikes {
    self = [super init];
    if(self) {
        self.commonLikeContentHeight = 44;
        self.horizontalInterimMargin = 12;
        self.verticalTitleHeight = 25;
        
        self.valueQuiz = [[TMCompatibilityQuiz alloc] initWithQuizType:TMQUIZSCORETYPE_VALUE withQuizScore:0];
        self.adapQuiz = [[TMCompatibilityQuiz alloc] initWithQuizType:TMQUIZSCORETYPE_VALUE_ADAPTABILITY withQuizScore:0];
        if(matchDetail) {
            //value quiz score
            NSNumber *valueScore = [matchDetail objectForKey:@"values"];
            if([valueScore isKindOfClass:[NSNumber class]]) {
                self.valueQuiz.quizScore = valueScore.integerValue;
            }
            
            //adaptability quiz score
            NSNumber *adapScore = [matchDetail objectForKey:@"adaptability"];
            if([adapScore isKindOfClass:[NSNumber class]]) {
                self.adapQuiz.quizScore = adapScore.integerValue;
            }
        }
        
        self.commonLike = [[TMProfileLikes alloc] initWithCommonLikes:commonLikes];
    }
    return self;
}

-(CGFloat)compatibilityContentHeight:(CGFloat)maxWidth row:(NSInteger)row {
    CGFloat contentHeight = 0;
    if(row == 0) {
        [self configureIconWidth:maxWidth];
        contentHeight = self.iconWidth + self.verticalTitleHeight + self.headerHeight;
    }
    else if(row == 1) {
        contentHeight = self.commonLikeContentHeight;
    }
    return contentHeight;
}

-(void)configureIconWidth:(CGFloat)maxWidthAllowed {
    CGFloat numberOfImages = 3;
    CGFloat totalInterimMargin = self.horizontalInterimMargin * (CGFloat)(numberOfImages-1);
    CGFloat width = (maxWidthAllowed - totalInterimMargin)/numberOfImages;
    self.iconWidth = width;
}

////////////////////////////////////////////////
-(NSInteger)compabilityItemCount {
    return 3;
}

-(NSString*)valueHeaderText {
    NSString *text = @"On Values";
    return text;
}

-(UIFont*)valueTitleTextFont {
    UIFont *font = [self.valueQuiz titleTextFont];
    return font;
}

/////////////////////////////////////////////////
-(UIFont*)adaptibilityTitleTextFont{
    UIFont *font = [self.adapQuiz titleTextFont];
    return font;
}
-(NSString*)adaptibilityHeaderText {
    NSString *text = @"On Adaptability";
    return text;
}


///////////////////////////////////////////////
-(NSString*)commonLikeText {
    NSString *text = [self.commonLike commonLikeCountAsText];
    return text;
}

//-(TMProfileCollectionViewCellType)compRowType {
//    TMProfileCollectionViewCellType cellType = TMPROFILECOLLECTIONVIEWCELL_COMPATIBILITY;
//    return cellType;
//}

//-(TMProfileCollectionViewCellType)commonLikeRowType {
//    TMProfileCollectionViewCellType cellType = TMPROFILECOLLECTIONVIEWCELL_COMMONLIKELSIT;
//    return cellType;
//}

-(NSString*)compatibilityHeaderText{
    return @"Personality Match";
}

@end

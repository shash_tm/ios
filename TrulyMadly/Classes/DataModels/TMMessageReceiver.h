//
//  TMMessageReceiver.h
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMMessageReceiver : NSObject

@property(nonatomic,assign)BOOL isAdmin;
@property(nonatomic,strong)NSString *fName;
@property(nonatomic,strong)NSNumber *lastSeenMsgId;
@property(nonatomic,strong)NSString *lastSeenReceiverTs;
@property(nonatomic,assign)NSInteger userId;
@property(nonatomic,strong)NSString *profileLink;
@property(nonatomic,strong)NSString *profilePic;
@property(nonatomic,assign)NSInteger loggedInUserId;


-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end

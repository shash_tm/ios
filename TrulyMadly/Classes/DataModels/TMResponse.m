//
//  TMResponse.m
//  TrulyMadly
//
//  Created by Ankit on 01/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMResponse.h"

@interface TMResponse ()

@end


@implementation TMResponse

-(instancetype)init {
    self = [super init];
    if(self) {
        
    }
    return self;
}

-(instancetype)initWithResponseDictionary:(NSDictionary*)responseDictionary {
    self = [super init];
    if(self) {
        self.responseDictionary = responseDictionary;
    }
    return self;
}
-(instancetype)initWithResponseArray:(NSArray*)responseArray {
    self = [super init];
    if(self) {
        self.responseArray = responseArray;
    }
    return self;
}
-(instancetype)initWithResponse:(NSDictionary*)response {
    self = [super init];
    if(self) {
        id responseCode = [response objectForKey:@"responseCode"];
        self.serverResponseCode = [responseCode integerValue];
    }
    return self;
}

-(NSInteger)responseCode {
    NSInteger responseCode = [[self.responseDictionary objectForKey:@"responseCode"] integerValue];
    return responseCode;
}

-(id)data {
    return [self.responseDictionary objectForKey:@"data"];
}

-(id)myData {
    return [self.responseDictionary objectForKey:@"my_data"];
}

-(NSDictionary*)systemMessageDictionary {
    return [self.responseDictionary objectForKey:@"system_messages"];
}

-(NSString*)status {
    NSString *status = [self.responseDictionary objectForKey:@"status"];
    if(status) {
        return status;
    }
    else {
        return @"";
    }
}

-(NSDictionary*)response {
    return self.responseDictionary;
}

-(NSString*)error {
    NSString *error = [self.responseDictionary objectForKey:@"error"];
    if(error) {
        return error;
    }
    else {
        return @"";
    }
}

-(NSArray*)psychoQuestion {
    return [self.responseDictionary objectForKey:@"psycho_question"];
}


@end

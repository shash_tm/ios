//
//  TMProfileCollectionViewConfig.h
//  TrulyMadly
//
//  Created by Ankit on 04/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@interface TMProfileCollectionViewConfig : NSObject

@property(nonatomic,assign)TMProfileCollectionViewSectionType sectionType;
//@property(nonatomic,assign)TMProfileCollectionViewCellType cellType;
@property(nonatomic,assign)NSInteger rowCount;
@property(nonatomic,assign)NSInteger contentHeight;

-(TMProfileCollectionViewCellType)getCellTypeForRow:(NSInteger)row;

@end

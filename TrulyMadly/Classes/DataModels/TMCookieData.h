//
//  TMCookieData.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 02/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCookieData : NSObject

@property(nonatomic,strong)NSMutableDictionary* register_basics_data;

@end

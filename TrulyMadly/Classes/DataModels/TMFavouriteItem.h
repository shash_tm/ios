//
//  TMFavouriteItem.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMFavouriteItem : NSObject

@property(nonatomic,assign)BOOL status;
@property(nonatomic,strong,readonly)NSArray *favs;
@property(nonatomic,strong,readonly)NSString *activeImageStr;
@property(nonatomic,strong,readonly)NSString *inActiveImageStr;
@property(nonatomic,strong,readonly)NSString *disabledImageStr;
@property(nonatomic,strong,readonly)NSString *commonImageStr;

-(void)configureFavouriteItem:(NSArray*)favList withKey:(NSString*)key;

@end
//
//  QuizQuestions.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 10/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMQuizQuestions : NSObject

@property (nonatomic) NSString* quizImageURL;
@property (nonatomic) NSString* quizName;
@property (nonatomic) NSString* quizID;
@property (nonatomic) NSArray* questionList;

- (instancetype) initWithResponseDictionary:(NSDictionary*) responseDictionary;

@end

//
//  TMChatSeenContext.m
//  TrulyMadly
//
//  Created by Ankit on 03/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMChatSeenContext.h"
#import "TMUserSession.h"


@implementation TMChatSeenContext

-(instancetype)initWithChatSeenData:(NSDictionary*)data {
    self = [super init];
    if(self) {
        self.lastSeenMessageTimestamp = data[@"last_seen_msg_time"];
        self.lastSeenMessageId = data[@"last_seen_msg_id"];
        self.matchId = [data[@"match_id"] integerValue];
    }
    return self;
}

-(instancetype)initWithChatReadListenerData:(NSDictionary*)data {
    self = [super init];
    if(self) {
        self.lastSeenMessageTimestamp = data[@"tstamp"];
        self.lastSeenMessageId = data[@"last_seen_msg_id"];
        self.matchId = [data[@"receiver_id"] integerValue];
    }
    return self;
}

-(NSInteger)userId {
    NSInteger userId = [[TMUserSession sharedInstance].user.userId integerValue];
    return userId;
}

@end

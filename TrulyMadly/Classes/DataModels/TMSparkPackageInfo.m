//
//  TMSparkPackageInfo.m
//  TrulyMadly
//
//  Created by Ankit on 26/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkPackageInfo.h"

@interface TMSparkPackageInfo ()

@property(nonatomic,strong)NSString *packageTitle;
@property(nonatomic,strong)NSString *packageDescription;
@property(nonatomic,strong)NSString *packageTag;
@property(nonatomic,strong)NSString *packagePrice;
@property(nonatomic,strong)NSString *currencyType;
@property(nonatomic,assign)TMBuyPackTagType tagType;

@end

@implementation TMSparkPackageInfo

- (instancetype)initWithDictionary:(NSDictionary*)packageDictionary
{
    self = [super init];
    if (self) {
        NSString *sparkCount = packageDictionary[@"spark_count"];
        NSString *price = packageDictionary[@"price"];
        NSString *description = packageDictionary[@"description"];
        NSDictionary *metadata = packageDictionary[@"metadata"];
        NSString *currencyType = packageDictionary[@"currency"];
        NSString *tag = nil;
        NSString *discount = nil;
        NSString *matchGurantee = nil;
        if(metadata && [metadata isKindOfClass:[NSDictionary class]]) {
            tag = metadata[@"tag"];
            discount = metadata[@"discount"];
            matchGurantee = metadata[@"match_guarantee"];
        }
     
        NSString *titleText = [NSString stringWithFormat:@"Get %@ Sparks",sparkCount];
        //NSMutableAttributedString *packageTitle = [[NSMutableAttributedString alloc] initWithString:titleText];
        
        self.packageTitle = titleText;
        self.packagePrice = price;
        self.currencyType = currencyType;
        if(description && [description isKindOfClass:[NSString class]]) {
            self.packageDescription = description;
        }
        
        if(matchGurantee) {
            self.packageTag = @"MATCH GUARANTEE ";
            self.tagType = TMBUYPACK_TAG_MATCHGURANTEE;
        }
        else if(tag && [tag isKindOfClass:[NSString class]]) {
            self.packageTag = tag;
            self.tagType = TMBUYPACK_TAG_INFO;
        }
        else if(discount && [discount isKindOfClass:[NSNumber class]]) {
            self.packageTag = [NSString stringWithFormat:@"SAVE %@%%",discount];
            self.tagType = TMBUYPACK_TAG_DISCOUNT;
        }
    }
    return self;
}

@end

//
//  TMEditPreference.m
//  TrulyMadly
//
//  Created by Ankit on 13/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMEditPreference.h"
#import "TMUserDataController.h"
#import "TMUserSession.h"

@interface TMEditPreference ()

@property(nonatomic,strong)NSDictionary* demo;
@property(nonatomic,strong)NSDictionary* trait;
@property(nonatomic,strong)NSMutableArray* statesToSend;
@property(nonatomic,strong)NSMutableArray* citiesToSend;
@property(nonatomic,strong)NSMutableArray* regionsToSend;
@property(nonatomic,strong)NSMutableArray* citiesId;

@property(nonatomic,strong)NSArray* allStates;
@property(nonatomic,strong)NSArray* allCities;
@property(nonatomic,strong)NSArray* allRegions;

@property(nonatomic,strong)TMUserDataController* userDataController;

@end

@implementation TMEditPreference

- (instancetype)initWithEditPreferenceData:(NSDictionary*)editPreferenceData
{
    self = [super init];
    if (self) {
        self.regionToToDisplay = [NSMutableArray array];
        self.statesToDisplay = [NSMutableArray array];
        self.citiesToDisplay = [NSMutableArray array];
        self.states = [NSMutableArray array];
        self.cities = [NSMutableArray array];
        self.regions = [NSMutableArray array];
        self.citiesId = [NSMutableArray array];
        NSDictionary* basicData = editPreferenceData[@"basics_data"];
        self.basicDataURLString = basicData[@"url"];
        self.demo = editPreferenceData[@"demo"];
        self.trait = editPreferenceData[@"trait"];
        self.allRegions = editPreferenceData[@"active_countries"];
        self.showOutsideIndiaProfiles = [editPreferenceData[@"show_us_profiles"] boolValue];
        self.outsideProfileSwitchEnabled = [editPreferenceData[@"us_profiles_enabled"] boolValue];
        [self setAge];
        [self setHeight];
        
        NSArray* selectedRegions = editPreferenceData[@"selected_countries"];
        [self setRegionsWithSelectedRegions:selectedRegions];
    }
    return self;
}
-(TMUserDataController*)userDataController {
    if(!_userDataController) {
        _userDataController = [[TMUserDataController alloc] init];
    }
    return _userDataController;
}
-(void)loadStateAndCityData {
    [self setStates];
    [self setCities];
}
-(void)setHeight {
    NSString* startHeight = self.trait[@"start_height"];
    NSString* endHeight = self.trait[@"end_height"];
    if((startHeight) && (startHeight != nil) ) {
        self.startHeight = startHeight;
    }else {
        self.startHeight = @"48";
    }
    if(endHeight && (endHeight != nil) ) {
        self.endHeight = endHeight;
    }else {
        self.endHeight = @"95";
    }
    
    if([self.startHeight isEqualToString:@"48"] && [self.endHeight isEqualToString:@"48"]) {
        self.endHeight = @"50";
    }
    
    self.startHeightInFeetandInch = [self getHeightInFeetandInch:self.startHeight.integerValue];
    self.endHeightInFeetandInch = [self getHeightInFeetandInch:self.endHeight.integerValue];
}
-(void)setStates {
    [self.userDataController getCityList:^(NSArray * _Nonnull cityList) {
        self.allCities = cityList;
    }];
    [self.userDataController getStateList:^(NSArray * _Nonnull stateList) {
        self.allStates = stateList;
        
        if(self.states.count == 0) {
            [self createStateObj];
        }
        
        NSString* selectedStates = self.demo[@"states"];
        if(![selectedStates isEqualToString:@""]) {
            NSArray* statesArr = [selectedStates componentsSeparatedByString:@","];
            NSInteger length = statesArr.count;
            for(int i=0; i<length; i++) {
                NSString* stateString = statesArr[i];
                NSArray* states = [stateString componentsSeparatedByString:@"-"];
                NSString* stateId = states[1];
                NSString* stateName = [self getStateNameForStateId:stateId];
                if(![stateName isEqualToString:@""]) {
                    [self createCityObjForStateId:stateId];
                    [self.statesToDisplay addObject:stateName];
                }
            }
        }
    }];
}
-(void)setCities {
    NSString* selectedCities = self.demo[@"cities"];
    if(![selectedCities isEqualToString:@""]) {
        NSArray* cityArr = [selectedCities componentsSeparatedByString:@","];
        NSInteger length = cityArr.count;
        for(int i=0;i<length;i++) {
            NSString* cityString = cityArr[i];
            NSArray* city = [cityString componentsSeparatedByString:@"-"]; //countryid-stateid-cityid
            NSString* cityId = city[2];
            NSString* cityName = [self getCityNameForCityId:cityId];
            if(![cityName isEqualToString:@""]) {
                [self.citiesId addObject:cityId];
                [self.citiesToDisplay addObject:cityName];
            }
        }
    }
}
-(void)setRegionsWithSelectedRegions:(NSArray*)selectedRegions {
    if(self.regions.count == 0) {
        [self createRegionObj];
    }
    
    if(selectedRegions.count) {
        NSInteger length = selectedRegions.count;
        for(int i=0;i<length;i++) {
            NSString* regionId = selectedRegions[i];
            NSString* regionName = [self getRegionNameForRegionId:regionId];
            if(![regionName isEqualToString:@""]) {
                [self.regionToToDisplay addObject:regionName];
            }
        }
    }
}

-(NSString*)getStateNameForStateId:(NSString*)stateId {
    NSString *predicateString = [NSString stringWithFormat:@"%@ == '%@'", @"sid", stateId];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    NSArray *filteredResults = [self.states filteredArrayUsingPredicate:predicate];
    if(filteredResults.count == 1) {
        NSMutableDictionary* stateDict = filteredResults[0];
        stateDict[@"isSelected"] = @(TRUE);
        NSString* stateName = stateDict[@"name"];
        return stateName;
    }
    return @"";
}
-(NSString*)getCityNameForCityId:(NSString*)cityId {
    NSString *predicateString = [NSString stringWithFormat:@"%@ == '%@'", @"cityid", cityId];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    NSArray *filteredResults = [self.cities filteredArrayUsingPredicate:predicate];
    if(filteredResults.count == 1) {
        NSMutableDictionary* cityDict = filteredResults[0];
        cityDict[@"isSelected"] = @(TRUE);
        NSString* cityName = cityDict[@"name"];
        return cityName;
    }
    return @"";
}
-(NSString*)getRegionNameForRegionId:(NSString*)regionId {
    NSString *predicateString = [NSString stringWithFormat:@"%@ == '%@'", @"country_id", regionId];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    NSArray *filteredResults = [self.regions filteredArrayUsingPredicate:predicate];
    if(filteredResults.count == 1) {
        NSMutableDictionary* regionDict = filteredResults[0];
        regionDict[@"isSelected"] = @(TRUE);
        NSString* regionName = regionDict[@"name"];
        return regionName;
    }
    return @"";
}
-(void)createRegionObj {
    for(int i=0;i<self.allRegions.count;i++) {
        NSDictionary* regionData = self.allRegions[i];
        NSMutableDictionary* regionObj = [regionData mutableCopy];
        regionObj[@"isSelected"] = @(FALSE);
        [self.regions addObject:regionObj];
    }
}
-(void)createStateObj {
    for(int i=0;i<self.allStates.count;i++) {
        NSDictionary* stateData = self.allStates[i];
        NSMutableDictionary* stateObj = [stateData mutableCopy];
        stateObj[@"isSelected"] = @(FALSE);
        stateObj[@"name"] = stateData[@"sname"];
        [self.states addObject:stateObj];
    }
}
-(void)createCityObjForStateId:(NSString*)stateId {
    for(int i=0;i<self.allCities.count;i++) {
        NSDictionary* cityData = self.allCities[i];
        NSString* sid = cityData[@"sid"];
        if([sid isEqualToString:stateId]) {
            NSMutableDictionary* cityObj = [cityData mutableCopy];
            cityObj[@"isSelected"] = @(FALSE);
            cityObj[@"name"] = cityData[@"cityname"];
            [self.cities addObject:cityObj];
        }
    }
}
-(void)updateLocationSelectionState {
    [self updateStateSelectionState];
    [self updateCitiesSelectionState];
    [self updateRegionSelectionState];
}
-(void)updateStateSelectionState {
    self.statesToDisplay = [NSMutableArray array];
    self.cities = [NSMutableArray array];
    for(int i=0;i<self.states.count;i++) {
        NSDictionary* stateDict = self.states[i];
        BOOL isSelected = [stateDict[@"isSelected"] boolValue];
        if(isSelected) {
            [self createCityObjForStateId:stateDict[@"sid"]];
            [self.statesToDisplay addObject:stateDict[@"name"]];
        }
    }
}
-(void)updateCitiesSelectionState {
    self.citiesToDisplay = [NSMutableArray array];
    for (int i=0;i<self.citiesId.count;i++) {//to update existing selected city dictionary objects "isSelected" key
        [self getCityNameForCityId:self.citiesId[i]];
    }
    //update citties to display
    for(int i=0; i<self.cities.count;i++) {
        NSDictionary* tmp = self.cities[i];
        BOOL isSelected = [tmp[@"isSelected"] boolValue];
        if(isSelected) {
            [self.citiesToDisplay addObject:tmp[@"name"]];
        }
    }
}
-(void)updateRegionSelectionState {
    self.regionToToDisplay = [NSMutableArray array];
    for(int i=0;i<self.regions.count;i++) {
        NSDictionary* regionDict = self.regions[i];
        BOOL isSelected = [regionDict[@"isSelected"] boolValue];
        if(isSelected) {
            [self.regionToToDisplay addObject:regionDict[@"name"]];
        }
    }
}
-(void)setAge {
    NSString* startAge = self.demo[@"start_age"];
    NSString* endAge = self.demo[@"end_age"];
    if(startAge && (startAge != nil) ) {
        self.startAge = startAge;
    }
    else {
        self.startAge = @"18";
    }
    if(endAge && (endAge != nil) ) {
        self.endAge = endAge;
        if([self.startAge isEqualToString:@"18"] && [self.endAge isEqualToString:@"18"]) {
            self.endAge = @"19";
        }
    }
    else {
        self.endAge = @"70";
    }
}

-(NSDictionary*)getEditPrefSaveRequestfParams {
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithCapacity:8];
    params[@"update"] = @(true);
    params[@"lookingAgeStart"] = self.startAge;
    params[@"lookingAgeEnd"] = self.endAge;
    params[@"start_height"] = self.startHeight;
    params[@"end_height"] = self.endHeight;
    params[@"show_us_profiles"] = (self.showOutsideIndiaProfiles) ? @"true" : @"false";
    params[@"spouseCountry"] = [self getCountryId];
    params[@"spouseState"] = [self getStateToSend];
    params[@"spouseCity"] = [self getCityToSend];
    return params;
}
-(NSString*)getHeightInFeetandInch:(NSInteger)height {
    NSInteger feet = height/12;
    NSInteger inch = height%12;
    NSString* heightToReturn = [NSString stringWithFormat:@"%ld%@%ld%@",feet,@"'",inch,@"\""];
    return heightToReturn;
}

-(NSString*)getSelectedLocationCellHeaderText {
    return @"Showing you matches from: ";
}
-(NSString*)getSelectedCityText {
    NSString* text = (self.citiesToDisplay.count) ? [NSString stringWithFormat:@"%ld Cities Selected",self.citiesToDisplay.count]
                                                  : @"Doesn't Matter";
    return text;
}
-(NSString*)getSelectedCityJoinedText {
    NSString* joinedText = [self.citiesToDisplay componentsJoinedByString:@", "];
    return joinedText;
}
-(NSString*)getSelectedStateText {
    NSString* text = (self.statesToDisplay.count) ? [NSString stringWithFormat:@"%ld States Selected",self.statesToDisplay.count]
                                                  : @"Doesn't Matter";
    return text;
}
-(NSString*)getSelectedStateJoinedText {
    NSString* joinedText = [self.statesToDisplay componentsJoinedByString:@", "];
    return joinedText;
}
-(NSString*)getSelectedRegionText {
    NSString* text = (self.regionToToDisplay.count) ? [NSString stringWithFormat:@"%ld Regions Selected",self.regionToToDisplay.count]
                                                    : @"Doesn't Matter";
    return text;
}
-(NSString*)getSelectedRegionJoinedText {
    NSString* joinedText = [self.regionToToDisplay componentsJoinedByString:@", "];
    return joinedText;
}
-(NSString*)getStateToSend {
    self.statesToSend = [NSMutableArray array];
    for(int i=0; i< self.states.count; i++) {
        NSDictionary* tmp = self.states[i];
        BOOL isSelected = [tmp[@"isSelected"] boolValue];
        if(isSelected) {
            NSString* countryId = tmp[@"cid"];
            NSString* stateId = tmp[@"sid"];
            NSString* finalStateId = [NSString stringWithFormat:@"%@-%@",countryId,stateId];
            [self.statesToSend addObject:finalStateId];
        }
    }
    
    if(self.statesToSend.count>0) {
        return [self.statesToSend componentsJoinedByString:@","];
    }
    return @"";
}
-(NSString*)getRegionsToSend {
    self.regionsToSend = [NSMutableArray array];
    for(int i=0; i< self.regions.count; i++) {
        NSDictionary* tmp = self.regions[i];
        BOOL isSelected = [tmp[@"isSelected"] boolValue];
        if(isSelected) {
            NSString* countryId = tmp[@"country_id"];
            [self.regionsToSend addObject:countryId];
        }
    }
    
    if(self.regionsToSend.count == 0) {
        for(int i=0; i< self.allRegions.count; i++) {
            NSDictionary* tmp = self.allRegions[i];
            NSString* countryId = tmp[@"country_id"];
            [self.regionsToSend addObject:countryId];
        }
    }
    return [self.regionsToSend componentsJoinedByString:@","];
}
-(NSString*)getCityToSend {
    self.citiesToSend = [NSMutableArray array];
    for(int i=0; i<self.cities.count;i++) {
        NSDictionary* tmp = self.cities[i];
        BOOL isSelected = [tmp[@"isSelected"] boolValue];
        if(isSelected) {
            NSString* countryId = tmp[@"cid"];
            NSString* stateId = tmp[@"sid"];
            NSString* cityId = tmp[@"cityid"];
            NSString* finalCityId = [NSString stringWithFormat:@"%@-%@-%@",countryId,stateId,cityId];
            [self.citiesToSend addObject:finalCityId];
        }
    }
    
    if(self.citiesToSend.count>0) {
        return [self.citiesToSend componentsJoinedByString:@","];
    }
    return @"";
}
-(NSString*)getCountryId {
    NSString* countryId = nil;
    if([[TMUserSession sharedInstance].user isUserCountryIndia]) {
        countryId = [NSString stringWithFormat:@"%d",[TMUserSession sharedInstance].user.country];
    }
    else {
        countryId = [self getRegionsToSend];
    }
    
    return countryId;
}
- (void)didStateChangeWithKey:(NSString * _Nonnull)key index:(NSInteger)index {
    if([key isEqualToString:@"state"]) {
        if(index == -1) {
            self.states = [NSMutableArray array];
            self.statesToDisplay = [NSMutableArray array];
            [self createStateObj];
        }
        else {
            NSDictionary* stateDict = self.states[index];
            BOOL isSelected = [stateDict[@"isSelected"] boolValue];
            
            NSMutableDictionary* stateObj = [stateDict mutableCopy];
            stateObj[@"isSelected"] = @(!isSelected);
            self.states[index] = stateObj;
        }
    }
    else if([key isEqualToString:@"city"]) {
        if(index == -1) {
            self.citiesToDisplay = [NSMutableArray array];
            self.citiesId = [NSMutableArray array];
        }
        else {
            NSDictionary* tmp = self.cities[index];
            BOOL isSelected = [tmp[@"isSelected"] boolValue];
            NSMutableDictionary* cityObj = [tmp mutableCopy];
            cityObj[@"isSelected"] = @(!isSelected);
            self.cities[index] = cityObj;
            
            NSString* cityId = cityObj[@"cityid"];
            if(isSelected) {
                //remove city id from array
                NSInteger index = [self.citiesId indexOfObject:cityId];
                [self.citiesId removeObjectAtIndex:index];
            } else {
                // add city id in array
                [self.citiesId addObject:cityObj[@"cityid"]];
            }
        }
    }
    else if([key isEqualToString:@"region"]) {
        if(index == -1) {
            self.regions = [NSMutableArray array];
            self.regionToToDisplay = [NSMutableArray array];
            [self createRegionObj];
        }
        else {
            NSDictionary* tmp = self.regions[index];
            BOOL isSelected = [tmp[@"isSelected"] boolValue];
            NSMutableDictionary* regionObj = [tmp mutableCopy];
            regionObj[@"isSelected"] = @(!isSelected);
            self.regions[index] = regionObj;
        }
    }

    [self.delegate didChangePreference];
    
}

@end

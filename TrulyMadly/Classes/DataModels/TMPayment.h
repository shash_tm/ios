//
//  TMPayment.h
//  TrulyMadly
//
//  Created by Ankit on 29/09/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

@interface TMPayment : NSObject

@property(nonatomic,assign)TMPaymentState paymentState;
@property(nonatomic,assign)TMPaymentStatus paymentStatus;
@property(nonatomic,assign)TMPackageType packageType;
@property(nonatomic,assign)NSInteger sparkCount;
@property(nonatomic,strong)NSString *packageIdentifier;
@property(nonatomic,strong,readonly)NSString *errorDisplayText;
@property(nonatomic,strong,readonly)NSString *errorTrackingText;
@property(nonatomic,assign)BOOL showChatAssist;
@property(nonatomic,assign)BOOL isUserCancelledtransaction;

-(void)setPaymentFailureReason:(NSString*)failureReason;

@end

//
//  QuizQuestions.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 10/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizQuestions.h"

@implementation TMQuizQuestions

- (instancetype) initWithResponseDictionary:(NSDictionary*) responseDictionary
{
    if (responseDictionary) {
        if (self = [super init]) {
            self.quizID = [responseDictionary objectForKey:@"quiz_id"];
            self.quizImageURL = [responseDictionary objectForKey:@"image"];
            self.quizName = [responseDictionary objectForKey:@"name"];
            self.questionList = [responseDictionary objectForKey:@"questions"];
            
           // [self testMethodForImageOptions];
           // [self testMethodForTextOptions];
            return self;
        }
    }
    return nil;
}


- (void) testMethodForTextOptions
{
//   NSMutableArray* optionsArray = [[NSMutableArray alloc] initWithArray:self.questionList];
//  [optionsArray addObjectsFromArray:self.questionList];
//    self.questionList = [optionsArray copy];

    NSMutableArray* questionArray = [NSMutableArray new];
    
    for (int index = 0; index < self.questionList.count; index++) {
        
        NSMutableDictionary* questionsMutableDict = [[NSMutableDictionary alloc] initWithDictionary:((NSDictionary*)[self.questionList objectAtIndex:index])];
        
        NSMutableArray* optionArray = [[NSMutableArray alloc] initWithArray:[questionsMutableDict objectForKey:@"options"]];
        [optionArray addObjectsFromArray:optionArray];
        [questionsMutableDict setObject:optionArray forKey:@"options"];
        [questionArray insertObject:questionsMutableDict atIndex:index];
        
        //[self.questionList setob]
    }
    self.questionList = [questionArray copy];
}

- (void) testMethodForImageOptions
{
    NSMutableArray* questionArray = [NSMutableArray new];
    
    for (int index = 0; index < self.questionList.count; index++) {
        NSMutableDictionary* questionDictionary = [[NSMutableDictionary alloc] initWithDictionary:[self.questionList objectAtIndex:index]];
        
        NSMutableArray* optionArray = [NSMutableArray new];
        
        for (int innerIndex = 0; innerIndex < 4; innerIndex++) {
            [optionArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"A",@"option_id",@"http://t1.trulymadly.com/images/quiz/options/Travel_5_C.jpg",@"option_image", nil]];
        }
        
        [questionDictionary setObject:optionArray forKey:@"options"];
      
        [questionArray addObject:questionDictionary];
    }
    self.questionList = questionArray;
    
   // [[NSDictionary alloc] initWithObjectsAndKeys:@"A",@"option_id",@"http://t1.trulymadly.com/images/quiz/options/Travel_5_C.jpg",@"option_image", nil];
}

@end

//
//  TMSelectQuizResultInfo.m
//  TrulyMadly
//
//  Created by Ankit on 07/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizResultInfo.h"

@implementation TMSelectQuizResultInfo

- (instancetype)initWithQuizResultDictionary:(NSDictionary*)quizResultDictionary
{
    self = [super init];
    if (self) {
        self.matchOption = [quizResultDictionary[@"match_option_id"] integerValue];
        self.userOption = [quizResultDictionary[@"my_option_id"] integerValue];
        self.questionText = quizResultDictionary[@"question_text"];
    }
    return self;
}

-(UIColor*)getBoundaryColorForUser {
    if(self.userOption == 1) {
        return  [UIColor colorWithRed:255.0f/255.0f green:0 blue:114.0f/255.0f alpha:1];
    }
    else {
        return  [UIColor colorWithRed:66.0f/255.0f green:199.0f/255.0f blue:233.0f/255.0f alpha:1];
    };
}
-(UIColor*)getBoundaryColorForMatch {
    if(self.matchOption == 1) {
        return  [UIColor colorWithRed:255.0f/255.0f green:0 blue:114.0f/255.0f alpha:1];
    }
    else {
        return  [UIColor colorWithRed:66.0f/255.0f green:199.0f/255.0f blue:233.0f/255.0f alpha:1];
    };
}
-(BOOL)showConnectingView {
    if(self.matchOption == self.userOption) {
        return TRUE;
    }
    return FALSE;
}
-(UIColor*)connectingColor {
    if(self.matchOption == 1 && self.userOption ==1) {
        return  [UIColor colorWithRed:255.0f/255.0f green:0 blue:114.0f/255.0f alpha:1];
    }
    else {
        return  [UIColor colorWithRed:66.0f/255.0f green:199.0f/255.0f blue:233.0f/255.0f alpha:1];
    };
}
-(UIImage*)connectingImage {
    if(self.matchOption == 1 && self.userOption ==1) {
        return  [UIImage imageNamed:@"selectquizanswerlike"];
    }
    else {
        return  [UIImage imageNamed:@"selectquizhide"];
    }
}
-(UIImage*)getUserActionImage {
    if(self.userOption == 1) {
        return  [UIImage imageNamed:@"selectquizanswerlike"];
    }
    else {
        return  [UIImage imageNamed:@"selectquizhide"];
    }
}
-(UIImage*)getMatchActionImage {
    if(self.matchOption == 1) {
        return  [UIImage imageNamed:@"selectquizanswerlike"];
    }
    else {
        return  [UIImage imageNamed:@"selectquizhide"];
    }
}

@end

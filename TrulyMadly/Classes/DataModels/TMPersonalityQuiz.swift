//
//  TMPersonalityQuiz.swift
//  TrulyMadly
//
//  Created by Chetan Bhardwaj on 04/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

import UIKit

class TMPersonalityQuiz: NSObject {
    
    
    var question:NSArray!
    
    init(question:NSArray) {
        self.question = question
        super.init()
    }
   
    
    func questionAtIndex(_ index: Int) -> NSDictionary {
        return question[index] as! NSDictionary
    }
}

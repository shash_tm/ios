//
//  TMProfileCommonFavorites.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileItem.h"

@interface TMProfileCommonFavorites : TMProfileItem

@property(nonatomic,assign)BOOL hasCommonFavorites;
@property(nonatomic,strong)NSMutableArray* commonFav;

-(instancetype)initWithCommonfavouriteData:(NSDictionary*)favouriteData;
-(BOOL)isCommonFavoritesAvailable;
-(NSMutableArray*)getCommonFav;

@end

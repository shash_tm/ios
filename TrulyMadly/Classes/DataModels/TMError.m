//
//  TMError.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMError.h"


@implementation TMError

-(instancetype)initWithResponseCode:(TMApiResponseCode)responseCode {
    self = [super init];
    if(self) {
        if(responseCode == TMRESPONSECODE_LOGOUT) {
            self.errorCode = TMERRORCODE_LOGOUT;
            self.errorMessage = @"tm lougout";
        }else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
            self.errorCode = TMERRORCODE_DATASAVEERROR;
            self.errorMessage = @"tm data save error";
        }
        else if(responseCode == TMRESPONSECODE_SERVER_FALLBACKERROR) {
            self.errorCode = TMERRORCODE_UNKNOWNSERVERERROR;
            self.errorMessage = @"tm unknown server error";
        }
    }
    return self;
}

-(instancetype)initWithError:(NSError*)error {
    self = [super init];
    if(self) {
        self.errorCode = TMERRORCODE_UNKNOWNSERVERERROR;
        self.errorMessage = error.localizedDescription;
        
        if ([error.domain isEqualToString:@"NSURLErrorDomain"]) {
            NSInteger errorCode = error.code;
            
            if(errorCode == NSURLErrorNetworkConnectionLost || errorCode == NSURLErrorNotConnectedToInternet) {
                self.errorCode = TMERRORCODE_NONETWORK;
            }
            else if(errorCode == NSURLErrorTimedOut) {
                self.errorCode = TMERRORCODE_SERVERTIMEOUT;
            }
            else if(errorCode == NSURLErrorCannotConnectToHost) {
                self.errorCode = TMERRORCODE_HOSTNOTREACHABLE;
            }
            else if(errorCode == NSURLErrorBadServerResponse) {
                self.errorCode = TMERRORCODE_BADRESPONSE;
            }
        }
    }
    return self;
}

-(instancetype)initWithNoNetworkErrorCode {
    self = [super init];
    if(self) {
        self.errorCode = TMERRORCODE_NONETWORK;
    }
    return self;
}


@end

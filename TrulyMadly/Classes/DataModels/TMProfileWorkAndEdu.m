//
//  TMProfileWorkAndEdu.m
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileWorkAndEdu.h"
#import "NSString+TMAdditions.h"
//#import "TMStringUtil.h"
#import "TMEnums.h"


@interface TMProfileWorkAndEdu ()

@property(nonatomic,assign)CGFloat contentYPos;
@property(nonatomic,assign)CGFloat spaceBetweenIconAndText;
@property(nonatomic,assign)CGFloat spaceBetweenTwoSameSection;
@property(nonatomic,assign)CGFloat contentMaxWidth;
@property(nonatomic,assign)CGFloat contentMaxHeight;
@property(nonatomic,assign)CGFloat workContentHeight;
@property(nonatomic,assign)CGFloat educationContentHeight;
@property(nonatomic,assign)BOOL isWorkContentSingleLine;
@property(nonatomic,assign)BOOL isEduContentSingleLine;

@end


@implementation TMProfileWorkAndEdu

-(instancetype)initWithProfileData:(NSDictionary*)profileData {
    self = [super init];
    if(self) {
        [self setDefaults];
        
        ////////
        self.status = profileData[@"work_status"];
        NSString *designation = profileData[@"designation"];
        if([designation isValidObject]) {
            self.designation = [designation stringByTrimingWhitespace];
        }
        
        NSString *industry = profileData[@"industry"];
        if([industry isValidObject]) {
            self.industry = [industry stringByTrimingWhitespace];
        }
        
        //NSString *income = profileData[@"income"];
        //if([income isValidObject]) {
          ///  self.income = [income stringByTrimingWhitespace];
        //}
        
        self.companies = profileData[@"companies"];
        
        NSString *highestDegree = profileData[@"highest_degree"];
        if([highestDegree isValidObject]) {
            self.highestDegree = [highestDegree stringByTrimingWhitespace];
        }else {
            self.highestDegree = @"";
        }
        
        self.institutes = profileData[@"institutes"];
        
    }
    return self;
}

-(void)configureWorkDataForEdit {
//    if(self.isSelfEdit && self.income == nil) {
//        self.income = @"Add Income";
//    }
    if(self.companies.count==0 && self.isSelfEdit) {
        self.companies = @[@"Add Workplace"];
    }
    if(self.institutes.count==0 && self.isSelfEdit) {
        self.institutes = @[@"Add Institute"];
    }
}
-(void)setDefaults {
    self.spaceBetweenIconAndText = 14.0;
    self.spaceBetweenTwoSameSection = 4.0;
    self.spaceBetweenWorkAndEduSection = 15.0;
    self.isWorkContentSingleLine = FALSE;
    self.isEduContentSingleLine = FALSE;
    self.workSectionHeight = 0;
    self.educationSectionHeight = 0;
    self.contentMaxWidth = 0;
    self.contentMaxHeight = 0;
}

-(BOOL)isWorkDataAvailable {
    if([self.status isEqualToString:@"yes"]) {
        return true;
    }
    else {
        return false;
    }
}

-(NSString*)workAndEducationHeaderText {
    return @"Work & Education";
}

-(TMProfileCollectionViewCellType)workAndEduRowType {
    TMProfileCollectionViewCellType cellType = TMPROFILECOLLECTIONVIEWCELL_WORKANDEDU;
    return cellType;
}

-(CGFloat)workAndEducationContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width  {
    self.contentYPos = 0;
    self.iconWidth = 24;
    CGFloat editModeWidthOffset = (self.editMode) ? 25 : 0;
    self.contentMaxWidth = maxWidth - (self.iconWidth + self.spaceBetweenIconAndText + editModeWidthOffset);
    CGFloat height = [self contentHeight:maxWidth];
    height += self.headerHeight;
    return height;
}

-(CGFloat)contentHeight:(CGFloat)maxWidth {
    CGFloat contentHeight = 0;
    self.workSectionHeight = [self getWorkContentHeight];
    
    ///////education section////////////////
    self.educationSectionHeight = [self getEducationContentHeight];
    
    ////////total height///////
    contentHeight = self.workSectionHeight + self.spaceBetweenWorkAndEduSection + self.educationSectionHeight;
    //self.contentMaxHeight = contentHeight;
    return contentHeight;
}


-(CGFloat)getWorkContentHeight {
    CGFloat workContentHeightTemp = 0;
    
    CGFloat h1 = [self calculateSizeForContnetArray:[self workDescriptionTexts] withFont:[self titleTextFont] withBlock:nil].height;
    workContentHeightTemp += h1;
    
    CGFloat h2 = [self calculateSizeForContnetArray:self.companies withFont:[self subtitleTextFont] withBlock:nil].height;
    
    workContentHeightTemp += h2 + self.spaceBetweenTwoSameSection;
    self.workContentHeight = workContentHeightTemp;
    
    if(workContentHeightTemp < self.iconWidth) { //as minimum height need to be icon height
        //as icon width and height are same
        workContentHeightTemp = self.iconWidth;
        self.isWorkContentSingleLine = TRUE;
    }
    
    return workContentHeightTemp;
}
-(CGFloat)getEducationContentHeight {
    CGFloat workContentHeightTemp = 0;
    
    CGFloat h1 = [self calculateSizeForContnetArray:@[self.highestDegree] withFont:[self titleTextFont] withBlock:nil].height;
    workContentHeightTemp += h1;
    
    CGFloat h2 = [self calculateSizeForContnetArray:self.institutes withFont:[self subtitleTextFont] withBlock:nil].height;
    
    workContentHeightTemp += h2 + self.spaceBetweenTwoSameSection;
    self.educationContentHeight = workContentHeightTemp;
    
    if(workContentHeightTemp < self.iconWidth) { //as minimum height need to be icon height
        //as icon width and height are same
        workContentHeightTemp = self.iconWidth;
        self.isEduContentSingleLine = TRUE;
    }

    return workContentHeightTemp;
}

-(CGFloat)getIconWidth {
    return self.iconWidth;
}

-(BOOL)isAlertShown {
    if(/*(self.income == nil || [self.income isEqualToString:@"Add Income"]) || */(self.companies.count==1 && [self.companies[0] isEqualToString:@"Add Workplace"]) || (self.institutes.count==1 && [self.institutes[0] isEqualToString:@"Add Institute"])) {
        return TRUE;
    }
    return FALSE;
}

-(void)workIconLayoutFrame:(void(^)(CGRect frame))resultBlock1
workDescriptionLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock2
      companiesLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock3 {
    
    //////
    CGRect iconRect = CGRectMake(0, 0, [self getIconWidth], [self getIconWidth]);
    resultBlock1(iconRect);
    
    ///////
    self.contentYPos =(self.isWorkContentSingleLine) ? [self getWorkContentYPosForSingleLineText] : 0;
    NSArray *workDescriptionTexts = [self workDescriptionTexts];
    CGSize size = [self calculateSizeForContnetArray:workDescriptionTexts withFont:[self titleTextFont] withBlock:resultBlock2];

    self.contentYPos = size.height +  self.spaceBetweenTwoSameSection;
    [self calculateSizeForContnetArray:self.companies withFont:[self subtitleTextFont] withBlock:resultBlock3];
}
-(void)eduIconLayoutFrame:(void(^)(CGRect frame))resultBlock1
eduDescriptionLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock2
      instituesLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock3 {
    
    //////
    CGRect iconRect = CGRectMake(0, 0, [self getIconWidth], [self getIconWidth]);
    resultBlock1(iconRect);
    
    ///////
    self.contentYPos =(self.isEduContentSingleLine) ? [self getEduContentYPosForSingleLineText] : 0;
    CGSize size = [self calculateSizeForContnetArray:@[self.highestDegree] withFont:[self titleTextFont] withBlock:resultBlock2];
    
    self.contentYPos = size.height +  self.spaceBetweenTwoSameSection;
    [self calculateSizeForContnetArray:self.institutes withFont:[self subtitleTextFont] withBlock:resultBlock3];
}

-(UIFont*)titleTextFont {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
}
-(UIFont*)subtitleTextFont {
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
}
-(NSMutableArray*)workDescriptionTexts {
    NSMutableArray *texts = [NSMutableArray array];
    if(self.designation) {
        [texts addObject:self.designation];
    }
    else {
        [texts addObject:@"Not Working"];
    }
    if(self.industry) {
        [texts addObject:self.industry];
    }
    if(self.income) {
        [texts addObject:self.income];
    }
    return texts;
}
-(NSString*)contentFromArray:(NSArray*)texts forIndex:(NSInteger)index {
    NSString *content = texts[index];
    if( (texts.count != 1) &&  (index<texts.count-1)) {
        content = [NSString stringWithFormat:@"%@,",content];
    }
    return content;
}
-(CGSize)sizeForContent:(NSString*)content withFont:(UIFont*)font {
    NSDictionary *attributes = @{NSFontAttributeName:font};
    CGSize constrintSize = CGSizeMake(self.contentMaxWidth, NSUIntegerMax);
    CGRect rect = [content boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    return rect.size;
}
-(CGFloat)getWorkContentYPosForSingleLineText {
    return (self.iconWidth - self.workContentHeight)/2;
}
-(CGFloat)getEduContentYPosForSingleLineText {
    return (self.iconWidth - self.educationContentHeight)/2;
}
-(CGFloat)getTextMinXPos {
    return (self.iconWidth + self.spaceBetweenIconAndText);
}

-(CGSize)calculateSizeForContnetArray:(NSArray*)contents
                             withFont:(UIFont*)font
                            withBlock:(void(^)(CGRect frame, NSString *content))resultBlock {
    if(!contents){
        assert(@"text array nil");
    }

    CGFloat xPos = [self getTextMinXPos];
    CGFloat yPos = self.contentYPos;
    CGFloat width = 0;
    CGFloat height = 0;
    CGFloat widthWithSpace = 0;
    CGFloat heightWithSpace = 0;
    CGFloat prevHeight = 0;
    
    for (int i=0; i<contents.count;i++){
        NSString *content = [self contentFromArray:contents forIndex:i];
        NSString *text = content;
        if (i != 0) {
            text = [NSString stringWithFormat:@" %@",content];
        }
        
        width = [self sizeForContent:content withFont:font].width;
        height = [self sizeForContent:content withFont:font].height;
        
        widthWithSpace = [self sizeForContent:text withFont:font].width;
        heightWithSpace = [self sizeForContent:text withFont:font].height;
        
        CGFloat textLayoutRectXMax = xPos + width;
        if(textLayoutRectXMax > self.contentMaxWidth) {
            xPos = [self getTextMinXPos];
            yPos = yPos + prevHeight;
        }else {
            textLayoutRectXMax = xPos + widthWithSpace;
            if(textLayoutRectXMax > self.contentMaxWidth) {
                xPos = [self getTextMinXPos];
                yPos = yPos + prevHeight;
                width = widthWithSpace;
                height = heightWithSpace;
            }else {
                content = text;
                width = widthWithSpace;
                height = heightWithSpace;
            }
        }
        
        if(resultBlock) {
            resultBlock(CGRectMake(xPos, yPos, width, height),content);
        }
        
        prevHeight = height;
        xPos = xPos + width;
    }
    
    CGSize finalsize = CGSizeMake(self.contentMaxWidth, (yPos + height));
    return finalsize;
}


@end


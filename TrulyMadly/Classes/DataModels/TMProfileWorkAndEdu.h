//
//  TMProfileWorkAndEdu.h
//  TrulyMadly
//
//  Created by Ankit on 13/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMProfileItem.h"
//#import <UIKit/UIKit.h>

@interface TMProfileWorkAndEdu : TMProfileItem

@property(nonatomic,strong)NSString* status;
@property(nonatomic,strong)NSString* designation;
@property(nonatomic,strong)NSString* industry;
@property(nonatomic,strong)NSString* income;
@property(nonatomic,strong)NSArray* companies;
@property(nonatomic,strong)NSString* highestDegree;
@property(nonatomic,strong)NSArray* institutes;
@property(nonatomic,assign)CGFloat iconWidth;
@property(nonatomic,assign)CGFloat spaceBetweenWorkAndEduSection;
@property(nonatomic,assign)CGFloat workSectionHeight;
@property(nonatomic,assign)CGFloat educationSectionHeight;
@property(nonatomic,assign)BOOL isSelfEdit;

-(instancetype)initWithProfileData:(NSDictionary*)profileData;
-(CGFloat)workAndEducationContentHeight:(CGFloat)maxWidth iconWidth:(CGFloat)width;
-(NSString*)workAndEducationHeaderText;
-(CGFloat)getIconWidth;
-(BOOL)isAlertShown;
-(void)configureWorkDataForEdit;
-(UIFont*)titleTextFont;
-(UIFont*)subtitleTextFont;

-(void)workIconLayoutFrame:(void(^)(CGRect frame))resultBlock1
workDescriptionLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock2
      companiesLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock3;

-(void)eduIconLayoutFrame:(void(^)(CGRect frame))resultBlock1
eduDescriptionLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock2
     instituesLayoutFrame:(void(^)(CGRect frame, NSString *content))resultBlock3;

@end

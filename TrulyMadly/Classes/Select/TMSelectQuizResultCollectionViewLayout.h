//
//  TMSelectQuizResultCollectionViewLayout.h
//  TrulyMadly
//
//  Created by Ankit on 06/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectQuizResultCollectionViewLayoutDataSource;

@interface TMSelectQuizResultCollectionViewLayout : UICollectionViewFlowLayout

@property (nonatomic, weak) id<TMSelectQuizResultCollectionViewLayoutDataSource> dataSource;

@end

@protocol TMSelectQuizResultCollectionViewLayoutDataSource <NSObject>

-(CGSize)contentSizeForIndex:(NSInteger)index;
-(CGSize)headerSizeForSection:(NSInteger)section;

@end

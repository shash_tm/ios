//
//  TMSelectPostPaymentView.h
//  TrulyMadly
//
//  Created by Ankit on 10/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectPostPaymentViewDelegate;

@interface TMSelectPostPaymentView : UIView

@property(nonatomic,weak)id<TMSelectPostPaymentViewDelegate> delegate;
-(void)showDataWithDescriptionText:(BOOL)showDescriptionText;

@end


@protocol TMSelectPostPaymentViewDelegate <NSObject>

-(void)didClickToLetsBeginButton;


@end

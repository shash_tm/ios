//
//  TMSelectPackView.m
//  TrulyMadly
//
//  Created by Ankit on 25/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectPackView.h"
#import "UIColor+TMColorAdditions.h"
#import "NSString+TMAdditions.h"

@interface TMSelectPackView ()

@property(nonatomic,strong)UIView* contentView;
@property(nonatomic,strong)UILabel* tagLabel;
@property(nonatomic,strong)UILabel* priceLabel;
@property(nonatomic,strong)UILabel* durationLabel;
@property(nonatomic,strong)UILabel* buyLabel;
@property(nonatomic,strong)UILabel* priceSubTextLabel;

@end


@implementation TMSelectPackView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        CALayer *layer = self.layer;
        layer.cornerRadius = 8.0;
        layer.shadowOffset = CGSizeMake(1, 1);
        layer.shadowColor = [[UIColor blackColor] CGColor];
        layer.shadowRadius = 4.0f;
        layer.shadowOpacity = 0.70f;
        layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
        
        self.contentView = [[UIView alloc] initWithFrame:self.bounds];
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.contentView.clipsToBounds = TRUE;
        self.contentView.layer.cornerRadius = 8;
        [self addSubview:self.contentView];
        
        self.tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), 20)];
        self.tagLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        self.tagLabel.textColor = [UIColor whiteColor];
        self.tagLabel.textAlignment = NSTextAlignmentCenter;
        self.tagLabel.layer.cornerRadius = 8.0;
        [self.contentView addSubview:self.tagLabel];
        
        CGFloat buyLabelHeight = 30;
        self.buyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-buyLabelHeight,
                                                                  CGRectGetWidth(frame), buyLabelHeight)];
        self.buyLabel.backgroundColor = [UIColor likeColor];
        self.buyLabel.textAlignment = NSTextAlignmentCenter;
        self.buyLabel.textColor = [UIColor whiteColor];
        self.buyLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
        self.buyLabel.text = @"BUY";
        self.buyLabel.layer.cornerRadius = 8.0;
        [self.contentView addSubview:self.buyLabel];
        
        CGFloat topAndBottomLabelHeight = CGRectGetHeight(self.tagLabel.frame) + CGRectGetHeight(self.buyLabel.frame);
        CGFloat remainingHeight = (CGRectGetHeight(self.frame) - topAndBottomLabelHeight);
        CGFloat priceAndDurationHeight = 65;
        CGFloat priceLabelYPos = (remainingHeight - priceAndDurationHeight)/2;
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tagLabel.frame) + priceLabelYPos,
                                                                    CGRectGetWidth(frame), 30)];
        self.priceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:24];
        self.priceLabel.textColor = [UIColor blackColor];
        self.priceLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.priceLabel];
        
        
        self.durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.priceLabel.frame), CGRectGetWidth(frame), 13)];
        self.durationLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
        self.durationLabel.textColor = [UIColor blackColor];
        self.durationLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.durationLabel];
        
        self.priceSubTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.durationLabel.frame) + 10, CGRectGetWidth(frame), 12)];
        self.priceSubTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        self.priceSubTextLabel.textColor = [UIColor blackColor];
        self.priceSubTextLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.priceSubTextLabel];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

-(void)setPackPrice:(NSString*)price packDuration:(NSString*)duration priceSubText:(NSString*)subText currency:(NSString *)currency{
    
    if(subText != nil) {
        self.priceLabel.text = [NSString stringWithFormat:@"%@%@", currency, price];
        self.priceSubTextLabel.text = subText;
    }else {
        self.priceLabel.text = [NSString stringWithFormat:@"₹%@",price];
        self.priceSubTextLabel.text = @"";
    }
    self.durationLabel.text = duration;

}

-(void)setPackTag:(NSString*)tag withBgColor:(UIColor*)bgColor {
    self.tagLabel.text = tag;
    self.tagLabel.backgroundColor = bgColor;
}
-(void)tapAction {
    [self.delegate didTapSelectPackAtIndex:self.index];
}

@end


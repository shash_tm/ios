//
//  TMSelectPackBuyView.m
//  TrulyMadly
//
//  Created by Ankit on 25/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectPackBuyView.h"
#import "TMPackageInfo.h"
#import "TMSelectPackView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMUserSession.h"

#define ACTIONVIEW_HEIGHT 180

@interface TMSelectPackBuyView ()<UIScrollViewDelegate,TMSelectPackViewDelegate>

@property(nonatomic,strong)UIView* actionView;
@property(nonatomic,strong)UIScrollView *contentScrollView;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIImageView *selectImageView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *subtitleLabel;
@property(nonatomic,strong)UIPageControl* pageControl;
@property(nonatomic,strong)UILabel *footerLabel;
@property(nonatomic,strong)UIButton *actionButton;

@end

@implementation TMSelectPackBuyView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGFloat maxWidth = CGRectGetWidth(frame);
       
        [self setupContentView];
        
        self.actionView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                   CGRectGetHeight(frame) - ACTIONVIEW_HEIGHT,
                                                                   maxWidth,
                                                                   ACTIONVIEW_HEIGHT)];
        self.actionView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.actionView];
    }
    return self;
}

-(void)setupContentView {
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    
    self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,
                                                                            maxWidth, CGRectGetHeight(self.frame) - ACTIONVIEW_HEIGHT)];
    self.contentScrollView.showsVerticalScrollIndicator = FALSE;
    [self addSubview:self.contentScrollView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, maxWidth, 25)];
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    self.titleLabel.textColor = [UIColor colorWithRed:126/255.0f green:38/255.0f blue:135/255.0f alpha:1];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text  = @"Introducing";
    [self.contentScrollView addSubview:self.titleLabel];
    
    CGFloat selectImageViewWidth = 180;
    CGFloat selectImageViewHeight = 90;
    CGFloat selectImageViewXPos = (maxWidth - selectImageViewWidth)/2;
    CGFloat selectImageViewYPos = CGRectGetMaxY(self.titleLabel.frame)+ 15;
    CGRect selectImageViewRect = CGRectMake(selectImageViewXPos, selectImageViewYPos, selectImageViewWidth, selectImageViewHeight);
    self.selectImageView = [[UIImageView alloc] initWithFrame:selectImageViewRect];
    self.selectImageView.image = [UIImage imageNamed:@"selectunit"];
    [self.contentScrollView addSubview:self.selectImageView];
    
    
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.selectImageView.frame)+10, maxWidth, 30)];
    self.subtitleLabel.textColor = [UIColor colorWithRed:126/255.0f green:38/255.0f blue:135/255.0f alpha:1];
    self.subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
    self.subtitleLabel.text  = @"The perfect way to find ‘The One’";
    [self.contentScrollView addSubview:self.subtitleLabel];

    CGFloat scrollViewYPos = CGRectGetMaxY(self.subtitleLabel.frame) + 20;
    CGFloat scrollViewHeight = 210;//CGRectGetHeight(self.contentScrollView.frame) - (scrollViewYPos + 80);
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, scrollViewYPos, maxWidth, scrollViewHeight)];
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.showsHorizontalScrollIndicator = FALSE;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.pagingEnabled = TRUE;
    [self.contentScrollView addSubview:self.scrollView];
    
    NSArray *paginationImages = @[@"selectbuypagination1",@"selectbuypagination2",@"selectbuypagination3"];
    NSArray *paginationText = @[@"Ready for long term commitment?",
                                @"Take our relationship quiz",
                                @"Match you with someone who thinks alike!"];
    CGFloat paginationXPos = 0;
    NSInteger index = 0;
    for (NSString *imageName in paginationImages) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(paginationXPos, 0, maxWidth, scrollViewHeight)];
        [self.scrollView addSubview:view];
        
        CGFloat xPos = 8;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, 0, maxWidth - (2*xPos), 180)];
        imageView.image = [UIImage imageNamed:imageName];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.clipsToBounds = TRUE;
        [view addSubview:imageView];
        
        UILabel* descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, CGRectGetMaxY(imageView.frame)+10,
                                                                              maxWidth - (2*xPos), 20)];
        descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        descriptionLabel.textColor = [UIColor colorWithRed:126/255.0f green:38/255.0f blue:135/255.0f alpha:1];
        descriptionLabel.textAlignment = NSTextAlignmentCenter;
        descriptionLabel.text = paginationText[index++];
        [view addSubview:descriptionLabel];
        
        paginationXPos = paginationXPos + maxWidth;
    }
    self.scrollView.contentSize = CGSizeMake(paginationXPos, scrollViewHeight);
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake((maxWidth-60)/2,
                                                                       CGRectGetMaxY(self.scrollView.frame)+5, 60, 20)];
    self.pageControl.numberOfPages = 3;
    self.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1];
    [self.contentScrollView addSubview:self.pageControl];
    
    self.contentScrollView.contentSize =CGSizeMake(CGRectGetWidth(self.contentScrollView.frame), CGRectGetMaxY(self.pageControl.frame)+20);
    
}
-(void)showSelectPacks:(NSArray*)selectPacks {
    if(selectPacks.count == 1) {
        NSDictionary *packageDictionary = selectPacks[0];
        TMPackageInfo *packageInfo = [[TMPackageInfo alloc] initWithDictionary:packageDictionary];
        [self showFreeTrialPack:packageInfo];
    }
    else {
        [self showPaidPacks:selectPacks];
    }
}
-(void)showPaidPacks:(NSArray*)selectPacks {
    NSInteger totalSelectPacks = selectPacks.count;
    CGFloat xPos = 16;
    CGFloat xSpaceBetweenTwoPacks = 14;
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    CGFloat availableWidth = maxWidth - ((2*xPos) + (xSpaceBetweenTwoPacks * (totalSelectPacks-1)));
    CGFloat packViewWidth = availableWidth / totalSelectPacks;
    for (int packCount=0; packCount<totalSelectPacks; packCount++) {
        NSDictionary *packageDictionary = selectPacks[packCount];
        TMPackageInfo *packageInfo = [[TMPackageInfo alloc] initWithDictionary:packageDictionary];
        
        CGFloat selectPackViewHeight = (packageInfo.isMostPopular) ? 140 : 124;
        CGFloat selectPackViewYPos = (CGRectGetHeight(self.actionView.frame) - selectPackViewHeight)/2;
        TMSelectPackView *selectPackView = [[TMSelectPackView alloc] initWithFrame:CGRectMake(xPos,
                                                                                              selectPackViewYPos,
                                                                                              packViewWidth, selectPackViewHeight)];
        selectPackView.index = packCount;
        selectPackView.delegate = self;
        [self.actionView addSubview:selectPackView];
        
        NSInteger zIndex = (packCount == 0) ? MAXFLOAT : 100;
        selectPackView.layer.zPosition = zIndex;
        
        [selectPackView setPackPrice:packageInfo.packagePrice packDuration:packageInfo.packageDuration priceSubText:packageInfo.priceSubText currency:packageInfo.currency];
        [selectPackView setPackTag:packageInfo.packageTag withBgColor:[packageInfo selectTagLabelBgColor]];
        
        xPos = xPos + packViewWidth + xSpaceBetweenTwoPacks;
    }
}
-(void)showFreeTrialPack:(TMPackageInfo*)packageInfo {
    CGFloat buttonWidth = 200;
    CGFloat buttonHeight = 50;
    CGFloat footerLabelHeight = 20;
    CGFloat buttonYPos = (CGRectGetHeight(self.actionView.frame) - (buttonHeight+footerLabelHeight))/2;
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = CGRectMake((maxWidth - buttonWidth)/2, buttonYPos, buttonWidth, buttonHeight);
    self.actionButton.backgroundColor = [UIColor likeColor];
    self.actionButton.layer.cornerRadius = 10;
    [self.actionButton addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    [self.actionButton setTitle:[packageInfo selectPackageTitle] forState:UIControlStateNormal];
    [self.actionView addSubview:self.actionButton];
    
    self.footerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.footerLabel.frame = CGRectMake(0, CGRectGetMaxY(self.actionButton.frame)+10, maxWidth, footerLabelHeight);
    self.footerLabel.text = packageInfo.freeTrialText;
    self.footerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    self.footerLabel.textColor = [UIColor colorWithRed:255.0f/255.0f green:0 blue:0 alpha:1];
    self.footerLabel.textAlignment = NSTextAlignmentCenter;
    [self.actionView addSubview:self.footerLabel];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}

-(void)buyAction {
    [self.delegate didBuySelectPackAtIndex:0];
}

-(void)didTapSelectPackAtIndex:(NSInteger)index {
    [self.delegate didBuySelectPackAtIndex:index];
}

@end

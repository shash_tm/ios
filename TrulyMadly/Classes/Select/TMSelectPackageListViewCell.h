//
//  TMSelectPackageListViewCell.h
//  TrulyMadly
//
//  Created by Suruchi Sinha on 1/13/17.
//  Copyright © 2017 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMPackageInfo.h"

@interface TMSelectPackageListViewCell : UITableViewCell
- (void)configureCellWithData:(TMPackageInfo *)packageData;
@end

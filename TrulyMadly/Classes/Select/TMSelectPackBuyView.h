//
//  TMSelectPackBuyView.h
//  TrulyMadly
//
//  Created by Ankit on 25/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectPackBuyViewDelegate;
@class TMPackageInfo;

@interface TMSelectPackBuyView : UIView

@property(nonatomic,weak)id<TMSelectPackBuyViewDelegate> delegate;

-(void)showSelectPacks:(NSArray*)selectPacks;

@end

@protocol TMSelectPackBuyViewDelegate <NSObject>

-(void)didBuySelectPackAtIndex:(NSInteger)selectedIndex;

@end

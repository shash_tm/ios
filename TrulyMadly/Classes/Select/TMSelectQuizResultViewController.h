//
//  TMSelectQuizResultViewController.h
//  TrulyMadly
//
//  Created by Ankit on 03/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"

@interface TMSelectQuizResultViewController : TMBaseViewController

- (instancetype)initWithMatchId:(NSInteger)matchId matchImageURL:(NSURL*)imageURL
                    headerTitle:(NSString*)headerTitle
                 headerSubTitle:(NSString*)subTitle;

@end

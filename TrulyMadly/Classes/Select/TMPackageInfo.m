//
//  TMPackageInfo.m
//  TrulyMadly
//
//  Created by Ankit on 11/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMPackageInfo.h"

@interface TMPackageInfo ()

@property(nonatomic,strong)NSString *packageIdentifier;
@property(nonatomic,strong)NSString *packageTitle;
@property(nonatomic,strong)NSString *packageDescription;
@property(nonatomic,strong)NSString *packageTag;
@property(nonatomic,strong)NSString *packagePrice;
@property(nonatomic,strong)NSString *packageDuration;
@property(nonatomic,strong)NSString *priceSubText;
@property(nonatomic,strong)NSString *durationText;
@property(nonatomic,strong)NSString *currency;
@property(nonatomic,strong)NSString *price;
@property(nonatomic,strong)NSString *freeTrialText;
@property(nonatomic,strong)NSString *freeCtaText;
@property(nonatomic,assign)TMPackagePriceType packageType;
@property(nonatomic,assign)TMBuyPackTagType tagType;
@property(nonatomic,assign)BOOL isMostPopular;
@property(nonatomic,assign)BOOL isMatchGuarantee;
@end

@implementation TMPackageInfo

- (instancetype)initWithDictionary:(NSDictionary*)packageDictionary
{
    self = [super init];
    if (self) {
        NSString *description = packageDictionary[@"description"];
        NSDictionary *metadata = packageDictionary[@"metadata"];
        NSString *tag = nil;
        NSString *packageDuration = nil;
        NSString *price = nil;
        int discount = 0;
        
        self.currency = packageDictionary[@"currency"];
        self.price = packageDictionary[@"price"];
        if(metadata && [metadata isKindOfClass:[NSDictionary class]]) {
            tag = metadata[@"tag"];
            discount = (metadata[@"discount"] == [NSNull null]) ? 0 : [metadata[@"discount"] intValue];
            self.isMostPopular = [metadata[@"most_popular"] boolValue];
            self.priceSubText = metadata[@"sub_text"];
            self.freeTrialText = metadata[@"free_trial_expiry_text"];
            packageDuration = metadata[@"per_unit"];
            price = metadata[@"price_per_unit"];
            self.isMatchGuarantee = [metadata[@"match_guarantee"] boolValue];
            self.freeCtaText = metadata[@"free_trial_cta_text"];
        }
        self.durationText = packageDictionary[@"title"];
        
        if(packageDuration == nil) {
            NSInteger expiryDays = [packageDictionary[@"expiry_days"] integerValue];
            NSInteger noOfMonths = expiryDays / 30;
            NSString* monthText = (noOfMonths == 1) ? @"Month" : @"Months" ;
            self.packageDuration = [NSString stringWithFormat:@"%ld %@",(long)noOfMonths,monthText];
        }else {
            self.packageDuration = packageDuration;
        }
        
        self.packagePrice = price ? price : packageDictionary[@"price"];
        self.packageIdentifier = packageDictionary[@"apple_sku"];
        if(self.packagePrice.integerValue == 0) {
            self.packageType = FreePackage;
        }
        else {
            self.packageType = PaidPackage;
        }
        
        if(description && [description isKindOfClass:[NSString class]]) {
            self.packageDescription = description;
        }
        if(tag && [tag isKindOfClass:[NSString class]]) {
            self.packageTag = tag;
            self.tagType = TMBUYPACK_TAG_INFO;
        }
        if(discount > 0) {
            self.packageTag = [NSString stringWithFormat:@"SAVE %d%%",discount];
            self.tagType = TMBUYPACK_TAG_DISCOUNT;
        }
    }
    return self;
}

-(NSString*)selectPackageTitle {
    NSString *title;
    if(self.packageType == FreePackage) {
        //title = @"START FREE TRIAL";
       title = self.freeCtaText;
    }
    else {
        title = [NSString stringWithFormat:@"Rs %@ Per Month",self.packagePrice];
    }
    
    return title;
}
-(UIColor*)selectTagLabelBgColor {
    if(self.isMostPopular) {
        return [UIColor colorWithRed:44.0f/255.0f green:159.0f/255.0f blue:38.0f/255.0f alpha:1];
    }
    else {
        return [UIColor colorWithRed:66.0f/255.0f green:199.0f/255.0f blue:233.0f/255.0f alpha:1];
    }
}

@end

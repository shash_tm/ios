//
//  TMSelectQuizStartView.m
//  TrulyMadly
//
//  Created by Ankit on 12/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizStartView.h"
#import "UIColor+TMColorAdditions.h"
#import "NSString+TMAdditions.h"

@interface TMSelectQuizStartView ()

@property(nonatomic,strong)UIScrollView *contentView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIImageView *introImageView;
@property(nonatomic,strong)UILabel *titleLabelCenter;
@property(nonatomic,strong)UILabel *descriptinLabel1;
@property(nonatomic,strong)UILabel *descriptinLabel2;
@property(nonatomic,strong)UILabel *disclaimerLabel;
@property(nonatomic,strong)UIImageView *disclaimerIcon;
@property(nonatomic,strong)UIButton *quizStartButton;

@end


@implementation TMSelectQuizStartView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGFloat maxWidth = CGRectGetWidth(self.frame);
        CGFloat titleLabelXPos = 20;
        CGFloat titleLabelYPos = 60;
        CGFloat allowedWidth = maxWidth - (2*titleLabelXPos);
        CGFloat iconXPos, iconYPos;
        CGRect textFrame;
        
        NSString *disclaimerMessage = @"This quiz can be taken only once";
        textFrame = [disclaimerMessage boundingRectWithConstraintSize:CGSizeMake(allowedWidth, 20) attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16]}];
        
        if(fabs(maxWidth) < 375) {
            iconYPos = 15;
        }else {
            iconYPos = 40;
        }
        self.contentView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:self.contentView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos, titleLabelYPos,
                                                                    allowedWidth, 30)];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:26];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.text  = @"Let’s Get Started";
        [self.contentView addSubview:self.titleLabel];
        
        
        self.introImageView = [[UIImageView alloc] initWithFrame:CGRectMake(titleLabelXPos,
                                                                            CGRectGetMaxY(self.titleLabel.frame)+20,
                                                                            allowedWidth,
                                                                            150)];
        self.introImageView.image = [UIImage imageNamed:@"selectquizintro"];
        [self.contentView addSubview:self.introImageView];
        
        self.titleLabelCenter = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos,
                                                                          CGRectGetMaxY(self.introImageView.frame)+25,
                                                                          allowedWidth, 30)];
        self.titleLabelCenter.font = [UIFont fontWithName:@"HelveticaNeue" size:26];
        self.titleLabelCenter.textColor = [UIColor blackColor];
        self.titleLabelCenter.textAlignment = NSTextAlignmentCenter;
        self.titleLabelCenter.text  = @"Compatibility Quotient";
        [self.contentView addSubview:self.titleLabelCenter];
        
        self.descriptinLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos,
                                                                          CGRectGetMaxY(self.titleLabelCenter.frame)+25,
                                                                          allowedWidth, 40)];
        self.descriptinLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        self.descriptinLabel1.textColor = [UIColor blackColor];
        self.descriptinLabel1.textAlignment = NSTextAlignmentCenter;
        self.descriptinLabel1.numberOfLines = 0;
        self.descriptinLabel1.text  = @"Does the stuff that matters to you, matter to them?";
        [self.contentView addSubview:self.descriptinLabel1];
        
        self.descriptinLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos,
                                                                          CGRectGetMaxY(self.descriptinLabel1.frame)+10 ,
                                                                          allowedWidth, 60)];
        self.descriptinLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        self.descriptinLabel2.textColor = [UIColor blackColor];
        self.descriptinLabel2.textAlignment = NSTextAlignmentCenter;
        self.descriptinLabel2.numberOfLines = 0;
        self.descriptinLabel2.text  = @"Just answer a few questions and compare your Compatibility Quotient.";
        [self.contentView addSubview:self.descriptinLabel2];
        
        iconXPos = titleLabelXPos + (allowedWidth - CGRectGetWidth(textFrame) - 25)/2;
        self.disclaimerIcon = [[UIImageView alloc] initWithFrame:CGRectMake(iconXPos, CGRectGetMaxY(self.descriptinLabel2.frame)+iconYPos, 20, 20)];
        self.disclaimerIcon.image = [UIImage imageNamed:@"quizInfo"];
        [self.contentView addSubview:self.disclaimerIcon];
        
        
        self.disclaimerLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.disclaimerIcon.frame) + 5, CGRectGetMaxY(self.descriptinLabel2.frame)+iconYPos, allowedWidth - 20, 20)];
        self.disclaimerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
        self.disclaimerLabel.textColor = [UIColor blackColor];
        self.disclaimerLabel.numberOfLines = 0;
        self.disclaimerLabel.text  = disclaimerMessage;
        [self.contentView addSubview:self.disclaimerLabel];
        
        self.quizStartButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.quizStartButton.frame = CGRectMake(titleLabelXPos, CGRectGetMaxY(self.disclaimerLabel.frame)+15, allowedWidth, 50);
        [self.quizStartButton setTitle:@"I’M READY" forState:UIControlStateNormal];
        self.quizStartButton.backgroundColor = [UIColor likeColor];
        self.quizStartButton.layer.cornerRadius = 10;
        [self.quizStartButton addTarget:self action:@selector(quizStartAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.quizStartButton];
        
        self.contentView.contentSize = CGSizeMake(CGRectGetWidth(self.contentView.frame), CGRectGetMaxY(self.quizStartButton.frame)+40);
    }
    return self;
}

-(void)quizStartAction {
    [self.delegate didStartQuiz];
}

@end

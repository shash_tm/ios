//
//  TMSelectQuizHeaderView.m
//  TrulyMadly
//
//  Created by Ankit on 03/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizHeaderView.h"


@interface TMSelectQuizHeaderView ()

@property(nonatomic,assign)NSInteger selectedIndex;
@property(nonatomic,strong)UIView* pointerView;
@property(nonatomic,strong)NSMutableArray* sectionViews;

@end


@implementation TMSelectQuizHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.sectionViews = [NSMutableArray array];
        self.pointerView = [[UIView alloc] initWithFrame:CGRectZero];
        self.pointerView.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:183.0f/255.0f blue:0 alpha:1];
        [self addSubview:self.pointerView];
    }
    return self;
}
-(void)layoutSubviews {
    [super layoutSubviews];
    
    [self layoutSectionViews];
    
    [self layoutPointerView];
}
-(void)setupUIForQuizCount:(NSInteger)quizCount {
    self.selectedIndex = 0;
    
    for(int i=0;i<quizCount;i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        UIColor *bgColor = (i <= self.selectedIndex) ? [UIColor colorWithRed:255.0f/255.0f green:183.0f/255.0f blue:0 alpha:1]
                                                     : [UIColor grayColor];
        view.backgroundColor = bgColor  ;
        [self.sectionViews addObject:view];
        [self addSubview:view];
    }
}
-(void)layoutSectionViews {
    CGFloat xPos = 0;
    CGFloat height = 12;
    CGFloat xoffset = 1;
    NSInteger quizCount = self.sectionViews.count;
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    CGFloat width  = (maxWidth - (quizCount*xoffset))/quizCount;
    for(UIView *view in self.sectionViews) {
        view.frame = CGRectMake(xPos, 0, width, height);
        xPos = xPos + width + xoffset;
    }
}
-(void)layoutPointerView {
    if(self.sectionViews.count) {
        UIView *sectionView = self.sectionViews[self.selectedIndex];
        self.pointerView.frame = CGRectMake(CGRectGetMinX(sectionView.frame),
                                            CGRectGetMaxY(sectionView.frame)+1,
                                            CGRectGetWidth(sectionView.frame),
                                            3);
    }
}
-(void)updateSelectedIndex:(NSInteger)selectedIndex {
    self.selectedIndex = selectedIndex;
    [UIView animateWithDuration:0.25
                          delay: 0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self layoutPointerView];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}
-(void)updateSelectHeaderTillIndex:(NSInteger)selectedIndex {
    for(int i=0; i<selectedIndex; i++) {
        UIView *sectionView = self.sectionViews[i];
        UIColor *bgColor = [UIColor colorWithRed:255.0f/255.0f green:183.0f/255.0f blue:0 alpha:1];
        sectionView.backgroundColor = bgColor;
    }
}

@end

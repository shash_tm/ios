//
//  TMBuySelectViewController.h
//  TrulyMadly
//
//  Created by Ankit on 07/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"

@interface TMBuySelectViewController : TMBaseViewController

@property(nonatomic,strong)NSDictionary* trackInfoDictionary;

///for tracking purpose. id of user which is on screen when user opened this viewcontroller
@property(nonatomic,strong)NSString* matchId;

- (instancetype)initWithSource:(NSString*)source;

@end

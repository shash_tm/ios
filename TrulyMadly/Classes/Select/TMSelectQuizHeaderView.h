//
//  TMSelectQuizHeaderView.h
//  TrulyMadly
//
//  Created by Ankit on 03/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSelectQuizHeaderView : UIView

-(void)setupUIForQuizCount:(NSInteger)quizCount;
-(void)updateSelectedIndex:(NSInteger)selectedIndex;
-(void)updateSelectHeaderTillIndex:(NSInteger)selectedIndex;

@end

//
//  TMSelectQuizViewController.m
//  TrulyMadly
//
//  Created by Ankit on 28/10/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizViewController.h"
#import "TMSelectManager.h"
#import "TMSelectQuizCollectionViewCell.h"
#import "TMSelectQuiz.h"
#import "TMSelectQuizHeaderView.h"
#import "TMMatchActioNView.h"
#import "TMSelectQuizSaveView.h"
#import "TMSelectQuizStartView.h"
#import "TMEnums.h"
#import "TMError.h"
#import "TMErrorMessages.h"
#import "TMUserSession.h"
#import "TMToastView.h"
#import "TMAnalytics.h"


@interface TMSelectQuizViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,TMMatchActionViewDelegate,
                                         UIScrollViewDelegate,TMSelectQuizSaveViewDelegate,TMSelectQuizStartViewDelegate>

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)TMSelectQuizSaveView *quizsaveView;
@property(nonatomic,strong)TMSelectQuizStartView *quizstartView;
@property(nonatomic,strong)TMSelectQuizHeaderView *quizHeaderView;
@property(nonatomic,strong)TMMatchActionView *quizActionView;
@property(nonatomic,strong)UILabel *quizCounterLabel;

@property(nonatomic,strong)TMSelectManager *selectManager;

@property(nonatomic,strong)NSMutableArray *quizList;
@property(nonatomic,assign)NSInteger currentdIndex;
@property(nonatomic,assign)NSInteger maxQuizViewedCounter;

@end

@implementation TMSelectQuizViewController

#pragma mark -
#pragma mark Initialization / LifeCyle Methos -
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
    
    self.currentdIndex = 0;
    self.maxQuizViewedCounter = 1;
    
    BOOL isQuizPending = [[TMUserSession sharedInstance] isSelectQuizPending];
    if(isQuizPending) {
        TMSelectQuizProgressState progressState = [[TMUserSession sharedInstance] getSelectQuizProgressState];
        if(progressState == OnReady) {
            [self setupQuizStartView];
        }
        else if(progressState == OnTheQuiz) {
            NSInteger quizPlayedIndex = [[TMUserSession sharedInstance] getSelectQuizPlayedIndex];
            self.currentdIndex = quizPlayedIndex-1;
            self.maxQuizViewedCounter = quizPlayedIndex;
            [self showSelectQuizData];
        }
        else if(progressState == SaveQuiz) {
            [self showSelectQuizData];
        }
    }
    else {
        [[TMUserSession sharedInstance] setSelectQuizAsStarted];
        [self setupQuizStartView];
        [self trackQuizEventWithStatus:@"quizstarted"];
    }
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat maxWidth = CGRectGetWidth(self.view.frame);
    CGFloat maxHeight = CGRectGetHeight(self.view.frame);
    CGFloat quizHeaderHeight = 16;
    CGFloat quizActionViewHeight = 90;
    CGFloat quizCounterLabelHeight = 55;
    CGFloat quizActionViewYPos = maxHeight - quizActionViewHeight;
    CGFloat collectionViewYPos = CGRectGetMaxY(self.quizCounterLabel.frame);
    CGFloat collectionViewHeight = maxHeight - ((CGRectGetMaxY(self.quizHeaderView.frame)+quizActionViewHeight+quizCounterLabelHeight));
    
    self.quizHeaderView.frame = CGRectMake(0, 20, maxWidth, quizHeaderHeight);
    self.quizCounterLabel.frame = CGRectMake(0, CGRectGetMaxY(self.quizHeaderView.frame), maxWidth, quizCounterLabelHeight);
    self.collectionView.frame = CGRectMake(0, collectionViewYPos, maxWidth, collectionViewHeight);
    self.quizActionView.frame = CGRectMake(0, quizActionViewYPos, maxWidth, quizActionViewHeight);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Data Loading -
-(void)showSelectQuizData {
    if(self.selectManager.isNetworkReachable) {
        [self configureViewForDataLoadingStart];
        [self.selectManager getQuizData:^(NSArray *quizData, TMError *error) {
            [self configureViewForDataLoadingFinish];
            
            if(quizData) {
                [self.quizList addObjectsFromArray:quizData];
                TMSelectQuizProgressState progressState = [[TMUserSession sharedInstance] getSelectQuizProgressState];
                if(progressState == SaveQuiz) {
                    [self showQuizSaveView];
                }
                else {
                    [self reloadView];
                }
                
                [self trackQuizEventWithStatus:@"fetch_success"];
            }
            else {
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewWithMessage:@"Oops something went wrong.Please try again"];
                }
                else {
                    //unknwon error
                    [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
                }
                
                [self trackQuizEventWithStatus:@"fetch_fail"];
            }
        }];
    }
    else {
        [self showRetryViewWithMessage:@"No Internet Connectivity"];
    }
}
-(void)saveQuiz {
    if(self.selectManager.isNetworkReachable) {
        NSMutableDictionary *quizAnswer = [NSMutableDictionary dictionary];
        for (int quizIndex=0; quizIndex<_quizList.count; quizIndex++) {
            TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:quizIndex];
            NSString* questionId = [NSString stringWithFormat:@"%ld",(long)selectQuiz.questionId];
            NSString* optionId = [selectQuiz getSelectedOptionId];
            quizAnswer[questionId] = optionId;
        }
        
        [self.selectManager saveQuizAnswer:quizAnswer response:^(BOOL result, TMError *error) {
            if(result) {
                [self removerAllCachedQuizAnswers];
                TMUserSession *userSession = [TMUserSession sharedInstance];
                userSession.isUserProfileUpdated = TRUE;
                if([TMUserSession sharedInstance].selectBuyActionOnSelectProfile) {
                    [TMUserSession sharedInstance].needToLoadProfileOnSelectPayment = TRUE;
                }
                [userSession setSelectQuizAsFinished];
                [self.quizDelegate didCompleteQuiz];
            }
            else {
                [self.quizsaveView updateButtonToDefaultState];
                BOOL showToast = FALSE;
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
                }
                else {
                    showToast = TRUE;
                }
                if(showToast){
                    [TMToastView showToastInParentView:self.view withText:@"Oops something went wrong.Please try again" withDuaration:3.0 presentationDirection:TMToastPresentationFromBottom];
                }
            }
        }];
    }
    else {
          [TMToastView showToastInParentView:self.view withText:TM_INTERNET_NOTAVAILABLE_MSG withDuaration:3.0 presentationDirection:TMToastPresentationFromBottom];
    }
}
-(void)removerAllCachedQuizAnswers {
    for (int quizIndex=0; quizIndex<_quizList.count; quizIndex++) {
        TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:quizIndex];
        [selectQuiz removeSelectedOtionId];
    }
}
-(void)updateQuizPlayedIndex {
    [[TMUserSession sharedInstance] setSelectQuizPlayedIndex:self.maxQuizViewedCounter];
    [[TMUserSession sharedInstance] setSelectQuizProgressState:OnTheQuiz];
}

#pragma mark -
#pragma mark getter -
-(TMSelectManager*)selectManager {
    if(!_selectManager) {
        _selectManager = [[TMSelectManager alloc] init];
    }
    return _selectManager;
}
-(NSMutableArray*)quizList {
    if(!_quizList) {
        _quizList = [NSMutableArray arrayWithCapacity:8];
    }
    return _quizList;
}
-(UILabel*)quizCounterLabel {
    if(!_quizCounterLabel) {
        _quizCounterLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _quizCounterLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_quizCounterLabel];
    }
    return _quizCounterLabel;
}
-(TMSelectQuiz*)getSelectQuizForIndex:(NSInteger)index {
    NSDictionary *selectQuizDictionary = self.quizList[index];
    TMSelectQuiz *selectQuiz = [[TMSelectQuiz alloc] initWithQuizDictionary:selectQuizDictionary];
    return selectQuiz;
}

#pragma mark -
#pragma mark UI setup methods -
-(void)setupUI {
    [self setupHeaderView];
    [self setupCollectionView];
}
-(void)setupHeaderView {
    CGFloat maxWidth = CGRectGetWidth(self.view.frame);
    CGFloat quizHeaderHeight = 15;
    self.quizHeaderView = [[TMSelectQuizHeaderView alloc] initWithFrame:CGRectMake(0, 0, maxWidth, quizHeaderHeight)];
    [self.view addSubview:self.quizHeaderView];
}
-(void)setupActionView {
    CGFloat maxWidth = CGRectGetWidth(self.view.frame);
    CGFloat quizActionViewHeight = 90;
    self.quizActionView = [[TMMatchActionView alloc] init];
    self.quizActionView.frame = CGRectMake(0, 0, maxWidth, quizActionViewHeight);
    self.quizActionView.delegate = self;
    [self.quizActionView addSelecQuizActionBar];
    [self.view addSubview:self.quizActionView];
}
-(void)setupCollectionView {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [flowLayout setMinimumInteritemSpacing:1];
    [flowLayout setMinimumLineSpacing:1];
    [flowLayout setSectionInset:UIEdgeInsetsMake(0,0,0,0)];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.pagingEnabled = TRUE;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[TMSelectQuizCollectionViewCell class]
            forCellWithReuseIdentifier:[TMSelectQuizCollectionViewCell reusableIdentifier]];
}
-(void)setupQuizStartView {
    self.quizstartView = [[TMSelectQuizStartView alloc] initWithFrame:self.view.bounds];
    self.quizstartView.delegate = self;
    [self.view addSubview:self.quizstartView];
}
-(void)configureViewForDataLoadingStart {
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading..."];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
    if(self.retryView.superview) {
        [self removeRetryView];
    }
}
-(void)showRetryViewWithMessage:(NSString*)message {
    if(!self.retryView.superview) {
        self.isRetryViewShown = TRUE;
        self.retryView = [[TMRetryView alloc] initWithFrame:self.view.bounds message:message];
        [self.view addSubview:self.retryView];
        [self.retryView.retryButton addTarget:self action:@selector(showSelectQuizData) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)updateQuizCounterLabelText:(NSInteger)quizCounter {
    self.quizCounterLabel.text = [NSString stringWithFormat:@"%ld of %ld",(long)quizCounter+1,(long)self.quizList.count];
}
-(void)reloadView {
    [self setupActionView];
    [self updateQuizCounterLabelText:self.currentdIndex];
    [self.collectionView reloadData];
    [self.quizHeaderView setupUIForQuizCount:self.quizList.count];
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:self.currentdIndex inSection:0];
    [self scrollToIndexPath:newIndexPath];
    [self.quizHeaderView updateSelectHeaderTillIndex:self.maxQuizViewedCounter];
}

#pragma mark -
#pragma mark collectionview delegate / datasource -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(self.maxQuizViewedCounter <= self.quizList.count) {
        return self.maxQuizViewedCounter;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size =  CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame));
    return size;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusableIdetifier = [TMSelectQuizCollectionViewCell reusableIdentifier];
    TMSelectQuizCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reusableIdetifier
                                                                                     forIndexPath:indexPath];
    TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:indexPath.row];
    [cell setImageFromURL:[NSURL URLWithString:selectQuiz.quizImageURLString]];
    [cell setQuizText:selectQuiz.quizText];
    
    return  cell;
}

#pragma mark -
#pragma mark UIScrollCiew delegate -
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = CGRectGetWidth(self.collectionView.frame);
    float fractionalPage = self.collectionView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    if(page != self.currentdIndex) {
        self.currentdIndex = page;
        [self updateQuizCounterLabelText:self.currentdIndex];
        [self.quizHeaderView updateSelectedIndex:self.currentdIndex];
        TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:self.currentdIndex];
        if(([selectQuiz quizAnswerStatus] == Yes) || ([selectQuiz quizAnswerStatus] == No)) {
            TMSelectQuizActionType actionType = ([selectQuiz quizAnswerStatus] == Yes) ? SelectQuizActionYes : SelectQuizActionNo;
            [self.quizActionView setSelectQuizAction:actionType];
        }
        else {
            [self.quizActionView setSelectQuizActionViewToDefault];
        }
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    CGFloat pageWidth = CGRectGetWidth(self.collectionView.frame);
    float fractionalPage = self.collectionView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);

    self.currentdIndex = page;
    [self updateQuizCounterLabelText:self.currentdIndex];
    [self.quizHeaderView updateSelectedIndex:self.currentdIndex];
    TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:self.currentdIndex];
    if(([selectQuiz quizAnswerStatus] == Yes) || ([selectQuiz quizAnswerStatus] == No)) {
        TMSelectQuizActionType actionType = ([selectQuiz quizAnswerStatus] == Yes) ? SelectQuizActionYes : SelectQuizActionNo;
        [self.quizActionView setSelectQuizAction:actionType];
    }
    else {
        [self.quizActionView setSelectQuizActionViewToDefault];
    }
}

#pragma mark -
#pragma mark Action handlers -
-(void)didPeformLikeAction {
    TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:self.currentdIndex];
    [selectQuiz setQuizAnswerWithYesOptionId];
    [self moveToNextQuestionWithAction:TMSELECTQUIZACTION_LIKE];
}

-(void)didPeformHideAction {
    TMSelectQuiz *selectQuiz = [self getSelectQuizForIndex:self.currentdIndex];
    [selectQuiz setQuizAnswerWithNoOptionId];
    [self moveToNextQuestionWithAction:TMSELECTQUIZACTION_HIDE];
}

-(void)moveToNextQuestionWithAction:(TMSelectQuizAction)quizAction {
    if((++self.currentdIndex) < self.quizList.count) {
        [self updateQuizCounterLabelText:self.currentdIndex];
        [self.quizHeaderView updateSelectedIndex:self.currentdIndex];
        
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:self.currentdIndex inSection:0];
        [self insertNextQuizAtIndexPath:newIndexPath];
        [self animateToNextQuestionWithAction:quizAction toIndexpath:newIndexPath];
    }
    else {
        //show lets go
        [self showQuizSaveView];
    }
}

-(void)insertNextQuizAtIndexPath:(NSIndexPath*)indexPath {
    if((self.currentdIndex < self.quizList.count)) {
        if(self.currentdIndex == self.maxQuizViewedCounter) {
            self.maxQuizViewedCounter++;
            [self updateQuizPlayedIndex];
            [self.quizHeaderView updateSelectHeaderTillIndex:self.maxQuizViewedCounter];
            NSArray *newItems = @[indexPath];
            [self.collectionView insertItemsAtIndexPaths:newItems];
        }
    }
}
-(void)scrollToIndexPath:(NSIndexPath*)indexPath {
    [self.collectionView scrollToItemAtIndexPath:indexPath
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                        animated:YES];
}
-(void)animateToNextQuestionWithAction:(TMSelectQuizAction)quizAction toIndexpath:(NSIndexPath*)indexPath {
    TMSelectQuizActionType actionType = (quizAction == TMSELECTQUIZACTION_LIKE) ? SelectQuizActionYes : SelectQuizActionNo;
    [self.quizActionView animateSelectQuizActionView:actionType withCompletion:^{
        [self scrollToIndexPath:indexPath];
    }];
}
-(void)showQuizSaveView {
    [[TMUserSession sharedInstance] setSelectQuizProgressState:SaveQuiz];
    self.quizsaveView = [[TMSelectQuizSaveView alloc] initWithFrame:CGRectMake(0,
                                                                               CGRectGetMaxY(self.quizHeaderView.frame),	
                                                                               CGRectGetWidth(self.view.frame),
                                                                               CGRectGetHeight(self.view.frame)-20)];
    self.quizsaveView.delegate = self;
    [self.view addSubview:self.quizsaveView];
    
    [self trackQuizEventWithStatus:@"quizended"];
}
-(void)updateQuizActionView {
    
}

#pragma mark -
#pragma mark TMQuizSaveView delegate -
-(void)didSaveQuiz {
    [self.quizsaveView updateButtonStateForDataLoading];
    [self saveQuiz];
}

#pragma mark -
#pragma mark TMQuizStartView delegate -
-(void)didStartQuiz {
    [self.quizstartView removeFromSuperview];
    self.quizstartView = nil;
    [self showSelectQuizData];
}

#pragma mark -
#pragma mark Tracking -

-(void)trackQuizEventWithStatus:(NSString*)status {
    [[TMAnalytics sharedInstance] trackSelectEvent:@"quiz" status:status eventInfo:nil];
}

@end

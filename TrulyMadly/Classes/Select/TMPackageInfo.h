//
//  TMPackageInfo.h
//  TrulyMadly
//
//  Created by Ankit on 11/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMEnums.h"

@interface TMPackageInfo : NSObject

@property(nonatomic,strong,readonly)NSString *packageIdentifier;
@property(nonatomic,strong,readonly)NSString *packageTitle;
@property(nonatomic,strong,readonly)NSString *packageDescription;
@property(nonatomic,strong,readonly)NSString *packageDuration;
@property(nonatomic,strong,readonly)NSString *packageTag;
@property(nonatomic,strong,readonly)NSString *packagePrice;
@property(nonatomic,strong,readonly)NSString *priceSubText;
@property(nonatomic,strong,readonly)NSString *currency;
@property(nonatomic,strong,readonly)NSString *price;
@property(nonatomic,strong,readonly)NSString *freeTrialText;
@property(nonatomic,strong,readonly)NSString *durationText;
@property(nonatomic,assign,readonly)TMPackagePriceType packageType;
@property(nonatomic,assign,readonly)TMBuyPackTagType tagType;
@property(nonatomic,assign,readonly)BOOL isMostPopular;
@property(nonatomic,assign,readonly)BOOL isMatchGuarantee;

- (instancetype)initWithDictionary:(NSDictionary*)packageDictionary;
-(NSString*)selectPackageTitle;
-(UIColor*)selectTagLabelBgColor;

@end

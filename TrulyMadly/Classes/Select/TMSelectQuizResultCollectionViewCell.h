//
//  TMSelectQuizResultCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 06/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMSelectQuizResultInfo;

@interface TMSelectQuizResultCollectionViewCell : UICollectionViewCell

+ (NSString *)cellReuseIdentifier;
-(void)setupCell:(TMSelectQuizResultInfo*)quizResultInfo withMatchImageURL:(NSURL*)imageURL;

@end

//
//  TMSelectPackageListViewCell.m
//  TrulyMadly
//
//  Created by Suruchi Sinha on 1/13/17.
//  Copyright © 2017 trulymadly. All rights reserved.
//

#import "TMSelectPackageListViewCell.h"
#import "NSString+TMAdditions.h"
@interface TMSelectPackageListViewCell()

@property(nonatomic, strong)UIImageView *descriptionIcon;
@property(nonatomic, strong)UILabel *descriptionLabel;
@property(nonatomic, strong)UILabel *durationLabel;
@property(nonatomic, strong)UILabel *matchLabel;
@property(nonatomic, strong)UILabel *priceLabel;
@property(nonatomic, strong)UIView *tagView;
@property(nonatomic, strong)UILabel *tagLabel;
@end


@implementation TMSelectPackageListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(UIImageView *)descriptionIcon {
    if(!_descriptionIcon) {
        _descriptionIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _descriptionIcon.image = [UIImage imageNamed:@"buysparkdescription"];
        [self.contentView addSubview:_descriptionIcon];
    }
    return _descriptionIcon;
}

-(UILabel *)descriptionLabel {
    if(!_descriptionLabel) {
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _descriptionLabel.font = [self descriptionLabelFont];
        _descriptionLabel.textColor = [UIColor blackColor];
        _descriptionLabel.numberOfLines = 2;
        [self.contentView addSubview:_descriptionLabel];
    }
    return _descriptionLabel;
}

-(UILabel *)durationLabel {
    if(!_durationLabel) {
        _durationLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _durationLabel.font = [self durationLabelFont];
        _durationLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:_durationLabel];
    }
    return _durationLabel;
}

-(UILabel *)priceLabel {
    if(!_priceLabel) {
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.clipsToBounds = TRUE;
        _priceLabel.textColor = [UIColor whiteColor];
        _priceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        _priceLabel.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:0.0F/255.0f blue:115.0F/255.0F alpha:1];
        _priceLabel.layer.cornerRadius = 8;
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

-(UILabel*)matchLabel {
    if(!_matchLabel) {
        _matchLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _matchLabel.textAlignment = NSTextAlignmentCenter;
        _matchLabel.clipsToBounds = TRUE;
        _matchLabel.font = [self matchLabelFont];
        _matchLabel.layer.cornerRadius = 5;
        [self.contentView addSubview:_matchLabel];
    }
    return _matchLabel;
}

-(UIView *)tagView {
    if(!_tagView) {
        _tagView = [[UIView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_tagView];
    }
    return _tagView;
}

-(UILabel *)tagLabel {
    if(!_tagLabel) {
        _tagLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _tagLabel.font = [self tagLabelFont];
        _tagLabel.textColor = [UIColor blackColor];
        _tagLabel.textAlignment = NSTextAlignmentRight;
        _tagLabel.numberOfLines = 0;
        [self.contentView addSubview:_tagLabel];
    }
    return _tagLabel;
}

-(UIFont*)descriptionLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:14];
}

-(UIFont*)durationLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:20];
}

-(UIFont*)matchLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:10];
}

-(UIFont*)tagLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:8];
}
- (void)layoutSubviews {
    
    CGFloat frameHeight, xPos, yPos, priceLabelHeight, priceLabelWidth;
    CGSize constraintSize;
    CGRect textFrame, bounds;
    CAShapeLayer *tagViewlayer;
    CGMutablePathRef path;
    BOOL isDescriptionAvailable;
    
    frameHeight = 20;
    priceLabelWidth = 80;
    priceLabelHeight = 40;
    
    constraintSize = CGSizeMake(200, 20);
    isDescriptionAvailable = (self.descriptionLabel.text) ? TRUE : FALSE;
    if(isDescriptionAvailable) {
        frameHeight = (frameHeight * 2) + 10;
    }
    
    textFrame = [self.durationLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[self durationLabelFont]}];
    yPos = (CGRectGetHeight(self.frame) - frameHeight)/2;
    self.durationLabel.frame = CGRectMake(10, yPos, CGRectGetWidth(textFrame),CGRectGetHeight(textFrame));
    
    xPos = CGRectGetWidth(self.frame) - priceLabelWidth - 30;
    yPos = (CGRectGetHeight(self.frame) - priceLabelHeight)/2;
    self.priceLabel.frame = CGRectMake(xPos, yPos, priceLabelWidth, priceLabelHeight);
    
    if(isDescriptionAvailable) {
        self.descriptionIcon.frame = CGRectMake(10, CGRectGetMaxY(self.durationLabel.frame)+10, 16, 15);
        
        CGFloat descriptionLabelXPos  = CGRectGetMaxX(self.descriptionIcon.frame)+5;
        CGFloat maxDescriptionLabelWidth = CGRectGetMinX(self.priceLabel.frame) - descriptionLabelXPos;
        CGSize constraintSize = CGSizeMake(maxDescriptionLabelWidth, 40);
        CGRect rect = [self.descriptionLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[self descriptionLabelFont]}];
        self.descriptionLabel.frame = CGRectMake(descriptionLabelXPos,
                                                 CGRectGetMaxY(self.durationLabel.frame)+10,
                                                 CGRectGetWidth(rect), CGRectGetHeight(rect));
    }
    else {
        self.descriptionIcon.frame = CGRectZero;
        self.descriptionLabel.frame = CGRectZero;
    }
    
    if(self.matchLabel.text) {
        CGSize constraintSize = CGSizeMake(150, 20);
        CGFloat matchLabelExtraXPosSpace = 5;
        yPos = CGRectGetMinY(self.durationLabel.frame) + (CGRectGetHeight(self.durationLabel.frame) - 20)/2;
        CGRect rect = [self.matchLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[self matchLabelFont]}];
        self.matchLabel.frame = CGRectMake(CGRectGetMaxX(self.durationLabel.frame)+5,
                                         yPos,
                                         CGRectGetWidth(rect)+matchLabelExtraXPosSpace,
                                         20);
    }
    else {
        self.matchLabel.frame = CGRectZero;
    }
    
    if(self.tagLabel.text) {
        xPos = CGRectGetWidth(self.frame) - 50;
        self.tagView.frame = CGRectMake(xPos, 0, 50, 50);
        
        tagViewlayer = [[CAShapeLayer alloc] init];
        tagViewlayer.frame = self.tagView.layer.bounds;
        bounds = self.tagView.layer.frame;
        
        path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, bounds.size.width, 0);
        CGPathAddLineToPoint(path, nil, bounds.size.width, bounds.size.height);
        CGPathAddLineToPoint(path, nil, 0, 0);
        CGPathCloseSubpath(path);
        
        tagViewlayer.path = path;
        tagViewlayer.shadowOpacity = 0.5;
        self.tagView.layer.mask = tagViewlayer;
        
        CAShapeLayer *shape = [CAShapeLayer layer];
        shape.frame = self.tagView.bounds;
        shape.path = path;
        shape.fillColor = [UIColor colorWithRed:(66.0f/255.0f) green:(199.0f/255.0f) blue:(233.0f/255.0f) alpha:1.0].CGColor;
        [self.tagView.layer addSublayer:shape];
        
        CGPathRelease(path);
        
        constraintSize = CGSizeMake(22, 200);
        textFrame = [self.tagLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:[self tagLabelFont]}];
        xPos = self.tagView.frame.size.width / 2;
        yPos = 5;
        self.tagLabel.frame = CGRectMake(xPos, yPos, 22, CGRectGetHeight(textFrame));
        [self.tagView addSubview:self.tagLabel];
        
    }
}

- (void)configureCellWithData:(TMPackageInfo *)packageInfo {
    
    if(packageInfo.isMatchGuarantee) {
        //self.matchLabel.text = @"MATCH GUARANTEE*";
        self.matchLabel.attributedText = [self getAttributedText:@"MATCH GUARANTEE*"];
        self.matchLabel.textColor = [UIColor whiteColor];
        self.matchLabel.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:163.0F/255.0f blue:18.0F/255.0F alpha:1];
        self.matchLabel.layer.borderColor = [UIColor clearColor].CGColor;
        self.matchLabel.layer.borderWidth = 0;
    }else {
        self.matchLabel.frame = CGRectZero;
    }
    
    if(packageInfo.tagType == TMBUYPACK_TAG_DISCOUNT) {
        self.tagLabel.text = packageInfo.packageTag;
    }
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@", packageInfo.currency, packageInfo.price];
    self.durationLabel.text = packageInfo.durationText;
    self.descriptionLabel.text = packageInfo.packageDescription;
}

-(NSAttributedString*)getAttributedText:(NSString*)text {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
    NSDictionary *atts = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]};
    NSRange range = NSMakeRange(15, 1);
    [attString setAttributes:atts range:range];
    return attString;
}

@end

//
//  TMSelectQuizResultCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 06/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizResultCollectionViewCell.h"
#import "TMSelectQuizResultInfo.h"
#import "TMUserSession.h"
#import "NSString+TMAdditions.h"
#import <UIImageView+AFNetworking.h>


@interface TMSelectQuizResultCollectionViewCell ()

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIImageView *userImageView;
@property(nonatomic,strong)UIImageView *matchImageView;
@property(nonatomic,strong)UIImageView *userActionImageView;
@property(nonatomic,strong)UIImageView *matchActionImageView;
@property(nonatomic,strong)UIImageView *iconImageView;
@property(nonatomic,strong)UIView *seperatorView;

@end


@implementation TMSelectQuizResultCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderWidth = 0.5;
        self.layer.borderColor = [UIColor grayColor].CGColor;

        CGFloat width = 40;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
        [self.contentView addSubview:self.titleLabel];
        
        self.userImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.userImageView.layer.cornerRadius = width/2;
        self.userImageView.layer.borderWidth = 2.0;
        self.userImageView.clipsToBounds = TRUE;
        [self.contentView addSubview:self.userImageView];
        
        self.matchImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.matchImageView.layer.cornerRadius = width/2;
        self.matchImageView.layer.borderWidth = 2.0;
        self.matchImageView.clipsToBounds = TRUE;
        [self.contentView addSubview:self.matchImageView];
    }
    return self;
}
+ (NSString *)cellReuseIdentifier {
    return NSStringFromClass([self class]);
}
+(CGFloat)cellHeightForText:(NSString*)text {
    return 0;
}
-(void)setupCell:(TMSelectQuizResultInfo*)quizResultInfo withMatchImageURL:(NSURL*)imageURL {
    self.userImageView.layer.borderColor = [quizResultInfo getBoundaryColorForUser].CGColor;
    self.matchImageView.layer.borderColor = [quizResultInfo getBoundaryColorForMatch].CGColor;
    self.userImageView.image = nil;
    self.matchImageView.image = nil;
    [self.userImageView setImageWithURL:[NSURL URLWithString:[TMUserSession sharedInstance].user.profilePicURL]];
    [self.matchImageView setImageWithURL:imageURL];
    
    self.titleLabel.text = quizResultInfo.questionText;
    
    BOOL showConectingImage = [quizResultInfo showConnectingView];
    if(showConectingImage){
        [self deinitImageViews];
        
        if(!self.seperatorView){
            self.seperatorView = [[UIView alloc] initWithFrame:CGRectZero];
            [self.contentView addSubview:self.seperatorView];
        }
        if(!self.iconImageView) {
            self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero] ;
            [self.contentView addSubview:self.iconImageView];
        }
        
        self.seperatorView.backgroundColor = [quizResultInfo connectingColor];
        self.iconImageView.image = [quizResultInfo connectingImage];
    }
    else {
        [self deinitImageViews];
        
        self.userActionImageView = [[UIImageView alloc] initWithImage:[quizResultInfo getUserActionImage]];
        [self.contentView addSubview:self.userActionImageView];
        
        self.matchActionImageView = [[UIImageView alloc] initWithImage:[quizResultInfo getMatchActionImage]];
        [self.contentView addSubview:self.matchActionImageView];
    }
    
    CGFloat xInit = 20;
    //CGFloat topYInit = 20;
    //CGFloat bottomYInit = 20;
    CGFloat yOffsetBetweenTitleAndImage = 10;
    CGFloat maxWidth = CGRectGetWidth(self.frame) - (2*xInit);
    CGFloat maxLabelHeightAllowed = 100;
    CGFloat imageViewWidth = 40;
    CGFloat imageViewHeight = 40;
    CGFloat bottomYExtraSpace = 10; //for tick and cross image on imageview
    
    NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
    CGSize constrintSize = CGSizeMake(maxWidth, maxLabelHeightAllowed);
    
    CGRect labelRect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    CGFloat contentHeight = (CGRectGetHeight(labelRect) + yOffsetBetweenTitleAndImage + imageViewHeight + bottomYExtraSpace);
    
    CGFloat titleLabelYPos = (CGRectGetHeight(self.frame) - contentHeight)/2;
    self.titleLabel.frame = CGRectMake(xInit,
                                       titleLabelYPos,
                                       maxWidth,
                                       CGRectGetHeight(labelRect));
    CGFloat imageViewXPos = 44;
    CGFloat imageViewYPos = CGRectGetMaxY(self.titleLabel.frame) + yOffsetBetweenTitleAndImage;
    self.userImageView.frame = CGRectMake(imageViewXPos, imageViewYPos, imageViewWidth, imageViewHeight);
    self.matchImageView.frame = CGRectMake((CGRectGetWidth(self.frame) - (imageViewXPos+imageViewWidth)),
                                           imageViewYPos,
                                           imageViewWidth, imageViewHeight);
    
    if(showConectingImage) {
        self.seperatorView.frame = CGRectMake(CGRectGetMaxX(self.userImageView.frame),
                                                  CGRectGetMidY(self.userImageView.frame),
                                                  CGRectGetMinX(self.matchImageView.frame) - CGRectGetMaxX(self.userImageView.frame),
                                                  2);
        
        self.iconImageView.frame = CGRectMake(CGRectGetMidX(self.seperatorView.frame)-15, CGRectGetMidY(self.seperatorView.frame)-15, 30, 30);
    }
    else {
        self.userActionImageView.frame = CGRectMake(CGRectGetMaxX(self.userImageView.frame)-20,
                                                    CGRectGetMaxY(self.userImageView.frame)-20,
                                                    25, 25);
        self.matchActionImageView.frame = CGRectMake(CGRectGetMaxX(self.matchImageView.frame) - 20,
                                                     CGRectGetMaxY(self.matchImageView.frame) - 20,
                                                     25, 25);
    }
}

-(void)deinitImageViews {
    [self.seperatorView removeFromSuperview];
    self.seperatorView = nil;
    [self.iconImageView removeFromSuperview];
    self.iconImageView = nil;
    [self.userActionImageView removeFromSuperview];
    self.userActionImageView = nil;
    [self.matchActionImageView removeFromSuperview];
    self.matchActionImageView = nil;
}

@end

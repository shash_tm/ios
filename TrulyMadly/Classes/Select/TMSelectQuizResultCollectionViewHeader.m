//
//  TMSelectQuizResultCollectionViewHeader.m
//  TrulyMadly
//
//  Created by Ankit on 06/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizResultCollectionViewHeader.h"
#import "TMUserSession.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+TMAdditions.h"


@interface TMSelectQuizResultCollectionViewHeader ()

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *subtitleLabel;
@property(nonatomic,strong)UILabel *dotLabel;
@property(nonatomic,strong)UIImageView *userImageView;
@property(nonatomic,strong)UIImageView *matchImageView;

@property(nonatomic,assign)CGFloat imageviewHeight;
@property(nonatomic,assign)CGFloat titleTextHeight;
@property(nonatomic,assign)CGFloat subtitleTextHeight;

@end

@implementation TMSelectQuizResultCollectionViewHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat width = 75;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.subtitleLabel.numberOfLines = 0;
        self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
        self.subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
        [self addSubview:self.subtitleLabel];
        
        self.dotLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.dotLabel.font = [UIFont systemFontOfSize:70];
        self.dotLabel.text = @"...";
        self.dotLabel.textColor = [UIColor blackColor];
        self.dotLabel.backgroundColor = [UIColor clearColor];
        self.dotLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.dotLabel];
        
        self.userImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.userImageView.layer.cornerRadius = width/2;
        self.userImageView.clipsToBounds = TRUE;
        [self addSubview:self.userImageView];
        
        self.matchImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.matchImageView.layer.cornerRadius = width/2;
        self.matchImageView.clipsToBounds = TRUE;
        [self addSubview:self.matchImageView];
    }
    return self;
}

+(CGFloat)contentHeightWithTitle:(NSString*)titleText subTitle:(NSString*)subTitleText maxWidth:(CGFloat)maxWidth {
    CGFloat titleLabelHeight;
    CGFloat subtitleLabelHeight;
    CGFloat titleLabelXPos = 20;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:16]};
    CGSize constrintSize = CGSizeMake(maxWidth - (2*titleLabelXPos), 200);
    CGRect titleLabelRect = [titleText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    titleLabelHeight = CGRectGetHeight(titleLabelRect);
    
    attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]};
    constrintSize = CGSizeMake(maxWidth - (2*titleLabelXPos), 100);
    CGRect subtitleLabelRect = [titleText boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    subtitleLabelHeight = CGRectGetHeight(subtitleLabelRect);
    
    CGFloat imageviewHeight = 75;
    CGFloat yOffsetBetweenImageAndTitleLabel = 15;
    CGFloat yOffsetBetweenTitleLabelAndSubTitle = 10;
    CGFloat yOffsetAfterSubTitleLabel = 40;
    CGFloat contentHeight = imageviewHeight + yOffsetBetweenImageAndTitleLabel + titleLabelHeight +
                            yOffsetBetweenTitleLabelAndSubTitle + subtitleLabelHeight+ yOffsetAfterSubTitleLabel;
    return contentHeight;
}

-(void)layoutSubviews {
    [super layoutSubviews];

    CGFloat width = 75;
    CGFloat xPos = (CGRectGetWidth(self.frame) - ((2*width)-20))/2; //20 => image overlap width
    CGFloat yPos = 0;
    self.userImageView.frame = CGRectMake(xPos, yPos, width, width);
    
    self.matchImageView.frame = CGRectMake(CGRectGetMaxX(self.userImageView.frame)-20, yPos, width, width);
    
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    CGFloat titleLabelXPos = 20;
    
    NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
    CGSize constrintSize = CGSizeMake(maxWidth - (2*titleLabelXPos), 200);
    CGRect titleLabelRect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    CGFloat titleLabelHeight = CGRectGetHeight(titleLabelRect);
    self.titleLabel.frame = CGRectMake(titleLabelXPos,
                                       CGRectGetMaxY(self.userImageView.frame)+15,
                                       CGRectGetWidth(self.frame) - (2*titleLabelXPos), titleLabelHeight);
    
    attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]};
    constrintSize = CGSizeMake(maxWidth - (2*titleLabelXPos), 100);
    CGRect subtitleLabelRect = [self.subtitleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    CGFloat subtitleLabelHeight = CGRectGetHeight(subtitleLabelRect);
    self.subtitleLabel.frame = CGRectMake(titleLabelXPos,
                                       CGRectGetMaxY(self.titleLabel.frame)+10,
                                       CGRectGetWidth(self.frame) - (2*titleLabelXPos), subtitleLabelHeight);
    
    self.dotLabel.frame = CGRectMake(titleLabelXPos,
                                     CGRectGetMaxY(self.subtitleLabel.frame),
                                     CGRectGetWidth(self.frame) - (2*titleLabelXPos), 20);
}

-(void)setMatchImageFromURL:(NSURL*)imageURL {
    [self.userImageView setImageWithURL:[NSURL URLWithString:[TMUserSession sharedInstance].user.profilePicURL]];
    [self.matchImageView setImageWithURL:imageURL];
}

-(void)setTitleText:(NSString*)titleText subTitleText:(NSString*)subTitleText {
    self.titleLabel.text = titleText;
    if(subTitleText && [subTitleText isKindOfClass:[NSString class]]) {
        self.subtitleLabel.text = [NSString stringWithFormat:@"\"%@\"",subTitleText];
    }
}

@end

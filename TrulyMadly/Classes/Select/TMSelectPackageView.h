//
//  TMSelectPackageView.h
//  TrulyMadly
//
//  Created by Suruchi Sinha on 1/13/17.
//  Copyright © 2017 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectPackBuyViewDelegate;
@class TMPackageInfo;

@interface TMSelectPackageView : UIView

@property(nonatomic,weak)id<TMSelectPackBuyViewDelegate> delegate;

- (void)loadPackageData:(NSArray *)list;
@end

@protocol TMSelectPackBuyViewDelegate <NSObject>

-(void)didBuySelectPackAtIndex:(NSInteger)selectedIndex;
-(void)didSelectMatchGuranteeTNC;

@end

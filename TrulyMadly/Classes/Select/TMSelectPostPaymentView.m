//
//  TMSelectPostPaymentView.m
//  TrulyMadly
//
//  Created by Ankit on 10/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectPostPaymentView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMUserSession.h"
#import "NSString+TMAdditions.h"

@interface TMSelectPostPaymentView ()

@property(nonatomic,strong)UIScrollView *contentView;
@property(nonatomic,strong)UILabel *topLabel;
@property(nonatomic,strong)UILabel *descriptionLabel;
@property(nonatomic,strong)UILabel *bottomLabel;
@property(nonatomic,strong)UIButton *letsbeginButton;

@end

@implementation TMSelectPostPaymentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.contentView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:self.contentView];
        
    }
    return self;
}

-(void)continueAction {
    [self.delegate didClickToLetsBeginButton];
}

-(void)showDataWithDescriptionText:(BOOL)showDescriptionText {
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    CGFloat titleLabelXPos = 20;
    CGFloat titleLabelYPos = (showDescriptionText) ? 100 : 150;
    
    self.topLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos, titleLabelYPos,
                                                              maxWidth - (2*titleLabelXPos), 60)];
    self.topLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    self.topLabel.textColor = [UIColor blackColor];//[UIColor colorWithRed:170/255.0f green:105/255.0f blue:163/255.0f alpha:1];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    self.topLabel.text  = [NSString stringWithFormat:@"Congratulations %@!",[TMUserSession sharedInstance].user.fName];
    [self.contentView addSubview:self.topLabel];
    
    if(showDescriptionText) {
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos,
                                                                          CGRectGetMaxY(self.topLabel.frame)+20,
                                                                          maxWidth - (2*titleLabelXPos), 80)];
        self.descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
        self.descriptionLabel.textColor = [UIColor blackColor];
        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.descriptionLabel.numberOfLines = 0;
        self.descriptionLabel.text  = @"You are now one step closer to finding The One by becoming a member of TrulyMadly Select.";
        [self.contentView addSubview:self.descriptionLabel];
    }
    
    CGFloat bottomLabelTextYPos = (showDescriptionText) ? CGRectGetMaxY(self.descriptionLabel.frame) : CGRectGetMaxY(self.topLabel.frame);
    NSString *bottomLabelText = (showDescriptionText) ? @"Enjoy the all-new TM experience!" : @"Your TrulyMadly Select experience has been successfully extended";
    self.bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos,
                                                                 bottomLabelTextYPos+20,
                                                                 maxWidth - (2*titleLabelXPos), 80)];
    self.bottomLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
    self.bottomLabel.textColor = [UIColor blackColor];//[UIColor colorWithRed:170/255.0f green:105/255.0f blue:163/255.0f alpha:1];
    self.bottomLabel.textAlignment = NSTextAlignmentCenter;
    self.bottomLabel.text  = bottomLabelText;
    self.bottomLabel.numberOfLines = 0;
    [self.contentView addSubview:self.bottomLabel];
    
    CGFloat actionButtonWidth = 200;
    self.letsbeginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.letsbeginButton.frame = CGRectMake((maxWidth - actionButtonWidth)/2,
                                            CGRectGetMaxY(self.bottomLabel.frame)+40,
                                            actionButtonWidth, 50);
    [self.letsbeginButton setTitle:@"LET’S BEGIN" forState:UIControlStateNormal];
    self.letsbeginButton.backgroundColor = [UIColor likeColor];
    self.letsbeginButton.layer.cornerRadius = 10;
    [self.letsbeginButton addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.letsbeginButton];
}

@end

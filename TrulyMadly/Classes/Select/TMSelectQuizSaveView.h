//
//  TMSelectQuizCSaveView.h
//  TrulyMadly
//
//  Created by Ankit on 04/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectQuizSaveViewDelegate;

@interface TMSelectQuizSaveView : UIView

@property(nonatomic,weak)id<TMSelectQuizSaveViewDelegate> delegate;

-(void)updateButtonToDefaultState;
-(void)updateButtonStateForDataLoading;

@end

@protocol TMSelectQuizSaveViewDelegate <NSObject>

-(void)didSaveQuiz;

@end

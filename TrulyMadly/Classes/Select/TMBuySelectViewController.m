//
//  TMBuySelectViewController.m
//  TrulyMadly
//
//  Created by Ankit on 07/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBuySelectViewController.h"
#import "TMSelectQuizViewController.h"
#import "TMSelectManager.h"
#import "TMActivityIndicatorView.h"
#import "TMSelectPostPaymentView.h"
#import "TMIAPManager.h"
#import "TMErrorMessages.h"
#import "TMError.h"
#import "TMPayment.h"
#import "UIColor+TMColorAdditions.h"
#import "TMPackageInfo.h"
#import "TMUserSession.h"
#import "TMAnalytics.h"
#import "TMSelectPackageView.h"
#import "TMBuySelectTNCViewController.h"

@interface TMBuySelectViewController ()<TMSelectPostPaymentViewDelegate,TMIAPManagerDelegate,
                                        TMSelectQuizViewControllerDelegate,TMSelectPackBuyViewDelegate>

@property(nonatomic,strong)TMSelectManager *selectManager;
@property(nonatomic,strong)UIView* contentView;
@property(nonatomic,strong)UIView* headerView;
@property(nonatomic,strong)TMActivityIndicatorView* activityIndicatorView;
@property(nonatomic,strong)TMSelectPackageView* selectPackBuyView;
@property(nonatomic,strong)TMSelectPostPaymentView* selectPostPaymentView;
@property(nonatomic,strong)UILabel* textLabel;
@property(nonatomic,strong)UIButton* retryButton;

@property(nonatomic,strong)NSArray* selectPackages;
@property(nonatomic,strong)NSDictionary *purchasedPackage;
//for tracking purpose
@property(nonatomic,strong)NSString* source;

@end

@implementation TMBuySelectViewController

- (instancetype)initWithSource:(NSString*)source
{
    self = [super init];
    if (self) {
        self.source = source;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = TRUE;
    [self setupHeaderView];
    [self setupContentView];
    [self getSelectPackage];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Getter -
-(TMSelectManager*)selectManager {
    if(!_selectManager) {
        _selectManager = [[TMSelectManager alloc] init];
    }
    return _selectManager;
}
-(TMSelectPostPaymentView*)selectPostPaymentView {
    if(!_selectPostPaymentView) {
        _selectPostPaymentView = [[TMSelectPostPaymentView alloc] initWithFrame:self.view.bounds];
        _selectPostPaymentView.delegate = self;
    }
    return _selectPostPaymentView;
}
-(TMSelectPackageView*)selectPackBuyView {
    if(!_selectPackBuyView) {
        _selectPackBuyView = [[TMSelectPackageView alloc] initWithFrame:CGRectMake(0,
                                                                                   0,
                                                                                   CGRectGetWidth(self.contentView.frame),
                                                                                   CGRectGetHeight(self.contentView.frame))];
        _selectPackBuyView.delegate = self;
    }
    return _selectPackBuyView;
}

#pragma mark Packge Handling -
-(void)getSelectPackage {
    if(self.selectManager.isNetworkReachable) {
        [self showDefaultLoadingtStateWithText:@"Loading Select Packages..."];
        [self.selectManager getSelectPackages:^(NSArray *packages, TMError *error) {
            [self removerAllContentViews];
            if(packages.count) {
                self.selectPackages = packages;
                [self showSelectPackages:packages];
            }
            else {
                NSString *errorMessage = (error.errorCode == TMERRORCODE_NONETWORK) ? TM_INTERNET_NOTAVAILABLE_MSG:
                @"Problem in getting select packages. Please try again";
                [self showRetryViewWithMessage:errorMessage];
            }
        }];
    }
    else {
        NSString *errorMessage = TM_INTERNET_NOTAVAILABLE_MSG;
        [self showRetryViewWithMessage:errorMessage];
    }
}
-(void)buyFreePackage {
    if(self.selectManager.isNetworkReachable) {
        [self.selectManager buyFreeSelectPackage:^(TMError *error) {
            self.view.userInteractionEnabled = TRUE;
            [self removerAllContentViews];
            if(!error) {
                [self showPostPaymentViewWithFirsPaymentDescription:TRUE];
                [self trackSelectBuyFreePackEventWithStatus:@"success" packageId:nil error:nil];
            }
            else {
                NSString *errorMessage = (error.errorCode == TMERRORCODE_NONETWORK) ? TM_INTERNET_NOTAVAILABLE_MSG:
                @"Problem in buying select package. Please try again";
                [self showRetryViewWithMessage:errorMessage];
                [self trackSelectBuyFreePackEventWithStatus:@"tm_error" packageId:nil error:error.errorMessage];
            }
        }];
    }
    else {
        NSString *errorMessage =  TM_INTERNET_NOTAVAILABLE_MSG;
        [self showRetryViewWithMessage:errorMessage];
    }
}
-(void)deleteSelectPackage {
    [self.selectManager deleteFreeTrialPackage:^{
        
    }];
}

#pragma mark View Setup -
-(void)setupHeaderView {
    CGFloat headerViewHeight = 30;
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.view.frame), headerViewHeight)];
    [self.view addSubview:self.headerView];
    
    UIButton *cancelActionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelActionButton addTarget:self action:@selector(cancelButtonAciton) forControlEvents:UIControlEventTouchUpInside];
    cancelActionButton.backgroundColor = [UIColor whiteColor];
    cancelActionButton.frame = CGRectMake(0, 0, 60, 44);
    [self.headerView addSubview:cancelActionButton];
    
    CGFloat iconWidth = 12;
    CGFloat iconYPos = (headerViewHeight-iconWidth)/2;
    UIImageView *crossIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15,iconYPos,iconWidth,iconWidth)];
    crossIcon.image = [UIImage imageNamed:@"sparkbuyclose"];
    [self.headerView addSubview:crossIcon];
}
-(void)setupContentView {
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                CGRectGetMaxY(self.headerView.frame),
                                                                CGRectGetWidth(self.view.frame),
                                                                CGRectGetHeight(self.view.frame) - CGRectGetMaxY(self.headerView.frame))];
    [self.view addSubview:self.contentView];
}
-(void)removerAllContentViews {
    NSArray *subviews = [self.contentView subviews];
    for (UIView *view in subviews) {
        [view removeFromSuperview];
    }
}
-(void)showSelectPackages:(NSArray*)packages {
    if(!self.selectPackBuyView.superview) {
        [self.contentView addSubview:self.selectPackBuyView];
    }
    [self.selectPackBuyView loadPackageData:packages];
    
    if(packages.count == 1) {
        [self trackSelectBuyFreePackEventWithStatus:@"view" packageId:nil error:nil];
    }
    else {
        [self trackSelectBuyPaidPackEventWithStatus:@"view" packageId:nil error:nil];
    }
}
-(void)showPostPaymentViewWithFirsPaymentDescription:(BOOL)isFirstPayment {
    BOOL isQuizPlayed = [[TMUserSession sharedInstance].user isQuizPlayed];
    if(!isQuizPlayed) {
        [[TMUserSession sharedInstance] setSelectQuizAsStarted];
    }
    if(!self.selectPostPaymentView.superview) {
        [self.view addSubview:self.selectPostPaymentView];
    }
    
    [self.selectPostPaymentView showDataWithDescriptionText:isFirstPayment];
}
-(void)showDefaultLoadingtStateWithText:(NSString*)loadingText {
    [self removerAllContentViews];
    
    [self showActivityIndicator];
    
    CGRect frame = self.view.frame;
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(frame)-200)/2,
                                                               CGRectGetMaxY(self.activityIndicatorView.frame),
                                                               200, 40)];
    self.textLabel.text = loadingText;
    self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.textLabel];
}
-(void)showRetryViewWithMessage:(NSString *)message {
    CGFloat titleLabelHeight = 60;
    CGFloat yOffsetBetweenLabelAndButton = 20;
    CGFloat buttonHeight = 50;
    CGFloat totalHeight = (titleLabelHeight + yOffsetBetweenLabelAndButton + buttonHeight);
    CGFloat initYPos = (CGRectGetHeight(self.view.frame) - totalHeight)/2;
    CGFloat xPos = 20;
    CGFloat width = CGRectGetWidth(self.view.frame) - (2 * xPos);
    
    NSString *text = (message) ? message : @"Error in completing payment. Please trying again";
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos,initYPos,width,titleLabelHeight)];
    self.textLabel.text = text
    ;
    self.textLabel.numberOfLines = 0;
    self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.textLabel];
   
    CGFloat buttonWidth = 200;
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame)-buttonWidth)/2,
                                                                      CGRectGetMaxY(self.textLabel.frame)+yOffsetBetweenLabelAndButton,
                                                                      buttonWidth,
                                                                      buttonHeight)];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitle:@"Retry" forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    sendButton.backgroundColor = [UIColor likeColor];
    sendButton.layer.cornerRadius = 10;
    [sendButton addTarget:self action:@selector(buyPackageRetryButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:sendButton];
}
-(void)showActivityIndicator {
    CGRect frame = self.view.frame;
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] initWithFrame:CGRectZero];
    self.activityIndicatorView.titleText = nil;
    self.activityIndicatorView.frame = CGRectMake((CGRectGetWidth(frame) - CGRectGetWidth(self.activityIndicatorView.frame))/2,
                                                  ((CGRectGetHeight(frame) - CGRectGetHeight(self.activityIndicatorView.frame))/2)-60,
                                                  CGRectGetWidth(self.activityIndicatorView.frame),
                                                  CGRectGetHeight(self.activityIndicatorView.frame));
    [self.contentView addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
}
-(void)removeActivityIndicator {
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
}
-(void)updatePaymentStateWithMessage:(NSString*)message {
    self.textLabel.text = message;
}

#pragma mark Action / Delegate Handler -
-(void)cancelButtonAciton {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
-(void)buyPackageRetryButtonClick {
    [self getSelectPackage];
}
-(void)didClickToLetsBeginButton {
    BOOL isQuizPlayed = [[TMUserSession sharedInstance].user isQuizPlayed];
    if(isQuizPlayed) {
        [TMUserSession sharedInstance].isUserProfileUpdated = TRUE;
        if([TMUserSession sharedInstance].selectBuyActionOnSelectProfile) {
            [TMUserSession sharedInstance].needToLoadProfileOnSelectPayment = TRUE;
        }
    
        [self dismissViewControllerAnimated:TRUE completion:nil];
    }
    else {
        [[TMUserSession sharedInstance] setSelectQuizAsStarted];
        TMSelectQuizViewController *quizViewController = [[TMSelectQuizViewController alloc] init];
        quizViewController.quizDelegate = self;
        [self.navigationController pushViewController:quizViewController animated:YES];
    }
}
-(void)didBuySelectPackAtIndex:(NSInteger)selectedIndex {
    NSDictionary *packageDictionary = self.selectPackages[selectedIndex];
    self.purchasedPackage = packageDictionary;
    TMPackageInfo *packageInfo = [[TMPackageInfo alloc] initWithDictionary:packageDictionary];
    if([[TMIAPManager sharedManager] canConnect]) {
        [self showDefaultLoadingtStateWithText:@"Initiating Payment..."];
        self.view.userInteractionEnabled = FALSE;
        
        if(packageInfo.packageType == FreePackage) {
            [self buyFreePackage];
            [self trackSelectBuyFreePackEventWithStatus:@"clicked" packageId:nil error:nil];
        }
        else {
            [[TMIAPManager sharedManager] buyPackage:Select withProductIdentifier:packageInfo.packageIdentifier withDelegateHandler:self];
            [self trackSelectBuyPaidPackEventWithStatus:@"buy" packageId:packageInfo.packageIdentifier error:nil];
        }
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TM_INTERNET_NOTAVAILABLE_MSG
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
}
-(void)didUpdateTransactionStatus:(NSString*)paymentStatus {
    [self updatePaymentStateWithMessage:paymentStatus];
}
-(void)didFinishPaymentTransaction:(TMPayment*)payment {
    self.view.userInteractionEnabled = TRUE;
    [self removerAllContentViews];
    if(payment.paymentStatus == success) {
        BOOL isQuizPlayed = [[TMUserSession sharedInstance].user isQuizPlayed];
        [self showPostPaymentViewWithFirsPaymentDescription:(!isQuizPlayed)];
        [self trackBuySelectEvent];
        [self trackSelectBuyPaidPackEventWithStatus:@"success" packageId:payment.packageIdentifier error:nil];
    }
    else {
        [self showRetryViewWithMessage:payment.errorDisplayText];
        [self trackSelectBuyPaidPackEventWithStatus:@"tm_error" packageId:payment.packageIdentifier error:payment.errorTrackingText];
    }
}
-(void)didCompleteQuiz {
    [self dismissViewControllerAnimated:TRUE completion:^{
        [self.navigationController popToRootViewControllerAnimated:FALSE];
    }];
}

-(void)didSelectMatchGuranteeTNC {
    TMBuySelectTNCViewController *tncViewController = [[TMBuySelectTNCViewController alloc] init];
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
    tncViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:tncViewController animated:TRUE completion:nil];
    
}
#pragma mark Tracking -

-(void)trackSelectBuyFreePackEventWithStatus:(NSString*)status packageId:(NSString*)packageId error:(NSString*)error {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSInteger likeCounter = [[TMUserSession sharedInstance] getLikeActionCounter];
    NSInteger hideCounter = [[TMUserSession sharedInstance] getHideActionCounter];
    NSInteger sparkCounter = [[TMUserSession sharedInstance] getSparkActionCounter];
    
    if(self.matchId) {
        eventInfo[@"match_id"] = self.matchId;
    }
    if(packageId) {
        eventInfo[@"pkg_id"] = packageId;
    }
    eventInfo[@"source"] = self.source;
    eventInfo[@"error"] = error;
    eventInfo[@"likes_done"] = @(likeCounter);
    eventInfo[@"hides_done"] = @(hideCounter);
    eventInfo[@"sparks_done"] = @(sparkCounter);
    
    [[TMAnalytics sharedInstance] trackSelectEvent:@"buy_trial" status:status eventInfo:eventInfo];
}
-(void)trackSelectBuyPaidPackEventWithStatus:(NSString*)status packageId:(NSString*)packageId error:(NSString*)error {
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    NSInteger likeCounter = [[TMUserSession sharedInstance] getLikeActionCounter];
    NSInteger hideCounter = [[TMUserSession sharedInstance] getHideActionCounter];
    NSInteger sparkCounter = [[TMUserSession sharedInstance] getSparkActionCounter];
    
    if(self.matchId) {
        eventInfo[@"match_id"] = self.matchId;
    }
    if(error) {
        eventInfo[@"error"] = error;
    }
    if(packageId) {
        eventInfo[@"pkg_id"] = packageId;
    }
    eventInfo[@"source"] = self.source;
    eventInfo[@"likes_done"] = @(likeCounter);
    eventInfo[@"hides_done"] = @(hideCounter);
    eventInfo[@"sparks_done"] = @(sparkCounter);

    [[TMAnalytics sharedInstance] trackSelectEvent:@"buy" status:status eventInfo:eventInfo];
}

- (void)trackBuySelectEvent {
    NSMutableDictionary *fbEventInfo = [NSMutableDictionary dictionaryWithCapacity:5];
    if(self.purchasedPackage != nil) {
        fbEventInfo[@"amount"] = [NSNumber numberWithDouble:[self.purchasedPackage[@"price"] doubleValue]];
        fbEventInfo[@"number_of_items"] = [NSNumber numberWithInt:[self.purchasedPackage[@"spark_count"] intValue]];
        if([self.purchasedPackage[@"currency"] isEqualToString:@"₹"]) {
            fbEventInfo[@"currency"] = @"INR";
        }else {
            fbEventInfo[@"currency"] = @"USD";
        }
        fbEventInfo[@"content_type"] = self.purchasedPackage[@"type"] ;
        fbEventInfo[@"package_identifier"] = self.purchasedPackage[@"apple_sku"];
        self.purchasedPackage = nil;
        [[TMAnalytics sharedInstance] trackFacebookEvent:@"buy_spark" withParams:fbEventInfo];
    }
}

@end

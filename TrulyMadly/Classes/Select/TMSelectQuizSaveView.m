//
//  TMSelectQuizCSaveView.m
//  TrulyMadly
//
//  Created by Ankit on 04/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizSaveView.h"
#import "TMToastView.h"
#import "UIColor+TMColorAdditions.h"


@interface TMSelectQuizSaveView ()

@property(nonatomic,strong)UIScrollView *contentView;
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *subtitleLabel;
@property(nonatomic,strong)UIButton *quizsaveButton;
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicatorView;

@end


@implementation TMSelectQuizSaveView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:self.contentView];
        
        CGFloat imageViewXPos = 35;
        CGFloat imageViewWidth = CGRectGetWidth(self.frame) - (2*imageViewXPos);
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewXPos, 10, imageViewWidth, imageViewWidth)];
        self.imageView.image = [UIImage imageNamed:@"selectquizend"];
        self.imageView.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:self.imageView];
        
        CGFloat titleLabelXPos = imageViewXPos;
        CGFloat titleLabelYPos = CGRectGetMaxY(self.imageView.frame)+20;
        CGFloat titleLabelWidth = imageViewWidth;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos, titleLabelYPos, titleLabelWidth, 30)];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
        self.titleLabel.text = @"All done!";
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.titleLabel];
        
        CGFloat subtitleLabelXPos = imageViewXPos;
        CGFloat subtitleLabelYPos = CGRectGetMaxY(self.titleLabel.frame)+20;
        CGFloat subtitleLabelWidth = imageViewWidth;
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(subtitleLabelXPos, subtitleLabelYPos, subtitleLabelWidth, 60)];
        self.subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
        self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
        self.subtitleLabel.numberOfLines = 0;
        self.subtitleLabel.text = @"Check out your potential Select matches and view your Compatibility Quotient with them.";
        [self.contentView addSubview:self.subtitleLabel];
        
        CGFloat buttonXPos = imageViewXPos;
        CGFloat buttonYPos = CGRectGetMaxY(self.subtitleLabel.frame)+20;
        CGFloat buttonWidth = imageViewWidth;
        self.quizsaveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.quizsaveButton.frame = CGRectMake(buttonXPos, buttonYPos, buttonWidth, 50);
        self.quizsaveButton.layer.cornerRadius = 10;
        self.quizsaveButton.backgroundColor = [UIColor likeColor];
        [self.quizsaveButton setTitle:@"LET’S GO" forState:UIControlStateNormal];
        [self.quizsaveButton addTarget:self action:@selector(quizSaveAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.quizsaveButton];
        
        self.contentView.contentSize = CGSizeMake(CGRectGetWidth(self.contentView.frame), CGRectGetMaxY(self.quizsaveButton.frame)+30);
        
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
}

-(void)quizSaveAction {
    [self.delegate didSaveQuiz];
}

-(void)updateButtonStateForDataLoading {
    if(!self.activityIndicatorView) {
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.activityIndicatorView.hidesWhenStopped = TRUE;
        self.activityIndicatorView.center = self.quizsaveButton.center;
        [self.contentView addSubview:self.activityIndicatorView];
    }
    [self.activityIndicatorView startAnimating];
    
    self.quizsaveButton.userInteractionEnabled = FALSE;
    [self.quizsaveButton setTitle:nil forState:UIControlStateNormal];
}

-(void)updateButtonToDefaultState {
    self.quizsaveButton.userInteractionEnabled = TRUE;
    [self.quizsaveButton setTitle:@"LET’S GO" forState:UIControlStateNormal];
    [self.activityIndicatorView stopAnimating];
}


@end

//
//  TMSelectQuizViewController.h
//  TrulyMadly
//
//  Created by Ankit on 28/10/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"

@protocol TMSelectQuizViewControllerDelegate;

@interface TMSelectQuizViewController : TMBaseViewController

@property(nonatomic,weak)id<TMSelectQuizViewControllerDelegate> quizDelegate;

@end


@protocol TMSelectQuizViewControllerDelegate <NSObject>

-(void)didCompleteQuiz;

@end

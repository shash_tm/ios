//
//  TMQuizCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 01/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSelectQuizCollectionViewCell : UICollectionViewCell

+(NSString*)reusableIdentifier;
-(void)setImageFromURL:(NSURL*)imageURL;
-(void)setQuizText:(NSString*)quizText;

@end

//
//  TMSelectPackageView.m
//  TrulyMadly
//
//  Created by Suruchi Sinha on 1/13/17.
//  Copyright © 2017 trulymadly. All rights reserved.
//

#import "TMSelectPackageView.h"
#import "TMSelectPackageListViewCell.h"
#import "DDPageControl.h"
#import "TMPackageInfo.h"
#import "NSString+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"

@interface TMSelectPackageView() <UIScrollViewDelegate, UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong)UITableView *selectPackTableView;
@property(nonatomic, strong)UIScrollView *contentScrollView;
@property(nonatomic, strong)DDPageControl *pageControl;
@property(nonatomic, strong)UIView *contentView;
@property(nonatomic, strong)UIView *tncView;
@property(nonatomic, strong)UIView *headerView;
@property(nonatomic, strong)NSArray *packageList;
@property(nonatomic, strong)UILabel *footerLabel;
@property(nonatomic, strong)UIButton *actionButton;
@property(nonatomic, strong)UILabel *headerTitle;
@property(nonatomic, strong)UIImageView *selectImageView;
@property(nonatomic, strong)UILabel *subtitleLabel;
@end

@implementation TMSelectPackageView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
    }
    return self;
    
}

- (void)loadPackageData:(NSArray *)list {
    self.packageList = list;
    [self setUpHeaderView];
    [self setupContentView];
}

- (void)setUpHeaderView {
    
    if([self.headerView superview]) {
        [self.headerView removeFromSuperview];
        self.headerView = nil;
    }
    
    CGFloat yPos, height, width, pageContentViewXPos, pageContentViewYPos, pageContentWidth, pageContentHeight, contentWidth, headerViewHeight;
    NSArray *paginationText, *paginationImages;
    yPos = 0;
    
    if(self.packageList.count == 1) {
        headerViewHeight = CGRectGetHeight(self.frame) * 0.7;
    }else {
        headerViewHeight = CGRectGetHeight(self.frame) * 0.4;
    }
    height = CGRectGetHeight(self.frame) * 0.4;
    width = CGRectGetWidth(self.frame);
    
    if(fabs(width) < 375) {
        paginationImages = @[@"selectBuyPage1", @"selectBuyPage2", @"selectBuyPage3", @"selectBuyPage4"];
    }else {
        paginationImages = @[@"buySelectPage1", @"buySelectPage2", @"buySelectPage3", @"buySelectPage4"];
    }
    paginationText = @[@"Exclusive community of singles looking for long term commitment",
                       @"Take our relationship quiz for deeper matching",
                       @"Enjoy An Ad-Free experience",
                       @"Consult with our TM Relationship Expert"];
    
    
    
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, headerViewHeight + 20)];
    //View appears only in case of free trial
    if(self.packageList.count == 1) {
        
        self.headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 25)];
        self.headerTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        self.headerTitle.textColor = [UIColor colorWithRed:126/255.0f green:38/255.0f blue:135/255.0f alpha:1];
        self.headerTitle.textAlignment = NSTextAlignmentCenter;
        self.headerTitle.text  = @"Introducing";
        
        CGFloat selectImageViewWidth = 140;
        CGFloat selectImageViewHeight = 70;
        CGFloat selectImageViewXPos = (width - selectImageViewWidth)/2;
        CGFloat selectImageViewYPos = CGRectGetMaxY(self.headerTitle.frame)+ 15;
        CGRect selectImageViewRect = CGRectMake(selectImageViewXPos, selectImageViewYPos, selectImageViewWidth, selectImageViewHeight);
        self.selectImageView = [[UIImageView alloc] initWithFrame:selectImageViewRect];
        self.selectImageView.image = [UIImage imageNamed:@"selectunit"];
        
        
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.selectImageView.frame)+10, width, 20)];
        self.subtitleLabel.textColor = [UIColor colorWithRed:126/255.0f green:38/255.0f blue:135/255.0f alpha:1];
        self.subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
        self.subtitleLabel.text  = @"The perfect way to find ‘The One’";
        
        [self.headerView addSubview:self.headerTitle];
        [self.headerView addSubview:self.selectImageView];
        [self.headerView addSubview:self.subtitleLabel];
        yPos = CGRectGetMaxY(self.subtitleLabel.frame);
        height = CGRectGetHeight(self.headerView.frame) - CGRectGetMaxY(self.subtitleLabel.frame);
    }
    
    self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yPos, width, height)];
    self.contentScrollView.delegate = self;
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    self.contentScrollView.showsHorizontalScrollIndicator = FALSE;
    self.contentScrollView.pagingEnabled = TRUE;
    
    pageContentViewXPos = 0;
    pageContentViewYPos = 0;
    pageContentWidth = CGRectGetWidth(self.contentScrollView.frame);
    pageContentHeight = CGRectGetHeight(self.contentScrollView.frame);
    contentWidth = CGRectGetWidth(self.contentScrollView.frame);
    
    for (int pageCount = 0; pageCount < paginationText.count; pageCount++) {
        UIView *pageView;
        UIImageView *imageView;
        UILabel *textLabel;
        CGRect textFrame;
        CGSize constraintSize;
        UIFont *textFont;
        CGFloat xPos, screenWidth;
        
        xPos = 8;
        pageView = [[UIView alloc] initWithFrame:CGRectMake(pageContentViewXPos, pageContentViewYPos, pageContentWidth, pageContentHeight)];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, 0, pageContentWidth - 2 *xPos, pageContentHeight - 60)];
        textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        imageView.image = [UIImage imageNamed:paginationImages[pageCount]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.clipsToBounds = TRUE;
        imageView.backgroundColor = [UIColor clearColor];
        
        constraintSize = CGSizeMake(pageContentWidth - 30, 200);
        textLabel.text = paginationText[pageCount];
        screenWidth = CGRectGetWidth(self.frame);
        if(fabs(screenWidth) < 375) {
            textFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        }else {
            textFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
        }
        
        textFrame = [textLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:textFont}];
        textLabel.frame = CGRectMake(15, pageContentHeight - 60, pageContentWidth - 30, CGRectGetHeight(textFrame));
        textLabel.font = textFont;
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.textColor = [UIColor blackColor];
        textLabel.numberOfLines = 0;
        
        pageContentViewXPos = pageContentViewXPos + pageContentWidth;
        contentWidth = CGRectGetMaxX(pageView.frame);
        
        [pageView addSubview:imageView];
        [pageView addSubview:textLabel];
        [self.contentScrollView addSubview:pageView];
    }
    self.contentScrollView.contentSize = CGSizeMake(contentWidth, pageContentHeight);
    
    self.pageControl = [[DDPageControl alloc] initWithFrame:CGRectZero];
    [self.pageControl setType: DDPageControlTypeOnFullOffEmpty] ;
    [self.pageControl setOnColor: [UIColor blackColor]];
    [self.pageControl setOffColor: [UIColor blackColor]];
    [self.pageControl setIndicatorDiameter: 8.0f];
    [self.pageControl setIndicatorSpace: 5.0f];
    [self.pageControl setNumberOfPages:paginationText.count];
    self.pageControl.frame = CGRectMake((width - CGRectGetWidth(self.pageControl.frame))/2,
                                        yPos + pageContentHeight-20,
                                        CGRectGetWidth(self.pageControl.frame),
                                        CGRectGetHeight(self.pageControl.frame));
    
    [self.headerView addSubview:self.contentScrollView];
    [self.headerView addSubview:self.pageControl];
    [self addSubview:self.headerView];
}

- (void)setupContentView {
    if([self.contentView superview]) {
        [self.contentView removeFromSuperview];
        self.contentView = nil;
    }
    CGFloat width, height;
    width = CGRectGetWidth(self.frame);
    height = CGRectGetHeight(self.frame) - CGRectGetHeight(self.headerView.frame);
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame), width, height)];
    [self addSubview:self.contentView];
    if(self.packageList.count ==  1) {
        [self showFreeTrialPack];
    }else {
        [self configurePackageListView];
    }
    
}

- (void)configurePackageListView {
    self.selectPackTableView = [[UITableView alloc] initWithFrame:self.contentView.bounds style:UITableViewStylePlain];
    self.selectPackTableView.delegate = self;
    self.selectPackTableView.dataSource = self;
    self.selectPackTableView.showsVerticalScrollIndicator = false;
    self.selectPackTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.selectPackTableView registerClass:[TMSelectPackageListViewCell class] forCellReuseIdentifier:@"selectPackCell"];
    [self.contentView addSubview:self.selectPackTableView];
}

- (void)setupMatchGuaranteeView {
    if(![self.tncView superview]) {
        UILabel *tncLabel;
        UIImageView *tncInfoView;
        CGRect textFrame;
        CGSize constraintSize;
        CGFloat height, imageHeight;
        UITapGestureRecognizer *tapGesture;
        
        height = 60;
        imageHeight = 16;
        self.tncView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.contentView.frame), height)];
        tncLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        tncLabel.text = @"*Terms & Conditions Applied";
        tncLabel.textColor = [UIColor colorWithRed:(6.0/255.0) green:(69.0/255.0) blue:(173.0/255.0) alpha:1.0];
        tncLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
        
        constraintSize = CGSizeMake(CGRectGetWidth(self.contentView.frame), 44);
        textFrame = [tncLabel.text boundingRectWithConstraintSize:constraintSize attributeDictionary:@{NSFontAttributeName:tncLabel.font}];
        tncLabel.frame = CGRectMake(10, 0, CGRectGetWidth(textFrame), 44);
        
        tncInfoView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tncLabel.frame)+5, (44 - imageHeight)/2, imageHeight, imageHeight)];
        tncInfoView.image = [UIImage imageNamed:@"sparktnc"];
        
        [self.tncView addSubview:tncLabel];
        [self.tncView addSubview:tncInfoView];
        tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapTNCView)];
        [self.tncView addGestureRecognizer:tapGesture];
        self.selectPackTableView.tableFooterView = self.tncView;
    }
}
         
- (void)showFreeTrialPack {
    
    NSDictionary *packageDictionary = self.packageList[0];
    TMPackageInfo *packageInfo = [[TMPackageInfo alloc] initWithDictionary:packageDictionary];
    CGFloat buttonWidth = 200;
    CGFloat buttonHeight = 50;
    CGFloat footerLabelHeight = 0;
    if(![packageInfo.freeTrialText isKindOfClass:[NSNull class]]) {
        footerLabelHeight = 20;
    }

    CGFloat buttonYPos = (CGRectGetHeight(self.contentView.frame) - buttonHeight - footerLabelHeight)/2;
    CGFloat maxWidth = CGRectGetWidth(self.frame);
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = CGRectMake((maxWidth - buttonWidth)/2, buttonYPos, buttonWidth, buttonHeight);
    self.actionButton.backgroundColor = [UIColor likeColor];
    self.actionButton.layer.cornerRadius = 10;
    [self.actionButton addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    [self.actionButton setTitle:[packageInfo selectPackageTitle] forState:UIControlStateNormal];
    [self.contentView addSubview:self.actionButton];
    
    if(![packageInfo.freeTrialText isKindOfClass:[NSNull class]]) {
        self.footerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.footerLabel.frame = CGRectMake(0, CGRectGetMaxY(self.actionButton.frame)+10, maxWidth, footerLabelHeight);
        self.footerLabel.text = packageInfo.freeTrialText;
        self.footerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
        self.footerLabel.textColor = [UIColor colorWithRed:255.0f/255.0f green:0 blue:0 alpha:1];
        self.footerLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.footerLabel];
    }
}

- (void)buyAction {
    [self.delegate didBuySelectPackAtIndex:0];
}

- (void)didTapTNCView {
    [self.delegate didSelectMatchGuranteeTNC];
}

#pragma mark - UITableview datasource / delegate methods
#pragma mark -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.packageList count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == self.packageList.count-1) {
        return 0.5;
    }
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TMSelectPackageListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selectPackCell"];
    TMPackageInfo *packageInfo = [[TMPackageInfo alloc] initWithDictionary:self.packageList[indexPath.section]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell configureCellWithData:packageInfo];
    if(packageInfo.isMatchGuarantee && (!tableView.tableFooterView)) {
        [self setupMatchGuaranteeView];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate didBuySelectPackAtIndex:indexPath.section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:102.0f/255.0f green:102.0F/255.0f blue:102.0F/255.0F alpha:1];
    return headerView;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:102.0f/255.0f green:102.0F/255.0f blue:102.0F/255.0F alpha:1];
    return headerView;
}

#pragma mark - UIScrollview delegate methods
#pragma mark -

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}

@end

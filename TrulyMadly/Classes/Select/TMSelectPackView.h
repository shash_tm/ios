//
//  TMSelectPackView.h
//  TrulyMadly
//
//  Created by Ankit on 25/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectPackViewDelegate;

@interface TMSelectPackView : UIView

@property(nonatomic,weak)id<TMSelectPackViewDelegate> delegate;
@property(nonatomic,assign)NSInteger index;

-(void)setPackPrice:(NSString*)price packDuration:(NSString*)duration priceSubText:(NSString*)subText currency:(NSString *)currency;
-(void)setPackTag:(NSString*)tag withBgColor:(UIColor*)bgColor;

@end

@protocol TMSelectPackViewDelegate <NSObject>

-(void)didTapSelectPackAtIndex:(NSInteger)index;

@end

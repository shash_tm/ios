//
//  TMSelectQuizResultViewController.m
//  TrulyMadly
//
//  Created by Ankit on 03/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizResultViewController.h"
#import "TMSelectQuizResultCollectionViewLayout.h"
#import "TMSelectQuizResultCollectionViewCell.h"
#import "TMSelectQuizResultCollectionViewHeader.h"
#import "TMSelectManager.h"
#import "TMSelectQuizResultInfo.h"
#import "TMErrorMessages.h"
#import "TMError.h"


@interface TMSelectQuizResultViewController ()<UICollectionViewDelegate,
                                               UICollectionViewDataSource>

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)UIView *cancelView;
@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIView *bgView;
@property(nonatomic,strong)NSArray *quizAnswers;
@property(nonatomic,strong)TMSelectManager *selectManager;
@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,strong)NSURL *matchImageURL;
@property(nonatomic,strong)NSString *headerTitle;
@property(nonatomic,strong)NSString *headerSubTitle;

@end

@implementation TMSelectQuizResultViewController

- (instancetype)initWithMatchId:(NSInteger)matchId matchImageURL:(NSURL*)imageURL
                    headerTitle:(NSString*)headerTitle
                 headerSubTitle:(NSString*)subTitle
{
    self = [super init];
    if (self) {
        self.headerTitle = headerTitle;
        self.headerSubTitle = subTitle;
        self.matchId = matchId;
        self.matchImageURL = imageURL;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    self.bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.bgView.backgroundColor = [UIColor blackColor];
    self.bgView.alpha = 0.9;
    [self.view addSubview:self.bgView];
    
    [self showQuizAnswers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat xPos = 0;
    CGFloat yPos = 30;
    CGFloat width = CGRectGetWidth(self.contentView.frame) - (2*xPos);
    CGFloat height = CGRectGetHeight(self.contentView.frame) - (yPos+20);
    self.collectionView.frame = CGRectMake(xPos, yPos, width, height);
}
-(UIView*)cancelView {
    if(!_cancelView) {
        _cancelView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 70, 70)];
        _cancelView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(crossAction)];
        [_cancelView addGestureRecognizer:tapGesture];

        CGFloat iconWidth = 12;
        CGFloat iconYPos = 40;
        UIImageView *crossIcon = [[UIImageView alloc] initWithFrame:CGRectMake(25,iconYPos,iconWidth,iconWidth)];
        crossIcon.image = [UIImage imageNamed:@"sparkbuyclose"];
        [self.view addSubview:crossIcon];
    }
    return _cancelView;
}
-(TMSelectManager*)selectManager {
    if(!_selectManager) {
        _selectManager = [[TMSelectManager alloc] init];
    }
    return _selectManager;
}
-(void)setupContentView {
    CGFloat xPos = 10;
    CGFloat yPos = 20;
    CGFloat width = CGRectGetWidth(self.view.frame) - (2*xPos);
    CGFloat height = CGRectGetHeight(self.view.frame) - (yPos);
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.layer.cornerRadius = 10;
    [self.view addSubview:self.contentView];
    
    [self setupCollectionView];
    
    [self.contentView addSubview:self.cancelView];
}
-(void)setupCollectionView {
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 20;
    flowLayout.minimumInteritemSpacing = 0;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.showsVerticalScrollIndicator = FALSE;
    [self.contentView addSubview:self.collectionView];
    
    [self.collectionView registerClass:[TMSelectQuizResultCollectionViewCell class]
            forCellWithReuseIdentifier:[TMSelectQuizResultCollectionViewCell cellReuseIdentifier]];
    [self.collectionView registerClass:[TMSelectQuizResultCollectionViewHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
}

-(void)showQuizAnswers {
    if(self.selectManager.isNetworkReachable) {
        [self configureViewForDataLoadingStart];
        [self.selectManager getQuizReportWithMatch:self.matchId response:^(NSDictionary *quizReport, TMError *error) {
            [self configureViewForDataLoadingFinish];
            
            if(quizReport) {
                NSDictionary *quizQuestions = quizReport[@"questions"];
                NSMutableArray *data = [NSMutableArray array];
                NSArray *allValues = [quizQuestions allValues];
                for (int i=0; i<allValues.count; i++) {
                    id object = allValues[i];
                    if([object isKindOfClass:[NSDictionary class]]) {
                        [data addObject:object];
                    }
                }
                self.quizAnswers = data;
                [self reloadData];
            }
            else {
                if(error.errorCode == TMERRORCODE_LOGOUT) {
                    [self.actionDelegate setNavigationFlowForLogoutAction:FALSE];
                }
                else if(error.errorCode == TMERRORCODE_NONETWORK || error.errorCode == TMERRORCODE_SERVERTIMEOUT) {
                    [self showRetryViewWithMessage:@"Oops something went wrong.Please try again"];
                }
                else {
                    //unknwon error
                    [self showRetryViewWithMessage:TM_UNKNOWN_MSG];
                }
            }
        }];
    }
    else {
        [self showRetryViewWithMessage:@"No Internet Connectivity"];
    }
}
-(void)configureViewForDataLoadingStart {
    if(self.retryView.superview) {
        [self removeRetryView];
    }
    [self showActivityIndicatorViewWithTranslucentViewAndMessage:@"Loading..."];
}
-(void)configureViewForDataLoadingFinish {
    [self hideActivityIndicatorViewWithtranslucentView];
    if(self.retryView.superview) {
        [self removeRetryView];
    }
}
-(void)showRetryViewWithMessage:(NSString*)message {
    if(!self.retryView.superview) {
        self.isRetryViewShown = TRUE;
        self.retryView = [[TMRetryView alloc] initWithFrame:self.view.bounds message:message];
        [self.view addSubview:self.retryView];
        [self.retryView.retryButton addTarget:self action:@selector(showQuizAnswers) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)removerAllSubviews
{
    NSArray *subviews = [self.view subviews];
    for(UIView *view in subviews) {
        [view removeFromSuperview];
    }
}
-(void)reloadData {
    self.bgView.alpha = 0.5;
    [self setupContentView];

    [self.collectionView reloadData];
}
-(void)crossAction{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
-(TMSelectQuizResultInfo*)getQuizResultInfoFromDictionary:(NSDictionary*)dic {
    TMSelectQuizResultInfo *quizResultInfo = [[TMSelectQuizResultInfo alloc] initWithQuizResultDictionary:dic];
    return quizResultInfo;
}

#pragma mark - Collection view data source
#pragma mark -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.quizAnswers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMSelectQuizResultCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMSelectQuizResultCollectionViewCell cellReuseIdentifier] forIndexPath:indexPath];
    NSDictionary* quizResultDictionary = self.quizAnswers[indexPath.row];
    TMSelectQuizResultInfo *info = [self getQuizResultInfoFromDictionary:quizResultDictionary];
    [cell setupCell:info withMatchImageURL:self.matchImageURL];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat xOffset = 10;
    CGFloat width = CGRectGetWidth(self.contentView.frame);
    return CGSizeMake(width - (2 * xOffset), 150);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGFloat width = CGRectGetWidth(self.contentView.frame);
    CGFloat height = [TMSelectQuizResultCollectionViewHeader contentHeightWithTitle:self.headerTitle
                                                                           subTitle:self.headerSubTitle
                                                                           maxWidth:CGRectGetWidth(self.collectionView.frame)];
    return CGSizeMake(width, height);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    TMSelectQuizResultCollectionViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
    [headerView setMatchImageFromURL:self.matchImageURL];
    [headerView setTitleText:self.headerTitle subTitleText:self.headerSubTitle];
    return headerView;
}


@end

//
//  TMSelectQuizResultCollectionViewHeader.h
//  TrulyMadly
//
//  Created by Ankit on 06/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSelectQuizResultCollectionViewHeader : UICollectionReusableView

+(CGFloat)contentHeightWithTitle:(NSString*)titleText subTitle:(NSString*)subTitleText maxWidth:(CGFloat)maxWidth;
-(void)setMatchImageFromURL:(NSURL*)imageURL;
-(void)setTitleText:(NSString*)titleText subTitleText:(NSString*)subTitleText;

@end

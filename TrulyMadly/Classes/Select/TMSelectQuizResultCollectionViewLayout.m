//
//  TMSelectQuizResultCollectionViewLayout.m
//  TrulyMadly
//
//  Created by Ankit on 06/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizResultCollectionViewLayout.h"

@interface TMSelectQuizResultCollectionViewLayout ()

@property (strong, nonatomic) NSMutableArray *itemAttributes;
@property (nonatomic, assign) CGSize contentSize;

@end

@implementation TMSelectQuizResultCollectionViewLayout

-(id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)prepareLayout {
    
    self.itemAttributes = [[NSMutableArray alloc] init];
    
    CGFloat xPos = 20;
    CGFloat yPos = 0;
    CGFloat contentWidth = 0.0;
    CGFloat contentHeight = 0.0;
    
    ///header
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    CGSize headerSize = [self.dataSource headerSizeForSection:0];
    
    UICollectionViewLayoutAttributes *headerAttributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader withIndexPath:indexPath];
    headerAttributes.frame = CGRectIntegral(CGRectMake(0, 0, headerSize.width, headerSize.height));
    [_itemAttributes addObject:headerAttributes];
    
    /////
    yPos = yPos + CGRectGetHeight(headerAttributes.frame) + 20;
    
    // Loop through all items and calculate the UICollectionViewLayoutAttributes for each on
    NSUInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    for (NSUInteger index = 0; index < numberOfItems; index++)
    {
        CGSize itemSize = [self.dataSource contentSizeForIndex:index];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = CGRectIntegral(CGRectMake(xPos, yPos, itemSize.width - (2*xPos), itemSize.height));
        [_itemAttributes addObject:attributes];
        
        attributes.zIndex = -1;
       
        yPos = yPos + itemSize.height + 20;
    }
    
    // Get the last item to calculate the total height of the content
    UICollectionViewLayoutAttributes *attributes = [_itemAttributes lastObject];
    contentHeight = attributes.frame.origin.y+attributes.frame.size.height;
    contentWidth = attributes.frame.origin.x + attributes.frame.size.width;
    
    _contentSize = CGSizeMake(contentWidth, contentHeight+10);
}

-(CGSize)collectionViewContentSize
{
    return self.contentSize;
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    return [_itemAttributes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [evaluatedObject frame]);
    }]];
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_itemAttributes objectAtIndex:indexPath.row];
}
- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind
                                                                     atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewLayoutAttributes *attributes = (UICollectionViewLayoutAttributes *)[super layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndexPath:indexPath];
    return attributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    
    return YES;
}


@end

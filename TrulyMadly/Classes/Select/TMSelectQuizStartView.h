//
//  TMSelectQuizStartView.h
//  TrulyMadly
//
//  Created by Ankit on 12/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSelectQuizStartViewDelegate;

@interface TMSelectQuizStartView : UIView

@property(nonatomic,weak)id<TMSelectQuizStartViewDelegate> delegate;

@end


@protocol TMSelectQuizStartViewDelegate <NSObject>

-(void)didStartQuiz;

@end

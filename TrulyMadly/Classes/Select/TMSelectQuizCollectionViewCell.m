//
//  TMQuizCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 01/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectQuizCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>


@interface TMSelectQuizCollectionViewCell ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *label;

@end

@implementation TMSelectQuizCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat imageviewXPos = 34;
        CGFloat imageviewWidth = CGRectGetWidth(frame) - (2*34);
        CGFloat imageViewHeight = (imageviewWidth * 2)/3;
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageviewXPos, 0, imageviewWidth, imageViewHeight)];
        self.imageView.backgroundColor = [UIColor grayColor];
        [self addSubview:self.imageView];
        
        CGLineCap labelXPos = imageviewXPos;
        CGFloat labelWidth = imageviewWidth;
        CGFloat labelYPos = CGRectGetMaxY(self.imageView.frame) + 24;
        CGFloat labelHeight = 80;//CGRectGetHeight(self.frame) - (labelYPos+24);
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(labelXPos, labelYPos, labelWidth, labelHeight)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.numberOfLines = 0;
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        self.label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.label];
    }
    return self;
}

+(NSString*)reusableIdentifier {
    return @"quizcell";
}

-(void)setImageFromURL:(NSURL*)imageURL {
    self.imageView.image = nil;
    [self.imageView setImageWithURL:imageURL];
}

-(void)setQuizText:(NSString*)quizText {
    self.label.text = quizText;
}

@end

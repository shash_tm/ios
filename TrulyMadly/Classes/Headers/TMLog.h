//
//  TMLog.h
//  TrulyMadly
//
//  Created by Ankit on 15/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#ifndef TrulyMadly_TMLog_h
#define TrulyMadly_TMLog_h
#include "TMBuildConfig.h"

#define TMLOG_PREFIX @"TM____"


/* **************************************************** */
/* ___________ MACROS FOR LOGGING PURPOSE _____________ */

// TMLOG() to display prefixed logs. (Same as NSLog).
#define TMLOG(format,...) {\
__TMLOG(1,format,##__VA_ARGS__)\
}

// TMLOG private macro.
#define __TMLOG(level,format,...) {\
__TMLOGPRIVATE(level,TMLOG_PREFIX,format,@"",##__VA_ARGS__);\
}

// One common macro for private macros.
#define __TMLOGPRIVATE(level,prefix,format,suffix,...) {\
if(TMLOG_LEVEL == (int)level) {\
NSLog(@"%@" format @"%@", prefix, ##__VA_ARGS__, suffix);\
}}
#endif


#if TMLOG_LEVEL == 1
#define NTMLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#else
#define NTMLog(FORMAT, ...);

#endif



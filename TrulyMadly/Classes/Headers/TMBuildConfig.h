//
//  TMBuildConfig.h
//  TrulyMadly
//
//  Created by Ankit on 16/04/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#ifndef TrulyMadly_TMBuildConfig_h
#define TrulyMadly_TMBuildConfig_h

// TM DIFF SERVER ENDPOINTS //
#define TM_BASE_URL_DEV                     @"http://dev.trulymadly.com/sumit"
#define TM_BASE_URL_T1                      @"https://t1.trulymadly.com"
#define TM_BASE_URL_PROD                    @"https://api.trulymadly.com"

//SOCKET CHAT SERER ENDPOINT
#define TM_SOCKET_ENDPOINT_DEV              @"http://chat_t.trulymadly.com:80"
#define TM_SOCKET_ENDPOINT_T1               @"https://t2chat.trulymadly.com"
#define TM_SOCKET_ENDPOINT_PROD             @"chat1.trulymadly.com"

// FACEBOOK DIFF ENV IDS //
#define FB_DEV_ID                           @"637809759576128"
#define FB_PROD_ID                          @"1442995922596983"

// LINKEDIN DIFF ENV IDS //
#define LI_DEV_API_KEY                      @"75wjtgy2qolino"
#define LI_DEV_SECRET_KEY                   @""
#define LI_PROD_API_KEY                     @"75dcgzg3oegb0g"
#define LI_PROD_SECRET_KEY                  @""

// GA DIFF ENV IDS //
#define GA_DEV_SERVER_ID                    @"UA-45604694-3"
#define GA_T1_SERVER_ID                     @"UA-45604694-2"
#define GA_PROD_SERVER_ID                   @"UA-45604694-4"

// CRASHLYTICS DIFF ENV IDS //
#define CRASHLYTICS_ENVIRONEMNT_KEY_DEV     @"dev"
#define CRASHLYTICS_ENVIRONEMNT_KEY_PROD    @"live"

// MOENGAGE DIFF ENV IDS //
#define MoENGAGE_APP_DEV_ID                 @"NMRQPNMV9R7D2WHJOAXFKYYO"
#define MoENGAGE_APP_PRODUCTION_ID          @"F6ZW6DOF0FI1LQRBGGY6VY6I"

// STICKER BASE URL
#define STICKER_CDN_URL                     @"http://cdn.trulymadly.com"

// APP RELEASE MODE TYPES //
#define TM_APP_RELEASE_MODE_DEV             0
#define TM_APP_RELEASE_MODE_RELEASE         1

// APP BUILD ENV TYPES //
#define TM_SERVER_ENVIRONMENT_DEVELOPMENT   0
#define TM_SERVER_ENVIRONMENT_T1            1
#define TM_SERVER_ENVIRONMENT_PRODUCTION    2

// APP BUILD MODE TYPES //
#define TM_BUILD_MODE_DEVELOPMENT           0
#define TM_BUILD_MODE_RELEASE               1


//************************************************************************************************//
////////////////// SET THESE MACROS FOR CREATING BUILD WITH DIFFERENT CONFIGURATION////////////////
/*
 * FOR PROD (APP STORE RELEASE) SET AS:
 * * TM_APP_BUILD_MODE            ==     TM_BUILD_MODE_RELEASE
 * * TM_APP_SERVER_ENVIRONMENT    ==     TM_SERVER_ENVIRONMENT_PRODUCTION
 *
 * FOR DEV (INTERNAL BUILD) SET AS:
 * * TM_APP_BUILD_MODE            ==     TM_BUILD_MODE_DEVELOPMENT
 * * TM_APP_SERVER_ENVIRONMENT    ==     SET AS PER REQUIREMENT (CAN BE ANY OF THREE)
 */

#define TM_APP_BUILD_MODE                   TM_BUILD_MODE_RELEASE
#define TM_APP_SERVER_ENVIRONMENT           TM_SERVER_ENVIRONMENT_PRODUCTION

////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//



/////////////////// CONSTANTS FOR DIFF BUILD ENV ////////////////////////

//define constants for dev mode
#if TM_APP_SERVER_ENVIRONMENT == TM_SERVER_ENVIRONMENT_DEVELOPMENT
#define TM_BASE_URL                         TM_BASE_URL_DEV
#define TM_SOCKET_ENDPOINT                  TM_SOCKET_ENDPOINT_DEV
#define FB_ID                               FB_DEV_ID
#define LI_API_KEY                          LI_DEV_API_KEY
#define LI_SECRET_KEY                       LI_DEV_SECRET_KEY
#define ADMIN_USERID                        1
#define GA_SERVER_ID                        GA_DEV_SERVER_ID
#define MoENGAGE_APP_ID                     MoENGAGE_APP_DEV_ID
#define STIKCER_BASE_URL                    TM_BASE_URL_DEV

//define constant for t1
#elif TM_APP_SERVER_ENVIRONMENT == TM_SERVER_ENVIRONMENT_T1
#define TM_BASE_URL                         TM_BASE_URL_T1
#define TM_SOCKET_ENDPOINT                  TM_SOCKET_ENDPOINT_T1
#define FB_ID                               FB_DEV_ID
#define LI_API_KEY                          LI_DEV_API_KEY
#define LI_SECRET_KEY                       LI_DEV_SECRET_KEY
#define ADMIN_USERID                        1
#define GA_SERVER_ID                        GA_T1_SERVER_ID
#define MoENGAGE_APP_ID                     MoENGAGE_APP_DEV_ID
#define STIKCER_BASE_URL                    TM_BASE_URL_T1

//prod
#else
#define TM_BASE_URL                         TM_BASE_URL_PROD
#define TM_SOCKET_ENDPOINT                  TM_SOCKET_ENDPOINT_PROD
#define FB_ID                               FB_PROD_ID
#define LI_API_KEY                          LI_PROD_API_KEY
#define LI_SECRET_KEY                       LI_PROD_SECRET_KEY
#define ADMIN_USERID                        19632
#define GA_SERVER_ID                        GA_PROD_SERVER_ID
#define MoENGAGE_APP_ID                     MoENGAGE_APP_PRODUCTION_ID
#define STIKCER_BASE_URL                    STICKER_CDN_URL

#endif

//////////////////////////////////////////////////////////////////////



////////////////// CONSTANTS FOR DIFF BUILD MODE /////////////////////
//define constants for dev mode
#if TM_APP_BUILD_MODE == TM_BUILD_MODE_RELEASE
#define TMLOG_LEVEL                         0
#define CRASHLYTICS_ENVIRONMENT_VALUE       CRASHLYTICS_ENVIRONEMNT_KEY_PROD
#define TM_APP_RELEASE_MODE                 TM_APP_RELEASE_MODE_RELEASE

//define constants for release mode
#else
#define TMLOG_LEVEL                         1
#define CRASHLYTICS_ENVIRONMENT_VALUE       CRASHLYTICS_ENVIRONEMNT_KEY_DEV
#define TM_APP_RELEASE_MODE                 TM_APP_RELEASE_MODE_DEV

#endif

//////////////////////////////////////////////////////////////////////////

#endif








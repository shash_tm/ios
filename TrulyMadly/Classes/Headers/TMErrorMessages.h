//
//  TMErrorMessages.h
//  TrulyMadly
//
//  Created by Ankit on 15/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#ifndef TrulyMadly_TMErrorMessages_h
#define TrulyMadly_TMErrorMessages_h

//#define TM_INTERNET_NOTAVAILABLE_MSG @"Internet appears to be offline\n Please check your Internet connection"
#define TM_INTERNET_NOTAVAILABLE_MSG @"Oops! It seems that you're offline. Please check your internet connection."

#define TM_SERVER_TIMEOUT_MSG @"Please try later"

#define TM_UNKNOWN_MSG @"Error! Please try again later."

#define PLEASETRYLATER_ERROR_MSG   @"Please try later"
#define DEACTIVATE_PROFILE_ERROR_TITLE @"Problem in deactivating your profile"
#define MATCH_LOADING_ERROR_TITLE @"Problem in fetching your matches"
#define MUTUALLIKE_LOADING_ERROR_TITLE @"Problem in fetching your mutual likes"
#define MESSAGE_LOADING_ERROR_TITLE @"Problem in fetching your message conversations"
#define PHOTO_LOADING_ERROR_TITLE @"Problem in fetching your Photos"
#define PROFILE_LOADING_ERROR_TITLE @"Problem in fetching your profile"
#define EM_BLOCK_ALERT_MSG @"This match is either not active anymore or has decided not to proceed further"

#define FAV_LOADING_ERROR_TITLE @"Problem in fetching your favourites"
#define FAV_SAVING_ERROR_TITLE @"Problem in saving your favourites"

#define PROFILE_SHARE_ERROR_TITLE @"Problem in sharing profile"

#define CLEARCHAT_ERROR_TITLE @"Problem in clearing your chat"

#define TM_PHOTO_FLOW_LIKE_MSG @"Add a pic. We're sure he'd like to see one!"

#define TM_NO_PHOTO_MSG @"Your smile could get you a compliment. Upload a picture now!"

#define TM_PHOTO_FLOW_REJECT_MSG @"Your pic wasn't approved. Upload your own pics now!"

#define TM_PHOTO_FLOW_REJECT_NO_LIKE_MSG @"Oh no! We couldn't approve your profile pic. You can't like guys till you have a pic."

#define TM_PHOTO_FLOW_NOTIFICATION_MSG @"Go ahead, add you pic. It can only mean more compliments!"//@"We need your pics as the guys you've liked won't see your profile till you upload your pic."

#define TM_DIGITS_ERROR_MSG @"Couldn't authenticate, please try later"

#endif

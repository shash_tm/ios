//
//  TMAPIConstants.h
//  TrulyMadly
//
//  Created by Ankit Jain on 05/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#ifndef TrulyMadly_TMAPIConstants_h
#define TrulyMadly_TMAPIConstants_h


#pragma mark - API URLS

#define KAPI_MATCH_DATA                         @"matches.php"
#define KAPI_USER_DATA                          @"index.php"
#define KAPI_REGISTER_RELATIVEPATH              @"register.php"
#define KAPI_LINKEDIN_RELATIVEPATH              @"linkedin2.php"
#define KAPI_TRUSTBUILDER_RELATIVEPATH          @"trustbuilder_api.php"
#define KAPI_GET_PHOTO_DETAILS                  @"photo.php"
#define KAPI_GET_MYPROFILE                      @"profile.php"
#define KAPI_GET_MUTUALLIKES                    @"likes.php"
#define KAPI_PERSONALITY_RELATIVEPATH           @"personality.php"
#define KAPI_PERSONALITY_FAVLIST_RELATIVEPATH   @"facebook_likes/facebook_likes.php"
#define KAPI_EDITPROFILE_RELATIVEPATH           @"editprofile.php"
#define KAPI_UPDATE_RELATIVEPATH                @"update.php"
#define KAPI_MESSAGE_LIST                       @"msg/messages.php"
#define KAPI_EDITPREFERENCE_RELATIVEPATH        @"editpreference_app.php"
#define KAPI_EDITPREFERENCE_SAVE_RELATIVEPATH   @"editpreference.php"
#define KAPI_TERM_OF_USE                        @"terms.php"
#define KAPI_SAFETY_GUIDELINES                  @"guidelines.php"
#define KAPI_TRUST_SECURITY                     @"trustnsecurity.php"
#define KAPI_TRUE_COMPATIBILITY                 @"truecompatibility.php"
#define KAPI_LOGOUT_API                         @"logout.php"
#define KAPI_DEACTIVATE_PROFILE                 @"logging/deactivateProfile.php"
#define KAPI_GCM_API                            @"mobile_utilities/gcm.php"
#define KAPI_FORGOT_PWD_API                     @"login/resetPass.php"
#define KAPI_FEEDBACK                           @"contact.php"
#define KAPI_LOG_EVENT                          @"trk.php"
#define KAPI_QUIZ_API                           @"quiz/quiz_data.php"
#define KAPI_HASHTAG_API                        @"hashtag_api.php"
#define KAPI_PROFILE_DISCOVERY_API              @"matches/matchesUtilites.php"
#define KAPI_SYNC_FB_API                @"updateFBData.php"
#define KAPI_INTENTION_SURVEY_API       @"intentionSurvey.php"
#define KAPI_USR_FLAG                   @"user/userFlags.php"
#define KAPI_STICKER_GALLERY_API        @"msg/stickersStaticApiV2.php"
#define KAPI_DELETE_API                 @"delete_api.php"
#define KAPI_SHARE_PROFILE              @"utilities/shortenurl.php"
#define KAPI_FAQ_URL                    @"faq.php"
#define KAPI_GETNOTIFICATIONCOUNT       @"utilities/notifications.php"
#define KAPI_DEAL_LIST_API              @"curatedDeals/dates.php"
#define KAPI_DEAL_DETAIL_PATH           @"curatedDeals/datespot.php"
#define KAPI_LINKEDIN_AUTH              @"linkedinOAuth.php"
#define KAPI_SHAREPHOTO_UPLOAD          @"photo_share.php"
#define KAPI_EVENTS_PATH                @"curatedDeals/events.php"
#define KAPI_SPARK_API                  @"spark/spark.php"
#define KSPARK_APIPATH                  @"spark/spark.php"
#define KAPI_MUTUALFBFRIEND             @"mutual_friends/get_mutual_friends.php"
#define KAPI_UPDATE_FB_ACCESSTOEKN      @"update_access_token.php"
#define KAPI_SELECT                     @"select/select.php"


#endif

//
//  TMNotificationHeaders.h
//  TrulyMadly
//
//  Created by Ankit on 10/06/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#ifndef TrulyMadly_TMNotificationHeaders_h
#define TrulyMadly_TMNotificationHeaders_h

#define TMNETWORKCHANGE_NOTIFICATION @"network_change_notification"
#define TMSOCKETCONNECTION_NOTIFICATION @"socket_connection_notification"
#define TMDISABLESENDACTION_NOTIFICATION @"disable_sendaction_notification"
#define TMNEWMESSAGE_NOTIFICATION @"new_message_notification"
#define TMJPEGIMAGEDOWNLOADFAIL_NOTIFICATION @"photoshare_image_downloadfail_notification"
#define TMEVENTMATCH_LIKEHIDEACTION_NOTIFICATION @"event_match_likehideaction_notification"

#define TMSPARK_REFRESH_NOTIFICATION @"spark_cache_refresh_notification"

#define TMSPARKACCEPTANCE_REFRESH_NOTIFICATION @"spark_acceptance_refresh_notification"

#define TMSPARKTIMER_NOTIFICATION @"spark_timer_notification"

#define TMDEEPLINK_NOTIFICATION @"tm_deeplink_notification"

#endif

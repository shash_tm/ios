//
//  TMDataStore.m
//  TrulyMadly
//
//  Created by Ankit on 26/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMDataStore.h"

@implementation TMDataStore

+(void)setBool:(BOOL)value forKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
    [TMDataStore synchronize];
}

+(void)setObject:(id)object forKey:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(id)retrieveObjectforKey:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+(BOOL)retrieveBoolforKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

+(void)removeObjectforKey:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [self synchronize];
}

+(void)synchronize {
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)containsObjectForKey:(NSString*) key {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key]) {
        return true;
    }
    return false;
}


@end

//
//  TMDataStore.h
//  TrulyMadly
//
//  Created by Ankit on 26/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define PROFILE_DISCOVERY_OFF_LAST_PROMPT_TIMESTAMP @"profileDiscoveryLastPromptTimestamp"
#define INTENTION_SURVEY_STATUS @"intentionSurveyStatus"
#define CURRENT_STICKER_VERSION @"currentStickerVersion"

#define LAST_QUIZZES_FETCH_DATE @"lastQuizzesFetchDate"
#define LAST_QUIZZES_STATUS_FETCH_DATE_INTIAL_KEY @"timestamp_list"
//sticker blooper key
#define STICKER_BLOOPER_TO_BE_SHOWN @"stickerBlooperToBeShown"


@interface TMDataStore : NSObject

+(void)setBool:(BOOL)value forKey:(NSString*)key;
+(void)setObject:(id)object forKey:(NSString*)key;
+(id)retrieveObjectforKey:(NSString*)key;
+(BOOL)retrieveBoolforKey:(NSString*)key;
+(void)removeObjectforKey:(NSString*)key;
+(void)synchronize;
+(BOOL)containsObjectForKey:(NSString*) key;

@end

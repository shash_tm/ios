//
//  TMRegisterJSONData.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 26/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"

@interface TMRegisterJSONData : TMCacheManager

-(instancetype)init;
-(void)configureRegisterTable;
-(void)updateRegisterTable:(NSString*)url data:(NSDictionary*)data;
-(BOOL)updateCountries:(FMDatabase*)db countries:(NSArray*)countries;
-(BOOL)updateCities:(FMDatabase*)db cities:(NSArray*)cities;
-(BOOL)updateStates:(FMDatabase*)db states:(NSArray*)states;
-(BOOL)updateDegree:(FMDatabase*)db degrees:(NSArray*)degrees;
-(BOOL)updateIncome:(FMDatabase*)db income:(NSArray*)income;
-(BOOL)updateHashtag:(FMDatabase*)db hashtags:(NSArray*)hashtags;

-(NSString*)fetchUrl;
-(NSMutableArray*)fetchCountries;
-(NSMutableArray*)fetchCities;
-(NSMutableArray*)fetchStates;
-(NSMutableArray*)fetchHashtags;
-(NSMutableArray*)fetchDegree;
-(NSMutableArray*)fetchIncome;

@end
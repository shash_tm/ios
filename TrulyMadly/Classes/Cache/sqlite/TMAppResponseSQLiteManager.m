//
//  TMAppResponseSQLiteManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 08/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMAppResponseSQLiteManager.h"

#import "TMLog.h"

#define MSG_CONV_DB_DIRECTORY_PATH @"tm/db"
#define MSG_CONV_DB_FILENAME @"chat.db"
#define TM_APP_CACHE_TABLE @"appCacheTable"


@implementation TMAppResponseSQLiteManager

-(instancetype)initWithConfiguration{
    self = [super initWithDatabaseDirectoryPath:MSG_CONV_DB_DIRECTORY_PATH databaseFile:MSG_CONV_DB_FILENAME];
    if(self) {
        [self configureDB];
    }
    return self;
}

- (void) configureDB {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    BOOL isDBPresent = [db tableExists:TM_APP_CACHE_TABLE];
    
    NSString* query;
    
    if (!isDBPresent) {
        TMLOG(@"App Cache table does not exist");
        
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"url\" TEXT NOT NULL, \"response\" BLOB, \"hash\" TEXT NOT NULL,\"tstamp\" TEXT NOT NULL)",TM_APP_CACHE_TABLE];
        
        isDBPresent = [db executeUpdate:query];
        
        if (!isDBPresent) {
            TMLOG(@"Error in creating table");
        }
    }
    [db close];
}


- (NSDictionary*) getCachedResponseForURL:(NSString*) urlString {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT response FROM \"%@\" WHERE url=\"%@\"",TM_APP_CACHE_TABLE,urlString];
    
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSDictionary* responseDictionary;
    
    while ([resultSet next]) {
        responseDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:[resultSet dataForColumn:@"response"]];
    }
    
    [db close];
    
    return responseDictionary;
}

- (NSString*) getHashValueForURL:(NSString*) urlString {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT hash FROM \"%@\" WHERE url=\"%@\"",TM_APP_CACHE_TABLE,urlString];
    
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSString* hashValue;
    
    while ([resultSet next]) {
        hashValue = [resultSet stringForColumn:@"hash"];
    }
    
    [db close];
    
    return hashValue;
}

- (NSString*) getTStampValueForURL:(NSString*) urlString {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT tstamp FROM \"%@\" WHERE url=\"%@\"",TM_APP_CACHE_TABLE,urlString];
    
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSString* tstamp;
    
    while ([resultSet next]) {
        tstamp = [resultSet stringForColumn:@"tstamp"];
    }
    
    [db close];
    
    return tstamp;
}

- (BOOL) containsCachedResponseForURL:(NSString*) urlString {
    if ([self getCachedResponseForURL:urlString]) {
        return YES;
    }
    return NO;
}

- (BOOL) updateCachedResponse:(NSDictionary*) response forURL:(NSString*) urlString forHashValue:(NSString*) hash tstamp:(NSString*) tstamp {
   
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    BOOL insertStatus = [db executeUpdate:@"INSERT OR REPLACE INTO appCacheTable (url,hash,tstamp,response) VALUES (?,?,?,?)",urlString,hash,tstamp,[NSKeyedArchiver archivedDataWithRootObject:response]];
    
    if (!insertStatus) {
        TMLOG(@"app caching insert statement failed");
    }
    [db close];
    
    return insertStatus;
}

- (BOOL) deleteCachedDataForURL:(NSString*) url {
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    //delete the cached table
    NSString* deleteQuery = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE url = \"%@\"",TM_APP_CACHE_TABLE,url];
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    [db close];
    return deleteStatus;
}

- (BOOL) deleteAllCachedData {
    FMDatabase *db = [self databaseInstance];
    [db open];
  
    //delete the cached table
   NSString* deleteQuery = [NSString stringWithFormat:@"DELETE from \"%@\"",TM_APP_CACHE_TABLE];
   BOOL deleteStatus = [db executeUpdate:deleteQuery];
    [db close];
    return deleteStatus;
}

@end

//
//  TMUserDataCacheManager.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"

@interface TMUserDataCacheManager : TMCacheManager

-(void)cacheCountryList:(NSArray*)countryList;
-(void)cacheStateList:(NSArray*)stateList;
-(void)cacheCityList:(NSArray*)cityList;
-(void)cachePopularCityList:(NSArray*)cityList;
-(void)cacheDegreeList:(NSArray*)degreeList;
-(NSArray*)getStateList;
-(NSArray*)getPopularCityList;
-(NSArray*)getCityList;
-(NSArray*)getDegreeList;

@end

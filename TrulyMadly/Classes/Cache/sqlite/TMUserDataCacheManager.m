//
//  TMUserDataCacheManager.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#define USERDATA_DB_DIRECTORY_PATH @".tm/db"
#define USERDATA_DB_FILENAME @"user.db"
#define USERDATA_DB_VERSION 100
#define USERDATA_TABLE @"userdata"

#define COUNTRYDATA_TABLE_NAME      @"countries"
#define STATEDATA_TABLE_NAME        @"states"
#define CITYDATA_TABLE_NAME         @"cities"
#define POPULARCITYDATA_TABLE_NAME  @"popularcities"
#define DEGREEDATA_TABLE_NAME       @"degrees"
#define INCOMEDATA_TABLE_NAME       @"income"


#import "TMUserDataCacheManager.h"
#import "TMLog.h"


@implementation TMUserDataCacheManager

#pragma mark Initialization Methods
#pragma mark -

-(instancetype)init {
    self = [super initWithDatabaseDirectoryPath:USERDATA_DB_DIRECTORY_PATH databaseFile:USERDATA_DB_FILENAME];
    if(self) {
        [self configureChatDatabaseTables];
    }
    return self;
}

-(void)configureChatDatabaseTables {
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *query = nil;
    BOOL isQueryExecuted = FALSE;
    
    BOOL isCountryDataTableExist = [db tableExists:COUNTRYDATA_TABLE_NAME];
    if(!isCountryDataTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"cid\" TEXT, \"cname\" TEXT)",
                                             COUNTRYDATA_TABLE_NAME];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating COUNTRY DATA TABLE:%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    BOOL isStateDataTableExist = [db tableExists:STATEDATA_TABLE_NAME];
    if(!isStateDataTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"cid\" TEXT, \"sid\" TEXT, \"sname\" TEXT)",
                 STATEDATA_TABLE_NAME];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating STATE DATA TABLE:%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    BOOL isCityDataTableExist = [db tableExists:CITYDATA_TABLE_NAME];
    if(!isCityDataTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"cid\" TEXT,\"sid\" TEXT,\
                                             \"cityid\" TEXT,\"cityname\" TEXT)",CITYDATA_TABLE_NAME];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating CITY DATA TABLE:%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    BOOL isPopilarCityDataTableExist = [db tableExists:POPULARCITYDATA_TABLE_NAME];
    if(!isPopilarCityDataTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"cid\" TEXT,\"sid\" TEXT,\
                 \"cityid\" TEXT,\"cityname\" TEXT)",POPULARCITYDATA_TABLE_NAME];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating POPULAR CITY DATA TABLE:%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    BOOL isDegreeDataTableExist = [db tableExists:DEGREEDATA_TABLE_NAME];
    if(!isDegreeDataTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",DEGREEDATA_TABLE_NAME];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating DEGREE DATA TABLE:%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    

    //NSString *incomeQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",INCOME_TABLE_NAME];
    
    //NSString *hashtagQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",HASHTAG_TABLE_NAME];
    
    [db close];
}

-(void)cacheCountryList:(NSArray*)countryList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    
    NSString *deleteQuery = [NSString stringWithFormat:@"delete from %@",COUNTRYDATA_TABLE_NAME];
    BOOL status = [adb executeUpdate:deleteQuery];
    if(!status) {
        TMLOG(@"Error in deleting state data");
    }
    
    for (int index = 0; index < countryList.count; index++) {
        NSDictionary *country = countryList[index];
        NSString* countryId = country[@"key"];
        NSString* countryName = country[@"value"];
        
        NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (cid,cname) VALUES (\"%@\",\"%@\")",
                                                     COUNTRYDATA_TABLE_NAME,countryId,countryName];
        status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"Error in updating country");
        }
    }

    [adb commit];
    [adb close];
}
-(void)cacheStateList:(NSArray*)stateList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    
    NSString *deleteQuery = [NSString stringWithFormat:@"delete from %@",STATEDATA_TABLE_NAME];
    BOOL status = [adb executeUpdate:deleteQuery];
    if(!status) {
        TMLOG(@"Error in deleting state data");
    }
    
    for (int index = 0; index < stateList.count; index++) {
        NSDictionary* state = stateList[index];
        NSString* countryId = state[@"country_id"];
        NSString* stateId = state[@"key"];
        NSString* stateName = state[@"value"];
        
        NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (cid,sid,sname) VALUES (\"%@\",\"%@\",\"%@\")",STATEDATA_TABLE_NAME,countryId,stateId,stateName];
        status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"Error in caching state");
        }
    }
    
    [adb commit];
    [adb close];
}
-(void)cacheCityList:(NSArray*)cityList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    
    NSString *deleteQuery = [NSString stringWithFormat:@"delete from %@",CITYDATA_TABLE_NAME];
    BOOL status = [adb executeUpdate:deleteQuery];
    if(!status) {
        TMLOG(@"Error in deleting city data");
    }
    
    for (int index = 0; index < cityList.count; index++) {
        NSDictionary* city = cityList[index];
        NSString* countryId = city[@"country_id"];
        NSString* stateId = city[@"state_id"];
        NSString* cityId = city[@"city_id"];
        NSString* cityName = city[@"name"];
        
        NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (cid,sid,cityid,cityname) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")",CITYDATA_TABLE_NAME,countryId,stateId,cityId,cityName];
        status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"Error in caching city");
        }
    }
    
    [adb commit];
    [adb close];
}
-(void)cachePopularCityList:(NSArray*)cityList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    
    NSString *deleteQuery = [NSString stringWithFormat:@"delete from %@",POPULARCITYDATA_TABLE_NAME];
    BOOL status = [adb executeUpdate:deleteQuery];
    if(!status) {
        TMLOG(@"Error in deleting city data");
    }
    
    for (int index = 0; index < cityList.count; index++) {
        NSDictionary* city = cityList[index];
        NSString* countryId = city[@"country_id"];
        NSString* stateId = city[@"state_id"];
        NSString* cityId = city[@"city_id"];
        NSString* cityName = city[@"name"];
        
        NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (cid,sid,cityid,cityname) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")",POPULARCITYDATA_TABLE_NAME,countryId,stateId,cityId,cityName];
        status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"Error in caching popular city");
        }
    }
    
    [adb commit];
    [adb close];
}
-(void)cacheDegreeList:(NSArray*)degreeList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    
    NSString *deleteQuery = [NSString stringWithFormat:@"delete from %@",DEGREEDATA_TABLE_NAME];
    BOOL status = [adb executeUpdate:deleteQuery];
    if(!status) {
        TMLOG(@"Error in deleting degree data");
    }
    
    for (int index = 0; index < degreeList.count; index++) {
        NSDictionary* degree = degreeList[index];
        NSString* key = degree[@"key"];
        NSString* value = degree[@"value"];
        
        NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (key,value) VALUES (\"%@\",\"%@\")",DEGREEDATA_TABLE_NAME,key,value];
        status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"Error in caching degree data");
        }
    }
    
    [adb commit];
    [adb close];
}

-(NSArray*)getStateList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSMutableArray* stateList = [NSMutableArray arrayWithCapacity:8];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\"",STATEDATA_TABLE_NAME];
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        NSMutableDictionary* stateDictionary = [NSMutableDictionary dictionary];
        stateDictionary[@"cid"] = [rs stringForColumn:@"cid"];
        stateDictionary[@"sid"] = [rs stringForColumn:@"sid"];
        stateDictionary[@"sname"] = [rs stringForColumn:@"sname"];
        [stateList addObject:stateDictionary];
    }
    
    [rs close];
    [adb close];
    
    return stateList;
}
-(NSArray*)getCityList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSMutableArray* cityList = [NSMutableArray arrayWithCapacity:8];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\"",CITYDATA_TABLE_NAME];
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        NSDictionary* cityDictionary = [self cityDictionaryFromResultSet:rs];
        [cityList addObject:cityDictionary];
    }
    
    [rs close];
    [adb close];
    
    return cityList;
}
-(NSArray*)getPopularCityList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];

    NSMutableArray* popularCityList = [NSMutableArray arrayWithCapacity:8];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\"",POPULARCITYDATA_TABLE_NAME];
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        NSDictionary* cityDictionary = [self cityDictionaryFromResultSet:rs];
        [popularCityList addObject:cityDictionary];
    }
    
    [rs close];
    [adb close];
    
    return popularCityList;
}
-(NSArray*)getDegreeList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSMutableArray* degreeList = [NSMutableArray arrayWithCapacity:8];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\"",DEGREEDATA_TABLE_NAME];
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        NSDictionary* degreeDictionary = [NSMutableDictionary dictionary];
        [degreeDictionary setValue:[rs stringForColumn:@"key"] forKey:@"key"];
        [degreeDictionary setValue:[rs stringForColumn:@"value"] forKey:@"value"];
        [degreeList addObject:degreeDictionary];
    }
    
    [rs close];
    [adb close];
    
    return degreeList;
}
-(NSDictionary*)cityDictionaryFromResultSet:(FMResultSet*)rs {
    NSMutableDictionary* cityDict = [NSMutableDictionary dictionaryWithCapacity:4];
    cityDict[@"cid"] = [rs stringForColumn:@"cid"];
    cityDict[@"sid"] = [rs stringForColumn:@"sid"];
    cityDict[@"cityid"] = [rs stringForColumn:@"cityid"];
    cityDict[@"cityname"] = [rs stringForColumn:@"cityname"];
    return cityDict;
}

@end

//
//  TMRegisterJSONData.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 26/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMRegisterJSONData.h"
#import "TMLog.h"

#define MSG_CONV_DB_DIRECTORY_PATH @"tm/db"
#define MSG_CONV_DB_FILENAME @"chat.db"
#define REG_TABLE_NAME @"user_json"
#define COUNTRY_TABLE_NAME @"countries"
#define CITY_TABLE_NAME @"cities"
#define STATE_TABLE_NAME @"states"
#define DEGREE_TABLE_NAME @"degrees"
//#define INDUSTRY_TABLE_NAME @"industries"
#define INCOME_TABLE_NAME @"income"
//#define DEGREE_RANGE_TABLE_NAME @"degree_keys"
#define HASHTAG_TABLE_NAME @"hashtags"

@implementation TMRegisterJSONData

-(instancetype)init{
    self = [super initWithDatabaseDirectoryPath:MSG_CONV_DB_DIRECTORY_PATH databaseFile:MSG_CONV_DB_FILENAME];
    if(self) {
        [self configureRegisterTable];
    }
    return self;
}

-(void)configureRegisterTable {
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"url\" TEXT)",REG_TABLE_NAME];
    NSString *countriesQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"country_id\" TEXT, \"name\" TEXT)",COUNTRY_TABLE_NAME];
    NSString *citiesQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"city_id\" TEXT, \"country_id\" TEXT, \"state_id\" TEXT, \"name\" TEXT)",CITY_TABLE_NAME];
    NSString *statesQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"country_id\" TEXT, \"key\" TEXT, \"value\" TEXT)",STATE_TABLE_NAME];
    NSString *degreeQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",DEGREE_TABLE_NAME];
    //NSString *industryQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",INDUSTRY_TABLE_NAME];
    NSString *incomeQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",INCOME_TABLE_NAME];
    //NSString *rangeQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"master_key\" TEXT, \"bachelor_key\" TEXT)",DEGREE_RANGE_TABLE_NAME];
    NSString *hashtagQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"key\" TEXT, \"value\" TEXT)",HASHTAG_TABLE_NAME];
    
    BOOL isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating REG_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
    isQueryExecuted = [db executeUpdate:countriesQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating COUNTRY_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
    isQueryExecuted = [db executeUpdate:citiesQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating CITY_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
    isQueryExecuted = [db executeUpdate:statesQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating STATE_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
    isQueryExecuted = [db executeUpdate:degreeQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating DEGREE_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
//    isQueryExecuted = [db executeUpdate:industryQuery];
//    if(!isQueryExecuted) {
//        TMLOG(@"Error in creating INDUSTRY_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
//    }
    isQueryExecuted = [db executeUpdate:incomeQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating INCOME_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
//    isQueryExecuted = [db executeUpdate:rangeQuery];
//    if(!isQueryExecuted) {
//        TMLOG(@"Error in creating DEGREE_RANGE_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
//    }
    isQueryExecuted = [db executeUpdate:hashtagQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating HASHTAG_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    [db close];
    
}

-(BOOL)updateCountries:(FMDatabase *)db countries:(NSArray *)countries {
    
    NSString *deleteQuery = @"delete from countries";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(deleteStatus) {
        
        @try {
            for (int i=0; i<countries.count; i++) {
                
                NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (country_id,name) VALUES (\"%@\",\"%@\")",COUNTRY_TABLE_NAME,countries[i][@"key"],countries[i][@"value"]];
                [self executeUpdateQuery:query withDB:db];
                
            }
        }
        @catch (NSException *exception) {
            TMLOG(@"reason is %@",exception.reason);
            return false;
        }
        return true;
    }
    return false;
    
}

-(BOOL)updateCities:(FMDatabase *)db cities:(NSArray *)cities {
    
    NSString *deleteQuery = @"delete from cities";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(deleteStatus) {

        @try {
            for (int i=0; i<cities.count; i++) {
                
                NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (city_id,country_id,state_id,name) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")",CITY_TABLE_NAME,cities[i][@"city_id"],cities[i][@"country_id"],cities[i][@"state_id"],cities[i][@"name"]];
                [self executeUpdateQuery:query withDB:db];
                
            }
        }
        @catch (NSException *exception) {
            TMLOG(@"reason is %@",exception.reason);
            return false;
        }
        return true;
    }
    return false;

}

-(BOOL)updateStates:(FMDatabase *)db states:(NSArray *)states {
   
    NSString *deleteQuery = @"delete from states";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(deleteStatus) {
    
        @try {
            for (int i=0; i<states.count; i++) {
                
                NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (country_id,key,value) VALUES (\"%@\",\"%@\",\"%@\")",STATE_TABLE_NAME,states[i][@"country_id"],states[i][@"key"],states[i][@"value"]];
                [self executeUpdateQuery:query withDB:db];
            
            }
        }
        @catch (NSException *exception) {
            TMLOG(@"reason is %@",exception.reason);
            return false;
        }
        return true;
    }
    return false;
}

-(BOOL)updateDegree:(FMDatabase *)db degrees:(NSArray *)degrees {
   
    NSString *deleteQuery = @"delete from degrees";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(deleteStatus) {
    
        @try {
            for (int i=0; i<degrees.count; i++) {
                
                NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (key,value) VALUES (\"%@\",\"%@\")",DEGREE_TABLE_NAME,degrees[i][@"key"],degrees[i][@"value"]];
                [self executeUpdateQuery:query withDB:db];
            
            }
        }
        @catch (NSException *exception) {
            TMLOG(@"reason is %@",exception.reason);
            return false;
        }
        return true;
    }
    return false;
}

-(BOOL)updateIncome:(FMDatabase *)db income:(NSArray *)income {
    
    NSString *deleteQuery = @"delete from income";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(deleteStatus) {
       @try {
           for (int i=0; i<income.count; i++) {
               
               NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (key,value) VALUES (\"%@\",\"%@\")",INCOME_TABLE_NAME,income[i][@"key"],income[i][@"value"]];
               [self executeUpdateQuery:query withDB:db];
           }
       }
        @catch (NSException *exception) {
            TMLOG(@"reason is %@",exception.reason);
            return false;
        }
        return true;
    }
    return false;
}

-(BOOL)updateHashtag:(FMDatabase *)db hashtags:(NSArray *)hashtags {
    
    NSString *deleteQuery = @"delete from hashtags";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(deleteStatus) {
        @try {
            for (int i=0; i<hashtags.count; i++) {
                
                NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (key,value) VALUES (\"%@\",\"%@\")",HASHTAG_TABLE_NAME,hashtags[i][@"key"],hashtags[i][@"value"]];
                [self executeUpdateQuery:query withDB:db];
            }
        }
        @catch (NSException *exception) {
            TMLOG(@"reason is %@",exception.reason);
            return false;
        }
        return true;
    }
    return false;
}

-(void)updateRegisterTable:(NSString *)url data:(NSDictionary *)data {
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *deleteQuery = @"delete from user_json";
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    
    if(!deleteStatus) {
        TMLOG(@"Error in deleting REG_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
    }
    else {
        NSArray *countries = [data objectForKey:@"countries"];
        NSArray *cities = [data objectForKey:@"cities"];
        NSArray *states = [data objectForKey:@"states"];
        NSArray *income = [data objectForKey:@"income"];
        NSArray *degree = [data objectForKey:@"highest_degree"];
        NSArray *hashtags = [data objectForKey:@"interest_new"];
        
        BOOL isCountryUpdated = [self updateCountries:db countries:countries];
        BOOL isStateUpdated = [self updateStates:db states:states];
        BOOL isCityUpdated = [self updateCities:db cities:cities];
        BOOL isDegreeUpdated = [self updateDegree:db degrees:degree];
        BOOL isIncomeUpdated = [self updateIncome:db income:income];
        BOOL isHashtagUpdated = [self updateHashtag:db hashtags:hashtags];
        
        if(isCountryUpdated && isStateUpdated && isCityUpdated && isDegreeUpdated && isIncomeUpdated && isHashtagUpdated) {
            
            NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (url) VALUES (\"%@\")",REG_TABLE_NAME,url];
            BOOL isQueryExecuted = [self executeUpdateQuery:query withDB:db];
        
            if(!isQueryExecuted) {
                TMLOG(@"Error in inserting REG_TABLE_NAME::%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
    }
    
    [db close];

}

-(NSString*)fetchUrl {
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *result = @"";
    NSString *selectQuery = @"select url from user_json";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        result = [rs stringForColumn:@"url"];
    }
    [rs close];
    [db close];
    
    return result;
}

-(NSMutableArray*)fetchCountries {
    NSMutableArray *countries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *selectQuery = @"select * from countries";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        [tmp setValue:[rs stringForColumn:@"country_id"] forKey:@"country_id"];
        [tmp setValue:[rs stringForColumn:@"name"] forKey:@"name"];
        [countries addObject:tmp];
    }
    
    [rs close];
    [db close];
    
    return countries;
}

-(NSMutableArray*)fetchCities {
    NSMutableArray *cities = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *selectQuery = @"select * from cities order by name";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        [tmp setValue:[rs stringForColumn:@"city_id"] forKey:@"city_id"];
        [tmp setValue:[rs stringForColumn:@"country_id"] forKey:@"country_id"];
        [tmp setValue:[rs stringForColumn:@"state_id"] forKey:@"state_id"];
        [tmp setValue:[rs stringForColumn:@"name"] forKey:@"name"];
        [cities addObject:tmp];
    }
    
    [rs close];
    [db close];
    
    return cities;
}

-(NSMutableArray*)fetchStates {
    NSMutableArray *states = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *selectQuery = @"select * from states";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        [tmp setValue:[rs stringForColumn:@"country_id"] forKey:@"country_id"];
        [tmp setValue:[rs stringForColumn:@"key"] forKey:@"key"];
        [tmp setValue:[rs stringForColumn:@"value"] forKey:@"value"];
        [states addObject:tmp];
    }
    
    [rs close];
    [db close];
    
    return states;
}

-(NSMutableArray*)fetchHashtags {
    NSMutableArray *hashtags = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *selectQuery = @"select * from hashtags";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        [tmp setValue:[rs stringForColumn:@"key"] forKey:@"key"];
        [tmp setValue:[rs stringForColumn:@"value"] forKey:@"value"];
        [hashtags addObject:tmp];
    }
    
    [rs close];
    [db close];
    
    return hashtags;
}

-(NSMutableArray*)fetchIncome {
    NSMutableArray *income = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *selectQuery = @"select * from income";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        [tmp setValue:[rs stringForColumn:@"key"] forKey:@"key"];
        [tmp setValue:[rs stringForColumn:@"value"] forKey:@"value"];
        [income addObject:tmp];
    }
    
    [rs close];
    [db close];
    
    return income;
}

-(NSMutableArray*)fetchDegree {
    NSMutableArray *degrees = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *selectQuery = @"select * from degrees";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    while ([rs next]) {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        [tmp setValue:[rs stringForColumn:@"key"] forKey:@"key"];
        [tmp setValue:[rs stringForColumn:@"value"] forKey:@"value"];
        [degrees addObject:tmp];
    }
    
    [rs close];
    [db close];
    
    return degrees;
}

@end
//
//  TMAppResponseSQLiteManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 08/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMCacheManager.h"

@interface TMAppResponseSQLiteManager : TMCacheManager

- (instancetype) initWithConfiguration;

- (NSDictionary*) getCachedResponseForURL:(NSString*) urlString;
- (NSString*) getHashValueForURL:(NSString*) urlString;
- (NSString*) getTStampValueForURL:(NSString*) urlString;
- (BOOL) containsCachedResponseForURL:(NSString*) urlString;
- (BOOL) updateCachedResponse:(NSDictionary*) response forURL:(NSString*) urlString forHashValue:(NSString*) hash tstamp:(NSString*) tstamp;
- (BOOL) deleteCachedDataForURL:(NSString*) url;
- (BOOL) deleteAllCachedData;

@end

//
//  TMMatchCacheManager.h
//  TrulyMadly
//
//  Created by Ankit on 02/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"

@interface TMMatchCacheManager : TMCacheManager

-(NSArray*)getCachedMatchDataForUser:(NSInteger)userId;
-(void)cacheMatchData:(NSArray*)matchList forUser:(NSInteger)userId;
-(void)markMatchDataAsShown:(NSInteger)matchId;
-(void)deleteMatchData;
-(NSDictionary*)getCachedProfileDataForUser:(NSInteger)userId;
@end

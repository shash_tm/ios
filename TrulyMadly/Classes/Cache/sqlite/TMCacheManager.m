//
//  TMCacheManager.m
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"
#import "TMLog.h"

@interface TMCacheManager ()

@property(nonatomic,strong)NSString *databaseDirectory;
@property(nonatomic,strong)NSString *dbName;

@end

@implementation TMCacheManager

#pragma mark - cache manager initializers -

-(instancetype)initWithDatabaseDirectoryPath:(NSString*)databaseDirectory databaseFile:(NSString*)dbName {
    
    self = [super init];
    if(self) {
        self.databaseDirectory = databaseDirectory;
        self.dbName = dbName;
        
        [self initializeDatabase];
    }
    
    return self;
}

-(BOOL)initializeDatabase {
    bool databaseDirectoryPathStatus = [[NSFileManager defaultManager] fileExistsAtPath:[self databaseDirectoryPath]];
    if(!databaseDirectoryPathStatus) {
        NSError *error = nil;
        BOOL status = [[NSFileManager defaultManager] createDirectoryAtPath:[self databaseDirectoryPath] withIntermediateDirectories:YES attributes:nil error:&error];
        
        // if database file does not exist, so we need to create one.
        if(status && !error) {
            ///create db
            NSString *path = [self databaseFilePath];
            sqlite3 *database = [self createDatabaseAtPath:path];
            if (database) {
                [self closeDatabaseInstance:database];
                return true;
            }
        }
        else {
            return false;
        }
    }
    return true;
}


#pragma mark - file system access methods -

-(NSString*)databaseDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:self.databaseDirectory];
    return databasePath;
}
-(NSString*)databaseFilePath {
    NSString *databasePath = [self databaseDirectoryPath];
    NSString *databaseFilePath = [databasePath stringByAppendingPathComponent:self.dbName];
    return databaseFilePath;
}

#pragma mark - database basic operations -

-(sqlite3*)createDatabaseAtPath:(NSString*)databasePath
{
    sqlite3 *database = NULL;
    if (sqlite3_open([databasePath UTF8String], &database) != SQLITE_OK)
    {
        sqlite3_close(database);
        database = NULL;
    }
    
    //warning, the caller is expected to free this pointer.
    return database;
}

-(void)closeDatabaseInstance:(sqlite3*)database
{
    if(database)
    {
        if (sqlite3_close(database) != SQLITE_OK )
        {
            TMLOG(@"Failed to close the database with error %s", sqlite3_errmsg(database));
        }
        database = NULL;
    }
}

-(FMDatabase*)databaseInstance {
    NSString *dbPath = [self databaseFilePath];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return db;
}
-(BOOL)executeUpdateQuery:(NSString*)updateQuery withDB:(FMDatabase*)dbInstance {
    BOOL status = [dbInstance executeUpdate:updateQuery];
    return status;
}
-(BOOL)executeUpdateQuery:(NSString*)updateQuery withDB:(FMDatabase*)dbInstance withArgs:(NSDictionary*)args {
    BOOL status = [dbInstance executeUpdate:updateQuery withParameterDictionary:args];
    return status;
}

@end

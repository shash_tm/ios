//
//  TMCacheManager.h
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"


@interface TMCacheManager : NSObject

@property(nonatomic,strong,readonly)NSString *databaseDirectory;
@property(nonatomic,strong,readonly)NSString *dbName;

-(instancetype)initWithDatabaseDirectoryPath:(NSString*)databaseDirectory databaseFile:(NSString*)dbName;
-(FMDatabase*)databaseInstance;
-(BOOL)executeUpdateQuery:(NSString*)updateQuery withDB:(FMDatabase*)dbInstance;
-(BOOL)executeUpdateQuery:(NSString*)updateQuery withDB:(FMDatabase*)dbInstance withArgs:(NSDictionary*)args;

@end

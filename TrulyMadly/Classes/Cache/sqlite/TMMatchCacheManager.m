//
//  TMMatchCacheManager.m
//  TrulyMadly
//
//  Created by Ankit on 02/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMatchCacheManager.h"
#import "TMLog.h"


#define MATCH_DB_DIRECTORY_PATH @".tm/db"
#define MATCH_DB_FILENAME @"match.db"
#define MATCH_DB_VERSION 61
#define MATCH_TABLE @"matchdata"


@implementation TMMatchCacheManager

#pragma mark Initialization Methods
#pragma mark -

-(instancetype)init {
    self = [super initWithDatabaseDirectoryPath:MATCH_DB_DIRECTORY_PATH databaseFile:MATCH_DB_FILENAME];
    if(self) {
        [self configureChatDatabaseTables];
    }
    return self;
}

-(NSDictionary*)getCachedProfileDataForUser:(NSInteger)userId {
    NSDictionary *dictionary = nil;
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    TMLOG(@"0%@",[adb databasePath]);
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE userid=%ld AND shown=0 ORDER BY rank LIMIT 1",MATCH_TABLE,(long)userId];
    //TMLOG(@"getCachedMessages Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        NSData *data = [rs dataForColumn:@"data"];
        dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    [rs close];
    [adb close];
    
    return dictionary;
}
-(NSArray*)getCachedMatchDataForUser:(NSInteger)userId {
    NSMutableArray *results = [NSMutableArray array];
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    TMLOG(@"0%@",[adb databasePath]);
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE userid=%ld AND shown=0 ORDER BY rank",MATCH_TABLE,(long)userId];
    //TMLOG(@"getCachedMessages Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        NSData *data = [rs dataForColumn:@"data"];
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        [results addObject:dictionary];
    }
    [rs close];
    [adb close];
    
    return results;
}
-(void)cacheMatchData:(NSArray*)matchList forUser:(NSInteger)userId {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    NSInteger failCount = 0;
    NSInteger successCount = 0;
    NSInteger rank = 0;
    ///delete all records which are not seen
    BOOL status = FALSE;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE shown=0",MATCH_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting match data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    //insert data
    for (int i=0; i<matchList.count; i++) {
        NSDictionary *matchDictionary = matchList[i];
        NSInteger matchId = [matchDictionary[@"user_id"] integerValue];
        TMLOG(@"Inserting match data withid:%ld",(long)matchId);
        BOOL shown = FALSE; //by default
        BOOL isMatchCached = FALSE;
        
        //////////////////
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld",MATCH_TABLE,(long)matchId];
        FMResultSet *rs = [adb executeQuery:query];
        while ([rs next]) {
            //retrieve values for each record
            shown = [rs boolForColumn:@"shown"];
            isMatchCached = TRUE;
            break;
        }
        [rs close];
        
        //////////////////
        if(isMatchCached) {
            if(shown) {
                BOOL status = FALSE;
                NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET rank=%ld WHERE matchid=%ld",
                                         MATCH_TABLE,(long)rank++,(long)matchId];
                
                //TMLOG(@"conversation list update query:%@",updateQuery);
                status = [self executeUpdateQuery:updateQuery withDB:adb];
                if(!status) {
                    TMLOG(@"match data list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
                }
            }
        }
        else {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:matchDictionary];
            NSString *insertQuery = @"INSERT INTO matchdata (matchid,userid,data,shown,rank)\
            VALUES (:matchid,:userid,:data,:shown,:rank)";
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            params[@"matchid"] = @(matchId);
            params[@"userid"] = @(userId);
            params[@"data"] = data;
            params[@"shown"] = @(shown);
            params[@"rank"] = @(rank++);
            BOOL status = [self executeUpdateQuery:insertQuery withDB:adb withArgs:params];
            if(!status) {
                failCount++;
                TMLOG(@"___MDC:cache match data:%@ %@",adb.lastError,adb.lastErrorMessage);
            }
            else {
                successCount++;
                TMLOG(@"___MDC:match data successfuully cached");
            }
        }
    }
    
    TMLOG(@"___MDC:FailCount:%ld SuccessCount:%ld",(long)failCount,(long)successCount);
    
    [adb commit];
    [adb close];
}
-(void)markMatchDataAsShown:(NSInteger)matchId {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    BOOL status = FALSE;
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET shown=%d WHERE matchid=%ld",
                             MATCH_TABLE,1, (long)matchId];
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    if(!status) {
        TMLOG(@"match data list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}
-(void)deleteMatchData {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    BOOL status = FALSE;
    
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@",MATCH_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting match data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
}
-(void)configureChatDatabaseTables {
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    [self updateDBVersion:db];
    
    NSString *query = nil;

    /* Table:matchdata
     *** table will store all chat messages
     */
    BOOL isQueryExecuted = FALSE;
    BOOL isMatchDataTableExist = [db tableExists:MATCH_TABLE];
    if(!isMatchDataTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"matchid\" INTEGER NOT NULL UNIQUE,\
                                                                                \"userid\" INTEGER NOT NULL , \"data\" BLOB,\
                                                                                 \"shown\" BOOL NOT NULL, \"rank\" INTEGER)",MATCH_TABLE];
        
        BOOL isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating table \"matchdata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    ////
    BOOL isRankColumnExist = [db columnExists:@"rank" inTableWithName:MATCH_TABLE];
    if(!isRankColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN rank INTEGER;",MATCH_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coumn:rank table \"matchdata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }

    [db close];
}

-(void)updateDBVersion:(FMDatabase*)adb {
    NSInteger userVersion = [adb userVersion];
    if(userVersion != MATCH_DB_VERSION) {
        [adb setUserVersion:MATCH_DB_VERSION];
    }
}


@end

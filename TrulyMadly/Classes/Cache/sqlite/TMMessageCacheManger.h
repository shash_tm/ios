//
//  TMMessageCacheManger.h
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"
#import "TMEnums.h"


@class TMChatContext;
@class TMChatSeenContext;
@class TMMessage;
@class TMChatUser;
@class TMConversation;
@class TMSpark;


@interface TMMessageCacheManger : TMCacheManager

-(NSDictionary*)getCachedConversationList;
-(NSString*)getLastChatFetchTimestamp;
-(NSString*)getLastMessageId:(NSInteger)matchId;
-(TMChatSeenContext*)getCachedChatMetaData:(TMChatContext*)context;
-(TMChatUser*)getChatUserMetaData:(TMChatContext*)context;
-(NSMutableArray*)getCachedMessages:(TMChatContext*)chatContext;
-(TMMessage*)getLastChatFetchedMessageFromContext:(TMChatContext*)context;
-(NSString*)getDealMessageTimesampSavedAsTextMessageFromTimestamp:(NSString*)timestamp;
-(NSString*)getPhotoShareMessageTimesampSavedAsTextMessageFromTimestamp:(NSString*)timestamp;
-(NSDictionary*)getCachedConversationListForShare;
-(NSInteger)getUnreadConversationCount;
-(TMSpark*)getActiveSpark;
-(NSInteger)getSparkCount;

/////////
-(void)updateSocketConnection:(TMMessage*)message;
-(void)updatePolligConnection:(TMMessage*)message;
-(void)updateCachedMessage:(TMMessage*)message;
-(void)setCachedConversationAsBlockedShown:(TMChatContext*)chatContext;
-(void)updateMetaParams:(TMMessage*)message;
-(void)updateDeleteConversationMetadata:(TMChatContext*)context tillTimestamp:(NSString*)timestamp;
-(void)updateCachedPhotoMessgaesInSendingStateAsFailed;

//////
-(void)cacheConversationList:(NSArray*)conversationList;
-(void)cacheSparkConversation:(TMConversation*)sparkConversation;
-(void)saveChatUserMetaData:(TMChatUser*)chatUser chatContext:(TMChatContext*)context;
-(void)cachedNewMessage:(TMMessage*)message;
-(void)cacheMessages:(NSArray*)messages fromSource:(TMMessageSource)messageSource;
-(void)cacheNewUnreadMessage:(TMMessage*)message;
-(void)cacheChatMetaData:(TMChatSeenContext*)context;
-(void)cacheSparkData:(NSArray*)sparkData;
-(void)cacheDealState:(NSInteger)dealState withMatch:(NSString*)matchId;
-(void)cacheDoodleLink:(NSString*)doodleLink withMatch:(NSInteger)matchId;

//////
-(void)deleteSpark:(TMChatContext*)chatContext;
-(void)deleteMessage:(TMMessage*)message;
-(void)deleteAllCachedMessageContent;
-(void)deleteCachedConversation;

//////
-(void)markSparkAsSeen:(TMChatContext*)chatContext withStartTime:(NSString*)startTime;
-(void)markMessagesInSendingStateAsFailed:(TMChatContext*)context;
-(void)markConversationAsRead:(TMChatContext*)chatContext;
-(void)markConversationAsUnRead:(TMChatContext*)chatContext;
-(void)archiveConversation:(TMConversation*)conversationMessage;

@end

//
//  TMQuizCacheManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 20/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizCacheManager.h"
#import "TMSwiftHeader.h"
#import "TMLog.h"

#define MSG_CONV_DB_DIRECTORY_PATH @"tm/db"
#define MSG_CONV_DB_FILENAME @"chat.db"
#define QZ_CONFIG  @"quiz_config"
#define QUIZ_TABLE @"quiz"
#define QUIZ_PLAYED_BY_ATTRIBUTE @"played_by"
#define TM_QUIZ_STATUS_TABLE @"quizStatusTable"


@implementation TMQuizCacheManager

-(instancetype)initWithConfiguration{
    self = [super initWithDatabaseDirectoryPath:MSG_CONV_DB_DIRECTORY_PATH databaseFile:MSG_CONV_DB_FILENAME];
    if(self) {
        [self configureQuizDB];
    }
    return self;
}

-(void)configureQuizDB {
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    //create quiz config table if not created
    //this table will store meta information about all quiz version
    NSString *query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"version\" TEXT, \"tstamp\" TEXT)",QZ_CONFIG];
    
    BOOL isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in QZ_CONFIG::%@ %@",db.lastError,db.lastErrorMessage);
    }
    //(banner_url, display_name, image, played_by, quiz_id)
    
    // create quiz table if not created
    // this table store quiz data like name,quiz_id,icon_img url
    NSString *query1 = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"quiz_data\" BLOB)",QUIZ_TABLE];
    BOOL isExecuted = [db executeUpdate:query1];
    if(!isExecuted) {
        TMLOG(@"Error in creating QUIZ_TABLE::%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    //check for quiz status table
    BOOL isQuizStatusTableExist = [db tableExists:TM_QUIZ_STATUS_TABLE];
    
    if (!isQuizStatusTableExist) {
        TMLOG(@"Quiz Status table does not exist");
        
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"conversation_id\" TEXT NOT NULL, \"user_timestamp\" TEXT NOT NULL, \"match_timestamp\" TEXT NOT NULL, \"unread_quiz_ids\" TEXT NOT NULL)",TM_QUIZ_STATUS_TABLE];
        
        isExecuted = [db executeUpdate:query];
        
        if (!isExecuted) {
            TMLOG(@"Error in creating t");
        }
    }
    
    [db close];
}

-(NSString*)fetchVersion {
    
    NSMutableDictionary *dictToSend = [[NSMutableDictionary alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\"",QZ_CONFIG];
    FMResultSet *rs = [db executeQuery:query];
    //TMLOG(@"result set is %@",rs);
    while ([rs next]) {
        [dictToSend setValue:[rs stringForColumn:@"version"] forKey:@"version"];
        [dictToSend setValue:[rs stringForColumn:@"tstamp"] forKey:@"tstamp"];
    }
    
    [rs close];
    
    [db close];
    
    return [dictToSend valueForKey:@"version"];
}

-(void)updateQuizConfig:(NSString*)version {
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    //Delete the old data
    NSString* deleteQuery = [NSString stringWithFormat:@"DELETE from \"%@\"",QZ_CONFIG];
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    if(!deleteStatus) {
        TMLOG(@"Error in deleting the data of quiz config table::%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    // insert new data
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *currDate = [NSDate date];
    NSString *tstamp = [formatter stringFromDate:currDate];
    
    NSString* insertQuery = [NSString stringWithFormat:@"INSERT into \"%@\" (version,tstamp) VALUES(\"%@\",\"%@\")",QZ_CONFIG,version,tstamp];
    BOOL insertStatus = [db executeUpdate:insertQuery];
    if(!insertStatus){
        TMLOG(@"Error in inserting the data of quiz config table::%@ %@",db.lastError,db.lastErrorMessage);
    }
    [db close];
}

- (void)updateQuiz:(NSArray*)quiz_data forUpdatedVersion:(NSString*) updatedQuizVersion {
    
    NSString* highestQuizID = [self getHighestQuizID];
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSMutableArray* updatedQuizData = [quiz_data mutableCopy];
    
    BOOL isNewQuizAvailable = NO;
    
    //adding the played_by attribute
    for (int index = 0;index < quiz_data.count;index++)
    {
        NSDictionary* localDict = [quiz_data objectAtIndex:index];
        NSMutableDictionary* updatedDict = [localDict mutableCopy];
        [updatedDict setValue:@"" forKey:QUIZ_PLAYED_BY_ATTRIBUTE];
        
        NSDictionary* oldQuizDict = [self getQuizDictForID:[updatedDict valueForKey:@"quiz_id"]];
        
        if (oldQuizDict) {
            updatedDict = [oldQuizDict mutableCopy];
        }
        
        if ([highestQuizID intValue] >= 0) {
            //previous DB exists
            if ([[updatedDict valueForKey:@"quiz_id"] intValue] > [highestQuizID intValue]) {
                //these are the new quizzes
                [updatedDict setValue:@"1" forKey:@"is_new"];
                isNewQuizAvailable = YES;
            }
            else {
                [updatedDict setValue:@"0" forKey:@"is_new"];
            }
        }
        
        //add the updated-in-version value
        [updatedDict setObject:updatedQuizVersion forKey:@"updated_in_version"];
        
        [updatedQuizData replaceObjectAtIndex:index withObject:updatedDict];
    }
    
    if (isNewQuizAvailable) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"newQuizAvailable" object:[NSNumber numberWithInt:-1]];
    }
    
    NSArray* oldQuizArray = [self getQuizForUpdatedVersion:NO];
    
    for (int index = 0; index < oldQuizArray.count; index++) {
        if ([[oldQuizArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            NSDictionary* oldQuizDict = [oldQuizArray objectAtIndex:index];
            
            BOOL oldQuizFound = NO;
            
            for (int innerIndex = 0; innerIndex < updatedQuizData.count && !oldQuizFound; innerIndex++) {
                if ([[updatedQuizData objectAtIndex:innerIndex] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary* updatedQuizDict = [updatedQuizData objectAtIndex:innerIndex];
                    if ([updatedQuizDict[@"quiz_id"] isEqualToString:oldQuizDict[@"quiz_id"]]) {
                        oldQuizFound = YES;
                    }
                }
            }
            if (!oldQuizFound) {
                //add the old quiz with its previous version
                [updatedQuizData addObject:oldQuizDict];
            }
        }
    }
    
    quiz_data = [updatedQuizData copy];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:quiz_data];
    NSString *insertQuery = @"insert into quiz(quiz_data) values (?)";
    BOOL insertStatus = [db executeUpdate:insertQuery, data];
    
    if(!insertStatus) {
        TMLOG(@"Inserting query error for the table quiz %@ %@",db.lastError,db.lastErrorMessage);
    }
    [db close];
}

- (NSDictionary*) getQuizDictForID:(NSString*) quizID
{
    if (quizID) {
        //fetching the old quiz state
        // NSMutableArray* quizData = [[NSMutableArray alloc] init];
        NSArray* quizData = [[NSArray alloc] init];
        
        FMDatabase *db = [self databaseInstance];
        [db open];
        
        //TODO::Abhijeet::SQLite work
        NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        }
        [rs close];
        [db close];
        
        for (NSDictionary* quizDict in quizData) {
            if ([[quizDict valueForKey:@"quiz_id"] isEqualToString:quizID]) {
                
                //convert the dictionary into quiz info object
                return quizDict;
            }
        }
        return nil;
    }
    return nil;
}



/**
 * Abhijeet
 * method to check the quiz status for a particular user
 * @param quiz ID
 * @param user ID
 */
- (BOOL) hasQuizWithID:(NSString*) quizID beenTakenByUserID:(NSString*) userID
{
    TMQuizInfo* quizInfo = [self getQuizDataForID:quizID];
    
    //quiz is avaialable
    
    NSArray* userIDArray = quizInfo.playedBy;
    
    if ([userIDArray containsObject:userID])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


/**
 * method to check the flare status for a particular user
 * @param : quiz ID
 * @param : user ID
 * @return : flare status
 */
- (BOOL) isFlareForQuizID:(NSString*) quizID userID:(NSString*) userID
{
    TMQuizInfo* quizInfo = [self getQuizDataForID:quizID];
    
    NSArray* flareIDArray = quizInfo.flareUserArray;
    
    if ([flareIDArray containsObject:userID]) {
        return YES;
    }
    return NO;
}

/**
 * gets the highest quiz id stored in DB
 * @return highest quiz id
 */
- (NSString*) getHighestQuizID
{
    NSString* highestQuizID = @"-1";
    
    NSArray* quizData = [[NSArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next]) {
        quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        
        NSMutableArray* mutableQuizDataArray = [[NSMutableArray alloc] initWithArray:quizData];
        
        for (int index = 0; index < mutableQuizDataArray.count; index++) {
            NSDictionary* quizDict = [mutableQuizDataArray objectAtIndex:index];
            if ([quizDict valueForKey:@"updated_in_version"] && [[quizDict valueForKey:@"updated_in_version"] isValidObject]) {
                // if ([[quizDict valueForKey:@"updated_in_version"] isEqualToString:[self fetchVersion]]) {
                if ([[quizDict valueForKey:@"quiz_id"] intValue] > [highestQuizID intValue]) {
                    highestQuizID = [quizDict valueForKey:@"quiz_id"];
                }
                //}
            }
            else {
                //store the updated version
                //update the quiz version
                [self updateQuizWithID:[quizDict valueForKey:@"quiz_id"] updatedInVersion:[self fetchVersion]];
            }
        }
        [rs close];
        [db close];
        
    }
    return highestQuizID;
}


- (void) updateQuizNewStatus:(BOOL) isNewQuiz forQuizID:(NSString*) quizID
{
    //fetching the old quiz state
    NSArray* quizData = [[NSArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next]) {
        quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
    }
    [rs close];
    [db close];
    
    NSDictionary* requiredQuizDict;
    
    NSMutableArray* mutableQuizDataArray = [[NSMutableArray alloc] initWithArray:quizData];
    
    int quizIndex = -1;
    
    for (int index = 0; index < mutableQuizDataArray.count; index++) {
        NSDictionary* quizDict = [mutableQuizDataArray objectAtIndex:index];
        if ([[quizDict valueForKey:@"quiz_id"] isEqualToString:quizID]) {
            requiredQuizDict = quizDict;
            quizIndex = index;
            [mutableQuizDataArray removeObject:quizDict];
        }
    }
    if (!requiredQuizDict) {
        return;
    }
    
    [requiredQuizDict setValue:isNewQuiz?@"1":@"0" forKey:@"is_new"];
    
    //save the updated state
    if (quizIndex >= 0 && quizIndex < mutableQuizDataArray.count) {
        [mutableQuizDataArray insertObject:requiredQuizDict atIndex:quizIndex];
    }
    else {
        [mutableQuizDataArray addObject:requiredQuizDict];
    }
    
    NSArray* updatedQuizData = [mutableQuizDataArray copy];
    
    // convert nsdictionary into nsdata before insert into table
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:updatedQuizData];
    
    [db open];
    
    //TODO::Abhijeet ::SQLite work
    NSString *insertQuery = @"insert into quiz(quiz_data) values (?)";
    //NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ (played_by) VALUES(%@)",QUIZ_TABLE,];
    BOOL insertStatus = [db executeUpdate:insertQuery, data];
    
    
    if(!insertStatus) {
        TMLOG(@"Inserting query error for the table quiz %@ %@",db.lastError,db.lastErrorMessage);
    }
    
    [db close];
}


/**
 * update the updated_in_version value of the quiz
 */
- (void) updateQuizWithID:(NSString*) quizID updatedInVersion:(NSString*) updatedInVersion {
    if (updatedInVersion && [updatedInVersion isValidObject]) {
        //fetching the old quiz state
        NSArray* quizData;
        
        FMDatabase *db = [self databaseInstance];
        [db open];
        
        NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        }
        [rs close];
        [db close];
        
        NSDictionary* requiredQuizDict;
        
        NSMutableArray* mutableQuizDataArray = [[NSMutableArray alloc] initWithArray:quizData];
        
        int quizIndex = -1;
        
        for (int index = 0; index < mutableQuizDataArray.count; index++) {
            NSDictionary* quizDict = [mutableQuizDataArray objectAtIndex:index];
            if ([[quizDict valueForKey:@"quiz_id"] isEqualToString:quizID]) {
                requiredQuizDict = quizDict;
                quizIndex = index;
                [mutableQuizDataArray removeObject:quizDict];
            }
        }
        
        if (!requiredQuizDict) {
            return;
        }
        
        [requiredQuizDict setValue:updatedInVersion forKey:@"updated_in_version"];
        
        
        //save the updated state
        if (quizIndex >= 0 && quizIndex < mutableQuizDataArray.count) {
            [mutableQuizDataArray insertObject:requiredQuizDict atIndex:quizIndex];
        }
        else {
            [mutableQuizDataArray addObject:requiredQuizDict];
        }
        NSArray* updatedQuizData = [mutableQuizDataArray copy];
        
        // convert nsdictionary into nsdata before insert into table
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:updatedQuizData];
        
        [db open];
        
        //Delete the old data
        NSString* deleteQuery = [NSString stringWithFormat:@"DELETE from \"%@\"",QUIZ_TABLE];
        BOOL deleteStatus = [db executeUpdate:deleteQuery];
        if(!deleteStatus) {
            TMLOG(@"Error in deleting the data of quiz table::%@ %@",db.lastError,db.lastErrorMessage);
        }
        
        [db close];
        
        [db open];
        
        //TODO::Abhijeet ::SQLite work
        NSString *insertQuery = @"insert into quiz(quiz_data) values (?)";
        //NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ (played_by) VALUES(%@)",QUIZ_TABLE,];
        BOOL insertStatus = [db executeUpdate:insertQuery, data];
        
        
        if(!insertStatus) {
            TMLOG(@"Inserting query error for the table quiz %@ %@",db.lastError,db.lastErrorMessage);
        }
        
        [db close];
    }
}


/**
 * update the quiz played by list with the response list
 */
- (void) updateQuizWithID:(NSString*) quizID playedByWithPlayerList:(NSArray*) updatedPlayerList
{
    if (updatedPlayerList && updatedPlayerList.count > 0) {
        
        //fetching the old quiz state
        NSArray* quizData = [[NSArray alloc] init];
        
        FMDatabase *db = [self databaseInstance];
        [db open];
        
        NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        }
        [db close];
        
        NSDictionary* requiredQuizDict;
        
        NSMutableArray* mutableQuizDataArray = [[NSMutableArray alloc] initWithArray:quizData];
        
        int quizIndex = -1;
        
        for (int index = 0; index < mutableQuizDataArray.count; index++) {
            NSDictionary* quizDict = [mutableQuizDataArray objectAtIndex:index];
            if ([[quizDict valueForKey:@"quiz_id"] isEqualToString:quizID]) {
                requiredQuizDict = quizDict;
                quizIndex = index;
                [mutableQuizDataArray removeObject:quizDict];
            }
        }
        
        if (!requiredQuizDict) {
            return;
        }
        
        //creating the playedBy attribute by joining components string
        //  if (quizData && [quizData isKindOfClass:[NSArray class]] && ((NSArray*)quizData).count > 0 && [[((NSArray*)quizData) firstObject] isKindOfClass:[NSDictionary class]] ) {
        
        NSMutableString* playedByList;
        
        //check if it contains the played_by key
        if (![requiredQuizDict objectForKey:QUIZ_PLAYED_BY_ATTRIBUTE]) {
            playedByList = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@""]];
        }
        
        playedByList = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@",[requiredQuizDict objectForKey:QUIZ_PLAYED_BY_ATTRIBUTE]]];
        
        //allow only unique values
        updatedPlayerList = [[NSOrderedSet orderedSetWithArray:updatedPlayerList] array];
        
        NSString* newPlayedByList = [updatedPlayerList componentsJoinedByString:@","];
        
        [playedByList appendString:@","];
        [playedByList appendString:newPlayedByList];
        
        
        NSMutableDictionary* mutableQuizData = [[NSMutableDictionary alloc] initWithDictionary:requiredQuizDict];
        [mutableQuizData setValue:[playedByList copy] forKey:QUIZ_PLAYED_BY_ATTRIBUTE];
        
        //save the updated state
        if (quizIndex >= 0 && quizIndex < mutableQuizDataArray.count) {
            [mutableQuizDataArray insertObject:mutableQuizData atIndex:quizIndex];
        }
        else {
            [mutableQuizDataArray addObject:mutableQuizData];
        }
        NSArray* updatedQuizData = [mutableQuizDataArray copy];
        
        // convert nsdictionary into nsdata before insert into table
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:updatedQuizData];
        
        [db open];
        
        //Delete the old data
        NSString* deleteQuery = [NSString stringWithFormat:@"DELETE from \"%@\"",QUIZ_TABLE];
        BOOL deleteStatus = [db executeUpdate:deleteQuery];
        if(!deleteStatus) {
            TMLOG(@"Error in deleting the data of quiz table::%@ %@",db.lastError,db.lastErrorMessage);
        }
        
        [db close];
        
        [db open];
        
        //TODO::Abhijeet ::SQLite work
        NSString *insertQuery = @"insert into quiz(quiz_data) values (?)";
        //NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ (played_by) VALUES(%@)",QUIZ_TABLE,];
        BOOL insertStatus = [db executeUpdate:insertQuery, data];
        
        
        if(!insertStatus) {
            TMLOG(@"Inserting query error for the table quiz %@ %@",db.lastError,db.lastErrorMessage);
        }
        
        [db close];
    }
}


/**
 * Abhijeet
 * get quiz for a quiz id
 * @param quiz ID
 * @return quiz data
 */
- (TMQuizInfo*) getQuizDataForID:(NSString*) quizID
{
    if (quizID) {
        //fetching the old quiz state
        // NSMutableArray* quizData = [[NSMutableArray alloc] init];
        NSArray* quizData = [[NSArray alloc] init];
        
        FMDatabase *db = [self databaseInstance];
        [db open];
        
        //TODO::Abhijeet::SQLite work
        NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        }
        [rs close];
        [db close];
        
        for (NSDictionary* quizDict in quizData) {
            if ([[quizDict valueForKey:@"quiz_id"] isEqualToString:quizID]) {
                //convert the dictionary into quiz info object
                return [[TMQuizInfo alloc] initWithDictionary:quizDict];
            }
        }
        return nil;
    }
    return nil;
}

-(NSArray*) getQuizForUpdatedVersion:(BOOL) isForUpdatedQuizVersion {
    
    NSArray* quizData = [[NSArray alloc] init];
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
    
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next]) {
        quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
    }
    
    BOOL isQuizToBeShown = YES;
    
    NSMutableArray* versionCheckedQuizData = [[NSMutableArray alloc] init];
    
    for (int index = 0; index < quizData.count; index++) {
        NSDictionary* quizDict;
        if ([[quizData objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            quizDict = [quizData objectAtIndex:index];
            
            isQuizToBeShown = YES;
            
            if ([quizDict valueForKey:@"updated_in_version"] && [[quizDict valueForKey:@"updated_in_version"] isValidObject]) {
                if (![[quizDict valueForKey:@"updated_in_version"] isEqualToString:[self fetchVersion]]) {
                    isQuizToBeShown = NO;
                }
            }
            else {
                //store the updated version
                //update the quiz version
                [self updateQuizWithID:[quizDict valueForKey:@"quiz_id"] updatedInVersion:[self fetchVersion]];
            }
        }
        if (isForUpdatedQuizVersion) {
            if (isQuizToBeShown) {
                [versionCheckedQuizData addObject:quizDict];
            }
        }
        else {
            [versionCheckedQuizData addObject:quizDict];
        }
    }
    
    [rs close];
    [db close];
    
    return versionCheckedQuizData;
}

#pragma mark - Quiz status methods


/**
 * returns user timestamp for the given conversation
 * @param: match ID
 * @param: user ID
 * @return: user timestamp
 */
- (NSString*) getUserTimestampForMatchID:(NSString*) matchID userID:(NSString*) userID {
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* conversationID = [NSString stringWithFormat:@"%@_%@",matchID,userID];
    
    NSString* query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE conversation_id = \"%@\"",TM_QUIZ_STATUS_TABLE,conversationID];
    
    FMResultSet* resultSet = [db executeQuery:query];
    
    NSString* userTimestamp;
    
    while ([resultSet next]) {
        userTimestamp = [resultSet stringForColumn:@"user_timestamp"];
    }
    
    [db close];
    
    return userTimestamp;
}


/**
 * returns the match timestamp for the given conversation
 * @param: match ID
 * @param: user ID
 * @return: match timestamp
 */
- (NSString*) getMatchTimestampForMatchID:(NSString*) matchID userID:(NSString*) userID {
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* conversationID = [NSString stringWithFormat:@"%@_%@",matchID,userID];
    
    NSString* query = [NSString stringWithFormat:@"SELECT match_timestamp FROM \"%@\" WHERE conversation_id = \"%@\"",TM_QUIZ_STATUS_TABLE,conversationID];
    
    FMResultSet* resultSet = [db executeQuery:query];
    
    NSString* matchTimeStamp;
    
    while ([resultSet next]) {
        matchTimeStamp = [resultSet stringForColumn:@"match_timestamp"];
    }
    
    [db close];
    
    return matchTimeStamp;
}


/**
 * returns all the flare quizzes for the match ID
 * @param: match ID
 */
- (NSArray*) getAllFlareQuizzesForMatchID:(NSString*) matchID {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSArray* quizData;
    
    NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
    FMResultSet *rs = [db executeQuery:query];
    
    NSMutableArray* flareQuizIDArray;
    while ([rs next]) {
        quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        for (int index = 0; index < quizData.count; index++) {
            NSArray* quizPlayedByUserArray = [[[quizData objectAtIndex:index] valueForKey:@"played_by"] componentsSeparatedByString:@","];
            
            if ([quizPlayedByUserArray containsObject:[NSString stringWithFormat:@"%@+",matchID]]) {
                if (!flareQuizIDArray) {
                    flareQuizIDArray = [[NSMutableArray alloc] init];
                }
                [flareQuizIDArray addObject:[[quizData objectAtIndex:index] valueForKey:@"quiz_id"]];
            }
        }
    }
    [rs close];
    [db close];
    
    return [flareQuizIDArray copy];
}


- (NSArray*) getAllQuizzesPlayedByUserID:(NSString*) userID {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSArray* quizData;
    
    NSString *query = [NSString stringWithFormat:@"SELECT quiz_data FROM \"%@\"",QUIZ_TABLE];
    FMResultSet *rs = [db executeQuery:query];
    
    NSMutableArray* playedQuizIDArray;
    while ([rs next]) {
        quizData = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"quiz_data"]]];
        for (int index = 0; index < quizData.count; index++) {
            NSArray* quizPlayedByUserArray = [[[quizData objectAtIndex:index] valueForKey:@"played_by"] componentsSeparatedByString:@","];
            
            if ([quizPlayedByUserArray containsObject:userID] || [quizPlayedByUserArray containsObject:[NSString stringWithFormat:@"%@+",userID]]) {
                if (!playedQuizIDArray) {
                    playedQuizIDArray = [[NSMutableArray alloc] init];
                }
                [playedQuizIDArray addObject:[[quizData objectAtIndex:index] valueForKey:@"quiz_id"]];
            }
        }
    }
    return [playedQuizIDArray copy];
}

/**
 * returns the unread quizzes for the given conversation
 * @param: match ID
 * @param: user ID
 * @return: unread quizzes
 */
- (NSArray*) getUnreadQuizzesForMatchID:(NSString*) matchID userID:(NSString*) userID {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* conversationID = [NSString stringWithFormat:@"%@_%@",matchID,userID];
    
    NSString* query = [NSString stringWithFormat:@"SELECT unread_quiz_ids FROM \"%@\" WHERE conversation_id = \"%@\"",TM_QUIZ_STATUS_TABLE,conversationID];
    
    FMResultSet* resultSet = [db executeQuery:query];
    
    NSString* unreadQuizIds;
    
    while ([resultSet next]) {
        unreadQuizIds = [resultSet stringForColumn:@"unread_quiz_ids"];
    }
    
    [db close];
    
    NSMutableArray* unreadQuizzes = [[unreadQuizIds componentsSeparatedByString:@";"] mutableCopy];
    
    for (int index = 0; index < unreadQuizzes.count; index++) {
        if ([[unreadQuizzes objectAtIndex:index] intValue] < 0) {
            [unreadQuizzes removeObject:[unreadQuizzes objectAtIndex:index]];
        }
        if (unreadQuizzes.count == 0 || (unreadQuizzes.count == 1 && [[unreadQuizzes firstObject] isKindOfClass:[NSString class]] && ((NSString*)[unreadQuizzes firstObject]).length == 0)) {
            unreadQuizzes = nil;
        }
    }
    
    return [unreadQuizzes copy];
}


/**
 * inserts entry into the quiz status cache
 */
- (BOOL) insertQuizStatusInfoForMatchID:(NSString*) matchID userID:(NSString*) userID userTimeStamp:(NSString*) userTimestamp matchTimestamp:(NSString*) matchTimeStamp unreadQuizID:(NSArray*) unreadQuizIDArrays
{
    NSString* conversationID = [NSString stringWithFormat:@"%@_%@",matchID,userID];
    
    NSString* unreadQuizIDs = unreadQuizIDArrays.count?[unreadQuizIDArrays componentsJoinedByString:@";"]:@"-1";
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* query = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ (conversation_id, user_timestamp, match_timestamp, unread_quiz_ids) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")",TM_QUIZ_STATUS_TABLE,conversationID,userTimestamp,matchTimeStamp,unreadQuizIDs];
    
    BOOL insertStatus = [self executeUpdateQuery:query withDB:db];
    
    if (!insertStatus) {
        //NSLog(@"quiz status insert failure");
    }
    
    [db close];
    
    return insertStatus;
}


/**
 * removes the quiz from updated played quizzes array
 * @param: match ID
 * @param: quiz ID
 * @return: removal status
 */
- (BOOL) removeQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID quizID:(NSString*) quizID
{
    NSArray* oldQuizArray = [self getUnreadQuizzesForMatchID:matchID userID:userID];
    
    oldQuizArray = [[NSOrderedSet orderedSetWithArray:oldQuizArray] array];
    
    if ([oldQuizArray containsObject:quizID]) {
        NSMutableArray* mutableQuizArray = [oldQuizArray mutableCopy];
        [mutableQuizArray removeObject:quizID];
        oldQuizArray = [mutableQuizArray copy];
    }
    
    NSString* conversationID = [NSString stringWithFormat:@"%@_%@",matchID,userID];
    
    NSString* updatedQuizzesString = (oldQuizArray && oldQuizArray.count > 0)?[oldQuizArray componentsJoinedByString:@";"]:@"-1";
    
    BOOL updateStatus = NO;
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"UPDATE \"%@\" SET unread_quiz_ids = \"%@\" WHERE conversation_id = \"%@\"",TM_QUIZ_STATUS_TABLE,updatedQuizzesString,conversationID];
    
    updateStatus = [self executeUpdateQuery:queryString withDB:db];
    
    [db close];
    
    return updateStatus;
}


/**
 * removes the quiz from updated played quizzes array
 * @param: match ID
 * @param: quiz ID
 * @return: removal status
 */
- (BOOL) removeAllQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID
{
    NSString* conversationID = [NSString stringWithFormat:@"%@_%@",matchID,userID];
    
    NSString* updatedQuizzesString = @"-1";
    
    BOOL updateStatus = NO;
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"UPDATE \"%@\" SET unread_quiz_ids = \"%@\" WHERE conversation_id = \"%@\"",TM_QUIZ_STATUS_TABLE,updatedQuizzesString,conversationID];
    
    updateStatus = [self executeUpdateQuery:queryString withDB:db];
    
    [db close];
    
    return updateStatus;
}
@end

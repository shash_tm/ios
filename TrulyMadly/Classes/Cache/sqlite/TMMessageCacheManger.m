
//
//  TMMessageCacheManger.m
//  TrulyMadly
//
//  Created by Ankit on 08/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMMessageCacheManger.h"
#import "TMConversation.h"
#import "TMChatSeenContext.h"
#import "TMChatContext.h"
#import "TMChatUser.h"
#import "TMMessage.h"
#import "TMStickerDBManager.h"
#import "TMSticker.h"
#import "TMLOG.h"
#import "TMTimestampFormatter.h"
#import "TMDataStore.h"
#import "TMUserSession.h"
#import "TMNotificationHeaders.h"
#import "TMDealMessageContext.h"
#import "TMEventMatch.h"
#import "TMSpark.h"


#define MSG_CONV_DB_DIRECTORY_PATH @".tm/db"
#define MSG_CONV_DB_FILENAME @"chat.db"
#define TMCHATCONVERSATIONLIST_TABLE @"chatconversationlist"
#define TMCHAT_MESSAGES_TABLE @"chatmessages"
#define TMCHAT_DELETEDATA_TABLE @"chatdeletedata"
#define TMCHATUSER_METADATA_TABLE @"chatusermetadata"
#define TMCHATSENT_METADATA_TABLE @"chatsentmetadata"
#define TMCHAT_POLLING_CONNECTION_METADATA_TABLE @"polingconnectionmetadata"
#define TMCHAT_SOCKET_CONNECTION_METADATA_TABLE @"socketconnectionmetadata"
#define TMCHAT_DATESPOT_METADATA_TABLE @"datespotmetadata"
#define TMSPARK_METADATA_TABLE @"sparkmetadata" //spark matches list

#define TM_DB_VERSION 17

@interface TMMessageCacheManger ()

-(TMMessage*)messageFromResultSet:(FMResultSet*)resultSet;

@end


@implementation TMMessageCacheManger

#pragma mark Initialization Methods
#pragma mark -

-(instancetype)init {
    self = [super initWithDatabaseDirectoryPath:MSG_CONV_DB_DIRECTORY_PATH databaseFile:MSG_CONV_DB_FILENAME];
    if(self) {
        [self configureChatDatabaseTables];
    }
    return self;
}

#pragma mark Public Methods
#pragma mark -

-(NSDictionary*)getCachedConversationList {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    NSMutableArray *activeConversationList = [NSMutableArray array];
    NSMutableArray *archivedConversationList = [NSMutableArray array];
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT c.userid, c.matchid, c.message, c.msgts, c.messagesender, c.messagesource,\
                                                          c.messagestatus, c.seen, c.convmessagetype, c.convlistsortts, c.convlink,\
                                                          c.blocked, c.blockshown, c.archive, c.dealstate, c.isselectmember,\
                                                          meta.profilelink, meta.profileimagelink, meta.fname,\
                                                          meta.age, meta.ismstm, meta.issparkmatch\
                                                 FROM \"%@\" c LEFT JOIN %@ meta ON c.matchid = meta.matchid AND c.userid = meta.userid\
                                                 WHERE c.blocked = 0 ORDER BY c.msgts DESC",TMCHATCONVERSATIONLIST_TABLE,TMCHATUSER_METADATA_TABLE];
    //TMLOG(@"Cached Message Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        TMConversation *msgConv = [self messageConversationFromResultSet:rs];
        if(msgConv.isArchived) {
            [archivedConversationList addObject:msgConv];
        }
        else {
            [activeConversationList addObject:msgConv];
        }
        
    }
    [rs close];
    [adb close];
    
    [result setObject:activeConversationList forKey:@"activeconvlist"];
    [result setObject:archivedConversationList forKey:@"archiveconvlist"];
    return result;
}
-(NSInteger)getUnreadConversationCount {
    NSInteger unreadConversationCount = 0;
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE seen=0 AND messagesender=%ld AND blocked=0 LIMIT 1",
                                                 TMCHATCONVERSATIONLIST_TABLE,(long)MESSAGESENDER_MATCH];
    //TMLOG(@"Cached Message Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        unreadConversationCount = 1;
        break;
    }
    [rs close];
    [adb close];
    
    return unreadConversationCount;
}
-(NSDictionary*)getCachedConversationListForShare {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    NSMutableArray *activeConversationList = [NSMutableArray array];
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT c.matchid, c.convlink,\
                       meta.profilelink, meta.profileimagelink, meta.fname\
                       FROM \"%@\" c LEFT JOIN %@ meta ON c.matchid = meta.matchid\
                       WHERE c.blocked = 0 AND meta.ismstm = 0 ORDER BY c.msgts DESC",TMCHATCONVERSATIONLIST_TABLE,TMCHATUSER_METADATA_TABLE];
    //TMLOG(@"Cached Message Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        TMEventMatch *profileConv = [self messageShareConvFromResultSet:rs];
        [activeConversationList addObject:profileConv];
    }
    [rs close];
    [adb close];
    
    [result setObject:activeConversationList forKey:@"convlist"];
    return result;
}


-(NSString*)getLastChatFetchTimestamp {
    NSString *timestamp = NULL;
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@",TMCHAT_SOCKET_CONNECTION_METADATA_TABLE];
    //TMLOG(@"Last Message Timestamp Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        timestamp = [rs stringForColumn:@"lastpolled_messagets"];
    }
    [rs close];
    [adb close];
    
    return timestamp;
}
-(TMMessage*)getLastChatFetchedMessageFromContext:(TMChatContext*)context {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    TMMessage *message = nil;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE matchid=%ld AND msgsender=%ld LIMIT 1",
                                                  TMCHAT_MESSAGES_TABLE, (long)context.matchId, (long)MESSAGESENDER_MATCH];
    TMLOG(@"Last Fetched Message Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        message = [self messageFromResultSet:rs];
    }
    [rs close];
    [adb close];
    
    return message;
}
-(NSString*)getLastMessageId:(NSInteger)matchId {
    NSString *messageId = NULL;
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE matchid=%ld",TMCHAT_POLLING_CONNECTION_METADATA_TABLE,(long)matchId];
    TMLOG(@"Last Message Timestamp Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        messageId = [rs stringForColumn:@"lastpolled_messageid"];
    }
    [rs close];
    [adb close];
    
    return messageId;
}
-(TMChatUser*)getChatUserMetaData:(TMChatContext*)context {
    TMChatUser *chatUser = nil;
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    chatUser = [self chatUserDataFromContext:context dbInstance:adb];
    
    [adb close];
    
    return chatUser;
}
-(TMChatUser*)chatUserDataFromContext:(TMChatContext*)context dbInstance:(FMDatabase*)adb {
    TMChatUser *chatUser = nil;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE matchid=%ld",TMCHATUSER_METADATA_TABLE,(long)context.matchId];
    //TMLOG(@"MutualMatchInfo Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        chatUser = [[TMChatUser alloc] init];
        chatUser.userId = [rs intForColumn:@"matchid"];
        chatUser.profileLinkURLString = [rs stringForColumn:@"profilelink"];
        chatUser.profileImageURLString = [rs stringForColumn:@"profileimagelink"];;
        chatUser.fName = [rs stringForColumn:@"fname"];
        chatUser.isMsTM = [rs intForColumn:@"matchid"];
        chatUser.doodleLink = [rs stringForColumn:@"doodlelink"];
        chatUser.age = [rs intForColumn:@"age"];
        chatUser.city = [rs stringForColumn:@"city"];
        chatUser.designation = [rs stringForColumn:@"designation"];
    }
    [rs close];
    return chatUser;
}

-(TMChatSeenContext*)getCachedChatMetaData:(TMChatContext*)context {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    TMChatSeenContext *seencontext = nil;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE matchid=%ld", TMCHATSENT_METADATA_TABLE,(long)context.matchId];
    //TMLOG(@"chat metadata get query:%@",query);
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        seencontext = [[TMChatSeenContext alloc] init];
        seencontext.lastSeenMessageTimestamp = [rs stringForColumn:@"lastseen_messagets"];
        seencontext.lastSeenMessageId = [rs stringForColumn:@"lastseen_messageid"];
        seencontext.matchId = [rs intForColumn:@"matchid"];
    }
    [rs close];
    [adb close];
    return seencontext;
}
-(NSMutableArray*)getCachedMessages:(TMChatContext*)chatContext {
    NSParameterAssert(chatContext != nil);
   
    NSMutableArray *results = [NSMutableArray array];
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld group by messageid ORDER BY msgts",TMCHAT_MESSAGES_TABLE,(long)chatContext.matchId];
    //TMLOG(@"getCachedMessages Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        TMMessage *msg = [self messageFromResultSet:rs];
        if([msg isCuratedDealMessage]) {
            [self setDatespotMetaData:&msg withDBInstance:adb];
        }
        [results addObject:msg];
    }
    [rs close];
    [adb close];
    
    return results;
}
-(NSString*)getChatDeleteTimestampForMatchId:(NSInteger)matchId withDBInstance:(FMDatabase*)adb {
    NSString *timestamp = nil;

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld",TMCHAT_DELETEDATA_TABLE,(long)matchId];
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        timestamp = [rs stringForColumn:@"deletets"];
    }
    [rs close];

    return timestamp;
}
-(NSString*)getDealMessageTimesampSavedAsTextMessageFromTimestamp:(NSString*)timestamp {
    NSString *dealTS = nil;
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE text like '%@%%' AND\
                                                   xts>\"%@\" ORDER BY msgts",TMCHAT_MESSAGES_TABLE,@"Hey, want to meet at",timestamp];
    //TMLOG(@"getCachedMessages Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        TMMessage *msg = [self messageFromResultSet:rs];
        TMLOG(@"_________TEST:%@",msg.timestamp);
        dealTS = msg.timestamp;
        break;
    }
    [rs close];
    [adb close];
    
    return dealTS;
}
-(NSString*)getPhotoShareMessageTimesampSavedAsTextMessageFromTimestamp:(NSString*)timestamp {
    NSString *dealTS = nil;
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE text like '%%%@%%' AND\
                       xts>\"%@\" ORDER BY msgts",TMCHAT_MESSAGES_TABLE,@"an image!",timestamp];
    //TMLOG(@"getCachedMessages Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        TMMessage *msg = [self messageFromResultSet:rs];
        TMLOG(@"_________TEST:%@",msg.timestamp);
        dealTS = msg.timestamp;
        break;
    }
    [rs close];
    [adb close];
    
    return dealTS;
}
-(TMSpark*)getActiveSpark {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    TMSpark *spark = [self getSparkWithDBInstance:adb];
    if([spark isSparkExpired]) {
        [self deleteSpark:[spark chatContext]];
        spark = [self getSparkWithDBInstance:adb];
    }
    
    if(spark) {
        TMChatUser *chatUser = [self chatUserDataFromContext:[spark chatContext] dbInstance:adb];
        [spark updateMatchData:chatUser];
    }
    
    [adb close];
    
    return spark;
}
-(NSInteger)getSparkCount {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSUInteger count = [adb intForQuery:[NSString stringWithFormat:@"SELECT COUNT(matchid) FROM %@",TMSPARK_METADATA_TABLE]];

    [adb close];
    
    return count;
}

-(TMSpark*)getSparkWithDBInstance:(FMDatabase*)adb {
    TMSpark *spark = nil;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" LIMIT 1",TMSPARK_METADATA_TABLE];
    //TMLOG(@"getCachedMessages Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        spark = [self sparkFromResultSet:rs];
        break;
    }
    [rs close];
    
    return spark;
}
/////////////////////////////////////////////////////////////////////////

#pragma mark - caching methods

-(void)cacheConversationList:(NSArray*)conversationList {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    NSString *rowupdatedTs = nil;
    for (int i=0; i<conversationList.count; i++) {
       // id messageObj = [conversationList objectAtIndex:i];
        NSDictionary *dataDict = [conversationList objectAtIndex:i];
        NSDictionary *convDict = [dataDict objectForKey:@"message_list"];
        TMConversation *messageConv = [[TMConversation alloc] initWithDictionary:convDict];
        if(rowupdatedTs == nil) {
            rowupdatedTs = messageConv.conversationSortTs;
        }
        else {
            BOOL isNewMessage = [[TMTimestampFormatter sharedFormatter] istimestamp:messageConv.conversationSortTs
                                                        greatetThanTimestamp:rowupdatedTs];
            if(isNewMessage) {
                //TMLOG(@"row updated ts changed");
                rowupdatedTs = messageConv.conversationSortTs;
            }
        }
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND userid=%ld ",TMCHATCONVERSATIONLIST_TABLE,(long)messageConv.matchId,(long)messageConv.userId];
        //TMLOG(@"Cached Conversaiton List Query:%@",query);
        BOOL isConversationExist = FALSE;
        
        TMConversation *cachedConversationMessage = nil;
        FMResultSet *rs = [adb executeQuery:query];
        while ([rs next]) {
            isConversationExist = TRUE;
            cachedConversationMessage = [[TMConversation alloc] init];
            cachedConversationMessage.userId = [rs intForColumn:@"userid"];
            cachedConversationMessage.matchId = [rs intForColumn:@"matchid"];
            cachedConversationMessage.message = [rs stringForColumn:@"message"];
            cachedConversationMessage.timestamp = [rs stringForColumn:@"msgts"];
            cachedConversationMessage.conversationSortTs = [rs stringForColumn:@"convlistsortts"];
            cachedConversationMessage.seen = [rs intForColumn:@"seen"];
            cachedConversationMessage.sender = [rs intForColumn:@"messagesender"];
            cachedConversationMessage.dealState = [rs intForColumn:@"dealstate"];
            break;
        }
        [rs close];
        
        if(isConversationExist) {
            BOOL isNewMessage = [[TMTimestampFormatter sharedFormatter] istimestamp:messageConv.timestamp
                                                        greatetThanTimestamp:cachedConversationMessage.timestamp];
            if(isNewMessage) {
                [self updateConversationList:messageConv withDBInstance:adb];
            }
            else {
                BOOL isConversationServerRowUpdate = [[TMTimestampFormatter sharedFormatter] istimestamp:messageConv.conversationSortTs greatetThanTimestamp:cachedConversationMessage.conversationSortTs];
                if((isConversationServerRowUpdate) && ([cachedConversationMessage isUnreadMessageConversation]) && (messageConv.seen == TRUE)) {
                    /////conv can be blocked also in case of previous unread message
                    [self updateConversationAsRead:[messageConv getChatContext] withDBInstance:adb];
                }
                if((isConversationServerRowUpdate) && (messageConv.isBlocked == TRUE)) {
                    [self updateConversationAsBlocked:messageConv withDBInstance:adb];
                }
            }
        }
        else {
            [self insertConversationListMessage:messageConv withDBInstance:adb];
        }
    }
    
    [adb commit];
    [adb close];
    
    if(rowupdatedTs) {
        [TMDataStore setObject:rowupdatedTs forKey:@"rowupdatedts"];
    }
}
-(void)cacheSparkConversation:(TMConversation*)sparkConversation {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    BOOL status = FALSE;
    NSString *insertQuery = @"INSERT INTO chatconversationlist (userid,matchid,message,msgts,messagesender,messagesource,messagestatus,seen,\
    convmessagetype,convlistsortts,convlink,blocked,blockshown,archive)\
    VALUES (:userid,:matchid,:message,:msgts,:messagesender,:messagesource,:messagestatus,\
    :seen,:convmessagetype,:convlistsortts,:convlink,:blocked,:blockshown,:archive)";
    
    //TMLOG(@"conversation list insert query:%@",insertQuery);
    status = [self executeUpdateQuery:insertQuery withDB:adb withArgs:[sparkConversation getInsertQueryParams]];
    if(!status) {
        TMLOG(@"cache send message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}
-(void)cacheMessages:(NSArray*)messages fromSource:(TMMessageSource)messageSource {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    for (int i=0; i<messages.count; i++) {
        id messageObj = [messages objectAtIndex:i];
        TMMessage *message = [self messageFromData:messageObj];
        message.source = messageSource;
        NSString *deleteTs = [self getChatDeleteTimestampForMatchId:message.matchId withDBInstance:adb];
        [self cacheMissedMessage:message withDBInstance:adb withDeleteTs:deleteTs];
        
        ///update conversation list
        [self updateConversationListForMessage:message withDBInstance:adb];
    }

    [adb commit];
    [adb close];
}
-(void)cacheNewUnreadMessage:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self cacheMissedMessage:message withDBInstance:adb withDeleteTs:nil];
    
    ///update conversation list
    [self updateConversationListForMessage:message withDBInstance:adb];
    
    //[adb commit];
    [adb close];
}
-(void)cacheMissedMessage:(TMMessage*)message withDBInstance:(FMDatabase*)adb withDeleteTs:(NSString*)deleteTs {
    BOOL canCacheMessage = TRUE;
    if(deleteTs) {
        canCacheMessage = [[TMTimestampFormatter sharedFormatter] istimestamp:message.timestamp
                                                           greatetThanTimestamp:deleteTs];
        
    }
    if(canCacheMessage) {
        NSString *query = nil;
        
        if(message.source == POLLING) {
            query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND messageid=\"%@\"",TMCHAT_MESSAGES_TABLE,(long)message.matchId,@(message.messageId)];
        }
        else {
            query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND uid=\"%@\"",TMCHAT_MESSAGES_TABLE,(long)message.matchId,message.uid];
        }
        
        //TMLOG(@"Cached Message Query:%@",query);
        BOOL isMessageExist = FALSE;
        
        FMResultSet *rs = [adb executeQuery:query];
        while ([rs next]) {
            isMessageExist = TRUE;
            break;
        }
        [rs close];
        
        if(isMessageExist) {
            //if(message.source != POLLING) {
                [self updateMessage:message withDBInstance:adb];
            //}
        }
        else {
            [self insertMessage:message withDBInstance:adb];
        }
    }
}

-(void)cachedNewMessage:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self insertMessage:message withDBInstance:adb];
    
    [adb close];
}
-(void)updateCachedMessage:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self updateMessage:message withDBInstance:adb];
    
    [self updateConversationListForMessage:message withDBInstance:adb];
    
    [adb close];
}
-(void)updateMetaParams:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *updateQuery = @"UPDATE chatmessages SET metadata=:metadata WHERE randomid=:randomid AND matchid=:matchid";
    //TMLOG(@"update query:%@",updateQuery);
    
    BOOL status = [self executeUpdateQuery:updateQuery withDB:adb withArgs:[message getMetadataUpdateCacheParams]];
    if(!status) {
        TMLOG(@"update message metadata erorr:%@ %@",adb.lastError,adb.lastErrorMessage);
    }

    [adb close];
}

-(void)saveChatUserMetaData:(TMChatUser*)chatUser chatContext:(TMChatContext*)context {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self cacheMatchData:chatUser withContext:context withDBInstance:adb];
    
    [adb close];
}

-(void)cacheMatchData:(TMChatUser*)chatUser withContext:(TMChatContext*)context withDBInstance:(FMDatabase*)adb {
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE matchid=%ld AND userid=%ld",TMCHATUSER_METADATA_TABLE,(long)chatUser.userId,(long)context.loggedInUserId];
    
    TMChatUser *cachedChatUser = nil;
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        cachedChatUser = [[TMChatUser alloc] init];
        cachedChatUser.userId = [rs intForColumn:@"matchid"];
        cachedChatUser.age = [rs intForColumn:@"age"];
        cachedChatUser.fName = [rs stringForColumn:@"fname"];
        cachedChatUser.profileLinkURLString = [rs stringForColumn:@"profilelink"];
        cachedChatUser.profileImageURLString = [rs stringForColumn:@"profileimagelink"];
        cachedChatUser.isMsTM = [rs intForColumn:@"ismstm"];
        cachedChatUser.isSparkMatch = [rs intForColumn:@"issparkmatch"];
    }
    [rs close];
    
    BOOL status;
    if(cachedChatUser) {
        if(chatUser.profileLinkURLString) {
            cachedChatUser.profileLinkURLString = chatUser.profileLinkURLString;
        }
        if(chatUser.age != 0) {
            cachedChatUser.age = chatUser.age;
        }
        if(chatUser.fName) {
            cachedChatUser.fName = chatUser.fName;
        }
        if(chatUser.profileImageURLString) {
            cachedChatUser.profileImageURLString = chatUser.profileImageURLString;
        }
        
        //////
        NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET profilelink=\"%@\",\
                                 profileimagelink=\"%@\", fname=\"%@\", age=%ld, ismstm=%d WHERE matchid=%ld",
                                 TMCHATUSER_METADATA_TABLE, cachedChatUser.profileLinkURLString, cachedChatUser.profileImageURLString,
                                 cachedChatUser.fName,(long)cachedChatUser.age,cachedChatUser.isMsTM,
                                 (long)cachedChatUser.userId];
        status = [self executeUpdateQuery:updateQuery withDB:adb];
        if(!status) {
            TMLOG(@"ChatUserMetaData update query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
    else {
        if(chatUser.isMsTM) {
            [TMDataStore setBool:TRUE forKey:@"create_miss_tm"];
        }
        //////
        NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO %@ (userid,matchid,profilelink,\
                                 profileimagelink,fname,age,ismstm,issparkmatch,city,designation)\
                                 VALUES (%ld,%ld,\"%@\",\"%@\",\"%@\",%ld,%d,%d,\"%@\",\"%@\")",
                                 TMCHATUSER_METADATA_TABLE, (long)context.loggedInUserId,
                                 (long)chatUser.userId,chatUser.profileLinkURLString,
                                 chatUser.profileImageURLString,chatUser.fName,(long)chatUser.age,
                                 chatUser.isMsTM,chatUser.isSparkMatch,chatUser.city,chatUser.designation];
        //TMLOG(@"usermetadata insertquery:%@",insertQuery);
        status = [self executeUpdateQuery:insertQuery withDB:adb];
        if(!status) {
            TMLOG(@"ChatUserMetaData insert query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
    
    //update mstm userid in userdefault for notification use.
    if(chatUser.isMsTM) {
        [TMDataStore setObject:@(chatUser.userId) forKey:@"com.mstm.userid"];
    }
}
-(void)cacheChatMetaData:(TMChatSeenContext*)context {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    /*@try*/ {
        NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET lastseen_messageid=\"%@\", lastseen_messagets=\"%@\"\
                                 WHERE matchid=%ld", TMCHATSENT_METADATA_TABLE, context.lastSeenMessageId,
                                 context.lastSeenMessageTimestamp, (long)context.matchId];
        ////TMLOG(@"chat metadata updateQuery query:%@",updateQuery);
        
        NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR IGNORE INTO %@ (userid, matchid, lastseen_messageid, lastseen_messagets)\
                                 VALUES (%ld,%ld,\"%@\",\"%@\")",
                                 TMCHATSENT_METADATA_TABLE, (long)context.userId,
                                 (long)context.matchId, context.lastSeenMessageId,
                                 context.lastSeenMessageTimestamp];
        ////TMLOG(@"chat metadata insert query:%@",insertQuery);
        BOOL status;
        status = [self executeUpdateQuery:updateQuery withDB:adb];
        status = [self executeUpdateQuery:insertQuery withDB:adb];
        if(!status) {
            //TMLOG(@"saveMessageData Error:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
    /*@catch (NSException *exception) {
     [adb close];
     }*/
    
    [adb close];
}
-(void)cacheSparkData:(NSArray*)sparkData {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [adb beginTransaction];
    
    //delete all spark data which are not seen
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE seen=0",TMSPARK_METADATA_TABLE];
    BOOL status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"sparkmetadata delete query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    for (int i=0; i<sparkData.count; i++) {
        NSDictionary *sparkDictionary = sparkData[i];
        TMSpark *spark = [[TMSpark alloc] initWithSparkDictionary:sparkDictionary];
        
        NSString *insertQuery  = @"INSERT OR IGNORE INTO sparkmetadata (userid,matchid,message,messageid,exptime,hash,\
                                                                        convlink,starttime,seen,createdate,imageurls,selectmember)\
                                   VALUES (:userid,:matchid,:message,:messageid,:exptime,:hash,:convlink,\
                                           :starttime,:seen,:createdate,:imageurls,:selectmember)";
            
        //TMLOG(@"spark insert query:%@",insertQuery);
        NSDictionary *params = [spark sparkInsertQueryParams];
        status = [self executeUpdateQuery:insertQuery withDB:adb withArgs:params];
        if(!status) {
            TMLOG(@"cache send message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
        
        //cache chat user metadata
        [self cacheMatchData:spark.matchData withContext:spark.chatContext withDBInstance:adb];
        
        //cache spark message if exist
        if(spark.messageId){
            TMMessage *sparkMessage = [[TMMessage alloc] initSparkSendMessageWithText:spark.message
                                                                            messageId:spark.messageId
                                                                           receiverId:spark.chatContext.loggedInUserId
                                                                             senderId:spark.matchId
                                                                            messageTs:spark.sparkCreateTime];
            
            [self cacheMissedMessage:sparkMessage withDBInstance:adb withDeleteTs:nil];
        }
    }
    
    [adb commit];
    [adb close];
}

-(void)updatePolligMessage:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
   NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET lastpolled_messageid=\"%@\"\
                             WHERE matchid=%ld", TMCHATSENT_METADATA_TABLE,[message messageIdAsString],
                             (long)message.matchId];
    ////TMLOG(@"chat metadata updateQuery query:%@",updateQuery);
    
    NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR IGNORE INTO %@ (userid, matchid, lastpolled_messageid)\
                             VALUES (%ld,%ld,\"%@\")",
                             TMCHATSENT_METADATA_TABLE, (long)message.loggedInUserId,
                             (long)message.matchId, [message messageIdAsString]];
    //////TMLOG(@"chat metadata insert query:%@",insertQuery);
    BOOL status;
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    status = [self executeUpdateQuery:insertQuery withDB:adb];
    if(!status) {
        TMLOG(@"updatePolligMessage query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
   
    [adb close];
}

-(void)updatePolligConnection:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET lastpolled_messageid=\"%@\"\
                             WHERE matchid=%ld", TMCHAT_POLLING_CONNECTION_METADATA_TABLE,[message messageIdAsString],
                             (long)message.matchId];
    ////TMLOG(@"chat metadata updateQuery query:%@",updateQuery);
    
    NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR IGNORE INTO %@ (userid, matchid, lastpolled_messageid)\
                             VALUES (%ld,%ld,\"%@\")",
                             TMCHAT_POLLING_CONNECTION_METADATA_TABLE, (long)message.loggedInUserId,
                             (long)message.matchId, [message messageIdAsString]];
    ////TMLOG(@"chat metadata insert query:%@",insertQuery);
    BOOL status;
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    status = [self executeUpdateQuery:insertQuery withDB:adb];
    if(!status) {
        TMLOG(@"updatePolligConnection query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }

    [adb close];
}

-(void)updateSocketConnection:(TMMessage*)message {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET lastpolled_messagets=\"%@\"\
                             WHERE userid=%ld", TMCHAT_SOCKET_CONNECTION_METADATA_TABLE,[message timestamp],
                             (long)message.loggedInUserId];
    ////TMLOG(@"chat metadata updateQuery query:%@",updateQuery);
    
    NSString *insertQuery = [NSString stringWithFormat:@"INSERT OR IGNORE INTO %@ (userid, lastpolled_messagets)\
                             VALUES (%ld,\"%@\")",
                             TMCHAT_SOCKET_CONNECTION_METADATA_TABLE, (long)message.loggedInUserId, [message timestamp]];
    ////TMLOG(@"chat metadata insert query:%@",insertQuery);
    BOOL status;
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    status = [self executeUpdateQuery:insertQuery withDB:adb];
    if(!status) {
        TMLOG(@"updateSocketConnection query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
  
    [adb close];
}
-(void)updateDeleteConversationMetadata:(TMChatContext*)context tillTimestamp:(NSString*)timestamp {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self deleteMessageWithContext:context tillTimestamp:timestamp withDBInstance:adb];
    
    [adb close];
}
-(void)deleteSpark:(TMChatContext*)chatContext {
    FMDatabase *adb = [self databaseInstance];
    [adb open];

    //delete record from spark table
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE matchid=%ld AND userid=%ld",TMSPARK_METADATA_TABLE,
                                                                        (long)chatContext.matchId,(long)chatContext.loggedInUserId];
    BOOL status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"sparkmetadata delete query Error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    //delete record chat user metadata table
    query = [NSString stringWithFormat:@"DELETE FROM %@ where matchid=%ld AND userid=%ld",TMCHATUSER_METADATA_TABLE,
                                                             (long)chatContext.matchId,(long)chatContext.loggedInUserId];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}
-(void)deleteMessageWithContext:(TMChatContext*)context tillTimestamp:(NSString*)timestamp withDBInstance:(FMDatabase*)adb {
    
    //cache timestamp
    [self cacheDeleteChatMetadata:context withTimestamp:timestamp DBInstance:adb];
    
    //delete messages
    NSString *query = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE xts<=\"%@\" AND matchid=%ld",TMCHAT_MESSAGES_TABLE,timestamp,(long)context.matchId];
    BOOL status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"delete message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    //clear message from conversationlist
    query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND userid=%ld ",TMCHATCONVERSATIONLIST_TABLE,(long)context.matchId,(long)context.loggedInUserId];
    //TMLOG(@"Cached Conversaiton List Get Query:%@",query);
    BOOL isConversationExist = FALSE;
    
    TMConversation *cachedConversationMessage = nil;
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        isConversationExist = TRUE;
        cachedConversationMessage = [[TMConversation alloc] init];
        cachedConversationMessage.userId = [rs intForColumn:@"userid"];
        cachedConversationMessage.matchId = [rs intForColumn:@"matchid"];
        cachedConversationMessage.timestamp = [rs stringForColumn:@"msgts"];
        break;
    }
    [rs close];
    
    BOOL canClearConversation = TRUE;
    if(isConversationExist) {
        canClearConversation = [[TMTimestampFormatter sharedFormatter] istimestamp:timestamp
                                                                   greatetThanEqualToTimestamp:cachedConversationMessage.timestamp];
    }
    if(canClearConversation) {
        query = [NSString stringWithFormat:@"UPDATE %@ SET message=\"%@\" WHERE matchid=%ld AND userid=%ld",
                 TMCHATCONVERSATIONLIST_TABLE,@"",(long)context.matchId,
                 (long)context.loggedInUserId];
        
        //TMLOG(@"conversation list update query:%@",updateQuery);
        status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
}
-(void)deleteMessage:(TMMessage*)message {
    
    FMDatabase *adb = [self databaseInstance];
    [adb open];

    [self deleteMessage:message withDBInstance:adb];
    
    [adb close];
}

-(void)deleteMessage:(TMMessage*)message withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE uid=\"%@\" AND matchid=%ld",
                       TMCHAT_MESSAGES_TABLE,message.uid, (long)message.matchId];
    //TMLOG(@"delete message query:%@",query);
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"delete message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}

-(void)markSparkAsSeen:(TMChatContext*)chatContext withStartTime:(NSString*)startTime {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET seen=%ld,starttime=\"%@\" WHERE userid=%ld AND matchid=%ld",
                       TMSPARK_METADATA_TABLE,(long)1,startTime,(long)chatContext.loggedInUserId,(long)chatContext.matchId];
    //TMLOG(@"Cached Message Query:%@",query);
    BOOL status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"spark list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}
-(void)markMessagesInSendingStateAsFailed:(TMChatContext*)chatContext {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND deliverystatus=%ld",TMCHAT_MESSAGES_TABLE,(long)chatContext.matchId,(long)0];
    //TMLOG(@"Cached Message Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        //retrieve values for each record
        TMMessage *msg = [self messageFromResultSet:rs];
        msg.deliveryStatus = SENDING_FAILED;
        msg.messageId = 0;
        [self updateMessage:msg withDBInstance:adb];
    }
    [rs close];
    [adb close];
}

-(void)setCachedConversationAsBlockedShown:(TMChatContext*)chatContext {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self updateConversationAsBlockedShown:chatContext withDBInstance:adb];
    
    [adb close];
}
-(void)archiveConversation:(TMConversation*)conversationMessage {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self updateConversationList:conversationMessage withDBInstance:adb];
    
    [adb close];
}
-(void)markConversationAsRead:(TMChatContext*)chatContext {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self updateConversationAsRead:chatContext withDBInstance:adb];
    
    [adb close];
}
-(void)markConversationAsUnRead:(TMChatContext*)chatContext {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    [self updateConversationAsUnRead:chatContext withDBInstance:adb];
    
    [adb close];
}
-(void)updateCachedPhotoMessgaesInSendingStateAsFailed {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET deliverystatus=%ld WHERE msgtype=%ld AND deliverystatus=%ld",
             TMCHAT_MESSAGES_TABLE,SENDING_FAILED,MESSAGETTYPE_PHOTO,SENDING];
    //TMLOG(@"Cached Message Query:%@",query);
    BOOL status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}
-(void)cacheDealState:(NSInteger)dealState withMatch:(NSString*)matchId {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET dealstate=%ld WHERE matchid=%ld",
                             TMCHATCONVERSATIONLIST_TABLE,(long)dealState, (long)[matchId integerValue]];
    
    //TMLOG(@"deal update query:%@",updateQuery);
    BOOL status = [self executeUpdateQuery:updateQuery withDB:adb];
    if(!status) {
        TMLOG(@"deal update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}
-(void)cacheDoodleLink:(NSString*)doodleLink withMatch:(NSInteger)matchId {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET doodlelink=\"%@\" WHERE matchid=%ld",
                             TMCHATUSER_METADATA_TABLE,doodleLink,(long)matchId];
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    BOOL status = [self executeUpdateQuery:updateQuery withDB:adb];

    if(!status) {
        TMLOG(@"update message metadata erorr:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}
-(void)cacheDeleteChatMetadata:(TMChatContext*)context withTimestamp:(NSString*)timestamp DBInstance:(FMDatabase*)adb {
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND userid=%ld ",TMCHAT_DELETEDATA_TABLE,(long)context.matchId,(long)context.loggedInUserId];
    FMResultSet *rs = [adb executeQuery:query];
    BOOL isRecordExist = FALSE;
    while ([rs next]) {
        isRecordExist = TRUE;
        break;
    }
    [rs close];
    
    if(isRecordExist) {
        NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET userid=%ld,matchid=%ld,messageid=%@,deletets=\"%@\"\
                                 WHERE matchid=%ld AND userid=%ld",
                                 TMCHAT_DELETEDATA_TABLE,(long)context.loggedInUserId,(long)context.matchId,@"null",timestamp,
                                 (long)context.matchId,(long)context.loggedInUserId];
        
        //TMLOG(@"conversation list update query:%@",updateQuery);
        BOOL status = [self executeUpdateQuery:updateQuery withDB:adb];
        if(!status) {
            TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
    else {
        query = [NSString stringWithFormat:@"INSERT INTO %@ (userid,matchid,messageid,deletets)\
                 VALUES (%ld,%ld,\"%@\",\"%@\")",
                 TMCHAT_DELETEDATA_TABLE, (long)context.loggedInUserId,(long)context.matchId,
                 @"null",timestamp];
        
        BOOL status = [self executeUpdateQuery:query withDB:adb];
        if(!status) {
            TMLOG(@"deletedata query error:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
}
-(void)deleteCachedConversation {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    BOOL status = FALSE;
    
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHATCONVERSATIONLIST_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    [adb close];
}

-(void)deleteAllCachedMessageContent {
    FMDatabase *adb = [self databaseInstance];
    [adb open];
    
    BOOL status = FALSE;
    
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHATCONVERSATIONLIST_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHAT_MESSAGES_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHATUSER_METADATA_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHATSENT_METADATA_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHAT_POLLING_CONNECTION_METADATA_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHAT_SOCKET_CONNECTION_METADATA_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    query = [NSString stringWithFormat:@"DELETE FROM %@",TMCHAT_DELETEDATA_TABLE];
    status = [self executeUpdateQuery:query withDB:adb];
    if(!status) {
        TMLOG(@"error in deleting chat delete info data error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    
    [adb close];
}


#pragma mark Internal Utilities
#pragma mark -
-(void)insertConversationListMessage:(TMConversation*)conversationMessage withDBInstance:(FMDatabase*)adb {
    NSString *deleteTs = [self getChatDeleteTimestampForMatchId:conversationMessage.matchId withDBInstance:adb];
    if(deleteTs) {
        //check if consersation is cleared till messsage timestamp or not
        BOOL canCacheConversation = [[TMTimestampFormatter sharedFormatter] istimestamp:conversationMessage.timestamp
                                                                   greatetThanTimestamp:deleteTs];
        if(!canCacheConversation) {
            conversationMessage.message = @"";
        }
    }
    
    BOOL status = FALSE;
    NSString *insertQuery = @"INSERT INTO chatconversationlist (userid,matchid,message,msgts,messagesender,messagesource,messagestatus,seen,\
                                                        convmessagetype,convlistsortts,convlink,blocked,blockshown,archive,isselectmember)\
                                                VALUES (:userid,:matchid,:message,:msgts,:messagesender,:messagesource,:messagestatus,\
                                                        :seen,:convmessagetype,:convlistsortts,:convlink,:blocked,:blockshown,:archive,\
                                                        :isselectmember)";
    
    //TMLOG(@"conversation list insert query:%@",insertQuery);
    status = [self executeUpdateQuery:insertQuery withDB:adb withArgs:[conversationMessage getInsertQueryParams]];
    if(!status) {
        TMLOG(@"cache send message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    //cache chat user metadata
    [self cacheMatchData:[conversationMessage getChatUser] withContext:[conversationMessage getChatContext] withDBInstance:adb];
}
-(void)updateConversationList:(TMConversation*)conversationMessage withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *updateQuery = @"UPDATE chatconversationlist SET userid=:userid,matchid=:matchid,message=:message,msgts=:msgts,\
                                                      messagesender=:messagesender,messagesource=:messagesource,\
                                                      messagestatus=:messagestatus,seen=:seen,convmessagetype=:convmessagetype,\
                                                      convlistsortts=:convlistsortts,convlink=:convlink,blocked=:blocked,\
                                                      blockshown=:blockshown,archive=:archive,isselectmember=:isselectmember\
                                                WHERE matchid=:matchidw AND userid=:useridw";
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb withArgs:[conversationMessage getUpdateQueryParams]];
    if(!status) {
        TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}
-(void)updateConversationListForMessage:(TMMessage*)message withDBInstance:(FMDatabase*)adb {
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE matchid=%ld AND userid=%ld ",TMCHATCONVERSATIONLIST_TABLE,(long)message.matchId,(long)message.loggedInUserId];
    //TMLOG(@"Cached Conversaiton List Query:%@",query);
    TMConversation *cachedConversationMessage = nil;
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        cachedConversationMessage = [self conversationFromResultSet:rs];
        break;
    }
    [rs close];
    
    if(cachedConversationMessage) {
        BOOL isNewMessage = [[TMTimestampFormatter sharedFormatter] istimestamp:message.timestamp
                                                           greatetThanTimestamp:cachedConversationMessage.timestamp];
        if(isNewMessage) {
            //post notification for conversationlist reload
            [self postSocketConnectionNotificationWithStatus];
            
            //check is message belong to active conversation then mark message as seen
            if([message isCurrentConversationMessage]) {
                message.isMessageSeen = TRUE;
            }

            //update cached conversation with new message data
            cachedConversationMessage.message = [message conversationListMessage];
            cachedConversationMessage.timestamp = message.timestamp;
            cachedConversationMessage.sender = message.senderType;
            cachedConversationMessage.source = message.source;
            cachedConversationMessage.status = 1;
            cachedConversationMessage.seen = message.isMessageSeen;
            cachedConversationMessage.conversationMessageType = CONVERSATIONMESSAGE_TEXT;
            cachedConversationMessage.conversationSortTs = message.timestamp;
            cachedConversationMessage.isBlocked = message.isBlocked;
            cachedConversationMessage.isBlockedShown = FALSE;
            cachedConversationMessage.isArchived = FALSE;
            [self updateConversationList:cachedConversationMessage withDBInstance:adb];
        }
    }
}
-(void)updateConversationAsRead:(TMChatContext*)chatContext withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET seen=%d WHERE matchid=%ld AND userid=%ld",
                             TMCHATCONVERSATIONLIST_TABLE,1, (long)chatContext.matchId, (long)chatContext.loggedInUserId];
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    if(!status) {
        TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}
-(void)updateConversationAsUnRead:(TMChatContext*)chatContext withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET seen=%d WHERE matchid=%ld AND userid=%ld",
                             TMCHATCONVERSATIONLIST_TABLE,0, (long)chatContext.matchId, (long)chatContext.loggedInUserId];
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    if(!status) {
        TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}
-(void)updateConversationAsBlocked:(TMConversation*)cconversation withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET blocked=%d WHERE matchid=%ld AND userid=%ld",
                             TMCHATCONVERSATIONLIST_TABLE,1, (long)cconversation.matchId, (long)cconversation.userId];
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    if(!status) {
        TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}
-(void)updateConversationAsBlockedShown:(TMChatContext*)chatContext withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE %@ SET blocked=%d, blockshown=%d WHERE matchid=%ld AND userid=%ld",
                             TMCHATCONVERSATIONLIST_TABLE,1, 1, (long)chatContext.matchId, (long)chatContext.loggedInUserId];
    
    //TMLOG(@"conversation list update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb];
    if(!status) {
        TMLOG(@"conversation list update query error:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}

////////////////////////////////////////////////////////////////////////////////////////
-(void)insertMessage:(TMMessage*)message withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    NSString *insertQuery  = @"INSERT INTO chatmessages (matchid,userid,randomid,uid,messageid,senderid,receiverid,quizid,\
                                                         text,msgtype,deliverystatus,msgts,xts,msgsender,source,gallery_id,\
                                                         sticker_id,datespotid,dealid,metadata)\
                                                 VALUES (:matchid,:userid,:randomid,:uid,:messageid,:senderid,:receiverid,:quizid,:text,\
                                                         :msgtype,:deliverystatus,:msgts,:xts,:msgsender,:source,:gallery_id,:sticker_id,\
                                                         :datespotid,:dealid,:metadata)";
    
    //TMLOG(@"insert message query:%@",insertQuery);
    NSDictionary *params = [message getInsertCacheParams];
    status = [self executeUpdateQuery:insertQuery withDB:adb withArgs:params];
    if(!status) {
        TMLOG(@"cache send message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    //cache datespot data
    if([message isCuratedDealMessage]) {
        [self cacheDateSpotDataFromMessage:message withDBInstance:adb];
    }
}
-(void)updateMessage:(TMMessage*)message withDBInstance:(FMDatabase*)adb {
    BOOL status = FALSE;
    BOOL timestampStatus;
    NSString *updateQuery = nil;
    if(message.senderType == MESSAGESENDER_ME) {
        ///do not update timestamp
        timestampStatus = FALSE;
        if(message.source == POLLING) {
            updateQuery = @"UPDATE chatmessages SET randomid=:randomid,uid=:uid,messageid=:messageid,senderid=:senderid,\
            receiverid=:receiverid,text=:text,msgtype=:msgtype,\
            deliverystatus=:deliverystatus,datespotid=:datespotid,dealid=:dealid,\
            metadata=:metadata WHERE messageid=:messageid1 AND matchid=:matchid";
        }
        else {
            updateQuery = @"UPDATE chatmessages SET randomid=:randomid,uid=:uid,messageid=:messageid,senderid=:senderid,\
            receiverid=:receiverid,text=:text,msgtype=:msgtype,\
            deliverystatus=:deliverystatus,datespotid=:datespotid,dealid=:dealid,\
            metadata=:metadata WHERE uid=:uid1 AND matchid=:matchid";
        }
    }
    else {
        //update timestamp
        timestampStatus = TRUE;
        updateQuery = @"UPDATE chatmessages SET randomid=:randomid,uid=:uid,messageid=:messageid,senderid=:senderid,\
                                                receiverid=:receiverid,text=:text,msgtype=:msgtype,\
                                                deliverystatus=:deliverystatus,msgts=:msgts,xts=:xts,datespotid=:datespotid,\
                                                dealid=:dealid,metadata=:metadata WHERE uid=:uid1 AND matchid=:matchid";
    }
    
    //TMLOG(@"update query:%@",updateQuery);
    status = [self executeUpdateQuery:updateQuery withDB:adb withArgs:[message getUpdateCacheParamsWithTimestampStatus:timestampStatus]];
    if(!status) {
        TMLOG(@"update message Data:%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    if([message isCuratedDealMessage]) {
        [self cacheDateSpotDataFromMessage:message withDBInstance:adb];
    }
}
-(void)setDatespotMetaData:(TMMessage**)message withDBInstance:(FMDatabase*)adb {
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE datespotid=\"%@\"",TMCHAT_DATESPOT_METADATA_TABLE,
                                                                                            (*message).datespotId];
    //TMLOG(@"get datespot data Query:%@",query);
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        [(*message) setDatespotImage:[rs stringForColumn:@"imageurl"]];
        [(*message) setDatespotAddress:[rs stringForColumn:@"address"]];
        //(*dealContext).datespotAddress = [rs stringForColumn:@"address"];
        break;
    }
    [rs close];
}
-(void)cacheDateSpotDataFromMessage:(TMMessage*)message withDBInstance:(FMDatabase*)adb {
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE datespotid=\"%@\"",TMCHAT_DATESPOT_METADATA_TABLE,
                                                                                            message.datespotId];
    //TMLOG(@"Cached datespot data Query:%@",query);
    BOOL isMessageExist = FALSE;
    
    FMResultSet *rs = [adb executeQuery:query];
    while ([rs next]) {
        isMessageExist = TRUE;
        break;
    }
    [rs close];
    
    if(!isMessageExist) {
        NSDictionary *params = [message datespotInsertQueryParmas];
        query = @"INSERT INTO datespotmetadata (userid,datespotid,address,imageurl) VALUES (:userid,:datespotid,:address,:imageurl)";
        BOOL status = [self executeUpdateQuery:query withDB:adb withArgs:params];
        if(!status) {
            TMLOG(@"cache datespot Data:%@ %@",adb.lastError,adb.lastErrorMessage);
        }
    }
}
-(void)configureChatDatabaseTables {
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    [self updateDBVersion:db];
    
    NSString *query = nil;
    BOOL isQueryExecuted = FALSE;
    /* Table:chatmessages
     *** table will store all chat messages
     */
    BOOL isChatMessageTableExist = [db tableExists:TMCHAT_MESSAGES_TABLE];
    if(!isChatMessageTableExist) {
        TMLOG(@"Chat Messages Table Not Exist");
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"matchid\" INTEGER NOT NULL, \"userid\" INTEGER NOT NULL,\
                           \"randomid\" TEXT, \"uid\" TEXT NOT NULL UNIQUE, \"messageid\" TEXT UNIQUE,\"senderid\" INTEGER NOT NULL,\
                            \"receiverid\" INTEGER NOT NULL, \"quizid\" TEXT, \"text\" TEXT,\"msgtype\" INTEGER NOT NULL ,\
                            \"deliverystatus\" INTEGER NOT NULL , \"msgts\" TEXT NOT NULL ,\"xts\" DATETIME NOT NULL,\
                            \"msgsender\" INTEGER NOT NULL,\"source\" INTEGER NOT NULL,\"gallery_id\" TEXT DEFAULT NULL,\
                            \"sticker_id\" TEXT DEFAULT NULL,\"dealid\" TEXT DEFAULT NULL,\"datespotid\" TEXT DEFAULT NULL,\
                            \"metadata\" BLOB DEFAULT NULL)",TMCHAT_MESSAGES_TABLE];
        
        BOOL isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
        }
        
        [self addIndexesOnDBInstance:db];
    }
    else {
        //check for quizid and source columns if not exist then create
        TMLOG(@"Chat Messages Table Exist");
        BOOL isQuizIdColumnExist = [db columnExists:@"quizid" inTableWithName:TMCHAT_MESSAGES_TABLE];
        BOOL isSourceColumnExist = [db columnExists:@"source" inTableWithName:TMCHAT_MESSAGES_TABLE];
        BOOL isGalleryIDColumnExist = [db columnExists:@"gallery_id" inTableWithName:TMCHAT_MESSAGES_TABLE];
        BOOL isStickerIDColumnExist = [db columnExists:@"sticker_id" inTableWithName:TMCHAT_MESSAGES_TABLE];
        BOOL isXTSColumnExist = [db columnExists:@"xts" inTableWithName:TMCHAT_MESSAGES_TABLE]; //for clearchat
        BOOL isDealIdColumnExist = [db columnExists:@"dealid" inTableWithName:TMCHAT_MESSAGES_TABLE];
        BOOL isDatespotIdColumnExist = [db columnExists:@"datespotid" inTableWithName:TMCHAT_MESSAGES_TABLE];
        BOOL isMetadataColumnExist = [db columnExists:@"metadata" inTableWithName:TMCHAT_MESSAGES_TABLE];
        if(!isQuizIdColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN quizid TEXT DEFAULT NULL;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coumn:quizid table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if(!isSourceColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN source INTEGER DEFAULT 0;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coulmn in table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if (!isGalleryIDColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN gallery_id TEXT DEFAULT NULL;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coumn:galleryid table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if (!isStickerIDColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN sticker_id TEXT DEFAULT NULL;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coumn:sticker_id table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if((!isQuizIdColumnExist) || (!isSourceColumnExist) || (!isGalleryIDColumnExist) || (!isStickerIDColumnExist)) {
            [self addIndexesOnDBInstance:db];
        }
        if(!isXTSColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN xts DATETIME;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new xts table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
            //copy msgts column data into xts
            query = [NSString stringWithFormat:@"update \"%@\" set xts = msgts;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in copying msgts data into xts table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if(!isDatespotIdColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN datespotid TEXT DEFAULT NULL;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new datespotid table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if(!isDealIdColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN dealid TEXT DEFAULT NULL;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coumn:dealid table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        if(!isMetadataColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN metadata BLOB DEFAULT NULL;",TMCHAT_MESSAGES_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coumn:metadata table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
    }
    
    /*
     **
     ** chatconverasationlist
     ** table will store conversation list
     **
     */
    BOOL isConversationTableExist = [db tableExists:TMCHATCONVERSATIONLIST_TABLE];
    if(!isConversationTableExist) {
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\"(\"userid\" INTEGER NOT NULL,\
                 \"matchid\" INTEGER PRIMARY KEY NOT NULL UNIQUE, \"message\" TEXT, \"msgts\" TEXT NOT NULL,\
                 \"messagesender\" INTEGER,\"messagesource\" INTEGER, \"messagestatus\" INTEGER, \"seen\" INTEGER,\
                 \"convmessagetype\" INTEGER NOT NULL,\"convlistsortts\" TEXT NOT NULL,\"convlink\" TEXT NOT NULL,\
                 \"blocked\" INTEGER,\"blockshown\" INTEGER,\"archive\" INTEGER NOT NULL DEFAULT 0,\"dealstate\" INTEGER DEFAULT 0,\
                 \"isselectmember\" INTEGER DEFAULT 0)",TMCHATCONVERSATIONLIST_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating table \"chatconverasationlist\":%@ %@",db.lastError,db.lastErrorMessage);
        }
        
        NSString *indexQuery = [NSString stringWithFormat:@"CREATE INDEX \"conv_0matchid_index\" ON %@ (\"matchid\")",TMCHATCONVERSATIONLIST_TABLE];
        BOOL isQueryExecuted = [db executeUpdate:indexQuery];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding matchid index table \"chatconverasationlist\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    else {
        BOOL isArchiveColumnExist = [db columnExists:@"archive" inTableWithName:TMCHATCONVERSATIONLIST_TABLE];
        if (!isArchiveColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN archive INTEGER NOT NULL DEFAULT 0;",TMCHATCONVERSATIONLIST_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new column \"archive\" in table \"chatconversationlist\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        BOOL isDealButtonStatusColumnExist = [db columnExists:@"dealstate" inTableWithName:TMCHATCONVERSATIONLIST_TABLE];
        if (!isDealButtonStatusColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN dealstate INTEGER DEFAULT 0;",TMCHATCONVERSATIONLIST_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new column \"dealstate\" in table \"chatconversationlist\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
        BOOL isSelectMemberColumnExist = [db columnExists:@"isselectmember" inTableWithName:TMCHATCONVERSATIONLIST_TABLE];
        if(!isSelectMemberColumnExist) {
            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN isselectmember INTEGER DEFAULT 0;",TMCHATCONVERSATIONLIST_TABLE];
            isQueryExecuted = [db executeUpdate:query];
            if(!isQueryExecuted) {
                TMLOG(@"Error in adding new coulmn:isselectmember in table \"chatconversationlist\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
    }
    
    /////Table:chatdeletedata
    BOOL isChatDeleteDataTableExist = [db tableExists:TMCHAT_DELETEDATA_TABLE];
    if(!isChatDeleteDataTableExist) {
        TMLOG(@"Chat Delete Data Table Not Exist");
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER NOT NULL, \"matchid\" INTEGER PRIMARY KEY\
                                            UNIQUE NOT NULL,\"messageid\" TEXT,\"deletets\" TEXT NOT NULL)",TMCHAT_DELETEDATA_TABLE];
        
        BOOL isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in creating table \"chatdeletedata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
        ///ad index on matchid
        NSString *indexQuery = [NSString stringWithFormat:@"CREATE INDEX \"matchid_deletedata_index\" ON %@ (\"matchid\")",TMCHAT_DELETEDATA_TABLE];
        isQueryExecuted = [db executeUpdate:indexQuery];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding matchid index table \"chatdeletedata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    /* Table:chatusermetadata
     *** table will store all chat user meta data
     */
    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER NOT NULL,\
                                        \"matchid\" INTEGER PRIMARY KEY NOT NULL UNIQUE, \"profilelink\" TEXT,\"profileimagelink\" TEXT,\
                                        \"fname\" TEXT,\"designation\" TEXT,\"city\" TEXT,\"age\" INTEGER,\"ismstm\"\
                                        INTEGER DEFAULT 0,\"doodlelink\" TEXT,\"issparkmatch\" INTEGER DEFAULT 0)",TMCHATUSER_METADATA_TABLE];
    isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
    }
    BOOL isAgeColumnExist = [db columnExists:@"age" inTableWithName:TMCHATUSER_METADATA_TABLE];
    if(!isAgeColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN age INTEGER DEFAULT 0",TMCHATUSER_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:age in table \"chatusermetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    BOOL isMsTmColumnExist = [db columnExists:@"ismstm" inTableWithName:TMCHATUSER_METADATA_TABLE];
    if(!isMsTmColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN ismstm INTEGER DEFAULT 0;",TMCHATUSER_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:ismstm in table \"chatusermetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    BOOL isDoodleLinkColumnExist = [db columnExists:@"doodlelink" inTableWithName:TMCHATUSER_METADATA_TABLE];
    if(!isDoodleLinkColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN doodlelink TEXT;",TMCHATUSER_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:doodlelink in table \"chatusermetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    BOOL isSparkMatchColumnExist = [db columnExists:@"issparkmatch" inTableWithName:TMCHATUSER_METADATA_TABLE];
    if(!isSparkMatchColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN issparkmatch INTEGER DEFAULT 0;",TMCHATUSER_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:issparkmatch in table \"chatusermetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    BOOL isDesignationColumnExist = [db columnExists:@"designation" inTableWithName:TMCHATUSER_METADATA_TABLE];
    if(!isDesignationColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN designation TEXT;",TMCHATUSER_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:designation in table \"chatusermetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    BOOL isCityColumnExist = [db columnExists:@"city" inTableWithName:TMCHATUSER_METADATA_TABLE];
    if(!isCityColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN city TEXT;",TMCHATUSER_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:city in table \"chatusermetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    /* Table:chatsentmetadata
     *** this table will store chat meta information
     */
    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER NOT NULL, \"matchid\" INTEGER PRIMARY KEY NOT NULL UNIQUE , \"lastseen_messageid\" TEXT, \"lastseen_messagets\" TEXT)",TMCHATSENT_METADATA_TABLE];
    isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating table \"chatsentmetadata\":%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    /* Table:polingconnectionmetadata
     *** this table will store polling connection meta information
     */
    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER NOT NULL, \"matchid\" INTEGER PRIMARY KEY NOT NULL UNIQUE , \"lastpolled_messageid\" TEXT NOT NULL)",TMCHAT_POLLING_CONNECTION_METADATA_TABLE];
    isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating table \"polingconnectionmetadata\":%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    /* Table:socketconnectionmetadata
     *** this table will store socket connection meta information
     */
    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER PRIMARY KEY NOT NULL UNIQUE,\
                                                                            \"lastpolled_messagets\" TEXT NOT NULL)",
                                                                            TMCHAT_SOCKET_CONNECTION_METADATA_TABLE];
    isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating table \"socketconnectionmetadata\":%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    /* Table:datespotmetadata
     *** this table will store datespot meta information
     */
    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER NOT NULL,\
                                                                            \"datespotid\" TEXT NOT NULL UNIQUE,\
                                                                            \"address\" TEXT,\"imageurl\" TEXT)",TMCHAT_DATESPOT_METADATA_TABLE];
    isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating table \"datespotmetadata\":%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    ////////
    /* Table:sparkdata
     *** this table will store spark user data
     */
    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"userid\" INTEGER NOT NULL,\"matchid\" INTEGER NOT NULL UNIQUE,\
             \"message\" TEXT,\"messageid\" INTEGER NOT NULL,\"exptime\" INTEGER,\"hash\" TEXT,\
             \"convlink\" TEXT NOT NULL,\"starttime\" TEXT,\"seen\" INTEGER,\"createdate\" TEXT,\"imageurls\" BLOB DEFAULT NULL)",TMSPARK_METADATA_TABLE];
    isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in creating table \"sparkmetadata\":%@ %@",db.lastError,db.lastErrorMessage);
    }
    BOOL selectMemberColumnExist = [db columnExists:@"selectmember" inTableWithName:TMSPARK_METADATA_TABLE];
    if(!selectMemberColumnExist) {
        query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN selectmember INTEGER DEFAULT 0;",TMSPARK_METADATA_TABLE];
        isQueryExecuted = [db executeUpdate:query];
        if(!isQueryExecuted) {
            TMLOG(@"Error in adding new coulmn:selectmember in table \"sparkmetadata\":%@ %@",db.lastError,db.lastErrorMessage);
        }
    }
    
    //c.isselectmember
    [db close];
}
-(TMConversation*)conversationFromResultSet:(FMResultSet*)resultSet {
    TMConversation *messageConv = [[TMConversation alloc] init];
    messageConv.userId = [resultSet intForColumn:@"userid"];
    messageConv.matchId = [resultSet intForColumn:@"matchid"];
    messageConv.message = [resultSet stringForColumn:@"message"];
    messageConv.timestamp = [resultSet stringForColumn:@"msgts"];
    messageConv.sender = [resultSet intForColumn:@"messagesender"];
    messageConv.source = [resultSet intForColumn:@"messagesource"];
    messageConv.status = [resultSet intForColumn:@"messagestatus"];
    messageConv.seen = [resultSet intForColumn:@"seen"];
    messageConv.conversationMessageType = [resultSet intForColumn:@"convmessagetype"];
    messageConv.conversationSortTs = [resultSet stringForColumn:@"convlistsortts"];
    messageConv.isBlocked = [resultSet intForColumn:@"blocked"];
    messageConv.isBlockedShown = [resultSet intForColumn:@"blockshown"];
    messageConv.fullConvLink = [resultSet stringForColumn:@"convlink"];
    messageConv.isSelectMember = [resultSet intForColumn:@"isselectmember"];
    return messageConv;
}
-(TMConversation*)messageConversationFromResultSet:(FMResultSet*)resultSet {
    TMConversation *messageConv = [[TMConversation alloc] init];
    messageConv.userId = [resultSet intForColumn:@"userid"];
    messageConv.matchId = [resultSet intForColumn:@"matchid"];
    messageConv.message = [resultSet stringForColumn:@"message"];
    messageConv.timestamp = [resultSet stringForColumn:@"msgts"];
    messageConv.sender = [resultSet intForColumn:@"messagesender"];
    messageConv.source = [resultSet intForColumn:@"messagesource"];
    messageConv.status = [resultSet intForColumn:@"messagestatus"];
    messageConv.seen = [resultSet intForColumn:@"seen"];
    messageConv.conversationMessageType = [resultSet intForColumn:@"convmessagetype"];
    messageConv.conversationSortTs = [resultSet stringForColumn:@"convlistsortts"];
    messageConv.isBlocked = [resultSet intForColumn:@"blocked"];
    messageConv.isBlockedShown = [resultSet intForColumn:@"blockshown"];
    
    messageConv.fullConvLink = [resultSet stringForColumn:@"convlink"];
    messageConv.profileImageURL = [resultSet stringForColumn:@"profileimagelink"];
    messageConv.profileURLString = [resultSet stringForColumn:@"profilelink"];
    messageConv.fName = [resultSet stringForColumn:@"fname"];
    messageConv.receiverAge = [resultSet intForColumn:@"age"];
    messageConv.isArchived = [resultSet intForColumn:@"archive"];
    messageConv.isMsTm = [resultSet intForColumn:@"ismstm"];
    messageConv.dealState = [resultSet intForColumn:@"dealstate"];
    messageConv.isSparkMatch = [resultSet intForColumn:@"issparkmatch"];
    messageConv.isSelectMember = [resultSet intForColumn:@"isselectmember"];
    return messageConv;
}
-(TMEventMatch*)messageShareConvFromResultSet:(FMResultSet*)resultSet {
    TMEventMatch *eventMatch = [[TMEventMatch alloc] init];
    eventMatch.user_id =  [NSString stringWithFormat:@"%d",[resultSet intForColumn:@"matchid"]];
    eventMatch.matchName = [resultSet stringForColumn:@"fname"];
    eventMatch.matchImageURL = [resultSet stringForColumn:@"profileimagelink"];
    eventMatch.profileUrlString = [resultSet stringForColumn:@"profilelink"];
    eventMatch.messageConvFullURL = [resultSet stringForColumn:@"convlink"];
    return eventMatch;
}
-(TMMessage*)messageFromResultSet:(FMResultSet*)resultSet {
    TMMessage *message = [[TMMessage alloc] init];
    message.messageId = [resultSet intForColumn:@"messageid"];
    message.senderId = [resultSet intForColumn:@"senderid"];
    message.receiverId = [resultSet intForColumn:@"receiverid"];
    message.text = [resultSet stringForColumn:@"text"];
    message.deliveryStatus = [resultSet intForColumn:@"deliverystatus"];
    message.timestamp = [resultSet stringForColumn:@"msgts"];
    message.senderType = [resultSet intForColumn:@"msgsender"];
    message.type = [resultSet intForColumn:@"msgtype"];
    message.randomId = [resultSet stringForColumn:@"randomid"];
    message.quizId = [resultSet stringForColumn:@"quizid"];
    message.source = [resultSet intForColumn:@"source"];
    [message setDatespotId:[resultSet stringForColumn:@"datespotid"]];
    [message setDealId:[resultSet stringForColumn:@"dealid"]];
    [message setMetadata:[resultSet dataForColumn:@"metadata"]];
    NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
    NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
    
    if (message.type == MESSAGETTYPE_STICKER) {
        TMSticker* sticker = [[TMSticker alloc] initWithGalleryID:galleryID stickerID:stickerID];
        if (!sticker) {
            //for backward compatibility of old sticker URL
            sticker = [[TMSticker alloc] initWithStickerURLString:message.text];
        }
        sticker.highQualityImageURLString = message.text;
        message.sticker = sticker;
    }
    
    if(message.deliveryStatus == SENDING) {
        [message registerNotificationForUid];
    }
    return message;
}
-(TMSpark*)sparkFromResultSet:(FMResultSet*)resultSet {
    TMSpark *spark = [[TMSpark alloc] init];
    spark.matchId = [resultSet intForColumn:@"matchid"];
    spark.message = [resultSet stringForColumn:@"message"];
    spark.messageId = [resultSet intForColumn:@"messageid"];
    spark.fullConversationLink = [resultSet stringForColumn:@"convlink"];
    spark.sparkHash = [resultSet stringForColumn:@"hash"];
    spark.sparkCreateTime = [resultSet stringForColumn:@"createdate"];
    spark.sparkExpiryTime = [resultSet intForColumn:@"exptime"];
    spark.sparkStartTime = [resultSet stringForColumn:@"starttime"];
    spark.isSparkSeen = [resultSet intForColumn:@"seen"];
    spark.isSelectMember = [resultSet intForColumn:@"selectmember"];
    [spark setProfileImagesURLData:[resultSet dataForColumn:@"imageurls"]];
    return spark;
}
-(TMConversation*)messageConversationFromData:(id)messageObj {
    TMConversation *message = nil;
    return message;
}
-(TMMessage*)messageFromData:(id)messageObj {
    TMMessage *message = nil;
    if([messageObj isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dict = (NSDictionary*)messageObj;
        message = [[TMMessage alloc] initWithMessageDictionary:dict];
    }
    else {
        message = (TMMessage*)messageObj;
    }
    return message;
}

-(void)updateDBVersion:(FMDatabase*)adb {
    NSInteger userVersion = [adb userVersion];
    if(userVersion != TM_DB_VERSION) {
        [adb setUserVersion:TM_DB_VERSION];
    }
}

-(void)addIndexesOnDBInstance:(FMDatabase*)adb {
    ///add index on uid, messageid
    
    ///matchid
    NSString *indexQuery = [NSString stringWithFormat:@"CREATE INDEX \"matchid_index\" ON %@ (\"matchid\")",TMCHAT_MESSAGES_TABLE];
    BOOL isQueryExecuted = [adb executeUpdate:indexQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in adding matchid index table \"chatmessages\":%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    ///uid
    indexQuery = [NSString stringWithFormat:@"CREATE INDEX \"uid_index\" ON %@ (\"uid\")",TMCHAT_MESSAGES_TABLE];
    isQueryExecuted = [adb executeUpdate:indexQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in adding uid index table \"chatmessages\":%@ %@",adb.lastError,adb.lastErrorMessage);
    }
    
    //messageid
    indexQuery = [NSString stringWithFormat:@"CREATE INDEX \"message_index\" ON %@ (\"messageid\")",TMCHAT_MESSAGES_TABLE];
    
    isQueryExecuted = [adb executeUpdate:indexQuery];
    if(!isQueryExecuted) {
        TMLOG(@"Error in adding message index table \"chatmessages\":%@ %@",adb.lastError,adb.lastErrorMessage);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - Migration

-(void)dropTable:(NSString*)tableName withDBInstance:(FMDatabase*)adb {

    NSString *query = [NSString stringWithFormat:@"DROP TABLE %@",tableName];
    BOOL isQueryExecuted = [adb executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in dropping table \"%@\":%@ %@",tableName,adb.lastError,adb.lastErrorMessage);
    }
}

-(TMMessage*)messageFromOldMessageTable:(FMResultSet*)resultSet {
    TMMessage *message = [[TMMessage alloc] init];
    message.messageId = [resultSet intForColumn:@"messageid"];
    message.senderId = [resultSet intForColumn:@"senderid"];
    message.receiverId = [resultSet intForColumn:@"receiverid"];
    message.text = [resultSet stringForColumn:@"msgtext"];
    message.timestamp = [resultSet stringForColumn:@"msgts"];
    message.senderType = [resultSet intForColumn:@"sender"];
    message.type = [resultSet intForColumn:@"msgtype"];
    message.randomId = [resultSet stringForColumn:@"randomId"];
    return message;
}

-(void)postSocketConnectionNotificationWithStatus {
    [[NSNotificationCenter defaultCenter] postNotificationName:TMNEWMESSAGE_NOTIFICATION
                                                        object:nil
                                                      userInfo:nil];
}

@end


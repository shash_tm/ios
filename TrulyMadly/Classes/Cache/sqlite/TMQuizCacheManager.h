//
//  TMQuizCacheManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 20/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"
#import "TMQuizInfo.h"

@interface TMQuizCacheManager : TMCacheManager

-(instancetype)initWithConfiguration;

- (void)configureQuizDB;
- (NSString*)fetchVersion;
- (void)updateQuizConfig:(NSString*)version;
- (void)updateQuiz:(NSArray*)quiz_data forUpdatedVersion:(NSString*) updatedQuizVersion;
-(NSArray*) getQuizForUpdatedVersion:(BOOL) isForUpdatedQuizVersion;
- (void) updateQuizWithID:(NSString*) quizID playedByWithPlayerList:(NSArray*) updatedPlayerList;
- (BOOL) hasQuizWithID:(NSString*) quizID beenTakenByUserID:(NSString*) userID;
- (BOOL) isFlareForQuizID:(NSString*) quizID userID:(NSString*) userID;
- (TMQuizInfo*) getQuizDataForID:(NSString*) quizID;
- (void) updateQuizNewStatus:(BOOL) isNewQuiz forQuizID:(NSString*) quizID;

- (NSArray*) getUnreadQuizzesForMatchID:(NSString*) matchID userID:(NSString*) userID;
- (NSString*) getMatchTimestampForMatchID:(NSString*) matchID userID:(NSString*) userID;
- (NSString*) getUserTimestampForMatchID:(NSString*) matchID userID:(NSString*) userID;
- (BOOL) removeQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID quizID:(NSString*) quizID;
- (NSArray*) getAllFlareQuizzesForMatchID:(NSString*) matchID;
- (NSArray*) getAllQuizzesPlayedByUserID:(NSString*) userID;
- (BOOL) insertQuizStatusInfoForMatchID:(NSString*) matchID userID:(NSString*) userID userTimeStamp:(NSString*) userTimestamp matchTimestamp:(NSString*) matchTimeStamp unreadQuizID:(NSArray*) unreadQuizIDArrays;
- (BOOL) removeAllQuizzesStatusForMatchID:(NSString*) matchID userID:(NSString*) userID;

@end

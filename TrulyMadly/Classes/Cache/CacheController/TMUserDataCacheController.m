//
//  TMUserDataCacheController.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMUserDataCacheController.h"
#import "TMUserDataCacheManager.h"
#import "TMUserSession.h"
#import "TMDataStore.h"

@interface TMUserDataCacheController ()

@property(nonatomic,strong)NSOperationQueue *cachingQueue;
@property(nonatomic,strong)TMUserDataCacheManager *cacheManager;

@end

@implementation TMUserDataCacheController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cachingQueue = [[NSOperationQueue alloc] init];
        self.cachingQueue.name = @"userdata_caching_queue";
        self.cachingQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}
-(void)dealloc {
    [self.cachingQueue cancelAllOperations];
    self.cachingQueue = nil;
}
-(TMUserDataCacheManager*)cacheManager {
    if(!_cacheManager) {
        _cacheManager = [[TMUserDataCacheManager alloc] init];
    }
    return _cacheManager;
}
-(BOOL)validateUserBasicDataURLString:(NSString*)userDataURLString {
    NSString *cachedURLStringKey = [self getCachedUserBasicDataURLStringKey];
    NSString *cachedURLString = [TMDataStore retrieveObjectforKey:cachedURLStringKey];
    if([cachedURLString isEqualToString:userDataURLString]) {
        return TRUE;
    }
    return FALSE;
}
-(void)cacheUserBasicsDataURLString:(NSString*)basicURLString {
    NSString *cachedURLStringKey = [self getCachedUserBasicDataURLStringKey];
    [TMDataStore setObject:basicURLString forKey:cachedURLStringKey];
}
-(NSString*)getCachedUserBasicDataURLStringKey {
    NSString* userId = [TMUserSession sharedInstance].user.userId;
    NSString *cachedURLStringKey = [NSString stringWithFormat:@"com.tm.basicdata.%@",userId];
    return cachedURLStringKey;
}
-(void)cacheUserBasicData:(NSDictionary*)userBasicData withURLString:(NSString*)basicURLString didCacheData:(void(^)(void))completionBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSArray* states = [userBasicData objectForKey:@"states"];
        NSArray *cities = [userBasicData objectForKey:@"cities"];
        NSArray* popularCities = [userBasicData objectForKey:@"popular_cities"];
        NSArray *degree = [userBasicData objectForKey:@"highest_degree"];
        
        [self.cacheManager cacheStateList:states];
        [self.cacheManager cacheCityList:cities];
        [self.cacheManager cachePopularCityList:popularCities];
        [self.cacheManager cacheDegreeList:degree];
        [self cacheUserBasicsDataURLString:basicURLString];
        
        completionBlock();
        
    }];
}

-(void)getAvailableCachedCityList:(void(^)(NSArray* popularCityList,NSArray* cityList))responseBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSArray *cachedPopulatCityList = [self.cacheManager getPopularCityList];
        NSArray *cachedCityList = [self.cacheManager getCityList];
        responseBlock(cachedPopulatCityList,cachedCityList);
    }];
}
-(void)getCachedStateList:(void(^)(NSArray* stateList))responseBlock {
    NSArray *cachedStateList = [self.cacheManager getStateList];
    responseBlock(cachedStateList);
}
-(void)getCachedPopularCityList:(void(^)(NSArray* popularCityList))responseBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSArray *cachedPopulatCityList = [self.cacheManager getPopularCityList];
        responseBlock(cachedPopulatCityList);
    }];
}
-(void)getCachedCityList:(void(^)(NSArray* cityList))responseBlock {
    NSArray *cachedCityList = [self.cacheManager getCityList];
    responseBlock(cachedCityList);
}
-(void)getCachedDegreeList:(void(^)(NSArray* degreeList))responseBlock {
    NSArray *cachedDegreeList = [self.cacheManager getDegreeList];
    responseBlock(cachedDegreeList);
}


@end

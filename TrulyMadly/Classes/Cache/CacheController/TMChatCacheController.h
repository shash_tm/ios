//
//  TMCacheController.h
//  TrulyMadly
//
//  Created by Ankit on 11/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"

//@class TMMutualMatch;
@class TMMessage;
//@class TMMessageReceiver;
@class TMChatContext;
@class TMChatUser;
@class TMChatSeenContext;
@class TMConversation;
@class TMSpark;

@protocol TMCacheControllerDelegate;

@interface TMChatCacheController : NSObject

@property(nonatomic,weak)id<TMCacheControllerDelegate> delegate;

-(void)getChatUserData:(TMChatContext*)context
                result:(void(^)(TMChatUser *chatUser, BOOL needUpdate, TMChatContext *context))resultBlock;

-(void)getLastChatFetchRequestTimestamp:(void(^)(NSString *timestamp))timestamp;

-(void)getLastFetchedMessageId:(TMChatContext*)context
                        result:(void(^)(NSString* messageId,TMChatContext* context))resultBlock;

-(void)getCachedMessagesWithChatContext:(TMChatContext*)context
                                 result:(void(^)(NSArray *result, TMChatContext *context))resultBlock;

-(void)getLastFetchedMessage:(TMChatContext*)context
                      result:(void(^)(TMMessage* message,TMChatContext* context))resultBlock;

-(void)getCachedConversationList:(void(^)(NSDictionary* coversationData))resultBlock;

-(void)getCachedUnreadConversationCount:(void(^)(NSInteger unreadConversation))resultBlock;
-(void)getCachedConversationListForShare:(void(^)(NSDictionary* coversationData))resultBlock;
-(void)getChatSentContextData:(TMChatContext*)context
                       result:(void(^)(TMChatSeenContext *seencontext, TMChatContext *context))resultBlock;

-(void)getMatchWithId:(NSInteger)userId result:(void(^)(TMChatUser* chatUser))resultBlock;
-(void)getTsForDealMessageStoredAsText:(void(^)(NSString* timestamp))resultBlock;
-(void)getTsForPhotoShareMessageStoredAsText:(void(^)(NSString* timestamp))resultBlock;
-(void)getCachedActiveSpark:(void(^)(TMSpark* spark, NSInteger cachedSparkCount))resultBlock;


-(void)cacheChatUserMetadata:(TMChatUser*)chatUser chatContext:(TMChatContext*)context;
-(void)cacheMessages:(NSArray*)messages
         withContext:(TMChatContext*)context
         fromSourece:(TMMessageSource)messageSource
  didCompleteCaching:(void(^)(NSArray *cachedMessages, TMChatContext *context))status
;
-(void)cacheNewUnreadMessages:(TMMessage*)message;
-(void)cacheOutgoingMessage:(TMMessage*)message;
-(void)cacheChatMetadata:(TMChatSeenContext*)chatContext;
-(void)cacheConversationListData:(NSArray*)conversationList;
-(void)cacheConversationListData:(NSArray*)conversationList
                     updatedData:(void(^)(NSDictionary* coversationData))resultBlock;
-(void)cacheSparkConversationData:(TMConversation*)conversation;
-(void)cacheSparkList:(NSArray*)sparkList;


-(void)updateMessage:(TMMessage*)message needToUpdateConnectionIdentifier:(BOOL)update;
-(void)updateMetaParamsForMessage:(TMMessage*)message;
-(void)deleteMessage:(TMMessage*)message;

-(void)updateSocketConnectionIdentifier:(TMMessage*)message;
-(void)updatePollingConnectionIdentifier:(TMMessage*)message;

-(void)updateSparkAsSeen:(TMChatContext*)chatContext withStartTime:(NSString*)startTime;

-(void)markSendingMessagesAsFailedForContext:(TMChatContext*)context;
-(void)markPhotoMessgaesInSendingStateAsFailed;
-(void)updateConversationAsBlockedShown:(TMChatContext*)chatContext;
-(void)markConversationAsRead:(TMChatContext*)chatContext;
-(void)markConversationAsUnRead:(TMChatContext*)chatContext;
-(void)archiveChatConversation:(TMConversation*)conversationMessage;
-(void)deleteMessageWithContext:(TMChatContext*)context tillTimestamp:(NSString *)timestamp;
-(void)deleteSparkWithChatContext:(TMChatContext*)context;
-(void)deleteAllMessageContent;
-(void)deleteConversation;
-(void)updateDeleteConversationMetadata:(TMChatContext*)context tillTimestamp:(NSString *)timestamp;
-(void)setDealState:(NSInteger)dealState withMatchId:(NSString*)matchId;
-(void)saveDoodleLink:(NSString*)doodleLink forMatchId:(NSInteger)matchId;

@end

@protocol TMCacheControllerDelegate <NSObject>

-(void)cacheController:(TMChatCacheController*)cacheController
      didChangeContent:(TMCacheResultChangeType)type;

@end

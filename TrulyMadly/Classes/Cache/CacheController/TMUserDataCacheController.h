//
//  TMUserDataCacheController.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMUserDataCacheController : NSObject

-(void)cacheUserBasicData:(NSDictionary*)userBasicData withURLString:(NSString*)basicURLString didCacheData:(void(^)(void))completionBlock;

-(BOOL)validateUserBasicDataURLString:(NSString*)userDataURLString;
-(void)getAvailableCachedCityList:(void(^)(NSArray* popularCityList,NSArray* cityList))responseBlock;
-(void)getCachedPopularCityList:(void(^)(NSArray* popularCityList))responseBlock;
-(void)getCachedCityList:(void(^)(NSArray* cityList))responseBlock;
-(void)getCachedStateList:(void(^)(NSArray* stateList))responseBlock;
-(void)getCachedDegreeList:(void(^)(NSArray* degreeList))responseBlock;

@end

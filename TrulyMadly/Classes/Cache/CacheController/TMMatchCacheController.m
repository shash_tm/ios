//
//  TMMatchCacheController.m
//  TrulyMadly
//
//  Created by Ankit on 01/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMMatchCacheController.h"
#import "TMDataStore.h"
#import "TMMatchCacheManager.h"
#import "TMUserSession.h"
#import "TMTimestampFormatter.h"
#import "TMProfile.h"


@interface TMMatchCacheController ()

@property(nonatomic,strong)NSOperationQueue *cachingQueue;
@property(nonatomic,strong)TMMatchCacheManager *cacheManager;

@end


@implementation TMMatchCacheController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.cachingQueue = [[NSOperationQueue alloc] init];
        self.cachingQueue.name = @"matchdata_caching_queue";
        self.cachingQueue.maxConcurrentOperationCount = 1;
        
        self.cacheManager = [[TMMatchCacheManager alloc] init];
    }
    return self;
}

-(void)dealloc {
    self.cachingQueue = nil;
}

//-(void)getNextProfile:(void(^)(TMProfile *profile))response {
//    [self.cachingQueue addOperationWithBlock:^{
//        TMProfile *profile = [];
//            
//            NSArray *cachedMatchData = [self.cacheManager getCachedMatchDataForUser:user.userId.integerValue];
//           
//        }
//        resultBlock(result);
//    }];
//}
-(BOOL)isCacheValid {
    NSDate *cuurentDate = [NSDate date];
    NSDate *cachedDate = [TMDataStore retrieveObjectforKey:@"cachets"];
    if(cuurentDate && cachedDate) {
        TMTimestampFormatter *dateFormatter = [TMTimestampFormatter sharedFormatter];
        NSInteger mins = [dateFormatter minsBetweenDate:cachedDate andDate:cuurentDate];
        NSInteger minsInMS = mins * 60 * 1000;
        NSTimeInterval cacheExp = [self appCacheTimeInterval];
        if(minsInMS> cacheExp) {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    return FALSE;
}

-(void)invalidateCache {
    [self.cachingQueue addOperationWithBlock:^{
        [TMDataStore removeObjectforKey:@"cachets"];
        [TMDataStore removeObjectforKey:@"matchmetadata"];
        [self.cacheManager deleteMatchData];
    }];
}

-(void)getCachedMatchResponse:(void(^)(NSMutableDictionary *cachedMatchResponse))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSMutableDictionary *result = nil;
        if([self isCacheValid]) {
            TMUser *user = [TMUserSession sharedInstance].user;
            
            NSArray *cachedMatchData = [self.cacheManager getCachedMatchDataForUser:user.userId.integerValue];
            if(cachedMatchData.count) {
                NSData *data = [TMDataStore retrieveObjectforKey:@"matchmetadata"];
                NSDictionary *matchMetdadata = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                result = [NSMutableDictionary dictionaryWithDictionary:matchMetdadata];
                [result setObject:cachedMatchData forKey:@"data"];
            }
        }
        resultBlock(result);
    }];
}

-(void)cacheMatchData:(NSArray*)matchData cacheMetaData:(NSDictionary*)metdaDataDictioanry {
    [self.cachingQueue addOperationWithBlock:^{
        NSData *metdaData = [NSKeyedArchiver archivedDataWithRootObject:metdaDataDictioanry];
        [TMDataStore setObject:metdaData forKey:@"matchmetadata"];
        NSDate *date = [NSDate date];
        [TMDataStore setObject:date forKey:@"cachets"];
        
        ///////
        TMUser *user = [TMUserSession sharedInstance].user;
        [self.cacheManager cacheMatchData:matchData forUser:user.userId.integerValue];
    }];
}

-(void)markMatchAsShown:(NSInteger)matchId {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager markMatchDataAsShown:matchId];
    }];
}

-(NSTimeInterval)appCacheTimeInterval {
    NSTimeInterval matchCacheExp = 30*60*1000;
    NSNumber *cacheExpiry = [TMDataStore retrieveObjectforKey:@"cacheexp"];
    if(cacheExpiry) {
        matchCacheExp = [cacheExpiry doubleValue];
    }
    return matchCacheExp;
}


@end

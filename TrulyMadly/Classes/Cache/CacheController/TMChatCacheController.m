//
//  TMCacheController.m
//  TrulyMadly
//
//  Created by Ankit on 11/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMChatCacheController.h"
#import "TMMessageCacheManger.h"
#import "TMMessage.h"
#import "TMChatContext.h"
#import "TMUserSession.h"
#import "TMLog.h"
#import "TMTimestampFormatter.h"
#import "TMMessageReceiver.h"
#import "TMCacheBlockOperation.h"
#import "TMChatUser.h"
#import "TMChatSeenContext.h"
#import "TMDataStore.h"
#import "TMConversation.h"
#import "TMSpark.h"


@interface TMChatCacheController ()

@property(nonatomic,strong)NSOperationQueue *cachingQueue;
@property(nonatomic,strong)TMMessageCacheManger *cacheManager;

@end


@implementation TMChatCacheController

-(instancetype)init {
    self = [super init];
    if(self) {
        self.cachingQueue = [[NSOperationQueue alloc] init];
        self.cachingQueue.name = @"chat_caching_queue";
        self.cachingQueue.maxConcurrentOperationCount = 1;
        
        self.cacheManager = [[TMMessageCacheManger alloc] init];
    }
    return self;
}

-(void)dealloc {
    self.cachingQueue = nil;
}

//-(void)migrateDB {
//    [self.cacheManager migrateDbToNewSchema];
//}
-(void)getLastChatFetchRequestTimestamp:(void(^)(NSString *timestamp))timestamp {
    [self.cachingQueue addOperationWithBlock:^{
        NSString *messageTs = [self.cacheManager getLastChatFetchTimestamp];
        messageTs = (messageTs != nil) ? messageTs : [self defaultMissedMessageTimestamp];
        timestamp(messageTs);
    }];
}
-(void)getLastFetchedMessage:(TMChatContext*)context
                        result:(void(^)(TMMessage* message,TMChatContext* context))resultBlock {
    TMLOG(@"%s",__FUNCTION__);
    NSParameterAssert(context != nil);
    if(context) {
        __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
            TMMessage *message = [self.cacheManager getLastChatFetchedMessageFromContext:bop.chatContext];
            resultBlock(message,bop.chatContext);
        }];
        [bop setContext:context];
        [self.cachingQueue addOperation:bop];
    }
}
-(void)getLastFetchedMessageId:(TMChatContext*)context
                        result:(void(^)(NSString* messageId,TMChatContext* context))resultBlock {
    TMLOG(@"%s",__FUNCTION__);
    NSParameterAssert(context != nil);
    if(context) {
        __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
            NSString *messageId = [self.cacheManager getLastMessageId:bop.chatContext.matchId];
            resultBlock(messageId,bop.chatContext);
        }];
        [bop setContext:context];
        [self.cachingQueue addOperation:bop];
    }
}
-(void)getChatUserData:(TMChatContext*)context
                result:(void(^)(TMChatUser *chatUser, BOOL needUpdate, TMChatContext *context))resultBlock {
    __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
        TMChatUser *user = [self.cacheManager getChatUserMetaData:context];
        BOOL needUpdate = [self needToUpdateChatUserData:user.cacheTs];
        resultBlock(user,needUpdate,bop.chatContext);
    }];
    [bop setContext:context];
    [self.cachingQueue addOperation:bop];
}
-(void)getChatSentContextData:(TMChatContext*)context
                result:(void(^)(TMChatSeenContext *seencontext, TMChatContext *context))resultBlock {
    __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
        TMChatSeenContext *seencontext = [self.cacheManager getCachedChatMetaData:context];
        resultBlock(seencontext,context);
    }];
    [bop setContext:context];
    [self.cachingQueue addOperation:bop];
}
-(void)getCachedMessagesWithChatContext:(TMChatContext*)context
                                 result:(void(^)(NSArray *result, TMChatContext *context))resultBlock {
    TMLOG(@"%s",__FUNCTION__);
    NSParameterAssert(context != nil);
    if(context) {
        __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
            NSArray *cachedMessages = [self.cacheManager getCachedMessages:bop.chatContext];
            resultBlock(cachedMessages,bop.chatContext);
        }];
        [bop setContext:context];
        [self.cachingQueue addOperation:bop];
    }
}
-(void)getCachedConversationList:(void(^)(NSDictionary* coversationData))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSDictionary *cachedConversationData = [self.cacheManager getCachedConversationList];
        resultBlock(cachedConversationData);
    }];
}
-(void)getCachedConversationListForShare:(void(^)(NSDictionary* coversationData))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSDictionary *cachedConversationData = [self.cacheManager getCachedConversationListForShare];
        resultBlock(cachedConversationData);
    }];
}
-(void)getCachedUnreadConversationCount:(void(^)(NSInteger unreadConversation))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSInteger unreadConversation = [self.cacheManager getUnreadConversationCount];
        NSInteger sparkCount = [self.cacheManager getSparkCount];
        resultBlock(unreadConversation+sparkCount);
    }];
}
-(void)getMatchWithId:(NSInteger)userId result:(void(^)(TMChatUser* chatUser))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        TMChatUser *chatUser = [self.cacheManager getChatUserMetaData:nil];
        resultBlock(chatUser);
    }];
}
-(void)getTsForDealMessageStoredAsText:(void(^)(NSString* timestamp))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSString *defaultTS = [self defaultDealMessagesUpdateTimestamp];
        NSString *timestamp = [self.cacheManager getDealMessageTimesampSavedAsTextMessageFromTimestamp:defaultTS];
        resultBlock(timestamp);
    }];
}
-(void)getTsForPhotoShareMessageStoredAsText:(void(^)(NSString* timestamp))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        NSString *defaultTS = [self defaultPhotoShareMessagesUpdateTimestamp];
        NSString *timestamp = [self.cacheManager getPhotoShareMessageTimesampSavedAsTextMessageFromTimestamp:defaultTS];
        resultBlock(timestamp);
    }];
}
-(void)getCachedActiveSpark:(void(^)(TMSpark* spark, NSInteger cachedSparkCount))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        TMSpark *spark = [self.cacheManager getActiveSpark];
        NSInteger totalCount = [self.cacheManager getSparkCount];
        resultBlock(spark,totalCount);
    }];
}

-(void)cacheConversationListData:(NSArray*)conversationList {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheConversationList:conversationList];
    }];
}
-(void)cacheSparkConversationData:(TMConversation*)conversation {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheSparkConversation:conversation];
    }];
}
-(void)cacheConversationListData:(NSArray*)conversationList
                  updatedData:(void(^)(NSDictionary* coversationData))resultBlock {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheConversationList:conversationList];
        NSDictionary *cachedConversationData = [self.cacheManager getCachedConversationList];
        resultBlock(cachedConversationData);
    }];
}
-(void)updateConversationAsBlockedShown:(TMChatContext*)chatContext {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager setCachedConversationAsBlockedShown:chatContext];
    }];
}
-(void)archiveChatConversation:(TMConversation*)conversationMessage {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager archiveConversation:conversationMessage];
    }];
}
-(void)markConversationAsRead:(TMChatContext*)chatContext {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager markConversationAsRead:chatContext];
    }];
}
-(void)markPhotoMessgaesInSendingStateAsFailed {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updateCachedPhotoMessgaesInSendingStateAsFailed];
    }];
}
-(void)markConversationAsUnRead:(TMChatContext*)chatContext {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager markConversationAsUnRead:chatContext];
    }];
}
-(void)setDealState:(NSInteger)dealState withMatchId:(NSString*)matchId {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheDealState:dealState withMatch:matchId];
    }];
}
-(void)cacheChatUserMetadata:(TMChatUser*)chatUser chatContext:(TMChatContext*)context {
    TMLOG(@"Log:%s",__FUNCTION__);
    //NSParameterAssert(context != nil && chatUser != nil);
    if(context && chatUser) {
        __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
            [self.cacheManager saveChatUserMetaData:chatUser chatContext:bop.chatContext];
        }];
        [bop setContext:context];
        [self.cachingQueue addOperation:bop];
    }
}
-(void)saveDoodleLink:(NSString*)doodleLink forMatchId:(NSInteger)matchId {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheDoodleLink:doodleLink withMatch:matchId];
    }];
}

//addd need to return whether inserted record is new or update
-(void)cacheMessages:(NSArray*)messages
         withContext:(TMChatContext*)context
         fromSourece:(TMMessageSource)messageSource
  didCompleteCaching:(void(^)(NSArray *cachedMessages, TMChatContext *context))status {
    
    //TMLOG(@"--- Started Saving Messages ---");
    //should return cached messages for registered match
    __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{

        //TMLOG(@"%ld",(long)bop.chatContext.matchId);
        [self.cacheManager cacheMessages:messages fromSource:messageSource];
        NSArray *cachedMessages = nil;
        if(bop.chatContext.matchId) {
            cachedMessages = [self.cacheManager getCachedMessages:bop.chatContext];
        }
    
        status(cachedMessages,bop.chatContext);
    }];
    [bop setContext:context];
    [self.cachingQueue addOperation:bop];
}
//addd need to return whether inserted record is new or update
-(void)cacheNewUnreadMessages:(TMMessage*)message {
    //TMLOG(@"--- Started Saving Messages ---");
    //should return cached messages for registered match
    __block TMCacheBlockOperation *bop = [TMCacheBlockOperation blockOperationWithBlock:^{
        
        [self.cacheManager cacheNewUnreadMessage:message];
    }];
    [self.cachingQueue addOperation:bop];
}
-(void)cacheOutgoingMessage:(TMMessage*)message {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cachedNewMessage:message];
    }];
}
-(void)cacheSparkList:(NSArray*)sparkList {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheSparkData:sparkList];
    }];
}
-(void)updateMessage:(TMMessage*)message needToUpdateConnectionIdentifier:(BOOL)update {
    TMLOG(@"---- update message -----");
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updateCachedMessage:message];
    }];
    
    //last fetch ts
    if(update && (message.type != MESSAGETTYPE_QUIZ_NUDGE_SINGLE)) {
        [self updateConnectionIdentifier:message];
    }
}
-(void)updateMetaParamsForMessage:(TMMessage*)message {
    TMLOG(@"---- update message -----");
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updateMetaParams:message];
    }];
}
-(void)deleteMessage:(TMMessage*)message {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager deleteMessage:message];
    }];
}
-(void)deleteConversation {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager deleteCachedConversation];
    }];
}
-(void)cacheChatMetadata:(TMChatSeenContext*)chatContext {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager cacheChatMetaData:chatContext];
    }];
}
-(void)deleteAllMessageContent {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager deleteAllCachedMessageContent];
    }];
}
-(void)deleteMessageWithContext:(TMChatContext*)context tillTimestamp:(NSString *)timestamp {
    NSParameterAssert(context != nil);
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updateDeleteConversationMetadata:context tillTimestamp:timestamp];
    }];
}
-(void)deleteSparkWithChatContext:(TMChatContext*)context {
    NSParameterAssert(context != nil);
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager deleteSpark:context];
    }];
}
-(void)updateDeleteConversationMetadata:(TMChatContext*)context tillTimestamp:(NSString *)timestamp {
    NSParameterAssert(context != nil);
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updateDeleteConversationMetadata:context tillTimestamp:timestamp];
    }];
}

////////////////////////////////////////////////////////////////////////////////////

-(void)updateConnectionIdentifier:(id)messageObj {
    TMMessage *message = NULL;
    if([messageObj isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dict = (NSDictionary*)messageObj;
        message = [[TMMessage alloc] initWithMessageDictionary:dict];
    }
    else {
        message = (TMMessage*)messageObj;
    }
    
    if([[TMUserSession sharedInstance] chatConnection] == CHATCONNECTION_SOCKET) {
        [self.cachingQueue addOperationWithBlock:^{
            [self.cacheManager updateSocketConnection:message];
        }];
    }
    else {
        [self.cachingQueue addOperationWithBlock:^{
            [self.cacheManager updatePolligConnection:message];
        }];
    }
}
-(void)updateSocketConnectionIdentifier:(TMMessage*)message {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updateSocketConnection:message];
    }];
}
-(void)updatePollingConnectionIdentifier:(TMMessage*)message {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager updatePolligConnection:message];
    }];
}
-(void)updateSparkAsSeen:(TMChatContext*)chatContext withStartTime:(NSString*)startTime {
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager markSparkAsSeen:chatContext withStartTime:startTime];
    }];
}
-(void)markSendingMessagesAsFailedForContext:(TMChatContext*)context {
    NSParameterAssert(context != nil);
    [self.cachingQueue addOperationWithBlock:^{
        [self.cacheManager markMessagesInSendingStateAsFailed:context];
    }];
}
////////////////////////////////////////////////////////////////////////////////////

-(BOOL)needToUpdateChatUserData:(NSString*)lastCacheTs {
    return TRUE;
}

-(NSInteger)loggedInUserId {
    NSInteger userId = [[TMUserSession sharedInstance].user.userId integerValue];
    return userId;
}
-(NSString*)defaultMissedMessageTimestamp {
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:-(20 * secondsPerDay)];
    NSString *dateString = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:date];
    return dateString;
}

-(NSString*)defaultDealMessagesUpdateTimestamp {
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:-(15 * secondsPerDay)];
    NSString *dateString = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:date];
    return dateString;
}
-(NSString*)defaultPhotoShareMessagesUpdateTimestamp {
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:-(15 * secondsPerDay)];
    NSString *dateString = [[TMTimestampFormatter sharedFormatter] absoluteTimestampForDate:date];
    return dateString;
}
@end

//
//  TMMatchCacheController.h
//  TrulyMadly
//
//  Created by Ankit on 01/03/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMMatchCacheController : NSObject

-(BOOL)isCacheValid;
-(void)invalidateCache;
-(void)getCachedMatchResponse:(void(^)(NSMutableDictionary *cachedMatchResponse))resultBlock;
-(void)markMatchAsShown:(NSInteger)matchId;
-(void)cacheMatchData:(NSArray*)matchData cacheMetaData:(NSDictionary*)metdaDataDictioanry;

@end

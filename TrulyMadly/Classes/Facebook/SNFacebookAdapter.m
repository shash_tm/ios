//
//  SNFacebookAdapter.m
//  Hello2
//
//  Created by sudha on 11/19/15.
//  Copyright © 2015 Sudha Tiwari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SNFacebookAdapter.h"
#import "SNADFacebookCustomization.h"
#import "SdkMediation.h"
#import "TMAdManager.h"

@interface SNFacebookAdapter() <MediationAdDelegate>

@property (nonatomic, strong) NSString *interstitialPlacementId;
@property (nonatomic, strong) NSString *nativePlacementId;
@property (nonatomic, strong) NSString *bannerPlacementId;
@property (nonatomic, weak) SdkMediation *sdkMediation;
@property (nonatomic, strong) NSString *nativeAdType;
@property (nonatomic, strong) FBNativeAdScrollView *adScrollView;
@property (nonatomic, strong) FBNativeAd *nativeAd;
@property (nonatomic, strong) FBMediaView *adCoverMediaView;
@property (nonatomic, strong) FBAdChoicesView *adChoicesView;
@property (nonatomic, strong) id<SNASFBNativeAdViewAttributesDelegate> nativeAdViewAttributesDelegate;
@property (nonatomic, getter = isDelegateTracker) BOOL delegateTracker;

@property (nonatomic, strong) UIImageView *adIconImageView;
@property (nonatomic, strong) UILabel *adTitleLabel;
@property (nonatomic, strong) UILabel *adBodyLabel;
@property (nonatomic, strong) UILabel *adSocialContextLabel;
@property (nonatomic, strong) UILabel *sponsoredLabel;
@property (nonatomic, strong) UILabel *socialContextLabel;
@property (nonatomic, strong) UIButton *adCallToActionButton;

@end

@implementation SNFacebookAdapter

// Facebook Interstitial Ad.
-(void)adViewDidShowFacebookInterstitialAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId {
    self.interstitialPlacementId = placementId;
    self.sdkMediation = sdkMediation;
    [self loadInterstital];
}

- (void)loadInterstital {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [FBAdSettings addTestDevice:@"54f9f32d38ec9c1eb6fdc81e7b126e84"];
        fbInterstitial = [[FBInterstitialAd alloc] initWithPlacementID:self.interstitialPlacementId];
        fbInterstitial.delegate = self;
        [fbInterstitial loadAd];
    }];
}


// Facebook BannerAd.
-(void)adViewDidShowFacebookBannerAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId {
    self.bannerPlacementId = placementId;
    self.sdkMediation = sdkMediation;
    [self showBannerAd];
}

- (void)showBannerAd {
	SNFacebookAdapter * __weak weakSelf = self;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        FBAdView *adView;
        [FBAdSettings addTestDevice:@"54f9f32d38ec9c1eb6fdc81e7b126e84"];
        
        if (weakSelf.sdkMediation.viewController) {
            adView = [[FBAdView alloc] initWithPlacementID:weakSelf.bannerPlacementId adSize:kFBAdSizeHeight50Banner rootViewController:self.sdkMediation.viewController];
        }
        adView.delegate = weakSelf;
        if (weakSelf.sdkMediation.fixedView) {
            [weakSelf.sdkMediation.fixedView addSubview:adView];
        } else {
            [weakSelf.sdkMediation.viewController.view addSubview:adView];
        }
        
        [adView loadAd];
    }];
}

// Facebook Native Ad.
-(void)adViewDidShowFacebookNativeAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId {
    self.nativePlacementId = placementId;
    self.sdkMediation = sdkMediation;
    self.nativeAdViewAttributesDelegate = [SNADFacebookCustomization new];

    if (self.sdkMediation.extraParameterArray.count > 1) {
        self.nativeAdType = [self.sdkMediation.extraParameterArray objectAtIndex:1];
    }
    if ([self.nativeAdType isEqualToString:@"scroll"]) {
        [self showScrollNativeAd];
    } else {
        [self showNativeAd];
    }
}

- (void)showNativeAd {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [FBAdSettings addTestDevice:@"54f9f32d38ec9c1eb6fdc81e7b126e84"];
        self.nativeAd = [[FBNativeAd alloc] initWithPlacementID:self.nativePlacementId];
        self.nativeAd.delegate = self;
        [self.nativeAd loadAd];
    }];
}

- (void)showScrollNativeAd {
    FBNativeAdsManager *manager = [[FBNativeAdsManager alloc] initWithPlacementID:self.nativePlacementId forNumAdsRequested:5];
    manager.delegate = self;
    [manager loadAds];
    nativeAdManager = manager;
}


#pragma mark - Scroll Native Delegate.

- (void)nativeAdsLoaded {
    
    self.sdkMediation.fixedView.superview.hidden = NO;
    self.sdkMediation.fixedView.hidden = NO;
    NSLog(@"Native ads loaded, constructing native UI...");
    
    NSString *height;
    NSInteger adType = FBNativeAdViewTypeGenericHeight100;
    if (self.sdkMediation.extraParameterArray.count > 2) {
        height = [self.sdkMediation.extraParameterArray objectAtIndex:2];
        
        if (height.integerValue == 120) {
            adType = FBNativeAdViewTypeGenericHeight120;
        } else if (height.integerValue == 300) {
            adType = FBNativeAdViewTypeGenericHeight300;
        } else if (height.integerValue == 400) {
            adType = FBNativeAdViewTypeGenericHeight400;
        }
    }
    
    if (self.adScrollView) {
        [self.adScrollView removeFromSuperview];
        self.adScrollView = nil;
    }
    
    FBNativeAdViewAttributes *nativeAdAttributes;
    if ([self.nativeAdViewAttributesDelegate respondsToSelector:@selector(nativeAdViewAttributesForAdId:)]) {
        nativeAdAttributes = [self.nativeAdViewAttributesDelegate nativeAdViewAttributesForAdId:self.sdkMediation.adId];
    }
    
    FBNativeAdScrollView *scrollView;
    if (nativeAdAttributes) {
        scrollView = [[FBNativeAdScrollView alloc] initWithNativeAdsManager:nativeAdManager withType:adType withAttributes:nativeAdAttributes];
    } else {
        scrollView = [[FBNativeAdScrollView alloc] initWithNativeAdsManager:nativeAdManager withType:adType];
    }
    scrollView.delegate = self;
    
    UIView *view;
    if (self.sdkMediation.fixedView) {
        view = self.sdkMediation.fixedView;
    } else {
        view = self.sdkMediation.viewController.view;
    }
    
    [view addSubview:scrollView];
    
    [scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *viewCT = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    
    NSLayoutConstraint *viewCB = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    NSLayoutConstraint *viewCL = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    
    NSLayoutConstraint *viewCR = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    
    [view addConstraints:@[viewCT, viewCB, viewCL, viewCR]];
    
    self.adScrollView = scrollView;
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adRecieved:@"FacebookNative"];
    
    NSInteger adHeight = 100;
    if (adType == FBNativeAdViewTypeGenericHeight120) {
        adHeight = 120;
    } else if (adType == FBNativeAdViewTypeGenericHeight300) {
        adHeight = 300;
    } else if (adType == FBNativeAdViewTypeGenericHeight400) {
        adHeight = 400;
    }
    
    [self.sdkMediation adWillPresentWithHeight:adHeight];
    [self.sdkMediation adWillPresent];
    [self.sdkMediation adDidPresent:@"FacebookNative"];
}

#pragma mark - Facebook Native Delegate.

- (void)nativeAdDidLoad:(FBNativeAd *)nativeAd {
    
    NSMutableDictionary *adData = [[NSMutableDictionary alloc] init];
    [adData setObject:nativeAd.title forKey:@"title"];
    [adData setObject:nativeAd.icon.url.absoluteString forKey:@"iconUrl"];
    [adData setObject:nativeAd.body forKey:@"description"];
    [adData setObject:[NSNumber numberWithBool:YES] forKey:@"mediationAd"];
    [adData setObject:nativeAd forKey:@"clickUrl"];
    
    [self.sdkMediation.callBackDelegate showNativeAdWithDataDictionary:adData seventyNineAd:NO];
    
//    self.sdkMediation.fixedView.superview.hidden = NO;
//    
//    UIView *view;
//    if (self.sdkMediation.fixedView) {
//        view = self.sdkMediation.fixedView;
//    } else {
//        view = self.sdkMediation.viewController.view;
//    }
//    //TODO: ImageView
//    self.adIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 60, 50)];
//    [view addSubview:self.adIconImageView];
//	
//	//TODO: TitleLabel
//    self.adTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 140, 40)];
//    self.adTitleLabel.font = [UIFont fontWithName:@"ArialMT" size:14];

//    self.adTitleLabel.adjustsFontSizeToFitWidth = YES;
//    [view addSubview:self.adTitleLabel];
    
//    self.adBodyLabel = [[UILabel alloc] init];
//    self.adBodyLabel.numberOfLines = 0;
//    [view addSubview:self.adBodyLabel];
	
//    self.socialContextLabel = [[UILabel alloc] init];
//    [view addSubview:self.socialContextLabel];
	//TODO: Ad Label
//    self.sponsoredLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 40, 30)];
//    self.sponsoredLabel.font = [UIFont fontWithName:@"ArialMT" size:10];
//    [view addSubview:self.sponsoredLabel];
//	
//	//TODO: Button
//    self.adCallToActionButton = [UIButton buttonWithType:UIButtonTypeSystem];
//    self.adCallToActionButton.frame = CGRectMake(215, 15, 100, 30);
//    self.adCallToActionButton.layer.borderColor = [UIColor grayColor].CGColor;
//    self.adCallToActionButton.layer.borderWidth = 1;
//    self.adCallToActionButton.layer.cornerRadius = 4;
//    self.adCallToActionButton.titleLabel.adjustsFontSizeToFitWidth = YES;
//    [view addSubview:self.adCallToActionButton];
//    
    // Constraints Call.
//    [self addConstraintForAdIconImageView:view];
//    [self addConstraintForAdTitleLabel:view];
//    [self addConstraintForAdBodyLabel:view];
//    [self addConstraintForAdCallToActionButton:view];
    
//    [self.adTitleLabel setText:nativeAd.title];
//    [self.adBodyLabel setText:nativeAd.body];
//    [self.socialContextLabel setText:nativeAd.socialContext];
//    [self.sponsoredLabel setText:@"Ad"];
//    [self.adCallToActionButton setTitle:nativeAd.callToAction forState:UIControlStateNormal];
//    
//    [nativeAd.icon loadImageAsyncWithBlock:^(UIImage *image) {
//        [self.adIconImageView setImage:image];
//    }];
    
//    self.adChoicesView = [[FBAdChoicesView alloc] initWithNativeAd:nativeAd];
//    [view addSubview:self.adChoicesView];
//    [self.adChoicesView updateFrameFromSuperview];
    
    
/*
    self.adCoverMediaView  = [[FBMediaView alloc] init];
    [self.adCoverMediaView setNativeAd:nativeAd];
    [view addSubview:self.adCoverMediaView];
    [self addConstraintForView:view];
    [view layoutIfNeeded];
 */
//    if (self.sdkMediation.viewController) {
//        [nativeAd registerViewForInteraction:view withViewController:self.sdkMediation.viewController];
//    }
//    
//    //Seventynine SDK Delegate.
//	[self.sdkMediation adWillPresentWithHeight:60];
//    [self.sdkMediation adRecieved:@"FacebookNative"];
//    [self.sdkMediation adWillPresent];
//    [self.sdkMediation adDidPresent:@"FacebookNative"];
}

// Constraints

- (void) addConsraintForView:(UIView *)view {
    [self.adCoverMediaView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *viewCT = [NSLayoutConstraint constraintWithItem:self.adCoverMediaView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    
    NSLayoutConstraint *viewCB = [NSLayoutConstraint constraintWithItem:self.adCoverMediaView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    NSLayoutConstraint *viewCL = [NSLayoutConstraint constraintWithItem:self.adCoverMediaView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    
    NSLayoutConstraint *viewCR = [NSLayoutConstraint constraintWithItem:self.adCoverMediaView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    
    [view addConstraints:@[viewCT, viewCB, viewCL, viewCR]];
}

- (void)addConstraintForAdIconImageView:(UIView *)view {
    [self.adIconImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *adIconImageViewCY = [NSLayoutConstraint constraintWithItem:self.adIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    
    NSLayoutConstraint *adIconImageViewCL = [NSLayoutConstraint constraintWithItem:self.adIconImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:8];
    
    NSLayoutConstraint *adIconImageViewCH = [NSLayoutConstraint constraintWithItem:self.adIconImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:0.8 constant:0];
    
    NSLayoutConstraint *adIconImageViewCW = [NSLayoutConstraint constraintWithItem:self.adIconImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:0.25 constant:0];
    
    [view addConstraints:@[adIconImageViewCL, adIconImageViewCY, adIconImageViewCH, adIconImageViewCW]];
}

- (void)addConstraintForAdTitleLabel:(UIView *)view {
    [self.adTitleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *adTitleLabelCT = [NSLayoutConstraint constraintWithItem:self.adTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.adIconImageView attribute:NSLayoutAttributeTop multiplier:0.8 constant:0];
    
    NSLayoutConstraint *adTitleLabelCL = [NSLayoutConstraint constraintWithItem:self.adTitleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.adIconImageView attribute:NSLayoutAttributeRight multiplier:1 constant:5];
    
    NSLayoutConstraint *adTitleLabelCH = [NSLayoutConstraint constraintWithItem:self.adTitleLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:0.2 constant:0];
    
    NSLayoutConstraint *adTitleLabelCW = [NSLayoutConstraint constraintWithItem:self.adTitleLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:0.4 constant:0];
    
    [view addConstraints:@[adTitleLabelCL, adTitleLabelCT, adTitleLabelCH, adTitleLabelCW]];
}

- (void)addConstraintForAdBodyLabel:(UIView *)view {
    [self.adBodyLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *adBodyLabelCT = [NSLayoutConstraint constraintWithItem:self.adBodyLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.adTitleLabel attribute:NSLayoutAttributeBottom multiplier:0.8 constant:0];
    
    NSLayoutConstraint *adBodyLabelCL = [NSLayoutConstraint constraintWithItem:self.adBodyLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.adIconImageView attribute:NSLayoutAttributeRight multiplier:1 constant:5];
    
    NSLayoutConstraint *adBodyLabelCH = [NSLayoutConstraint constraintWithItem:self.adBodyLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:0.5 constant:0];
    
    NSLayoutConstraint *adBodyLabelCW = [NSLayoutConstraint constraintWithItem:self.adBodyLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:0.4 constant:0];
    
    [view addConstraints:@[adBodyLabelCL, adBodyLabelCT, adBodyLabelCH, adBodyLabelCW]];

}

- (void)addConstraintForAdCallToActionButton:(UIView *)view {
    [self.adCallToActionButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *adCallToActionButtonCT = [NSLayoutConstraint constraintWithItem:self.adCallToActionButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.adTitleLabel attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    
    NSLayoutConstraint *adCallToActionButtonCR = [NSLayoutConstraint constraintWithItem:self.adCallToActionButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1 constant:-8];
    
    NSLayoutConstraint *adCallToActionButtonCH = [NSLayoutConstraint constraintWithItem:self.adCallToActionButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:0.5 constant:0];
    
    NSLayoutConstraint *adCallToActionButtonCL = [NSLayoutConstraint constraintWithItem:self.adCallToActionButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.adTitleLabel attribute:NSLayoutAttributeRight multiplier:1 constant:8];
    
    [view addConstraints:@[adCallToActionButtonCR, adCallToActionButtonCT, adCallToActionButtonCH, adCallToActionButtonCL]];
}

- (void)nativeAd:(FBNativeAd *)nativeAd didFailWithError:(NSError *)error {
    NSLog(@"Ad failed to load with error: %@", error);
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewDidFailWithError:@"FacebookNative"];
}

- (void)nativeAdDidFinishHandlingClick:(nonnull FBNativeAd *)nativeAd {
    //Seventynine SDK Delegate.
//    [[SdkMediation sharedInstance] adDidClick];
}

- (void)nativeAdDidClick:(nonnull FBNativeAd *)nativeAd {
    //Seventynine SDK Delegate.
    [self.sdkMediation adDidClick];
}

- (void)nativeAdsFailedToLoadWithError:(NSError *)error; {
    NSLog(@"Native ads failed to load with error: %@", error);
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewDidFailWithError:@"FacebookNative Fail To Load."];
}

- (void)nativeAdWillLogImpression:(nonnull FBNativeAd *)nativeAd {
}



#pragma mark - Facebook Interstitial Delegates

- (void)interstitialAdDidClick:(nonnull FBInterstitialAd *)interstitialAd {
    //Seventynine SDK Delegate.
    [self.sdkMediation adDidClick];
}

- (void)interstitialAdWillLogImpression:(nonnull FBInterstitialAd *)interstitialAd {
}

- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd {
    NSLog(@"Ad is loaded and ready to be displayed.");
    
    //Seventynine SDK Delegate
    [self.sdkMediation adRecieved:@"FacebookInterstitial"];
    [self.sdkMediation adWillPresent];
    
    if (self.sdkMediation.viewController) {
        [interstitialAd showAdFromRootViewController:self.sdkMediation.viewController];
    }
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adDidPresent:@"FacebookInterstitial"];
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd didFailWithError:(NSError *)error {
    NSLog(@"Ad failed to load.");
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewDidFailWithError:@"FacebookInterstitial"];
    
}

- (void)interstitialAdDidClose:(nonnull FBInterstitialAd *)interstitialAd {
    //Seventynine SDK Delegate.
    [self.sdkMediation adDIdClose];
    [self.sdkMediation adViewWillRemove];
}

- (void)interstitialAdWillClose:(nonnull FBInterstitialAd *)interstitialAd {
    //Seventynine SDK Delegate.
    [self.sdkMediation adWillClose];
}

#pragma mark - Facebook Banner Delegate.
- (void)adViewDidLoad:(nonnull FBAdView *)adView {
    self.sdkMediation.fixedView.superview.hidden = NO;
    
    //Seventynine SDK Delegate.
    if (self.delegateTracker == NO) {
        [self.sdkMediation adWillPresentWithHeight:50];
        [self.sdkMediation adWillPresent];
        [self.sdkMediation adDidPresent:@"FacebookBanner"];
        
        self.delegateTracker = YES;
    }
    [self.sdkMediation adRecieved:@"FacebookBanner"];
}

- (void)adView:(nonnull FBAdView *)adView didFailWithError:(nonnull NSError *)error {
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewDidFailWithError:@"FacebookBanner"];
}

- (void)adViewDidClick:(nonnull FBAdView *)adView {
    //Seventynine SDK Delegate.
    [self.sdkMediation adDidClick];
}

- (void)adViewDidFinishHandlingClick:(nonnull FBAdView *)adView {
    //Seventynine SDK Delegate.
    //    [[SdkMediation sharedInstance] adDidClick];
}

@end

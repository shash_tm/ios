//
//  SNFacebookAdapter.h
//  Hello2
//
//  Created by sudha on 11/19/15.
//  Copyright © 2015 Sudha Tiwari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBAudienceNetwork/FBAudienceNetwork.h>

@interface SNFacebookAdapter : NSObject <FBInterstitialAdDelegate, FBNativeAdDelegate, FBAdViewDelegate, FBNativeAdsManagerDelegate> {
    FBInterstitialAd *fbInterstitial;
    FBNativeAdsManager *nativeAdManager;
}

@end



@protocol SNASFBNativeAdViewAttributesDelegate <NSObject>

@optional

- (FBNativeAdViewAttributes *)nativeAdViewAttributesForAdId:(NSString *)adId;

@end
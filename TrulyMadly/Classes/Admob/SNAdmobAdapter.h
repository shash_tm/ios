//
//  SNAdmobAdapter.h
//  Hello2
//
//  Created by sudha on 11/18/15.
//  Copyright © 2015 Sudha Tiwari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface SNAdmobAdapter : NSObject <GADInterstitialDelegate, GADBannerViewDelegate> {
    GADInterstitial *interstitial_;
    GADBannerView *banner_;
}

@property (nonatomic, retain) GADInterstitial *interstitial;
@property (nonatomic, retain) GADBannerView *banner;

- (void)showGoogleAd;
- (void)showGoogleBannerAd;

@end

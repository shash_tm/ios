//
//  SNAdmobAdapter.m
//  Hello2
//
//  Created by sudha on 11/18/15.
//  Copyright © 2015 Sudha Tiwari. All rights reserved.
//

#import "SNAdmobAdapter.h"
#import "SdkMediation.h"

@interface SNAdmobAdapter() <MediationAdDelegate>
@property (nonatomic, strong) NSString *placementId;
@property (nonatomic, weak) SdkMediation *sdkMediation;
@property (nonatomic, getter = isDelegateTracker) BOOL delegateTracker;
@property (nonatomic) NSNumber *width;
@property (nonatomic) NSNumber *height;
@property (nonatomic, assign) int bannerHeight;
@property (nonatomic, strong) NSString *bannerType;
@end

@implementation SNAdmobAdapter

// Banner Ad.
- (void)adViewDidShowAdMobBannerAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId bannerType:(NSString *)bannerType width:(NSNumber *)width height:(NSNumber *)height {
    self.placementId = placementId;
    self.sdkMediation = sdkMediation;
    self.bannerType = bannerType;
    self.width = width;
    self.height = height;
    [self showGoogleBannerAd];
}

- (void) showGoogleBannerAd {
    
    SNAdmobAdapter * __weak weakSelf = self;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if ([self.bannerType isEqualToString:@"Standard_Banner"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            weakSelf.bannerHeight = 50;
        } else if ([self.bannerType isEqualToString:@"Large_Banner"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
            weakSelf.bannerHeight = 100;
        } else if ([self.bannerType isEqualToString:@"Medium_Rectangle"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle];
            weakSelf.bannerHeight = 250;
        } else if ([self.bannerType isEqualToString:@"Full_Size_Banner"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeFullBanner];
            weakSelf.bannerHeight = 60;
        } else if ([self.bannerType isEqualToString:@"Leaderboard"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
            weakSelf.bannerHeight = 90;
        } else if ([self.bannerType isEqualToString:@"Smart_Banner"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
            weakSelf.bannerHeight = 50;
        } else if ([self.bannerType isEqualToString:@"Custom"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(CGSizeMake(self.width.floatValue, self.height.floatValue))];
            weakSelf.bannerHeight = (int)self.height.integerValue;
        } else if ([self.bannerType isEqualToString:@"Custom_Landscape"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:GADAdSizeFullWidthLandscapeWithHeight(self.height.floatValue)];
            weakSelf.bannerHeight = (int)self.height.integerValue;
        } else if ([self.bannerType isEqualToString:@"Custom_Portrait"])
        {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:GADAdSizeFullWidthPortraitWithHeight(self.height.floatValue)];
            weakSelf.bannerHeight = (int)self.height.integerValue;
        } else {
            weakSelf.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            weakSelf.bannerHeight = 50;
        }

        weakSelf.banner.adUnitID = weakSelf.placementId;
        weakSelf.banner.delegate = weakSelf;
        weakSelf.banner.rootViewController = weakSelf.sdkMediation.viewController;
        
        if (weakSelf.sdkMediation.fixedView) {
            [weakSelf.sdkMediation.fixedView addSubview:weakSelf.banner];
        } else {
            [weakSelf.sdkMediation.viewController.view addSubview:weakSelf.banner];
        }
        
        GADRequest *request = [GADRequest request];
        request.testDevices = @[@"efbb1f1d5d5f68d372bd517af2a34416"];
        request.testDevices = @[ kGADSimulatorID ];
        [weakSelf.banner loadRequest:request];
    }];
}


// Interstitial Ad.
- (void)adViewDidShowAdMobInterstitialAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId {
    self.placementId = placementId;
    self.sdkMediation = sdkMediation;
    [self showGoogleAd];
}

- (void)showGoogleAd {
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:self.placementId] ;
    self.interstitial.delegate = self;
    [self.interstitial loadRequest:[GADRequest request]];
}

#pragma mark - AdMob Interstitial Delegate.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [self.interstitial presentFromRootViewController:self.sdkMediation.viewController];
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adRecieved:@"AdMobInterstitial"];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"AdMobInterstitial Error. %@", error);
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewDidFailWithError:@"AdMobInterstitial"];
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"AdMobInterstitial WillPresentScreen Delegate.");
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adWillPresent];
    [self.sdkMediation adDidPresent:@"AdMobInterstitial"];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
     NSLog(@"AdMobInterstitial WillDismissScreen Delegate.");
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewWillRemove];
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"AdMobInterstitial DidDismissScreen Delegate.");
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"AdMobInterstitial WillLeaveApplication Delegate.");
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adWillLeaveApplication];
}


#pragma mark - AdMob Banner Delegate.

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    self.sdkMediation.fixedView.superview.hidden = NO;
    
    [self.sdkMediation adRecieved:@"AdMobBanner"];

    //Seventynine SDK Delegate.
    if (self.delegateTracker == NO) {
        [self.sdkMediation adWillPresentWithHeight:self.bannerHeight];
        [self.sdkMediation adWillPresent];
        [self.sdkMediation adDidPresent:@"AdMobBanner"];
        
        self.delegateTracker = YES;
    }
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"AdMobBanner DidFailToReceiveAdWithError: %@", error.localizedDescription);
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adViewDidFailWithError:@"AdMobBanner"];
}

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView {
    NSLog(@"AdMobBanner WillPresentScreen Delegate.");
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"AdMobBanner WillDismissScreen Delegate.");
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"AdMobBanner DidDismissScreen Delegate.");
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView {
    NSLog(@"AdMobBanner WillLeaveApplication Delegate.");
    
    //Seventynine SDK Delegate.
    [self.sdkMediation adWillLeaveApplication];
}

@end

//
//  TMSparkPackageListView.h
//  TrulyMadly
//
//  Created by Ankit on 11/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSparkPackageListViewDelegate;

/*!
 @class TMSparkPackageListView
 @brief The TMSparkPackageListView class
 @discussion This class is designed and implemented to create custom view for TMBuySparkViewController. This view holds the header view to hold image or video asset as well as the list of spark packages with package details.
*/

@interface TMSparkPackageListView : UIView

/*! A delegate object of TMSparkPackageListViewDelegate. */
@property(nonatomic,weak)id<TMSparkPackageListViewDelegate> delegate;
/*! This property contains the details of selected package for tracking. */
@property(nonatomic, strong)NSDictionary *purchasedPackageForTracking;

-(void)loadData:(NSArray*)packageList withVideo:(NSString *)sparkVideoUrl;

@end

/*!
 @protocol TMSparkPackageListViewDelegate
 
 @discussion This protocol is implemented for handling purchase action of spark packages or display terms and conditions view.
 */
@protocol TMSparkPackageListViewDelegate <NSObject>

/*!
 @brief This delegate method is called on select of any spark package for purchase.
 
 @discussion It is implemented to handle the purchase action of the select package.
 
 @param packageIdentifier It is the identifier of the selected package.
*/
-(void)didBuySparkPackageWithIdentifier:(NSString*)packageIdentifier;

/*!
 @brief This delegate method is implemented to display terms and conditions.
 */
-(void)didSelectMatchGuranteeTNC;

@end

//
//  TMSparkTNCTableViewCell.h
//  TrulyMadly
//
//  Created by Suruchi Sinha on 12/13/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 @class TMSparkTNCTableViewCell
 @brief The TMSparkTNCTableViewCell class
 @discussion This class is designed and implemented to create a custom cells for TMBuySparkTNCViewController. This cell displays the message which describes terms and conditions.
*/

@interface TMSparkTNCTableViewCell : UITableViewCell

/*! @brief This method assigns text message to the label that describes terms and conditions.
 @param msgText Message to be displayed.
*/
- (void)configureCellWithText:(NSString *) msgText;

@end

//
//  TMSparkConversationView.h
//  TrulyMadly
//
//  Created by Ankit on 06/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMSpark;
@protocol TMSparkConversationViewDelegate;
@protocol TMSparkConversationViewDataSource;

@interface TMSparkConversationView : UIView

@property(nonatomic,assign)id<TMSparkConversationViewDelegate> delegate;
@property(nonatomic,assign)id<TMSparkConversationViewDataSource> datasource;

-(void)reloadSparkDataForTutorialView;
-(void)reloadSparkData;

@end


@protocol TMSparkConversationViewDelegate <NSObject>

-(void)didSelectSpark;

@end

@protocol TMSparkConversationViewDataSource <NSObject>

-(NSInteger)sparkRowCount;
-(NSString*)sparkProfileName;
-(NSString*)sparkProfileAge;
-(NSString*)sparkProfileDesignation;
-(NSString*)sparkMessage;
-(NSString*)sparkProfileImageURLString;
-(NSString*)sparkProgressPercent;
-(BOOL)isSelectUser;

@end

//
//  TMSparkPackageTableViewCell.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMSparkPackageInfo;

/*!
 @class TMSparkPackageTableViewCell
 @brief The TMSparkPackageTableViewCell class
 @discussion This class is designed and implemented to create custom tableview cell to display details of spark packages.
*/

@interface TMSparkPackageTableViewCell : UITableViewCell

/*!
 @brief This method configures the UI of tableview cell with the data passed as parameter.
 @param packageInfo An object of TMSparkPackageInfo containing details of the spark package.
*/
-(void)configureCellWithData:(TMSparkPackageInfo*)packageInfo;

@end

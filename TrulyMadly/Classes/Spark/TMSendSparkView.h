//
//  TMSendSparkView.h
//  TrulyMadly
//
//  Created by Ankit on 27/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSendSparkViewDelegate;

@interface TMSendSparkView : UIView

@property(nonatomic,weak)id<TMSendSparkViewDelegate> delegate;
@property(nonatomic,assign)BOOL didSwipeCommonIntroText;
@property(nonatomic,assign)BOOL didShowMessageTextAlert;
@property(nonatomic,assign)BOOL didShowDefaultCommonalityString;
@property(nonatomic,assign)BOOL didShowFromIntro;
@property(nonatomic,assign)NSInteger messageLength;

-(void)showSendMessageDialog;
-(void)deinit;
-(void)setSendingState;
-(void)resetToNormalState;
-(void)showCommonalityString:(NSArray*)commons;

@end

@protocol TMSendSparkViewDelegate <NSObject>

-(void)didRemoveSendSparkView;
-(void)sendSparkActionWithMessage:(NSString*)sparkMessage;

@end

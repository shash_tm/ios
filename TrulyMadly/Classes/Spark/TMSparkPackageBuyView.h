//
//  TMSparkPackageBuyView.h
//  TrulyMadly
//
//  Created by Ankit on 27/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSparkPackageBuyViewDelegate;

@interface TMSparkPackageBuyView : UIView

@property(nonatomic,weak)id<TMSparkPackageBuyViewDelegate> delegate;
@property(nonatomic,assign)BOOL didShowFromSideMenu;
@property(nonatomic,assign)BOOL didShowFromIntro;

//for tracking purpose
@property(nonatomic,strong)NSString* source;
@property(nonatomic,strong)NSString* action;
@property(nonatomic,strong)NSString* eventName;

-(void)showSparkNowAlertViewWithImageURLStrings:(NSArray*)imageURLStrings;
-(void)showHasLikedBeforeAlertWithImageURLStrings:(NSArray*)imageURLStrings;
-(void)showSparkPaymentCompletionWithNewSparkCount:(NSInteger)sparkCount show:(BOOL)relationshipExpertIcon;
-(void)showSelectActionNudgeAlertWithActionText:(NSString*)actionText withImageURLStrings:(NSArray*)imageURLStrings;
-(void)showSelectLongTermNudgeAlertWithTitleText:(NSString*)titleText;
-(void)showSparkTutorialView;
-(void)showLastSparkAlertState;
-(void)deinit;
-(NSDictionary*)getTrackingInfoDictionary;
-(void)showNRIProfileNudgeAlertwithText;
@end

@protocol TMSparkPackageBuyViewDelegate <NSObject>

@optional
-(void)didRemoveSparkView;
-(void)didTakeLikeNowActionOnHasLikedSparkAlert;
-(void)didClickSparkTutorialKnowMoreButton;
-(void)didClickSparkNowButton;
-(void)didClickSparkNowButtonFromAlert;
-(void)didClickSparkNowMayBeLaterButton;
-(void)didClickSparkPackageBuySuccessContinueButton;
-(void)didClickLastSparkAlertBuyButton;
-(void)didClickJoinSelectButton;
-(void)didCancelJoinSelectNudge;
-(void)didCancelShowNRIProfileNudge;
-(void)didEditPreferenceForNRIProfile;

@end

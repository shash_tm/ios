//
//  TMSparkTNCTableViewCell.m
//  TrulyMadly
//
//  Created by Suruchi Sinha on 12/13/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkTNCTableViewCell.h"
#import "NSString+TMAdditions.h"

@interface  TMSparkTNCTableViewCell()

/*! This property holds the tick image. */
@property(nonatomic,strong) UIImageView *sparkTick;
/*! This property displays the message. */
@property(nonatomic,strong) UILabel *msgLabel;

@end

@implementation TMSparkTNCTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (UILabel *)msgLabel {
    if(! _msgLabel) {
        _msgLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _msgLabel.font = [self msgLabelFont];
        _msgLabel.textColor = [UIColor blackColor];
        _msgLabel.numberOfLines = 0;
        [self.contentView addSubview:_msgLabel];
    }
    return _msgLabel;
}

- (UIImageView *)sparkTick {
    if(! _sparkTick) {
        _sparkTick = [[UIImageView alloc] initWithFrame:CGRectZero];
        _sparkTick.contentMode = UIViewContentModeTop;
        _sparkTick.image = [UIImage imageNamed:@"sparktnctick"];
        [self.contentView addSubview:_sparkTick];
    }
    return _sparkTick;
}

-(UIFont*)msgLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:18];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect textRect;
    CGSize constrintSize;
    CGFloat xPos, width, height;
    NSDictionary *attributes;
    
    self.sparkTick.frame = CGRectMake(16, 0, 18, 18);
    [self.contentView addSubview:self.sparkTick];
    if(self.msgLabel.text) {
        
        xPos = self.sparkTick.frame.origin.x + self.sparkTick.frame.size.width + 8;
        width = self.contentView.bounds.size.width - xPos - 16;
        height = self.msgLabel.frame.size.height;
        attributes = @{NSFontAttributeName:[self msgLabelFont]};
        constrintSize = CGSizeMake(width, 200);
        textRect = [self.msgLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        self.msgLabel.frame = CGRectMake(xPos, self.sparkTick.frame.origin.y, CGRectGetWidth(textRect), CGRectGetHeight(textRect));
    }
    else {
        self.msgLabel.frame = CGRectZero;
    }
}

- (void)configureCellWithText:(NSString *) msgText {
    self.msgLabel.text = msgText;
    
}



@end

//
//  TMSParkCollectionViewStackLayout.h
//  TrulyMadly
//
//  Created by Ankit on 09/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSParkCollectionViewStackLayoutDataSource;


@interface TMSParkCollectionViewStackLayout : UICollectionViewFlowLayout

@property (nonatomic, weak) id<TMSParkCollectionViewStackLayoutDataSource> dataSource;

@end


@protocol TMSParkCollectionViewStackLayoutDataSource <NSObject>

-(CGSize)contentSizeForIndex:(NSInteger)index;
-(CGSize)headerSizeForSection:(NSInteger)section;

@end
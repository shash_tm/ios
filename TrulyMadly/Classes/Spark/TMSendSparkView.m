//
//  TMSendSparkView.m
//  TrulyMadly
//
//  Created by Ankit on 27/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSendSparkView.h"
#import "TMWindowView.h"
#import "NSString+TMAdditions.h"
#import "UIColor+TMColorAdditions.h"
#import "TMActivityIndicatorView.h"

#define disableTintColor [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:0.5]
#define enableTintColor [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:1.0]
#define SPARKTEXTVIEW_PLACEHOLDERTEXT @"Write a message and increase your chances of a Like back."


@interface TMSendSparkView ()<TMWindowViewDelegate,UIScrollViewDelegate,UITextViewDelegate>

@property(nonatomic, strong) TMActivityIndicatorView* activityIndicatorView;
@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) TMWindowView* windowView;
@property(nonatomic, strong) UIView *alertView;
@property(nonatomic, strong) UITextView *sparkMessageTextView;
@property(nonatomic, strong) UIButton *leftButton;
@property(nonatomic, strong) UIButton *rightButton;
@property(nonatomic, strong) NSString *sparkMessage;
@property(nonatomic, strong) NSArray *commonProp;
@property(nonatomic, assign) NSInteger currentIndex;
@property(nonatomic, assign) CGFloat initYPos;
@property(nonatomic, assign) BOOL isRequestInProgress;

@end

@implementation TMSendSparkView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius  = 10;
        
        [self registerForKeyBoardNotifications];
    }
    return self;
}

-(NSInteger)messageLength {
    return self.sparkMessageTextView.text.length;
}

#pragma mark - Public methods
#pragma mark -

-(void)deinit {
   [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(self.alertView) {
        [self.alertView removeFromSuperview];
        self.alertView = nil;
    }
    if(self.activityIndicatorView) {
        [self.activityIndicatorView stopAnimating];
        [self.activityIndicatorView removeFromSuperview];
        self.activityIndicatorView = nil;
    }
    if(self.superview) {
        [self removeFromSuperview];
    }
    
    [self.windowView deinit];
    self.windowView = nil;
}

-(void)showSendMessageDialog {
    CGFloat xPos = 10;
    CGFloat width = CGRectGetWidth(self.windowView.frame) - 2*xPos;
    CGFloat height =   320;
    self.frame = CGRectMake(xPos, (CGRectGetHeight(self.windowView.frame)-height)/2, width, height);
    
    if(self.activityIndicatorView) {
        [self.activityIndicatorView removeFromSuperview];
        self.activityIndicatorView = nil;
    }
    
    self.activityIndicatorView.frame = CGRectMake((CGRectGetWidth(self.frame) - CGRectGetWidth(self.activityIndicatorView.frame))/2,
                                                  70,
                                                  CGRectGetWidth(self.activityIndicatorView.frame),
                                                  CGRectGetHeight(self.activityIndicatorView.frame));
    [self.activityIndicatorView startAnimating];

    
    [self initSendSparkDialog];
    
    [self.windowView addSubview:self];
}

#pragma mark - getter methods
#pragma mark -

-(TMActivityIndicatorView*)activityIndicatorView {
    if(!_activityIndicatorView) {
        _activityIndicatorView = [[TMActivityIndicatorView alloc] initWithFrame:CGRectZero];
        _activityIndicatorView.titleText = nil;
    }
    if(!_activityIndicatorView.superview) {
        [self addSubview:_activityIndicatorView];
    }
    return _activityIndicatorView;
}

-(TMWindowView*)windowView {
    if(!_windowView) {
        _windowView = [[TMWindowView alloc] init];
        _windowView.delegate = self;
    }
    return _windowView;
}

-(void)initSendSparkDialog {
    self.initYPos = CGRectGetMinY(self.frame);
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat scrollHeight = 80;
    
    UILabel *titleHdr = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 44)];
    titleHdr.text = @"Write a Message!";
    titleHdr.textColor = [UIColor likeColor];
    titleHdr.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0];
    titleHdr.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleHdr];
    
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(titleHdr.frame), width-30, 0.5)];
    horizontalLine.backgroundColor = [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:1.0];
    [self addSubview:horizontalLine];
    
    // scroll view for common properties height 80
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(35, CGRectGetMaxY(horizontalLine.frame), width-70, scrollHeight)];
    self.scrollView.pagingEnabled = false;
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.pagingEnabled = TRUE;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.scrollView];
    
    
    // add left button
    UIImage *leftbtnImage = [UIImage imageNamed:@"left_arrow"];
    self.leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.leftButton.frame = CGRectMake(0, CGRectGetMaxY(horizontalLine.frame)+(scrollHeight-44)/2, 44, 44);
    [self.leftButton setImage:leftbtnImage forState:UIControlStateNormal];
    self.leftButton.hidden =  TRUE;
    self.leftButton.tintColor =  enableTintColor;
    [self.leftButton addTarget:self action:@selector(didLeftArrowClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.leftButton];
    
    UIImage *rightbtnImage = [UIImage imageNamed:@"right_arrow"];
    self.rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.rightButton.frame = CGRectMake(width-44, CGRectGetMaxY(horizontalLine.frame)+(scrollHeight-44)/2, 44, 44);
    [self.rightButton setImage:rightbtnImage forState:UIControlStateNormal];
    self.rightButton.hidden = TRUE;
    self.rightButton.tintColor = enableTintColor;
    [self.rightButton addTarget:self action:@selector(didRightArrowClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.rightButton];
    
    UIView *horizontalLine1 = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(titleHdr.frame)+scrollHeight, width-30, 0.5)];
    horizontalLine1.backgroundColor = [UIColor colorWithRed:255/255.0f green:183/255.0f blue:0/255.0f alpha:1.0];
    [self addSubview:horizontalLine1];
    
    self.sparkMessageTextView = [[UITextView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(horizontalLine1.frame)+10, width-30, 110)];
    self.sparkMessageTextView.text = SPARKTEXTVIEW_PLACEHOLDERTEXT;
    self.sparkMessageTextView.backgroundColor = [UIColor colorWithRed:238/255.0f green:238/255.0f blue:238/255.0f alpha:1.0];
    self.sparkMessageTextView.layer.borderWidth = 0.5;
    self.sparkMessageTextView.layer.borderColor = [UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:1.0].CGColor;
    self.sparkMessageTextView.layer.cornerRadius = 5;
    self.sparkMessageTextView.delegate = self;
    self.sparkMessageTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    [self addSubview:self.sparkMessageTextView];
    [self setTextViewDefaultState];
    
    UIButton *sparkButton = [UIButton buttonWithType:UIButtonTypeSystem];
    sparkButton.frame = CGRectMake(15, CGRectGetMaxY(self.sparkMessageTextView.frame)+15, width-30, 44);
    sparkButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [sparkButton setTitle:@"Send Spark" forState:UIControlStateNormal];
    sparkButton.layer.cornerRadius = 6;
    sparkButton.backgroundColor = [UIColor likeColor];
    [sparkButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sparkButton addTarget:self action:@selector(didSparkButtonClick) forControlEvents:UIControlEventTouchUpInside];
    sparkButton.tag = 110390;
    [self addSubview:sparkButton];
}

-(void)showCommonalityString:(NSArray*)commons {
//    NSString *str = @"<p margin=0, padding=0>\
//    <font color=\"#ffb700\">Ask him if he watches </font>\
//    <font color=\"#46A3C5\">Nargis Fakhri</font>\
//    <font color=\"#ffb700\"> on repeat?</font>\
//    </p>";
//    commons = @[str];
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView = nil;
    
    if(!self.commonProp) {
        self.commonProp = commons;
        if(commons.count == 1) {
            self.leftButton.hidden = TRUE;
            self.rightButton.hidden = TRUE;
        }
        else {
            self.leftButton.hidden = TRUE;
            self.rightButton.hidden = FALSE;
        }
        CGFloat xPos  = 0;
        CGFloat yPos  = 0;
        CGFloat width = CGRectGetWidth(self.scrollView.frame);
        CGFloat height = CGRectGetHeight(self.scrollView.frame);
        CGFloat contentWidth = CGRectGetWidth(self.scrollView.frame);
        for(int i=0; i<commons.count;i++){
            UIView* view = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, height)];
            
            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            textLabel.backgroundColor = [UIColor clearColor];
            textLabel.textColor = [UIColor colorWithRed:255.0f/255.0f green:183.0f/255.0f blue:0/255.0f alpha:1];
            
            NSString *text = commons[i];
            //text = [NSString stringWithFormat:@"<p margin=0, padding=0> %@ </p>",text];
            //NSError *err;
//            NSAttributedString *attText = [[NSAttributedString alloc] initWithData:[text dataUsingEncoding:NSUTF8StringEncoding]
//                                                                           options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
//                                                                                     NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)
//                                                                                     }
//                                                                documentAttributes:nil error:&err];
//            textLabel.attributedText = attText;
//            if(err) {
//                NSLog(@"Error:%@",err);
//            }
            textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
            textLabel.text = text;
            textLabel.numberOfLines = 0;
            textLabel.textAlignment = NSTextAlignmentCenter;
            textLabel.frame = view.bounds;
            [view addSubview:textLabel];
            [self.scrollView addSubview:view];
            
            xPos = xPos + width;
            contentWidth = CGRectGetMaxX(view.frame);
        }
        
        self.scrollView.contentSize = CGSizeMake(contentWidth, height);
    }
}

-(void)showAlertView {
    
    if(!self.alertView) {
        self.didShowMessageTextAlert = TRUE;
        
        CGFloat height = self.frame.size.height;
        CGFloat width = self.frame.size.width;
        CGFloat iconWidth = 60;
        CGFloat bgViewHeight = 0;
        
        self.alertView = [[UIView alloc] initWithFrame:self.window.bounds];
        self.alertView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.72];
        [self addSubview:self.alertView];
        
        // add icon
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake((width-iconWidth)/2, (height-bgViewHeight)/2, iconWidth, iconWidth)];
        iconView.image = [UIImage imageNamed:@"popUpSpark"];
        [self.alertView addSubview:iconView];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(30, (CGRectGetMaxY(iconView.frame))-iconWidth/2, width-60, bgViewHeight)];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.cornerRadius = 8;
        [self.alertView addSubview:bgView];
        
        [self.alertView bringSubviewToFront:iconView];
        
        // add message and button to alert view
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
        NSDictionary *attributes = @{NSFontAttributeName:font};
        CGSize constrintSize = CGSizeMake(bgView.frame.size.width-30, NSUIntegerMax);
        NSString *text = @"Sure you want to send a Spark without a message?";
        CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        width = bgView.frame.size.width;
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((width-rect.size.width)/2, (iconWidth/2)+10, rect.size.width, rect.size.height)];
        lbl.text = text;
        lbl.textColor = [UIColor colorWithRed:115/255.0f green:115/255.0f blue:115/255.0f alpha:1.0];
        lbl.font = font;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.numberOfLines = 0;
        [bgView addSubview:lbl];
        
        // horizonatal line
        UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lbl.frame)+10, width, 0.5)];
        horizontalLine.backgroundColor = [UIColor colorWithRed:226/255.0f green:226/255.0f blue:226/255.0f alpha:1.0];
        [bgView addSubview:horizontalLine];
        
        UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake((width-0.5)/2, CGRectGetMaxY(horizontalLine.frame), 0.5, 44)];
        verticalLine.backgroundColor = [UIColor colorWithRed:226/255.0f green:226/255.0f blue:226/255.0f alpha:1.0];
        [bgView addSubview:verticalLine];
        
        UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(horizontalLine.frame), (width-0.5)/2, 44)];
        [sendButton setTitleColor:[UIColor likeColor] forState:UIControlStateNormal];
        [sendButton setTitle:@"Send Spark" forState:UIControlStateNormal];
        sendButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
        sendButton.backgroundColor = [UIColor clearColor];
        [sendButton addTarget:self action:@selector(didSparkButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:sendButton];
        
        UIButton *writeMessage = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verticalLine.frame), CGRectGetMaxY(horizontalLine.frame), (width-0.5)/2, 44)];
        [writeMessage setTitleColor:[UIColor likeColor] forState:UIControlStateNormal];
        [writeMessage setTitle:@"Write Message" forState:UIControlStateNormal];
        writeMessage.titleLabel.font = [UIFont systemFontOfSize:15.0];
        writeMessage.backgroundColor = [UIColor clearColor];
        [writeMessage addTarget:self action:@selector(writeMessageClick) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:writeMessage];
        
        bgViewHeight = bgViewHeight + CGRectGetMaxY(writeMessage.frame);
        iconView.frame = CGRectMake(iconView.frame.origin.x, (height-bgViewHeight)/2, iconWidth, iconWidth);
        bgView.frame = CGRectMake(30, (CGRectGetMaxY(iconView.frame))-iconWidth/2, width, bgViewHeight);
    }
}

-(void)setSendingState {
    self.isRequestInProgress = TRUE;
    UIButton *button = (UIButton*)[self viewWithTag:110390];
    [button setTitle:@"Sending..." forState:UIControlStateNormal];
    [self.sparkMessageTextView resignFirstResponder];
}

-(void)didSparkButtonClick {
    if(!self.isRequestInProgress) {
        if(self.alertView.superview){
            [self removeAlertViewFromView];
            [self sendSparkToDelegate];
        }else {
            if(self.sparkMessage){
                [self sendSparkToDelegate];
            }else {
                [self showAlertView];
            }
        }
    }
}
-(void)sendSparkToDelegate {
    [self.delegate sendSparkActionWithMessage:self.sparkMessage];
}
-(void)writeMessageClick {
    [self removeAlertViewFromView];
    [self.sparkMessageTextView becomeFirstResponder];
}

-(void)removeAlertViewFromView {
    if(self.alertView){
        [self.alertView removeFromSuperview];
        self.alertView = nil;
    }
}
-(void)resetToNormalState {
    self.isRequestInProgress = FALSE;
    UIButton *button = (UIButton*)[self viewWithTag:110390];
    [button setTitle:@"Send Spark" forState:UIControlStateNormal];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSInteger characterCount = textView.text.length + (text.length - range.length);
    if(characterCount < 140 ) {
        return TRUE;
    }
    return FALSE;
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textViewc {
    if([self.sparkMessageTextView.text isEqualToString:SPARKTEXTVIEW_PLACEHOLDERTEXT]) {
        self.sparkMessageTextView.text = @"";
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    self.sparkMessage = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(self.sparkMessageTextView.text.length == 0){
        [self setTextViewDefaultState];
        self.sparkMessageTextView.text = SPARKTEXTVIEW_PLACEHOLDERTEXT;
        [self.sparkMessageTextView resignFirstResponder];
    }
    else {
        [self setTextViewTypingState];
    }
}


#pragma mark - TMWindowView delegate methods
#pragma mark -

-(void)didDissmissWindowView {
    if(!self.isRequestInProgress) {
        [self.delegate didRemoveSendSparkView];
    }
}

#pragma mark - Keyboard handling methods

-(void)registerForKeyBoardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
-(void)keyboardWillBeShown:(NSNotification *)sender {
    CGSize keyboardSize = [[[sender userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.frame;
        CGFloat newHeight = self.windowView.frame.size.height - keyboardSize.height;
        f.origin.y = (newHeight-f.size.height);
        self.frame = f;
    }];
}
-(void)keyboardWillBeHidden:(NSNotification *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.frame;
        f.origin.y = self.initYPos;
        self.frame = f;
    }];
}

#pragma mark scroll view delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.didSwipeCommonIntroText = TRUE;
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    self.currentIndex = lround(fractionalPage);
    
    if(self.currentIndex == 0) {
        self.leftButton.hidden = TRUE;
    }else {
        self.leftButton.hidden = FALSE;
    }
    
    if(self.currentIndex == self.commonProp.count-1){
        self.rightButton.hidden = TRUE;
    }else {
        self.rightButton.hidden = FALSE;
    }
}

-(void)didLeftArrowClick {
    if(self.currentIndex > 0){
        self.didSwipeCommonIntroText = TRUE;
        CGFloat xPos = self.scrollView.frame.size.width*(self.currentIndex-1);
        [UIScrollView beginAnimations:@"scrollAnimation" context:nil];
        [UIScrollView setAnimationDuration:1.0f];
        [self.scrollView setContentOffset:CGPointMake(xPos, self.scrollView.bounds.origin.y)];
        [UIScrollView commitAnimations];
    }
}

-(void)didRightArrowClick {
    if(self.currentIndex < self.commonProp.count-1) {
        self.didSwipeCommonIntroText = TRUE;
        CGFloat xPos = self.scrollView.frame.size.width*(self.currentIndex+1);
        [UIScrollView beginAnimations:@"scrollAnimation" context:nil];
        [UIScrollView setAnimationDuration:1.0f];
        [self.scrollView setContentOffset:CGPointMake(xPos, self.scrollView.bounds.origin.y)];
        [UIScrollView commitAnimations];
    }
}
-(void)setTextViewDefaultState {
    self.sparkMessageTextView.textColor = [UIColor colorWithRed:64/255.0f green:64/255.0f blue:64/255.0f alpha:1.0];
    self.sparkMessageTextView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
}
-(void)setTextViewTypingState {
    self.sparkMessageTextView.textColor = [UIColor colorWithRed:64/255.0f green:64/255.0f blue:64/255.0f alpha:1.0];
    self.sparkMessageTextView.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
}

@end

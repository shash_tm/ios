//
//  TMBuySparkHeaderView.m
//  TrulyMadly
//
//  Created by Suruchi Sinha on 1/18/17.
//  Copyright © 2017 trulymadly. All rights reserved.
//


#import "TMBuySparkHeaderView.h"
#import <AVFoundation/AVFoundation.h>

@interface TMBuySparkHeaderView()

/*! An AVPlayer object used as video player. */
@property(nonatomic, strong)AVPlayer *player;
/*! An AVPlayerLayer object the holds the video player object. */
@property(nonatomic, strong)AVPlayerLayer *playerLayer;
/*! This property holds the image/video to be displayed. */
@property(nonatomic, strong)UIImageView *imageView;
/*! This property keeps the current status of the video player. */
@property(nonatomic, assign)BOOL isVideoPlaying;
/*! This property keeps the video url. */
@property(nonatomic, strong)NSString *urlString;
@end

@implementation TMBuySparkHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}

- (void)loadDataOnView:(NSString *)imageName withVideoUrl:(NSString *) videoUrl{

    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.imageView.image = [UIImage imageNamed:imageName];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.backgroundColor = [UIColor clearColor];
        
    ///Adds play icon and creates video player with the given url if video url is not nil.
    if(videoUrl != nil) {
        
        UIImageView *playIconView;
        UITapGestureRecognizer *tapGesture;
        NSURL *url;
        AVPlayerItem *playerItem;
        CGFloat xPos, yPos, iconWidth;
        
        iconWidth = 70;
        xPos = (self.frame.size.width - iconWidth)/2;
        yPos = (self.frame.size.height - iconWidth)/2;
        playIconView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, iconWidth, iconWidth)];
        playIconView.image = [UIImage imageNamed:@"playIcon"];
        playIconView.contentMode = UIViewContentModeScaleAspectFit;
        playIconView.backgroundColor = [UIColor clearColor];
        self.isVideoPlaying = NO;
        self.urlString = videoUrl;
        
        url = [NSURL URLWithString:self.urlString];
        playerItem = [AVPlayerItem playerItemWithURL:url];
        self.player= [[AVPlayer alloc] initWithPlayerItem:playerItem];
        [self.imageView addSubview:playIconView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerOutOfFocus) name:@"stopPlayer" object:nil];
        [self.player addObserver:self forKeyPath:@"rate" options:0 context:nil];
        tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoTapAction)];
        [self addGestureRecognizer:tapGesture];
    }
    [self addSubview:self.imageView];
}

/*!
 @brief This method handles the tap action on spark video view. It plays the video and stops the active video player accordingly.
*/
- (void)videoTapAction {
    
    if(!self.isVideoPlaying) {
        if([self.imageView superview]) {
            [self.imageView removeFromSuperview];
        }
        if(self.playerLayer == nil) {
            self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
            self.playerLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
            self.playerLayer.backgroundColor = [UIColor clearColor].CGColor;
            self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
            [self.layer addSublayer:self.playerLayer];
        }
        self.isVideoPlaying = YES;
        [self.player play];
    }else {
        self.isVideoPlaying = NO;
        [self.player pause];
    }
}

/*! @brief It pauses the active player when spark video is out of focus. */
- (void)playerOutOfFocus {
    
    if([self.player rate]) {
        [self.player pause];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    ///Incase the player stops, placeholder image is added to the view.
    if ([keyPath isEqualToString:@"rate"]) {
        if (![self.player rate]) {
            [self addSubview:self.imageView];
        }else {
            if(self.imageView.superview) {
                [self.player pause];
            }
        }
    }
}

#pragma mark - AVPlayerItem delegate methods
#pragma mark -

/*! @discussion This method is called when AVPlayerItem has finished playing the video. It resets the player to start and removes the player layer from the view. Also, it adds the placeholder imageView to the given view.
 */
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    [self.player seekToTime:kCMTimeZero];
    self.isVideoPlaying = NO;
    [self.playerLayer removeFromSuperlayer];
    self.playerLayer = nil;
    [self addSubview:self.imageView];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.player removeObserver:self forKeyPath:@"rate" context:nil];
    
    if([self.imageView superview]) {
        [self.imageView removeFromSuperview];
    }
    self.imageView = nil;
    if([self.player rate]) {
        [self.player pause];
    }
    if([self.playerLayer superlayer]){
        [self.playerLayer removeFromSuperlayer];
        self.player = nil;
        self.playerLayer = nil;
    }
}

@end

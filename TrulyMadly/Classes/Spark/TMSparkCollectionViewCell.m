//
//  TMSparkCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 09/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkCollectionViewCell.h"
#import "UIColor+TMColorAdditions.h"
#import "UIImageView+AFNetworking.h"
#import "TMSparkTimeProgressView.h"
#import "NSString+TMAdditions.h"
#import "TMDataStore.h"
#import "TMUserSession.h"


@interface TMSparkCollectionViewCell ()

@property(nonatomic,strong)TMSparkTimeProgressView *progressiveView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *designationLabel;
@property(nonatomic,strong)UILabel *messageLabel;
@property(nonatomic,strong)UIView *seperatorView;
@property(nonatomic,strong)UIView *timerView;
@property(nonatomic,strong)UIImageView* selectTag;

@property(nonatomic,assign)NSInteger maxAllowedMessageLabelHeight;
@property(nonatomic,assign)NSInteger maxAllowedLabelWidth;

@end


@implementation TMSparkCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor colorWithRed:66/255.0f green:199/255.0f blue:233/255.0f alpha:1.0].CGColor;
        self.layer.borderWidth = 1.0f;
        
        CGFloat progressViewHeight = CGRectGetHeight(self.frame) - 20;
        self.progressiveView = [[TMSparkTimeProgressView alloc] initWithFrame:CGRectMake(10, 10, progressViewHeight, progressViewHeight)];
        self.progressiveView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.progressiveView];
        
        CGFloat xOffsetBetweenImageAndText = 20;
        CGFloat titleLableInitXPos = CGRectGetMaxX(self.progressiveView.frame) + xOffsetBetweenImageAndText;
        CGFloat titleLabelYPos = 15;
        CGFloat titleLabelHeight = 20;
        self.maxAllowedLabelWidth = CGRectGetWidth(self.bounds) - ((titleLableInitXPos) + 10);
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLableInitXPos,
                                                                    titleLabelYPos,
                                                                    self.maxAllowedLabelWidth,
                                                                    titleLabelHeight)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
        self.titleLabel.textColor = [UIColor sparkCollectionViewCellTileColor];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.titleLabel];
        
        CGFloat yOffsetForNextComponent = 5;
        CGFloat designationHeight = 20;
        self.designationLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLableInitXPos,
                                                                          CGRectGetMaxY(self.titleLabel.frame) + yOffsetForNextComponent,
                                                                          self.maxAllowedLabelWidth,
                                                                          designationHeight)];
        self.designationLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15];
        self.designationLabel.backgroundColor = [UIColor clearColor];
        self.designationLabel.textColor = [UIColor sparkCollectionViewCellTileColor];
        [self addSubview:self.designationLabel];
        
        CGFloat seperatorViewHeight = 1;
        self.seperatorView = [[UIView alloc] initWithFrame:CGRectMake(titleLableInitXPos,
                                                                      CGRectGetMaxY(self.designationLabel.frame) + 8,
                                                                      self.maxAllowedLabelWidth,
                                                                      seperatorViewHeight)];
        self.seperatorView.backgroundColor = [UIColor sparkCollectionViewCellSeperatorColor];
        [self addSubview:self.seperatorView];
        
        //////
        self.maxAllowedMessageLabelHeight = 40;
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLableInitXPos,
                                                                      CGRectGetMaxY(self.seperatorView.frame) + 8,
                                                                      self.maxAllowedLabelWidth,
                                                                      self.maxAllowedMessageLabelHeight)];
        self.messageLabel.font = [UIFont systemFontOfSize:15];
        self.messageLabel.backgroundColor = [UIColor clearColor];
        self.messageLabel.numberOfLines = 2;
        self.messageLabel.textColor = [UIColor sparkCollectionViewCellMessageColor];
        [self addSubview:self.messageLabel];
    }
    return self;
}

-(UIView*)timerView {
    if(!_timerView) {
        _timerView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _timerView;
}
-(UIImageView*)selectTag {
    if(!_selectTag) {
        _selectTag = [[UIImageView alloc] initWithFrame:CGRectZero];
        _selectTag.image = [UIImage imageNamed:@"tmselectlabel"];
    }
    return _selectTag;
}
-(void)setupTutorialView {
    self.seperatorView.hidden = TRUE;
    self.titleLabel.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame)-10,
                                       (CGRectGetHeight(self.frame)-60)/2,
                                       CGRectGetWidth(self.titleLabel.frame)+20,
                                       60);
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    self.titleLabel.text = @"Spark someone and find yourself on top of their conversation’s list.";
    [self.progressiveView setupTutorialImage];
}
-(void)setupTimerView {
    if(self.timerView) {
        CGFloat timerViewHeight = 22;
        CGFloat timerViewWidth = 70;
        self.timerView.frame = CGRectMake((CGRectGetMaxY(self.progressiveView.frame) - timerViewWidth)/2,
                                          CGRectGetMaxY(self.progressiveView.frame)-18,
                                          timerViewWidth,
                                          timerViewHeight);
        self.timerView.backgroundColor = [UIColor grayColor];
        self.timerView.layer.cornerRadius = 5;
        [self addSubview:self.timerView];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 18, 18)];
        imgView.image = [UIImage imageNamed:@"timer"];
        [self.timerView addSubview:imgView];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)+5,
                                                                       0,
                                                                       300,
                                                                       CGRectGetHeight(_timerView.frame))];
        textLabel.tag = 5001;
        textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
        textLabel.textColor = [UIColor whiteColor];
        textLabel.backgroundColor = [UIColor clearColor];
        [self.timerView addSubview:textLabel];
    }
}
+ (NSString *)cellReuseIdentifier {
    return NSStringFromClass([self class]);
}

-(void)setName:(NSString*)name age:(NSString*)age designation:(NSString*)designation isSelectUser:(BOOL)selectUser {
    self.titleLabel.text = [NSString stringWithFormat:@"%@, %@",name,age];
    self.designationLabel.text = designation;
    BOOL isSelectEnabled = [[TMUserSession sharedInstance] isSelectEnabled];
    if(selectUser && isSelectEnabled) {
        CGFloat selectTagWidth = 48;
        CGFloat selectTagHeight = 16;
        self.titleLabel.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame),
                                           CGRectGetMinY(self.titleLabel.frame),
                                           CGRectGetWidth(self.titleLabel.frame) - 50,
                                           CGRectGetHeight(self.titleLabel.frame));
        
        NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
        CGSize constrintSize = CGSizeMake(CGRectGetWidth(self.titleLabel.frame), CGRectGetHeight(self.titleLabel.frame));
        CGRect rect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        [self addSubview:self.selectTag];
        self.selectTag.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame) + CGRectGetWidth(rect) + 10,
                                          CGRectGetMidY(self.titleLabel.frame) - (selectTagHeight/2),
                                          selectTagWidth, selectTagHeight);
    }
}

-(void)setMessage:(NSString*)message {
    self.messageLabel.text = nil;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGSize constrintSize = CGSizeMake(self.maxAllowedLabelWidth, self.maxAllowedMessageLabelHeight);
    CGRect rect = [message boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    self.messageLabel.text = message;
    self.messageLabel.frame = CGRectMake(CGRectGetMinX(self.messageLabel.frame),
                                         CGRectGetMinY(self.messageLabel.frame),
                                         CGRectGetWidth(rect),
                                         CGRectGetHeight(rect));
}

-(void)setProfileImage:(NSString*)profileImageURLString {
    [self.progressiveView setImageFromURLString:profileImageURLString];
}

-(void)updateProgressViewWithPercentage:(NSString*)progressPercent {
    UILabel *textLabel = [self.timerView viewWithTag:5001];
    textLabel.text = [NSString stringWithFormat:@"%@",progressPercent];
    
    BOOL showAnimation = TRUE;//[TMDataStore retrieveBoolforKey:@"spark_convlist_animation"];
    if(showAnimation) {
        [UIView animateWithDuration:0.4
                              delay: 0.5
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             CGFloat timerViewWidth = 300;
                             self.timerView.frame = CGRectMake(CGRectGetMinX(self.timerView.frame),
                                                               CGRectGetMinY(self.timerView.frame),
                                                               timerViewWidth,
                                                               CGRectGetHeight(self.timerView.frame));
                         }
                         completion:^(BOOL finished){
                             
                             UILabel *textLabel = [self.timerView viewWithTag:5001];
                             textLabel.text = [NSString stringWithFormat:@"%@ left to respond!",progressPercent];
                             [UIView animateWithDuration:0.4
                                                   delay:3
                                                 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^{
                                                  
                                                  CGFloat timerViewWidth = 70;
                                                  self.timerView.frame = CGRectMake(CGRectGetMinX(self.timerView.frame),
                                                                                    CGRectGetMinY(self.timerView.frame),
                                                                                    timerViewWidth,
                                                                                    CGRectGetHeight(self.timerView.frame));
                                              }
                                              completion:^(BOOL finished){
                                                  
                                                  UILabel *textLabel = [self.timerView viewWithTag:5001];
                                                  textLabel.text = [NSString stringWithFormat:@"%@",progressPercent];
                                              }];
                         }];
    }
}

@end



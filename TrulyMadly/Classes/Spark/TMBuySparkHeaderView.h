//
//  TMBuySparkHeaderView.h
//  TrulyMadly
//
//  Created by Suruchi Sinha on 1/18/17.
//  Copyright © 2017 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 @class TMBuySparkHeaderView
 @brief The TMBuySparkHeaderView class
 @discussion This class is designed and implemented to create custom header view for TMBuySparkViewController. This view holds the image to be displayed or the video player to play the video with given url.
 
*/

@interface TMBuySparkHeaderView : UIView

/*!
 @brief It loads the data passed as parameters on the spark header view.
 
 @discussion This method loads the image on the spark header view with the image name passed as parameter. In case the video url passed is not nil, the video player is initialized with the given url.
 
 @param  imageName Name of the image that needs to be loaded on the header view.
 @param  videoUrl  Url of the video to be played.
*/

- (void)loadDataOnView:(NSString *)imageName withVideoUrl:(NSString *) videoUrl;

@end

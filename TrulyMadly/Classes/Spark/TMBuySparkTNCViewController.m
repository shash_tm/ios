//
//  TMBuySparkTNCViewController.m
//  TrulyMadly
//
//  Created by Ankit on 17/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBuySparkTNCViewController.h"
#import "TMSparkTNCTableViewCell.h"
#import "UIColor+TMColorAdditions.h"
#import "NSString+TMAdditions.h"

@interface TMBuySparkTNCViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView* tncTableView;
@property(nonatomic,strong)NSArray* tncs;

@end

@implementation TMBuySparkTNCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    [self initDefaults];
    [self setupBgView];
    
    [self setupTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupBgView {
    UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.85;
    [self.view addSubview:bgView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [bgView addGestureRecognizer:tapGesture];
}
-(void)initDefaults {
    self.tncs = @[@"Your money will be refunded if you do not get 1 match after using all your Sparks of the specified pack.",
                      @"This guarantee is not applicable when a user is blocked.",
                      @"Changes suggested by Relationship Expert need to be incorporated in the profile.",
                      @"This guarantee is applicable after 5 days from the last Spark consumption."];
    
}
-(UILabel*)headerTextLabel {
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tncTableView.frame), 80)];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    headerLabel.textColor = [UIColor grayColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.text = @"Terms and Conditions";
    return headerLabel;
}
-(UIButton*)footerTextLabel {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, CGRectGetWidth(self.tncTableView.frame), 80);
    [button setTitle:@"OK" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor likeColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    
    CALayer *TopBorder = [CALayer layer];
    TopBorder.frame = CGRectMake(0.0f, 0.0f, button.frame.size.width, 1.5f);
    TopBorder.backgroundColor = [UIColor grayColor].CGColor;
    [button.layer addSublayer:TopBorder];
    
   return button;
}
-(void)setupTableView {
    CGFloat xPos = 20;
    CGFloat yPos = CGRectGetHeight(self.view.frame) / 5;
    self.tncTableView = [[UITableView alloc] initWithFrame:CGRectMake(xPos,
                                                                      yPos,
                                                                      CGRectGetWidth(self.view.frame) - (2*xPos),
                                                                      CGRectGetHeight(self.view.frame) - (2*yPos))
                                                              style:UITableViewStylePlain];
    
    self.tncTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tncTableView.backgroundColor = [UIColor whiteColor];
    self.tncTableView.dataSource = self;
    self.tncTableView.delegate = self;
    self.tncTableView.tableFooterView = [self footerTextLabel];
    self.tncTableView.tableHeaderView = [self headerTextLabel];
    [self.view addSubview:self.tncTableView];
}

-(void)tapAction {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
    
#pragma mark - UITableview datasource / delegate methods
#pragma mark -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tncs.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = [self getHeightForCell:[self.tncs objectAtIndex:indexPath.row]];
    return cellHeight;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TMSparkTNCTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uiCell"];
    if(!cell) {
        cell = [[TMSparkTNCTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"uiCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell configureCellWithText:self.tncs[indexPath.row]];
    return cell;
    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uiCell"];
//    if(!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"uiCell"];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    
//    cell.imageView.contentMode = UIViewContentModeTop;
//    cell.imageView.image = [UIImage imageNamed:@"sparktnctick"];
//    cell.textLabel.text = self.tncs[indexPath.row];
//    cell.textLabel.numberOfLines = 0;
//    return cell;
}

- (CGFloat)getHeightForCell:(NSString *)cellText {
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:18]};
    CGSize constraintSize = CGSizeMake(self.tncTableView.bounds.size.width - 58, 200);
    CGRect textFrame = [cellText boundingRectWithConstraintSize:constraintSize attributeDictionary:attributes];
    CGFloat cellHeight = textFrame.size.height + 16;
    return  cellHeight;
}


@end


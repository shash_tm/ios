//
//  TMSparkTimeProgressView.h
//  TrulyMadly
//
//  Created by Ankit on 16/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMProfileImageView;

@interface TMSparkTimeProgressView : UIView

@property(nonatomic,strong)TMProfileImageView *profileImgView;

-(void)setImageFromURLString:(NSString*)imageURLString;
-(void)setupTutorialImage;

@end

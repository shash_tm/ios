//
//  TMSparkTimeProgressView.m
//  TrulyMadly
//
//  Created by Ankit on 16/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkTimeProgressView.h"
#import "TMProfileImageView.h"
#import "UIColor+TMColorAdditions.h"
#import "TMNotificationHeaders.h"


@interface TMSparkTimeProgressView ()

@end

@implementation TMSparkTimeProgressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGFloat xPos = 8;
        CGFloat width =  CGRectGetWidth(self.frame) - (2 * xPos);
        self.profileImgView = [[TMProfileImageView alloc] initWithFrame:CGRectMake(xPos,
                                                                                   xPos,
                                                                                   width,
                                                                                   width)];
        self.profileImgView.layer.cornerRadius = width/2;
        self.profileImgView.backgroundColor = [UIColor clearColor];
        self.profileImgView.clipsToBounds = TRUE;
        [self.profileImgView setImageContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.profileImgView];
        
        CGFloat sparkIconWidth = 22;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxY(self.profileImgView.frame) - (sparkIconWidth/2),
                                                                               (CGRectGetHeight(frame) - sparkIconWidth)/2,
                                                                               sparkIconWidth,
                                                                               sparkIconWidth)];
        imageView.tag = 5001;
        imageView.image = [UIImage imageNamed:@"sparkconversation"];
        [self addSubview:imageView];
    }
    return self;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)setupTutorialImage {
    UIImageView *imageView = (UIImageView*)[self viewWithTag:5001];
    imageView.hidden = TRUE;
    UIImage *image = [UIImage imageNamed:@"dummyImage"];
    [self.profileImgView setDefaultImage:image];
}
-(void)setImageFromURLString:(NSString*)imageURLString {
    UIImageView *imageView = (UIImageView*)[self viewWithTag:5001];
    imageView.hidden = FALSE;
    NSString *lastPathComponent = [imageURLString lastPathComponent];
    [self.profileImgView setDefaultImage];
    [self.profileImgView setImageFromURLString:imageURLString];
    [self.profileImgView forceStartImageDownloadWithRelativePath:lastPathComponent showLoader:FALSE];
}


@end

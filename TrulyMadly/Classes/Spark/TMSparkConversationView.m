//
//  TMSparkConversationView.m
//  TrulyMadly
//
//  Created by Ankit on 06/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkConversationView.h"
#import "TMSParkCollectionViewStackLayout.h"
#import "TMSparkCollectionViewCell.h"
#import "TMSparkCollectionViewHeader.h"
#import "UIColor+TMColorAdditions.h"
#import "NSMutableArray+SWUtilityButtons.h"
#import "TMChatUser.h"


typedef enum {
    UnMatch,
    Archive,
    Share
}RightOptionType;

@interface TMSparkRightOptionView : UIView

@property(nonatomic,assign)RightOptionType rightOptionType;

+(UIView*)viewForRightOptionType:(RightOptionType)rightOptionType;

@end

@implementation TMSparkRightOptionView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
    }
    return self;
}
-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = CGRectGetWidth(self.frame);
    
    //////
    UILabel *label = (UILabel*)[self viewWithTag:1500];
    CGFloat labelXPos = 0;
    CGFloat labelYPos = 50;
    CGFloat labelHeight = 20;
    label.frame = CGRectMake(labelXPos, labelYPos, width, labelHeight);
    
    /////
    UIImageView *imageView = (UIImageView*)[self viewWithTag:1501];
    CGFloat imageWidth;
    CGFloat imageHeight;
    CGFloat imageYPos = 18;
    if(self.rightOptionType == UnMatch) {
        imageWidth = 30;
        imageHeight = 26;
        imageView.frame = CGRectMake((width-imageWidth)/2, imageYPos, imageWidth, imageHeight);
    }
    else if (self.rightOptionType == Archive) {
        imageWidth = 28;
        imageHeight = 24;
        imageView.frame = CGRectMake((width-imageWidth)/2, imageYPos, imageWidth, imageHeight);
    }
    else if (self.rightOptionType == Share) {
        imageWidth = 20;
        imageHeight = 24;
        imageView.frame = CGRectMake((width-imageWidth)/2, imageYPos, imageWidth, imageHeight);
    }
}
+(UIView*)viewForRightOptionType:(RightOptionType)rightOptionType {
    TMSparkRightOptionView *view = [[TMSparkRightOptionView alloc] initWithFrame:CGRectZero];
    view.rightOptionType = rightOptionType;
    view.backgroundColor = [UIColor likeselectedColor];
    view.layer.borderColor = [UIColor whiteColor].CGColor;
    view.layer.borderWidth = 0.5;
    
    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectZero];
    imgview.backgroundColor = [UIColor clearColor];
    imgview.tag = 1501;
    [view addSubview:imgview];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 1500;
    [view addSubview:label];
    
    if(rightOptionType == UnMatch) {
        imgview.image = [UIImage imageNamed:@"Unmatch"];
        label.text = @"Unmatch";
    }
    else if (rightOptionType == Archive) {
        imgview.image = [UIImage imageNamed:@"Archive"];
        label.text = @"Archive";
    }
    else if (rightOptionType == Share) {
        imgview.image = [UIImage imageNamed:@"ShareProfile"];
        label.text = @"Share";
    }
    return view;
}

@end


@interface TMSparkConversationView ()<UICollectionViewDelegate,UICollectionViewDataSource,TMSParkCollectionViewStackLayoutDataSource>

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,assign)NSInteger rowCount;
@property(nonatomic,assign)BOOL isTutorialView;

@end


@implementation TMSparkConversationView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupCollectionView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
-(void)setupCollectionView {
    TMSParkCollectionViewStackLayout *flowLayout = [[TMSParkCollectionViewStackLayout alloc] init];
    flowLayout.dataSource = self;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.scrollEnabled = FALSE;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.collectionView];
    
    [self.collectionView registerClass:[TMSparkCollectionViewCell class]
            forCellWithReuseIdentifier:[TMSparkCollectionViewCell cellReuseIdentifier]];
    [self.collectionView registerClass:[TMSparkCollectionViewHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
}


#pragma mark - Handlers
#pragma mark -

-(void)reloadSparkDataForTutorialView {
    self.rowCount = 1;
    self.isTutorialView = TRUE;
    [self.collectionView reloadData];
}
-(void)reloadSparkData {
    NSInteger rows = [self.datasource sparkRowCount];
    self.rowCount = ([self.datasource sparkRowCount] >= 3) ? 3 : rows;
    [self.collectionView reloadData];
}
-(void)tapAction:(UITapGestureRecognizer*)tapGesture {
    [self.delegate didSelectSpark];
}


#pragma mark - Collection view data source
#pragma mark -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.rowCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TMSparkCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TMSparkCollectionViewCell cellReuseIdentifier] forIndexPath:indexPath];
    if(indexPath.row == 0) {
        if(!self.isTutorialView) {
            [cell setName:[self.datasource sparkProfileName]
                      age:[self.datasource sparkProfileAge]
              designation:[self.datasource sparkProfileDesignation]
             isSelectUser:[self.datasource isSelectUser]];
            
            [cell setMessage:[self.datasource sparkMessage]];
            
            [cell setProfileImage:[self.datasource sparkProfileImageURLString]];
            [cell setupTimerView];
            [cell updateProgressViewWithPercentage:[self.datasource sparkProgressPercent]];
        }
        else {
            [cell setupTutorialView];
        }
    }
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    TMSparkCollectionViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        return headerView;
}

-(NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButton:[TMSparkRightOptionView viewForRightOptionType:Archive]];
    [rightUtilityButtons sw_addUtilityButton:[TMSparkRightOptionView viewForRightOptionType:UnMatch]];
    
    return rightUtilityButtons;
}

#pragma mark - Collection view delegate flow layout
#pragma mark -

-(CGSize)contentSizeForIndex:(NSInteger)index {
    CGSize size = CGSizeMake(CGRectGetWidth(self.bounds), 130);
    return size;
}

-(CGSize)headerSizeForSection:(NSInteger)section {
    CGSize size = CGSizeMake(CGRectGetWidth(self.bounds), 36);
    return size;
}

@end

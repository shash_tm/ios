//
//  TMSparkPackageTableViewCell.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 06/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkPackageTableViewCell.h"
#import "UIColor+TMColorAdditions.h"
#import "TMSparkPackageInfo.h"
#import "NSString+TMAdditions.h"

@interface TMSparkPackageTableViewCell()

/*! This property holds the description icon. */
@property(nonatomic,strong)UIImageView *descriptionIcon;
/*! This property holds the info icon on tag view. */
@property(nonatomic,strong)UIImageView *infoIcon;
/*! This property holds the description icon. */
@property(nonatomic,strong)UILabel *titleLabel;
/*! This property holds the description label. */
@property(nonatomic,strong)UILabel *descriptionLabel;
/*! This property holds the tag label for match guarantee. */
@property(nonatomic,strong)UILabel *tagLabel;
@property(nonatomic,strong)UILabel *infoLabel;
/*! This property holds the price label. */
@property(nonatomic,strong)UILabel *priceLabel;
/*! A view object that holds the tag label and info icon. */
@property(nonatomic,strong)UIView *tagView;
@end

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)

@implementation TMSparkPackageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}
-(UILabel*)titleLabel {
    if(!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.font = [self titleLabelFont];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.numberOfLines = 2;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}
-(UILabel*)descriptionLabel {
    if(!_descriptionLabel) {
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _descriptionLabel.font = [self descriptionLabelFont];
        _descriptionLabel.textColor = [UIColor blackColor];
        _descriptionLabel.numberOfLines = 2;
        [self.contentView addSubview:_descriptionLabel];
    }
    return _descriptionLabel;
}
-(UIImageView*)descriptionIcon {
    if(!_descriptionIcon) {
        _descriptionIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _descriptionIcon.image = [UIImage imageNamed:@"buysparkdescription"];
        [self.contentView addSubview:_descriptionIcon];
    }
    return _descriptionIcon;
}

-(UIImageView*)infoIcon {
    if(!_infoIcon) {
        _infoIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _infoIcon.image = [UIImage imageNamed:@"sparkInfoIcon"];
        _infoIcon.contentMode = UIViewContentModeCenter;
        [self.tagView addSubview:_infoIcon];
    }
    return _infoIcon;
}

-(UILabel*)tagLabel {
    if(!_tagLabel) {
        _tagLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        _tagLabel.clipsToBounds = TRUE;
        _tagLabel.font = [self tagLabelFont];
        [self.tagView addSubview:_tagLabel];
    }
    return _tagLabel;
}

-(UILabel*)infoLabel {
    if(!_infoLabel) {
        _infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _infoLabel.textAlignment = NSTextAlignmentCenter;
        _infoLabel.clipsToBounds = TRUE;
        _infoLabel.font = [self infoLabelFont];
        _infoLabel.layer.cornerRadius = 10;
        [self.contentView addSubview:_infoLabel];
    }
    return _infoLabel;
}

-(UIView*)tagView {
    if(!_tagView) {
        _tagView = [[UIView alloc] initWithFrame:CGRectZero];
        _tagView.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:163.0F/255.0f blue:18.0F/255.0F alpha:1];
        _tagView.layer.cornerRadius = 10;
        [self.contentView addSubview:_tagView];
    }
    return _tagView;
}
-(UILabel*)priceLabel {
    if(!_priceLabel) {
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.clipsToBounds = TRUE;
        _priceLabel.textColor = [UIColor whiteColor];
        _priceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        _priceLabel.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:0.0F/255.0f blue:115.0F/255.0F alpha:1];
        _priceLabel.layer.cornerRadius = 8;
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}
-(UIFont*)titleLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
}
-(UIFont*)descriptionLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:10];
}

-(UIFont*)infoLabelFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:10];
}

-(UIFont*)tagLabelFont {
    CGFloat size = 10.0;
    if(fabs(SCREEN_WIDTH) < 375) {
        size = 8.0;
    }
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat contentPerLineHeight = 20;
    CGFloat totalHeight = contentPerLineHeight;
    
    BOOL isDescriptionAvailable = (self.descriptionLabel.text) ? TRUE : FALSE;
    if(isDescriptionAvailable) {
        totalHeight = (contentPerLineHeight * 2) + 10;
    }
    
    ///title label
    NSDictionary *attributes = @{NSFontAttributeName:[self titleLabelFont]};
    CGSize constrintSize = CGSizeMake(200, 20);
    CGRect titleTextRect = [self.titleLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    CGFloat yPos = (CGRectGetHeight(self.frame) - totalHeight)/2;
    self.titleLabel.frame = CGRectMake(10, yPos, CGRectGetWidth(titleTextRect),20);
    
    ///price label
    CGFloat priceLabelHeight = 40;
    CGFloat priceLabelWidth = 80;
    self.priceLabel.frame = CGRectMake(CGRectGetWidth(self.frame) - (priceLabelWidth+8),
                                   (CGRectGetHeight(self.frame) - priceLabelHeight)/2,
                                   priceLabelWidth,
                                   priceLabelHeight);
    
    
    
    ///update description state if available
    if(isDescriptionAvailable) {
        self.descriptionIcon.frame = CGRectMake(10, CGRectGetMaxY(self.titleLabel.frame)+10, 16, 15);
        
        CGFloat descriptionLabelXPos  = CGRectGetMaxX(self.descriptionIcon.frame)+5;
        CGFloat maxDescriptionLabelWidth = CGRectGetMinX(self.priceLabel.frame) - descriptionLabelXPos;
        CGFloat yPos;
        NSDictionary *attributes = @{NSFontAttributeName:[self descriptionLabelFont]};
        CGSize constrintSize = CGSizeMake(maxDescriptionLabelWidth, 40);
        CGRect rect = [self.descriptionLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        yPos = CGRectGetMaxY(self.titleLabel.frame)+10 + (CGRectGetHeight(self.descriptionIcon.frame) - CGRectGetHeight(rect))/2;
        self.descriptionLabel.frame = CGRectMake(descriptionLabelXPos,
                                                 yPos,
                                                 CGRectGetWidth(rect), CGRectGetHeight(rect));
    }
    else {
        self.descriptionIcon.frame = CGRectZero;
        self.descriptionLabel.frame = CGRectZero;
    }
    
    ///tag labelframe
    if(self.tagLabel.text) {
        NSDictionary *attributes = @{NSFontAttributeName:[self tagLabelFont]};
        CGSize constrintSize = CGSizeMake(150, 20);
        CGFloat tagLabelExtraXPosSpace = 15;
        CGFloat yPos, tagViewWidth, xPos, imageHeight;
        CGRect titleLabelFrame;
        CGRect rect = [self.tagLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        yPos = (20 - CGRectGetHeight(rect))/2;
        imageHeight = 14;
        tagViewWidth = CGRectGetWidth(rect)+14+tagLabelExtraXPosSpace;
        xPos = (tagViewWidth -CGRectGetWidth(rect)-14)/2;
        self.tagLabel.frame = CGRectMake(xPos,
                                         yPos,
                                         CGRectGetWidth(rect),
                                         CGRectGetHeight(rect));
        yPos = (20 - imageHeight)/2;
        self.infoIcon.frame = CGRectMake(CGRectGetMaxX(self.tagLabel.frame), yPos, imageHeight, imageHeight);
        self.tagView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+5,
                                        CGRectGetMinY(self.priceLabel.frame),
                                        tagViewWidth,
                                        20);
        titleLabelFrame = self.titleLabel.frame;
        titleLabelFrame.origin.y = CGRectGetMinY(self.titleLabel.frame)+(20 - CGRectGetHeight(rect))/2;
        self.titleLabel.frame = titleLabelFrame;
    }
    else {
        self.tagLabel.frame = CGRectZero;
    }
    
    ///info Label frame
    if(self.infoLabel.text) {
        NSDictionary *attributes = @{NSFontAttributeName:[self infoLabelFont]};
        CGSize constrintSize = CGSizeMake(150, 20);
        CGFloat tagLabelExtraXPosSpace = 15;
        CGRect rect = [self.infoLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        self.infoLabel.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+5,
                                          CGRectGetMinY(self.priceLabel.frame),
                                          CGRectGetWidth(rect)+tagLabelExtraXPosSpace,
                                          20);
        CGRect titleLabelFrame = self.titleLabel.frame;
        titleLabelFrame.origin.y = CGRectGetMinY(self.titleLabel.frame)+(20 - CGRectGetHeight(rect))/2;
        self.titleLabel.frame = titleLabelFrame;
    }else {
        self.infoLabel.frame = CGRectZero;
    }
    
}

-(void)configureCellWithData:(TMSparkPackageInfo*)packageInfo {
    
    self.titleLabel.attributedText = [self getAttributedTitleTextFromText:packageInfo.packageTitle];
    self.descriptionLabel.text = packageInfo.packageDescription;
    
    ///update tag state if available
    if(packageInfo.tagType == TMBUYPACK_TAG_DISCOUNT) {
        self.infoLabel.text = packageInfo.packageTag;
        self.infoLabel.textColor = [UIColor colorWithRed:220.0f/255.0f green:163.0F/255.0f blue:18.0F/255.0F alpha:1];
        self.infoLabel.backgroundColor = [UIColor whiteColor];
        self.infoLabel.layer.borderColor = [UIColor colorWithRed:220.0f/255.0f green:163.0F/255.0f blue:18.0F/255.0F alpha:1].CGColor;
        self.infoLabel.layer.borderWidth = 1;
    }
    else if(packageInfo.tagType == TMBUYPACK_TAG_INFO) {
        self.infoLabel.text = packageInfo.packageTag;
        self.infoLabel.textColor = [UIColor whiteColor];
        self.infoLabel.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:163.0F/255.0f blue:18.0F/255.0F alpha:1];
        self.infoLabel.layer.borderColor = [UIColor clearColor].CGColor;
        self.infoLabel.layer.borderWidth = 0;
    }
    else if(packageInfo.tagType == TMBUYPACK_TAG_MATCHGURANTEE) {
        self.tagLabel.text = packageInfo.packageTag;
        self.tagLabel.textColor = [UIColor whiteColor];
        //self.tagLabel.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:163.0F/255.0f blue:18.0F/255.0F alpha:1];
        //self.tagLabel.layer.borderColor = [UIColor clearColor].CGColor;
        //self.tagLabel.layer.borderWidth = 0;
    }
    else {
        self.tagLabel.frame = CGRectZero;
    }
    
    ///update price
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@", packageInfo.currencyType, packageInfo.packagePrice];
}

/*!
 @brief This method creates an attributed string for a string passed as parameter.
 @param text A string object for which attributed string needs to be created.
 @return attributed string.
*/
-(NSAttributedString*)getAttributedTitleTextFromText:(NSString*)text {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
    NSDictionary *atts = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]};
    NSRange range = NSMakeRange(4, 2);
    [attString setAttributes:atts range:range];
    return attString;
}

@end

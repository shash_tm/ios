//
//  TMBuySparkViewController.m
//  TrulyMadly
//
//  Created by Ankit on 10/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBuySparkViewController.h"
#import "TMActivityIndicatorView.h"
#import "TMSparkPackageListView.h"
#import "TMIAPManager.h"
#import "UIColor+TMColorAdditions.h"
#import "TMErrorMessages.h"
#import "TMError.h"
#import "TMPayment.h"
#import "TMBuySparkTNCViewController.m"
#import "TMAnalytics.h"

@interface TMBuySparkViewController ()<TMSparkPackageListViewDelegate,TMIAPManagerDelegate>

@property(nonatomic,strong)UIView* contentView;
@property(nonatomic,strong)UIView* headerView;
@property(nonatomic,strong)TMActivityIndicatorView* activityIndicatorView;
@property(nonatomic,strong)TMSparkPackageListView* sparkPackageListView;
@property(nonatomic,strong)UIImageView* sparkIcon;
@property(nonatomic,strong)UILabel* textLabel;

@end

@implementation TMBuySparkViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupHeaderView];
    [self setupContentView];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIImageView*)sparkIcon {
    if(!_sparkIcon) {
        _sparkIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _sparkIcon.image = [UIImage imageNamed:@"popUpSpark"];
        [self.contentView addSubview:_sparkIcon];
    }
    return _sparkIcon;
}
-(TMSparkPackageListView*)sparkPackageListView {
    if(!_sparkPackageListView) {
        _sparkPackageListView = [[TMSparkPackageListView alloc] initWithFrame:self.contentView.bounds];
        _sparkPackageListView.delegate = self;
    }
    return _sparkPackageListView;
}

-(void)setupHeaderView {
    CGFloat headerViewHeight = 30;
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.view.frame), headerViewHeight)];
    [self.view addSubview:self.headerView];
    
    UIButton *cancelActionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelActionButton addTarget:self action:@selector(cancelButtonAciton) forControlEvents:UIControlEventTouchUpInside];
    cancelActionButton.backgroundColor = [UIColor whiteColor];
    cancelActionButton.frame = CGRectMake(0, 0, 60, 44);
    [self.headerView addSubview:cancelActionButton];
    
    CGFloat iconWidth = 12;
    CGFloat iconYPos = (headerViewHeight-iconWidth)/2;
    UIImageView *crossIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15,iconYPos,iconWidth,iconWidth)];
    crossIcon.image = [UIImage imageNamed:@"sparkbuyclose"];
    [self.headerView addSubview:crossIcon];
}
-(void)setupContentView {
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                CGRectGetMaxY(self.headerView.frame),
                                                                CGRectGetWidth(self.view.frame),
                                                                CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.headerView.frame))];
    [self.view addSubview:self.contentView];
}
-(void)removerAllSubviews {
    NSArray *subviews = [self.contentView subviews];
    for (UIView *view in subviews) {
        [view removeFromSuperview];
    }
}
-(void)cancelButtonAciton {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
-(void)showSparkPackages {
    [self showDefaultState];
    
    [[TMIAPManager sharedManager] getSparkPackages:^(NSArray *packageList, NSString *sparkVideoUrl,TMError *error) {
        [self.activityIndicatorView stopAnimating];
        if(packageList && !error) {
            if(packageList.count) {
                [self showSparkPackages:packageList withVideo:sparkVideoUrl];
            }
            else {
                [self showSparkPackageErrorWithMessage:@"No spark package available."];
            }
        }
        else {
            NSString *errorMessage = (error.errorCode == TMERRORCODE_NONETWORK) ? TM_INTERNET_NOTAVAILABLE_MSG: @"Problem in getting spark packages. Please try again";
            [self showSparkPackageErrorWithMessage:errorMessage];
        }
    }];
}
-(void)updatePaymentStateWithMessage:(NSString*)message {
    self.textLabel.text = message;
}
-(void)updatePaymentStateWithErrorMessage:(NSString*)errorMessage {
    [self removerAllSubviews];
    
    CGFloat iconWidth = 60;
    CGFloat yOffsetBetweenIconAndSubTitleLabel = 10;
    CGFloat subtitleLabelHeight = 40;
    CGFloat yOffsetBetweensubtitleLabelAndButton = 10;
    CGFloat ButtonHeight = 50;
    CGFloat totalHeight = (iconWidth+yOffsetBetweenIconAndSubTitleLabel
                           + subtitleLabelHeight + yOffsetBetweensubtitleLabelAndButton+ButtonHeight);
    
    CGFloat initYPos = (CGRectGetHeight(self.view.frame) - totalHeight)/2;
    
    [self showSparkPaymentFailureWithMessage:errorMessage initYPos:initYPos];
}
-(void)showDefaultState {
    [self removerAllSubviews];
    [self showActivityIndicator];
}
-(void)showDefaultPaymentState {
    [self removerAllSubviews];
    
    [self showActivityIndicator];
    
    CGRect frame = self.view.frame;
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(frame)-200)/2,
                                                               CGRectGetMaxY(self.activityIndicatorView.frame),
                                                               200, 40)];
    self.textLabel.text = @"Initiating Payment";
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.textLabel];
}
-(void)showActivityIndicator {
    CGRect frame = self.view.frame;
    self.activityIndicatorView = [[TMActivityIndicatorView alloc] initWithFrame:CGRectZero];
    self.activityIndicatorView.titleText = nil;
    self.activityIndicatorView.frame = CGRectMake((CGRectGetWidth(frame) - CGRectGetWidth(self.activityIndicatorView.frame))/2,
                                                  ((CGRectGetHeight(frame) - CGRectGetHeight(self.activityIndicatorView.frame))/2)-60,
                                                  CGRectGetWidth(self.activityIndicatorView.frame),
                                                  CGRectGetHeight(self.activityIndicatorView.frame));
    [self.contentView addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
}
-(void)showSparkPackages:(NSArray*)packages withVideo:(NSString *)sparkVideoUrl{
    if(!self.sparkPackageListView.superview) {
        [self.contentView addSubview:self.sparkPackageListView];
    }
    [self.sparkPackageListView loadData:packages withVideo:(NSString *)sparkVideoUrl];
}
-(void)showSparkPackageErrorWithMessage:(NSString*)errorMessage {
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView = nil;

    CGFloat iconWidth = 60;
    CGFloat width = self.view.frame.size.width;
    CGFloat errorViewHeight = 85;
    CGFloat yOffset = 10;
    CGFloat sparkIconYPos = (CGRectGetHeight(self.view.frame) - (errorViewHeight+yOffset+iconWidth))/2;
    self.sparkIcon.frame = CGRectMake((width-iconWidth)/2, sparkIconYPos, iconWidth, iconWidth);
    [self showErrorViewWithYPos:CGRectGetMaxY(self.sparkIcon.frame)+yOffset errorMessage:errorMessage];
}
-(void)showErrorViewWithYPos:(CGFloat)yPos errorMessage:(NSString*)errorMessage {
    
    CGFloat xPos = 10;
    CGFloat width = CGRectGetWidth(self.view.frame) - (2 * xPos);
    UILabel *titleHdr = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, width, 40)];
    titleHdr.textColor = [UIColor likeColor];
    titleHdr.text = errorMessage;
    titleHdr.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    titleHdr.textAlignment = NSTextAlignmentCenter;
    titleHdr.numberOfLines = 0;
    [self.contentView addSubview:titleHdr];
    
    CGFloat buttonWidth = 100;
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame)-buttonWidth)/2,
                                                                      CGRectGetMaxY(titleHdr.frame)+5,
                                                                      buttonWidth, 40)];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitle:@"Retry" forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    sendButton.backgroundColor = [UIColor likeColor];
    sendButton.layer.cornerRadius = 10;
    [sendButton addTarget:self action:@selector(sparkPackageRetryButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:sendButton];
}
-(void)showSparkPaymentFailureWithMessage:(NSString*)errorMessage initYPos:(CGFloat)initYPos {
    
    CGFloat iconWidth = 60;
    CGFloat yOffsetBetweenIconAndSubTitleLabel = 10;
    CGFloat subtitleLabelHeight = 40;
    CGFloat yOffsetBetweensubtitleLabelAndButton = 10;
    CGFloat ButtonHeight = 50;
    CGFloat xPos = 10;
    CGFloat width = CGRectGetWidth(self.view.frame) - (2 * xPos);
    
    self.sparkIcon.frame = CGRectMake((width-iconWidth)/2,
                                      initYPos,
                                      iconWidth,
                                      iconWidth);
    
    UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos,
                                                                       CGRectGetMaxY(self.sparkIcon.frame)+yOffsetBetweenIconAndSubTitleLabel,
                                                                       width,
                                                                       subtitleLabelHeight)];
    subtitleLabel.textColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0];
    subtitleLabel.text = errorMessage;
    subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
    subtitleLabel.textAlignment = NSTextAlignmentCenter;
    subtitleLabel.numberOfLines = 2;
    [self.contentView addSubview:subtitleLabel];
    
    CGFloat buttonWidth = 150;
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame)-buttonWidth)/2,
                                                                      CGRectGetMaxY(subtitleLabel.frame)+yOffsetBetweensubtitleLabelAndButton,
                                                                      buttonWidth,
                                                                      ButtonHeight)];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitle:@"Retry" forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    sendButton.backgroundColor = [UIColor likeColor];
    sendButton.layer.cornerRadius = 10;
    [sendButton addTarget:self action:@selector(sparkPackageBuyFailureRetryButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:sendButton];
}


-(void)sparkPackageRetryButtonClick {
    [self showSparkPackages];
}

-(void)sparkPackageBuyFailureRetryButtonClick {
    [self showSparkPackages];
}

-(void)didSelectMatchGuranteeTNC {
    TMBuySparkTNCViewController *tncViewController = [[TMBuySparkTNCViewController alloc] init];
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
    tncViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:tncViewController animated:TRUE completion:nil];
}
-(void)didBuySparkPackageWithIdentifier:(NSString*)packageIdentifier {
    
    if([[TMIAPManager sharedManager] canConnect]) {
        [self showDefaultPaymentState];
        self.view.userInteractionEnabled = FALSE;
        
        [[TMIAPManager sharedManager] buyPackage:Spark withProductIdentifier:packageIdentifier withDelegateHandler:self];
        
        [self.delegate didInitiatePaymentWithPackageId:packageIdentifier];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TM_INTERNET_NOTAVAILABLE_MSG
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
        [alertView show];
    }
}

-(void)didUpdateTransactionStatus:(NSString*)paymentStatus {
    [self updatePaymentStateWithMessage:paymentStatus];
}
-(void)didFinishPaymentTransaction:(TMPayment*)payment {
    self.view.userInteractionEnabled = TRUE;
    if( (payment.paymentStatus == success) && payment.sparkCount ) {
        [self.delegate didCompletePaymentForPackageId:payment.packageIdentifier
                                           sparkCount:payment.sparkCount
                                                 show:payment.showChatAssist];
        [self trackBuySparkEvent];
    }
    else {
        [self updatePaymentStateWithErrorMessage:payment.errorDisplayText];
        [self.delegate didFailToCompletePaymentWithTrackingError:payment.errorTrackingText];
    }

}

- (void)trackBuySparkEvent {
    NSMutableDictionary *fbEventInfo = [NSMutableDictionary dictionaryWithCapacity:5];
    NSDictionary *packageData = self.sparkPackageListView.purchasedPackageForTracking;
    if(packageData != nil) {
        fbEventInfo[@"amount"] = [NSNumber numberWithDouble:[packageData[@"price"] doubleValue]];
        fbEventInfo[@"number_of_items"] = [NSNumber numberWithInteger:[packageData[@"spark_count"] integerValue]];
        if([packageData[@"currency"] isEqualToString:@"₹"]) {
            fbEventInfo[@"currency"] = @"INR";
        }else {
            fbEventInfo[@"currency"] = @"USD";
        }
        fbEventInfo[@"content_type"] = @"Sparks" ;
        fbEventInfo[@"package_identifier"] = packageData[@"apple_sku"];
        [[TMAnalytics sharedInstance] trackFacebookEvent:@"buy_spark" withParams:fbEventInfo];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    if([self.contentView superview]) {
        [self.contentView removeFromSuperview];
        self.sparkPackageListView = nil;
        self.contentView = nil;
        self.headerView = nil;
    }
}

@end

//
//  TMSparkCollectionViewHeader.m
//  TrulyMadly
//
//  Created by Ankit on 10/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkCollectionViewHeader.h"
#import "UIColor+TMColorAdditions.h"

@interface TMSparkCollectionViewHeader ()

@property(nonatomic,strong)UILabel *titleLabel;

@end

@implementation TMSparkCollectionViewHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor conversationHeaderColor];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,
                                                                    0,
                                                                    CGRectGetWidth(self.frame)-40,
                                                                    CGRectGetHeight(self.frame))];
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = [UIColor conversationHeaderTitleColor];
        self.titleLabel.text = @"Sparks";
        [self addSubview:self.titleLabel];
        
        UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(frame) - 0.7,
                                                                         CGRectGetWidth(frame),
                                                                         0.7)];
        [seperatorView setBackgroundColor:[UIColor conversationListSeperatorColor]];
        [self addSubview:seperatorView];
    }
    return self;
}
@end

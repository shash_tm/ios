//
//  TMSparkCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 09/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSwipeCollectionViewCell.h"


@interface TMSparkCollectionViewCell : UICollectionViewCell

+ (NSString *)cellReuseIdentifier;

-(void)setName:(NSString*)name age:(NSString*)age designation:(NSString*)designation isSelectUser:(BOOL)selectUser;
-(void)setMessage:(NSString*)message;
-(void)setProfileImage:(NSString*)profileImageURLString;
-(void)updateProgressViewWithPercentage:(NSString*)progressPercent;
-(void)setupTimerView;
-(void)setupTutorialView;

@end

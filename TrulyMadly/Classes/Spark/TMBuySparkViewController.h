//
//  TMBuySparkViewController.h
//  TrulyMadly
//
//  Created by Ankit on 10/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMIneractiveModalViewController.h"

@protocol TMBuySparkViewControllerDelegate;

@interface TMBuySparkViewController : TMIneractiveModalViewController

@property(nonatomic,weak)id<TMBuySparkViewControllerDelegate> delegate;

-(void)showSparkPackages;
-(void)showDefaultPaymentState;
-(void)updatePaymentStateWithErrorMessage:(NSString*)errorMessage;
-(void)updatePaymentStateWithMessage:(NSString*)message;

@end


@protocol TMBuySparkViewControllerDelegate <NSObject>

-(void)didInitiatePaymentWithPackageId:(NSString*)packageId;
-(void)didCompletePaymentForPackageId:(NSString*)packageId sparkCount:(NSInteger)sparkCount show:(BOOL)chatAssistIcon;
-(void)didFailToCompletePaymentWithTrackingError:(NSString*)errorDescription;

@end

//
//  TMSparkPackageListView.m
//  TrulyMadly
//
//  Created by Ankit on 11/08/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkPackageListView.h"
#import "TMSparkPackageTableViewCell.h"
#import "DDPageControl.h"
#import "TMSparkPackageInfo.h"
#import "NSString+TMAdditions.h"
#import "TMBuySparkTNCViewController.h"
#import "TMBuySparkHeaderView.h"
#import "TMUserSession.h"

@interface TMSparkPackageListView ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

/*! A tableview object that contains the list of spark packages. */
@property(nonatomic,strong)UITableView* sparkPackageTableView;
@property(nonatomic,strong)UIView *baseView;
/*! This property contains the images to be displayed. */
@property(nonatomic,strong)UIView *headerView;
/*! This property contains the tableview object. */
@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIView *sparkInfoView;
/*! A view object that holds terms and conditions. */
@property(nonatomic,strong)UIView *tncView;
/*! A pagecontrol object for pagination. */
@property(nonatomic,strong)DDPageControl* pageControl;
/*! This property contains the list of spark packages. */
@property(nonatomic,strong)NSArray* packages;
/*! This property holds the index of selected spark package. */
@property(nonatomic,assign)NSInteger selectedIndex;
/*! This property holds the url of the video to be shown. */
@property(nonatomic,strong)NSString *videoUrl;
@end


@implementation TMSparkPackageListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

/*!
 @brief This method loads the data passed as parameters and configures the view.
 @see   setupHeaderView
 @see   setupContentView
 @param packageList List of spark packages that needs to be displayed.
 @param sparkVideoUrl  Url of the video to be shown.
 */
-(void)loadData:(NSArray*)packageList withVideo:(NSString *)sparkVideoUrl{
    self.packages = packageList;
    self.videoUrl = sparkVideoUrl;
    [self setupHeaderView];
    [self setupContentView];
}

/*!
 @brief This method configures the header view. It loads the image/video asset and adds page control to the view.
*/
-(void)setupHeaderView {
    
    if(self.headerView.superview) {
        [self.headerView removeFromSuperview];
        self.headerView = nil;
        
    }
    
    ///A list of messages to be displayed on the header view.
    NSArray *text = @[@"Send a personal message",
                      @"Appear on top of the conversations",
                      @"5x more chances of becoming a match",
                      @"Watch how it works"];
    
    NSArray *images;
    
    ///Initializes array with different image names as per the user.
    if([[TMUserSession sharedInstance].user isUserFemale]) {
        images = @[@"buySparkPage1",@"buySparkPage2",@"buySparkPage3", @"buySparkVideoPage4"];
    }else {
        images = @[@"buySparkGuysPage1",@"buySparkGuysPage2",@"buySparkGuysPage3", @"buySparkGuysVideoPage"];
    }
    
    ///Setting up the frame of header view.
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = (CGRectGetHeight(self.frame) * 55)/100;
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height+20)];
    [self addSubview:self.headerView];
    
    ///Initializing and adding scroll view to the header view.
    UIScrollView *introScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,width,height)];
    introScrollView.delegate = self;
    introScrollView.backgroundColor = [UIColor clearColor];
    introScrollView.showsHorizontalScrollIndicator = FALSE;
    introScrollView.pagingEnabled = TRUE;
    [self.headerView addSubview:introScrollView];
    
    ///Configuring scroll view with the images and labels corresponding to each other.
    CGFloat introViewXPos  = 0;
    CGFloat introViewYPos  = 0;
    CGFloat introViewWidth = CGRectGetWidth(introScrollView.frame);
    CGFloat introViewHeight = CGRectGetHeight(introScrollView.frame);
    CGFloat contentWidth = CGRectGetWidth(introScrollView.frame);
    
    for (int introViewCount=0; introViewCount<text.count; introViewCount++) {
        
        TMBuySparkHeaderView *headerView;
        UIView *view;
        UILabel *textLabel;
        CGRect textFrame;
        
        view = [[UIView alloc] initWithFrame:CGRectMake(introViewXPos, introViewYPos, introViewWidth, introViewHeight)];
        
        ///Configuring view with image/video.
        headerView = [[TMBuySparkHeaderView alloc] initWithFrame:CGRectMake(12, 0, CGRectGetWidth(view.frame) - 24, CGRectGetHeight(view.frame)-60)];
        if(introViewCount == text.count - 1) {
            [headerView loadDataOnView:images[introViewCount] withVideoUrl:self.videoUrl];
        }else {
            [headerView loadDataOnView:images[introViewCount] withVideoUrl:nil];
        }
        
        ///configuring textLabel frame with message.
        textFrame = [text[introViewCount] boundingRectWithConstraintSize:CGSizeMake(introViewWidth - 20, 100) attributeDictionary:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(view.frame)- 50, introViewWidth-20, CGRectGetHeight(textFrame))];
        textLabel.text = text[introViewCount];
        textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.numberOfLines = 2;
        textLabel.textColor = [UIColor blackColor];
        
        [view addSubview:headerView];
        [view addSubview:textLabel];
        [introScrollView addSubview:view];
        
        introViewXPos = introViewXPos + introViewWidth;
        contentWidth = CGRectGetMaxX(view.frame);
    }
    
    ///Determining actual content size of the scroll view.
    introScrollView.contentSize = CGSizeMake(contentWidth, introViewHeight);
    
    self.pageControl = [[DDPageControl alloc] initWithFrame:CGRectZero];
    [self.pageControl setType: DDPageControlTypeOnFullOffEmpty] ;
    [self.pageControl setOnColor: [UIColor blackColor]];
    [self.pageControl setOffColor: [UIColor blackColor]];
    [self.pageControl setIndicatorDiameter: 6.0f];
    [self.pageControl setIndicatorSpace: 5.0f];
    [self.pageControl setNumberOfPages:text.count];
    self.pageControl.frame = CGRectMake((width - CGRectGetWidth(self.pageControl.frame))/2,
                                    introViewHeight-16,
                                    CGRectGetWidth(self.pageControl.frame),
                                    CGRectGetHeight(self.pageControl.frame));
    [self.headerView addSubview:self.pageControl];
}

/*!
 @brief This method configures the content view that holds the tableview.
 @see setupSparkPackageTableView
 */
-(void)setupContentView {
    if(self.contentView.superview) {
        [self.contentView removeFromSuperview];
        self.contentView = nil;
    }
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat yPos = CGRectGetMaxY(self.headerView.frame);
    CGFloat contentViewHeight = CGRectGetHeight(self.frame) - CGRectGetHeight(self.headerView.frame);
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0,yPos,width,contentViewHeight)];
    [self addSubview:self.contentView];
    
    [self setupSparkPackageTableView];
}

/*!
 @brief This method configures the tableview that displays the list of spark packages.
 */
-(void)setupSparkPackageTableView {
    self.sparkPackageTableView = [[UITableView alloc] initWithFrame:self.contentView.bounds
                                                          style:UITableViewStylePlain];
    
    [self.sparkPackageTableView registerClass:[TMSparkPackageTableViewCell class] forCellReuseIdentifier:@"uiCell"];
    self.sparkPackageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.sparkPackageTableView.dataSource = self;
    self.sparkPackageTableView.delegate = self;
    [self.contentView addSubview:self.sparkPackageTableView];
}

/*!
 @brief This method configures the footerview for displaying terms and conditions if match guarantee flag is enabled for any of the spark packages.
 */
-(void)setupMatchGuranteeView {
    if(!self.tncView.superview) {
        CGFloat height = 60;
        self.tncView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.contentView.frame), height)];
        
        UILabel *tncLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        tncLabel.textColor = [UIColor blackColor];
        tncLabel.text = @"*Terms & Condition Apply";
        tncLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
        [self.tncView addSubview:tncLabel];
        
        NSDictionary *attributes = @{NSFontAttributeName:tncLabel.font};
        CGSize constrintSize = CGSizeMake(300, height);
        CGRect rect = [tncLabel.text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
        
        tncLabel.frame = CGRectMake(10, 0, CGRectGetWidth(rect), height);
        
        CGFloat imageviewWidth = 14;
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tncLabel.frame)+5,
                                                                               (height - imageviewWidth)/2, imageviewWidth, imageviewWidth)];
        imageview.image = [UIImage imageNamed:@"sparktnc"];
        [self.tncView addSubview:imageview];
        
        self.sparkPackageTableView.tableFooterView = self.tncView;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tncAction)];
        [self.tncView addGestureRecognizer:tapGesture];
    }
}
/*!
 @brief This method calls a delegate method to display a view describing terms and conditions, when a user taps on the footerview.
 @see didSelectMatchGuranteeTNC
 */
-(void)tncAction {
    [self.delegate didSelectMatchGuranteeTNC];
}

#pragma mark - UITableview datasource / delegate methods
#pragma mark -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.packages.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TMSparkPackageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"uiCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSInteger index = indexPath.section;
    
    TMSparkPackageInfo *sparkPackageInfo = [[TMSparkPackageInfo alloc] initWithDictionary:self.packages[index]];
    [cell configureCellWithData:sparkPackageInfo];
    if(sparkPackageInfo.tagType == TMBUYPACK_TAG_MATCHGURANTEE && (!tableView.tableFooterView)) {
        [self setupMatchGuranteeView];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *packageDictionary = self.packages[indexPath.section];
    self.purchasedPackageForTracking = packageDictionary;
    NSString *packageIdentifier = packageDictionary[@"apple_sku"];
    ///Delegate method called to initiate purchase of selected package.
    [self.delegate didBuySparkPackageWithIdentifier:packageIdentifier];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == self.packages.count-1) {
        return 0.5;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:102.0f/255.0f green:102.0F/255.0f blue:102.0F/255.0F alpha:1];
    return headerView;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:102.0f/255.0f green:102.0F/255.0f blue:102.0F/255.0F alpha:1];
    return headerView;
}


#pragma mark - UIScrollview delegate methods
#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
    
    ///Posts notification to stop the active video player when out of focus.
    if(page != self.pageControl.numberOfPages) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopPlayer" object:nil];
    }
}

@end

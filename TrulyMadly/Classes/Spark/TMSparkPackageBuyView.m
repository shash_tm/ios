//
//  TMSparkPackageBuyView.m
//  TrulyMadly
//
//  Created by Ankit on 27/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSparkPackageBuyView.h"
#import "TMWindowView.h"
#import "UIColor+TMColorAdditions.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+TMAdditions.h"


@interface TMSparkPackageBuyView ()<TMWindowViewDelegate>

@property(nonatomic,strong)TMWindowView* windowView;
//@property(nonatomic,strong)UIView* errorView;
@property(nonatomic,strong)UIImageView* sparkIcon;
@property(nonatomic,strong)UIImageView* titleImageView;
@property(nonatomic,strong)UILabel* textLabel;

@property(nonatomic,assign)CGFloat xPos;
@property(nonatomic,assign)CGFloat width;
@property(nonatomic,assign)CGFloat height;
//@property(nonatomic,assign)CGFloat sparkIconWidth;

@end

@implementation TMSparkPackageBuyView

#pragma mark - Initializer
#pragma mark -

- (instancetype)init {
    self = [super init];
    if (self) {
        self.clipsToBounds = TRUE;
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius  = 10;
        [self setupDefault];
    }
    return self;
}

-(void)setupDefault {
    self.xPos = 20;
    self.width = CGRectGetWidth(self.windowView.frame) - (2*self.xPos);
    self.height = 300;
   //elf.sparkIconWidth = 60;
}
-(void)removerAllSubviews {
    NSArray *subviews = [self subviews];
    for (UIView *view in subviews) {
        [view removeFromSuperview];
    }
}

#pragma mark - getter methods
#pragma mark -

-(TMWindowView*)windowView {
    if(!_windowView) {
        _windowView = [[TMWindowView alloc] init];
        _windowView.delegate = self;
    }
    return _windowView;
}
-(UIImageView*)sparkIcon {
    if(!_sparkIcon) {
        _sparkIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _sparkIcon.image = [UIImage imageNamed:@"popUpSpark"];
        [self addSubview:_sparkIcon];
    }
    if(!_sparkIcon.superview) {
        [self addSubview:_sparkIcon];
    }
    return _sparkIcon;
}

#pragma mark - Public methods
#pragma mark -

-(void)deinit {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self removeFromSuperview];
    [self.windowView deinit];
    self.windowView = nil;
}
-(void)showSelectLongTermNudgeAlertWithTitleText:(NSString*)titleText {
    [self setupSelectIconImageView];
    
    NSArray *texts = @[titleText];
    NSArray *actiontexts = @[@"NO THANKS",@"LEARN MORE"];
    
    [self setupNudgeViewWithImageURLStrings:nil
                         actionButtonTitles:actiontexts
                     attributedDescriptions:texts
                                  iconImage:nil
                                  buttonSel:NSSelectorFromString(@"joinSelectAction:")
                       imageViewBorderColor:nil
                                totalHeight:300];
}

-(void)showNRIProfileNudgeAlertwithText {
    
    NSString *titleText = @"You can now see matches from US. Edit your preferences to change this anytime you want!";
    NSDictionary *boldAttributes = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18],
                                      NSForegroundColorAttributeName:[UIColor colorWithRed:105.0f/255.0f green:105.0f/255.0f blue:105.0f/255.0f alpha:1]};
    NSDictionary *nonBoldAttributes = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:18],
                                         NSForegroundColorAttributeName:[UIColor colorWithRed:105.0f/255.0f green:105.0f/255.0f blue:105.0f/255.0f alpha:1]};
    NSAttributedString *attributedString = [titleText attributedStringWithNonBoldAttributes:nonBoldAttributes
                                                                             boldAttributes:boldAttributes
                                                                                  boldRange:NSMakeRange(12, 17)];
    NSArray *texts = @[attributedString];
    NSArray *actiontexts = @[@"EDIT", @"GOT IT!"];
    
    [self setUpNudgeViewForNRIProfileWithActionButtonTitles:actiontexts attributedDescriptions:texts buttonSel:NSSelectorFromString(@"showNRIProfileNudgeAction:") totalHeight:180];
}
-(void)showSelectActionNudgeAlertWithActionText:(NSString*)actionText withImageURLStrings:(NSArray*)imageURLStrings {
    NSString *titleText = @"Please join TrulyMadly Select to connect with this person";
    NSDictionary *boldAttributes = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18],
                                      NSForegroundColorAttributeName:[UIColor colorWithRed:105.0f/255.0f green:105.0f/255.0f blue:105.0f/255.0f alpha:1]};
    NSDictionary *nonBoldAttributes = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:18],
                                      NSForegroundColorAttributeName:[UIColor colorWithRed:105.0f/255.0f green:105.0f/255.0f blue:105.0f/255.0f alpha:1]};
    NSAttributedString *attributedString = [titleText attributedStringWithNonBoldAttributes:nonBoldAttributes
                                                                             boldAttributes:boldAttributes
                                                                                  boldRange:NSMakeRange(12, 17)];
    NSArray *texts = @[attributedString];
    NSArray *actiontexts = @[@"NO THANKS",actionText];
    
    [self setupNudgeViewWithImageURLStrings:imageURLStrings
                         actionButtonTitles:actiontexts
                     attributedDescriptions:texts
                                  iconImage:nil
                                  buttonSel:NSSelectorFromString(@"joinSelectAction:")
                       imageViewBorderColor:[UIColor colorWithRed:126.0f/255.0f green:38.0f/255.0f blue:135.0f/255.0f alpha:1]
                                totalHeight:326];
}
-(void)showHasLikedBeforeAlertWithImageURLStrings:(NSArray*)imageURLStrings {
    NSArray *texts = @[@"You have liked her before, this time jump the queue?"];
    NSArray *actiontexts = @[@"NO THANKS",@"SPARK NOW!"];
    [self setupNudgeViewWithImageURLStrings:imageURLStrings
                         actionButtonTitles:actiontexts
                     attributedDescriptions:texts
                                  iconImage:nil
                                  buttonSel:NSSelectorFromString(@"hasLikedBeforeSparkAlertAction:")
                       imageViewBorderColor:[UIColor colorWithRed:255.0f/255.0f green:183.0f/255.0f blue:0 alpha:1]
                                totalHeight:326];
}
-(void)showSparkNowAlertViewWithImageURLStrings:(NSArray*)imageURLStrings {
    
    NSArray *texts = @[@"Why stop at just a 'Like'?",@"Send a Spark and you'll have 5X more chances of getting a match!"];
    NSArray *actiontexts = @[@"MAYBE LATER",@"SPARK NOW!"];
    [self setupNudgeViewWithImageURLStrings:imageURLStrings
                         actionButtonTitles:actiontexts
                     attributedDescriptions:texts
                                  iconImage:nil
                                  buttonSel:NSSelectorFromString(@"sparkAlertAction:")
                       imageViewBorderColor:[UIColor colorWithRed:255.0f/255.0f green:183.0f/255.0f blue:0 alpha:1]
                                totalHeight:356];
}
-(void)showSparkTutorialView {
    [self removerAllSubviews];
    
    CGFloat xPos = 20;
    CGFloat width = CGRectGetWidth(self.windowView.frame) - 2*xPos;
    CGFloat height = 300;
    self.frame = CGRectMake(xPos, (CGRectGetHeight(self.windowView.frame)-height)/2, width, height);
    if(!self.superview) {
        [self.windowView addSubview:self];
    }
    
    [self setupSparkTutorial];
}
-(void)showLastSparkAlertState {
    [self removerAllSubviews];
  
    CGFloat xPos = 10;
    CGFloat width = CGRectGetWidth(self.windowView.frame) - 2*xPos;
    CGFloat height = 288;
    self.frame = CGRectMake(xPos, (CGRectGetHeight(self.windowView.frame)-height)/2, width, height);
    if(!self.superview) {
        [self.windowView addSubview:self];
    }
    
    [self setupLastSparkAlert];
}
-(void)showSparkPaymentCompletionWithNewSparkCount:(NSInteger)sparkCount show:(BOOL)relationshipExpertIcon{
    [self removerAllSubviews];
    NSString *sparkCountText = [NSString stringWithFormat:@"%ld new Sparks",(long)sparkCount];
    NSArray *labelTexts = (relationshipExpertIcon) ? @[sparkCountText,@"&",@"TM Relationship Expert added"] : @[[NSString stringWithFormat:@"%@ added",sparkCountText]];
    NSArray *icons = (relationshipExpertIcon) ? @[@"popUpSpark",@"relationshipexpert"] : @[@"popUpSpark"];
    
    CGFloat xPos = 20;
    CGFloat width = CGRectGetWidth(self.windowView.frame) - 2*xPos;
    CGFloat height = 300;
    self.frame = CGRectMake(xPos, (CGRectGetHeight(self.windowView.frame)-height)/2, width, height);
    if(!self.superview) {
        [self.windowView addSubview:self];
    }
    
    CGFloat iconWidth = 60;
    CGFloat yOffsetBetweenTitleLabelAndIcon = 30;
    CGFloat ButtonHeight = 50;
    CGFloat labelHeight = 30;
    
    CGFloat contentHeight = ((labelTexts.count*labelHeight) + yOffsetBetweenTitleLabelAndIcon + iconWidth);
    CGFloat initYPos = ((height-ButtonHeight) - contentHeight)/2;
    
    for(NSString* text in labelTexts) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, initYPos, width, labelHeight)];
        label.text = text;
        label.textColor = [UIColor likeColor];
        label.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:19.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        [self addSubview:label];
        
        initYPos += labelHeight;
    }
    
    CGFloat imageXPosOffset = (relationshipExpertIcon) ? 30 : 0;
    CGFloat imageXPos = (width - ((icons.count * iconWidth)+imageXPosOffset))/2;
    for(NSString* imageName in icons) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageXPos,
                                                                               initYPos+yOffsetBetweenTitleLabelAndIcon,
                                                                               iconWidth,
                                                                               iconWidth)];
        imageView.image = [UIImage imageNamed:imageName];
        [self addSubview:imageView];
        imageXPos += iconWidth + imageXPosOffset;
    }
    
    UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0,
                                                                      height-ButtonHeight,
                                                                      width,
                                                                      ButtonHeight)];
    [actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [actionButton setTitle:@"GET SPARKING" forState:UIControlStateNormal];
    actionButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    actionButton.backgroundColor = [UIColor likeColor];
    [actionButton addTarget:self action:@selector(sparkPackageBuySuccessContinueButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:actionButton];
}

-(void)setupSparkTutorial{
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat iconWidth = 80;
    CGFloat titleLabelHeight = 30;
    CGFloat yOffsetBetweenTitleLabelAndIcon = 10;
    CGFloat yOffsetBetweenIconAndSubTitleLabel = 10;
    CGFloat subtitleLabelHeight = 60;
    CGFloat yOffsetBetweensubtitleLabelAndButton = 20;
    CGFloat ButtonHeight = 50;
    CGFloat totalHeight = (titleLabelHeight + yOffsetBetweenTitleLabelAndIcon + iconWidth+yOffsetBetweenIconAndSubTitleLabel
                           + subtitleLabelHeight + yOffsetBetweensubtitleLabelAndButton+ButtonHeight);
    
    CGFloat initYPos = (CGRectGetHeight(self.frame) - totalHeight)/2;
    CGFloat xPos = (width - width)/2;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, initYPos, width, titleLabelHeight)];
    titleLabel.textColor = [UIColor likeColor];
    titleLabel.text = @"Introducing Spark";
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:19.0];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 0;
    [self addSubview:titleLabel];
    
    xPos = (width - iconWidth)/2;
    self.sparkIcon.frame = CGRectMake(xPos,
                                      CGRectGetMaxY(titleLabel.frame) + yOffsetBetweenTitleLabelAndIcon,
                                      iconWidth,
                                      iconWidth);
    
    xPos = 10;
    UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos,
                                                                       CGRectGetMaxY(self.sparkIcon.frame)+yOffsetBetweenIconAndSubTitleLabel,
                                                                       width - (2*xPos),
                                                                       subtitleLabelHeight)];
    subtitleLabel.textColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0];
    subtitleLabel.text = @"Start a conversation without waiting for a 'Like' back. Let someone know they've got the Spark!";
    subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    subtitleLabel.textAlignment = NSTextAlignmentCenter;
    subtitleLabel.numberOfLines = 0;
    subtitleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:subtitleLabel];
    
    CGFloat buttonWidth = 150;
    //xPos = (width - buttonWidth)/2;
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-buttonWidth)/2,
                                                                      CGRectGetMaxY(subtitleLabel.frame)+yOffsetBetweensubtitleLabelAndButton,
                                                                      buttonWidth,
                                                                      ButtonHeight)];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitle:@"Try It!" forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    sendButton.backgroundColor = [UIColor likeColor];
    sendButton.layer.cornerRadius = 10;
    [sendButton addTarget:self action:@selector(sparkTutorialKnowMoreButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sendButton];
}

-(void)setupNudgeViewWithImageURLStrings:(NSArray*)imageURLStrings
                      actionButtonTitles:(NSArray*)buttonTiltes
                  attributedDescriptions:(NSArray*)descriptionTexts
                               iconImage:(UIImage*)iconImage
                               buttonSel:(SEL)selector
                    imageViewBorderColor:(UIColor*)borderColor
                             totalHeight:(CGFloat)height {
    [self removerAllSubviews];
    
    self.height = height;
    self.frame = CGRectMake(self.xPos, (CGRectGetHeight(self.windowView.frame)-self.height)/2, self.width, self.height);
    if(!self.superview) {
        [self.windowView addSubview:self];
    }
    
    
    CGFloat maxYPos = 0;
    if(imageURLStrings.count) {
        CGFloat initYPos = 30;
        CGFloat imageViewWidth = 140;
        CGFloat imageViewXOverlap = 44;
        CGFloat imageViewXPos = (CGRectGetWidth(self.frame) - ((2*imageViewWidth) - imageViewXOverlap))/2;
        for (int imageCount=0; imageCount<2; imageCount++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewXPos, initYPos, imageViewWidth, imageViewWidth)];
            imageView.backgroundColor = [UIColor grayColor];
            imageView.clipsToBounds = TRUE;
            imageView.layer.cornerRadius = imageViewWidth/2;
            imageView.layer.borderWidth = 5;
            imageView.layer.borderColor = borderColor.CGColor;
            [self addSubview:imageView];
            
            imageViewXPos += (imageViewWidth-imageViewXOverlap);
            NSString *imageURLString = imageURLStrings[imageCount];
            [imageView setImageWithURL:[NSURL URLWithString:imageURLString]];
            
            maxYPos = CGRectGetMaxY(imageView.frame);
        }
    }
    else {
        CGFloat imageViewWidth = CGRectGetWidth(self.titleImageView.frame);
        self.titleImageView.frame = CGRectMake((CGRectGetWidth(self.frame) - imageViewWidth)/2,
                                                CGRectGetMinY(self.titleImageView.frame),
                                                CGRectGetWidth(self.titleImageView.frame),
                                               CGRectGetHeight(self.titleImageView.frame));
        [self addSubview:self.titleImageView];
        maxYPos = CGRectGetMaxY(self.titleImageView.frame);
    }
    
    CGFloat titleLabelYPos = maxYPos + 20;
    CGFloat titleLabelXPos = 20;
    CGFloat titleLabelWidth = CGRectGetWidth(self.frame) - (2*titleLabelXPos);
    CGFloat titleLabelHeight = (descriptionTexts.count==1) ? 60 : 40;
    
    for (int labelCount=0; labelCount<descriptionTexts.count; labelCount++) {
        id text = descriptionTexts[labelCount];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos, titleLabelYPos, titleLabelWidth, titleLabelHeight)];
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        titleLabel.numberOfLines = 0;
        if([text isKindOfClass:[NSString class]]) {
            titleLabel.text = (NSString*)text;
        }
        else if([text isKindOfClass:[NSAttributedString class]]) {
            titleLabel.attributedText = (NSAttributedString*)text;
        }
        [self addSubview:titleLabel];
        
        titleLabelYPos += titleLabelHeight;
        maxYPos = CGRectGetMaxY(titleLabel.frame);
    }
    
    CGFloat buttonXPos = 0;
    CGFloat buttonWidth = CGRectGetWidth(self.frame)/2;
    CGFloat buttonHeight = 60;
    CGFloat buttonYPos = CGRectGetHeight(self.frame) - buttonHeight;
    
    NSArray *textColors = @[[UIColor colorWithRed:245.0f/255.0f green:0 blue:115.0f/255.0f alpha:1],[UIColor whiteColor]];
    NSArray *bgColors = @[[UIColor whiteColor],[UIColor colorWithRed:245.0f/255.0f green:0 blue:115.0f/255.0f alpha:1]];
    for (int buttonCount=0; buttonCount<buttonTiltes.count; buttonCount++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(buttonXPos, buttonYPos, buttonWidth, buttonHeight);
        button.backgroundColor = bgColors[buttonCount];
        button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        button.tag = 5000+buttonCount;
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = [UIColor grayColor].CGColor;
        [button setTitle:buttonTiltes[buttonCount] forState:UIControlStateNormal];
        [button setTitleColor:textColors[buttonCount] forState:UIControlStateNormal];
        [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        buttonXPos += buttonWidth;
    }
}

- (void)setUpNudgeViewForNRIProfileWithActionButtonTitles:(NSArray *)buttonTitles
                                   attributedDescriptions:(NSArray *)titleText
                                                buttonSel:(SEL)selector totalHeight:(CGFloat)viewHeight {
    
    [self removerAllSubviews];
    
    self.height = viewHeight;
    self.frame = CGRectMake(self.xPos, (CGRectGetHeight(self.windowView.frame)-self.height)/2, self.width, self.height);
    if(!self.superview) {
        [self.windowView addSubview:self];
    }
    CGFloat maxYPos = 0;
    CGFloat titleLabelYPos = maxYPos + 20;
    CGFloat titleLabelXPos = 20;
    CGFloat titleLabelWidth = CGRectGetWidth(self.frame) - (2*titleLabelXPos);
    CGFloat titleLabelHeight = (titleText.count==1) ? 80 : 40;
    
    for (int labelCount=0; labelCount<titleText.count; labelCount++) {
        id text = titleText[labelCount];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelXPos, titleLabelYPos, titleLabelWidth, titleLabelHeight)];
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        titleLabel.numberOfLines = 0;
        if([text isKindOfClass:[NSString class]]) {
            titleLabel.text = (NSString*)text;
        }
        else if([text isKindOfClass:[NSAttributedString class]]) {
            titleLabel.attributedText = (NSAttributedString*)text;
        }
        [self addSubview:titleLabel];
        
        titleLabelYPos += titleLabelHeight;
        maxYPos = CGRectGetMaxY(titleLabel.frame);
    }
    
    CGFloat buttonXPos = 0;
    CGFloat buttonWidth = CGRectGetWidth(self.frame)/2;
    CGFloat buttonHeight = 60;
    CGFloat buttonYPos = CGRectGetHeight(self.frame) - buttonHeight;
    
    NSArray *textColors = @[[UIColor colorWithRed:245.0f/255.0f green:0 blue:115.0f/255.0f alpha:1],[UIColor whiteColor]];
    NSArray *bgColors = @[[UIColor whiteColor],[UIColor colorWithRed:245.0f/255.0f green:0 blue:115.0f/255.0f alpha:1]];
    for (int buttonCount=0; buttonCount<buttonTitles.count; buttonCount++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(buttonXPos, buttonYPos, buttonWidth, buttonHeight);
        button.backgroundColor = bgColors[buttonCount];
        button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        button.tag = 5000+buttonCount;
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = [UIColor grayColor].CGColor;
        [button setTitle:buttonTitles[buttonCount] forState:UIControlStateNormal];
        [button setTitleColor:textColors[buttonCount] forState:UIControlStateNormal];
        [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        buttonXPos += buttonWidth;
    }
    
    
}

-(void)setupLastSparkAlert {
    
    CGFloat iconWidth = 60;
    CGFloat titleLabelHeight = 30;
    CGFloat yOffsetBetweenTitleLabelAndIcon = 10;
    CGFloat yOffsetBetweenIconAndSubTitleLabel = 10;
    CGFloat subtitleLabelHeight = 20;
    CGFloat yOffsetBetweensubtitleLabelAndButton = 10;
    CGFloat ButtonHeight = 50;
    CGFloat totalHeight = (titleLabelHeight + yOffsetBetweenTitleLabelAndIcon + iconWidth+yOffsetBetweenIconAndSubTitleLabel
                           + subtitleLabelHeight + yOffsetBetweensubtitleLabelAndButton+ButtonHeight);
    
    CGFloat initYPos = (CGRectGetHeight(self.frame) - totalHeight)/2;
    CGFloat xPos = 10;
    CGFloat width = CGRectGetWidth(self.frame) - (2 * xPos);
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, initYPos, width, titleLabelHeight)];
    titleLabel.textColor = [UIColor likeColor];
    titleLabel.text = @"One Spark Left!";
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:19.0];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 0;
    [self addSubview:titleLabel];
    
    self.sparkIcon.frame = CGRectMake((width-iconWidth)/2,
                                      CGRectGetMaxY(titleLabel.frame)+yOffsetBetweenTitleLabelAndIcon,
                                      iconWidth,
                                      iconWidth);
    
    UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos,
                                                                       CGRectGetMaxY(self.sparkIcon.frame)+yOffsetBetweenIconAndSubTitleLabel,
                                                                       width,
                                                                       subtitleLabelHeight)];
    subtitleLabel.textColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0];
    subtitleLabel.text = @"Buy more to keep Sparking!";
    subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
    subtitleLabel.textAlignment = NSTextAlignmentCenter;
    subtitleLabel.numberOfLines = 0;
    [self addSubview:subtitleLabel];
    
    CGFloat buttonWidth = 150;
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-buttonWidth)/2,
                                                                      CGRectGetMaxY(subtitleLabel.frame)+yOffsetBetweensubtitleLabelAndButton,
                                                                      buttonWidth,
                                                                      ButtonHeight)];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitle:@"Buy Spark" forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    sendButton.backgroundColor = [UIColor likeColor];
    sendButton.layer.cornerRadius = 10;
    [sendButton addTarget:self action:@selector(lastSparkAlertBuyButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sendButton];
}
-(void)setupSelectIconImageView {
    CGFloat imageViewWidth = 205;
    CGFloat imageViewHeight = 102;
    CGFloat imageViewYPos = 25;
    self.titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                        imageViewYPos,
                                                                        imageViewWidth, imageViewHeight)];
    self.titleImageView.image = [UIImage imageNamed:@"selectunit"];
    self.titleImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleImageView];
}

#pragma mark - Action Handlers
#pragma mark -
-(void)sparkAlertAction:(UIButton*)sender {
    if(sender.tag == 5000) {
        [self.delegate didClickSparkNowMayBeLaterButton];
    }
    else {
        [self.delegate didClickSparkNowButtonFromAlert];
    }
}
-(void)hasLikedBeforeSparkAlertAction:(UIButton*)sender {
    if(sender.tag == 5000) {
        [self.delegate didTakeLikeNowActionOnHasLikedSparkAlert];
    }
    else {
        [self.delegate didClickSparkNowButtonFromAlert];
    }
}
-(void)joinSelectAction:(UIButton*)sender {
    if(sender.tag == 5000) {
        [self.delegate didCancelJoinSelectNudge];
    }
    else {
        [self.delegate didClickJoinSelectButton];
    }
}

-(void)showNRIProfileNudgeAction:(UIButton *)sender {
    if(sender.tag == 5000) {
        [self.delegate didEditPreferenceForNRIProfile];
    }
    else {
        [self.delegate didCancelShowNRIProfileNudge];
    }
}


#pragma mark - TMWindowView delegate methods
#pragma mark -

-(void)didDissmissWindowView {
    [self.delegate didRemoveSparkView];
}
//-(void)sparkPackageRetryButtonClick {
//    [self.delegate didRetryGetSparkPackage];
//}
-(void)sparkPackageBuySuccessContinueButtonClick {
    [self.delegate didClickSparkPackageBuySuccessContinueButton];
}
-(void)lastSparkAlertBuyButtonClick {
    [self.delegate didClickLastSparkAlertBuyButton];
}
-(void)sparkTutorialKnowMoreButtonClick {
    [self.delegate didClickSparkTutorialKnowMoreButton];
}
//-(void)sparkPackageBuyFailureRetryButtonClick {
//    [self.delegate didClickSparkPackageBuyFailureRetryButton];
//}

-(NSDictionary*)getTrackingInfoDictionary {
    NSMutableDictionary *trackingInfo = [NSMutableDictionary dictionary];
    if(self.source) {
        trackingInfo[@"source"] = self.source;
    }
    if(self.action) {
        trackingInfo[@"on_action"] = self.action;
    }
    return trackingInfo;
}

@end

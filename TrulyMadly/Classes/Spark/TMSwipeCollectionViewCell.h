//
//  TMSwipeCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 16/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TMSwipeCellState)
{
    kCellStateCenter,
    kCellStateLeft,
    kCellStateRight,
};


@protocol TMSwipeCollectionViewCellDelegate;


@interface TMSwipeCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id <TMSwipeCollectionViewCellDelegate> delegate;
@property (nonatomic, copy) NSArray *leftUtilityButtons;
@property (nonatomic, copy) NSArray *rightUtilityButtons;

- (void)setRightUtilityButtons:(NSArray *)rightUtilityButtons WithButtonWidth:(CGFloat) width;
- (void)setLeftUtilityButtons:(NSArray *)leftUtilityButtons WithButtonWidth:(CGFloat) width;

@end


@protocol TMSwipeCollectionViewCellDelegate <NSObject>

@optional
- (void)swipeableTableViewCell:(TMSwipeCollectionViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index;
- (void)swipeableTableViewCell:(TMSwipeCollectionViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;
- (void)swipeableTableViewCell:(TMSwipeCollectionViewCell *)cell scrollingToState:(TMSwipeCellState)state;
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(TMSwipeCollectionViewCell *)cell;
- (BOOL)swipeableTableViewCell:(TMSwipeCollectionViewCell *)cell canSwipeToState:(TMSwipeCellState)state;
- (void)swipeableTableViewCellDidEndScrolling:(TMSwipeCollectionViewCell *)cell;
- (void)swipeableTableViewCell:(TMSwipeCollectionViewCell *)cell didScroll:(UIScrollView *)scrollView;

@end

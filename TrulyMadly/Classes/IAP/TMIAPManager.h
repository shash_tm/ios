//
//  TMIAPManager.h
//  TrulyMadly
//
//  Created by Ankit on 14/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "TMSparkManager.h"

@class TMError;
@class TMPayment;


@protocol TMIAPManagerDelegate;

@interface TMIAPManager : NSObject

@property(nonatomic,weak)id<TMIAPManagerDelegate> delegate;
@property(nonatomic,assign,readonly)BOOL isPaymentInitaitedByUserInProgress;

+(instancetype)sharedManager;

-(BOOL)canConnect;
-(void)addIAPTransactionObserver;
-(void)getSparkPackages:(void(^)(NSArray *packageList, NSString *sparkVideoUrl,TMError *error))responseBlock;
-(void)getCommanalityStringWithMatch:(NSString*)matchId response:(void(^)(NSArray* commonalityList,NSString* errorMessage))commonalityBlock;
-(void)buySpark:(SKProduct*)product;
-(void)buyPackage:(TMPackageType)packageType withProductIdentifier:(NSString *)productIdentifier
withDelegateHandler:(id<TMIAPManagerDelegate>)delegate;
               
@end

@protocol TMIAPManagerDelegate <NSObject>

-(void)didUpdateTransactionStatus:(NSString*)paymentStatus;
-(void)didFinishPaymentTransaction:(TMPayment*)payment;

@end

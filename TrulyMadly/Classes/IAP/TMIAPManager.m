//
//  TMIAPManager.m
//  TrulyMadly
//
//  Created by Ankit on 14/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMIAPManager.h"
#import "TMError.h"
#import "TMLog.h"
#import "TMAnalytics.h"
#import "TMUserSession.h"
#import "TMDataStore.h"
#import "TMPayment.h"
#import "TMEnums.h"

typedef void (^TMSaprkPackageRequestCompletionHandler)(NSArray *products, NSError *error);


@interface TMIAPManager () <SKPaymentTransactionObserver,SKProductsRequestDelegate>

@property(nonatomic,strong)SKProductsRequest *productsRequest;
@property(nonatomic,strong)TMSparkManager *sparkManager;
@property(nonatomic,strong)TMPayment *currentPayment;
@property(nonatomic,copy)TMSaprkPackageRequestCompletionHandler completionHandler;

@property(nonatomic,assign)BOOL isPaymentInitaitedByUserInProgress;

-(void)validateProductIdentifiers:(NSArray *)productIdentifiers response:(TMSaprkPackageRequestCompletionHandler)handlerl;

@end


@implementation TMIAPManager

+(instancetype)sharedManager {
    static TMIAPManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate,^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

-(TMSparkManager*)sparkManager {
    if(!_sparkManager) {
        _sparkManager = [[TMSparkManager alloc] init];
    }
    return _sparkManager;
}
-(BOOL)canConnect {
    BOOL status = [self.sparkManager isNetworkReachable];
    return status;
}
-(void)addIAPTransactionObserver {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}
-(void)getSparkPackages:(void(^)(NSArray *packageList, NSString *sparkVideoUrl,TMError *error))responseBlock {
    if(self.sparkManager.isNetworkReachable) {
        [self.sparkManager getAllPackages:^(NSArray *packageList, NSString *sparkVideoUrl,TMError *error) {
            if(packageList.count) {
                responseBlock(packageList, sparkVideoUrl, nil);
            }
            else {
                responseBlock(nil,nil,error);
            }
        }];
    }
    else {
        TMError *error = [[TMError alloc] initWithNoNetworkErrorCode];
        responseBlock(nil,nil,error);
    }
}
-(void)getCommanalityStringWithMatch:(NSString*)matchId response:(void(^)(NSArray* commonalityList,NSString* errorMessage))commonalityBlock; {
    [self.sparkManager getCommonStringWithSpark:matchId response:^(NSArray *commonList, TMError *error) {
        if(commonList.count) {
            commonalityBlock(commonList,nil);
        }
        else {
            NSArray *commonList = @[@"Got a quirky story about yourself, we\'re sure she\'d love to hear it!"];
            commonalityBlock(commonList,error.errorMessage);
        }
    }];
}
-(void)buyPackage:(TMPackageType)packageType withProductIdentifier:(NSString *)productIdentifier
withDelegateHandler:(id<TMIAPManagerDelegate>)delegate {

    self.delegate = delegate;
    self.isPaymentInitaitedByUserInProgress = TRUE;
    BOOL isNewPayment = TRUE;
    
    NSArray *transactions = [[SKPaymentQueue defaultQueue] transactions];
    for (SKPaymentTransaction *transaction in transactions) {
        if((transaction.transactionState == SKPaymentTransactionStatePurchased) &&
           [transaction.payment.productIdentifier isEqualToString:productIdentifier]) {
            [self completeTransaction:transaction];
            isNewPayment = FALSE;
            break;
        }
    }
    if(isNewPayment) {
        self.currentPayment = [[TMPayment alloc] init];
        self.currentPayment.packageType = packageType;
        self.currentPayment.packageIdentifier = productIdentifier;
        [self validateProductIdentifiers:@[productIdentifier] response:^(NSArray *products, NSError *error) {
            if(products.count && !error) {
                SKProduct *product = products[0];
                [self buySpark:product];
            }
            else {
                NSString *errorDescription = (error) ? error.localizedDescription : @"Invalid product identifier";
                self.isPaymentInitaitedByUserInProgress = FALSE;
                self.currentPayment.paymentStatus = fail;
                [self.currentPayment setPaymentFailureReason:errorDescription];
                [self updatePaymentCompletion];
            }
        }];
    }
}


-(void)validateProductIdentifiers:(NSArray *)productIdentifiers response:(TMSaprkPackageRequestCompletionHandler)handler {
    self.currentPayment.paymentState = sktoreproductvalidation;
    self.completionHandler = handler;
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
                                          initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
    self.productsRequest = productsRequest;
    productsRequest.delegate = self;
    [productsRequest start];
}
-(void)buySpark:(SKProduct*)product {
    [self.sparkManager startSparkBuyTransactionWithProductIdentifier:product.productIdentifier
                                                       response:^(NSString *transactionIdentifier, TMError *error) {
                                                           
           if( (transactionIdentifier) && (!error) ) {
               TMLOG(@"Initiating Payment with transaction identifier:%@",transactionIdentifier);
               [TMDataStore setObject:transactionIdentifier forKey:@"com.spark.paymentinitiation"];
               SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
               payment.applicationUsername = transactionIdentifier;
               [[SKPaymentQueue defaultQueue] addPayment:payment];
           }
           else {
               self.isPaymentInitaitedByUserInProgress = FALSE;
               self.currentPayment.paymentStatus = fail;
               [self.currentPayment setPaymentFailureReason:error.errorMessage];
               [self updatePaymentCompletion];
           }
    }];
}

#pragma mark - Request Delegate / Observer
#pragma mark -

- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    if(self.completionHandler) {
        self.completionHandler(response.products,nil);
    }
}
-(void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    if(self.completionHandler) {
        self.completionHandler(nil,error);
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions{
    //[[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}
- (void)paymentQueue:(SKPaymentQueue *)queue
 updatedTransactions:(NSArray *)transactions
{
    //NSInteger index = 0;
    for (SKPaymentTransaction *transaction in transactions) {
        //TMLOG(@"####################### %ld ###################################",(long)index++);
        TMLOG(@"Transaction Identofer:%@",transaction.transactionIdentifier);
        TMLOG(@"Product Identifer:%@",transaction.payment.productIdentifier);
        TMLOG(@"Developer Payload:%@",transaction.payment.applicationUsername);
        TMLOG(@"transaction state:%ld",(long)transaction.transactionState);
        TMLOG(@"###########################################################");
        switch (transaction.transactionState) {
                
            case SKPaymentTransactionStatePurchasing:
                //[self showTransactionAsInProgress:transaction deferred:NO];
                break;
            case SKPaymentTransactionStateDeferred:
                //[self showTransactionAsInProgress:transaction deferred:YES];
                break;
            case SKPaymentTransactionStateFailed:
                //SKErrorPaymentCancelled
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                //[self restoreTransaction:transaction];
                break;
            default:
                // For debugging
                NSLog(@"Unexpected transaction state %@", @(transaction.transactionState));
                break;
        }
    }
}

-(void)failedTransaction:(SKPaymentTransaction*)transaction {
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    NSString *error = @"Unknown error";
    switch (transaction.error.code) {
        case SKErrorUnknown:
            error = @"Unknown error";
            break;
        case SKErrorClientInvalid:
            error = @"client is not allowed to issue the request";
            break;
        case SKErrorPaymentCancelled:
            error = @"user cancelled the request";
            break;
        case SKErrorPaymentInvalid:
            error = @"purchase identifier was invalid";
            break;
        case SKErrorPaymentNotAllowed:
            error = @"this device is not allowed to make the payment";
            break;
        default:
            break;
    }
    [self.currentPayment setPaymentFailureReason:error];
    [self updatePaymentCompletion];
}

-(void)completeTransaction:(SKPaymentTransaction*)transaction {
    //[self.sparkManager validateDirectlyWithApple:[receipt base64EncodedStringWithOptions:0]];
    
    TMLOG(@"=============================================================");
    TMLOG(@"Transaction Identofer:%@",transaction.transactionIdentifier);
    TMLOG(@"Product Identifer:%@",transaction.payment.productIdentifier);
    TMLOG(@"Developer Payload:%@",transaction.payment.applicationUsername);
    TMLOG(@"=============================================================");
    
    SKPayment *payment = transaction.payment;
    NSString *developerPayload = payment.applicationUsername;
    if(developerPayload == nil) {
        developerPayload = [TMDataStore retrieveObjectforKey:@"com.spark.paymentinitiation"];
    }
    
    ////
    if((developerPayload) && [developerPayload isKindOfClass:[NSString class]]) {
        [self validatePaymentTransaction:transaction withPayload:developerPayload];
    }
    else {
        //since developer payload is null or not valid, this transaction cannot be completed.
        //Hence mark as complete and track this.
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        NSString *payload = [NSString stringWithFormat:@"nullfromios:%@",[TMUserSession sharedInstance].user.userId];
        //NSLog(@"payload tracking:%@",payload);
        [[TMAnalytics sharedInstance] trackSparkEvent:@"developer_payload" status:payload eventInfo:nil];
        //if(self.purchaseCompletionHandler) {
            self.isPaymentInitaitedByUserInProgress = FALSE;
           // self.purchaseCompletionHandler(0,0,SPARK_BUY_ERRORMSG);
        //}
    }
}

-(void)validatePaymentTransaction:(SKPaymentTransaction*)transaction withPayload:(NSString*)payload {
    
    if(self.delegate) {
        [self.delegate didUpdateTransactionStatus:@"Completing Payment"];
    }

    NSString *receiptData = [self getReceiptDataForTransaction:transaction];
    NSString *appleTransactionId = transaction.transactionIdentifier;
    [self.sparkManager validateBuyTransation:receiptData
                          appleTransactionId:appleTransactionId
                             tmtransactionId:payload
                                      status:^(NSDictionary* paymentData, TMError *error) {
              
          self.isPaymentInitaitedByUserInProgress = FALSE;
          if(paymentData && !error)  {
              //mark transaction as finished
              [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
              self.currentPayment.paymentStatus = success;
              
              NSDictionary *mySelect = paymentData[@"my_select"];
              if(mySelect && [mySelect isKindOfClass:[NSDictionary class]]) {
                  [[TMUserSession sharedInstance] setMySelectStatus:mySelect];
              }
              else {
                  BOOL showChatAssist = [paymentData[@"chat_assist"] boolValue];
                  NSInteger sparkCount = [paymentData[@"sparks"] integerValue];
                  NSInteger newSparkAddCount = [paymentData[@"new_sparks_added"] integerValue];
                  
                  self.currentPayment.showChatAssist = showChatAssist;
                  self.currentPayment.sparkCount = newSparkAddCount;
                  TMUserSession *userSession = [TMUserSession sharedInstance];
                  [userSession updateAvailableSparkCount:[NSString stringWithFormat:@"%ld",(long)sparkCount]];
              }
          }
          else {
              [self.currentPayment setPaymentFailureReason:error.errorMessage];
          }
         [self updatePaymentCompletion];
    }];
}

-(NSString*)getReceiptDataForTransaction:(SKPaymentTransaction*)transaction {
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    if(!receipt) {
        receipt = transaction.transactionReceipt;
    }
    NSString *receiptString = [receipt base64EncodedStringWithOptions:0];
    return receiptString;
}

-(void)updatePaymentCompletion {
    if(self.delegate) {
        [self.delegate didFinishPaymentTransaction:self.currentPayment];
    }
}

@end


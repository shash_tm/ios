//
//  TMNotification.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNotification.h"
#import "TMDataStore.h"
#import "TMAnalytics.h"
#import "TMPushNotification.h"
#import "TMCommonUtil.h"
#import "AppVirality.h"

@implementation TMNotification

+ (void)setDeviceToken:(NSString *)token {
    [TMDataStore setObject:token forKey:@"deviceToken"];
    [TMDataStore synchronize];
}

+ (void)sendToken:(NSString *)pushStatus token:(NSString *)token {
    TMPushNotification* pushMgr = [TMPushNotification alloc];
    NSString *deviceId = [TMCommonUtil vendorId];
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys: token, @"registration_id",pushStatus, @"status",deviceId, @"device_id",nil];
    
    [AppVirality setUserDetails:@{@"pushDeviceToken":token} Oncompletion:^(BOOL success, NSError *error) {
        
    }];
    
    [self setDeviceToken:token];
    
    if ([pushMgr isNetworkReachable]) {
        // GCM status event
        NSDictionary* eventDict = @{@"screenName":@"GCM",
                                    @"eventCategory":@"GCM",
                                    @"eventAction":pushStatus,
                                    @"status":token};
        [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
        
        [pushMgr sendDeviceToken:dict with:^(BOOL status) {
            if (status) {
                [TMDataStore removeObjectforKey:@"failure"];
                if ([pushStatus isEqualToString:@"install"]) {
                    [TMDataStore setObject:[NSNumber numberWithBool:true] forKey:@"appInstall"];
                    [TMDataStore synchronize];
                }
            }else {
                [TMDataStore setObject:@"true" forKey:@"failure"];
            }
        }];
    }else {
        [TMDataStore setObject:@"true" forKey:@"failure"];
    }
}

@end

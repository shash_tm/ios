//
//  TMNotificationReciever.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 30/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMNotificationReciever : NSObject<NSCoding>

-(instancetype) initWithDictionary:(NSDictionary*)userInfo fromMoEngage:(BOOL)fromMoEngage;
-(BOOL) canShowBanner;


@property(nonatomic,strong)NSString *pushType;
@property(nonatomic,strong)NSString *url;
@property(nonatomic,assign)NSInteger senderId;
@property(nonatomic,assign)NSString *alert;
@property(nonatomic,strong)NSNumber *isAdminMessage;
@property(nonatomic,assign)BOOL showRejectToastOnPhoto;
@property(nonatomic,strong)NSString *eventId;
@property(nonatomic,strong)NSString *categoryId;
@property(nonatomic,strong)NSString *categoryTitle;

@end

//
//  TMNotification.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMNotification : NSObject

@property(nonatomic,strong)NSString *pushType;
@property(nonatomic,strong)NSString *url;

+ (void)setDeviceToken: (NSString*)token;
+ (void)sendToken: (NSString*)status token:(NSString*)token;

@end

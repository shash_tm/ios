//
//  TMNotificationReciever.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 30/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMNotificationReciever.h"
#import "TrulyMadly-Swift.h"

@implementation TMNotificationReciever


- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.pushType forKey:@"pushType"];
    [encoder encodeBool:self.showRejectToastOnPhoto forKey:@"showRejectToastOnPhoto"];
    [encoder encodeObject:self.alert forKey:@"alert"];
    [encoder encodeInteger:self.senderId forKey:@"senderId"];
    [encoder encodeObject:self.isAdminMessage forKey:@"isAdminMessage"];
    [encoder encodeObject:self.url forKey:@"url"];
    [encoder encodeObject:self.eventId forKey:@"eventId"];
    [encoder encodeObject:self.categoryId forKey:@"categoryId"];
    [encoder encodeObject:self.categoryTitle forKey:@"categoryTitle"];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    self.pushType = [aDecoder decodeObjectForKey:@"pushType"];
    self.alert = [aDecoder decodeObjectForKey:@"alert"];
    self.isAdminMessage = [aDecoder decodeObjectForKey:@"isAdminMessage"];
    self.url = [aDecoder decodeObjectForKey:@"url"];
    self.senderId = [aDecoder decodeIntegerForKey:@"senderId"];
    self.showRejectToastOnPhoto = [aDecoder decodeBoolForKey:@"showRejectToastOnPhoto"];
    self.eventId = [aDecoder decodeObjectForKey:@"eventId"];
    self.categoryId = [aDecoder decodeObjectForKey:@"categoryId"];
    self.categoryTitle = [aDecoder decodeObjectForKey:@"categoryTitle"];
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary*)userInfo fromMoEngage:(BOOL)fromMoEngage {
    self = [super init];
    if(self) {
       
        NSDictionary *dictionary = [userInfo objectForKey:@"aps"];
        if(dictionary != nil) {
           
            NSString *type = [dictionary valueForKey:@"type"];
            NSString *alert = [dictionary valueForKey:@"alert"];
            //NSLog(@"type is %@",type);
            
            if(fromMoEngage) {
                NSDictionary *appExtra = [userInfo objectForKey:@"app_extra"];
                type = [appExtra valueForKey:@"screenName"];
            }
            
            self.pushType = type;
            self.alert = alert;
            self.showRejectToastOnPhoto = false;
            
            if ([type isEqualToString:@"TRUST"]) {
                
            }
            else if([type isEqualToString:@"MESSAGE"]) {
                NSString *url = [dictionary valueForKey:@"message_url"];
                self.url = url;
                NSString *senderId = [dictionary valueForKey:@"sender_id"];
                //self.senderId = senderId;
                self.senderId = [senderId intValue];
                self.isAdminMessage = (dictionary[@"is_admin_set"] && ![dictionary[@"is_admin_set"] isKindOfClass:[NSNull class]]) ? [dictionary valueForKey:@"is_admin_set"] : false;
            }
            else if([type isEqualToString:@"WEB"]) {
                if(fromMoEngage) {
                    NSDictionary *appExtra = [userInfo objectForKey:@"app_extra"];
                    NSDictionary *screenData = [appExtra objectForKey:@"screenData"];
                    self.url = [screenData valueForKey:@"web_url"];
                }
                else {
                    NSString *url = [dictionary valueForKey:@"web_url"];
                    self.url = url;
                }
            }
            else if([type isEqualToString:@"MATCH"]) {
                NSString *url = [dictionary valueForKey:@"match_url"];
                NSString *senderId = [dictionary valueForKey:@"sender_id"];
                self.url = url;
                self.senderId = [senderId intValue];
                
            }
            else if([type isEqualToString:@"PHOTO"])
            {
                if(dictionary[@"previousLikeStatus"] && ![dictionary[@"previousLikeStatus"] isKindOfClass:[NSNull class]]) {
                    self.showRejectToastOnPhoto = [[dictionary valueForKey:@"previousLikeStatus"] boolValue];
                }else {
                    self.showRejectToastOnPhoto = false;
                }
            }
            else if ([type isEqualToString:@"EVENT"])
            {
                self.eventId = [dictionary valueForKey:@"event_id"];
            }
            else if ([type isEqualToString:@"EVENT_LIST"])
            {
                self.categoryId = [dictionary valueForKey:@"category_id"];
                self.categoryTitle = [dictionary valueForKey:@"categoryTitle"];
            }
        }
        
    }
    return self;
}

-(BOOL)canShowBanner {
    
    if([self.pushType isEqualToString:@"MESSAGE"]) {
        TMUserSession *userSession = [TMUserSession sharedInstance];
        if(userSession.isUserOnOneToOneMessaging && [userSession.currentMatchIdForOneToOneMessaging intValue] == self.senderId) {
            return false;
        }
        return true;
    }
    return true;
}

@end


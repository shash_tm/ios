//
//  TMVideoTutorialView.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@protocol TMVideoTutorialViewDelegate;


@interface TMVideoTutorialView : UIView

@property(nonatomic,weak)id<TMVideoTutorialViewDelegate>delegate;
-(instancetype)initWithFrameAndVideoURLString:(CGRect)frame URLString:(NSString *)URLString;
-(void)deinit;

@end

@protocol TMVideoTutorialViewDelegate <NSObject>

-(void)didMediaFinishPlay:(NSString *)type currentTime:(NSString *)currentTime;

@end

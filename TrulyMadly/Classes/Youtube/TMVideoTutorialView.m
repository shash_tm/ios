//
//  TMVideoTutorialView.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 05/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMVideoTutorialView.h"

@interface TMVideoTutorialView()

@property(nonatomic, strong)NSString *videoURLString;
@property(nonatomic, strong)MPMoviePlayerController *moviePlayer;
@property(nonatomic, strong)UIImageView *closeImage;
@property(nonatomic, strong)UIButton *closeButton;

@end

@implementation TMVideoTutorialView

- (instancetype) initWithFrameAndVideoURLString:(CGRect)frame URLString:(NSString *)URLString
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
        self.videoURLString = URLString;
        [self playVideo];
    }
    return self;
}

-(void)playVideo{
    CGFloat iconWidth = 30;
    CGFloat height = (9*self.bounds.size.width)/16+80;
    CGFloat yPos = (self.bounds.size.height-height)/2;
    
    UIView *playerView = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.bounds.size.width, height)];
    playerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:playerView];
    
    _closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width-30, 8, 20, 20)];
    _closeImage.image = [UIImage imageNamed:@"Cancel"];
    _closeImage.alpha = 0.0;
    [playerView addSubview:_closeImage];
    
    _closeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _closeButton.frame = CGRectMake(self.bounds.size.width-54, 10, 44, 44);
    _closeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _closeButton.alpha = 0.0;
    [_closeButton addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [playerView addSubview:_closeButton];
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake((self.bounds.size.width-iconWidth)/2, 12, iconWidth, iconWidth)];
    icon.image = [UIImage imageNamed:@"TM_scenes_blue"];
    [playerView addSubview: icon];
    
    UILabel *textLbl = [[UILabel alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(icon.frame)+10,playerView.frame.size.width,20)];
    textLbl.text = @"Introducing TM Scenes";
    textLbl.textAlignment = NSTextAlignmentCenter;
    textLbl.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    textLbl.font = [UIFont systemFontOfSize:16];
    [playerView addSubview:textLbl];
    
    self.moviePlayer = [[MPMoviePlayerController alloc]initWithContentURL:[NSURL URLWithString:self.videoURLString]];
    
    [_moviePlayer prepareToPlay];
    
    _moviePlayer.controlStyle = MPMovieControlStyleNone;
    _moviePlayer.view.frame = CGRectMake(0, CGRectGetMaxY(textLbl.frame)+8, self.frame.size.width, (9*self.frame.size.width)/16);
    _moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
    _moviePlayer.view.backgroundColor = [UIColor clearColor];
    [playerView addSubview:_moviePlayer.view];
    [_moviePlayer setFullscreen:NO animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playMovie:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:_moviePlayer];
    
    [self showCloseButton];

}

- (void)playMovie:(NSNotification *)notification {
    MPMoviePlayerController *player = notification.object;
    
    if (player.loadState & MPMovieLoadStatePlayable)
    {
        [player play];
    }
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    MPMoviePlayerController *videoplayer = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if ([videoplayer respondsToSelector:@selector(setFullscreen:animated:)])
    {
        // remove the video player from superview.
        NSString *curTime = [NSString stringWithFormat:@"%f", self.moviePlayer.currentPlaybackTime];
        [videoplayer.view removeFromSuperview];
        videoplayer = nil;
        self.moviePlayer = nil;
        if(self.delegate){
            [self.delegate didMediaFinishPlay:@"stopped" currentTime:curTime];
        }
    }
}

-(void)deinit {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(self.moviePlayer){
        [self.moviePlayer.view removeFromSuperview];
        self.moviePlayer = nil;
    }
}

-(void)showCloseButton {
    [UIView animateWithDuration:0.3
                          delay: 5.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _closeImage.alpha = 1;
                         _closeButton.alpha = 1;
                         
                     }
                     completion:^(BOOL finished){
                    }];
}

-(void)closeButtonPressed {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(self.moviePlayer){
        NSString *curTime = [NSString stringWithFormat:@"%f", self.moviePlayer.currentPlaybackTime];
    
        [self.moviePlayer stop];
        [self deinit];
        if(self.delegate){
            [self.delegate didMediaFinishPlay:@"stopped" currentTime:curTime];
        }
    }
}

-(void)dealloc {
    //NSLog(@"dealloc called");
    [self deinit];
}
@end

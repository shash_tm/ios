//
//  TMStickerManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TMSticker.h"

@interface TMStickerManager : NSObject

+ (TMStickerManager*) sharedStickerManager;

/**
 * makes the sticker gallery API request
 * @param gallery refresh block
 * @param failure block
 */
- (void) makeStickerAPITRequestWithGalleryRefreshBlock:(void(^)()) galleryRefreshBlock stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock allGalleriesRefreshBlock:(void(^)()) allGalleriesRefreshBlock failureBlock:(void(^)())failureBlock;


/**
 * returns all the sticker galleries
 * @return sticker galleries
 */
- (NSArray*) getAllStickerGalleries;


/**
 * gets the image for a given sticker
 * @param sticker object
 * @return image object
 */
- (UIImage*) getImageForSticker:(TMSticker*) sticker;


/**
 * gets the hd image for a given sticker
 * @param sticker object
 * @param image object
 */
- (UIImage*) getHDImageForSticker:(TMSticker*) sticker;


/**
 * gets all the stickers for a given gallery ID
 * @param gallery ID
 * @return sticker array
 */
- (NSArray*) getAllStickerForStickerGalleryID:(NSString*) stickerGalleryID;


/**
 * gets all stickers sorted on the basis of timestamp
 * @return stickers sorted on the basis of timestamp
 */
- (NSArray*) getStickerBasedOnTimestamp;


/**
 * updates the timestamp for a given sticker
 * @param sticker ID
 * @param gallery ID
 * @return update status
 */
- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID;


/**
 * stores the hd image for sticker
 * @param sticker object
 * @param sticker refresh block
 */
- (void) storeHDImageForSticker:(TMSticker* ) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock;


@end
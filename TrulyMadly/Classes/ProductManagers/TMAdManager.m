//
//  TMAdManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 06/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMAdManager.h"
#import "TMUserSession.h"
#import "TMDataStore+TMAdAddtions.h"
#import "TMAnalytics.h"
#import "TMUser.h"
#import "TMLog.h"

//SeventyNine Ad Changes
#import "SNADViewManager.h"
#import "SdkMediation.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>

#define SNPUBLISHER_ID @"5204"
#define SNINTERSTITIALZONEID @"20406"
#define SNPROFILEZONEID @"20408"
#define SNNATIVEZONEID @"20410"

@interface TMAdManager ()

//property for current SNAd type
@property (nonatomic, assign) SeventyNineAdType currentAdType;
//property for date formatter
@property (nonatomic, strong) NSDateFormatter* dateFormatter;
//property for event tracking
@property (nonatomic, strong) NSMutableDictionary* eventDictionary;
//property for native ad fetched from SeventyNine
@property (nonatomic, strong) NSMutableArray* nativeAdArray;

@property (nonatomic, assign) BOOL isInterstitialFromSeventyNine;

@end


@implementation TMAdManager

//Initializes SeventyNine SDK
- (void) initializeSeventyNineSDK {
    if([TMDataStore retrieveBoolforKey:TMAD_ENABLED]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SNADViewManager setRunTimeConfigration:@{SNpublisherId:SNPUBLISHER_ID}];
            [self addObserverForSNSdkInitialization];
        });
    }
}

- (void) addObserverForSNSdkInitialization {
    if(![TMDataStore retrieveBoolforKey:TMAD_SDK_INITIALIZED]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sdkInitialized) name:kSNADSDKReadyNotificationName object:nil];
    }
}

- (void)sdkInitialized {
    [TMDataStore setBool:true forKey:TMAD_SDK_INITIALIZED];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//Initializes the user flags
- (instancetype) init {
    if (self = [super init]) {
        
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_ENABLED]) {
            [TMDataStore setObjectWithUserIDForObject:@"false" key:TMAD_ENABLED];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_INTERSTITIAL_GLOBAL_COUNT]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_GLOBAL_COUNT];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_INTERSTITIAL_TIME_INTERVAL]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_INTERSTITIAL_TIME_INTERVAL];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_INTERSTITIAL_MATCHES_COUNT]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_INTERSTITIAL_MATCHES_COUNT];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_NATIVE_TIME_INTERVAL]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_NATIVE_TIME_INTERVAL];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_PROFILE_ENABLED]) {
            [TMDataStore setObjectWithUserIDForObject:@"false" key:TMAD_PROFILE_ENABLED];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_PROFILE_FINAL_POSITION]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:-1] key:TMAD_PROFILE_FINAL_POSITION];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_PROFILE_TIME_INTERVAL]) {
            [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithDouble:-1] key:TMAD_PROFILE_TIME_INTERVAL];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_PROFILE_REPEAT]) {
            [TMDataStore setObjectWithUserIDForObject:@"false" key:TMAD_PROFILE_REPEAT];
        }
        if (![TMDataStore containsObjectWithUserIDForKey:TMAD_MEDIATION_ENABLED]) {
            [TMDataStore setObjectWithUserIDForObject:@"false" key:TMAD_MEDIATION_ENABLED];
        }
        
        //set the age
        if ([TMUserSession sharedInstance].user.age > 0) {
            [SNADViewManager setAge:[TMUserSession sharedInstance].user.age];
        }
        //set the gender
        if ([TMUserSession sharedInstance].user.gender) {
            if ([[TMUserSession sharedInstance].user isUserFemale]) {
                [SNADViewManager setGender:SeventynineUserGenderFemale];
            } else {
                [SNADViewManager setGender:SeventynineUserGenderMale];
            }
        }
    }
    return self;
}

//Checks if the interstitial ad can be shown
-(BOOL) canShowInterstitial {
    
    [self checkForInterstitialCounterReset];
    
    BOOL isInterstitialAdToBeShown = NO;
    
    if([TMDataStore retrieveBoolforKey:TMAD_ENABLED] && [TMDataStore retrieveBoolforKey:TMAD_SDK_INITIALIZED]) {
        NSDate *lastTimestampObject = [TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_LAST_TIMESTAMP];
        double interstitialTimeInterval;
        int currentInterstitialCount, matchesInterstitialCounter;
        currentInterstitialCount = [TMDataStore containsObjectWithUserIDForKey:TMAD_INTERSTITIAL_CURRENT_COUNT]?([[TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_CURRENT_COUNT] intValue]):0;
        matchesInterstitialCounter = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_MATCHES_COUNT] intValue];
        interstitialTimeInterval = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_TIME_INTERVAL] doubleValue];
            
        if(interstitialTimeInterval >= 0 && fabs([[NSDate date] timeIntervalSinceDate:lastTimestampObject]) >= interstitialTimeInterval && currentInterstitialCount <= matchesInterstitialCounter) {
                isInterstitialAdToBeShown = YES;
        }
    }
    return isInterstitialAdToBeShown;
}

//Initializes the flags for profile ad.
- (void) initializeProfileAdFlags {
    if([TMDataStore retrieveBoolforKey:TMAD_ENABLED] && [TMDataStore retrieveBoolforKey:TMAD_PROFILE_ENABLED]) {
         [TMDataStore setBool:NO forKey:TMAD_PROFILE_VIEWED];
    }
}

//Updates the counter when the profiles are viewed.
- (void) updateCounterForProfileAd {
    
    int noOfProfilesViewed = [TMDataStore containsObjectWithUserIDForKey:TMAD_PROFILE_VIEWED_COUNT] ? [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_VIEWED_COUNT] intValue] : 0;
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:noOfProfilesViewed + 1] key:TMAD_PROFILE_VIEWED_COUNT];
}

//Checks if the profile ad can be viewed.
- (BOOL) canShowProfileAd {
    
    BOOL isProfileAdToBeShown = NO;
    if([TMDataStore retrieveBoolforKey:TMAD_ENABLED] && [TMDataStore retrieveBoolforKey:TMAD_PROFILE_ENABLED] && [TMDataStore retrieveBoolforKey:TMAD_SDK_INITIALIZED]) {
        int profileViewed, profileAdPosition;
        [self updateCounterForProfileAd];
        profileViewed = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_VIEWED_COUNT] intValue];
        profileAdPosition = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_FINAL_POSITION] intValue];
        if(profileAdPosition > 0 && profileViewed >= profileAdPosition - 1) {
            if(![TMDataStore retrieveBoolforKey:TMAD_PROFILE_VIEWED] ) {
                NSDate *firstTimestamp = [TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_FIRST_TIMESTAMP];
                double profileAdTimeInterval = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_TIME_INTERVAL] doubleValue];
                if(profileAdTimeInterval > 0 && fabs([[NSDate date] timeIntervalSinceDate:firstTimestamp]) >= profileAdTimeInterval) {
                    isProfileAdToBeShown = YES;
                    [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_PROFILE_FIRST_TIMESTAMP];
                }
            }else if ([TMDataStore retrieveBoolforKey:TMAD_PROFILE_REPEAT]) {
                isProfileAdToBeShown = YES;
                [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_PROFILE_FIRST_TIMESTAMP];
            }
        }
    }
    return isProfileAdToBeShown;
}

//Checks if native ad can be viewed.
- (BOOL) canShowNativeAd {
    BOOL isNativeAdToBeShown = NO;
    if([TMDataStore retrieveBoolforKey:TMAD_ENABLED] && [TMDataStore retrieveBoolforKey:TMAD_SDK_INITIALIZED]) {
        NSDate *firstTimestamp = [TMDataStore retrieveObjectWithUserIDForKey:TMAD_NATIVE_LAST_TIMESTAMP];
        if(firstTimestamp == nil) {
            [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_NATIVE_LAST_TIMESTAMP];
            isNativeAdToBeShown = YES;
        }else {
            double nativeAdTimeInterval = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_NATIVE_TIME_INTERVAL] doubleValue];
            if(nativeAdTimeInterval > 0 && fabs([[NSDate date] timeIntervalSinceDate:firstTimestamp]) >= nativeAdTimeInterval) {
                isNativeAdToBeShown = YES;
            }
        }
    }
    return isNativeAdToBeShown;
}

//Returns the zone id for the ad passed as an argument.
- (NSString *) getZoneIdForAdType:(SeventyNineAdType) currentAdType {
    
    NSString *zoneId;
    NSDictionary *eventDetails;
    self.currentAdType = currentAdType;
    
    if(self.currentAdType == InterstitialAd) {
        zoneId = SNINTERSTITIALZONEID;
    } else if (self.currentAdType == NativeAd) {
        zoneId = SNNATIVEZONEID;
        eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_made", @"from_79" : [NSNumber numberWithBool:YES]};
        [self trackAdEvents:eventDetails];
    } else if (self.currentAdType == ProfileAd) {
        zoneId = SNPROFILEZONEID;
        eventDetails = @{@"category": @"matches", @"adType": @"profile_ads", @"status": @"ad_request_made", @"from_79" : [NSNumber numberWithBool:YES]};
        [self trackAdEvents:eventDetails];
    }
    
    return zoneId;

}

//Notifies the status of the availability of Profile Ad.
- (void) showProfileAdWithResponse:(void (^)(BOOL status))responseBlock{
    
    BOOL canProfileAdBeShown = [self canShowProfileAd];
    NSString *zoneId = nil;
    if(canProfileAdBeShown) {
        zoneId = [self getZoneIdForAdType:ProfileAd];
        [SNADViewManager isAdReadyWithZoneId:zoneId audioAd:NO nativeAdView:NO goForMediation:NO completion:^(BOOL isAdReady) {
            NSDictionary *eventDetails;
            if(isAdReady) {
               eventDetails = @{@"category": @"matches", @"adType": @"profile_ads", @"status": @"ad_request_success", @"from_79" : [NSNumber numberWithBool:YES]};
            }else {
               eventDetails = @{@"category": @"matches", @"adType": @"profile_ads", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:YES]};
            }
            [self trackAdEvents:eventDetails];
            responseBlock(isAdReady);
        }];
    }else {
        responseBlock(false);
    }
    
}

- (void)resetProfileViewedCounter {
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_PROFILE_VIEWED_COUNT];
}

//Resets the profile ad position if the matches available are less than the specified position.
- (void)checkForProfileAdPositionReset:(NSInteger) matchCount {
    int profileAdPosition = [[TMDataStore retrieveObjectWithUserIDForKey:TMAD_PROFILE_FINAL_POSITION] intValue];
    if(profileAdPosition > matchCount) {
        [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInteger:matchCount] key:TMAD_PROFILE_FINAL_POSITION];
    }
}

//Displays the interstitial ad if it is ready to be displayed.

- (void) showInterstitialAd {
    
    self.isInterstitialFromSeventyNine = YES;
    BOOL canInterstitialAdBeShown = [self canShowInterstitial];
    NSDictionary *adFeasible;
    
    if (canInterstitialAdBeShown) {
        NSString *zoneId = [self getZoneIdForAdType:InterstitialAd];
        BOOL isMediationEnabled = [TMDataStore retrieveBoolforKey:TMAD_MEDIATION_ENABLED];
        adFeasible = @{@"ad_feasible": [NSNumber numberWithBool:YES]};
        [SNADViewManager isAdReadyWithZoneId:zoneId audioAd:NO nativeAdView:NO  goForMediation:isMediationEnabled completion:^(BOOL isAdReady) {
            NSDictionary *eventDetails, *seventyNineAdRequestDetails, *seventyNineAdFailureDetails;
            seventyNineAdRequestDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_request_made", @"from_79" : [NSNumber numberWithBool:YES]};
            [self trackAdEvents:seventyNineAdRequestDetails];
            if(isAdReady || isMediationEnabled ) {
                if(!isAdReady) {
                    self.isInterstitialFromSeventyNine = NO;
                    seventyNineAdFailureDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:YES]};
                    [self trackAdEvents:seventyNineAdFailureDetails];
                    eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_request_made", @"from_79" : [NSNumber numberWithBool:NO]};
                    [self trackAdEvents:eventDetails];
                }
                NSDictionary *optionsDict = @{kSNADVariable_ZoneIdKey: zoneId, kSNADVariable_DelegateKey:self};
                [SNADViewManager showFullScreenAdInViewController:self.adDelegate priority:nil options:optionsDict targetCondition:nil];
            }else {
                eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:YES]};
                [self trackAdEvents:eventDetails];
            }
        }];
    }else {
        adFeasible = @{@"ad_feasible": [NSNumber numberWithBool:NO]};
    }
    [self trackFeasibleInterstitialAd:adFeasible];
}

- (void) trackFeasibleInterstitialAd:(NSDictionary *) eventInfo{
    if(eventInfo != nil) {
        self.eventDictionary = [[NSMutableDictionary alloc] init];
        [self.eventDictionary setObject:eventInfo forKey:@"event_info"];
        [self.eventDictionary setObject:@"matches" forKey:@"eventCategory"];
        [self.eventDictionary setObject:@"matches_last" forKey:@"status"];
        [self.eventDictionary setObject:@"matches_last" forKey:@"label"];
        [self.eventDictionary setObject:@"ad_interstitial" forKey:@"eventAction"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDictionary];
    }
}

//Checks if native ad is ready to be displayed and then fetches the data from the server.
- (void) getNativeAd {
    BOOL canNativeAdBeShown = [self canShowNativeAd];
    if(canNativeAdBeShown) {
        NSString *zoneId = [self getZoneIdForAdType:NativeAd];
        [self fetchNativeAdData:zoneId];
    }
}

//Fetches the ad data from the server.
- (void) fetchNativeAdData:(NSString *)zoneId {
    
    if(![self.nativeAdArray count]) {
        NSArray *nativeAdData = [SNADViewManager getCustomNativeAdDataWithZoneId:zoneId];
        self.nativeAdArray = [NSMutableArray arrayWithArray:nativeAdData];
    }
    if([self.nativeAdArray count]) {
        NSMutableDictionary *adData = nil;
        NSDictionary *adDataJson = [self.nativeAdArray objectAtIndex:0];
        [self.nativeAdArray removeObjectAtIndex:0];
        adData = [[NSMutableDictionary alloc] init];
        [adData setObject:[adDataJson objectForKey:@"title"] forKey:@"title"];
        [adData setObject:[adDataJson objectForKey:@"desc"] forKey:@"description"];
        [adData setObject:[adDataJson objectForKey:@"bannerContent"] forKey:@"iconUrl"];
        [adData setObject:[adDataJson objectForKey:@"bannerid"] forKey:@"bannerId"];
        [adData setObject:[adDataJson objectForKey:@"bannerContent_md5"] forKey:@"clickUrl"];
        [adData setObject:[NSNumber numberWithBool:false] forKey:@"mediationAd"];
        [self showNativeAdWithDataDictionary:adData seventyNineAd:YES];
    }else if([TMDataStore retrieveBoolforKey:TMAD_MEDIATION_ENABLED]) {
        NSDictionary *eventDetails, *optionsDict, *seventyNineAdFailureDetails;
        UIView *adView;
        
        seventyNineAdFailureDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:YES]};
        [self trackAdEvents:seventyNineAdFailureDetails];
        optionsDict = @{kSNADVariable_ZoneIdKey: SNNATIVEZONEID, kSNADVariable_DelegateKey:self, kSNADVariable_CustomNativeAdKey: @"YES"};
        adView = [[UIView alloc] initWithFrame:CGRectZero];
        eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_made", @"from_79" : [NSNumber numberWithBool:NO]};
        [self trackAdEvents:eventDetails];
        [SNADViewManager showNativeOrBannerAdInView:adView isBanner:NO priority:nil viewController:self.adDelegate options:optionsDict targetCondition:nil];
    }else {
        NSDictionary *eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:YES]};
        [self trackAdEvents:eventDetails];
    }
}

- (void) registerNativeAdForImpressions:(NSDictionary *) adData {
    NSString *bannerId, *bannerContent;
    bannerId = [adData objectForKey:@"bannerId"];
    bannerContent = [adData objectForKey:@"clickUrl"];
    [SNADViewManager OnNativeAdImpressionWithZoneId:SNNATIVEZONEID bannerId:bannerId bannerContentMD5:bannerContent];
}

//Updates the profile ad flags after the ad is displayed.
- (void) updateProfileAdFlags {
    
    NSDictionary *eventDetails = @{@"category": @"matches", @"adType": @"profile_ads", @"status": @"ad_shown", @"from_79" : [NSNumber numberWithBool:YES]};
    [self trackAdEvents:eventDetails];
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:0] key:TMAD_PROFILE_VIEWED_COUNT];
    if( ![TMDataStore retrieveBoolforKey:TMAD_PROFILE_VIEWED] ) {
        [TMDataStore setBool:YES forKey:TMAD_PROFILE_VIEWED];
    }
}

//Displays the profile ad in the given view.
- (void) showProfileAdInView:(UIView *) adView viewController:(UIViewController *) viewController {
    
    NSDictionary *optionsDict = @{kSNADVariable_ZoneIdKey: SNPROFILEZONEID, kSNADVariable_DelegateKey:self, kSNADVariable_CrossButtonKey :@"NO"};
    [SNADViewManager showNativeOrBannerAdInView:adView isBanner:NO priority:nil viewController:viewController options:optionsDict targetCondition:nil];
    [self updateProfileAdFlags];
}

//Updates the interstitial flag after the ad has been diaplayed.
-(void) updateInterstitialAdFlags {
    
    [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_INTERSTITIAL_LAST_TIMESTAMP];
    int totalInterstitialCount = [TMDataStore containsObjectWithUserIDForKey:TMAD_INTERSTITIAL_CURRENT_COUNT]?([[TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_CURRENT_COUNT] intValue]):0;
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithInt:totalInterstitialCount + 1] key:TMAD_INTERSTITIAL_CURRENT_COUNT];
    
}

//Notifies the class with the available ad data dictionary.
- (void) showNativeAdWithDataDictionary:(NSDictionary *) adData seventyNineAd:(BOOL)isMediationAd{
    NSDictionary *eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_success", @"from_79" : [NSNumber numberWithBool:isMediationAd]};
    [self trackAdEvents:eventDetails];
    [self.adDelegate showNativeAd:adData];
    [TMDataStore setObjectWithUserIDForObject:[NSDate date] key:TMAD_NATIVE_LAST_TIMESTAMP];
}

//Checks if the interstitial counter needs to be reset.
-(void) checkForInterstitialCounterReset {
    
    NSDate *currentDate = [NSDate date];
    BOOL isResetRequired = NO;
    if ([TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_LAST_TIMESTAMP]) {
        
        NSDate* lastTimeStampDateObject = [TMDataStore retrieveObjectWithUserIDForKey:TMAD_INTERSTITIAL_LAST_TIMESTAMP];
        if (fabs([currentDate timeIntervalSinceDate:lastTimeStampDateObject]) >= 24*60*60) {
            isResetRequired = YES;
        }else {
            //check for dates
            [self.dateFormatter setDateFormat:@"dd"];
            NSString* presentDateString = [self.dateFormatter stringFromDate:currentDate];
            NSString* lastTimeStampDateString = [self.dateFormatter stringFromDate:lastTimeStampDateObject];
            if (![presentDateString isEqualToString:lastTimeStampDateString]) {
                isResetRequired = YES;
            }
        }
    }
    if(isResetRequired) {
         [TMDataStore resetInterstitialCounterFlags];
    }
    
}

//Tracks the ad for various events.
- (void) trackAdEvents:(NSDictionary*) eventDetails {
    
    NSString *category, *adType, *status;
    NSDictionary *eventInfo;
    category = [eventDetails objectForKey:@"category"];
    adType = [eventDetails objectForKey:@"adType"];
    status = [eventDetails objectForKey:@"status"];
    if([eventDetails objectForKey:@"from_79"] != nil) {
       eventInfo = [NSDictionary dictionaryWithObject:[eventDetails objectForKey:@"from_79"] forKey:@"from_79"];
    }
    if(category != nil && adType != nil && status != nil && eventInfo != nil) {
        self.eventDictionary = [[NSMutableDictionary alloc] init];
        [self.eventDictionary setObject:status forKey:@"label"];
        [self.eventDictionary setObject:status forKey:@"status"];
        [self.eventDictionary setObject:category forKey:@"eventCategory"];
        [self.eventDictionary setObject:adType forKey:@"eventAction"];
        [self.eventDictionary setObject:eventInfo forKey:@"event_info"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDictionary];
    }
    
}

//Redirects to the link associated with the SeventyNine ad after it is clicked.
- (void) nativeAdClickedWithParam:(NSDictionary*) paramDictionary {
    NSString *bannerContent, *bannerId;
    bannerContent = [paramDictionary objectForKey:@"clickUrl"];
    bannerId = [paramDictionary objectForKey:@"bannerId"];
    [SNADViewManager nativeClickEventWithOrderId:@"" zoneId:SNNATIVEZONEID bannerId:bannerId bannerContentMD5:bannerContent];
    NSDictionary *eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_clicked", @"from_79" : [NSNumber numberWithBool:YES]};
    [self trackAdEvents:eventDetails];
}

//Registers facebook native ad with the view object.
- (void) registerNativeAdWithViewObject:(NSDictionary *) paramDictionary view:(UIView *)adView viewController:(UIViewController *) viewController {
    FBNativeAd *nativeAd = [paramDictionary objectForKey:@"clickUrl"];
    [nativeAd registerViewForInteraction:adView withViewController:viewController];
}

//Unregisters facebook native ad registered with the view object.
- (void) unregisterNativeAd:(NSDictionary *) paramDictionary{
    FBNativeAd *nativeAd = [paramDictionary objectForKey:@"clickUrl"];
    [nativeAd unregisterView];
}

- (void) trackNativeAdShownEvent:(BOOL)isSeventyNineAd {
    NSDictionary *eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_shown", @"from_79" : [NSNumber numberWithBool:isSeventyNineAd]};
    [self trackAdEvents:eventDetails];
}

- (void) trackProfileAdClosedEvent {
    NSDictionary *eventDetails = @{@"category": @"matches", @"adType": @"profile_ads", @"status": @"ad_closed", @"from_79" : [NSNumber numberWithBool:YES]};
    [self trackAdEvents:eventDetails];
}

- (void) adWillPresent:(NSString *)adId adType:(SeventynineAdType)adType {
    NSDictionary *eventDetails;
    if(self.currentAdType == InterstitialAd) {
        eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_request_success", @"from_79" : [NSNumber numberWithBool:self.isInterstitialFromSeventyNine]};
        [self trackAdEvents:eventDetails];
    }
    
}

- (void) adDidPresent:(NSString *)adId adType:(SeventynineAdType)adType {
    NSDictionary *eventDetails;
    if(self.currentAdType == InterstitialAd) {
        [self updateInterstitialAdFlags];
        eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_shown", @"from_79" : [NSNumber numberWithBool:self.isInterstitialFromSeventyNine]};
        [self trackAdEvents:eventDetails];
    }
    
}
- (void)adViewDidFailWithError:(NSError *)error {
    NSDictionary *eventDetails;
    if(self.currentAdType == NativeAd) {
        eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:NO]};
    }else if(self.currentAdType == InterstitialAd) {
        eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:NO]};
    }
    if(eventDetails != nil) {
       [self trackAdEvents:eventDetails]; 
    }
    
}

- (void)adDidClickedWithAdId:(NSString *)adId adType:(SeventynineAdType)adType {
    NSDictionary *eventDetails;
    if(self.currentAdType == InterstitialAd) {
        eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_clicked", @"from_79" : [NSNumber numberWithBool:self.isInterstitialFromSeventyNine]};
    }else if(self.currentAdType == ProfileAd) {
        eventDetails = @{@"category": @"matches", @"adType": @"profile_ads", @"status": @"ad_clicked", @"from_79" : [NSNumber numberWithBool:YES]};
    }else if(self.currentAdType == NativeAd) {
        eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_clicked", @"from_79" : [NSNumber numberWithBool:NO]};
    }
    if(eventDetails != nil) {
       [self trackAdEvents:eventDetails];
    }
    
}
- (void)adDidClose:(NSString *)adId adType:(SeventynineAdType)adType {
    NSDictionary *eventDetails;
    if(self.currentAdType == InterstitialAd) {
        eventDetails = @{@"category": @"matches", @"adType": @"ad_interstitial", @"status": @"ad_closed", @"from_79" : [NSNumber numberWithBool:self.isInterstitialFromSeventyNine]};
    }else if(self.currentAdType == NativeAd) {
        eventDetails = @{@"category": @"conversation_list", @"adType": @"ad_native", @"status": @"ad_request_failure", @"from_79" : [NSNumber numberWithBool:NO]};
    }
    if(eventDetails != nil) {
       [self trackAdEvents:eventDetails];
    }
    
}

- (void)adDidGetTrulyMadlyHashTag:(NSString *)hashTag {
    if(self.currentAdType == ProfileAd) {
        NSArray *adProfileLikes;
        if(hashTag == nil) {
            adProfileLikes = [NSArray arrayWithObjects:@"#Sponsored", @"#Advertisment", nil];
        }else {
            adProfileLikes = [hashTag componentsSeparatedByString:@","];
        }
        [self.adDelegate hashTagForProfileAd:adProfileLikes];
    }
}


- (NSDateFormatter*) dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    return _dateFormatter;
}

#pragma mark - Method to nullify variables

- (void) deinit {
    //remove the date formatter object
    
    //SeventyNine Ad Changes
    self.dateFormatter = nil;
    self.nativeAdArray = nil;
}

#pragma mark - dealloc

- (void) dealloc {
    [self deinit];
}

@end

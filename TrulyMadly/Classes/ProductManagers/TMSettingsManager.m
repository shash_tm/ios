//
//  TMSettingsManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMSettingsManager.h"
#import "TrulyMadly-Swift.h"
#import "TMLog.h"

@implementation TMSettingsManager

-(void)updateProfileDiscovery:(NSDictionary*)queryString with:(void(^)(BOOL status, TMError *error))settingBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_PROFILE_DISCOVERY_API];
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                settingBlock(TRUE,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                settingBlock(FALSE,error);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            settingBlock(FALSE,tmerror);
        }
    }];

}

-(void)deleteUserProfile:(NSDictionary*)queryString with:(void(^)(BOOL status, TMError *error))deleteBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_DELETE_API];
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                deleteBlock(TRUE,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                deleteBlock(FALSE,error);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = [response error];
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                error.errorMessage = msg;
                deleteBlock(FALSE,error);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            deleteBlock(FALSE,tmerror);
        }
    }];
}

@end

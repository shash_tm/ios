//
//  TMLoginManager.h
//  TrulyMadly
//
//  Created by Ankit Jain on 04/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMBaseManager.h"

@class TMUser;
@class TMError;

typedef void (^UserDataResponseBlock)(NSDictionary *response, TMError *error);
typedef void (^LoginBlock)(NSDictionary *response, TMError *error);
typedef void (^LogoutBlock)(NSString *status, TMError *error);
typedef void (^ActivateBlock)(NSString *status, TMError *error);
//typedef void (^ForgotPwdBlock)(BOOL status, TMError *error);

@interface TMUserDataManager : TMBaseManager

-(void)fetchBasicDataFromURLString:(NSString*)basicURLString withResponse:(void(^)(NSDictionary* reponseData,TMError* error))responseBlock;
-(void)getUserDataWithResponse:(UserDataResponseBlock)responseBlock;
-(void)validateLogin:(NSDictionary*)paramDict with:(LoginBlock)loginBlock;
-(void)logout:(NSDictionary*)queryString with:(LogoutBlock)logoutBlock;
-(void)activateUser:(NSDictionary*)queryString with:(ActivateBlock)activateBlock;
-(void)forgotPassword:(NSDictionary*)queryString with:(void(^)(BOOL status, TMError *error))forgotPassword;

@end

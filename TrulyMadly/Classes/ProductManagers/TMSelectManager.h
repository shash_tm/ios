//
//  TMSelectManager.h
//  TrulyMadly
//
//  Created by Ankit on 01/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;

@interface TMSelectManager : TMBaseManager

-(void)getQuizData:(void(^)(NSArray* quizData,TMError* error))responseBlock;
-(void)getQuizReportWithMatch:(NSInteger)matchId response:(void(^)(NSDictionary* quizReport,TMError* error))responseBlock;
-(void)saveQuizAnswer:(NSDictionary*)quizAnswer response:(void(^)(BOOL result,TMError *error))responseBlock;
-(void)getSelectPackages:(void(^)(NSArray *packages,TMError *error))responseBlock;
-(void)buyFreeSelectPackage:(void(^)(TMError* error))responseBlock;
-(void)deleteFreeTrialPackage:(void(^)(void))responseBlock;

@end

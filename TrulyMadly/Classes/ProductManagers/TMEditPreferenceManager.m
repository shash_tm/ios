//
//  TMEditPreferenceManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMEditPreferenceManager.h"
#import "TrulyMadly-Swift.h"
#import "TMAppResponseCachingManager.h"
#import "TMEditPreference.h"

@implementation TMEditPreferenceManager

-(void)getUserPreferenceDataWithResponse:(void(^)(TMEditPreference *editPreference ,TMError *error))statusBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EDITPREFERENCE_RELATIVEPATH];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"login_mobile"] = @"true";
    params[@"app"] = @"iOS";
    [request setPostRequestParameters:params];
    
    BOOL isCachePresent = false;
    //caching to be done here
    if ([TMAppResponseCachingManager containsCachedResponseForURL:request.urlString]) {
        isCachePresent = TRUE;
        NSDictionary* editPrefData = [TMAppResponseCachingManager getCachedResponseForURL:request.urlString];
        statusBlock([[TMEditPreference alloc] initWithEditPreferenceData:editPrefData],nil);
        
        //add the hash and tstamp value to the request dict
        NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
        NSString* tStampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
        
        if (hashValue && [hashValue isValidObject]) {
            NSMutableDictionary* headerInfoMutableDict = [params mutableCopy];
            [headerInfoMutableDict setObject:hashValue forKey:@"hash"];
            if (tStampValue && [tStampValue isValidObject]) {
                [headerInfoMutableDict setObject:tStampValue forKey:@"tstamp"];
            }
            params = [headerInfoMutableDict mutableCopy];
        }
    }
    
    [request setPostRequestParameters:params];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                NSDictionary *responseData = [response response];
                TMEditPreference *editPreference = [[TMEditPreference alloc] initWithEditPreferenceData: responseData];
                statusBlock(editPreference,nil);
                
                NSString* hashValue = [responseData valueForKey:@"hash"];
                
                NSString* tstampValue = [responseData valueForKey:@"tstamp"];
                
                //update the cache info here
                [TMAppResponseCachingManager setCachedResponse:responseData forURL:request.urlString hash:hashValue tstamp:tstampValue];
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                statusBlock(nil,tmError);
            }
        }
        else {
            //error
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            self.trackEventDictionary = nil;
            [self.trackEventDictionary removeAllObjects];
            if(!isCachePresent) {
                TMError *tmError = [[TMError alloc] initWithError:error];
                statusBlock(nil,tmError);
            }
        }
    }];
}

-(void)saveData:(NSDictionary*)editPrefParams with:(void(^)(NSString *success, TMError *error))saveBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EDITPREFERENCE_SAVE_RELATIVEPATH];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    [params addEntriesFromDictionary:editPrefParams];
    params[@"login_mobile"] = @"true";
    params[@"app"] = @"iOS";
    [request setPostRequestParameters:params];
    
    NSDate* startDate = [NSDate date];
    
    //delete the cached response
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_EDITPREFERENCE_RELATIVEPATH];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                saveBlock(@"success",nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                saveBlock(nil,tmError);
            }
        }
        else {
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            //error
            TMError *tmError = [[TMError alloc] initWithError:error];
            saveBlock(nil,tmError);
        }
    }];
}

- (BOOL) isCachedResponsePresent {
    
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_EDITPREFERENCE_RELATIVEPATH]) {
        return YES;
    }
    return NO;
}

@end

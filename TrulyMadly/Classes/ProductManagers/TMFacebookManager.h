//
//  TMFacebookManager.h
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
@class TMError;

typedef void(^PermissionBlock)(NSArray *permissionList, TMError *error);
typedef void(^FbPhotoAlbumBlock)(NSArray *photoAlbums, TMError *error);

@interface TMFacebookManager : TMBaseManager

-(void)fetchFacebookAlbumList:(FbPhotoAlbumBlock)photoAlbum;
-(void)fetchPhotosFromAlbum:(NSString*)photoId withResponse:(FbPhotoAlbumBlock)photoBlock;
-(void)fetchFBPermissions:(PermissionBlock)permissionBlock;
@end

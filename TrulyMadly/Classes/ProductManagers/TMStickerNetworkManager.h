//
//  TMStickerNetworkManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
#import "TMError.h"
#import "TMSticker.h"

@interface TMStickerNetworkManager : TMBaseManager

+ (TMStickerNetworkManager*) getSharedNetworkManager;


/**
 * makes sticker API request
 * @param success block
 * @param failure block
 */
- (void) makeStickerAPIRequestWithSuccessBlock:(void(^)(NSDictionary* responseDictionary)) successBlock failureBlock:(void(^)(TMError* error)) failureBlock;


/**
 * gets the image data for given path
 * @param imageRelative URL
 * @return image data
 */
- (NSData*) getImageDataForRelativePathURL:(NSString*) imageRelativeURL;

@end

//
//  TMQuizManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 20/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
#import "TMQuizController.h"


@class TMError;
@class TMQuizCacheManager;

typedef void (^Block)(NSDictionary *response, TMError *error);

typedef void (^successBlock) (NSURLSessionDataTask *task, id responseObject);

typedef void(^failureBlock) (NSURLSessionDataTask *task, NSError *error);

@interface TMQuizManager : TMBaseManager

- (instancetype) initWithQuizController:(TMQuizController*) quizController;
-(void)getAllAvailableQuizData;
- (void) getPlayedQuizesForMatchID:(NSString*) matchID;
-(void)fetchQuizFromServer:(NSDictionary*)queryString with:(Block)block;
-(NSArray*)fetchQuizFromDBForLatestVersion:(BOOL) isQuizForLatestVersion;
- (void) saveQuizWithID:(NSString*) quizID withMatchID:(NSString*) matchID withQuestionAnswerDictionary:(NSDictionary*) answerDict;
- (void) decideQuizActionFromChatForQuizID:(NSString*) quizID matchID:(NSString*) matchID;
- (void) decideQuizActionForQuizID:(NSString*) quizID matchID:(NSString*) matchID;
- (void) updateQuizNewStatus:(BOOL) isNewQuiz forQuizID:(NSString*) quizID;
- (void) getPlayedQuizFlareForMatchID;


@end

//
//  TMEditProfile.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMEditProfile.h"
#import "TrulyMadly-Swift.h"
#import "TMAppResponseCachingManager.h"
#import "NSDictionary+JSONString.h"

@implementation TMEditProfile

-(void)initCall:(NSDictionary*)queryString with:(void(^)(NSDictionary *response ,TMError *error))responseBlock; {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_EDITPROFILE_RELATIVEPATH];
    
    BOOL isCachePresent = false;
    //caching to be done here
    if ([TMAppResponseCachingManager containsCachedResponseForURL:request.urlString]) {
        isCachePresent = true;
        //update the UI with the old cached response
        responseBlock([TMAppResponseCachingManager getCachedResponseForURL:request.urlString],nil);
        
        //add the hash & tstamp value to the request dict
        NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
        
        NSString* tstampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
        
        if (hashValue && [hashValue isValidObject]) {
        
            NSMutableDictionary* mutableQueryDict = [queryString mutableCopy];
            
            [mutableQueryDict setObject:hashValue forKey:@"hash"];
            
            if (tstampValue && [tstampValue isValidObject]) {
                [mutableQueryDict setObject:tstampValue forKey:@"tstamp"];    
            }
            
            queryString = [mutableQueryDict copy];
        }
    }
    
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                NSDictionary *responseData = [response response];
                responseBlock(responseData,nil);
                
                //cache the response
                NSString* hashValue = [responseData valueForKey:@"hash"];
                NSString* tstampValue = [responseData valueForKey:@"tstamp"];
                //cache the updated response
                [TMAppResponseCachingManager setCachedResponse:responseData forURL:request.urlString hash:hashValue tstamp:tstampValue];
                
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                responseBlock(nil,tmError);
            }
        }
        else {
            
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            //error
            if(!isCachePresent) {
                TMError *tmError = [[TMError alloc] initWithError:error];
                responseBlock(nil,tmError);
            }
        }
    }];
}

-(void)update:(NSDictionary*)queryString with:(void(^)(NSString *response ,TMError *error))updateBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_UPDATE_RELATIVEPATH];
    
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_EDITPROFILE_RELATIVEPATH];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                updateBlock(@"save",nil);
                
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                // logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                updateBlock(nil,tmError);
            }
        }
        else {
            //error
            
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmError = [[TMError alloc] initWithError:error];
            updateBlock(nil,tmError);
        }
    }];
}

- (BOOL) isCachedResponsePresent {
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_EDITPROFILE_RELATIVEPATH]) {
        return YES;
    }
    return NO;
}

-(void)updateBadWordList {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:@"https://sheetsu.com/apis/40d75ed2/column/bad_word_list"];
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"TMBadWordList" ofType:@"json"];
                    NSString *temp = [responseObject jsonStringWithPrettyPrint:YES];
                    [temp writeToFile:file atomically:YES encoding:NSUTF8StringEncoding error:nil];
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    
                }];
    
}

@end

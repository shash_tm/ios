//
//  TMTrustBuilderManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 15/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;
@class TMTrustBuilder;
@class TMResponse;

//typedef void (^TrustBuilderBlock)(TMTrustBuilder *tbData,TMError *error);
//typedef void (^FacebookBlock)(NSDictionary *response,TMError *error);
//typedef void (^PhoneBlock)(NSString *success,TMError *error);
//typedef void (^PhoneStatus)(NSDictionary *response, TMError *error);
//typedef void (^IDBlock)(NSString *status, TMError *error);

@interface TMTrustBuilderManager : TMBaseManager

-(void)getTrustBuilderData:(NSDictionary*)queryString with:(void(^)(TMTrustBuilder *tbData,TMError *error))trustbuilderBlock;

-(void)verifyFacebook:(NSDictionary*)queryString with:(void(^)(NSDictionary *response,TMError *error))facebookBlock;

-(void)uploadDocument:(NSData*)imageData params:(NSDictionary*)params with:(void(^)(NSString *status, TMError *error))idBlock;

-(void)submitPhone:(NSDictionary*)queryString with:(void(^)(NSString *success,TMError *error))phoneBlock;

-(void)phoneStatus:(NSDictionary*)queryString with:(void(^)(NSDictionary *response, TMError *error))phoneStatus;

-(void)sendIDPassword:(NSDictionary*)queryString with:(void(^)(NSString *status, TMError *error))idBlock;

- (BOOL) isCachedResponsePresent;

@end

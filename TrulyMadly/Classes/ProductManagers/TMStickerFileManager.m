//
//  TMStickerFileManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerFileManager.h"
#include <UIKit/UIKit.h>

@implementation TMStickerFileManager

- (BOOL) storeStickerIfRequiredWithStickerInfo:(TMSticker*) sticker withImageData:(NSData*) imageData forImageFormat:(stickerImageFormat) stickerImageFormat
{
    switch (stickerImageFormat) {
        case imageFormatThumbnail:
        {
            if (![self isFilePresentAtPath:sticker.thumbnailFilePathURL] || ![[UIImage alloc] initWithData:[self getFileAtPath:sticker.thumbnailFilePathURL]]) {
                //store the file
                [self writeFileWithData:imageData toPath:sticker.thumbnailFilePathURL];
                //[self writeWebPFileWithData:imageData toPath:sticker.filePathURL];
                return YES;
            }
        }
            break;
        case imageFormatHD:
        {
            if (![self isFilePresentAtPath:sticker.hdFilePathURL] || ![UIImage imageWithContentsOfFile:[self getStickerHDImageFilePathForSticker:sticker]]) {
                //store the file
                [self writeFileWithData:imageData toPath:sticker.hdFilePathURL];
                //[self writeWebPFileWithData:imageData toPath:sticker.hdFilePathURL];
                return YES;
            }
        }
            break;
            
        default:
            break;
    }
    return NO;
}


- (NSString*) getStickerFilePathForSticker:(TMSticker*) sticker
{
    if (sticker && sticker.thumbnailFilePathURL) {
        NSString* stickerFilePath = [[self getDocumentsDirectoryPath] stringByAppendingPathComponent:sticker.thumbnailFilePathURL];
        return stickerFilePath;
    }
    return nil;
}

- (NSString*) getStickerHDImageFilePathForSticker:(TMSticker*) sticker
{
    if (sticker && sticker.thumbnailFilePathURL) {
        NSString* stickerFilePath = [[self getDocumentsDirectoryPath] stringByAppendingPathComponent:sticker.hdFilePathURL];
        return stickerFilePath;
    }
    return nil;
}


@end

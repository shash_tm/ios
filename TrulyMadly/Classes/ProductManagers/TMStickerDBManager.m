//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerDBManager.h"
#import "NSObject+TMAdditions.h"
#import "TMDataStore+TMAdAddtions.h"
#import "TMLog.h"

@implementation TMStickerDBManager

#define MSG_CONV_DB_DIRECTORY_PATH @"tm/db"
#define MSG_CONV_DB_FILENAME @"chat.db"

#define TMCHAT_STICKERS_TABLE @"stickers"
#define TMCHAT_STICKERS_BACKUP_TABLE @"stickersBackup"


#define STICKER_TIMESTAMP_COUNT 12

#define STICKER_GENDER_COMMON 0
#define STICKER_GENDER_FEMALE 1
#define STICKER_GENDER_MALE   2


- (instancetype) init {
    
    self = [super initWithDatabaseDirectoryPath:MSG_CONV_DB_DIRECTORY_PATH databaseFile:MSG_CONV_DB_FILENAME];
    
    if (self) {
        [self configureStickerDBTable];
        return self;
        
    }
    return nil;
}

- (void) configureStickerDBTable
{
    FMDatabase* db = [self databaseInstance];
    
    [db open];
    
    //[self updateDBVersion];
    
    NSString* query = nil;
    
    BOOL isStickerTableExist = [db tableExists:TMCHAT_STICKERS_TABLE];
    
    BOOL isQueryExecuted;
    
    if (!isStickerTableExist) {
        TMLOG(@"Sticker Table Does Not Exist");
        
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"combined_id\" TEXT PRIMARY KEY, \"gallery_id\" INTEGER NOT NULL, \"sticker_id\" INTEGER NOT NULL, \"active\" INTEGER NOT NULL, \"updated_in_version\" INTEGER NOT NULL, \"timestamp\" INTEGER NOT NULL,  \"gender\" INTEGER NOT NULL,  \"gallery_position\" INTEGER,  \"sticker_position\" INTEGER)",TMCHAT_STICKERS_TABLE];
        
        isQueryExecuted = [db executeUpdate:query];
        if (!isQueryExecuted) {
            TMLOG(@"Error in creating table \"chatmessages\":%@ %@",db.lastError,db.lastErrorMessage);
        }
        
        //create the backup table
        
        BOOL isStickerBackupTableExist = [db tableExists:TMCHAT_STICKERS_BACKUP_TABLE];
        
        if (!isStickerBackupTableExist) {
            TMLOG(@"Sticker Table Does Not Exist");
            
            query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"combined_id\" TEXT PRIMARY KEY, \"gallery_id\" INTEGER NOT NULL, \"sticker_id\" INTEGER NOT NULL, \"active\" INTEGER NOT NULL, \"updated_in_version\" INTEGER NOT NULL, \"timestamp\" INTEGER NOT NULL,  \"gender\" INTEGER NOT NULL,  \"gallery_position\" INTEGER,  \"sticker_position\" INTEGER)",TMCHAT_STICKERS_BACKUP_TABLE];
            
            isQueryExecuted = [db executeUpdate:query];
            if (!isQueryExecuted) {
                TMLOG(@"Error in creating table \"stickers backup table\":%@ %@",db.lastError,db.lastErrorMessage);
            }
        }
    }
    else {
        //alter the existing table
        //add the combined_id column
        
        BOOL isCombinedIDColumnExists = [db columnExists:@"combined_id" inTableWithName:TMCHAT_STICKERS_TABLE];
        //        if (!isCombinedIDColumnExists) {
        //            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN combined_id TEXT DEFAULT NULL;",TMCHAT_STICKERS_TABLE];
        //            isQueryExecuted = [db executeUpdate:query];
        //
        //            if (!isQueryExecuted) {
        //                TMLOG(@"Error in adding column combined_id");
        //            }
        //        }
        
        BOOL isGalleryPositionColumnExists = [db columnExists:@"gallery_position" inTableWithName:TMCHAT_STICKERS_TABLE];
        //        if (!isGalleryPositionColumnExists) {
        //            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN gallery_position INTEGER;",TMCHAT_STICKERS_TABLE];
        //            isQueryExecuted = [db executeUpdate:query];
        //
        //            if (!isQueryExecuted) {
        //                TMLOG(@"Error in adding column combined_id");
        //            }
        //        }
        
        BOOL isStickerPositionColumnExists = [db columnExists:@"sticker_position" inTableWithName:TMCHAT_STICKERS_TABLE];
        //        if (!isStickerPositionColumnExists) {
        //            query = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN sticker_position INTEGER;",TMCHAT_STICKERS_TABLE];
        //            isQueryExecuted = [db executeUpdate:query];
        //
        //            if (!isQueryExecuted) {
        //                TMLOG(@"Error in adding column combined_id");
        //            }
        //        }
        
        if (!isCombinedIDColumnExists || !isGalleryPositionColumnExists || !isStickerPositionColumnExists) {
            //create backup table
            //copy data from main table to backup table
            //drop main table
            //create main table
            //copy data from backup table to main table
            //delete all data from backup table
            
            
            
            //create the backup table
            
            BOOL isStickerBackupTableExist = [db tableExists:TMCHAT_STICKERS_BACKUP_TABLE];
            
            if (!isStickerBackupTableExist) {
                TMLOG(@"Sticker Table Does Not Exist");
                
                query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"combined_id\" TEXT PRIMARY KEY, \"gallery_id\" INTEGER NOT NULL, \"sticker_id\" INTEGER NOT NULL, \"active\" INTEGER NOT NULL, \"updated_in_version\" INTEGER NOT NULL, \"timestamp\" INTEGER NOT NULL,  \"gender\" INTEGER NOT NULL,  \"gallery_position\" INTEGER,  \"sticker_position\" INTEGER)",TMCHAT_STICKERS_BACKUP_TABLE];
                
                isQueryExecuted = [db executeUpdate:query];
                if (!isQueryExecuted) {
                    TMLOG(@"Error in creating table \"stickers backup table\":%@ %@",db.lastError,db.lastErrorMessage);
                }
            }
            
            //copy data from main table to backup table
            
            NSString* query = nil;
            
            query = [NSString stringWithFormat:@"INSERT INTO \"%@\" (gallery_id,sticker_id,active,updated_in_version,timestamp,gender)SELECT gallery_id,sticker_id,active,updated_in_version,timestamp,gender FROM \"%@\"",TMCHAT_STICKERS_BACKUP_TABLE,TMCHAT_STICKERS_TABLE];
            
            BOOL isQueryExecuted = [db executeUpdate:query];
            
            if (isQueryExecuted) {
                //drop main table
                NSString *query = [NSString stringWithFormat:@"DROP TABLE %@",TMCHAT_STICKERS_TABLE];
                BOOL isQueryExecuted = [db executeUpdate:query];
                
                if (isQueryExecuted) {
                    //create the main table
                    query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"combined_id\" TEXT PRIMARY KEY, \"gallery_id\" INTEGER NOT NULL, \"sticker_id\" INTEGER NOT NULL, \"active\" INTEGER NOT NULL, \"updated_in_version\" INTEGER NOT NULL, \"timestamp\" INTEGER NOT NULL,  \"gender\" INTEGER NOT NULL,  \"gallery_position\" INTEGER,  \"sticker_position\" INTEGER)",TMCHAT_STICKERS_TABLE];
                    
                    isQueryExecuted = [db executeUpdate:query];
                    
                    if (isQueryExecuted) {
                        //copy data from backup table to main table
                        
                        NSString* query = nil;
                        
                        query = [NSString stringWithFormat:@"INSERT INTO \"%@\" (gallery_id,sticker_id,active,updated_in_version,timestamp,gender)SELECT gallery_id,sticker_id,active,updated_in_version,timestamp,gender FROM \"%@\"",TMCHAT_STICKERS_TABLE,TMCHAT_STICKERS_BACKUP_TABLE];
                        
                        BOOL isQueryExecuted = [db executeUpdate:query];
                        
                        if (isQueryExecuted) {
                            
                            //delete all data from backup table
                            isQueryExecuted = [self deleteAllStickersFromBackupStickerTable];
                            
                            if (isQueryExecuted) {
                                //db migration successfully done by dropping
                                TMLOG(@"db migration successfully done by dropping");
                            }
                        }
                    }
                }
            }
        }
    }
    [db close];
}

- (void) exchangeBackupAndMainStickerTablesForReferenceTimestamp:(NSTimeInterval) referenceTimestamp userGender:(stickerGender) userGender {
    
    NSArray* timeStampStickers = [self getStickersBasedOnTimestampWithRefrenceTimestamp:referenceTimestamp userGender:userGender];
    
    //update the timestamp value in backup table
    for (int index = 0; index < timeStampStickers.count; index++) {
        if ([[timeStampStickers objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            NSDictionary* timeStampDictionary = [timeStampStickers objectAtIndex:index];
            [self updateTimestampValueInBackUpTableForStickerID:[timeStampDictionary valueForKey:@"sticker_id"] galleryID:[timeStampDictionary valueForKey:@"gallery_id"] updatedTimestamp:[timeStampDictionary valueForKey:@"timestamp"]];
        }
    }
    
    //delete all stickers from main table
    [self deleteAllStickers];
    
    //copy data from backup table to main table
    
    FMDatabase* db = [self databaseInstance];
    
    [db open];
    
    NSString* query = nil;
    
    query = [NSString stringWithFormat:@"INSERT INTO \"%@\" (combined_id,gallery_id,sticker_id,active,updated_in_version,timestamp,gender,gallery_position,sticker_position)SELECT combined_id,gallery_id,sticker_id,active,updated_in_version,timestamp,gender,gallery_position,sticker_position FROM \"%@\"",TMCHAT_STICKERS_TABLE,TMCHAT_STICKERS_BACKUP_TABLE];
    
    BOOL isQueryExecuted = [db executeUpdate:query];
    
    [db close];
    
    if (!isQueryExecuted) {
        TMLOG(@"Error in copying data from sticker backup table to main table :: %@ :: %@",db.lastError,db.lastErrorMessage);
    }
    else {
        //delete the backup table
        [self deleteAllStickersFromBackupStickerTable];
    }
    
}


- (void) updateStickerInfo:(NSDictionary*) stickerInfoDictionary withGender:(int) stickerGender andGalleryID:(NSString*) galleryID galleryPosition:(NSString*) galleryPosition stickerPosition:(NSString*) stickerPosition
{
    NSString* stickerID = ([stickerInfoDictionary valueForKey:@"id"] && [[stickerInfoDictionary valueForKey:@"id"] isValidObject])?[stickerInfoDictionary valueForKey:@"id"]:nil;
    NSString* genderString = [NSString stringWithFormat:@"%d",stickerGender];
    
    //store the base timestamp
    NSString* baseTimeIntervalString = [NSString stringWithFormat:@"%d",0];
    
    
    if (stickerID && [stickerID isValidObject] && galleryID && [galleryID isValidObject]) {
        BOOL doesStickerExist = NO;
        
        NSDictionary* stickerDictionary = [self getStickerWithStickerID:stickerID galleryID:galleryID];
        
        if (stickerDictionary && [stickerDictionary valueForKey:@"sticker_id"]) {
            doesStickerExist = YES;
        }
        
        if (doesStickerExist) {
            
            baseTimeIntervalString = [stickerDictionary valueForKey:@"timestamp"];
            
            [self deleteStickerForGalleryID:galleryID stickerID:stickerID];
        }
        
        FMDatabase* db = [self databaseInstance];
        
        [db open];
        
        NSString* combinedID = [NSString stringWithFormat:@"%@_%@",galleryID,stickerID];
        
        NSString* query = [NSString stringWithFormat:@"INSERT INTO %@ (combined_id,gallery_id, sticker_id, active, updated_in_version,timestamp,gender,gallery_position,sticker_position) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",TMCHAT_STICKERS_BACKUP_TABLE,combinedID,galleryID,stickerID,@"",@"",baseTimeIntervalString,genderString,galleryPosition,stickerPosition];
        BOOL insertStatus = [self executeUpdateQuery:query withDB:db];
        if (!insertStatus) {
            //NSLog(@"Inserting query error for the table quiz %@ %@",db.lastError,db.lastErrorMessage);
        }
        [db close];
    }
}


- (NSArray*) getAllStickerGalleryIDsForGender:(stickerGender) userGender
{
    NSMutableArray* galleryArray;
    
    galleryArray = [[self getAllStickerGalleryIDsFromMainTableForGender:userGender] mutableCopy];
    
    if (!galleryArray) {
        galleryArray = [[self getAllStickerGalleryIDsFromBackupTableForGender:userGender] mutableCopy];
    }
    else {
        NSArray* backUpDBArray = [self getAllStickerGalleryIDsFromBackupTableForGender:userGender];
        if (backUpDBArray && backUpDBArray.count) {
            
            [galleryArray addObjectsFromArray:backUpDBArray];
            
            galleryArray = [[[[NSOrderedSet alloc] initWithArray:galleryArray] array] mutableCopy];
        }
    }
    return [galleryArray copy];
}

- (NSArray*) getAllStickerGalleryIDsFromBackupTable
{
    //make query for fetching specific gallery id
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT DISTINCT gallery_id from \"%@\" ORDER BY CAST(gallery_id AS INTEGER) DESC",TMCHAT_STICKERS_BACKUP_TABLE];
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSMutableArray* galleryArray = nil;
    
    while ([resultSet next]) {
        
        if (!galleryArray) {
            galleryArray = [[NSMutableArray alloc] init];
        }
        
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        [galleryArray addObject:galleryID];
    }
    
    [db close];
    
    return galleryArray;
}

- (NSArray*) getAllStickerGalleryIDsFromMainTable
{
    //make query for fetching specific gallery id
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT DISTINCT gallery_id from \"%@\" ORDER BY CAST(gallery_id AS INTEGER) DESC",TMCHAT_STICKERS_TABLE];
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSMutableArray* galleryArray = nil;
    
    while ([resultSet next]) {
        
        if (!galleryArray) {
            galleryArray = [[NSMutableArray alloc] init];
        }
        
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        [galleryArray addObject:galleryID];
    }
    
    [db close];
    
    return galleryArray;
}


- (NSArray*) getAllStickerGalleryIDsFromMainTableForGender:(stickerGender) userGender
{
    //make query for fetching specific gallery id
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    stickerGender oppositeGender = (userGender == femaleStickerGender)?maleStickerGender:femaleStickerGender;
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT DISTINCT gallery_id from \"%@\" WHERE gender!= %d ORDER BY CAST(gallery_position AS INTEGER) ASC",TMCHAT_STICKERS_TABLE,oppositeGender];
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSMutableArray* galleryArray = nil;
    
    //   NSMutableArray* tempArray= [NSMutableArray new];
    
    while ([resultSet next]) {
        
        if (!galleryArray) {
            galleryArray = [[NSMutableArray alloc] init];
        }
        
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        [galleryArray addObject:galleryID];
        // [tempArray addObject:[resultSet stringForColumn:@"gallery_position"]];
    }
    
    [db close];
    
    return galleryArray;
}

- (NSArray*) getAllStickerGalleryIDsFromBackupTableForGender:(stickerGender) userGender
{
    //make query for fetching specific gallery id
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    stickerGender oppositeGender = (userGender == femaleStickerGender)?maleStickerGender:femaleStickerGender;
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT DISTINCT gallery_id from \"%@\" WHERE gender!= %d ORDER BY CAST(sticker_position AS INTEGER) ASC",TMCHAT_STICKERS_BACKUP_TABLE,oppositeGender];
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSMutableArray* galleryArray = nil;
    
    //  NSMutableArray* tempArray= [NSMutableArray new];
    
    while ([resultSet next]) {
        
        if (!galleryArray) {
            galleryArray = [[NSMutableArray alloc] init];
        }
        
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        [galleryArray addObject:galleryID];
        // [tempArray addObject:[resultSet stringForColumn:@"gallery_position"]];
    }
    
    [db close];
    
    return galleryArray;
}

- (NSArray*) getStickerGalleryForGalleryID:(NSString*) galleryID
{
    NSMutableArray* galleryArray = [[self getStickerGalleryFromMainTableForGalleryID:galleryID] mutableCopy];
    
    if (!galleryArray) {
        galleryArray = [[self getStickerGalleryFromBackupTableForGalleryID:galleryID] mutableCopy];
    }
    else {
        NSArray* backupDBArray = [[self getStickerGalleryFromBackupTableForGalleryID:galleryID] mutableCopy];
        if (backupDBArray && backupDBArray.count) {
            
            for (int index = 0; index < backupDBArray.count; index++) {
                if ([[backupDBArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary* valueDictionary = (NSDictionary*)[backupDBArray objectAtIndex:index];
                    if ([valueDictionary valueForKey:@"combined_id"] && [[valueDictionary valueForKey:@"combined_id"] isKindOfClass:[NSString class]]) {
                        if (![self isStickerWithCombinedID:[valueDictionary valueForKey:@"combined_id"] presentInArray:galleryArray]) {
                            [galleryArray addObject:valueDictionary];
                        }
                    }
                }
            }
        }
    }
    return [galleryArray copy];
}

- (BOOL) isStickerWithCombinedID:(NSString*) combinedID presentInArray:(NSArray*) array {
    if (combinedID) {
        for (int index = 0; index < array.count; index++) {
            if ([[array objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                NSDictionary* valueDictionary = (NSDictionary*)[array objectAtIndex:index];
                if ([valueDictionary valueForKey:@"combined_id"] && [[valueDictionary valueForKey:@"combined_id"] isEqualToString:combinedID]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (NSArray*) getStickerGalleryFromMainTableForGalleryID:(NSString*) galleryID
{
    if (galleryID) {
        FMDatabase* db = [self databaseInstance];
        [db open];
        
        NSString* queryString = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE gallery_id=\"%@\" AND sticker_id!=\"%@\" ORDER BY sticker_position ASC",TMCHAT_STICKERS_TABLE,galleryID,galleryID];
        FMResultSet* resultSet = [db executeQuery:queryString];
        
        NSMutableArray* stickerInfoArray;
        
        NSDictionary* stickerInfoDictionary;
        
        while ([resultSet next]) {
            NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
            NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
            NSString* stickerPostion = [resultSet stringForColumn:@"sticker_position"];
            //   NSString* combinedID = [resultSet stringForColumn:@"combined_id"];
            
            stickerInfoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:galleryID,@"gallery_id",stickerID,@"sticker_id",stickerPostion,@"sticker_position",nil];
            
            if (!stickerInfoArray) {
                stickerInfoArray = [[NSMutableArray alloc]init];
            }
            [stickerInfoArray addObject:stickerInfoDictionary];
        }
        return stickerInfoArray;
    }
    return nil;
    
}

- (NSArray*) getStickerGalleryFromBackupTableForGalleryID:(NSString*) galleryID
{
    if (galleryID) {
        FMDatabase* db = [self databaseInstance];
        [db open];
        
        NSString* queryString = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE gallery_id=\"%@\" AND sticker_id!=\"%@\" ORDER BY sticker_position ASC",TMCHAT_STICKERS_BACKUP_TABLE,galleryID,galleryID];
        FMResultSet* resultSet = [db executeQuery:queryString];
        
        NSMutableArray* stickerInfoArray;
        
        NSDictionary* stickerInfoDictionary;
        
        while ([resultSet next]) {
            NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
            NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
            //   NSString* combinedID = [resultSet stringForColumn:@"combined_id"];
            
            stickerInfoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:galleryID,@"gallery_id",stickerID,@"sticker_id",nil];
            
            if (!stickerInfoArray) {
                stickerInfoArray = [[NSMutableArray alloc]init];
            }
            [stickerInfoArray addObject:stickerInfoDictionary];
        }
        return stickerInfoArray;
    }
    return nil;
}


- (NSArray*) getStickersBasedOnTimestampWithRefrenceTimestamp:(NSTimeInterval) referenceTimestampInterval userGender:(stickerGender) userGender
{
    NSMutableArray* stickerTimeStampArray = [[self getStickersFromMainTableBasedOnTimestampWithRefrenceTimestamp:referenceTimestampInterval userGender:userGender] mutableCopy];
    
    if (!stickerTimeStampArray) {
        stickerTimeStampArray = [[self getStickersFromBackupTableBasedOnTimestampWithRefrenceTimestamp:referenceTimestampInterval userGender:userGender] mutableCopy];
    }
    else {
        NSArray* backupDBArray = [self getStickersFromBackupTableBasedOnTimestampWithRefrenceTimestamp:referenceTimestampInterval userGender:userGender];
        
        for (int index = 0; index < backupDBArray.count; index++) {
            if ([[backupDBArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                NSDictionary* valueDictionary = (NSDictionary*)[backupDBArray objectAtIndex:index];
                if ([valueDictionary valueForKey:@"combined_id"] && [[valueDictionary valueForKey:@"combined_id"] isKindOfClass:[NSString class]]) {
                    if (![self isStickerWithCombinedID:[valueDictionary valueForKey:@"combined_id"] presentInArray:stickerTimeStampArray]) {
                        [stickerTimeStampArray addObject:valueDictionary];
                    }
                }
            }
        }
    }
    return [stickerTimeStampArray copy];
}


- (NSArray*) getStickersFromMainTableBasedOnTimestampWithRefrenceTimestamp:(NSTimeInterval) referenceTimestampInterval userGender:(stickerGender) userGender
{
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    stickerGender oppositeGender = (userGender == femaleStickerGender)?maleStickerGender:femaleStickerGender;
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE gender!= %d ORDER BY timestamp DESC",TMCHAT_STICKERS_TABLE,oppositeGender];
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSMutableArray* stickerTimestampArray;
    
    NSUInteger stickerCount = 0;
    
    while ([resultSet next] && stickerCount < STICKER_TIMESTAMP_COUNT) {
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
        NSString* combinedID = [resultSet stringForColumn:@"combined_id"];
        NSTimeInterval timeStampValue = [[resultSet stringForColumn:@"timestamp"] longLongValue];
        
        //add only if the timestamp has been modified
        if (timeStampValue > referenceTimestampInterval) {
            NSDictionary* stickerInfoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:galleryID,@"gallery_id",stickerID,@"sticker_id",combinedID,@"combined_id",[NSString stringWithFormat:@"%ld",(long)timeStampValue],@"timestamp", nil];
            if (!stickerTimestampArray) {
                stickerTimestampArray = [[NSMutableArray alloc]init];
            }
            [stickerTimestampArray addObject:stickerInfoDictionary];
        }
        stickerCount++;
    }
    
    [db close];
    
    return stickerTimestampArray;
}

- (NSArray*) getStickersFromBackupTableBasedOnTimestampWithRefrenceTimestamp:(NSTimeInterval) referenceTimestampInterval userGender:(stickerGender) userGender
{
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    stickerGender oppositeGender = (userGender == femaleStickerGender)?maleStickerGender:femaleStickerGender;
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE gender!= %d ORDER BY timestamp DESC",TMCHAT_STICKERS_BACKUP_TABLE,oppositeGender];
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSMutableArray* stickerTimestampArray;
    
    NSUInteger stickerCount = 0;
    
    while ([resultSet next] && stickerCount < STICKER_TIMESTAMP_COUNT) {
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
        NSTimeInterval timeStampValue = [[resultSet stringForColumn:@"timestamp"] longLongValue];
        NSString* combinedID = [resultSet stringForColumn:@"combined_id"];
        
        //add only if the timestamp has been modified
        if (timeStampValue > referenceTimestampInterval) {
            NSDictionary* stickerInfoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:galleryID,@"gallery_id",stickerID,@"sticker_id",combinedID,@"combined_id",[NSString stringWithFormat:@"%ld",(long)timeStampValue],@"timestamp", nil];
            if (!stickerTimestampArray) {
                stickerTimestampArray = [[NSMutableArray alloc]init];
            }
            [stickerTimestampArray addObject:stickerInfoDictionary];
        }
        stickerCount++;
    }
    [db close];
    
    return stickerTimestampArray;
}
- (NSDictionary*) getThumbnailStickerForGalleryID:(NSString*) galleryID
{
    NSDictionary* thumbNailStickerDictionary;
    if (galleryID) {
        thumbNailStickerDictionary = [self getThumbnailStickerFromMainTableForGalleryID:galleryID];
        
        if (!thumbNailStickerDictionary) {
            thumbNailStickerDictionary = [self getThumbnailStickerFromBackupTableForGalleryID:galleryID];
        }
    }
    return thumbNailStickerDictionary;
}

- (NSDictionary*) getThumbnailStickerFromMainTableForGalleryID:(NSString*) galleryID
{
    if (galleryID) {
        return [self getStickerFromMainTableWithStickerID:galleryID galleryID:galleryID];
    }
    return nil;
}

- (NSDictionary*) getThumbnailStickerFromBackupTableForGalleryID:(NSString*) galleryID
{
    if (galleryID) {
        return [self getStickerFromBackUpTableWithStickerID:galleryID galleryID:galleryID];
    }
    return nil;
}


- (NSDictionary*) getStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID
{
    NSDictionary* stickerInfoDictionary;
    stickerInfoDictionary = [self getStickerFromMainTableWithStickerID:stickerID galleryID:galleryID];
    if (!stickerInfoDictionary) {
        stickerInfoDictionary = [self getStickerFromBackUpTableWithStickerID:stickerID galleryID:galleryID];
    }
    return stickerInfoDictionary;
}


- (NSDictionary*) getStickerFromMainTableWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID
{
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    //@"SELECT * FROM \"%@\" WHERE matchid=%ld AND uid=\"%@\""
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE gallery_id=\"%@\" AND sticker_id=\"%@\"",TMCHAT_STICKERS_TABLE,galleryID,stickerID];
    
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSDictionary* stickerInfoDictionary;
    
    while ([resultSet next]) {
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
        NSString* timeStamp = [resultSet stringForColumn:@"timestamp"];
        NSString* galleryPosition = [resultSet stringForColumn:@"gallery_position"];
        
        stickerInfoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:galleryID,@"gallery_id",stickerID,@"sticker_id",timeStamp,@"timestamp",galleryPosition,@"gallery_position", nil];
    }
    [db close];
    
    return stickerInfoDictionary;
}

- (NSDictionary*) getStickerFromBackUpTableWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID
{
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    //@"SELECT * FROM \"%@\" WHERE matchid=%ld AND uid=\"%@\""
    
    NSString* queryString = [NSString stringWithFormat:@"SELECT * FROM \"%@\" WHERE gallery_id=\"%@\" AND sticker_id=\"%@\"",TMCHAT_STICKERS_BACKUP_TABLE,galleryID,stickerID];
    
    FMResultSet* resultSet = [db executeQuery:queryString];
    
    NSDictionary* stickerInfoDictionary;
    
    while ([resultSet next]) {
        NSString* galleryID = [resultSet stringForColumn:@"gallery_id"];
        NSString* stickerID = [resultSet stringForColumn:@"sticker_id"];
        NSString* timeStamp = [resultSet stringForColumn:@"timestamp"];
        NSString* galleryPosition = [resultSet stringForColumn:@"gallery_position"];
        
        stickerInfoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:galleryID,@"gallery_id",stickerID,@"sticker_id",timeStamp,@"timestamp",galleryPosition,@"gallery_position", nil];
    }
    [db close];
    
    return stickerInfoDictionary;
}

- (BOOL) updateTimestampValueInBackUpTableForStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID updatedTimestamp:(NSString*) updatedTimeStamp {
    if (stickerID && [stickerID isValidObject] && galleryID && [galleryID isValidObject]) {
        FMDatabase* db = [self databaseInstance];
        [db open];
        
        NSString* queryString = [NSString stringWithFormat:@"UPDATE \"%@\" SET timestamp = \"%@\" WHERE gallery_id=\"%@\" AND sticker_id=\"%@\"",TMCHAT_STICKERS_BACKUP_TABLE,updatedTimeStamp,galleryID,stickerID];
        
        BOOL updateStatus = [self executeUpdateQuery:queryString withDB:db];
        
        return updateStatus;
    }
    return NO;
}

- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID updatedTimeStamp:(NSString*) updatedTimeStamp {
    
    [self updateTimestampValueInBackUpTableForStickerID:stickerID galleryID:galleryID updatedTimestamp:updatedTimeStamp];
    
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    NSString* queryString = [NSString stringWithFormat:@"UPDATE \"%@\" SET timestamp = \"%@\" WHERE gallery_id=\"%@\" AND sticker_id=\"%@\"",TMCHAT_STICKERS_TABLE,updatedTimeStamp,galleryID,stickerID];
    
    BOOL updateStatus = [self executeUpdateQuery:queryString withDB:db];
    
    return updateStatus;
}


- (BOOL) deleteStickerForGalleryID:(NSString*) galleryID stickerID:(NSString*) stickerID {
    
    FMDatabase *db = [self databaseInstance];
    [db open];
    
    //delete the cached table
    NSString* deleteQuery = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE gallery_id = \"%@\" AND sticker_id = \"%@\"",TMCHAT_STICKERS_TABLE,galleryID,stickerID];
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    [db close];
    return deleteStatus;
}

- (BOOL) deleteAllStickers {
    return [self deleteAllStickersFromMainStickerTable];
}

- (BOOL) deleteAllStickersFromMainStickerTable {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    //delete the cached table
    NSString* deleteQuery = [NSString stringWithFormat:@"DELETE FROM \"%@\"",TMCHAT_STICKERS_TABLE];
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    [db close];
    return deleteStatus;
    
}

- (BOOL) deleteAllStickersFromBackupStickerTable {
    FMDatabase* db = [self databaseInstance];
    [db open];
    
    //delete the cached table
    NSString* deleteQuery = [NSString stringWithFormat:@"DELETE FROM \"%@\"",TMCHAT_STICKERS_BACKUP_TABLE];
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    [db close];
    return deleteStatus;
    
}



@end
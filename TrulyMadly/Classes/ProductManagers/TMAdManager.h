//
//  TMAdManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 06/11/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
#import "SNADViewManager.h"

typedef enum{
    InterstitialAd,
    NativeAd,
    ProfileAd
}SeventyNineAdType;

@protocol TMProfileAdDelegate
- (void) hashTagForProfileAd:(NSArray *) hashTags;
@end
//SeventyNine Ad protocols
@protocol TMNativeDelegate <NSObject>
- (void) showNativeAd:(NSDictionary *)adDictionary;
@end

@interface TMAdManager : TMBaseManager
//SeventyNine Ad delegate
@property (nonatomic, weak) id adDelegate;

//SeventyNine Ad Changes
- (void) initializeSeventyNineSDK;
- (void) addObserverForSNSdkInitialization;
- (BOOL) canShowInterstitial;
- (BOOL) canShowNativeAd;
- (void) showNativeAdWithDataDictionary:(NSDictionary *) adData seventyNineAd:(BOOL) isSeventyNineAd;
- (void) initializeProfileAdFlags;
- (void) checkForProfileAdPositionReset:(NSInteger) matchCount;
- (void) resetProfileViewedCounter;
- (void) showProfileAdWithResponse:(void (^)(BOOL status))responseBlock;
- (void) showInterstitialAd;
- (void) getNativeAd;
- (void) showProfileAdInView:(UIView *) adView viewController:(UIViewController *) viewController;
- (void) nativeAdClickedWithParam:(NSDictionary*) paramDictionary;
- (void) trackNativeAdShownEvent:(BOOL)isSeventyNineAd;
- (void) trackProfileAdClosedEvent;
- (void) registerNativeAdWithViewObject:(NSDictionary *) paramDictionary view:(UIView *)adView viewController:(UIViewController *) viewController;
- (void) unregisterNativeAd:(NSDictionary *) paramDictionary;
- (void) registerNativeAdForImpressions:(NSDictionary *) adData;
- (void) deinit;
@end

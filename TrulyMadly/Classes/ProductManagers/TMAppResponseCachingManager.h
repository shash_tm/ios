//
//  TMAppResponseCachingManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 08/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMAppResponseCachingManager : NSObject

+ (NSDictionary*) getCachedResponseForURL:(NSString*) url;

+ (BOOL) containsCachedResponseForURL:(NSString*) url;

+ (NSString*) getHashValueForURL:(NSString*) url;

+ (NSString*) getTStampValueForURL:(NSString*) url;

+ (BOOL) setCachedResponse:(NSDictionary*) response forURL:(NSString*) url hash:(NSString*) hash tstamp:(NSString*) tstamp;

+ (BOOL) deleteCachedDataForURL:(NSString*) url;

+ (BOOL) deleateAllCacheData;

@end

//
//  TMStickerDBManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMCacheManager.h"

typedef enum {
    commonStickerGender,
    femaleStickerGender,
    maleStickerGender
} stickerGender;

@interface TMStickerDBManager : TMCacheManager


/**
 * updates the sticker info
 * @param sticker info dictionary
 * @param gallery id
 */
- (void) updateStickerInfo:(NSDictionary*) stickerInfoDictionary withGender:(int) stickerGender andGalleryID:(NSString*) galleryID galleryPosition:(NSString*) galleryPosition stickerPosition:(NSString*) stickerPosition;


/**
 * gets sticker gallery for the given gallery ID
 * @param gallery ID
 * @return sticker array
 */
- (NSArray*) getStickerGalleryForGalleryID:(NSString*) galleryID;


/**
 * gets all the sticker gallery IDs based on the gender
 * @param user gender
 * @return sticker gallery array
 */
- (NSArray*) getAllStickerGalleryIDsForGender:(stickerGender) userGender;


/**
 * gets the stickers sorted on the basis of timestamp
 * @param reference timestamp
 * @param user gender
 * @return sorted array
 */
- (NSArray*) getStickersBasedOnTimestampWithRefrenceTimestamp:(NSTimeInterval) referenceTimestampInterval userGender:(stickerGender) userGender;


/**
 * gets the thumbnail sticker for a given gallery ID
 * @param gallery ID
 * @return sticker info
 */
- (NSDictionary*) getThumbnailStickerForGalleryID:(NSString*) galleryID;


/**
 * gets the sticker with a given sticker id and gallery id
 * @param sticker id
 * @param gallery id
 * @return sticker info
 */
- (NSDictionary*) getStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID;


/**
 * updates the timestamp for sticker with sticker id gallery id
 * @param sticker id
 * @param gallery id
 * @param updated timestamp
 * @return update status
 */
- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID updatedTimeStamp:(NSString*) updatedTimeStamp;


/**
 * delete all the stickers
 * @return delete status
 */
- (BOOL) deleteAllStickers;


/**
 * exchanges data between main table and backup table
 */
- (void) exchangeBackupAndMainStickerTablesForReferenceTimestamp:(NSTimeInterval) referenceTimestamp userGender:(stickerGender) userGender;

@end

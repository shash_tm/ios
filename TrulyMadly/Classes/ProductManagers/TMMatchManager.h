//
//  TMMatchManger.h
//  TrulyMadly
//
//  Created by Ankit Jain on 04/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMBaseManager.h"

@class TMMatch;
@class TMError;

////need to merge TMProfile and TMMatchProfile
@class TMProfile;
@class TMMatchProfile;
@class TMProfileResponse;


typedef void (^MatchesBlock)(NSDictionary *responseDictionary, TMError *error);
typedef void (^LikeBlock)(NSDictionary *mutualData, TMError *error);
typedef void (^ProfileResponseBlock)(TMProfileResponse *profileResponse, TMError *error);
typedef void (^MutualLikeBlock)(NSArray *list, TMError *error);
typedef void (^DeactivateProfileBlock)(BOOL status, TMError *error);
typedef void (^updateFBProfileBlock)(BOOL status, TMError *error);


@interface TMMatchManager : TMBaseManager

-(void)getMatchesWithParams:(NSDictionary*)params result:(MatchesBlock)matchesResultBLock;
-(void)getFBMutualConnectionsWithMatch:(NSInteger)matchId response:(void(^)(NSDictionary* data))responseBlock;
-(void)sendMatchWithUrl:(NSString*)url completionBlock:(LikeBlock)likeBlock;
-(void)getMyProfileData:(ProfileResponseBlock)profileResponseBlock;
-(void)getMutualLikeData:(MutualLikeBlock)mutualLikeBlock;
-(void)deactivateProfile:(NSString*)deactivateReason
                response:(DeactivateProfileBlock)deactivateResponseBlock;
-(void)sendInteractionWithBlockUserStatusPing:(NSString*)url;
-(void)userProfileFromUrl:(NSDictionary*)params response:(ProfileResponseBlock)profileResultBlock;
-(void)updateFBProfile:(NSString*)token
                response:(updateFBProfileBlock)updateFBProfileResponseBlock;

-(void)sendFBAccesToken:(NSString*)token completionBlock:(void(^)(BOOL isDeviceTokenExpired))completionBlock;

-(BOOL)isCachedResponsePresentForMyProfile;
-(BOOL)isCachedResponsePResentForMatchProfile:(NSString*) matchProfileURL;
-(void)deleteCacheResponseForMyProfile;

@end

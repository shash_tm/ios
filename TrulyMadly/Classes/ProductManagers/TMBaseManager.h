//
//  TMBaseManager.h
//  TrulyMadly
//
//  Created by Ankit Jain on 05/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMAPIClient.h"
#import "TMAPIConstants.h"
#import "TMEnums.h"


@class TMResponse;
@class TMRequest;

@interface TMBaseManager : NSObject

@property(nonatomic,assign)BOOL isRequestInProgress;
@property(nonatomic,assign)BOOL networkTracking;
@property(nonatomic,strong)NSMutableDictionary* trackEventDictionary;

-(void)executePOSTRequest:(TMRequest*)request
            responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock;

-(void)executePOSTRequest1:(TMRequest*)request
             responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock;

-(void)executePOSTAPI:(TMRequest*)request
              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

///////////////////////////////////////////////////////////////
-(void)executeGETRequest:(TMRequest*)request
           responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock;

-(void)executeGETRequest1:(TMRequest*)request
            responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock;

-(void)executeGETAPI:(TMRequest*)request
             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

-(NSURLSessionDataTask*)uploadData:(NSData*)imgData
      withRequest:(TMRequest*)request
    responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock;

-(NSURLSessionDataTask*)uploadDataAPI:(NSData*)imgData
                          withRequest:(TMRequest*)request
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
-(BOOL)isNetworkReachable;

@end

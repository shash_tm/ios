//
//  TMMessageManager.h
//  TrulyMadly
//
//  Created by Ankit on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMBaseManager.h"

@class TMError;
@class TMMessageResponse;

@interface TMMessageManager : TMBaseManager

-(void)fetchConversationList:(void(^)(NSArray *conversationList,TMError *error))reposneBlock;

-(void)sendMessage:(NSString*)URLString
            params:(NSDictionary*)params
      withResponse:(void(^)(NSDictionary *responseDict, TMError *error))response;

-(void)getChatMessages:(NSString*)URLString
                params:(NSDictionary*)params
              response:(void(^)(TMMessageResponse *response,TMError *tmError))response;

-(void)sendBlockUserCall:(NSString *)userId;

-(void)blockUserWithReason:(NSString *)reason withURL:(NSString*)url
                  response:(void(^)(BOOL status))responseBlock;

-(void)getShareProfileURLForMatch:(NSString*)matchId
                    responseBlock:(void(^)(NSDictionary* data))responseBlock;

-(void)deleteConversation:(NSInteger)lastMessageId
                  withURL:(NSString*)URLString
                 response:(void(^)(BOOL status))responseBlock;

-(void)getDeletedConversationListWihtResponse:(void(^)(NSArray* deletedConversationList))responseBlock;

-(void)pingForNetworkStatus:(void(^)(BOOL status))responseBlock;

-(void)doodleLinkFromURLString:(NSString*)URLString
                      response:(void(^)(NSString *doodleLink))response;

@end

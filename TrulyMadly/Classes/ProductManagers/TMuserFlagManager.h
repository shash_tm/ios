//
//  TMuserFlagManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 27/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMNotificationUpdate;

typedef void (^UserFlagBlock)(NSDictionary* dic);
typedef void (^NotificationUpdateBlock)(TMNotificationUpdate *notificationUpdate);

@interface TMuserFlagManager : TMBaseManager

-(void)getNotificationCount:(NotificationUpdateBlock)responseBlock;

-(void)userFlags:(UserFlagBlock)userFlagBlock;

@end

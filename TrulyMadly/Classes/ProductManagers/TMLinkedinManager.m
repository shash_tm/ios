//
//  TMLinkedinManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMLinkedinManager.h"
#import "TrulyMadly-Swift.h"
#import "TMAppResponseCachingManager.h"

@implementation TMLinkedinManager

-(void)sendAuthorizeCode:(NSDictionary*)paramDict with:(void(^)(BOOL flag, NSString* connections, TMError *error))authBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_LINKEDIN_AUTH];
    [request setPostRequestParameters:paramDict];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *responseData = [response response];
                NSString *conn = [responseData valueForKey:@"connections"];
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                authBlock(true, conn, nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                authBlock(false, @"", tmError);
                
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = (NSString*)[response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                authBlock(false, @"", tmError);
            }
            
        }
        else {
            //error
            
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            
            TMError *tmError = [[TMError alloc] initWithError:error];
            authBlock(false, @"", tmError);
        }
        
    }];

}

@end

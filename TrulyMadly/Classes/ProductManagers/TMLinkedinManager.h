//
//  TMLinkedinManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMBaseManager.h"

@class TMError;

//typedef void (^AuthBlock)(BOOL flag, NSString* connections, TMError *error);

@interface TMLinkedinManager : TMBaseManager

-(void)sendAuthorizeCode:(NSDictionary*)paramDict with:(void(^)(BOOL flag, NSString* connections, TMError *error))authBlock;

@end

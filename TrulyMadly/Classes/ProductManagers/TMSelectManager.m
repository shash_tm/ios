//
//  TMSelectManager.m
//  TrulyMadly
//
//  Created by Ankit on 01/11/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMSelectManager.h"
#import "TMError.h"
#import "TMRequest.h"
#import "TrulyMadly-Swift.h"
#import "TMUserSession.h"
#import "TMLog.h"

@implementation TMSelectManager

-(void)getQuizData:(void(^)(NSArray* quizData,TMError* error))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SELECT];
    NSDictionary *queryParams = @{@"action":@"get_quiz"};
    [request setPostRequestParameters:queryParams];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSString *networkRequestStatus = nil;
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                networkRequestStatus = @"success";
                NSDictionary *responseDict = [response response];
                NSString *quizId = responseDict[@"quiz_id"];
                [[TMUserSession sharedInstance] setSelectQuizId:quizId];
                NSArray *questions = responseDict[@"questions"];
                responseBlock(questions,nil);
            }
            else {
                NSString *errorMessage = ([response error]) ? [response error] : @"unknown_server_error";
                networkRequestStatus = errorMessage;
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                tmerror.errorMessage = errorMessage;
                responseBlock(nil,tmerror);
            }
        }
        else {
           TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(nil,tmerror);
        }
    }];
}

-(void)saveQuizAnswer:(NSDictionary*)quizAnswer response:(void(^)(BOOL result,TMError *error))responseBlock {
    NSString *quizId = [[TMUserSession sharedInstance] getActiveSelectQuizId];
    NSData *quizAnswerData = [NSJSONSerialization dataWithJSONObject:quizAnswer options:NSJSONWritingPrettyPrinted error:nil];
    NSString *quizAnswerString = [[NSString alloc] initWithData:quizAnswerData encoding:NSUTF8StringEncoding];
    NSDictionary* queryParams = @{@"quiz_id":quizId,@"action":@"submit_quiz",@"answers":quizAnswerString};
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SELECT];
    [request setPostRequestParameters:queryParams];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                responseBlock(TRUE,nil);
            }
            else {
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                responseBlock(FALSE,error);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(FALSE,tmerror);
        }
    }];
}
-(void)getQuizReportWithMatch:(NSInteger)matchId response:(void(^)(NSDictionary* quizReport,TMError* error))responseBlock {
    NSDictionary* queryParams = @{@"match_id":@(matchId),@"action":@"compare_quiz"};
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SELECT];
    [request setPostRequestParameters:queryParams];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *responseDict = [response response];
                responseBlock(responseDict,nil);
            }
            else {
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                responseBlock(nil,error);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(nil,tmerror);
        }
    }];
}
-(void)deleteFreeTrialPackage:(void(^)(void))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SELECT];
    NSDictionary *queryParams = @{@"action":@"delete_select"};
    [request setPostRequestParameters:queryParams];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                responseBlock();
            }
            else {
                responseBlock();
            }
        }
        else {
            responseBlock();
        }
    }];
}
-(void)getSelectPackages:(void(^)(NSArray *packages,TMError *error))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SPARK_API];
    NSDictionary *queryParams = @{@"action":@"get_packages",@"type":@"select"};
    [request setPostRequestParameters:queryParams];
        
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSString *networkRequestStatus = nil;
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                networkRequestStatus = @"success";
                NSDictionary *responseDict = [response response];
                NTMLog(@"Select packages:%@",responseDict);
                NSArray *packageList = responseDict[@"spark_packages"];
                responseBlock(packageList,nil);
            }
            else {
                NSString *errorMessage = ([response error]) ? [response error] : @"unknown_server_error";
                networkRequestStatus = errorMessage;
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                tmerror.errorMessage = errorMessage;
                responseBlock(nil,tmerror);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(nil,tmerror);
        }
    }];
}
-(void)buyFreeSelectPackage:(void(^)(TMError* error))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SELECT];
    NSDictionary *queryParams = @{@"action":@"start_free_trial"};
    [request setPostRequestParameters:queryParams];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSString *networkRequestStatus = nil;
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                networkRequestStatus = @"success";
                NSDictionary *responseDict = [response response];
                NSDictionary *mySelectData = responseDict[@"my_select"];
                [[TMUserSession sharedInstance] setMySelectStatus:mySelectData];
                responseBlock(nil);
            }
            else {
                NSString *errorMessage = ([response error]) ? [response error] : @"unknown_server_error";
                networkRequestStatus = errorMessage;
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                tmerror.errorMessage = errorMessage;
                responseBlock(tmerror);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(tmerror);
        }
    }];
}
@end

//
//  TMSparkManager.h
//  TrulyMadly
//
//  Created by Ankit on 01/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
@class TMError;

typedef void (^SuccessBlock)(NSDictionary *response ,TMError *error);
typedef void (^PackageBlock)(NSArray *packageList, NSString *sparkVideoUrl,TMError *error);
typedef void (^CommonalityBlock)(NSArray *commonList, TMError *error);


@interface TMSparkManager : TMBaseManager

-(void)fetchSparkProfilesData:(void(^)(NSArray *sparkList,TMError *error))reposneBlock;

-(void)updateSparkAction:(NSString*)action params:(NSDictionary*)params response:(void(^)(BOOL status,NSInteger senderId))responseBlock;

-(void)getCommonStringWithSpark:(NSString*)matchId response:(CommonalityBlock)commonalityBlock;

-(void)sendSparkWithParams:(NSDictionary *)queryParams with:(SuccessBlock)successBlock;

-(void)getAllPackages:(PackageBlock)packageBlock;

-(void)startSparkBuyTransactionWithProductIdentifier:(NSString*)productIdentiifer
                                       response:(void(^)(NSString *transactionIdentifier,TMError *error))responseBlock;

-(void)validateBuyTransation:(NSString*)data
          appleTransactionId:(NSString*)appleTransactionId
             tmtransactionId:(NSString*)transactionId
                      status:(void(^)(NSDictionary* paymentData, TMError *error))responseBlock;

-(void)validateDirectlyWithApple:(NSString*)receipt;

@end

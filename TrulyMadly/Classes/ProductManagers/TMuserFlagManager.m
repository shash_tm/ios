//
//  TMuserFlagManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 27/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMuserFlagManager.h"
#import "TrulyMadly-Swift.h"
#import "TMSocketSession.h"
#import "TMDataStore+TMAdAddtions.h"
#import "TMMessagingController.h"
#import "TMNotificationUpdate.h"
#import "TMLog.h"

@implementation TMuserFlagManager

-(void)userFlags:(UserFlagBlock)userFlagBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_USR_FLAG];
    NSDate* startDate = [NSDate date];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        NSInteger responseCode = response.responseCode;
        if(response) {
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                NSDictionary *responseDictionary = response.responseDictionary;
                NTMLog(@"user flag api data:%@",responseDictionary);
                [TMSocketSession setSocketConnectionParameterFromDictionary:responseDictionary[@"chat_configurations"]];
                ////
                NSNumber *allowPhotShare = responseDictionary[@"allow_photo_share"];
                if(allowPhotShare) {
                    [TMDataStore setBool:[allowPhotShare boolValue] forKey:@"com.photoshare.status"];
                }
                NSNumber *allowPhotoShareForMsTM = responseDictionary[@"allow_photo_share_on_miss_tm"];
                if(allowPhotoShareForMsTM) {
                    [TMDataStore setBool:[allowPhotoShareForMsTM boolValue] forKey:@"com.photosharetomstm.status"];
                }
                //SeventyNine Ad Flags
                if([responseDictionary objectForKey:@"matches_ads_flags"]) {
                    if([[responseDictionary objectForKey:@"matches_ads_flags"] isKindOfClass:[NSDictionary class]]) {
                        [TMDataStore updateAdFlagsWithDictionary:[responseDictionary objectForKey:@"matches_ads_flags"]];
                    }else if([[responseDictionary objectForKey:@"matches_ads_flags"] isKindOfClass:[NSArray class]]) {
                        [TMDataStore updateAdFlagsWithDictionary:[[responseDictionary objectForKey:@"matches_ads_flags"] firstObject]];
                    }
                }else {
                    [TMDataStore updateAdFlagsWithDictionary:nil];
                }
                /////curated deal icon flags
                NSDictionary* curatedDealInfoDictionary = [responseDictionary valueForKey:@"curated_deal_icon"];
                if((curatedDealInfoDictionary!= nil) && ([curatedDealInfoDictionary isKindOfClass:[NSDictionary class]])) {
                    BOOL dealIconVisibilityStatus = [curatedDealInfoDictionary[@"icon_shown"] boolValue];
                    [[TMMessagingController sharedController] setDealIconVisibilityStatus:dealIconVisibilityStatus];
                }
                
                // last seen shown flag
                if(responseDictionary[@"shouldInactivityTimeBeShown"]){
                    BOOL last_seen_status = [responseDictionary[@"shouldInactivityTimeBeShown"] boolValue];
                    [TMDataStore setBool:last_seen_status forKey:@"last_seen_status"];
                }
                
                //select flags
                NSDictionary *selectData = responseDictionary[@"select_flags"];
                [[TMUserSession sharedInstance] setSelectStatusData:selectData];
                BOOL isSelecteEnabled = [responseDictionary[@"select_active"] boolValue];
                [[TMUserSession sharedInstance] setSelectEnabled:isSelecteEnabled];
                
                //limit like flag
                
                if(responseDictionary[@"limit_likes"]) {
                    NSDictionary *data = responseDictionary[@"limit_likes"];
                    int likeLimit = [data[@"no_of_profiles"] intValue];
                    [[TMUserSession sharedInstance] setLikeLimitForUser:likeLimit];
                }
                //update quiz status
                NSDictionary* mySelectData = selectData[@"my_select"];
                BOOL selectUser = [mySelectData[@"is_tm_select"] boolValue];
                if(selectUser) {
                    NSNumber* isQuizPlayed = mySelectData[@"quiz_played"];
                    if( (isQuizPlayed) && (isQuizPlayed.boolValue == FALSE) ) {
                        [[TMUserSession sharedInstance] setSelectQuizAsStarted];
                    }
                }
                
                //scenes flag
                NSDictionary *sceneData = responseDictionary[@"scenes_ab"];
                [[TMUserSession sharedInstance] updateSceneData:sceneData];
                
                userFlagBlock(responseDictionary);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                userFlagBlock(nil);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                userFlagBlock(nil);
            }
        }
        else { //failure case
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            userFlagBlock(nil);
        }
    }];
    
}

-(void)getNotificationCount:(NotificationUpdateBlock)responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GETNOTIFICATIONCOUNT];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [request setGetRequestParams:dict];
    
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                    NSInteger responseCode = response.responseCode;
                    if(response && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                        TMNotificationUpdate *notificationUpdate = [[TMNotificationUpdate alloc] initWithResponse:response.responseDictionary];
                        responseBlock(notificationUpdate);
                    }
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    //do nothing
                }];
}


@end

//
//  TMPhotoManager1.h
//  TrulyMadly
//
//  Created by Chetan Bhardwaj on 17/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;
@class TMPhotoResponse;

typedef void (^PrefillDataBlock)(NSDictionary *user, TMError *error);
typedef void(^PhotoBlock)(TMPhotoResponse *photoResponse, TMError *error);
typedef void (^PhotoFacebookBlock)(BOOL status,TMError *error);

@interface TMPhotoManager : TMBaseManager

-(void)getPhotoData:(PhotoBlock)photoBlock;
//-(void)uploadPhoto:(NSData*)photoData;
-(NSURLSessionDataTask*)uploadPhoto:(NSData*)photoData response:(PhotoBlock)photoBlock;
-(void)deletePhotoWithId:(NSString*)photoId photoResponse:(PhotoBlock)photoBlock;
-(void)setPhotoAsProfilePicWithId:(NSString*)photoId photoResponse:(PhotoBlock)photoBlock;
-(void)uploadFacebookPhotoUrls:(NSArray*)photoUrls response:(PhotoBlock)photoBlock;
//-(void)uploadPhotoFromFacebook:(NSArray*)photoUrls response:(PhotoBlock)photoBlock;
-(NSURLSessionDataTask*)uploadPhotoWithId:(NSString*)photoId photoData:(NSData*)photoData response:(PhotoBlock)photoBlock;

-(void)verifyFacebook:(NSDictionary*)queryString with:(PhotoFacebookBlock)facebookBlock;

- (BOOL) isCachedResponsePresent;

@end


//
//  TMTrustBuilderManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 15/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMTrustBuilderManager.h"
#import "TrulyMadly-Swift.h"
#import "TMAppResponseCachingManager.h"


@implementation TMTrustBuilderManager

-(void)getTrustBuilderData:(NSDictionary*)queryString with:(void(^)(TMTrustBuilder *tbData,TMError *error))trustbuilderBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    
    BOOL isCachePresent = false;
   
    if ([TMAppResponseCachingManager getCachedResponseForURL:request.urlString]) {
        isCachePresent = true;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                //update UI with the old cached response
                NSDictionary *trustBuilderDict = [TMAppResponseCachingManager getCachedResponseForURL:request.urlString];
                trustbuilderBlock([[TMTrustBuilder alloc] initWithTrustbuilderData:trustBuilderDict],nil);
            });
        });
                //add the hash && tstamp value to the request dict
                NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
                
                NSString* tstampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
                
                if (hashValue && [hashValue isValidObject]) {
                    NSMutableDictionary* mutableHeaderParamDict = [queryString mutableCopy];
                    [mutableHeaderParamDict setObject:hashValue forKey:@"hash"];
                    if (tstampValue && [tstampValue isValidObject]) {
                        [mutableHeaderParamDict setObject:tstampValue forKey:@"tstamp"];
                    }
                    queryString = [mutableHeaderParamDict copy];
                }

    }
    
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                TMTrustBuilder *tbData = [[TMTrustBuilder alloc] initWithTrustbuilderData:response.data];
                trustbuilderBlock(tbData,nil);
                
                NSString* hashValue = [response.responseDictionary valueForKey:@"hash"];
                
                NSString* tstampValue = [response.responseDictionary valueForKey:@"tstamp"];
                
                //cache the response
                [TMAppResponseCachingManager setCachedResponse:response.data forURL:request.urlString hash:hashValue tstamp:tstampValue];
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // logout the user
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                trustbuilderBlock(nil,error);
            }
            
            
        }
        else {
            ///error
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            if(!isCachePresent) {
                TMError *tmerror = [[TMError alloc] initWithError:error];
                trustbuilderBlock(nil,tmerror);
            }
        }
        
    }];
    
}

-(void)verifyFacebook:(NSDictionary*)queryString with:(void(^)(NSDictionary *response,TMError *error))facebookBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    //delete old cache
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_TRUSTBUILDER_RELATIVEPATH];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                NSDictionary *responseData = [response response];
                //TMTrustBuilder *tbData = [[TMTrustBuilder alloc] initWithTrustbuilderData:response.data];
                facebookBlock(responseData,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // logout the user
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                facebookBlock(nil,tmerror);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // error
                NSDictionary *responseData = [response response];
                facebookBlock(responseData,nil);
            }
            
            
        }
        else {
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            //error
            TMError *tmerror = [[TMError alloc] initWithError:error];
            facebookBlock(nil,tmerror);
        }
        
    }];
    
}

-(void)uploadDocument:(NSData*)imageData params:(NSDictionary*)params with:(void(^)(NSString *status, TMError *error))idBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    [request setPostRequestParameters:params];
    
     NSDate* startDate = [NSDate date];
    
    //delete old cache
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_TRUSTBUILDER_RELATIVEPATH];
    
    [self uploadData:imageData withRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                idBlock(@"success",nil);
                
            }else if (responseCode == TMRESPONSECODE_DATASAVEERROR) {
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                tmError.errorMessage = msg;
                idBlock(nil,tmError);
            }else if (responseCode == TMRESPONSECODE_LOGOUT) {
                //logout functionality
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                idBlock(nil,tmerror);
            }
            
        }else {
            ///error
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            idBlock(nil,tmerror);
        }
    }];
    
}

-(void)submitPhone:(NSDictionary*)queryString with:(void(^)(NSString *success,TMError *error))phoneBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
               // NSDictionary *responseData = [response response];
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                phoneBlock(@"yes",nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                // logout the user
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                phoneBlock(nil,tmerror);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                // error
                NSDictionary *responseData = [response response];
                phoneBlock(responseData[@"error"],nil);
            }
        }
        else {
            [self.trackEventDictionary setObject:@"error_timeout" forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            ///error
            TMError *tmerror = [[TMError alloc] initWithError:error];
            phoneBlock(nil,tmerror);
        }
        
    }];

}

-(void)phoneStatus:(NSDictionary*)queryString with:(void(^)(NSDictionary *response, TMError *error))phoneStatus {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    [request setPostRequestParameters:queryString];
    
    //delete old cache
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_TRUSTBUILDER_RELATIVEPATH];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *responseData = [response response];
                phoneStatus(responseData,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                phoneStatus(nil,tmerror);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                // error
                NSDictionary *responseData = [response response];
                phoneStatus(responseData,nil);
            }
        }
        else {
            ///error
            TMError *tmerror = [[TMError alloc] initWithError:error];
            phoneStatus(nil,tmerror);
        }
        
    }];

}

-(void)sendIDPassword:(NSDictionary*)queryString with:(void(^)(NSString *status, TMError *error))idBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    [request setPostRequestParameters:queryString];
    
    //delete old cache
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_TRUSTBUILDER_RELATIVEPATH];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                idBlock(@"success",nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                // logout the user
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                idBlock(nil,tmerror);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                // error
                TMError *error = [[TMError alloc] init];
                idBlock(nil,error);
            }
        }
        else {
            ///error
            TMError *tmerror = [[TMError alloc] initWithError:error];
            idBlock(nil,tmerror);
        }
        
    }];
    
}

- (BOOL) isCachedResponsePresent {
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_TRUSTBUILDER_RELATIVEPATH]) {
        return YES;
    }
    return NO;
}

@end

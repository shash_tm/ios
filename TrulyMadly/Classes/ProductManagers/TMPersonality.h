//
//  TMPersonality.h
//  TrulyMadly
//
//  Created by Chetan Bhardwaj on 04/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;

@interface TMPersonality : TMBaseManager

-(void)getFavQuizListForKey:(NSString*)key withCategory:(NSString*)category
               withResponse:(void(^)(NSDictionary *response,TMError *error))responseBlock;

-(void)getFavListWithResponse:(void(^)(NSDictionary *dict,TMError *error))responseBlock;

-(void)saveFavourite:(NSDictionary*)favDic withResponse:(void(^)(BOOL status))responsBlock;

- (BOOL) isCachedResponsePresent;
@end

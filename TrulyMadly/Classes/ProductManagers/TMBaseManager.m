//
//  TMBaseManager.m
//  TrulyMadly
//
//  Created by Ankit Jain on 05/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"
#import "TrulyMadly-Swift.h"
#import "AFURLRequestSerialization.h"
#import "TMLog.h"


@implementation TMBaseManager


-(void)dealloc {
    //NSLog(@"TMBaseManager Dealloc");
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr.operationQueue cancelAllOperations];
}

-(BOOL)isNetworkReachable {
    TMAPIClient *apiClient = [TMAPIClient sharedAPIManager];
    BOOL connected = [apiClient connected];
    return connected;
}


#pragma mark POST REQUEST
#pragma mark -
-(void)executePOSTRequest:(TMRequest*)request
                        responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock {
 
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    [apiMgr POST:request.urlString
            parameters:[request httpPostRequestParameters]
            success:^(NSURLSessionDataTask *task, id responseObject) {
        
                TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                NTMLog(@"Response:%@ for URL:%@",response.responseDictionary,request.urlString);
                responseBlock(response,nil);
            }
            failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                NTMLog(@"Error:%@",error);
                responseBlock(nil,error);
            }
     ];
}
-(void)executePOSTRequest1:(TMRequest*)request
            responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    [apiMgr POST:request.urlString
      parameters:[request httpPostRequestParameters]
         success:^(NSURLSessionDataTask *task, id responseObject) {
             
             TMResponse *response = [[TMResponse alloc] initWithResponseArray:responseObject];
             //NTMLog(@"Response:%@",response.responseArray);
             responseBlock(response,nil);
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             
             //NTMLog(@"Error:%@",error);
             responseBlock(nil,error);
         }
     ];
}

-(void)executePOSTAPI:(TMRequest*)request
             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    [apiMgr POST:request.urlString
      parameters:[request httpPostRequestParameters]
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NTMLog(@"______TMBaseManager Response Description_____%@\n______________",responseObject);
             success(task,responseObject);
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NTMLog(@"Error:%@",error);
             failure(task,error);
         }
     ];
}
-(void)executePOSTAPI:(TMRequest*)request
             responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    [apiMgr POST:request.urlString
      parameters:[request httpPostRequestParameters]
         success:^(NSURLSessionDataTask *task, id responseObject) {
             
             TMResponse *response = [[TMResponse alloc] initWithResponseArray:responseObject];
             //NTMLog(@"Response:%@",response.responseArray);
             responseBlock(response,nil);
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             
             //NTMLog(@"Error:%@",error);
             responseBlock(nil,error);
         }
     ];
}


#pragma mark GET REQUEST
#pragma mark -
-(void)executeGETRequest:(TMRequest*)request
                   responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    
    [apiMgr GET:request.urlString
            parameters:[request httpGetRequestParameters]
            success:^(NSURLSessionDataTask *task, id responseObject) {
        
                NTMLog(@"Response:%@",responseObject);
                TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                
                responseBlock(response,nil);
        
            }
            failure:^(NSURLSessionDataTask *task, NSError *error) {
                NTMLog(@"Error:%@",error);
                responseBlock(nil,error);
            }
     ];
}

-(void)executeGETRequest1:(TMRequest*)request
           responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    
    [apiMgr GET:request.urlString
     parameters:[request httpGetRequestParameters]
        success:^(NSURLSessionDataTask *task, id responseObject) {
            
            //NTMLog(@"Response:%@",responseObject);
            TMResponse *response = [[TMResponse alloc] initWithResponseArray:responseObject];
            
            responseBlock(response,nil);
            
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
            //NTMLog(@"Error:%@",error);
            responseBlock(nil,error);
        }
     ];
}

-(void)executeGETAPI:(TMRequest*)request
             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    
    [apiMgr GET:request.urlString
     parameters:[request httpGetRequestParameters]
        success:^(NSURLSessionDataTask *task, id responseObject) {
            NTMLog(@"______TMBaseManager Response Description_____%@\n______________",responseObject);
            success(task,responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
            //NTMLog(@"Error:%@",error);
            failure(task,error);
        }
     ];
}

#pragma mark Upload Request
#pragma mark -
-(NSURLSessionDataTask*)uploadData:(NSData*)imgData
      withRequest:(TMRequest*)request
    responseBlock:(void (^)(TMResponse *response, NSError *error))responseBlock {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    
    NSURLSessionDataTask *task = [apiMgr POST:request.urlString parameters:[request httpPostRequestParameters] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imgData name:@"file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
        //NSLog(@"Response:%@",response.responseDictionary);
        responseBlock(response,nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        //NSLog(@"Error:%@",error);
        responseBlock(nil,error);
        
    }];
    
    return task;
}

-(NSURLSessionDataTask*)uploadDataAPI:(NSData*)imgData
                          withRequest:(TMRequest*)request
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
    [apiMgr setHttpHeadersFromParams:[request httpHeaders]];
    
    NSURLSessionDataTask *task = [apiMgr POST:request.urlString parameters:[request httpPostRequestParameters] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imgData name:@"file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"______TMBaseManager Response Description_____%@\n______________",responseObject);
        success(task,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //NSLog(@"Error:%@",error);
        failure(task,error);
    }];
    
    return task;
}
@end

//
//  TMEditPreferenceManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;
@class TMEditPreference;

//typedef void (^StatusBlock)(TMEditPreference *status ,TMError *error);
//typedef void (^SaveBlock)(NSString *success, TMError *error);

@interface TMEditPreferenceManager : TMBaseManager

-(void)getUserPreferenceDataWithResponse:(void(^)(TMEditPreference *editPreference ,TMError *error))statusBlock;
//-(void)initCall:(NSDictionary*)queryString with:(void(^)(TMEditPreference *status ,TMError *error))statusBlock;
-(void)saveData:(NSDictionary*)editPrefParams with:(void(^)(NSString *success, TMError *error))saveBlock;

- (BOOL) isCachedResponsePresent;

@end

//
//  TMRegisterManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 13/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMBaseManager.h"

@class TMError;

typedef void (^jsonBlock)(NSString *status, TMError *error);
typedef void (^SaveBlock)(NSString *flag,TMError *error);

//typedef void (^PrefillDataBlock)(NSDictionary *user, TMError *error);
//typedef void (^SignUpBlock)(NSString *flag,TMError *error);
//typedef void (^SearchBlock)(NSDictionary *dict, TMError *error);

@interface TMRegisterManager : TMBaseManager

-(void)getPrefilledData:(NSDictionary*)queryString with:(void(^)(NSDictionary *user, TMError *error))prefillBlock;
-(void)saveBasics:(NSDictionary *)queryString with:(SaveBlock)saveBlock;

//-(void)registerNewUser:(NSDictionary*)paramDict with:(SignUpBlock)signUpBlock;
//-(void)searchHashTag:(NSDictionary*)queryString with:(SearchBlock)searchBlock;

@end

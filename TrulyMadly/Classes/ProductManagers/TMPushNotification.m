//
//  TMPushNotification.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMPushNotification.h"
#import "TrulyMadly-Swift.h"

@implementation TMPushNotification

- (void) sendDeviceToken:(NSDictionary *)dict with:(void (^)(BOOL))statusBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GCM_API];
    
    [request setPostRequestParameters:dict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                statusBlock(true);
            }
            else{
                statusBlock(false);
            }
        }
        else {
            statusBlock(false);
        }
        
    }];
}

@end

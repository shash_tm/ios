//
//  TMStickerNetworkManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerNetworkManager.h"
#import "TMRequest.h"
#import "TMResponse.h"
#import "TMBuildConfig.h"

@interface TMStickerNetworkManager ()

@property (nonatomic, strong) NSMutableArray* currentRequestArray;

@end

@implementation TMStickerNetworkManager

typedef void (^Block)(NSDictionary *response, TMError *error);

static TMStickerNetworkManager* sharedNetworkManager;

+ (TMStickerNetworkManager*) getSharedNetworkManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedNetworkManager = [[TMStickerNetworkManager alloc] init];
    });
    return sharedNetworkManager;
}

- (void) makeStickerAPIRequestWithSuccessBlock:(void(^)(NSDictionary* responseDictionary)) successBlock failureBlock:(void(^)(TMError* error)) failureBlock
{
    //make the server request
    [self fetchStickerFromServerWith:^(NSDictionary* response, TMError* error){
        if (response) {
            successBlock(response);
        }
        else {
            failureBlock(error);
        }
    }];
}

-(void)fetchStickerFromServerWith:(Block)block {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_STICKER_GALLERY_API];
    
    //setting timeout interval for the sticker operations
    //making it very large
    [AFHTTPSessionManager manager].requestSerializer.timeoutInterval = 3000;
    
    NSDate* startDate = [NSDate date];
    
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                // [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                NSDictionary *quizResponse = [response response];
                block(quizResponse,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                //[[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // logout the user
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                block(nil,error);
            }
        }
        else {
            ///error
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            // [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            block(nil,tmerror);
        }
    }];
}

- (NSData*) getImageDataForRelativePathURL:(NSString*) imageRelativeURL
{
    if (imageRelativeURL) {
        
        NSData* imageData;
        
        NSArray* tempArray;
        
        @synchronized(self) {
           tempArray = [self.currentRequestArray copy];
        }
        
        if (![tempArray containsObject:imageRelativeURL]) {
            NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:imageRelativeURL]];
            request.HTTPMethod = @"GET";
            
            NSURLResponse* urlResponse;
            
            NSError* error;
            
            BOOL isRequestAddedToQueue = NO;
            
            @synchronized(self) {
                if (self.currentRequestArray.count < 51) {
                    [self.currentRequestArray addObject:imageRelativeURL];
                    isRequestAddedToQueue = YES;
                }
            }
            
            if (!isRequestAddedToQueue) {
                return nil;
            }
            
            imageData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
            
            @synchronized(self) {
                [self.currentRequestArray removeObject:imageRelativeURL];
            }
            
            if (error) {
                //error occured
                imageData = nil;
            }
        }
        return imageData;
    }
    return nil;
}

- (NSMutableArray*) currentRequestArray {
    if (!_currentRequestArray) {
        _currentRequestArray = [[NSMutableArray alloc] init];
    }
    return _currentRequestArray;
}

@end

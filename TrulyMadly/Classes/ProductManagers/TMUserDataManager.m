//
//  TMLoginManager.m
//  TrulyMadly
//
//  Created by Ankit Jain on 04/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMUserDataManager.h"
#import "TrulyMadly-Swift.h"

@implementation TMUserDataManager

-(void)fetchBasicDataFromURLString:(NSString*)basicURLString withResponse:(void(^)(NSDictionary* reponseData,TMError* error))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:basicURLString];
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(response) {
            responseBlock(response.response,nil);
        }
        else {
            TMError *tmError = [[TMError alloc] initWithError:error];
            responseBlock(nil,tmError);
        }
    }];
}

//index
-(void)getUserDataWithResponse:(UserDataResponseBlock)responseBlock {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"true" forKey:@"getuserdata"];
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_USER_DATA];
    [request setPostRequestParameters:dict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
      if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                //update user data
                TMUserSession *userSession = [TMUserSession sharedInstance];
                [userSession setUserData:response.data];
                //[userSession.user setUserDataFromDictioanry:response.data];
                responseBlock(response.response,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                responseBlock(nil,tmError);
            }
        }
        else {
            ///error
            TMError *tmError = [[TMError alloc] initWithError:error];
            responseBlock(nil,tmError);
        }
    }];
}

//on signup and login
-(void)validateLogin:(NSDictionary*)paramDict with:(LoginBlock)loginBlock{
 
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_USER_DATA];
    [request setPostRequestParameters:paramDict];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response && !error) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                NSDictionary *dataDict = [response data];
                //save cookie
                TMUserSession *userSession = [TMUserSession sharedInstance];
                [userSession setSessionCookie];
                [userSession setUserData:dataDict];
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                loginBlock(response.response,nil);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                loginBlock(nil,tmError);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401" forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                loginBlock(nil,tmError);
            }
        }
        else {
            ///error
            [self.trackEventDictionary setObject:@"error_timeout" forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmError = [[TMError alloc] initWithError:error];
            loginBlock(nil,tmError);
        }
        
    }];
}

-(void)activateUser:(NSDictionary *)queryString with:(ActivateBlock)activateBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_USER_DATA];
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                //save cookie
                TMUserSession *userSession = [TMUserSession sharedInstance];
                [userSession setSessionCookie];
                activateBlock(@"success",nil);
            }
        }
        else {
            ///error
            TMError *tmError = [[TMError alloc] initWithError:error];
            activateBlock(nil,tmError);
        }
        
    }];
    
}


-(void)logout:(NSDictionary *)queryString with:(LogoutBlock)logoutBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_LOGOUT_API];
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                // clear session id, isloggedIn
                TMUserSession *userSession = [TMUserSession sharedInstance];
                [userSession clearUserSessionData];
                TMAPIClient *apiMgr = [TMAPIClient sharedAPIManager];
                [apiMgr resetAllRequesttHeaders];
                logoutBlock(@"success",nil);
            }
        }
        else {
            ///error
            TMError *tmError = [[TMError alloc] initWithError:error];
            logoutBlock(nil,tmError);
        }
        
    }];

}


-(void)forgotPassword:(NSDictionary*)queryString with:(void(^)(BOOL status, TMError *error))forgotPassword; {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_FORGOT_PWD_API];
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {

                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                forgotPassword(true,nil);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                forgotPassword(false,tmError);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                forgotPassword(false,tmError);
            }
        }
        else {
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;

            TMError *tmError = [[TMError alloc] initWithError:error];
            forgotPassword(false,tmError);
        }
        
    }];
}

@end

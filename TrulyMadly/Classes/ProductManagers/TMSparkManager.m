    //
//  TMSparkManager.m
//  TrulyMadly
//
//  Created by Ankit on 01/06/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//


#import "TMSparkManager.h"
#import "TrulyMadly-Swift.h"
#import "TMLog.h"

@implementation TMSparkManager

-(void)sendSparkWithParams:(NSDictionary *)queryParams with:(SuccessBlock)successBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SPARK_API];
    [request setPostRequestParameters:queryParams];
    
    [self initSparkTrackingDictioanryWithEventCategory:@"sendspark"];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSString *networkRequestStatus = nil;
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                networkRequestStatus = @"success";
                NSDictionary *responseDict = [response response];
                successBlock(responseDict,nil);
            }
            else {
                NSString *errorMessage = ([response error]) ? [response error] : @"unknown_server_error";
                networkRequestStatus = errorMessage;
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                error.errorMessage = errorMessage;
                successBlock(nil,error);
            }
            
            [self trackRequestWithStatus:networkRequestStatus];
        }
        else {
            NSDate* endDate = [NSDate date];
            [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
            [self trackRequestWithStatus:error.localizedDescription];
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            successBlock(nil,tmerror);
        }
    }];
}

-(void)getAllPackages:(PackageBlock)packageBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SPARK_API];
    NSDictionary *queryParams = @{@"action":@"get_packages"};
    [request setPostRequestParameters:queryParams];
    
    [self initSparkTrackingDictioanryWithEventCategory:@"getsparkpackages"];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSString *networkRequestStatus = nil;
        if(response) {
            NSDate* endDate = [NSDate date];
            self.trackEventDictionary[@"endDate"] = endDate;
            
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                networkRequestStatus = @"success";
                NSDictionary *responseDict = [response response];
                NSArray *packageList = responseDict[@"spark_packages"];
                NSString *sparkVideoUrl = responseDict[@"spark_video"];
                packageBlock(packageList, sparkVideoUrl, nil);
            }
            else {
                NSString *errorMessage = ([response error]) ? [response error] : @"unknown_server_error";
                networkRequestStatus = errorMessage;
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                tmerror.errorMessage = errorMessage;
                packageBlock(nil,nil,tmerror);
            }
            
            [self trackRequestWithStatus:networkRequestStatus];
        }
        else {
            NSDate* endDate = [NSDate date];
            self.trackEventDictionary[@"endDate"] = endDate;
            [self trackRequestWithStatus:error.localizedDescription];
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            packageBlock(nil,nil,tmerror);
        }
    }];
}
-(void)getCommonStringWithSpark:(NSString*)matchId response:(CommonalityBlock)commonalityBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SPARK_API];
    NSDictionary *queryParams = @{@"action":@"get_commonalities",@"match_id":matchId};
    [request setPostRequestParameters:queryParams];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *responseDict = [response response];
                NSArray *commonalityList = responseDict[@"commonalities_string"];
                commonalityBlock(commonalityList,nil);
            }
            else {
                NSString *errorMessage = ([response error]) ? [response error] : @"unknown_server_error";
                
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                tmerror.errorMessage = errorMessage;
                commonalityBlock(nil,tmerror);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            commonalityBlock(nil,tmerror);
        }
    }];
}

-(void)startSparkBuyTransactionWithProductIdentifier:(NSString*)productIdentiifer
                                            response:(void(^)(NSString *transactionIdentifier,TMError *error))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KSPARK_APIPATH];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:@"buy_package" forKey:@"action"];
    [dict setObject:productIdentiifer forKey:@"apple_sku"];
    [request setPostRequestParameters:dict];
    
    [self initSparkTrackingDictioanryWithEventCategory:@"developerpayload"];
    
    [self executePOSTAPI:request
                 success:^(NSURLSessionDataTask *task, id responseObject) {
                     NSDate* endDate = [NSDate date];
                     self.trackEventDictionary[@"endDate"] = endDate;
                     NSString *networkRequestStatus = nil;
                     
                     TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                     NSInteger responseCode = response.responseCode;
                     if(response && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                         networkRequestStatus = @"success";
                         NSString *payload = response.responseDictionary[@"developer_payload"];
                         responseBlock(payload,nil);
                     }
                     else if(response && responseCode == TMRESPONSECODE_LOGOUT) {
                         networkRequestStatus = @"error401";
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                         responseBlock(nil,tmerror);
                     }
                     else {
                         networkRequestStatus = @"unknown_server_error";
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                         responseBlock(nil,tmerror);
                     }
                     
                     [self trackRequestWithStatus:networkRequestStatus];
                 }
                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                     NSDate* endDate = [NSDate date];
                     [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                     [self trackRequestWithStatus:error.localizedDescription];
                     
                     TMError *tmerror = [[TMError alloc] initWithError:error];
                     responseBlock(nil,tmerror);
                 }];
}

-(void)validateBuyTransation:(NSString*)data
          appleTransactionId:(NSString*)appleTransactionId
             tmtransactionId:(NSString*)transactionId
                      status:(void(^)(NSDictionary* paymentData, TMError *error))responseBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KSPARK_APIPATH];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:@"buy_package" forKey:@"action"];
    [dict setObject:data forKey:@"receipt-data"];
    [dict setObject:appleTransactionId forKey:@"transaction_id"];
    [dict setObject:transactionId forKey:@"developer_payload"];
    [request setPostRequestParameters:dict];
    
    [self initSparkTrackingDictioanryWithEventCategory:@"completetransaction"];
    
    [self executePOSTAPI:request
                 success:^(NSURLSessionDataTask *task, id responseObject) {
                     NSDate* endDate = [NSDate date];
                     self.trackEventDictionary[@"endDate"] = endDate;
                     NSString *networkRequestStatus = nil;
                     
                     TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                     NSInteger responseCode = response.responseCode;
                     
                     if(response && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                         networkRequestStatus = @"success";
                         NSDictionary *responseDictionary = response.responseDictionary;
                         responseBlock(responseDictionary,nil);
                     }
                     else if(response && responseCode == TMRESPONSECODE_DATASAVEERROR) {
                         networkRequestStatus = @"error403";
                         
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                         responseBlock(0,tmerror);
                     }
                     else if(response && responseCode == TMRESPONSECODE_LOGOUT) {
                         networkRequestStatus = @"error401";
                         
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                         responseBlock(0,tmerror);
                     }
                     else {
                         networkRequestStatus = @"unknown_server_error";
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                         responseBlock(0,tmerror);
                     }
                     
                     [self trackRequestWithStatus:networkRequestStatus];
                 }
                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                     NSDate* endDate = [NSDate date];
                     [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                     [self trackRequestWithStatus:@"error_timeout"];
                     
                     TMError *tmerror = [[TMError alloc] initWithError:error];
                     responseBlock(0,tmerror);
                 }];
}


-(void)validateDirectlyWithApple:(NSString*)receipt {
    NSError *error;
    NSDictionary *requestContents = @{
                                      @"receipt-data": receipt
                                      };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) { /* ... Handle error ... */ }
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"];
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   /* ... Handle error ... */
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) { /* ... Handle error ...*/ }
                                   /* ... Send a response back to the device ... */
                               }
                           }];
}


-(void)fetchSparkProfilesData:(void(^)(NSArray *sparkList,TMError *error))reposneBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KSPARK_APIPATH];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:@"get_sparks" forKey:@"action"];
    [request setPostRequestParameters:dict];
    
    NSDate* startDate = [NSDate date];
    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
     self.isRequestInProgress = TRUE;
    [self executePOSTAPI:request
                 success:^(NSURLSessionDataTask *task, id responseObject) {
                     self.isRequestInProgress = FALSE;
                     NSDate* endDate = [NSDate date];
                     [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                     NSString *networkRequestStatus = nil;
                     TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                     NSInteger responseCode = response.responseCode;
                     
                     if(response && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                         networkRequestStatus = @"success";
                         NSArray *sparkList = response.responseDictionary[@"sparks"];
                         NTMLog(@"Spark data:%@",response.responseDictionary);
                         reposneBlock(sparkList,nil);
                     }
                     else if(response && responseCode == TMRESPONSECODE_LOGOUT) {
                         networkRequestStatus = @"error401";
                         
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                         reposneBlock(nil,tmerror);
                     }
                     else {
                         networkRequestStatus = @"unknown_server_error";
                         TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                         reposneBlock(nil,tmerror);
                     }
                     
                     [self trackRequestWithStatus:networkRequestStatus];
                 }
                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                     NSDate* endDate = [NSDate date];
                     [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                     [self trackRequestWithStatus:@"error_timeout"];
                     
                     TMError *tmerror = [[TMError alloc] initWithError:error];
                     reposneBlock(nil,tmerror);
                 }];
}
-(void)updateSparkAction:(NSString*)action params:(NSDictionary*)params response:(void(^)(BOOL status,NSInteger senderId))responseBlock {
    NSMutableDictionary *reqParams = [NSMutableDictionary dictionaryWithCapacity:4];
    reqParams[@"action"] = @"update_spark";
    reqParams[@"spark_action"] = action;
    [reqParams addEntriesFromDictionary:params];
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SPARK_API];
    [request setPostRequestParameters:reqParams];
    
    __block NSInteger senderId = [params[@"sender_id"] integerValue];
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                responseBlock(TRUE,senderId);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                responseBlock(FALSE,senderId);
            }
        }
        else {
            responseBlock(FALSE,senderId);
        }
    }];
}

-(void)initSparkTrackingDictioanryWithEventCategory:(NSString*)eventCategory {
    NSDate* startDate = [NSDate date];
    self.trackEventDictionary = [NSMutableDictionary dictionaryWithCapacity:8];
    self.trackEventDictionary[@"screenName"] = @"spark";
    self.trackEventDictionary[@"eventCategory"] = eventCategory;
    self.trackEventDictionary[@"eventAction"] = @"page_load";
    self.trackEventDictionary[@"startDate"] = startDate;
}
-(void)trackRequestWithStatus:(NSString*)status {
    status = (status != nil) ? status : @"unknown_server_error";
    [self.trackEventDictionary setObject:status  forKey:@"status"];
    [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
    [self.trackEventDictionary removeAllObjects];
    self.trackEventDictionary = nil;
}

@end

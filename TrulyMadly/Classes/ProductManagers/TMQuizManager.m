//
//  TMQuizManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 20/05/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMQuizManager.h"
#import "TrulyMadly-Swift.h"
#import "TMQuizCacheManager.h"
#import "TMMessagingController.h"
#import "TMError.h"
#import "TMQuizQuestions.h"
#import "NSDictionary+JSONString.h"
#import "TMUserSession.h"
#import "TMLog.h"
#import "TMAnalytics.h"

@interface TMQuizManager ()

@property(nonatomic,strong) NSMutableDictionary* eventDict;
@property(nonatomic,strong) TMQuizController* quizController;
@end


@implementation TMQuizManager


#define KEY_RESPONSE_TYPE @"response_type"

- (instancetype) initWithQuizController:(TMQuizController*) quizController
{
    if (self = [super init]) {
        self.quizController = quizController;
    }
    return self;
}

//-(void)getQuizData:(void (^)(NSArray *quizResponse, TMError *error))quizBlock {

- (BOOL) isNetworkRequestRequiredForUpdatedQuizVersion:(NSString*) updateQuizVersion
{
    TMQuizCacheManager* quizCacheMngr = [[TMQuizCacheManager alloc] initWithConfiguration];
    NSString* dbVersion = [quizCacheMngr fetchVersion];
    if (dbVersion == nil) {
        //not a likely case though
        return YES;
    }
    else {
        if (updateQuizVersion && [updateQuizVersion isKindOfClass:[NSString class]] && [dbVersion isKindOfClass:[NSString class]]) {
            if ([updateQuizVersion intValue] != [dbVersion intValue]) {
                //updated version available
                return YES;
            }
            else {
                return NO;
            }
        }
        else {
            return NO;
        }
    }
}


/**
 * Abhijeet
 * get all available quizzes
 */
-(void)getAllAvailableQuizData {
    
    //as per the new implementation this request will be made only during the app launch or when an updated version is obtained in any server response
    //removing the date comparison logic
    
    TMQuizCacheManager* quizCacheMngr = [[TMQuizCacheManager alloc] initWithConfiguration];
    NSString* version = [quizCacheMngr fetchVersion];
    TMLOG(@"version is: %@",version);
    
    NSMutableDictionary* queryString = [[NSMutableDictionary alloc] init];
    [queryString setValue:@"get_quiz" forKey:@"action"];
    
    if(version == nil){
        [queryString setValue:@"0" forKey:@"version"];
    }else {
        [queryString setValue:version forKey:@"version"];
    }
    
    //test code
     // [queryString setValue:@"0" forKey:@"version"];
    
    [self fetchQuizFromServer:queryString with:^(NSDictionary *response, TMError *error) {
        if(response!= nil) {
            NSArray* quizData = [response objectForKey:@"quizzes"];
            NSString* curr_version = [response objectForKey:@"current_version"];
            if (quizData && ![quizData isKindOfClass:[NSNull class]]) {
                [quizCacheMngr updateQuiz:quizData forUpdatedVersion:curr_version];
                [quizCacheMngr updateQuizConfig:curr_version];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quizzesFetchedForImageCaching" object:quizData];
                
                //update the quiz list or any other concerned UI
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateQuizDataFetched" object:nil];
            }
            //make a request for getting the updated status
            [self getPlayedQuizFlareForMatchID];
        }
        else {
            //error response recieved
            if ([error errorCode] == TMERRORCODE_LOGOUT) {
                //logout response
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quizLogoutResponseReceived" object:nil];
            }
            else {
                //timeout response
            }
        }
    }];
}


/**
 * Abhijeet
 * save quiz question-answers key-value pairs
 * @param dict containing the question-answers pairs
 */
- (void) saveQuizWithID:(NSString*) quizID withMatchID:(NSString*) matchID withQuestionAnswerDictionary:(NSDictionary*) answerDict
{
    if (quizID && ![quizID isKindOfClass:[NSNull class]]) {
        NSMutableDictionary* queryDict = [[NSMutableDictionary alloc] init];
        [queryDict setValue:@"save_answer" forKey:@"action"];
        [queryDict setValue:[NSString stringWithString:quizID] forKey:@"quiz_id"];
        [queryDict setValue:matchID forKey:@"match_id"];
        [queryDict setValue:[answerDict jsonStringWithPrettyPrint:YES] forKey:@"answers"];
        if ([self isNetworkReachable]) {
            [self fetchQuizFromServer:queryDict with:^(NSDictionary* response, TMError* error){
                
                if (response) {
                    //shoot notification to notify any appropriate listeners
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveQuizResponseReceived" object:response];
                }
                if (error) {
                    //error response recieved
                    if ([error errorCode] == TMERRORCODE_LOGOUT) {
                        //logout response
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizLogoutResponseReceived" object:nil];
                    }
                    else {
                        
                        NSString* failureMessage = @"Save quiz failed.";
                        
                        NSDictionary* quizFailureDictionary = @{@"quizID":quizID,@"failureMessage":failureMessage};
                        
                        //timeout response
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"saveQuizFailed" object:quizFailureDictionary];
                    }
                }
            }];
        }
        else {
            NSString* failureMessage = @"No internet connectivity.";
            
            NSDictionary* quizFailureDictionary = @{@"quizID":quizID,@"failureMessage":failureMessage};
            
            //timeout response
            [[NSNotificationCenter defaultCenter] postNotificationName:@"saveQuizFailed" object:quizFailureDictionary];
        }
    }
}


/**
 * Abhijeet
 * decide quiz action
 * @param quiz ID
 * @param match ID
 */
- (void) decideQuizActionFromChatForQuizID:(NSString*) quizID matchID:(NSString*) matchID
{
    if (quizID && matchID) {
        NSMutableDictionary* queryDict = [[NSMutableDictionary alloc] init];
        [queryDict setValue:@"quiz_click" forKey:@"action"];
        [queryDict setValue:matchID forKey:@"match_id"];
        [queryDict setValue:[NSNumber numberWithBool:YES] forKey:@"from_chat"];
        [queryDict setValue:quizID forKey:@"quiz_id"];
        
        if ([self isNetworkReachable]) {
            [self fetchQuizFromServer:queryDict with:^(NSDictionary *response, TMError *error) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"decideQuizRequestCompleted" object:response];
                
                if (response) {
                    
                    if ([self isNetworkRequestRequiredForUpdatedQuizVersion:[response valueForKey:@"current_version"]]) {
                        //post notification to update the UI in order to inform about the failed quiz action
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizVersionChanged" object:nil];
                        
                        //update the quiz list
                        [self getAllAvailableQuizData];
                    }
                    
                    else {
                        
                        NSString* responseType = [response valueForKey:KEY_RESPONSE_TYPE];
                        
                        if ([responseType isEqualToString:@"questions"]) {
                            
                            self.eventDict = [[NSMutableDictionary alloc] init];
                            
                            if (quizID) {
                                //adding event tracking for save quiz action
                                [self.eventDict setObject:@"TMQuizChatView" forKey:@"screenName"];
                                [self.eventDict setObject:quizID forKey:@"label"];
                                [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
                                [self.eventDict setObject:@"true" forKey:@"GA"];
                                [self.eventDict setObject:@"fetch_quiz" forKey:@"eventAction"];
                                [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
                            }
                            
                            //quiz questions
                            TMQuizQuestions* quizQuestions = [[TMQuizQuestions alloc] initWithResponseDictionary:response];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"quizQuestionsReceived" object:quizQuestions];
                        }
                        else if ([responseType isEqualToString:@"answers"]) {
                            //common answer reponse
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"commonAnswersReceived" object:response];
                        }
                        else if ([responseType isEqualToString:@"common_answers"]) {
                            
                            if (!self.eventDict) {
                                self.eventDict = [[NSMutableDictionary alloc] init];
                            }
                            
                            if (quizID) {
                                BOOL isFlare = [[response objectForKey:@"flare"] boolValue];
                                NSString *action = (isFlare) ? @"flare_quiz_fetch_common_answers" : @"quiz_fetch_common_answers";
                                self.eventDict = [[NSMutableDictionary alloc] init];
                                
                                //adding event tracking for common answers
                                [self.eventDict setObject:@"TMQuizChatView" forKey:@"screenName"];
                                [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
                                [self.eventDict setObject:quizID forKey:@"label"];
                                [self.eventDict setObject:quizID forKey:@"status"];
                                //[self.eventDict setObject:@"true" forKey:@"GA"];
                                [self.eventDict setObject:action forKey:@"eventAction"];
                                [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
                            }
                            
                            //common answer reponse
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"commonAnswersReceived" object:response];
                        }
                        else if ([responseType isEqualToString:@"nudge_list"]) {
                            //nudge list response
                            if([response objectForKey:@"auto_nudge"] && [[response objectForKey:@"auto_nudge"] isKindOfClass:[NSNumber class]] && ((NSNumber*)[response objectForKey:@"auto_nudge"]).intValue) {
                                //auto nudge
                                
                                NSMutableArray* nudgedUserMatchIDArray = [[NSMutableArray alloc] init];
                                
                                if ([response valueForKey:@"nudge_list"] && [[response valueForKey:@"nudge_list"] isKindOfClass:[NSArray class]]) {
                                    
                                    NSArray* nudgeListArray = [response valueForKey:@"nudge_list"];
                                    for (int index = 0; index < nudgeListArray.count; index++) {
                                        NSDictionary* nudgeUserDictionary = ([[nudgeListArray objectAtIndex:index] isKindOfClass:[NSDictionary class]])?[nudgeListArray objectAtIndex:index]:nil;
                                        if (nudgeUserDictionary && [[nudgeUserDictionary valueForKey:@"match_id"] isKindOfClass:[NSString class]]) {
                                            NSString* matchID = [nudgeUserDictionary valueForKey:@"match_id"];
                                            [nudgedUserMatchIDArray addObject:[NSString stringWithFormat:@"%@",matchID]];
                                        }
                                    }
                                }
                                
                                NSDictionary* infoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[response valueForKey:@"quiz_id"],@"quiz_id",[response valueForKey:@"name"],@"name",nudgedUserMatchIDArray,@"nudgedUsers", nil];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"autoNudgeResponseRecievedFromChat" object:infoDictionary];
                            }
                        }
                        else {
                            //failure cases
                        }
                    }
                }
                else {
                    //error response recieved
                    if ([error errorCode] == TMERRORCODE_LOGOUT) {
                        //logout response
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizLogoutResponseReceived" object:nil];
                    }
                    else {
                        //timeout response
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizDecideActionFailed" object:nil];
                    }
                }
            }];
        }
        else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //timeout response
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quizDecideActionFailed" object:@"networkFailure"];
                
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"decideQuizRequestCompleted" object:nil];
            });
        }
    }
}


/**
 * Abhijeet
 * decide quiz action
 * @param quiz ID
 * @param match ID
 */
- (void) decideQuizActionForQuizID:(NSString*) quizID matchID:(NSString*) matchID
{
    if (quizID && matchID) {
        NSMutableDictionary* queryDict = [[NSMutableDictionary alloc] init];
        [queryDict setValue:@"quiz_click" forKey:@"action"];
        [queryDict setValue:matchID forKey:@"match_id"];
        [queryDict setValue:quizID forKey:@"quiz_id"];
        
        if ([self isNetworkReachable]) {
            [self fetchQuizFromServer:queryDict with:^(NSDictionary *response, TMError *error) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"decideQuizRequestCompleted" object:response];
                
                if (response) {
                    
                    if ([self isNetworkRequestRequiredForUpdatedQuizVersion:[response valueForKey:@"current_version"]]) {
                        //post notification to update the UI in order to inform about the failed quiz action
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizVersionChanged" object:nil];
                        
                        //update the quiz list
                        [self getAllAvailableQuizData];
                    }
                    else {
                        
                        NSString* responseType = [response valueForKey:KEY_RESPONSE_TYPE];
                        
                        if ([responseType isEqualToString:@"questions"]) {
                            
                            self.eventDict = [[NSMutableDictionary alloc] init];
                            
                            if (quizID) {
                                //adding event tracking for save quiz action
                                [self.eventDict setObject:quizID forKey:@"label"];
                                [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
                                [self.eventDict setObject:@"true" forKey:@"GA"];
                                [self.eventDict setObject:@"fetch_quiz" forKey:@"eventAction"];
                                [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
                            }
                            
                            //quiz questions
                            TMQuizQuestions* quizQuestions = [[TMQuizQuestions alloc] initWithResponseDictionary:response];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"quizQuestionsReceived" object:quizQuestions];
                        }
                        else if ([responseType isEqualToString:@"answers"]) {
                            //common answer reponse
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"commonAnswersReceived" object:response];
                        }
                        else if ([responseType isEqualToString:@"common_answers"]) {
                            
                            if (quizID) {
                                BOOL isFlare = [[response objectForKey:@"flare"] boolValue];
                                NSString *action = (isFlare) ? @"flare_quiz_fetch_common_answers" : @"quiz_fetch_common_answers";
                                
                                self.eventDict = [[NSMutableDictionary alloc] init];
                                
                                //adding event tracking for common answers
                                [self.eventDict setObject:@"TMQuizChatView" forKey:@"screenName"];
                                [self.eventDict setObject:@"quiz" forKey:@"eventCategory"];
                                [self.eventDict setObject:quizID forKey:@"label"];
                                [self.eventDict setObject:quizID forKey:@"status"];
                                [self.eventDict setObject:action forKey:@"eventAction"];
                                [[TMAnalytics sharedInstance] trackNetworkEvent:self.eventDict];
                            }
                            
                            //common answer reponse
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"commonAnswersReceived" object:response];
                        }
                        else if ([responseType isEqualToString:@"nudge_list"]) {
                            //nudge list response
                            if([response objectForKey:@"auto_nudge"] && [[response objectForKey:@"auto_nudge"] isKindOfClass:[NSNumber class]] && ((NSNumber*)[response objectForKey:@"auto_nudge"]).intValue) {
                                //auto nudge
                                
                                NSMutableArray* nudgedUserMatchIDArray = [[NSMutableArray alloc] init];
                                
                                if ([response valueForKey:@"nudge_list"] && [[response valueForKey:@"nudge_list"] isKindOfClass:[NSArray class]]) {
                                    
                                    NSArray* nudgeListArray = [response valueForKey:@"nudge_list"];
                                    for (int index = 0; index < nudgeListArray.count; index++) {
                                        NSDictionary* nudgeUserDictionary = ([[nudgeListArray objectAtIndex:index] isKindOfClass:[NSDictionary class]])?[nudgeListArray objectAtIndex:index]:nil;
                                        if (nudgeUserDictionary && [[nudgeUserDictionary valueForKey:@"match_id"] isKindOfClass:[NSString class]]) {
                                            NSString* matchID = [nudgeUserDictionary valueForKey:@"match_id"];
                                            [nudgedUserMatchIDArray addObject:[NSString stringWithFormat:@"%@",matchID]];
                                        }
                                    }
                                }
                                
                                
                                NSDictionary* infoDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[response valueForKey:@"quiz_id"],@"quiz_id",[response valueForKey:@"name"],@"name",nudgedUserMatchIDArray,@"nudgedUsers", nil];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"autoNudgeResponseRecieved" object:infoDictionary];
                            }
                        }
                        else {
                            //failure cases
                        }
                    }
                }
                else {
                    //error response recieved
                    if ([error errorCode] == TMERRORCODE_LOGOUT) {
                        //logout response
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizLogoutResponseReceived" object:nil];
                    }
                    else {
                        //timeout response
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"quizDecideActionFailed" object:nil];
                    }
                }
            }];

        }
        else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //timeout response
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quizDecideActionFailed" object:@"networkFailure"];
                
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"decideQuizRequestCompleted" object:nil];
            });
        }
    }
}

- (void) getPlayedQuizesForMatchID:(NSString*) matchID
{
    NSString* flareStatus = @"flare";
    
    if (matchID) {
        TMQuizCacheManager* quizCacheMngr = [[TMQuizCacheManager alloc] initWithConfiguration];
        
        NSMutableDictionary* queryDict = [[NSMutableDictionary alloc] init];
        [queryDict setValue:@"get_played_quiz" forKey:@"action"];
        [queryDict setValue:matchID forKey:@"match_id"];
        
        [self fetchQuizFromServer:queryDict with:^(NSDictionary *response, TMError *error) {
            if(response!= nil) {
                
                if ([self isNetworkRequestRequiredForUpdatedQuizVersion:[response valueForKey:@"current_version"]]) {
                    //post notification to update the UI in order to inform about the failed quiz action
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"quizVersionChanged" object:nil];
                    
                    //update the quiz liste
                    [self getAllAvailableQuizData];
                }
                else {
                    
                    if (![[response objectForKey:@"played_quizzes"] isKindOfClass:[NSNull class]]) {
                        NSArray* playedQuizzes = [response objectForKey:@"played_quizzes"];
                        
                        NSMutableArray* updatedUserIDArray;
                        
                        for (int index = 0; index < playedQuizzes.count; index++) {
                            NSNumber* playedByUser = [[playedQuizzes objectAtIndex:index] objectForKey:@"played_by_user"];
                            NSNumber* playedByMatch = [[playedQuizzes objectAtIndex:index] objectForKey:@"played_by_match"];
                            
                            BOOL isFlare = ([[[playedQuizzes objectAtIndex:index] objectForKey:flareStatus] boolValue])?YES:NO;
                            
                            NSString* quizID = [[playedQuizzes objectAtIndex:index] objectForKey:@"quiz_id"];
                            
                            NSMutableArray* playedByUserIDArray = [[NSMutableArray alloc] init];
                            
                            TMQuizInfo* oldQuizInfo = [quizCacheMngr getQuizDataForID:quizID];
                            
                            if ([playedByUser boolValue]) {
                                [playedByUserIDArray addObject:[NSString stringWithFormat:@"%@",[TMUserSession sharedInstance].user.userId]];
                            }
                            if ([playedByMatch boolValue]) {
                                if ([[TMMessagingController sharedController] getCurrentMatchId]) {
                                    if (isFlare) {
                                        [playedByUserIDArray addObject:[NSString stringWithFormat:@"%@+",[[TMMessagingController sharedController] getCurrentMatchId]]];
                                    }
                                    else {
                                        [playedByUserIDArray addObject:[[TMMessagingController sharedController] getCurrentMatchId]];
                                    }
                                }
                                if (![oldQuizInfo.playedBy containsObject:[[TMMessagingController sharedController] getCurrentMatchId]]) {
                                    //matchID updated from not played to played
                                    if (!updatedUserIDArray) {
                                        updatedUserIDArray = [[NSMutableArray alloc] init];
                                    }
                                    [updatedUserIDArray addObject:quizID];
                                }
                            }
                            [quizCacheMngr updateQuizWithID:quizID playedByWithPlayerList:playedByUserIDArray];
                        }
                        // [self.quizController saveUpdatedStatusWithUpdatedQuizIDArray:[updatedUserIDArray copy]];
                    }
                    //provide notification so that the quiz view can update their content accordingly
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"playedQuizzesStatusUpdate" object:nil];
                }
                //update the timestamp here for the last fetching of quizzes
                [TMDataStore setObject:[NSDate date] forKey:LAST_QUIZZES_FETCH_DATE];
            }
            else {
                //null response recieved
                //TODO::Abhijeet update UI accordingly
                //error response recieved
                if ([error errorCode] == TMERRORCODE_LOGOUT) {
                    //logout response
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"quizLogoutResponseReceived" object:nil];
                }
                else {
                    //timeout response
                }
            }
        }];
    }
}


/**
 * make request from 1 to 1 conversation
 */
- (void) getPlayedQuizFlareForMatchID
{
    NSString* userID = [TMUserSession sharedInstance].user.userId;
    NSString* matchID = [[TMMessagingController sharedController] getCurrentMatchId];
    
    if (userID && matchID) {
        TMQuizCacheManager* quizCacheMngr = [[TMQuizCacheManager alloc] initWithConfiguration];
        NSString* userTimestamp = [quizCacheMngr getUserTimestampForMatchID:matchID userID:userID];
        NSString* matchTimestamp = [quizCacheMngr getMatchTimestampForMatchID:matchID userID:userID];
        
        
        NSMutableDictionary* queryDict = [[NSMutableDictionary alloc] init];
        [queryDict setValue:@"get_played_quiz_flare" forKey:@"action"];
        [queryDict setValue:matchID forKey:@"match_id"];
        [queryDict setValue:matchTimestamp forKey:@"match_tstamp"];
        [queryDict setValue:userTimestamp forKey:@"user_tstamp"];
        
        [self fetchQuizFromServer:queryDict with:^(NSDictionary *response, TMError *error) {
            
            if (response) {
                if ([self isNetworkRequestRequiredForUpdatedQuizVersion:[response valueForKey:@"current_version"]]) {
                    [self getAllAvailableQuizData];
                }
                else {
                    //parse the response
                    
                    //parse the sticker info
                    NSString* currentStickerVersion = ([response[@"sticker_version"] isKindOfClass:[NSString class]])?response[@"sticker_version"]:nil;
                   
                    if (currentStickerVersion) {
                        //check with the previous version
                        
                        NSString* oldStickerVersion = ([TMDataStore containsObjectForKey:CURRENT_STICKER_VERSION])?([TMDataStore retrieveObjectforKey:CURRENT_STICKER_VERSION]):nil;

                     	   if (!oldStickerVersion || ([currentStickerVersion intValue] > [oldStickerVersion intValue])) {
                            //need to make the sticker request now
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"stickerVersionUpdated" object:nil];
                        }
                    }
                    
                    NSArray* previousUnreadQuizzesArray = [quizCacheMngr getUnreadQuizzesForMatchID:matchID userID:userID];
                    NSArray* unreadUserQuizzes = [response valueForKey:@"user_quiz"];
                    NSArray* unreadMatchQuizzes = [response valueForKey:@"match_quiz"];
                    NSArray* flareQuizzes = (![[response valueForKey:@"flare"] isKindOfClass:[NSNull class]])?[response valueForKey:@"flare"]:nil;
                    
                    NSDictionary* curatedDealInfoDictionary = ([[response valueForKey:@"curated_deal_icon"] isValidObject])?[response valueForKey:@"curated_deal_icon"]:nil;
                   
                    //process curated deal data
                    BOOL isIconClickable = ([[curatedDealInfoDictionary valueForKey:@"icon_clickable"] isValidObject])?[[curatedDealInfoDictionary valueForKey:@"icon_clickable"] boolValue]:NO;
                    
                    
                    [[TMMessagingController sharedController] setDealIconClickStatus:isIconClickable withMatch:matchID];
                    
                    //////////////
                    BOOL isTimeStampAbsent = (!matchTimestamp || matchTimestamp.length == 0)?YES:NO;
                    
                    BOOL isUpdatedQuizAvaialable = NO;
                    
                    NSMutableArray* updatedQuizzesInBetweenAppUpdateArray;
                    
                    if (isTimeStampAbsent) {
                        
                        TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
                        NSArray* oldQuizArray = [quizCacheManager getAllQuizzesPlayedByUserID:matchID];
                        
                        updatedQuizzesInBetweenAppUpdateArray = [self updatedQuizIDsInBetweenAppUpdateForOldQuizIDs:oldQuizArray
                                                                                                      andNewQuizIDs:unreadMatchQuizzes];
                        
                        isUpdatedQuizAvaialable = (updatedQuizzesInBetweenAppUpdateArray.count > 1)?YES:NO;
                    }
                    
                    NSMutableArray* totalUnreadQuizzes = [[NSMutableArray alloc] init];
                    [totalUnreadQuizzes addObjectsFromArray:unreadMatchQuizzes];
                    
                    if (previousUnreadQuizzesArray && previousUnreadQuizzesArray.count > 0) {
                        //add the previousUnreadQuizzesArray items
                        [totalUnreadQuizzes addObjectsFromArray:previousUnreadQuizzesArray];
                    }
                    
                    if (isTimeStampAbsent) {
                        //mark only the newly updated quizzes as unread
                        totalUnreadQuizzes = updatedQuizzesInBetweenAppUpdateArray;
                    }
                    
                    totalUnreadQuizzes = [[[NSOrderedSet orderedSetWithArray:totalUnreadQuizzes] array] mutableCopy];
                    
                    if (totalUnreadQuizzes && totalUnreadQuizzes.count > 0) {
                        //update the toolbar blooper
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"newQuizAvailable" object:[NSNumber numberWithInt:[matchID intValue]]];
                    }
                    
                    [quizCacheMngr insertQuizStatusInfoForMatchID:matchID userID:userID userTimeStamp:[response valueForKey:@"user_tstamp"] matchTimestamp:[response valueForKey:@"match_tstamp"] unreadQuizID:totalUnreadQuizzes];
                    
                    //now add the unread quizzes from the self user
                    [totalUnreadQuizzes addObjectsFromArray:unreadUserQuizzes];
                    
                    //update the played by status for the 2 users
                    for (int index = 0; index < totalUnreadQuizzes.count; index++) {
                        NSMutableArray* playedByUserIDMutableArray;
                        
                        NSString* quizID;
                        quizID = [totalUnreadQuizzes objectAtIndex:index];
                        
                        //has quiz been played by user
                        if ([self checkIfQuizArray:unreadUserQuizzes ContainsQuizID:quizID]) {
                            //quiz has been played by user
                            if (!playedByUserIDMutableArray) {
                                playedByUserIDMutableArray = [[NSMutableArray alloc] init];
                            }
                            [playedByUserIDMutableArray addObject:userID];
                            
                            if ([flareQuizzes containsObject:quizID] && ![unreadMatchQuizzes containsObject:quizID]) {
                                //update the flare status for this quiz as this quiz is only present in user array
                                [playedByUserIDMutableArray addObject:[NSString stringWithFormat:@"%@+",matchID]];
                            }
                        }
                        
                        //has quiz been played by match
                        if ([self checkIfQuizArray:unreadMatchQuizzes ContainsQuizID:quizID]) {
                            //quiz has been played by match
                            if (!playedByUserIDMutableArray) {
                                playedByUserIDMutableArray = [[NSMutableArray alloc] init];
                            }
                            
                            //check if this quiz has a flare
                            if ([flareQuizzes containsObject:quizID]) {
                                //add flare info to this quiz
                                [playedByUserIDMutableArray addObject:[NSString stringWithFormat:@"%@+",matchID]];
                            }
                            else {
                                //add info without flare
                                [playedByUserIDMutableArray addObject:matchID];
                            }
                        }
                        
                        [quizCacheMngr updateQuizWithID:[NSString stringWithFormat:@"%d",[quizID intValue]] playedByWithPlayerList:[playedByUserIDMutableArray copy]];
                    }
                }
                //provide notification so that the quiz view can update their content accordingly
                [[NSNotificationCenter defaultCenter] postNotificationName:@"playedQuizzesStatusUpdate" object:nil];
            }
        }];
    }
}


- (BOOL) checkIfQuizArray:(NSArray*) quizArray ContainsQuizID:(NSString*) quizID {
    
    NSString* quizToSearch;
    
    if (quizID && [quizID isKindOfClass:[NSString class]]) {
        quizToSearch = quizID;
    }
    else if (quizID && [quizID isKindOfClass:[NSNumber class]]) {
        quizToSearch = [NSString stringWithFormat:@"%d",[quizID intValue]];
    }
    if (quizToSearch) {
        for (int index = 0; index < quizArray.count; index++) {
            if ([[quizArray objectAtIndex:index] isKindOfClass:[NSString class]]) {
                if ([[quizArray objectAtIndex:index] isEqualToString:quizToSearch]) {
                    return YES;
                }
            }
            else if ([[quizArray objectAtIndex:index] isKindOfClass:[NSNumber class]]) {
                if ([quizToSearch isEqualToString:[NSString stringWithFormat:@"%d",[[quizArray objectAtIndex:index] intValue]]]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (NSMutableArray*) updatedQuizIDsInBetweenAppUpdateForOldQuizIDs:(NSArray*) oldQuizIDs andNewQuizIDs:(NSArray*) newQuizIDs {
    NSMutableArray* updatedQuizIDs;
    for (int index = 0; index < newQuizIDs.count; index++) {
        NSString* newQuizString = [NSString stringWithFormat:@"%d",[[newQuizIDs objectAtIndex:index] intValue]];
        if (![oldQuizIDs containsObject:newQuizString]) {
            if (!updatedQuizIDs) {
                updatedQuizIDs = [[NSMutableArray alloc] init];
            }
            [updatedQuizIDs addObject:newQuizString];
        }
    }
    return updatedQuizIDs;
}


- (NSArray*) addTimeStampToQuizResponseArray:(NSArray*) responseQuizDataArray
{
    if (responseQuizDataArray) {
        for (int index = 0; index < responseQuizDataArray.count; index++) {
            
        }
    }
    return nil;
}


- (void) updateQuizNewStatus:(BOOL) isNewQuiz forQuizID:(NSString*) quizID
{
    TMQuizCacheManager* quizCacheManager = [[TMQuizCacheManager alloc] initWithConfiguration];
    [quizCacheManager updateQuizNewStatus:isNewQuiz forQuizID:quizID];
}

-(NSArray*)fetchQuizFromDBForLatestVersion:(BOOL) isQuizForLatestVersion {
    TMQuizCacheManager* quizCacheMngr = [[TMQuizCacheManager alloc] initWithConfiguration];
    NSArray* quizData = [quizCacheMngr getQuizForUpdatedVersion:isQuizForLatestVersion];
   if ([quizData isKindOfClass:[NSNull class]]) {
        return nil;
    }
    else {
        return quizData;
    }
}


-(void)fetchQuizFromServer:(NSDictionary *)queryString with:(Block)block {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_QUIZ_API];
    [request setPostRequestParameters:queryString];
    
    //setting timeout interval for the quiz operations
    [AFHTTPSessionManager manager].requestSerializer.timeoutInterval = 30;
    
    NSDate* startDate = [NSDate date];
    
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                // [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                NSDictionary *quizResponse = [response response];
                block(quizResponse,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                //[[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // logout the user
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                block(nil,error);
            }
            else if (responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = (NSString*)[response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                block(nil,tmError);
            }
        }
        else {
            ///error
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            // [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            block(nil,tmerror);
        }
    }];
}
@end

//
//  TMFileManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMFileManager : NSObject

/**
 * writes file data to a given path
 * @param file data
 * @param path URL String
 * @return write status
 */
- (BOOL) writeFileWithData:(NSData*) fileData toPath:(NSString*) pathURLString;


/**
 * determines if file is present at a path
 * @param relative file path
 * @return file status
 */
- (BOOL) isFilePresentAtPath:(NSString*) relativeFilePath;


/**
 * gets file at a given path
 * @param file path
 * @return data at the given file path
 */
- (NSData*) getFileAtPath:(NSString*) filePath;


/**
 * gets the documents directory path
 * @return documents directory path
 */
- (NSString*) getDocumentsDirectoryPath;


/**
 * writes webp file with given data to a given path
 * @param data to be written
 * @param path
 * @return write status
 */
//- (BOOL) writeWebPFileWithData:(NSData*) data toPath:(NSString*) pathURLString;


/////////////////////////////////
+(BOOL)writeFileWithData1:(NSData*)fileData toPath:(NSString*)pathURLString;
+(BOOL)isFilePresentAtPath1:(NSString*)relativeFilePath;
+(NSData*)getFileAtPath1:(NSString*)filePath;
+(BOOL)removeFileAtPath:(NSString*)filePath;
+(BOOL)cacheData:(NSData*)fileData atPath:(NSString*)path withFileName:(NSString*)fileName;

@end

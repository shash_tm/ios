//
//  TMFileManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMFileManager.h"
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "TMLog.h"

//#import <WebP/encode.h>
//#import <WebP/decode.h>

@implementation TMFileManager

- (BOOL) writeFileWithData:(NSData*) fileData toPath:(NSString*) pathURLString
{
    if (fileData && pathURLString) {
        NSString* documentsDirectory = [self getDocumentsDirectoryPath];
        
        NSString* filePath = [documentsDirectory stringByAppendingPathComponent:pathURLString];
        
        if (![fileData writeToFile:filePath atomically:NO]) {
            return NO;
        }
        
        return YES;
    }
    return NO;
}

//- (BOOL) writeWebPFileWithData:(NSData*) data toPath:(NSString*) pathURLString {
//    if (data && pathURLString) {
//        NSString* documentsDirectory = [self getDocumentsDirectoryPath];
//
//        NSData* webPData = [self getWebPDataWithData:data];
//
//        NSString* filePath = [documentsDirectory stringByAppendingPathComponent:pathURLString];
//
//        if (![webPData writeToFile:filePath atomically:YES]) {
//            return NO;
//        }
//
//        return YES;
//    }
//    return NO;
//}

- (BOOL) isFilePresentAtPath:(NSString*) relativeFilePath
{
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    NSString* documentsDirectory = [self getDocumentsDirectoryPath];
    
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:relativeFilePath];
    
    if ([fileManager fileExistsAtPath:filePath]) {
        return YES;
    }
    
    return NO;
}

- (NSData*) getFileAtPath:(NSString*) filePath
{
    NSFileManager* fileManger = [NSFileManager defaultManager];
    
    NSString* documentsDirectory = [self getDocumentsDirectoryPath];
    
    NSString* absoluteFilePath = [documentsDirectory stringByAppendingPathComponent:filePath];
    
    if ([fileManger fileExistsAtPath:absoluteFilePath]) {
        return [fileManger contentsAtPath:absoluteFilePath];
    }
    return nil;
}

- (NSString*) getDocumentsDirectoryPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+(NSString*)getTMCacheDirectory {
    NSString* documentsDirectory = [self getDocumentsDirectoryPath1];
    NSString *tmDirectory = [documentsDirectory stringByAppendingPathComponent:@".tm"];
    return tmDirectory;
}

/////////////////////////////////////////////////////////////////
+(NSString*)getDocumentsDirectoryPath1
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}
+(BOOL)cacheData:(NSData*)fileData atPath:(NSString*)path withFileName:(NSString*)fileName
{
    if (fileData && path && fileName) {
        NSString *tmDirectory = [self getTMCacheDirectory];
        NSString* filePathDirectory = [tmDirectory stringByAppendingPathComponent:path];
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePathDirectory]) {
            BOOL status = [[NSFileManager defaultManager] createDirectoryAtPath:filePathDirectory
                                                    withIntermediateDirectories:YES
                                                                     attributes:nil
                                                                          error:nil];
            TMLOG(@"Status:%d",status);
        }
        
        NSString *filePathWithName = [filePathDirectory stringByAppendingPathComponent:fileName];
        if (![fileData writeToFile:filePathWithName atomically:YES]) {
            return NO;
        }
        return YES;
    }
    return NO;
}
+(BOOL)writeFileWithData1:(NSData*)fileData toPath:(NSString*)pathURLString
{
    if (fileData && pathURLString) {
        NSString* documentsDirectory = [self getDocumentsDirectoryPath1];
        NSString *tmDirectory = [documentsDirectory stringByAppendingPathComponent:@".tm"];
        NSString* filePath = [tmDirectory stringByAppendingPathComponent:pathURLString];
        if (![fileData writeToFile:filePath atomically:YES]) {
            return NO;
        }
        return YES;
    }
    return NO;
}
+(BOOL)isFilePresentAtPath1:(NSString*)relativeFilePath
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* documentsDirectory = [self getDocumentsDirectoryPath1];
    NSString *tmDirectory = [documentsDirectory stringByAppendingPathComponent:@".tm"];
    NSString* filePath = [tmDirectory stringByAppendingPathComponent:relativeFilePath];
    if ([fileManager fileExistsAtPath:filePath]) {
        return YES;
    }
    return NO;
}
+(NSData*)getFileAtPath1:(NSString*)filePath
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* documentsDirectory = [self getDocumentsDirectoryPath1];
    NSString *tmDirectory = [documentsDirectory stringByAppendingPathComponent:@".tm"];
    NSString* absoluteFilePath = [tmDirectory stringByAppendingPathComponent:filePath];
    if ([fileManager fileExistsAtPath:absoluteFilePath]) {
        return [fileManager contentsAtPath:absoluteFilePath];
    }
    return nil;
}
+(BOOL)removeFileAtPath:(NSString*)filePath {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* documentsDirectory = [self getDocumentsDirectoryPath1];
    NSString *tmDirectory = [documentsDirectory stringByAppendingPathComponent:@".tm"];
    NSString* absoluteFilePath = [tmDirectory stringByAppendingPathComponent:filePath];
    BOOL success = [fileManager removeItemAtPath:absoluteFilePath error:nil];
    return success;
}
//////////////////////////////////////////////////////////////////////////////////////
//- (BOOL) writeWebPFileWithData:(NSData*) data toPath:(NSString*) pathURLString {
//    if (data && pathURLString) {
//        NSString* documentsDirectory = [self getDocumentsDirectoryPath];
//
//        NSData* webPData = [self getWebPDataWithData:data];
//
//        NSString* filePath = [documentsDirectory stringByAppendingPathComponent:pathURLString];
//
//        if (![webPData writeToFile:filePath atomically:YES]) {
//            return NO;
//        }
//
//        return YES;
//    }
//    return NO;
//}


//- (NSData*) getWebPDataWithData:(NSData*) data {
//    int rc = WebPGetEncoderVersion();
//    NSLog(@"WebP encoder version: %d", rc);
//
//    CGImageRef imageRef = [UIImage imageWithData:data].CGImage;
//    CGColorSpaceRef colorSpace = CGImageGetColorSpace(imageRef);
//    if (CGColorSpaceGetModel(colorSpace) != kCGColorSpaceModelRGB) {
//        NSLog(@"Sorry, we need RGB");
//    }
//    CGDataProviderRef dataProvider = CGImageGetDataProvider(imageRef);
//    CFDataRef imageData = CGDataProviderCopyData(dataProvider);
//    const UInt8 *rawData = CFDataGetBytePtr(imageData);
//
//    size_t width = CGImageGetWidth(imageRef);
//    size_t height = CGImageGetHeight(imageRef);
//    uint8_t *output;
//    NSUInteger stride = CGImageGetBytesPerRow(imageRef);
//    size_t ret_size;
//
////    if (quality == kWebPLossless) {
////        ret_size = WebPEncodeLosslessRGB(rawData, width, height, stride, &output);
////    }else
//        ret_size = WebPEncodeRGBA(rawData, width, height, stride, 100, &output);
//
//    if (ret_size == 0) {
//        NSLog(@"Oops, no data");
//    }
//    CFRelease(imageData);
//    CGColorSpaceRelease(colorSpace);
//    NSData *webPdata = [NSData dataWithBytes:(const void *)output length:ret_size];
//
//    return webPdata;
//}

@end

//
//  TMMatchManger.m
//  TrulyMadly
//
//  Created by Ankit Jain on 04/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMatchManager.h"
#import "TrulyMadly-Swift.h"
#import "TMAppResponseCachingManager.h"
#import "TMLog.h"
#import "TMUserSession.h"

@implementation TMMatchManager

-(void)dealloc {
    TMLOG(@"TMMatchManger Dealloc");
}

-(void)getMatchesWithParams:(NSDictionary*)params result:(MatchesBlock)matchesResultBLock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_MATCH_DATA];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    dict[@"login_mobile"] = @"true";
    dict[@"app"] = @"iOS";
    dict[@"mutual_friends"] = @"true";
    NSString *refreshMatchId = [[TMUserSession sharedInstance] getCachedSelectedMatchIdForRefresh];
    NSString *hash = [[TMUserSession sharedInstance] getCachedSelectedHashForRefresh];
    if(refreshMatchId) {
        dict[@"select_match_id"] = refreshMatchId;
    }
    if(hash) {
        dict[@"select_profile_hash"] = hash;
    }
    [dict addEntriesFromDictionary:params];
    [request setGetRequestParams:dict];
    
    NSDate* startDate = [NSDate date];
    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
    
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    ///
                    [[TMUserSession sharedInstance] resetCachedSelectMatchRefreshData];
                    
                    ////
                    TMResponse *response = [[TMResponse alloc] initWithResponse:responseObject];
                    NSInteger responseCode = response.serverResponseCode;
                    
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                
                    if(responseObject && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                        //set user data
                        [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        response = nil;
                        //NSLog(@"__Match Data:%@__",responseObject);
                        matchesResultBLock(responseObject,nil);
                    }
                    //logout case
                    else if(responseObject && responseCode == TMRESPONSECODE_LOGOUT) {
                        [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        response = nil;
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                        matchesResultBLock(nil,tmerror);
                    }
                    else {
                        //for any other error from server
                        ///check
                        response = nil;
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                        matchesResultBLock(nil,tmerror);
                    }
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
                    [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                    [self.trackEventDictionary removeAllObjects];
                    self.trackEventDictionary = nil;

                    TMError *tmerror = [[TMError alloc] initWithError:error];
                    matchesResultBLock(nil,tmerror);
                }];
}
-(void)getFBMutualConnectionsWithMatch:(NSInteger)matchId response:(void(^)(NSDictionary* data))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_MUTUALFBFRIEND];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    dict[@"login_mobile"] = @"true";
    dict[@"app"] = @"iOS";
    dict[@"target_user_id"] = @(matchId);
    [request setGetRequestParams:dict];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSInteger responseCode = [[response.responseDictionary objectForKey:@"responseCode"] integerValue];
        if(responseCode == 200) {
            responseBlock(response.responseDictionary);
        }
        else { //failure case
            responseBlock(nil);
        }
    }];
}

-(void)sendMatchWithUrl:(NSString*)url completionBlock:(LikeBlock)likeBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:url];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [request setGetRequestParams:dict];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        //in case response is received from server
        NSInteger responseCode = [[response.responseDictionary objectForKey:@"response_code"] integerValue];
        if(responseCode == 200) {
            NSDictionary *dic = [response responseDictionary];
            NSDictionary *mutLikeDict = [dic objectForKey:@"return_action"];
            if(mutLikeDict) {
                NSDictionary *mutualData = @{@"name":[dic objectForKey:@"name"], @"msg_url":[dic objectForKey:@"msg_url"],@"isMutualMatch":[NSNumber numberWithBool:true]};
                likeBlock(mutualData,nil);
            }
            else {
                NSDictionary *mutualData = @{@"isMutualMatch":[NSNumber numberWithBool:false]};
                likeBlock(mutualData,nil);
            }
        }
        else if(responseCode == 201) {
            NSDictionary *mutualData = @{@"isMutualMatch":[NSNumber numberWithBool:false]};
            TMError *tmerror = [[TMError alloc] initWithResponseCode:201];
            likeBlock(mutualData,tmerror);
        }
        else { //failure case
            NSDictionary *mutualData = @{@"isMutualMatch":[NSNumber numberWithBool:false]};
            TMError *tmerror = [[TMError alloc] initWithError:error];
            likeBlock(mutualData,tmerror);
        }
    }];
    
}
-(void)sendFBAccesToken:(NSString*)token completionBlock:(void(^)(BOOL isDeviceTokenExpired))completionBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_UPDATE_FB_ACCESSTOEKN];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:token forKey:@"token"];
    [request setPostRequestParameters:dict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        //in case response is received from server
        NSInteger responseCode = [[response.responseDictionary objectForKey:@"responseCode"] integerValue];
        if(responseCode == 200) {
            completionBlock(FALSE);
        }
        else if(responseCode == TMRESPONSECODE_LOGOUT) {
            completionBlock(TRUE);
        }
    }];
}
-(void)getMyProfileData:(ProfileResponseBlock)profileResponseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_MYPROFILE];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"true" forKey:@"me"];
    
    BOOL isCachePresent = false;
    
    if ([TMAppResponseCachingManager containsCachedResponseForURL:request.urlString]) {
        isCachePresent = true;
        dispatch_async(dispatch_get_main_queue(), ^{
            //update the UI with the old cached response
            profileResponseBlock([[TMProfileResponse alloc] initWithResponse:[TMAppResponseCachingManager getCachedResponseForURL:request.urlString]],nil);
        });
        
        //add the hash & tstamp value to the request dict
        NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
        
        NSString* tstampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
        
        if (hashValue && [hashValue isValidObject]) {
             [dict setObject:[TMAppResponseCachingManager getHashValueForURL:request.urlString] forKey:@"hash"];
            if (tstampValue && [tstampValue isValidObject]) {
                [dict setObject:[TMAppResponseCachingManager getTStampValueForURL:request.urlString] forKey:@"tstamp"];
            }
        }
    }
    
    [request setGetRequestParams:dict];
    NSDate* startDate = [NSDate date];
    
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
        TMProfileResponse *profileResponse = [[TMProfileResponse alloc] initWithResponse:responseObject];
        NSInteger responseCode = profileResponse.serverResponseCode;
        if(profileResponse && (responseCode == TMRESPONSECODE_SUCCESS) ) {
            [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            profileResponseBlock(profileResponse,nil);
            
            NSString* hashValue = [responseObject valueForKey:@"hash"];
            
            NSString* tstamp = [responseObject valueForKey:@"tstamp"];
            
            //cache the updated response
            [TMAppResponseCachingManager setCachedResponse:responseObject forURL:request.urlString hash:hashValue tstamp:tstamp];
        }
        //logout case
        else if(profileResponse && responseCode == TMRESPONSECODE_LOGOUT) {
            [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
            profileResponseBlock(nil,tmerror);
        }
    }
    failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
        [self.trackEventDictionary removeAllObjects];
        self.trackEventDictionary = nil;
        
        if(!isCachePresent){
            TMError *tmerror = [[TMError alloc] initWithError:error];
            profileResponseBlock(nil,tmerror);
        }
    }];
}


-(void)getMutualLikeData:(MutualLikeBlock)mutualLikeBlock; {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_MUTUALLIKES];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [request setGetRequestParams:dict];
    
    NSDate* startDate = [NSDate date];
    
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
            
                    TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                    NSInteger responseCode = response.responseCode;
                    
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    if(response && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                        [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        
                        mutualLikeBlock(response.data,nil);
                    }
                    //logout case
                    else if(response && responseCode == TMRESPONSECODE_LOGOUT) {
                        [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                        mutualLikeBlock(nil,tmerror);
                    }
                    else {
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                        mutualLikeBlock(nil,tmerror);
                    }
                    
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
                    [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                    [self.trackEventDictionary removeAllObjects];
                    self.trackEventDictionary = nil;

                    TMError *tmerror = [[TMError alloc] initWithError:error];
                    mutualLikeBlock(nil,tmerror);
                }];
}

-(void)deactivateProfile:(NSString*)deactivateReason
                response:(DeactivateProfileBlock)deactivateResponseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_DEACTIVATE_PROFILE];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:deactivateReason forKey:@"reason"];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [request setGetRequestParams:dict];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSInteger responseCode = response.responseCode;
        if(response) {
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                deactivateResponseBlock(true,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                deactivateResponseBlock(false,tmerror);
            }
        }
        else { //failure case
            TMError *tmerror = [[TMError alloc] initWithError:error];
            deactivateResponseBlock(false,tmerror);
        }
    }];
    
}

-(void)updateFBProfile:(NSString *)token response:(updateFBProfileBlock)updateFBProfileResponseBlock {
   
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SYNC_FB_API];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:token forKey:@"token"];
    [request setGetRequestParams:dict];
    NSDate* startDate = [NSDate date];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        NSInteger responseCode = response.responseCode;
        if(response) {
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                updateFBProfileResponseBlock(true,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                updateFBProfileResponseBlock(false,tmerror);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = [response error];
                NSString *status = [@"error403 - " stringByAppendingString:msg];
                
                [self.trackEventDictionary setObject:status forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmerror.errorMessage = msg;
                updateFBProfileResponseBlock(false,tmerror);
            }
        }
        else { //failure case
            
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            updateFBProfileResponseBlock(false,tmerror);
        }
    }];
}

-(void)userProfileFromUrl:(NSDictionary*)params
                 response:(ProfileResponseBlock)profileResultBlock {
    NSString *profileUrl = params[@"profile_url"];
    TMRequest *request = [[TMRequest alloc] initWithUrlString:profileUrl];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    
    BOOL enable_profile_cache = [params[@"enable_profile_cache"] boolValue];
    BOOL isCachePresent = false;
    
    if ([TMAppResponseCachingManager containsCachedResponseForURL:request.urlString]) {
        isCachePresent = true;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            [NSThread sleepForTimeInterval:0.1];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //show the old cached data
                //in a new run loop
                profileResultBlock([[TMProfileResponse alloc] initWithResponse:[TMAppResponseCachingManager getCachedResponseForURL:request.urlString]], nil);
            });
        });
        
        //add the hash  & tstamp value to the request dict
        NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
        
        NSString* tstampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
        
        if (hashValue && [hashValue isValidObject]) {
            [dict setObject:hashValue forKey:@"hash"];
            if (tstampValue && [tstampValue isValidObject]) {
                [dict setObject:tstampValue forKey:@"tstamp"];
            }
        }
    }
    
    [request setGetRequestParams:dict];
    
    NSDate* startDate = [NSDate date];
    
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    TMProfileResponse *profileResponse = [[TMProfileResponse alloc] initWithResponse:responseObject];
                    NSInteger responseCode = profileResponse.serverResponseCode;
                    
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    if(profileResponse && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                        //set user data
                        [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        
                        profileResultBlock(profileResponse,nil);
                        
                        //cache the response
                        if(enable_profile_cache) {
                            NSString* hashValue = [responseObject valueForKey:@"hash"];
                            NSString* tstampValue = [responseObject valueForKey:@"tstamp"];
                            [TMAppResponseCachingManager setCachedResponse:responseObject forURL:request.urlString hash:hashValue tstamp:tstampValue];
                        }
                    }
                    //logout case
                    else if(profileResponse && responseCode == TMRESPONSECODE_LOGOUT) {
                        [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                        profileResultBlock(nil,tmerror);
                    }
                    else if (responseCode == TMRESPONSECODE_RESOURCE_NOT_MODIFIED) {
                      //do nothing
                    }
                    else {
                        //for any other error from server
                        ///check
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                        profileResultBlock(nil,tmerror);
                    }
                    
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
                    [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                    [self.trackEventDictionary removeAllObjects];
                    self.trackEventDictionary = nil;
                    
                    if(!isCachePresent) {
                        TMError *tmerror = [[TMError alloc] initWithError:error];
                        profileResultBlock(nil,tmerror);
                    }
                }];
}


//from match when user blocks other user
-(void)sendInteractionWithBlockUserStatusPing:(NSString*)url {
    //NSLog(@"Blocking %@",url);
    TMRequest *request = [[TMRequest alloc] initWithUrlString:url];
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(error) {
            //NSLog(@"error%@",error);
        }
        else {
//            /NSLog(@"Success");
        }
    }];
}

- (BOOL) isCachedResponsePresent {
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_GET_MYPROFILE]) {
        return YES;
    }
    return NO;
}
- (BOOL) isCachedResponsePresentForMyProfile {
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_GET_MYPROFILE]) {
        return YES;
    }
    return NO;
}

- (BOOL) isCachedResponsePResentForMatchProfile:(NSString*) matchProfileURL {
    if ([TMAppResponseCachingManager getCachedResponseForURL:matchProfileURL]) {
        return YES;
    }
    return NO;
}

-(void)deleteCacheResponseForMyProfile
{
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_GET_MYPROFILE];
}

@end


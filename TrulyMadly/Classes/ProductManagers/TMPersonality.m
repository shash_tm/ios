//
//  TMPersonality.m
//  TrulyMadly
//
//  Created by Chetan Bhardwaj on 04/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMPersonality.h"
#import "TrulyMadly-Swift.h"
#import "TMAppResponseCachingManager.h"

@implementation TMPersonality

-(void)getFavListWithResponse:(void(^)(NSDictionary *dict,TMError *error))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_PERSONALITY_RELATIVEPATH];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"hobby" forKey:@"step"];
    
    BOOL isCachePresent = false;
    if ([TMAppResponseCachingManager getCachedResponseForURL:request.urlString]) {
        isCachePresent = true;
        //update the UI with the old cached response
        responseBlock([TMAppResponseCachingManager getCachedResponseForURL:request.urlString],nil);
        
        //add the hash value & tstamp to the request dict
        NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
        
        NSString* tstampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
        
        if (hashValue && [hashValue isValidObject]) {
            [dict setObject:hashValue forKey:@"hash"];
            if (tstampValue && [tstampValue isValidObject]) {
                 [dict setObject:tstampValue forKey:@"tstamp"];
            }
        }
    }
    
    [request setGetRequestParams:dict];
    
    NSDate* startDate = [NSDate date];

    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response){
            NSInteger responseCode = response.responseCode;
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                responseBlock([response response],nil);
                
                //cache the response
                NSString* hashValue = [response.response valueForKey:@"hash"];
                NSString* tstampValue = [response.response valueForKey:@"tstamp"];
                [TMAppResponseCachingManager setCachedResponse:response.response forURL:request.urlString hash:hashValue tstamp:tstampValue];
            }
            //logout case
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                responseBlock(nil,tmerror);
            }
            else {
                //for any other error from server
                if(!isCachePresent) {
                    TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                    responseBlock(nil,tmerror);
                }
            }
        }
        else { //failure case
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            if(!isCachePresent){
                TMError *tmerror = [[TMError alloc] initWithError:error];
                responseBlock(nil,tmerror);
            }
        }
    }];
    
}
-(void)getFavQuizListForKey:(NSString*)key withCategory:(NSString*)category
               withResponse:(void(^)(NSDictionary *response,TMError *error))responseBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_PERSONALITY_FAVLIST_RELATIVEPATH];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    //[dict setObject:key forKey:@"value"];
    [dict setObject:@"search_page" forKey:@"action"];
    [dict setObject:key forKey:@"search_text"];
    [dict setObject:category forKey:@"category"];
    [request setGetRequestParams:dict];
    
    NSDate* startDate = [NSDate date];
    [self.trackEventDictionary setObject:@"TMFavQuizOptionListView" forKey:@"screenName"];
    [self.trackEventDictionary setObject:@"dictionary" forKey:@"eventCategory"];
    [self.trackEventDictionary setObject:@"server_call" forKey:@"eventAction"];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
            //NSLog(@"track dict %@",self.trackEventDictionary);
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;

            responseBlock(response.responseDictionary,nil);
        }
        else {
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            //error
            TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(nil,tmerror);
        }
    }];
    
}

-(void)saveFavourite:(NSDictionary*)favDic
        withResponse:(void(^)(BOOL status))responsBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:[NSString stringWithFormat:@"%@?save=hobby",KAPI_PERSONALITY_RELATIVEPATH]];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    //[dict setObject:@"hobby" forKey:@"save"];
    NSString *str = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:favDic options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
    [dict setObject:str forKey:@"data"];
    [request setPostRequestParameters:dict];
    
    NSDate* startDate = [NSDate date];
    
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_PERSONALITY_RELATIVEPATH];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = response.responseCode;
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;

                responsBlock(true);
            }
        }
        else {
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            //error
            responsBlock(false);
        }
    }];
}

- (BOOL) isCachedResponsePresent {
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_PERSONALITY_RELATIVEPATH]) {
        return YES;
    }
    return NO;
}


@end


//
//  TMFacebookManager.m
//  TrulyMadly
//
//  Created by Ankit on 02/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMFacebookManager.h"
#import <FacebookSDK/FacebookSDK.h>
#import "TMSwiftHeader.h"


@implementation TMFacebookManager

-(void)fetchFBPermissions:(PermissionBlock)permissionBlock {
    FBRequest *request = [FBRequest requestForGraphPath:@"me/permissions"];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary <FBGraphUser> *my, NSError *error) {
        NSArray *permissionList = [my objectForKey:@"data"];
        if (permissionList) {
            permissionBlock(permissionList,nil);
        }else{
            TMError *err = [[TMError alloc] initWithError:error];
            permissionBlock(nil,err);
        }
        
    }];

}

-(void)fetchFacebookAlbumList:(FbPhotoAlbumBlock)photoAlbum {
    
    FBRequest *request = [FBRequest requestForGraphPath:@"me/albums"];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary <FBGraphUser> *my, NSError *error) {
        
        NSArray *albumList = [my objectForKey:@"data"];
        if(albumList) {
            photoAlbum(albumList,nil);
        }
        else {
            photoAlbum(nil,nil);
        }
        
    }];
}

-(void)fetchPhotosFromAlbum:(NSString*)photoId withResponse:(FbPhotoAlbumBlock)photoBlock{
 
    FBRequest *r = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"/%@/photos",photoId]];
    [r startWithCompletionHandler:^(FBRequestConnection *c, NSDictionary <FBGraphUser> *m, NSError *err) {
        //here you will get pictures for each album
        NSArray *albumList = [m objectForKey:@"data"];
        if(albumList) {
            photoBlock(albumList,nil);
        }
        else {
            TMError *error = [[TMError alloc] initWithError:err];
            photoBlock(nil,error);
        }
    }];
}

@end

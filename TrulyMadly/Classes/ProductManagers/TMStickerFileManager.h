//
//  TMStickerFileManager.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMFileManager.h"
#import "TMSticker.h"

typedef enum {
    imageFormatThumbnail,
    imageFormatHD
} stickerImageFormat;

@interface TMStickerFileManager : TMFileManager

/**
 * stores sticker if required 
 * @param sticker object
 * @param image data
 * @param sticker image format
 * @return sticker storage status
 */
- (BOOL) storeStickerIfRequiredWithStickerInfo:(TMSticker*) sticker withImageData:(NSData*) imageData forImageFormat:(stickerImageFormat) stickerImageFormat;


/**
 * gets sticker file path for a given sticker
 * @param sticker object
 * @return sticker path
 */
- (NSString*) getStickerFilePathForSticker:(TMSticker*) sticker;


/**
 * gets the sticker hd file path for a given sticker
 * @param sticker object
 * @return sticker path
 */
- (NSString*) getStickerHDImageFilePathForSticker:(TMSticker*) sticker;

@end

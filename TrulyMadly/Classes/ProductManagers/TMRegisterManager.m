//
//  TMRegisterManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 13/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMRegisterManager.h"
#import "TMAnalytics.h"
#import "TrulyMadly-Swift.h"

@implementation TMRegisterManager

-(void)getPrefilledData:(NSDictionary*)queryString with:(void(^)(NSDictionary *user, TMError *error))prefillBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_REGISTER_RELATIVEPATH];
    [request setGetRequestParams:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        //in case response is received from server
        if(response) {
                        
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                NSDictionary *registeredData = [response response];
                prefillBlock(registeredData,nil);
                
            }else if(responseCode == TMRESPONSECODE_LOGOUT) {
                
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                //logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                prefillBlock(nil,tmError);
            }

            
        }
        else { //failure case
            
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmError = [[TMError alloc] initWithError:error];
            prefillBlock(nil,tmError);
        }
    }];


}


///////////////////
/*
-(void)registerNewUser:(NSDictionary *)paramDict with:(SignUpBlock)signUpBlock{
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_LOGIN_RELATIVEPATH];
    [request setPostRequestParameters:paramDict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *err) {
        
        if(response) {
            
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                NSDictionary *dataDict = [response data];
                //save cookie
                TMUserSession *userSession = [TMUserSession sharedInstance];
                [userSession setSessionCookie];
                [userSession.user setUserDataFromDictioanry:dataDict];
                signUpBlock(@"yes",nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                signUpBlock(nil,tmError);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                NSString *msg = [response error];
                tmError.errorMessage = msg;
                signUpBlock(nil,tmError);
            }
            
        }
        else {
            ///error
            TMError *tmError = [[TMError alloc] initWithError:err];
            signUpBlock(nil,tmError);
        }
        
    }];
}
*/
////////////edu
-(void)saveBasics:(NSDictionary *)queryString with:(SaveBlock)saveBlock{
   
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_REGISTER_RELATIVEPATH];
    [request setGetRequestParams:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        //in case response is received from server
        if(response) {
            
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                saveBlock(@"yes",nil);
            }else if(responseCode == TMRESPONSECODE_LOGOUT) {
                
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                
                //logout the user
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                saveBlock(nil,tmError);
            }
            
        }
        else { //failure case
            
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmError = [[TMError alloc] initWithError:error];
            saveBlock(nil,tmError);
        }
    }];
    
}

/*
-(void)searchHashTag:(NSDictionary *)queryString with:(SearchBlock)searchBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_HASHTAG_API];
    [request setPostRequestParameters:queryString];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                //[[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                searchBlock(response.response,nil);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = (NSString*)[response error];
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                searchBlock(nil,tmError);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                //[[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                [self.trackEventDictionary removeAllObjects];
                self.trackEventDictionary = nil;
                // logout the user
                TMError *error = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                searchBlock(nil,error);
            }
        }
        else {
            ///error
            [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
            //[[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            TMError *tmerror = [[TMError alloc] initWithError:error];
            searchBlock(nil,tmerror);
        }
        
    }];
}
*/

@end

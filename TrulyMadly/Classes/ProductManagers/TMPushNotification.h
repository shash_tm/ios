//
//  TMPushNotification.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 29/01/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@interface TMPushNotification : TMBaseManager

- (void)sendDeviceToken: (NSDictionary*)dict with:(void(^)(BOOL status))statusBlock;

@end

//
//  TMAppResponseCachingManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 08/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMAppResponseCachingManager.h"
#import "TMAppResponseSQLiteManager.h"
#import "NSObject+TMAdditions.h"
#import "TMUserSession.h"
#import "TMUser.h"


@interface TMAppResponseCachingManager ()

@property (nonatomic, strong) TMAppResponseSQLiteManager* sqliteManager;

@end

@implementation TMAppResponseCachingManager

static TMAppResponseSQLiteManager* sharedSQLiteManager;

#pragma mark - custom getters

+ (TMAppResponseSQLiteManager*) sqliteManager {
    if (!sharedSQLiteManager) {
        sharedSQLiteManager = [[TMAppResponseSQLiteManager alloc] initWithConfiguration];
    }
    return sharedSQLiteManager;
}

+ (BOOL) containsCachedResponseForURL:(NSString*) url {
    if (url && [url isValidObject]) {
        return [[TMAppResponseCachingManager sqliteManager] containsCachedResponseForURL:[NSString stringWithFormat:@"%@_%@",[TMUserSession sharedInstance].user.userId,url]];
    }
    return NO;
}

+ (NSDictionary*) getCachedResponseForURL:(NSString*) url {
    if (url && [url isValidObject]) {
        return [[TMAppResponseCachingManager sqliteManager] getCachedResponseForURL:[NSString stringWithFormat:@"%@_%@",[TMUserSession sharedInstance].user.userId,url]];
    }
    return nil;
}

+ (NSString*) getHashValueForURL:(NSString*) url {
    if (url && [url isValidObject]) {
         return [[TMAppResponseCachingManager sqliteManager] getHashValueForURL:[NSString stringWithFormat:@"%@_%@",[TMUserSession sharedInstance].user.userId,url]];
    }
    return nil;
}

+ (NSString*) getTStampValueForURL:(NSString*) url {
    if (url && [url isValidObject]) {
        NSString* tstampValue = [[TMAppResponseCachingManager sqliteManager] getTStampValueForURL:[NSString stringWithFormat:@"%@_%@",[TMUserSession sharedInstance].user.userId,url]];
        if (tstampValue.length == 0) {
            return nil;
        }
        return tstampValue;
    }
    return nil;
}

+ (BOOL) setCachedResponse:(NSDictionary*) response forURL:(NSString*) url hash:(NSString*) hash tstamp:(NSString*) tstamp {
    if (response && [response isValidObject] && url && [url isValidObject] && hash && [hash isValidObject]) {
        
        if (!tstamp || ![tstamp isValidObject]) {
            tstamp = @"";
        }        
        return [[TMAppResponseCachingManager sqliteManager] updateCachedResponse:response forURL:[NSString stringWithFormat:@"%@_%@",[TMUserSession sharedInstance].user.userId,url] forHashValue:hash tstamp:tstamp];
    }
    return NO;
}

+ (BOOL) deleteCachedDataForURL:(NSString*) url {
    return [[TMAppResponseCachingManager sqliteManager] deleteCachedDataForURL:[NSString stringWithFormat:@"%@_%@",[TMUserSession sharedInstance].user.userId,url]];
}

+ (BOOL) deleateAllCacheData {
    return [[TMAppResponseCachingManager sqliteManager] deleteAllCachedData];
}

@end

//
//  TMMessageManager.m
//  TrulyMadly
//
//  Created by Ankit on 30/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMMessageManager.h"
#import "TMMessageResponse.h"
#import "TMRequest.h"
#import "TMError.h"
#import "TMLog.h"
#import "TMAnalytics.h"
#import "TMDataStore.h"


@interface TMMessageManager ()

@end

@implementation TMMessageManager

-(void)fetchConversationList:(void(^)(NSArray *conversationList,TMError *error))reposneBlock {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_MESSAGE_LIST];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    NSString *rowupdatedTs = [TMDataStore retrieveObjectforKey:@"rowupdatedts"];
    if(rowupdatedTs){
        [dict setObject:rowupdatedTs forKey:@"tstamp"];
    }
    [dict setObject:@"true" forKey:@"isMessageMatchMergedList"];
    BOOL isMsTMResponseReceived = [TMDataStore retrieveBoolforKey:@"create_miss_tm"];
    if(!isMsTMResponseReceived) {
        [dict setObject:@"true" forKey:@"create_miss_tm"];
    }
    [request setPostRequestParameters:dict];
    
    NSDate* startDate = [NSDate date];
    
    [self executePOSTAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    TMResponse *response = [[TMResponse alloc] initWithResponseDictionary:responseObject];
                    NSInteger responseCode = response.responseCode;
                    if(response && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                        [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        
                        reposneBlock(response.data,nil);
                        //[TMDataStore setBool:TRUE forKey:@"create_miss_tm"];
                    }
                    else if(response && responseCode == TMRESPONSECODE_LOGOUT) {
                        [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
                        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                        [self.trackEventDictionary removeAllObjects];
                        self.trackEventDictionary = nil;
                        
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                        reposneBlock(nil,tmerror);
                    }
                    else {
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                        reposneBlock(nil,tmerror);
                    }
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
                    [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
                    [self.trackEventDictionary removeAllObjects];
                    self.trackEventDictionary = nil;
                    
                    TMError *tmerror = [[TMError alloc] initWithError:error];
                    reposneBlock(nil,tmerror);
                }];
}

///
-(void)getChatMessages:(NSString*)URLString
                       params:(NSDictionary*)params
                       response:(void(^)(TMMessageResponse *response,TMError *tmError))response {
    TMLOG(@"Polling:%@ Params:%@",URLString,params);
    TMRequest *request = [[TMRequest alloc] initWithUrlString:URLString];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict addEntriesFromDictionary:params];
    [request setGetRequestParams:dict];
    
    [self executeGETAPI:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    TMMessageResponse *messageResponse = [[TMMessageResponse alloc] initWithResponse:responseObject];
                    NSInteger responseCode = messageResponse.serverResponseCode;
                    if(messageResponse && (responseCode == TMRESPONSECODE_SUCCESS) ) {
                        //set user data
                        response(messageResponse,nil);
                    }
                    //logout case
                    else if(messageResponse && responseCode == TMRESPONSECODE_LOGOUT) {
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                        response(nil,tmerror);
                    }
                    else {
                        TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_SERVER_FALLBACKERROR];
                        response(nil,tmerror);
                    }
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    TMError *tmerror = [[TMError alloc] initWithError:error];
                    response(nil,tmerror);
                }];
}

-(void)sendMessage:(NSString*)URLString
            params:(NSDictionary*)params
      withResponse:(void(^)(NSDictionary *responseDict, TMError *error))response {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:URLString];
    [request setPostRequestParameters:params];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *responseObj, NSError *error) {
        //TMLOG(@"Response Dict:%@",responseObj.responseDictionary);
        NSInteger responseCode = responseObj.responseCode;
        if(responseObj && (responseCode == TMRESPONSECODE_SUCCESS) ) {
            //set user data
            response(responseObj.responseDictionary,nil);
        }
        //logout case
        else if(responseObj && responseCode == TMRESPONSECODE_LOGOUT) {
            TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
            response(nil,tmerror);
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            response(nil,tmerror);
        }
    }];
}
-(void)doodleLinkFromURLString:(NSString*)URLString
                      response:(void(^)(NSString *doodleLink))response {
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:URLString];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"doodle" forKey:@"type"];
    [request setPostRequestParameters:dict];
    
    [self executePOSTAPI:request success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *doodleLink = responseObject[@"doodle_url"];
        response(doodleLink);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        response(nil);
    }];
}

///////////////////////////////////////////////////////////////////
-(void)sendBlockUserCall:(NSString *)userId {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_MESSAGE_LIST];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:userId forKey:@"block_shown_id"];
    [dict setObject:@"block_shown" forKey:@"block_shown"];
    [request setPostRequestParameters:dict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(error) {
            //NSLog(@"error:%@",error);
        }
    }];
}

///////////////////////////////////////////////////////////////////
-(void)deleteConversation:(NSInteger)lastMessageId
                  withURL:(NSString*)URLString
                  response:(void(^)(BOOL status))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:URLString];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    dict[@"login_mobile"] = @"true";
    dict[@"type"] = @"clear_chat";
    dict[@"last_msg_id"] = @(lastMessageId);
    [request setPostRequestParameters:dict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(!error) {
            responseBlock(true);
        }
        else {
            responseBlock(false);
        }
    }];
}
-(void)getDeletedConversationListWihtResponse:(void(^)(NSArray* deletedConversationList))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_MESSAGE_LIST];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"1" forKey:@"get_clear_chat_data"];
    [request setGetRequestParams:dict];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(!error) {
            NSDictionary *responseDictionary = response.responseDictionary;
            NSArray *records = responseDictionary[@"cleared_data"];
            responseBlock(records);
        }
        else {
            responseBlock(nil);
        }
    }];
}
///////////////////////////////////////////////////////////////////
-(void)blockUserWithReason:(NSString *)reason withURL:(NSString*)url
                  response:(void(^)(BOOL status))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:url];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"report_abuse" forKey:@"type"];
    [dict setObject:reason forKey:@"reason"];
    [request setPostRequestParameters:dict];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        if(!error) {
            responseBlock(true);
        }
        else {
            responseBlock(false);
        }
    }];
}

///////////////////////////////////////////////////////////////////
-(void)getShareProfileURLForMatch:(NSString*)matchId responseBlock:(void(^)(NSDictionary* data))responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_SHARE_PROFILE];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    dict[@"login_mobile"] = @"true";
    dict[@"app"] = @"iOS";
    dict[@"shorten_url_conversationlist"] = @"1";
    dict[@"source"] = @"converstion_list";
    dict[@"match_id"] = matchId;
    [request setGetRequestParams:dict];
    
    [self executeGETRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        //in case response is received from server
        NSInteger responseCode = [[response.responseDictionary objectForKey:@"responseCode"] integerValue];
        if(responseCode == 200) {
            NSDictionary *dictionary = [response responseDictionary];
            NSDictionary *contentDictionary = [dictionary objectForKey:@"response"];
            responseBlock(contentDictionary);
        }
        else if(responseCode == 201) {
            ///TMError *tmerror = [[TMError alloc] initWithResponseCode:201];
            responseBlock(nil);
        }
        else { //failure case
            //TMError *tmerror = [[TMError alloc] initWithError:error];
            responseBlock(nil);
        }
    }];

}

/////////////////////////////////////////////////////////
-(void)pingForNetworkStatus:(void(^)(BOOL status))responseBlock {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://djy1s2eqovnmt.cloudfront.net/files/ping_new.json"]];
    [request setTimeoutInterval:10];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
                               
                               BOOL status = FALSE;
                               
                               if(data && !connectionError) {
                                   NSError *error;
                                   NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                                   NSInteger responseCode = [dictionary[@"responseCode"] integerValue];
                                   if(responseCode ==200) {
                                       status = TRUE;
                                   }
                               }
                               
                               responseBlock(status);
    }];
}

@end

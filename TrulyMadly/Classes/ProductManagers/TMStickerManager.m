//
//  TMStickerManager.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 19/08/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMStickerManager.h"
#import "TMStickerNetworkManager.h"
#import "TMStickerDBManager.h"
#import "TMStickerFileManager.h"
#import "TMUserSession.h"
#import "TMDataStore.h"
#import "TMUser.h"
#import "TMLog.h"
//#import "UIImage+WebP.h"


@interface TMStickerManager ()

@property (nonatomic, strong) dispatch_queue_t dbWriterQueue;
@property (nonatomic, readwrite) int storedGalleryCount;

@end

@implementation TMStickerManager

//parsing keywords
#define KEY_GALLERY @"gallery"
#define STICKER_VERSION @"sticker_version"
#define KEY_ACTIVE @"active"
#define KEY_DEFAULT_DOWNLOAD @"default_download"
#define KEY_GALLERY_ID @"gallery_id"
#define KEY_GENDER @"gender"
#define KEY_ID @"id"
#define KEY_STICKERS @"stickers"
#define KEY_UPDATED_IN_VERSION @"updated_in_version"


static TMStickerManager* sharedManager;

+ (TMStickerManager*) sharedStickerManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[TMStickerManager alloc] init];
    });
    return sharedManager;
}

- (instancetype) init {
    if (self = [super init]) {
        self.dbWriterQueue = dispatch_queue_create("com.TrulyMadly.StickerWriterQueue", NULL);
    }
    return self;
}

- (void) makeStickerAPITRequestWithGalleryRefreshBlock:(void(^)()) galleryRefreshBlock stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock allGalleriesRefreshBlock:(void(^)()) allGalleriesRefreshBlock failureBlock:(void(^)())failureBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        TMStickerNetworkManager* stickerNetworkManager = [TMStickerNetworkManager getSharedNetworkManager];
        
        //make the API request
        [stickerNetworkManager makeStickerAPIRequestWithSuccessBlock:^(NSDictionary* responseDictionary){
            //parse the response
            if (responseDictionary) {
                BOOL isStickerVersionUpdated = NO;
                
                //storing the sticker version
                if ([responseDictionary valueForKey:STICKER_VERSION] && ![[responseDictionary valueForKey:STICKER_VERSION] isKindOfClass:[NSNull class]]) {
                    NSString* currentStickerVersion = [responseDictionary valueForKey:STICKER_VERSION];
                    
                    NSString* previousStickerVersion = [TMDataStore retrieveObjectforKey:CURRENT_STICKER_VERSION];
                    
                    if ([previousStickerVersion intValue] < [currentStickerVersion intValue]) {
                        isStickerVersionUpdated = YES;
                    }
                }
                if (isStickerVersionUpdated) {
                    
                    TMStickerDBManager* dbManager = [[TMStickerDBManager alloc] init];
                    
                    if ([responseDictionary valueForKey:KEY_GALLERY] && [[responseDictionary valueForKey:KEY_GALLERY] isKindOfClass:[NSArray class]]) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSArray* galleryArray = [responseDictionary valueForKey:KEY_GALLERY];
                        
                            if ([self isValidGalleryResponse:galleryArray]) {
                                
                                [self checkForNewGalleryInResponseGalleryArray:galleryArray];
                                
                                self.storedGalleryCount = 0;
                                
                                for (int index = 0; index < galleryArray.count; index++) {
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                        
                                        if ([[galleryArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                                            NSDictionary* galleryDictionary = [galleryArray objectAtIndex:index];
                                            
                                            stickerGender currentStickerGender;
                                            
                                            NSString* galleryID = ([galleryDictionary valueForKey:@"id"]?([galleryDictionary valueForKey:@"id"]):@"-1");
                                            
                                            //default sticker gender type
                                            currentStickerGender = commonStickerGender;
                                            
                                            if ([galleryDictionary valueForKey:KEY_GENDER] && [[galleryDictionary valueForKey:KEY_GENDER] isKindOfClass:[NSString class]]) {
                                                NSString* galleryGender = [galleryDictionary valueForKey:KEY_GENDER];
                                                
                                                if ([galleryGender hasPrefix:@"c"] || [galleryGender hasPrefix:@"C"]) {
                                                    //common gender
                                                    currentStickerGender = commonStickerGender;
                                                }
                                                else if ([galleryGender hasPrefix:@"f"] || [galleryGender hasPrefix:@"F"]) {
                                                    //female gender
                                                    currentStickerGender = femaleStickerGender;
                                                }
                                                else if ([galleryGender hasPrefix:@"m"] || [galleryGender hasPrefix:@"M"]) {
                                                    //male gender
                                                    currentStickerGender = maleStickerGender;
                                                }
                                            }
                                            
                                            //download stickers only for the user gender
                                            if ([galleryDictionary valueForKey:KEY_STICKERS] && [[galleryDictionary valueForKey:KEY_STICKERS] isKindOfClass:[NSArray class]]) {
                                                NSArray* stickerArray = [galleryDictionary valueForKey:KEY_STICKERS];
                                                
                                                //get the thumbnail images for the galleries
                                                [self downloadGalleryThumbnailForStickerDictionary:[stickerArray firstObject] gender:currentStickerGender galleryID:galleryID galleryPosition:[NSString stringWithFormat:@"%d",index] galleryRefreshBlock:galleryRefreshBlock];
                                                
                                                dispatch_sync(self.dbWriterQueue, ^{
                                                    for (int innerIndex = 0; innerIndex < stickerArray.count; innerIndex++) {
                                                        if ([[stickerArray objectAtIndex:innerIndex] isKindOfClass:[NSDictionary class]]) {
                                                            NSDictionary* stickerDictionary = [stickerArray objectAtIndex:innerIndex];
                                                            [dbManager updateStickerInfo:stickerDictionary withGender:currentStickerGender andGalleryID:galleryID galleryPosition:[NSString stringWithFormat:@"%d",index] stickerPosition:[NSString stringWithFormat:@"%d",innerIndex]];
                                                        }
                                                    }
                                                    
                                                    self.storedGalleryCount++;
                                                    
                                                    if (self.storedGalleryCount == galleryArray.count) {
                                                        //sticker info stored in DB
                                                        
                                                         BOOL isFemaleUser = [[TMUserSession sharedInstance].user isUserFemale];
                                                        
                                                        stickerGender userGender = isFemaleUser?femaleStickerGender:maleStickerGender;
                                                        
                                                        NSString* referenceTimeString = [NSString stringWithFormat:@"%ld",(long)0];
                                                        
                                                        //swap backup and main table
                                                        [dbManager exchangeBackupAndMainStickerTablesForReferenceTimestamp:(([TMDataStore containsObjectForKey:referenceTimeString])?[[TMDataStore retrieveObjectforKey:referenceTimeString] longLongValue]:0) userGender:userGender];
                                                        
                                                        //storing the sticker version
                                                        if ([responseDictionary valueForKey:STICKER_VERSION] && ![[responseDictionary valueForKey:STICKER_VERSION] isKindOfClass:[NSNull class]]) {
                                                            NSString* currentStickerVersion = [responseDictionary valueForKey:STICKER_VERSION];
                                                            
                                                            //update the sticker version
                                                            [TMDataStore setObject:currentStickerVersion forKey:CURRENT_STICKER_VERSION];
                                                        }
                                                    }
                                                    //refresh all the galleries
                                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            allGalleriesRefreshBlock();
                                                        });
                                                    });
                                                });
                                                for (int innerIndex = 0; innerIndex < stickerArray.count; innerIndex++) {
                                                    if ([[stickerArray objectAtIndex:innerIndex] isKindOfClass:[NSDictionary class]]) {
                                                        
                                                        NSMutableDictionary* stickerDictionary = [[stickerArray objectAtIndex:innerIndex] mutableCopy];
                                                      
                                                        [stickerDictionary setObject:galleryID forKey:@"gallery_id"];
                                                        
                                                        TMSticker* sticker = [[TMSticker alloc] initWithDictionary:stickerDictionary];
                                                        [self storeThumbnailImageForSticker:sticker stickerRefreshBlock:stickerRefreshBlock];
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }
        } failureBlock:^(TMError* error){
            //handle the error response
        }];
    });
}

////test method for sticker ordering

//- (NSArray *)reversedArrayForInputArray:(NSArray*) inputArray {
//    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[inputArray count]];
//    NSEnumerator *enumerator = [inputArray reverseObjectEnumerator];
//    for (id element in enumerator) {
//        [array addObject:element];
//    }
//    return array;
//}
////
////test method for sticker/gallery removal
//- (NSArray*) arrayByRemovingGalleryID:(NSString*) ID fromArray:(NSArray*) array{
//
//    NSMutableArray* modifiedArray = [array mutableCopy];
//
//    for (int index = 0; index < array.count; index++) {
//        if ([[array objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
//            NSDictionary* valueDictionary = [array objectAtIndex:index];
//            if ([valueDictionary valueForKey:@"id"] && [[valueDictionary valueForKey:@"id"] isKindOfClass:[NSString class]]) {
//                if ([[valueDictionary valueForKey:@"id"] isEqualToString:ID]) {
//                    [modifiedArray removeObjectAtIndex:index];
//                }
//            }
//        }
//    }
//
//    return [modifiedArray copy];
//}


- (NSArray*) getAllStickerGalleries
{
    //getting the user gender
    BOOL isFemaleUser = [[TMUserSession sharedInstance].user isUserFemale];
    
    TMStickerDBManager* stickerDBManager = [[TMStickerDBManager alloc] init];
    
    NSArray* stickerInfoArray = [stickerDBManager getAllStickerGalleryIDsForGender:isFemaleUser?femaleStickerGender:maleStickerGender];
    
    NSMutableArray* stickerDictArray;
    
    NSMutableArray* stickerObjectArray;
    
    if (stickerInfoArray) {
        for (int index = 0; index < stickerInfoArray.count; index++) {
            if ([[stickerInfoArray objectAtIndex:index] isKindOfClass:[NSString class]]) {
                NSString* galleryID = [stickerInfoArray objectAtIndex:index];
                
                //get the main gallery sticker
                NSDictionary* stickerGalleryThumbnailDictionary = [stickerDBManager getThumbnailStickerForGalleryID:galleryID];
            
                if (!stickerDictArray) {
                    stickerDictArray = [[NSMutableArray alloc] init];
                }
                if (stickerGalleryThumbnailDictionary) {
                     [stickerDictArray addObject:stickerGalleryThumbnailDictionary];
                }
            }
        }
        
        //sort on the basis of gallery position
        NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"gallery_position" ascending:YES];
        [stickerDictArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        for (int index = 0; index < stickerDictArray.count; index++) {
            if ([[stickerDictArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                NSDictionary* stickerGalleryThumbnailDictionary = [stickerDictArray objectAtIndex:index];
                TMSticker* stickerThumbnail = [[TMSticker alloc] initWithDictionary:stickerGalleryThumbnailDictionary];
                
                if (!stickerObjectArray) {
                    stickerObjectArray = [[NSMutableArray alloc] init];
                }
                [stickerObjectArray addObject:stickerThumbnail];
            }
        }
    }
    return stickerObjectArray;
}

- (UIImage*) getImageForSticker:(TMSticker*) sticker
{
    TMStickerFileManager* stickerFileManager = [[TMStickerFileManager alloc] init];
    
    if (sticker.thumbnailFilePathURL && [stickerFileManager isFilePresentAtPath:sticker.thumbnailFilePathURL]) {
        UIImage* image = [[UIImage alloc] initWithData:[stickerFileManager getFileAtPath:sticker.thumbnailFilePathURL]];
        //UIImage* image = [UIImage imageWithContentsOfFile:[stickerFileManager getStickerFilePathForSticker:sticker]];
        //UIImage* image = [UIImage imageWithWebPAtPath:[stickerFileManager getStickerFilePathForSticker:sticker]];
        if (image) {
             return image;
        }
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self storeThumbnailImageForSticker:sticker stickerRefreshBlock:^(int galleryID, int stickerID) {
            //do nothing
        }];
    });
    return nil;
}

- (UIImage*) getHDImageForSticker:(TMSticker*) sticker
{
    TMStickerFileManager* stickerFileManager = [[TMStickerFileManager alloc] init];
    
    if (sticker.hdFilePathURL && [stickerFileManager isFilePresentAtPath:sticker.hdFilePathURL]) {
        
        UIImage* image = [UIImage imageWithContentsOfFile:[stickerFileManager getStickerHDImageFilePathForSticker:sticker]];
        // UIImage* image = [UIImage imageWithWebPAtPath:[stickerFileManager getStickerHDImageFilePathForSticker:sticker]];
        
        return image;
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self storeHDImageForSticker:sticker stickerRefreshBlock:^(int galleryID, int stickerID) {
            //do nothing
        }];
    });
    return nil;
}

- (NSArray*) getAllStickerForStickerGalleryID:(NSString*) stickerGalleryID
{
    if (stickerGalleryID) {
        TMStickerDBManager* stickerDBManager = [[TMStickerDBManager alloc] init];
        
        NSArray* stickerInfoArray = [stickerDBManager getStickerGalleryForGalleryID:stickerGalleryID];
        
        NSMutableArray* stickerArray;
        
        for (int index = 0; index < stickerInfoArray.count; index++) {
            TMSticker* sticker = [[TMSticker alloc] initWithDictionary:[stickerInfoArray objectAtIndex:index]];
            
            // if (sticker.isActive && sticker.isUpdated) {
            if (!stickerArray) {
                stickerArray = [[NSMutableArray alloc] init];
            }
            [stickerArray addObject:sticker];
            //}
        }
        return stickerArray;
    }
    return nil;
}


- (NSArray*) getStickerBasedOnTimestamp {
    
     BOOL isFemaleUser = [[TMUserSession sharedInstance].user isUserFemale];
    
    stickerGender userGender = isFemaleUser?femaleStickerGender:maleStickerGender;
    
    NSString* referenceTimeString = [NSString stringWithFormat:@"%ld",(long)0];
    
    NSMutableArray* stickerInfoArray = [[[[TMStickerDBManager alloc] init] getStickersBasedOnTimestampWithRefrenceTimestamp:([TMDataStore containsObjectForKey:referenceTimeString])?[[TMDataStore retrieveObjectforKey:referenceTimeString] longLongValue]:0 userGender:userGender] mutableCopy];
    
    [stickerInfoArray sortUsingDescriptors:[NSArray arrayWithObjects:[[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO], nil]];
    
    NSMutableArray* stickerArray;
    
    for (int index = 0; index < stickerInfoArray.count; index++) {
        if ([[stickerInfoArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
            if (!stickerArray) {
                stickerArray = [[NSMutableArray alloc] init];
            }
            TMSticker* sticker = [[TMSticker alloc] initWithDictionary:[stickerInfoArray objectAtIndex:index]];
            [stickerArray addObject:sticker];
        }
    }
    return stickerArray;
}

- (BOOL) updateTimestampForStickerWithStickerID:(NSString*) stickerID galleryID:(NSString*) galleryID
{
    NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
    
    NSString* updatedTimeStamp = [NSString stringWithFormat:@"%ld",(long)timeInterval];
    
    if (stickerID && galleryID && updatedTimeStamp) {
        return [[[TMStickerDBManager alloc] init] updateTimestampForStickerWithStickerID:stickerID galleryID:galleryID updatedTimeStamp:updatedTimeStamp];
    }
    return NO;
}

#pragma mark - Sticker documents directory store methods

- (void) storeThumbnailImageForSticker:(TMSticker* ) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock {
    
    TMStickerFileManager* stickerFileManager = [[TMStickerFileManager alloc] init];
    TMStickerNetworkManager* stickerNetworkManager = [TMStickerNetworkManager getSharedNetworkManager];
    if (![stickerFileManager isFilePresentAtPath:sticker.thumbnailFilePathURL] || ![[UIImage alloc] initWithData:[stickerFileManager getFileAtPath:sticker.thumbnailFilePathURL]]) {
        //get the image data from server
        NSData* imageData = [stickerNetworkManager getImageDataForRelativePathURL:sticker.thumbnailQualityImageURLString];
        
        if (!imageData) {
            //image fetching failed
        }
        else {
            TMLOG(@"fetching image for gallery id :: %dsticker id :: %d",[sticker.galleryID intValue],[sticker.stickerID intValue]);
            //store the image data
            BOOL imageStorageStatus = [stickerFileManager storeStickerIfRequiredWithStickerInfo:sticker withImageData:imageData forImageFormat:imageFormatThumbnail];
            
            if (imageStorageStatus) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //refresh the caller gallery
                    stickerRefreshBlock([sticker.galleryID intValue],[sticker.stickerID intValue]);
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"imageDownloaded" object:sticker.thumbnailQualityImageURLString];
                });
            }
            else {
                //image storing failed
            }
        }
    }
}

- (void) storeHDImageForSticker:(TMSticker* ) sticker stickerRefreshBlock:(void(^)(int galleryID, int stickerID)) stickerRefreshBlock {
    
    TMStickerFileManager* stickerFileManager = [[TMStickerFileManager alloc] init];
    TMStickerNetworkManager* stickerNetworkManager = [TMStickerNetworkManager getSharedNetworkManager];
    if (![stickerFileManager isFilePresentAtPath:sticker.hdFilePathURL] || ![UIImage imageWithContentsOfFile:[stickerFileManager getStickerHDImageFilePathForSticker:sticker]]) {
        //get the image data from server
        NSData* imageData = [stickerNetworkManager getImageDataForRelativePathURL:sticker.highQualityImageURLString];
        
        TMLOG(@"fetching HD image for sticker id :: %d",[sticker.stickerID intValue]);
        //store the image data
        BOOL imageStorageStatus = [stickerFileManager storeStickerIfRequiredWithStickerInfo:sticker withImageData:imageData forImageFormat:imageFormatHD];
        
        if (imageStorageStatus) {
            //refresh the caller gallery
            stickerRefreshBlock([sticker.galleryID intValue],[sticker.stickerID intValue]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"imageDownloaded" object:sticker.highQualityImageURLString];
            });
        }
        else {
            //image storing failed
        }
    }
}

#pragma mark - Gallery thumbnail download method

- (void) downloadGalleryThumbnailForStickerDictionary:(NSDictionary*) stickerDictionary gender:(stickerGender) gender galleryID:(NSString*) galleryID galleryPosition:(NSString*) galleryPosition galleryRefreshBlock:(void(^)()) galleryRefreshBlock
{
    if (galleryID && [galleryID isKindOfClass:[NSString class]]) {
        
        //sticker position for thumbnail
        int stickerPosition = -1;
        
        NSMutableDictionary* galleryThumbnailDictionary = [[NSMutableDictionary alloc] initWithDictionary:stickerDictionary];
        [galleryThumbnailDictionary setValue:galleryID forKey:@"id"];
        [galleryThumbnailDictionary setValue:galleryID forKey:@"gallery_id"];
        
        TMStickerDBManager* dbManager = [[TMStickerDBManager alloc] init];
        
        dispatch_sync(self.dbWriterQueue, ^{
            [dbManager updateStickerInfo:galleryThumbnailDictionary withGender:gender andGalleryID:galleryID galleryPosition:galleryPosition stickerPosition:[NSString stringWithFormat:@"%d",stickerPosition]];
        });
        
        TMSticker* sticker = [[TMSticker alloc] initWithDictionary:galleryThumbnailDictionary];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self storeThumbnailImageForSticker:sticker stickerRefreshBlock:^(int galleryID, int stickerID){
                //do nothing
                dispatch_async(dispatch_get_main_queue(), ^{
                    galleryRefreshBlock();
                });
            }];
        });
    }
}

#pragma mark - Utility methods

- (void) checkForNewGalleryInResponseGalleryArray:(NSArray*) galleryArray {
    
    //getting the user gender
    BOOL isFemaleUser = [[TMUserSession sharedInstance].user isUserFemale];
    
    if (galleryArray && [galleryArray isKindOfClass:[NSArray class]]) {
        {
            NSArray* oldGalleryArray = [self getAllStickerGalleries];
            
            if (oldGalleryArray) {
                NSMutableArray* oldGalleryIDsArray;
                
                for (int index = 0; index < oldGalleryArray.count; index++) {
                    TMSticker* oldGalleryObject = [oldGalleryArray objectAtIndex:index];
                    if (!oldGalleryIDsArray) {
                        oldGalleryIDsArray = [[NSMutableArray alloc] init];
                    }
                    [oldGalleryIDsArray addObject:oldGalleryObject.galleryID];
                }
                
                BOOL hasNewGalleryBeenFound = NO;
                
                for (int index = 0; index < galleryArray.count && !hasNewGalleryBeenFound; index++) {
                    if ([[galleryArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary* galleryDictionary = [galleryArray objectAtIndex:index];
                        
                        NSString* galleryID = ([galleryDictionary valueForKey:@"id"] && [[galleryDictionary valueForKey:@"id"] isKindOfClass:[NSString class]])?([galleryDictionary valueForKey:@"id"]):nil;
                        
                        NSString* galleryGender = ([galleryDictionary valueForKey:@"gender"] && [[galleryDictionary valueForKey:@"gender"] isKindOfClass:[NSString class]])?[galleryDictionary valueForKey:@"gender"]:nil;
                        
                        BOOL isSameGenderSticker = NO;
                        
                        if ([galleryGender hasPrefix:@"c"] || [galleryGender hasPrefix:@"C"]) {
                            isSameGenderSticker = YES;
                        }
                        else {
                            if (isFemaleUser) {
                                if ([galleryGender hasPrefix:@"f"] || [galleryGender hasPrefix:@"F"]) {
                                    isSameGenderSticker = YES;
                                }
                            }
                            else {
                                if ([galleryGender hasPrefix:@"m"] || [galleryGender hasPrefix:@"M"]) {
                                    isSameGenderSticker = YES;
                                }
                            }
                        }
                        
                        if (isSameGenderSticker && galleryID && ![oldGalleryIDsArray containsObject:galleryID]) {
                            //blooper logic
                            
                            //store the info in user defaults
                            [TMDataStore setObject:@YES forKey:STICKER_BLOOPER_TO_BE_SHOWN];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"newStickerAvailable" object:nil];
                            hasNewGalleryBeenFound = YES;
                        }
                    }
                }
            }
            else {
                //blooper logic
                
                //store the info in user defaults
                [TMDataStore setObject:@YES forKey:STICKER_BLOOPER_TO_BE_SHOWN];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newStickerAvailable" object:nil];
            }
        }
    }
}

- (BOOL) isValidGalleryResponse:(NSArray*) galleryArray {
    if (galleryArray && galleryArray.count > 1) {
        
        BOOL isInvalidGalleryFound = NO;
        
        for (int index = 0; index < galleryArray.count && !isInvalidGalleryFound; index++) {
            if ([[galleryArray objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                NSDictionary* galleryDictionary = [galleryArray objectAtIndex:index];
                if ([galleryDictionary valueForKey:KEY_STICKERS] && [[galleryDictionary valueForKey:KEY_STICKERS] isKindOfClass:[NSArray class]]) {
                    NSArray* stickerArray = [galleryDictionary valueForKey:KEY_STICKERS];
                    if (!stickerArray || stickerArray.count < 5) {
                        //low sticker count
                        isInvalidGalleryFound = YES;
                    }
                }
                else {
                    //non array object
                    return NO;
                }
            }
            else {
                //non dictionary object
                return NO;
            }
        }
        if (!isInvalidGalleryFound) {
            return YES;
        }
        else {
            return NO;
        }
    }
    else {
        //low gallery count
        return NO;
    }
}

@end
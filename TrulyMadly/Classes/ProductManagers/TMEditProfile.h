//
//  TMEditProfile.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/12/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;

//typedef void (^ResponseBlock)(NSDictionary *response ,TMError *error);
//typedef void (^UpdateBlock)(NSString *response ,TMError *error);

@interface TMEditProfile : TMBaseManager

-(void)initCall:(NSDictionary*)queryString with:(void(^)(NSDictionary *response ,TMError *error))responseBlock;

-(void)update:(NSDictionary*)queryString with:(void(^)(NSString *response ,TMError *error))updateBlock;

- (BOOL) isCachedResponsePresent;

-(void)updateBadWordList;

@end

//
//  TMSettingsManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 14/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@class TMError;

typedef void (^SettingBlock)(BOOL status, TMError *error);
typedef void (^DeleteBlock)(BOOL status, TMError *error);

@interface TMSettingsManager : TMBaseManager

-(void)updateProfileDiscovery:(NSDictionary*)queryString with:(void(^)(BOOL status, TMError *error))settingBlock;

-(void)deleteUserProfile:(NSDictionary*)queryString with:(void(^)(BOOL status, TMError *error))deleteBlock;

@end

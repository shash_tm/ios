//
//  TMPhotoManager1.m
//  TrulyMadly
//
//  Created by Chetan Bhardwaj on 17/11/14.
//  Copyright (c) 2014 trulymadly. All rights reserved.
//

#import "TMPhotoManager.h"
#import "TMSwiftHeader.h"
#import "TMAppResponseCachingManager.h"


@implementation TMPhotoManager

-(void)getPhotoData:(PhotoBlock)responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_PHOTO_DETAILS];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"display_pics" forKey:@"action"];
    
    BOOL isCachePresent = false;
    
    //caching to be done here
    if ([TMAppResponseCachingManager containsCachedResponseForURL:request.urlString]) {
        isCachePresent = true;
        //update the UI with the old cached response
        responseBlock([[TMPhotoResponse alloc] initWithResponse:[TMAppResponseCachingManager getCachedResponseForURL:request.urlString]],nil);
        
        //add the hash value & tstamp to the request dict
        NSString* hashValue = [TMAppResponseCachingManager getHashValueForURL:request.urlString];
        
        NSString* tstampValue = [TMAppResponseCachingManager getTStampValueForURL:request.urlString];
        
        if (hashValue && [hashValue isValidObject]) {
            [dict setObject:hashValue forKey:@"hash"];
            if (tstampValue && [tstampValue isValidObject]) {
              [dict setObject:tstampValue forKey:@"tstamp"];   
            }
        }
    }
    
    [request setGetRequestParams:dict];
    
    NSDate* startDate = [NSDate date];
    
    [self executeGETAPI:request success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
        [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            responseBlock(photoResponse,error);
            if (photoResponse) {
                //200 OK response received
                //cache the response
                NSString* hashValue = [responseObject valueForKey:@"hash"];
                NSString* tstampValue = [responseObject valueForKey:@"tstamp"];
                //cache the updated response
                [TMAppResponseCachingManager setCachedResponse:responseObject forURL:request.urlString hash:hashValue tstamp:tstampValue];
            }
        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        if(!isCachePresent) {
            [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
                responseBlock(photoResponse,error);
            }];
        }
    }];
}

-(void)deletePhotoWithId:(NSString*)photoId photoResponse:(PhotoBlock)responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_PHOTO_DETAILS];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"delete" forKey:@"action"];
    [dict setObject:photoId forKey:@"id"];
    [request setPostRequestParameters:dict];
    
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_GET_PHOTO_DETAILS];
    
    [self executePOSTAPI:request success:^(NSURLSessionDataTask *task, id responseObject) {
        TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
        [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            responseBlock(photoResponse,nil);
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            responseBlock(nil,error);
        }];
    }];
}

-(void)setPhotoAsProfilePicWithId:(NSString*)photoId photoResponse:(PhotoBlock)responseBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_PHOTO_DETAILS];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"set_as_profile" forKey:@"action"];
    [dict setObject:photoId forKey:@"id"];
    [request setPostRequestParameters:dict];
    
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_GET_PHOTO_DETAILS];
    
    [self executePOSTAPI:request success:^(NSURLSessionDataTask *task, id responseObject) {
        TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
        [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            responseBlock(photoResponse,nil);
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            responseBlock(nil,error);
        }];
    }];
}

//-(void)uploadPhotoFromFacebook:(NSDictionary*)fbQueryParams photoUrl:(NSString*)photoUrl
//                      response:(PhotoBlock)responseBlock {
//    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
//    [request setPostRequestParameters:fbQueryParams];
//    
//    [self executePOSTAPI:request success:^(NSURLSessionDataTask *task, id responseObject) {
//        TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
//        [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
//            if(photoResponse) {
//                NSArray *images = [NSArray arrayWithObject:photoUrl];
//               
//            }
//            else {
//                responseBlock(nil,error);
//            }
//        }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
//            responseBlock(nil,error);
//        }];
//    }];
//}

-(void)uploadFacebookPhotoUrls:(NSArray*)photoUrls response:(PhotoBlock)photoBlock {
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_PHOTO_DETAILS];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"iOS" forKey:@"app"];
    [dict setObject:@"saveFb" forKey:@"action"];
    [dict setObject:photoUrls forKey:@"data"];
    [request setPostRequestParameters:dict];
    
    NSDate* startDate = [NSDate date];
    [self executePOSTAPI:request
                 success:^(NSURLSessionDataTask *task, id responseObject) {
                     NSDate* endDate = [NSDate date];
                     [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                     [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                     
        TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
                     [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
                         photoBlock(photoResponse,nil);
                     }];
                     
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            photoBlock(nil,error);
        }];
    }];
}


-(NSURLSessionDataTask*)uploadPhoto:(NSData*)photoData response:(PhotoBlock)responseBlock {
    //NSLog(@"File size is : %.2f KB",(float)photoData.length/1024.0f);
    //NSLog(@"%@",[NSByteCountFormatter stringFromByteCount:photoData.length countStyle:NSByteCountFormatterCountStyleFile]);
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_PHOTO_DETAILS];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"add_from_computer_without_file_reader_api" forKey:@"action"];
    [request setPostRequestParameters:dict];
    
    NSDate* startDate = [NSDate date];

    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_GET_PHOTO_DETAILS];

    NSURLSessionDataTask* task = [self uploadDataAPI:photoData withRequest:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
                    [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
                        responseBlock(photoResponse,nil);
                    }];
                    
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSDate* endDate = [NSDate date];
        [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
        [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
        
        [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
            responseBlock(nil,error);
        }];
    }];
    
    return task;
}

-(NSURLSessionDataTask*)uploadPhotoWithId:(NSString *)photoId photoData:(NSData *)photoData response:(PhotoBlock)photoBlock {
    //NSLog(@"File size is : %.2f KB",(float)photoData.length/1024.0f);
    //NSLog(@"%@",[NSByteCountFormatter stringFromByteCount:photoData.length countStyle:NSByteCountFormatterCountStyleFile]);
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_GET_PHOTO_DETAILS];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:16];
    [dict setObject:@"true" forKey:@"login_mobile"];
    [dict setObject:@"edit_photo" forKey:@"action"];
    [dict setObject:photoId forKey:@"photo_id"];
    [request setPostRequestParameters:dict];
    
    NSDate* startDate = [NSDate date];
    
    [TMAppResponseCachingManager deleteCachedDataForURL:KAPI_GET_PHOTO_DETAILS];
    
    NSURLSessionDataTask* task = [self uploadDataAPI:photoData withRequest:request
                success:^(NSURLSessionDataTask *task, id responseObject) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    TMPhotoResponse *photoResponse = [[TMPhotoResponse alloc] initWithResponse:responseObject];
                    [self sendPhotoResponse:photoResponse withError:nil inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
                        photoBlock(photoResponse,nil);
                    }];
                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                    NSDate* endDate = [NSDate date];
                    [self.trackEventDictionary setObject:startDate  forKey:@"startDate"];
                    [self.trackEventDictionary setObject:endDate  forKey:@"endDate"];
                    
                    [self sendPhotoResponse:nil withError:error inBlock:^(TMPhotoResponse *photoResponse, TMError *error) {
                        photoBlock(nil,error);
                    }];
                }];
    
    return task;
}

-(void)sendPhotoResponse:(TMPhotoResponse*)photoResponse withError:(NSError*)error inBlock:(PhotoBlock)photoResponseBlock {
    
    if(photoResponse && !error) {
        NSInteger responseCode= photoResponse.serverResponseCode;
        if((photoResponse) && (responseCode == 200)) {
            [self.trackEventDictionary setObject:@"success"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            
            photoResponseBlock(photoResponse,nil);
        }
        else if(responseCode == TMRESPONSECODE_LOGOUT) {
            [self.trackEventDictionary setObject:@"error401"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            // logout the user
            TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
            photoResponseBlock(nil,tmerror);
        }
        else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
            [self.trackEventDictionary setObject:@"data_save_error"  forKey:@"status"];
            [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
            [self.trackEventDictionary removeAllObjects];
            self.trackEventDictionary = nil;
            // error
            TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
            photoResponseBlock(nil,tmerror);
        }
    }
    else {
        [self.trackEventDictionary setObject:@"error_timeout"  forKey:@"status"];
        [[TMAnalytics sharedInstance] trackNetworkEvent:self.trackEventDictionary];
        [self.trackEventDictionary removeAllObjects];
        self.trackEventDictionary = nil;
        
        TMError *tmerror = [[TMError alloc] initWithError:error];
        photoResponseBlock(nil,tmerror);
    }
}

-(void) verifyFacebook:(NSDictionary *)queryString with:(PhotoFacebookBlock)facebookBlock{
    
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_TRUSTBUILDER_RELATIVEPATH];
    [request setPostRequestParameters:queryString];
    
    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        
        if(response) {
            NSInteger responseCode = [response responseCode];
            if(responseCode == TMRESPONSECODE_SUCCESS) {
                facebookBlock(true,nil);
            }
            else if(responseCode == TMRESPONSECODE_LOGOUT) {
                TMError *tmerror = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_LOGOUT];
                facebookBlock(false,tmerror);
            }
            else if(responseCode == TMRESPONSECODE_DATASAVEERROR) {
                NSString *msg = [response error];
                TMError *tmError = [[TMError alloc] initWithResponseCode:TMRESPONSECODE_DATASAVEERROR];
                tmError.errorMessage = msg;
                facebookBlock(false,tmError);
            }
        }
        else {
            TMError *tmerror = [[TMError alloc] initWithError:error];
            facebookBlock(nil,tmerror);
        }
    }];
}

- (BOOL) isCachedResponsePresent {
    if ([TMAppResponseCachingManager getCachedResponseForURL:KAPI_GET_PHOTO_DETAILS]) {
        return YES;
    }
    return NO;
}

@end



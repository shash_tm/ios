
//
//  TMAnalytics.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMAnalytics.h"
#import "GAI.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "TMLogManager.h"
#import "TMLogDB.h"
#import "TMUserSession.h"
#import "TMDataStore.h"
#import "TMLog.h"
#import "TMCommonUtil.h"
#import "MoEngage.h"
#import "TMSocketSession.h"
#import <FacebookSDK/FacebookSDK.h>


@interface TMAnalytics()<TMLogManagerDelegate>

@property(nonatomic,weak)NSTimer *autoCompleteTimer;
@property(nonatomic,strong)NSOperationQueue *queue;
@property(nonatomic,strong)TMLogManager *logMngr;
@property(nonatomic,strong)TMLogDB *logdb;
@property(nonatomic,strong)NSDateFormatter *dateFormatter;

@end

@implementation TMAnalytics

-(instancetype)init {
    self = [super init];
    if(self) {
        self.dispatchInterval = 5;
        self.queue = [NSOperationQueue new];
        self.queue.maxConcurrentOperationCount = 1;
        self.logMngr = [[TMLogManager alloc] init];
        self.logMngr.delegate = self;
        self.logdb = [[TMLogDB alloc] init];
        //self.logdb.delegate = self;
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [self.dateFormatter setTimeZone:gmt];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:self.dispatchInterval target:self selector:@selector(sendLogToServer) userInfo:nil repeats:YES];
    }
    return self;
}

+ (TMAnalytics*)sharedInstance
{
    static TMAnalytics *_sharedAPIMgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAPIMgr = [[TMAnalytics alloc] init];
    });
    
    return _sharedAPIMgr;
}

-(void)trackEvent:(NSString *)screenName eventCategory:(NSString *)eventCategory eventAction:(NSString *)eventAction eventLabel:(NSString*)label {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName  value:screenName];
    
    TMUserSession *userSession = [TMUserSession sharedInstance];
    
    NSString* gender = userSession.user.gender;
    if(gender) {
        [tracker set:[GAIFields customDimensionForIndex:1] value:gender];
    }
    
    NSString * city =  [userSession.user city];
    if(city) {
        [tracker set:[GAIFields customDimensionForIndex:2] value:city];
    }
    
    int age = userSession.user.age;
    if(age) {
        NSString *myAge = [NSString stringWithFormat:@"%d", age];
        [tracker set:[GAIFields customDimensionForIndex:3] value:myAge];
    }
    
    NSString * userId = [userSession.user userId];
    if(userId) {
        [tracker set:@"&uid" value:userId];
        [tracker set:[GAIFields customDimensionForIndex:4] value:userId];
    }
    
    NSString *networkClass = [self networkType];
    if(networkClass) {
        [tracker set:[GAIFields customDimensionForIndex:5] value:networkClass];
    }
    
    NSString *socketAbPrefix = [TMSocketSession abPrefixString];
    if(socketAbPrefix) {
        [tracker set:[GAIFields customDimensionForIndex:6] value:socketAbPrefix];
    }
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:eventCategory
                                                          action:eventAction
                                                           label:label
                                                           value:nil] build]];
    [tracker set:kGAIScreenName value:nil];
    
}

-(void)trackFacebookEvent:(NSString *)action withParams:(NSDictionary *)data {
    if([action isEqualToString:@"registration"] && data != nil) {
        NSString *registrationScreen = data[@"register_method"];
        [FBAppEvents logEvent:FBAppEventNameCompletedRegistration
                   parameters:@{FBAppEventParameterNameRegistrationMethod:registrationScreen}];
    }else if(([action isEqualToString:@"buy_spark"] || [action isEqualToString:@"buy_select"]) && data != nil) {
        double amount = [data[@"amount"] doubleValue];
        NSNumber *numberOfItems = data[@"number_of_items"];
        NSString *currency = data[@"currency"];
        NSString *contentType = data[@"content_type"];
        NSString *contentID = data[@"package_identifier"];
        [FBAppEvents logPurchase:amount currency:currency
                      parameters:@{FBAppEventParameterNameNumItems:numberOfItems,   FBAppEventParameterNameContentType:contentType ,
                                       FBAppEventParameterNameContentID:contentID}];
    }else if([action isEqualToString:@"spark_sent"]){
        [FBAppEvents logEvent:FBAppEventNameSpentCredits
                   parameters:@{FBAppEventParameterNameContentType:@"Sparks"}];
    }
}

-(void)trackNetworkEvent:(NSDictionary *)data {
    
    // GA tracking
    NSString *label = (data[@"label"]) ? data[@"label"] : @"ios_app";
    [self trackEvent:data[@"screenName"] eventCategory:data[@"eventCategory"] eventAction:data[@"eventAction"] eventLabel:label];
    
    
    NSMutableDictionary *dict = [self finalDictToInsert:data];
    if(dict)
    {
        // convert nsdictionary into nsdata before insert into table
        NSData *theDictionaryData = [NSKeyedArchiver archivedDataWithRootObject:dict];
        
        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                selector:@selector(insertData:)
                                                                                  object:theDictionaryData];
        [self.queue addOperation:operation];
    }
}
-(void)trackSparkEvent:(NSString*)eventAction status:(NSString*)status eventInfo:(NSDictionary*)eventInfo {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"sparks";
    eventDictionary[@"eventAction"] = eventAction;
    eventDictionary[@"status"] = status;
    eventDictionary[@"label"] = status;
    if(eventInfo) {
        eventDictionary[@"event_info"] = eventInfo;
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}
-(void)trackSelectEvent:(NSString*)eventAction status:(NSString*)status eventInfo:(NSDictionary*)eventInfo {
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"select";
    eventDictionary[@"eventAction"] = eventAction;
    eventDictionary[@"status"] = status;
    eventDictionary[@"label"] = status;
    if(eventInfo) {
        eventDictionary[@"event_info"] = eventInfo;
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}
-(NSMutableDictionary *)finalDictToInsert:(NSDictionary *)data {
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    
    if(data[@"time_taken"]) {
        [dict setObject:data[@"time_taken"] forKey:@"time_taken"];
    }
    else {
        [dict setObject:@"0" forKey:@"time_taken"];
    }
    
    if (data[@"startDate"] && data[@"endDate"]) {
        int timeDiff = (int)(([data[@"endDate"] timeIntervalSinceDate:data[@"startDate"]])*1000);
        float time = [data[@"endDate"] timeIntervalSinceDate:data[@"startDate"]];
        [dict setObject:@(time).stringValue forKey:@"time_taken"];
        [self trackAppSpeed:timeDiff eventCategory:data[@"eventCategory"] eventAction:data[@"eventAction"]];
    }
    
    // data[@"GA"] should be true if it is not for TM server
    if (!data[@"GA"] && data[@"eventCategory"] && data[@"eventAction"]) {
        
        [dict setObject:data[@"eventCategory"] forKey:@"activity"];
        [dict setObject:data[@"eventAction"] forKey:@"event_type"];
        
        if(data[@"status"]) {
            [dict setValue:data[@"status"] forKey:@"event_status"];
        }
        
        NSString *stringDate = [self.dateFormatter stringFromDate:[NSDate date]];
        NSString *deviceId = [TMCommonUtil vendorId];
        NSString *systemVesrion = [[UIDevice currentDevice] systemVersion];
        NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
        version = [version stringByAppendingString:@"."];
        version = [version stringByAppendingString:build];
        [dict setObject:@"ios_app" forKey:@"source"];
        //for chat ab
        NSString *socketAbPrefix = [TMSocketSession abPrefixString];
        if(socketAbPrefix) {
            version = [NSString stringWithFormat:@"%@_%@",version,socketAbPrefix];
        }
        [dict setObject:version forKey:@"sdk_version"];
        [dict setObject:systemVesrion forKey:@"os_version"];
        [dict setObject:deviceId forKey:@"device_id"];
        [dict setObject:stringDate forKey:@"tstamp"];
        if([[TMUserSession sharedInstance].user userId]) {
            [dict setObject:[[TMUserSession sharedInstance].user userId] forKey:@"user_id"];
        }
        if(data[@"event_info"]) {
            NSDictionary *eventInfoParmas = data[@"event_info"];
            NSMutableDictionary *event = [NSMutableDictionary dictionary];
            [event addEntriesFromDictionary:eventInfoParmas];
            [event setObject:stringDate forKey:@"local_tstamp"];
            [event setObject:[self networkType] forKey:@"NetworkClass"];
            [dict setObject:event forKey:@"event_info"];
        }else {
            NSMutableDictionary *event = [[NSMutableDictionary alloc] init];
            [event setValue:stringDate forKey:@"local_tstamp"];
            [event setObject:[self networkType] forKey:@"NetworkClass"];
            [dict setObject:event forKey:@"event_info"];
        }
        return dict;
    }
    
    return nil;
}

-(void)trackView:(NSString *)screenName {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [tracker set:kGAIScreenName value:nil];
}

-(void)trackAppSpeed:(int)loadTime eventCategory:(NSString *)eventCategory eventAction:(NSString *)eventAction {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSNumber* timeInterval = [NSNumber numberWithInt:loadTime];
    [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:eventCategory interval:timeInterval name:eventAction label:@"ios_app"] build]];
    
}

-(void)trackInstall {
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    version = [version stringByAppendingString:@"."];
    version = [version stringByAppendingString:build];
    
    NSString *appInstall = [TMDataStore retrieveObjectforKey:@"install"];
    if(appInstall == nil) {
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setValue:@"install" forKey:@"eventCategory"];
        [dict setValue:@"install" forKey:@"eventAction"];
        [self trackNetworkEvent:dict];
        [TMDataStore setObject:@"true" forKey:@"install"];
        [TMDataStore setObject:version forKey:@"update"];
        [TMDataStore synchronize];
        
        [[MoEngage sharedInstance]appStatus:INSTALL];
        
    }
    else {
        NSString *oldVer = [TMDataStore retrieveObjectforKey:@"update"];
        if (![version isEqualToString:oldVer]) {
            NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
            [dict setValue:@"update" forKey:@"eventCategory"];
            [dict setValue:@"update" forKey:@"eventAction"];
            [self trackNetworkEvent:dict];
            [TMDataStore setObject:version forKey:@"update"];
            [TMDataStore synchronize];
            
            [[MoEngage sharedInstance]appStatus:UPDATE];
        }
    }
}

#pragma mark - Notification Handler

-(void)appWillEnterForeground:(NSNotification*)notification {
    if(!self.autoCompleteTimer) {
        self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:self.dispatchInterval target:self selector:@selector(sendLogToServer) userInfo:nil repeats:YES];
    }
}

-(void)appDidEnterBackground:(NSNotification*)notification {
    if(self.autoCompleteTimer) {
        [self.autoCompleteTimer invalidate];
        self.autoCompleteTimer = nil;
    }
}

-(void) sendLogToServer {

    if(!self.logMngr.requestInProgress) {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                selector:@selector(sendEvent)
                                                                                  object:self.logMngr];
        [self.queue addOperation:operation];
    }
    
}

#pragma mark - background thread insert function

-(void)insertData:(NSData*)data {
    [self.logdb insertEvent:data];
}

#pragma mark - background thread select function

-(void)sendEvent {
    @try {
        self.logMngr.requestInProgress = TRUE;
        NSMutableArray * dictArr = [self.logdb fetchEvent];
        if(dictArr.count>0) {
            NSError *writeError = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictArr options:NSJSONWritingPrettyPrinted error:&writeError];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
            NSMutableDictionary *dictToSend = [[NSMutableDictionary alloc] init];
            [dictToSend setObject:jsonString forKey:@"data"];
            [dictToSend setObject:@"application/json" forKey:@"content_type"];
            
            [self selectOperationDidFinish:dictToSend];
        }
        else {
            self.logMngr.requestInProgress = FALSE;
        }
    }
    @catch (NSException *exception) {
       // NSMutableArray * dictArr = [self.logdb fetchEvent];
        TMLOG(@"exception:%@",exception);
    }
    @finally {
        //TMLOG(@"finally");
    }
}

#pragma mark - background thread delete function

-(void)deleteLog{
    [self.logdb deleteEvent];
}

#pragma mark - tm log manager delegate

-(void)networkOperationDidFinish:(BOOL)status {
    if(status) {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                selector:@selector(deleteLog)
                                                                                  object:nil];
        [self.queue addOperation:operation];
    }
}

#pragma mark - log db delegate

-(void)selectOperationDidFinish:(NSMutableDictionary *)dict {
    if([self.logMngr isNetworkReachable]) {
        //self.logMngr.requestInProgress = TRUE;
        [self.logMngr logEvent:dict];
    }
    else {
        self.logMngr.requestInProgress = false;
    }
    
}

-(void)trackNotification:(NSString *)eventType eventStatus:(NSString *)eventStatus {
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setValue:@"push" forKey:@"eventCategory"];
    [dict setValue:eventType forKey:@"eventAction"];
    [dict setValue:eventStatus forKey:@"status"];
    [self trackNetworkEvent:dict];
    
    if([eventType isEqualToString:@"push_open"]) {
        [dict setValue:@"push_received" forKey:@"eventAction"];
        [self trackNetworkEvent:dict];
    }

}

-(NSString*)networkType{
    return [[TMUserSession sharedInstance] currentConnectedNetwork];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

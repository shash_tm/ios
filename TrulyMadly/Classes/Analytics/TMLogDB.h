//
//  TMLogDB.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 16/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

//@protocol TMLogDBDelegate;

@interface TMLogDB : NSObject

//@property (nonatomic, assign) id <TMLogDBDelegate> delegate;
@property(nonatomic,strong)NSMutableArray *ids;

-(NSMutableArray *)fetchEvent;

-(void)insertEvent:(NSData*)data;

-(void)deleteEvent;

@end

//@protocol TMLogDBDelegate <NSObject>
//
//- (void)selectOperationDidFinish:(NSMutableDictionary*)dict;
//
//@end


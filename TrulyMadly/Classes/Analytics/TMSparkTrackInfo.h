//
//  TMSparkTrackInfo.h
//  TrulyMadly
//
//  Created by Ankit on 07/07/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMSparkTrackInfo : NSObject

@property(nonatomic,assign)BOOL sparkActionFromIntro;
@property(nonatomic,assign)BOOL didShowDefaultCommonIntro;
@property(nonatomic,assign)BOOL didSwipeCommonIntroText;
@property(nonatomic,assign)BOOL didShowMessageTextAlert;
@property(nonatomic,assign)BOOL didViewReceivedSparkProfile;

@property(nonatomic,assign)NSInteger matchId;
@property(nonatomic,assign)NSInteger noOfProfileLiked;
@property(nonatomic,assign)NSInteger noOfProfileHide;
@property(nonatomic,assign)NSInteger noOfProfileSparked;
@property(nonatomic,assign)NSInteger sparkLeft;
@property(nonatomic,assign)NSInteger sparkSendMessageLength;
@property(nonatomic,assign)NSInteger sparkExpiryTimeLeft;

@property(nonatomic,strong)NSString* packageId;
@property(nonatomic,strong)NSString* sparkAction;
@property(nonatomic,strong)NSString* receivedSparkProfileViewSource;
@property(nonatomic,strong)NSString* sparkMessageReceived;
@property(nonatomic,strong)NSString* sendSparkRequestCompletionTime;
@property(nonatomic,strong)NSString* sendSparkRequestError;

@end

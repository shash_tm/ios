//
//  TMAnalytics.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 19/02/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAITrackedViewController.h"


@interface TMAnalytics : GAITrackedViewController

@property(nonatomic, assign) NSTimeInterval dispatchInterval;

+(TMAnalytics*)sharedInstance;

-(void)trackInstall;

-(void)trackFacebookEvent:(NSString *)action withParams:(NSDictionary *)data;

-(void)trackNetworkEvent:(NSDictionary*)data;

-(void)trackNotification:(NSString*)eventType eventStatus:(NSString*)eventStatus;

-(void)trackEvent:(NSString *)screenName eventCategory:(NSString *)eventCategory eventAction:(NSString *)eventAction eventLabel:(NSString*)label;

-(void)trackView:(NSString*)screenName;

-(void)trackAppSpeed:(int)loadTime eventCategory:(NSString*)eventCategory eventAction:(NSString*)eventAction;

-(void)trackSparkEvent:(NSString*)eventAction status:(NSString*)status eventInfo:(NSDictionary*)eventInfo;

-(void)trackSelectEvent:(NSString*)eventAction status:(NSString*)status eventInfo:(NSDictionary*)eventInfo;

@end

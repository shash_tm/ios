//
//  TMLogManager.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 16/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMLogManager.h"
#import "TrulyMadly-Swift.h"
#import "TMLog.h"

@implementation TMLogManager

-(void)logEvent:(NSDictionary *)dict {
    //TMLOG(@"dict is %@",dict);
    TMRequest *request = [[TMRequest alloc] initWithUrlString:KAPI_LOG_EVENT];
    
    [request setPostRequestParameters:dict];
    
    self.requestInProgress = true;

    [self executePOSTRequest:request responseBlock:^(TMResponse *response, NSError *error) {
        self.requestInProgress = false;
        if(response) {
            // delete from db
            [self.delegate networkOperationDidFinish:true];
            //[self.logdb deleteEvent];
        }
    }];

}

@end

//
//  TMLogDB.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 16/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMLogDB.h"
#import "FMDB.h"
#import "TMLog.h"

@implementation TMLogDB


#pragma mark Initialization Methods
#pragma mark -

-(instancetype)init {
    self = [super init];
    if(self) {
        //check if message db is not created then create it
        
    }
    return self;
}

-(BOOL)initializeDatabase {
    
    bool databaseDirectoryPathStatus = [[NSFileManager defaultManager] fileExistsAtPath:[self databaseDirectoryPath]];
    if(!databaseDirectoryPathStatus) {
        NSError *error = nil;
        BOOL status = [[NSFileManager defaultManager] createDirectoryAtPath:[self databaseDirectoryPath] withIntermediateDirectories:YES attributes:nil error:&error];
        if(status && !error) {
            ///create db
            sqlite3 *database = [self newConnection];
            if (database) {
                [self closeConnection:database];
                return true;
            }
        }
        else {
            return false;
        }
    }
    return true;
}
-(sqlite3 *)newConnection
{
    sqlite3 *database = NULL;
    NSString *path = [self databaseFilePath];
    
    ////////
    // the database file does not exist, so we need to create one.
    if (sqlite3_open([path UTF8String], &database) != SQLITE_OK)
    {
        //NSLog(@"Failed to open the database with error %s", sqlite3_errmsg(database));
        sqlite3_close(database);
        database = NULL;
    }
    
    //warning, the caller is expected to free this pointer.
    return database;
}

-(void) closeConnection:(sqlite3 *)database
{
    if(database)
    {
        if (sqlite3_close(database) != SQLITE_OK )
        {
            //NSLog(@"Failed to close the database with error %s", sqlite3_errmsg(database));
        }
        database = NULL;
    }
}
-(NSString*)databaseDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"tm/db"];
    return databasePath;
}
-(NSString*)databaseFilePath {
    NSString *databasePath = [self databaseDirectoryPath];
    NSString *databaseFilePath = [databasePath stringByAppendingPathComponent:@"log.db"];
    return databaseFilePath;
}
-(FMDatabase*)messageDatabase {
    NSString *dbPath = [self databaseFilePath];
    //TMLOG(@"db path %@",dbPath);
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return db;
}


-(void)insertEvent:(NSData *)data {
    BOOL isDatabaseInitialized = [self initializeDatabase];
    if(!isDatabaseInitialized) {
        TMLOG(@"Error in initializing database");
    }
    
    FMDatabase *db = [self messageDatabase];
    [db open];
    
    ////////
    NSString *query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\" (\"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\"event\" BLOB)",@"action_log"];
    
    BOOL isQueryExecuted = [db executeUpdate:query];
    if(!isQueryExecuted) {
        TMLOG(@"Error in action_log table::%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    NSString *insertQuery = @"insert into action_log(event) values (?)";
    BOOL insertStatus = [db executeUpdate:insertQuery, data];
    if(!insertStatus) {
        TMLOG(@"update_action_log_InfoTableWithConfiguration Inserting query error%@ %@",db.lastError,db.lastErrorMessage);
    }
    [db close];

}


-(NSMutableArray *)fetchEvent {
    
    NSMutableArray *dictArr = [[NSMutableArray alloc] init];
    self.ids = [[NSMutableArray alloc] init];
//    NSMutableDictionary *dictToSend = [[NSMutableDictionary alloc] init];
    FMDatabase *db = [self messageDatabase];
    [db open];
    
    NSString *selectQuery = @"select id,event from action_log";
    FMResultSet *rs = [db executeQuery:selectQuery];
    
    
    while ([rs next]) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithData:[rs dataForColumn:@"event"]]];
        [dictArr addObject:dict];
        [self.ids addObject:[NSNumber numberWithInt:[rs intForColumn:@"id"]]];
    }
    
    //TMLOG(@"ids selected %@",self.ids);
    
    [rs close];
    
    [db close];
    
    return dictArr;
   
//    if(dictArr.count>0) {
//        NSError *writeError = nil;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictArr options:NSJSONWritingPrettyPrinted error:&writeError];
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        
//        [dictToSend setObject:jsonString forKey:@"data"];
//        [dictToSend setObject:@"application/json" forKey:@"content_type"];
//        
//        [self.delegate selectOperationDidFinish:dictToSend];
//    }

}

-(void)deleteEvent {
    
    FMDatabase *db = [self messageDatabase];
    [db open];
    
    NSString *deleteQuery = @"delete from action_log where id in (";
    deleteQuery = [deleteQuery stringByAppendingString:[self.ids componentsJoinedByString:@","]];
    deleteQuery = [deleteQuery stringByAppendingString:@")"];
    
    BOOL deleteStatus = [db executeUpdate:deleteQuery];
    TMLOG(@"delete result is %@",deleteQuery);
    if(!deleteStatus) {
        TMLOG(@"update_action_log_InfoTableWithConfiguration Inserting query error%@ %@",db.lastError,db.lastErrorMessage);
    }
    
    [db close];
    
}


@end

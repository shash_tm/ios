//
//  TMSocketTracking.m
//  TrulyMadly
//
//  Created by Ankit on 09/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMSocketTracking.h"
#import "TMAnalytics.h"
#import "TMUserSession.h"

@implementation TMSocketTracking

+(void)trackMovingToPollingFromSocketEvent {
    [TMSocketTracking trackSocketEvent:@"connection"
                                status:@"failed_reconnections-switching_to_polling"
                       eventInfoParams:nil
                           eventParams:nil
                             sendToAll:TRUE];
}

+(void)trackSocketEvent:(NSString*)eventAction
           status:(NSString*)status
        eventInfoParams:(NSDictionary*)eventInfoParams
            eventParams:(NSDictionary*)params
              sendToAll:(BOOL)sendToAll {
    
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"eventCategory"] = @"socket";
    eventDictionary[@"eventAction"] = eventAction;
    if(eventInfoParams) {
        eventDictionary[@"event_info"] = eventInfoParams;
    }
    
    if(status) {
        [eventDictionary addEntriesFromDictionary:[self getEventStatusDictionary:status sendToAll:sendToAll]];
    }
    else {
        eventDictionary[@"GA"] = @"true";
    }
    if(params) {
        [eventDictionary addEntriesFromDictionary:params];
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDictionary];
}

+(NSDictionary*)getEventStatusDictionary:(NSString*)status sendToAll:(BOOL)sendToAll {
    BOOL isDebugMode = FALSE;
    if([self isTrackingDebugModeOn] /*|| [status isEqualToString:@"ack_timed_out"]*/) {
        isDebugMode = TRUE;
    }
   
    //for ga
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    eventDictionary[@"label"] = status;
    
    //send to both trk and ga
    if(isDebugMode || sendToAll) {
        //for trk
        eventDictionary[@"status"] = status;
    }
    else {//send to ga only
        eventDictionary[@"GA"] = @"true";
    }
    return eventDictionary;
}
+(BOOL)isTrackingDebugModeOn {
    BOOL status  = [TMUserSession sharedInstance].socketDebugEnabled;
    return status;
}

@end

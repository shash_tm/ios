//
//  TMSocketTracking.h
//  TrulyMadly
//
//  Created by Ankit on 09/07/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMSocketTracking : NSObject

+(void)trackSocketEvent:(NSString*)eventAction
                 status:(NSString*)status
        eventInfoParams:(NSDictionary*)eventInfoParams
            eventParams:(NSDictionary*)params
              sendToAll:(BOOL)sendToAll;

+(void)trackMovingToPollingFromSocketEvent;

@end

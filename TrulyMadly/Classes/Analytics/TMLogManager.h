//
//  TMLogManager.h
//  TrulyMadly
//
//  Created by Rajesh Singh on 16/03/15.
//  Copyright (c) 2015 trulymadly. All rights reserved.
//

#import "TMBaseManager.h"

@protocol TMLogManagerDelegate;

@interface TMLogManager : TMBaseManager

@property (nonatomic, weak) id <TMLogManagerDelegate> delegate;

@property (nonatomic,assign)BOOL requestInProgress;
-(void)logEvent:(NSDictionary*)dict;

@end

@protocol TMLogManagerDelegate <NSObject>

- (void)networkOperationDidFinish:(BOOL)status;

@end


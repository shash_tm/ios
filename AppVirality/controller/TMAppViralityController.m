//
//  TMAppViralityController.m
//  TrulyMadly
//
//  Created by Ankit on 02/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityController.h"
#import "TMUserSession.h"
#import "AppVirality.h"
#import "NSObject+TMAdditions.h"
#import "TMDataStore.h"
#import "AppViralityWelcomeViewController.h"
//#import "AppViralityGrowthHackViewController.h"
#import "TMDataStore.h"
#import "TMAppViralityGrowthHackViewController.h"

#define KAPPVIRALITY_EMAILPOPUP_SHOWNSTATUS @"com.appvirality.useremail"

@implementation TMAppViralityController


+(void)showGrowthHackScreenFromController:(UIViewController *)viewController {
    [AppVirality getGrowthHack:GrowthHackTypeWordOfMouth completion:^(NSDictionary *campaignDetails,NSError*error) {
        
        if (campaignDetails) {
            TMAppViralityGrowthHackViewController * growthHackVC = [[TMAppViralityGrowthHackViewController alloc] initWithCampaignDetails:campaignDetails ForGrowthHack:GrowthHackTypeWordOfMouth];
            if (growthHackVC) {
                UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:growthHackVC];
                [viewController presentViewController:navVC animated:YES completion:^{
                
                }];
            }
        }
    }];
}
+(void)showWelcomeScreenFromController:(UIViewController*)viewController {
    [AppVirality getReferrerDetails:^(NSDictionary *referrerDetails,NSError* error) {
        AppViralityWelcomeViewController * welcomeVC = [[AppViralityWelcomeViewController alloc] initWithReferrerDetails:referrerDetails];
        if (welcomeVC) {
            UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:welcomeVC];
            [viewController presentViewController:navVC animated:YES completion:^{
                //make isExistingUser True after showing welcome screen so that welcome screen should not be shown again.
                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"AV_ReferrerDetails"]) {
                    NSMutableDictionary * referrerDetails = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"AV_ReferrerDetails"] mutableCopy];
                    if ([referrerDetails objectForKey:@"isExistingUser"]) {
                        [referrerDetails setValue:@"True" forKey:@"isExistingUser"];
                        [[NSUserDefaults standardUserDefaults] setObject:referrerDetails forKey:@"AV_ReferrerDetails"];
                    }
                }
            }];
        }
    }];
}

+(void)updateUserData {
    
    //check if already updated
    //BOOL status = [TMDataStore retrieveBoolforKey:@"com.appvirality.setuserdata"];
    //if(!status) {
       /* TMUser *user = [TMUserSession sharedInstance].user;
        NSMutableDictionary *userDetails = [NSMutableDictionary dictionaryWithCapacity:4];
        if(user.profilePicURL && [user.profilePicURL isValidObject]) {
            userDetails[@"ProfileImage"] = user.profilePicURL;
        }
        if(user.userId && [user.userId isValidObject]) {
            userDetails[@"UserIdInstore"] = user.userId;
        }
        if(user.city && [user.city isValidObject]) {
            userDetails[@"city"] = user.city;
        }
        if(user.state && [user.state isValidObject]) {
            userDetails[@"state"] = user.state;
        }
        if(user.fName && [user.fName isValidObject]) {
            userDetails[@"AppUserName"] = user.fName;
        }
        
        [AppVirality setUserDetails:userDetails Oncompletion:^(BOOL success, NSError *error) {
            BOOL isMatchesShown = [[TMUserSession sharedInstance] navigateUserToMatches];
            if(success && isMatchesShown) {
                [TMDataStore setBool:TRUE forKey:@"com.appvirality.setuserdata"];
            }
        }];*/
    //}
}
+(void)updateUserEmailId:(NSString*)emailid {
    if(emailid && ![self showEmailAlert]) {
        [self saveEmailAlertPopShownStatus:TRUE];
        
        [AppVirality setUserDetails:@{@"EmailId":emailid} Oncompletion:^(BOOL success, NSError *error) {
        }];
    }
}
+(BOOL)showEmailAlert {
    BOOL status = [TMDataStore retrieveBoolforKey:KAPPVIRALITY_EMAILPOPUP_SHOWNSTATUS];
    return status;
}
+(void)saveEmailAlertPopShownStatus:(BOOL)status {
    [TMDataStore setBool:status forKey:KAPPVIRALITY_EMAILPOPUP_SHOWNSTATUS];
}

+(void)trackAppViralityEvent:(TMAppViralityEvent)event {
    NSString *genderTrackEvent = nil;
    NSString *trackEvent = nil;
    BOOL isFemale = [TMUserSession sharedInstance].user.isUserFemale;
    if(event == SignupComplete) {
        genderTrackEvent = (isFemale) ? @"SignupFemale" : @"SignupMale";
        trackEvent = @"Signup";
    }
    else if(event == RegistrationComplete) {
        genderTrackEvent = (isFemale) ? @"RegFemale" : @"RegMale";
        trackEvent = @"Registration";
    }
    
    [self sendAppViralityConversionEvent:trackEvent];
    [self sendAppViralityConversionEvent:genderTrackEvent];
}
+(void)sendAppViralityConversionEvent:(NSString*)event {
    if(event) {
        [AppVirality saveConversionEvent:@{@"eventName":event} completion:^(NSDictionary *conversionResult,NSError* error) {
            
        }];
    }
}

+(unsigned)checkAndGetColorAtKey:(NSString*)key InDictionary:(NSDictionary*)dictionary
{
    unsigned result = 0;
    
    if ([dictionary objectForKey:key]) {
        NSString * color = [dictionary valueForKey:key];
        NSScanner *scanner = [NSScanner scannerWithString:color];
        
        [scanner setScanLocation:[color hasPrefix:@"#"]?1:0]; // bypass '#' character
        [scanner scanHexInt:&result];
    }
    return result;
}

@end

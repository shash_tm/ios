//
//  TMAppViralityCollectionViewCellTNCData.h
//  TrulyMadly
//
//  Created by Ankit on 13/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAppViralityCollectionViewCellTNCData : UICollectionViewCell

-(void)setupWebViewWithString:(NSString*)htmlString;

@end

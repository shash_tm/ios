//
//  TMAppViralityMessageCollectionViewCell.m
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityCollectionViewCellMessage.h"

@interface TMAppViralityCollectionViewCellMessage ()

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *messageLabel;

@end


@implementation TMAppViralityCollectionViewCellMessage

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,
                                                                   20,
                                                                   CGRectGetWidth(self.bounds)-40,
                                                                   30)];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.text = @"Refer and Earn";
        [self addSubview:self.titleLabel];
        
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,
                                                                      CGRectGetMaxY(self.titleLabel.frame)+10,
                                                                      CGRectGetWidth(self.bounds)-40,
                                                                      60)];
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.textColor = [UIColor lightGrayColor];
        self.messageLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:self.messageLabel];
        
    }
    return self;
}

-(void)setOfferTitle:(NSString*)title offerMessageText:(NSString*)messagetext {
    self.titleLabel.text = title;
    self.messageLabel.text = messagetext;
}

@end

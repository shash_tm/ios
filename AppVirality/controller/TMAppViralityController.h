//
//  TMAppViralityController.h
//  TrulyMadly
//
//  Created by Ankit on 02/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEnums.h"
#import <UIKit/UIKit.h>

    
@interface TMAppViralityController : NSObject

+(void)showWelcomeScreenFromController:(UIViewController*)viewController;
+(void)showGrowthHackScreenFromController:(UIViewController *)viewController;
+(BOOL)showEmailAlert;

+(unsigned)checkAndGetColorAtKey:(NSString*)key InDictionary:(NSDictionary*)dictionary;

+(void)updateUserData;
+(void)updateUserEmailId:(NSString*)emailid;
+(void)saveEmailAlertPopShownStatus:(BOOL)status;

+(void)trackAppViralityEvent:(TMAppViralityEvent)event;

@end

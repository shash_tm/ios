//
//  TMAppViralityCollectionViewCellShareButton.h
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAppViralityCollectionViewCellShareButton : UICollectionViewCell

-(void)setupShareButtonsWithImage:(NSString*)imageName withText:(NSString*)text;

@end

//
//  TMAppViralityCollectionViewHeader.m
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityCollectionViewHeader.h"

@interface TMAppViralityCollectionViewHeader ()

@property(strong,nonatomic)UILabel *textLabel;
@property(strong,nonatomic)UIImageView *imageView;

@end

@implementation TMAppViralityCollectionViewHeader

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        self.imageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"icon-arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        self.imageView.frame = CGRectOffset(self.frame, CGRectGetWidth(self.bounds)-60, 15);
        [self addSubview:self.imageView];
        
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(21, 0, CGRectGetWidth(self.bounds)-42, 35)];
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.font = [UIFont boldSystemFontOfSize:16];
        [self addSubview:self.textLabel];
    
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    for (CALayer *layer in [self.layer sublayers]) {
        if ([[layer name] isEqualToString:@"layer"]) {
            CALayer *bottomBorder = layer;
            bottomBorder.frame = CGRectMake(0.0f, CGRectGetHeight(self.frame)-0.8,CGRectGetWidth(self.frame), 0.8f);
        }
    }
}
-(void)setBottomLayerWithStatus:(BOOL)status {
    CALayer *bottomBorder = nil;
    for (CALayer *layer in [self.layer sublayers]) {
        if ([[layer name] isEqualToString:@"layer"]) {
            bottomBorder = layer;
            break;
        }
    }
    
    if(status) {
        if(!bottomBorder) {
            CALayer *bottomBorder = [CALayer layer];//add a divider line
            bottomBorder.frame = CGRectMake(0.0f, CGRectGetHeight(self.frame)-0.8,CGRectGetWidth(self.frame), 0.8f);
            bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
            bottomBorder.name = @"layer";
            //[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0].CGColor;
            [self.layer addSublayer:bottomBorder];
        }
    }
    else{
        if(bottomBorder) {
            [bottomBorder removeFromSuperlayer];
            bottomBorder = nil;
        }
    }
}
-(void)setTitleText:(NSString*)text transformArrowForSelectedState:(BOOL)isSelected {
    self.textLabel.text = text;
    self.imageView.transform = CGAffineTransformMakeRotation(isSelected ? -M_PI_2 : M_PI_2);
    
}

@end

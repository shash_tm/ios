//
//  TMAppViralityMessageCollectionViewCell.h
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAppViralityCollectionViewCellMessage : UICollectionViewCell

-(void)setOfferTitle:(NSString*)title offerMessageText:(NSString*)messagetext;

@end

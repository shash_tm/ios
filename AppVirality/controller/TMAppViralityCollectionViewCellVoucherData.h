//
//  TMAppViralityCollectionViewCellVoucherData.h
//  TrulyMadly
//
//  Created by Ankit on 13/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAppViralityCollectionViewCellVoucherData : UICollectionViewCell

-(void)setVoucherCode:(NSString*)code voucherValue:(NSString*)value voucherExpiry:(NSString*)expiry isHeader:(BOOL)header
         voucherCount:(NSInteger)voucherCount;
-(void)stopActivityLoader;

@end

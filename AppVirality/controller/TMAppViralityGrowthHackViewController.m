//
//  TMAppViralityGrowthHackViewController.m
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityGrowthHackViewController.h"
#import "TMAppViralityController.h"
#import "TMAppViralityCollectionViewCellMessage.h"
#import "TMAppViralityCollectionViewCellShareButton.h"
#import "TMAppViralityCollectionViewCellReferralData.h"
#import "TMAppViralityCollectionViewCellVoucherData.h"
#import "TMAppViralityCollectionViewCellTNCData.h"
#import "TMAppViralityCollectionViewHeader.h"
#import <FacebookSDK/FacebookSDK.h>
#import <MessageUI/MessageUI.h>
#import "NSString+TMAdditions.h"


@interface TMAppViralityGrowthHackViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UIAlertViewDelegate,MFMessageComposeViewControllerDelegate>

@property (strong) NSMutableArray *selectedSections;
@property (strong) NSArray *referredusers;
@property (strong) NSArray *myvouchers;
@property (strong) NSArray *shareApps;
@property (strong) NSArray *headerArray;
@property (strong) NSMutableArray *shareButtons;
@property (strong) NSDictionary *urlSchemes,*campaginDetails;
@property (strong) NSMutableDictionary *shareMesgs;
@property (strong) NSString *terms;
@property (strong) NSString *shareURLString;
@property (strong) NSString *offerTitle;
@property (strong) NSString *offerMessage;
@property (strong) UICollectionView *collectionview;
@property NSInteger termsHeight;
@property GrowthHackType growthHack;
@property BOOL shareButtonActionStatus;
@property BOOL didAppBecomeInactiveAfterShare;
@property BOOL isEmailPopActive;
@property(strong) NSString *subject;
@end

@implementation TMAppViralityGrowthHackViewController

-(id)initWithCampaignDetails:(NSDictionary *)campaignDetails ForGrowthHack:(GrowthHackType)growthHack
{
    if (!campaignDetails) {
        return nil;
    }
    self = [super init];
    if (self) {
        self.shareButtonActionStatus = FALSE;
        self.didAppBecomeInactiveAfterShare = FALSE;
        self.isEmailPopActive = FALSE;
        self.growthHack = growthHack;
        self.campaginDetails = campaignDetails;
        self.shareMesgs = [NSMutableDictionary dictionary];
        self.selectedSections = [NSMutableArray array];
        self.shareButtons = [NSMutableArray array];
        self.termsHeight=0;
        self.shareApps = @[@"WhatsApp",@"Messenger",@"Message",@"Mail",@"More"];
        self.headerArray = @[@"Referrals Accepted",@"My Vouchers",@"Terms and Conditions"];
        ////
        if ([campaignDetails objectForKey:@"OfferTitle"]) {
            self.offerTitle = [campaignDetails objectForKey:@"OfferTitle"];
        }
        
        if ([campaignDetails objectForKey:@"OfferDescription"]) {
            self.offerMessage = [campaignDetails objectForKey:@"OfferDescription"];
        }

        if(!self.referredusers) {
            [AppVirality getUserBalance:self.growthHack completion:^(NSDictionary *userInfo,NSError*error) {
                self.referredusers = [userInfo valueForKey:@"referredusers"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionview reloadData];
                });
            }];
        }
        if(!self.myvouchers) {
            [AppVirality getUserCoupons:^(NSDictionary *coupons, NSError *error) {
                self.myvouchers = (coupons[@"coupons"] && ![coupons[@"coupons"] isKindOfClass:[NSNull class]]) ? [coupons valueForKey:@"coupons"] : [[NSArray alloc] init];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionview reloadData];
                });
            }];
        }
        [AppVirality getTerms:self.growthHack completion:^(NSDictionary *userTerms,NSError*error) {
            if ([userTerms objectForKey:@"message"]) {
                self.terms = [userTerms valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionview reloadData];
                });
            }
        }];
        
        
        ////
        NSArray * socialActions = [campaignDetails objectForKey:@"socialactions"];
        if ( ([socialActions isKindOfClass:[NSArray class]]) && (socialActions.count) ) {
            
            for (NSDictionary * socialAction in socialActions) {
                NSString * message = [socialAction valueForKey:@"shareMessage"];
                message = [message stringByReplacingOccurrencesOfString:@"SHARE_URL" withString:[socialAction valueForKey:@"shareUrl"]];
                [self.shareMesgs setValue:message forKey:[socialAction valueForKey:@"socialActionName"]];
                if([[socialAction valueForKey:@"socialActionName"] isEqualToString:@"Mail"]){
                    self.subject = [socialAction valueForKey:@"shareTitle"];
                }
            }
            
            if (socialActions.count!=0) {
                NSString *shareUrl =[[socialActions valueForKey:@"shareUrl"] firstObject];
//                NSURL *myURL = [NSURL URLWithString:shareUrl];
//                myURL = [myURL URLByDeletingLastPathComponent];
//                shareUrl = [myURL absoluteString];
                self.shareURLString = shareUrl;
            }
        }
        
        [AppVirality recordImpressionsForGrowthHack:self.growthHack WithParams:@{@"click":@"true",@"impression":@"false"} completion:^(NSDictionary *response, NSError* error) {
            
        }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonClicked:)];
    
    [self setupUIWithcampaignDetails:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showEmailPopup];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.didAppBecomeInactiveAfterShare = (_shareButtonActionStatus) ? TRUE : FALSE;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupUIWithcampaignDetails:(NSDictionary*)campaignDetails {
    [self setupCollectionView];
}
-(void)setupCollectionView {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [flowLayout setMinimumInteritemSpacing:1];
    [flowLayout setMinimumLineSpacing:1];
    [flowLayout setSectionInset:UIEdgeInsetsMake(5,10,5,10)];
    
    self.collectionview = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.collectionview.dataSource=self;
    self.collectionview.delegate=self;
    self.collectionview.showsVerticalScrollIndicator = NO;
    self.collectionview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionview];
    
    [self.collectionview registerClass:[TMAppViralityCollectionViewCellMessage class] forCellWithReuseIdentifier:@"messagecell"];
    [self.collectionview registerClass:[TMAppViralityCollectionViewCellShareButton class] forCellWithReuseIdentifier:@"sharebuttonscell"];
    [self.collectionview registerClass:[TMAppViralityCollectionViewCellReferralData class] forCellWithReuseIdentifier:@"referraldatacell"];
    [self.collectionview registerClass:[TMAppViralityCollectionViewCellVoucherData class] forCellWithReuseIdentifier:@"voucherdatacell"];
    [self.collectionview registerClass:[TMAppViralityCollectionViewCellTNCData class] forCellWithReuseIdentifier:@"tncdatacell"];
    [self.collectionview registerClass:[TMAppViralityCollectionViewHeader class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"headerview"];
}
-(void)showEmailPopup {
    if(self.didAppBecomeInactiveAfterShare && ![TMAppViralityController showEmailAlert]) {
        self.isEmailPopActive = TRUE;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your email to get notified when you receive your vouchers." delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Save", nil];
            alertview.alertViewStyle = UIAlertViewStylePlainTextInput;
            [alertview show];
        });
    }
}
- (void)scrollToBottomAnimated:(BOOL)animated {
    
    /*
    CGPoint bottomOffset = CGPointMake(0, self.collectionview.contentSize.height -  self.collectionview.bounds.size.height);
    [self.collectionview setContentOffset:bottomOffset animated:TRUE];
     */
    
}

-(void)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark UIAlertview delegate methods -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *emailId = [alertView textFieldAtIndex:0].text;
    if(emailId && [emailId isValidEmail]) {
        [TMAppViralityController updateUserEmailId:[alertView textFieldAtIndex:0].text];
    }
    ////in case of skip also do not show alert next time
    [TMAppViralityController saveEmailAlertPopShownStatus:TRUE];
}

#pragma mark Collectionview datasource / delegate methods -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 6;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ( (section > 1) && (![self.selectedSections containsObject:[NSNumber numberWithInteger:section]]) ) {
        return 0;
    }
    if (section==0) {
        return 1;
    }
    if (section==1) {
        return 5;
    }
    if (section==2) {
        return 0;
    }
    if (section==3) {
        NSInteger row = (self.referredusers.count) ? self.referredusers.count : 0;
        return row;
    }
    if (section==4) {
        NSInteger row = (self.myvouchers.count) ? self.myvouchers.count+1 : 0;
        return row;
    }
    if (section==5) {
        return 1;
    }
    return 0;

}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        TMAppViralityCollectionViewCellMessage *cell = [collectionView
                                         dequeueReusableCellWithReuseIdentifier:@"messagecell"
                                         forIndexPath:indexPath];
        [cell setOfferTitle:self.offerTitle offerMessageText:self.offerMessage];
        return cell;
    }
    else if(indexPath.section == 1) {
        TMAppViralityCollectionViewCellShareButton *cell = [collectionView
                                                        dequeueReusableCellWithReuseIdentifier:@"sharebuttonscell"
                                                        forIndexPath:indexPath];
        NSString *text = self.shareApps[indexPath.row];
        [cell setupShareButtonsWithImage:text withText:text];
        return cell;
    }
    else if(indexPath.section == 3) {
        TMAppViralityCollectionViewCellReferralData *cell = [collectionView
                                                        dequeueReusableCellWithReuseIdentifier:@"referraldatacell"
                                                        forIndexPath:indexPath];
        NSInteger index = indexPath.row;
        NSString *name,*date,*imageURL = nil;
        if(index < self.referredusers.count) {
            name = [self getReferredUserNameAtIndex:index];
            date = [self getReferredUserDateAtIndex:index];
            imageURL = [self getReferredUserImageURLStringAtIndex:index];
            [cell setupReferalName:name image:imageURL date:date];
        }
        return cell;
    }
    else if(indexPath.section == 4) {
        TMAppViralityCollectionViewCellVoucherData *cell = [collectionView
                                                        dequeueReusableCellWithReuseIdentifier:@"voucherdatacell"
                                                        forIndexPath:indexPath];
        BOOL isHeader = (indexPath.row == 0) ? TRUE : FALSE;
        NSString *voucherCode = @"Voucher";
        NSString *voucherValue = @"Value";
        NSString *voucherExpiry = @"Expiry";
        NSInteger index = indexPath.row-1;
        if(index < self.myvouchers.count) {
            voucherCode = [self getVoucherCodeAtIndex:index];
            voucherValue = [self getVoucherValueAtIndex:index];
            voucherExpiry = [self getVoucherExpiryAtIndex:index];
        }
        [cell setVoucherCode:voucherCode voucherValue:voucherValue voucherExpiry:voucherExpiry isHeader:isHeader
                voucherCount:self.myvouchers.count];
        return cell;
    }
    else if(indexPath.section == 5) {
        TMAppViralityCollectionViewCellTNCData *cell = [collectionView
                                                        dequeueReusableCellWithReuseIdentifier:@"tncdatacell"
                                                        forIndexPath:indexPath];
        NSString * htmlString = [self.terms stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        [cell setupWebViewWithString:htmlString];
        return cell;
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    TMAppViralityCollectionViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerview" forIndexPath:indexPath];
    headerView.tag = indexPath.section;
    if(!headerView.gestureRecognizers) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerSelected:)];
        [headerView addGestureRecognizer:tapGesture];
    }
    
    if(indexPath.section == 2) {
        //headerView.backgroundColor = [UIColor clearColor];
        [headerView setBottomLayerWithStatus:FALSE];
        [headerView setTitleText:nil transformArrowForSelectedState:FALSE];
    }
    else {
        NSInteger headerArrayIndex = indexPath.section-3;
        NSString *textString = self.headerArray[headerArrayIndex];
        BOOL status = [self.selectedSections containsObject:[NSNumber numberWithInteger:indexPath.section]];
        
        if(indexPath.section == 3) {
            [headerView setBottomLayerWithStatus:TRUE];
            [headerView setTitleText:textString transformArrowForSelectedState:status];
            //headerView.backgroundColor = [UIColor blueColor];
        }
        else if(indexPath.section == 4) {
            [headerView setBottomLayerWithStatus:TRUE];
            [headerView setTitleText:textString transformArrowForSelectedState:status];
            //headerView.backgroundColor = [UIColor blueColor];
        }
        else if(indexPath.section == 5) {
            [headerView setBottomLayerWithStatus:TRUE];
            [headerView setTitleText:textString transformArrowForSelectedState:status];
            //headerView.backgroundColor = [UIColor clearColor];
        }
    }
    return headerView;
}


#pragma mark - Collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return CGSizeMake(self.view.bounds.size.width-20, 120);
    }
    else if (indexPath.section==1) {
        return CGSizeMake(90, 90);
    }
    if (indexPath.section==3) {
        return CGSizeMake(self.view.bounds.size.width-20, 50);
    }
    if (indexPath.section==4) {
        return CGSizeMake(self.view.bounds.size.width-20, 30);
    }
    if (indexPath.section==5) {
        return CGSizeMake(self.view.bounds.size.width-20, 90);
    }

    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if(section == 2) {
        return CGSizeMake(self.view.bounds.size.width, 10);
    }
    if(section > 2) {
        return CGSizeMake(self.view.bounds.size.width, 40);
    }
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1) {
        [self shareButtonAction:indexPath.row];
    }
}

-(void)headerSelected:(UITapGestureRecognizer*)tapGestureRecognizer
{
    UIView *view = tapGestureRecognizer.view;
    NSInteger tag = [view tag];
    if ([self.selectedSections containsObject:[NSNumber numberWithInteger:tag]]) {
        [self.selectedSections removeObject:[NSNumber numberWithInteger:tag]];
    }else
        [self.selectedSections addObject:[NSNumber numberWithInteger:tag]];
    
    [self.collectionview reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self scrollToBottomAnimated:TRUE];
    });
}

-(void)shareButtonAction:(NSInteger)row {
    self.shareButtonActionStatus = TRUE;
    
    switch(row) {
        case 0:
            [self whatsappButtonClickAction];
            break;
        case 1:
            [self fbMessengerButtonClickAction];
            break;
        case 2:
            [self smsButtonClickAction];
            break;
        case 3:
            [self mailButtonClickAction];
            break;
        case 4:
            [self moreButtonClickAction];
        default:
            break;
    }
}
-(void)whatsappButtonClickAction
{
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",[self.shareMesgs objectForKey:@"WhatsApp"]?[[self.shareMesgs valueForKey:@"WhatsApp"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@""]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [self recordSocialActionForActionType:@"WhatsApp"];
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

-(void)mailButtonClickAction
{
    NSString *body = [self.shareMesgs objectForKey:@"Mail"];
    if(body) {
        body = [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *subject = self.subject;//[NSString stringWithFormat:@"Let\'s go UnSingling!"];
        NSString *encodeSub = [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodeSub1 = [encodeSub stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        
        NSString *text =  [NSString stringWithFormat:@"mailto:?subject=%@%@%@",encodeSub1,@"&body=",body];
        
        NSURL *mailURL = [NSURL URLWithString:text];
        if ([[UIApplication sharedApplication] canOpenURL: mailURL]) {
            [self recordSocialActionForActionType:@"Mail"];
            [[UIApplication sharedApplication] openURL: mailURL];
        }
    }
}
-(void)smsButtonClickAction
{
    NSString *smsText = [self.shareMesgs objectForKey:@"Messaging"];
    if(smsText) {
        [self recordSocialActionForActionType:@"Messaging"];
        
        if(![MFMessageComposeViewController canSendText]) {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            return;
        }
        
        NSArray *recipents = @[];
        NSString *message = [NSString stringWithFormat:@"%@",smsText];
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];
        
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

-(void)fbMessengerButtonClickAction
{
    NSString *shareKey = [self.shareMesgs objectForKey:@"Messenger"];
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [[NSURL alloc] initWithString:self.shareURLString];
    params.name = shareKey;
    
    if ([FBDialogs canPresentMessageDialogWithParams:params]) {
        [FBDialogs presentMessageDialogWithParams:params clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            if(error != nil){
                
            }
        }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"There was an error connecting to Messenger. Please make sure that it is installed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        });
    }
}
-(void)moreButtonClickAction
{
    //NSURL *myWebsite = [NSURL URLWithString:[[self.shareURLString componentsSeparatedByString:@"\n"] lastObject]];
    NSString * message = [[self.shareMesgs allValues] firstObject];
    NSArray *objectsToShare = @[message];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [activityVC setValue:self.subject forKey:@"Subject"];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToTwitter];
    
    activityVC.excludedActivityTypes = excludeActivities;
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if (completed) {
            if([activityType isEqualToString: UIActivityTypeMail]){
                [self recordSocialActionForActionType:@"Mail"];
            }
            else if([activityType isEqualToString: UIActivityTypeMessage]){
                [self recordSocialActionForActionType:@"Messaging"];
            }
            else if([activityType isEqualToString: @"com.google.hangouts.ShareExtension"]){
                [self recordSocialActionForActionType:@"HangOuts"];
            }
            else
            {
                [self recordSocialActionForActionType:@"CustomLink"];
            }
        }
    }];
    [self presentViewController:activityVC animated:YES completion:nil];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
            break;
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)recordSocialActionForActionType:(NSString*)socialActionName
{
    if ([self.campaginDetails objectForKey:@"socialactions"]) {
        for (NSDictionary * socialAction in [self.campaginDetails valueForKey:@"socialactions"]) {
            if ([[socialAction valueForKey:@"socialActionName"] isEqualToString:socialActionName]) {
                
                [AppVirality recordSocialActionForGrowthHack:self.growthHack WithParams:@{@"shareMessage":[socialAction valueForKey:@"shareMessage"],@"shortcode":[[[socialAction valueForKey:@"shareUrl"] stringByDeletingLastPathComponent] lastPathComponent],@"socialActionId":[socialAction valueForKey:@"socialActionId"]} completion:^(BOOL success,NSError*error) {
                    
                }];
            }
        }
    }
}

-(NSString*)getReferredUserNameAtIndex:(NSInteger)index {
    NSString *name = [[self.referredusers objectAtIndex:index] objectForKey:@"name"];
    if (name && ![name isEqual:[NSNull null]]) {
        return name;
    }
    return nil;
}
-(NSString*)getReferredUserDateAtIndex:(NSInteger)index {
    NSString *date = [[self.referredusers objectAtIndex:index] objectForKey:@"regdate"];
    if (date && ![date isEqual:[NSNull null]]) {
        return date;
    }
    return nil;
}
-(NSString*)getReferredUserImageURLStringAtIndex:(NSInteger)index {
    NSString *imageURLString = [[self.referredusers objectAtIndex:index] objectForKey:@"profileimage"];
    if (imageURLString && ![imageURLString isEqual:[NSNull null]]) {
        return imageURLString;
    }
    return nil;
}
-(NSString*)getVoucherCodeAtIndex:(NSInteger)index {
    NSString *voucherCode = [[self.myvouchers objectAtIndex:index] objectForKey:@"couponCode"];
    if (voucherCode && ![voucherCode isEqual:[NSNull null]]) {
        return voucherCode;
    }
    return nil;
}
-(NSString*)getVoucherValueAtIndex:(NSInteger)index {
    NSString *voucherValue = [[self.myvouchers objectAtIndex:index] objectForKey:@"couponValue"];
    if (voucherValue && ![voucherValue isEqual:[NSNull null]]) {
        return voucherValue;
    }
    return nil;
}
-(NSString*)getVoucherExpiryAtIndex:(NSInteger)index {
    NSString *voucherExpiry = [[self.myvouchers objectAtIndex:index] objectForKey:@"couponExpiry"];
    if (voucherExpiry && ![voucherExpiry isEqual:[NSNull null]]) {
        return voucherExpiry;
    }
    return nil;
}

#pragma mark - Notification Handler

-(void)appWillEnterForeground:(NSNotification*)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showEmailPopup];
    });
}
-(void)appDidEnterBackground:(NSNotification*)notification {
    self.didAppBecomeInactiveAfterShare = (_shareButtonActionStatus) ? TRUE : FALSE;
}

@end

//
//  AppViralityGrowthHackViewController.m
//  testAV
//
//  Created by Ram on 07/05/15.
//  Copyright (c) 2015 AppVirality. All rights reserved.
//

#import "AppViralityGrowthHackViewController.h"
#import "TMAppViralityController.h"


@interface AppViralityGrowthHackViewController ()

@property (strong) NSMutableArray *selectedSections;
@property (strong) NSArray *referredusers;
@property (strong) NSArray *myvouchers;
@property (strong) NSMutableArray *shareButtons;
@property (strong) NSDictionary *urlSchemes,*campaginDetails;
@property (strong) NSMutableDictionary *shareMesgs;
@property (strong) NSString *terms;
@property (strong) NSString *shareURLString;
@property (strong) UIWebView *termsView;
@property NSInteger termsHeight;
@property GrowthHackType growthHack;

@end


@implementation AppViralityGrowthHackViewController


-(id)initWithCampaignDetails:(NSDictionary *)campaignDetails ForGrowthHack:(GrowthHackType)growthHack
{
    if (!campaignDetails) {
        return nil;
    }
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.growthHack = growthHack;
        self.campaginDetails = campaignDetails;
        self.shareMesgs = [NSMutableDictionary dictionary];
        self.selectedSections = [NSMutableArray array];
        self.shareButtons = [NSMutableArray array];
        self.termsHeight=0;
        
        ////
        if(!self.referredusers) {
            [AppVirality getUserBalance:self.growthHack completion:^(NSDictionary *userInfo,NSError*error) {
                self.referredusers = [userInfo valueForKey:@"referredusers"];
            }];
        }
        if(!self.myvouchers) {
            [AppVirality getUserCoupons:^(NSDictionary *coupons, NSError *error) {
                self.myvouchers = @[@"",@"",@""];//[coupons valueForKey:@"coupons"];
            }];
        }
        [AppVirality getTerms:self.growthHack completion:^(NSDictionary *userTerms,NSError*error) {
            if ([userTerms objectForKey:@"message"]) {
                self.terms = [userTerms valueForKey:@"message"];
            }
        }];

        
        ////
        NSArray * socialActions = [campaignDetails objectForKey:@"socialactions"];
        if ( ([socialActions isKindOfClass:[NSArray class]]) && (socialActions.count) ) {
            
            for (NSDictionary * socialAction in socialActions) {
                NSString * message = [socialAction valueForKey:@"shareMessage"];
                message = [message stringByReplacingOccurrencesOfString:@"SHARE_URL" withString:[socialAction valueForKey:@"shareUrl"]];
                [self.shareMesgs setValue:message forKey:[socialAction valueForKey:@"socialActionName"]];
            }
            
            if (socialActions.count!=0) {
                NSString *shareUrl =[[socialActions valueForKey:@"shareUrl"] firstObject];
                NSURL *myURL = [NSURL URLWithString:shareUrl];
                myURL = [myURL URLByDeletingLastPathComponent];
                shareUrl = [myURL absoluteString];
                self.shareURLString = shareUrl;
            }
        }
        
        [self setupUIWithcampaignDetails:campaignDetails];
        
        [AppVirality recordImpressionsForGrowthHack:self.growthHack WithParams:@{@"click":@"true",@"impression":@"false"} completion:^(NSDictionary *response, NSError* error) {
            
        }];
        
        [self.view setNeedsDisplay];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //self.tableView.separatorColor = [UIColor grayColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonClicked:)];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your email to get notified when you win vouchers." delegate:nil cancelButtonTitle:@"Skip" otherButtonTitles:@"Save", nil];
    //        alertview.alertViewStyle = UIAlertViewStylePlainTextInput;
    //        [alertview show];
    //    });
}
-(void)viewWillLayoutSubviews
{
    self.greetingLabel.frame = CGRectMake(CGRectGetMinX(self.greetingLabel.frame),
                                          CGRectGetMinY(self.greetingLabel.frame),
                                          CGRectGetWidth(self.greetingLabel.frame),
                                          CGRectGetHeight(self.greetingLabel.frame));
    
    self.messageLabel.frame = CGRectMake(CGRectGetMinX(self.messageLabel.frame),
                                         CGRectGetMaxY(self.greetingLabel.frame)+20,
                                         CGRectGetWidth(self.messageLabel.frame),
                                         CGRectGetHeight(self.messageLabel.frame));
    
    for(UIView *view in self.shareButtons) {
        view.frame = CGRectMake(CGRectGetMinX(view.frame),
                                CGRectGetMaxY(self.messageLabel.frame)+20,
                                CGRectGetWidth(view.frame),
                                CGRectGetHeight(view.frame));
    }
}

-(void)setupUIWithcampaignDetails:(NSDictionary*)campaignDetails {
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    self.tableView.tableHeaderView = headerView;
    unsigned color = [TMAppViralityController checkAndGetColorAtKey:@"CampaignBGColor" InDictionary:campaignDetails];
    headerView.backgroundColor = UIColorFromRGB(color);
    self.view.backgroundColor = UIColorFromRGB(color);
    
    self.greetingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 120)];
    self.greetingLabel.text = [NSString stringWithFormat:@"Love the app?\n Refer your friends and help us\ngrow!"];
    self.greetingLabel.textAlignment = NSTextAlignmentCenter;
    self.greetingLabel.numberOfLines =0;
    self.greetingLabel.font = [UIFont boldSystemFontOfSize:16];
    [headerView addSubview:self.greetingLabel];
    
    self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.greetingLabel.frame), SCREEN_WIDTH-20, 120)];
    self.messageLabel.text = [NSString stringWithFormat:@"Offer Description"];
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    self.messageLabel.numberOfLines=0;
    self.messageLabel.backgroundColor = [UIColor clearColor];
    [headerView addSubview:self.messageLabel];
    
    if ([campaignDetails objectForKey:@"OfferTitle"]) {
        NSString * titleText= [campaignDetails valueForKey:@"OfferTitle"];
        titleText = [titleText stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        titleText = [titleText stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        
        unsigned color = [TMAppViralityController checkAndGetColorAtKey:@"OfferTitleColor" InDictionary:campaignDetails];
        self.greetingLabel.textColor =UIColorFromRGB(color);
        self.greetingLabel.text = titleText;
        [AppViralityUIUtility resetLabelHeight:self.greetingLabel];
    }
    
    if ([campaignDetails objectForKey:@"OfferDescription"]) {
        NSString * descriptionText= [campaignDetails valueForKey:@"OfferDescription"];
        descriptionText = [descriptionText stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        descriptionText = [descriptionText stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        
        unsigned color = [TMAppViralityController checkAndGetColorAtKey:@"OfferDescriptionColor" InDictionary:campaignDetails];
        self.messageLabel.textColor =UIColorFromRGB(color);
        self.messageLabel.text = descriptionText;
        [AppViralityUIUtility resetLabelHeight:self.messageLabel];
    }

    /////
    [self setupShareButtonsOnSuperView:headerView];
    
    /////
    if ([campaignDetails objectForKey:@"CampaignBGImage"]) {
        [AppViralityUIUtility downloadImageWithURL:[NSURL  URLWithString:[campaignDetails objectForKey:@"CampaignBGImage"]] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded&&image) {
                UIImageView * imageView = [[UIImageView alloc] initWithImage:image];
                self.tableView.backgroundView = imageView;
                headerView.backgroundColor = [UIColor clearColor];
            }
        }];
    }
}

-(void)setupShareButtonsOnSuperView:(UIView*)superView {
    
    CGFloat xPos = 15;
    CGFloat yPos = CGRectGetMaxY(self.messageLabel.frame)+20;
    CGFloat width = 90;
    CGFloat height = 90;
    
    CGFloat iconWidth = 60;
    
    NSArray *shareApps = @[@"WhatsApp",@"Messenger",@"Message",@"Mail",@"More"];
    for (int i=0; i<shareApps.count; i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos,yPos, width, height)];
        view.tag = i;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((width-iconWidth)/2, 12, iconWidth, iconWidth)];
        imageView.image = [UIImage imageNamed:shareApps[i]];
        [view addSubview:imageView];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame)+5, width, 20)];
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.text = shareApps[i];
        lbl.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lbl];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [view addGestureRecognizer:tapGesture];
        
        [superView addSubview:view];
        
        self.shareButtons[i] = view;
        
        xPos = xPos+width;
    }
}

-(void)tapAction:(UITapGestureRecognizer *)sender {
    switch(sender.view.tag) {
        case 0:
            [self whatsappButtonClickAction];
            break;
        case 1:
            [self fbMessengerButtonClickAction];
            break;
        case 2:
            [self smsButtonClickAction];
            break;
        case 3:
            [self mailButtonClickAction];
            break;
        case 4:
            [self moreButtonClickAction];
        default:
            break;
    }
}


-(void)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)recordSocialActionForActionType:(NSString*)socialActionName
{
    if ([self.campaginDetails objectForKey:@"socialactions"]) {
        for (NSDictionary * socialAction in [self.campaginDetails valueForKey:@"socialactions"]) {
            if ([[socialAction valueForKey:@"socialActionName"] isEqualToString:socialActionName]) {
           
                [AppVirality recordSocialActionForGrowthHack:self.growthHack WithParams:@{@"shareMessage":[socialAction valueForKey:@"shareMessage"],@"shortcode":[[[socialAction valueForKey:@"shareUrl"] stringByDeletingLastPathComponent] lastPathComponent],@"socialActionId":[socialAction valueForKey:@"socialActionId"]} completion:^(BOOL success,NSError*error) {
                    
                }];
            }
        }
    }
}


-(void)whatsappButtonClickAction
{
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",[self.shareMesgs objectForKey:@"WhatsApp"]?[[self.shareMesgs valueForKey:@"WhatsApp"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@""]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [self recordSocialActionForActionType:@"WhatsApp"];
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

-(void)mailButtonClickAction
{
    NSURL *mailURL = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:?body=%@",[self.shareMesgs objectForKey:@"Mail"]?[[self.shareMesgs valueForKey:@"Mail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@""]];
    if ([[UIApplication sharedApplication] canOpenURL: mailURL]) {
        [self recordSocialActionForActionType:@"Mail"];
        [[UIApplication sharedApplication] openURL: mailURL];

    }
}
-(void)smsButtonClickAction
{
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",[self.shareMesgs objectForKey:@"WhatsApp"]?[[self.shareMesgs valueForKey:@"WhatsApp"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@""]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [self recordSocialActionForActionType:@"WhatsApp"];
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

-(void)fbMessengerButtonClickAction
{
    NSURL *mailURL = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:?body=%@",[self.shareMesgs objectForKey:@"Mail"]?[[self.shareMesgs valueForKey:@"Mail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@""]];
    if ([[UIApplication sharedApplication] canOpenURL: mailURL]) {
        [self recordSocialActionForActionType:@"Mail"];
        [[UIApplication sharedApplication] openURL: mailURL];
        
    }
}
-(void)moreButtonClickAction
{
    NSURL *myWebsite = [NSURL URLWithString:[[self.shareURLString componentsSeparatedByString:@"\n"] lastObject]];
    NSString * message = [[self.shareMesgs allValues] firstObject];
    NSArray *objectsToShare = @[myWebsite,message];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if (completed) {
            if([activityType isEqualToString: UIActivityTypeMail]){
                [self recordSocialActionForActionType:@"Mail"];
            }else if([activityType isEqualToString: UIActivityTypePostToFacebook]){
                [self recordSocialActionForActionType:@"Facebook"];
            }else if([activityType isEqualToString: UIActivityTypePostToTwitter]){
                [self recordSocialActionForActionType:@"Twitter"];
            }else
            {
                [self recordSocialActionForActionType:@"CustomLink"];
            }
        }
        
    }];
    [self presentViewController:activityVC animated:YES completion:nil];
}


#pragma mark -Table View Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![self.selectedSections containsObject:[NSNumber numberWithInteger:section]]) {
        return 0;
    }
    if (section==0) {
        return self.referredusers.count;
    }
    if (section==1) {
        return self.myvouchers.count;
    }
    if (section==2) {
        return 1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==2&&indexPath.row==0) {
        return self.termsHeight==0?45:self.termsHeight;
    }
    return 45;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray * headerArray = @[@"Referrals Accepted",@"My Vouchers",@"Terms and Conditions"];
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(21, 0, SCREEN_WIDTH-42, 35)];
    headerLabel.text = [headerArray objectAtIndex:section];
    headerLabel.textColor = self.greetingLabel.textColor;
    [headerView addSubview:headerLabel];
    UIImageView * imageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"arrow.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    imageView.tintColor = self.greetingLabel.textColor;
    imageView.transform = CGAffineTransformMakeRotation([self.selectedSections containsObject:[NSNumber numberWithInteger:section]]?-M_PI_2:M_PI_2);
    imageView.frame = CGRectOffset(imageView.frame, SCREEN_WIDTH-60, 15);
    [headerView addSubview:imageView];
    UIButton *button = [[UIButton alloc] initWithFrame: CGRectMake(0.0, 0.0, SCREEN_WIDTH, 45)];
    button.alpha = 0.7;
    button.tag = section;
    /* Prepare target-action */
    [button addTarget: self action: @selector(headerSelected:)
     forControlEvents: UIControlEventTouchUpInside];
    UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetHeight(headerView.frame)-1, SCREEN_WIDTH-10, 1)];
    lineView.backgroundColor = [UIColor grayColor];
    //[headerView addSubview:lineView];
    [headerView addSubview: button];

    return headerView;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * reuseIdentifier = [@"reuseIdentifier" stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    //NSLog(@"%ld %ld %@",(long)indexPath.section,(long)indexPath.row,reuseIdentifier);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0.1];
        cell.textLabel.textColor = self.greetingLabel.textColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //cell.sepe
    }
    
    if (indexPath.section==0) {
        cell.textLabel.text = @"no name";
        cell.imageView.image  = [UIImage imageNamed:@"profileIcon.png"];
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"emailid"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"emailid"] isEqual:[NSNull null]]) {
            cell.textLabel.text =[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"emailid"];
        }
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"name"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"name"] isEqual:[NSNull null]]) {
            cell.textLabel.text =[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"name"];
        }
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"regdate"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"regdate"] isEqual:[NSNull null]]) {
            cell.textLabel.text = [cell.textLabel.text stringByAppendingFormat:@"\n%@",[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"regdate"]];
            cell.textLabel.numberOfLines = 0;
        }
        if ([[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"profileimage"]&&![[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"profileimage"] isEqual:[NSNull null]]) {
            [AppViralityUIUtility downloadImageWithURL:[NSURL  URLWithString:[[self.referredusers objectAtIndex:indexPath.row] objectForKey:@"profileimage"]] completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded&&image) {
                    cell.imageView.image = image;
                    cell.imageView.layer.cornerRadius = CGRectGetWidth(cell.imageView.frame)/2;
                    cell.imageView.layer.masksToBounds = YES;
                }
            }];
        }
    }
    if (indexPath.section==1) {
        //NSArray * titleArray = @[@"Total Referral Points"];
        cell.textLabel.text = @"Total Referral Points";//[titleArray objectAtIndex:indexPath.row];
        
        cell.textLabel.textColor = self.greetingLabel.textColor;
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
        label.text =@"0";
        label.textColor = self.greetingLabel.textColor;

        cell.accessoryView = label;
        return cell;
    }
    
    if (indexPath.section==2) {
        if (self.terms) {
            cell.textLabel.text = self.terms;
            NSString * htmlString = [self.terms stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.numberOfLines=0;
            
            if (self.termsHeight==0) {
                [AppViralityUIUtility resetLabelHeight:cell.textLabel];
                self.termsHeight = CGRectGetHeight(cell.textLabel.frame)+30;
                
                [self.tableView reloadData];
                
            }
            else {
                cell.textLabel.text = @"";
                
                if (!self.termsView) {
                    self.termsView= [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.termsHeight)];
                    self.termsView.alpha=0.5f;
                    [self.termsView setBackgroundColor:[UIColor clearColor]];
                    [self.termsView setOpaque:NO];
                }
                [self.termsView loadHTMLString:htmlString baseURL:nil];
                [cell.contentView addSubview:self.termsView];
            }
        }
    }

    return cell;
}

-(void)headerSelected:(UIButton*)sender
{
    NSInteger tag = [sender tag];
    if ([self.selectedSections containsObject:[NSNumber numberWithInteger:tag]]) {
        [self.selectedSections removeObject:[NSNumber numberWithInteger:tag]];
    }else
        [self.selectedSections addObject:[NSNumber numberWithInteger:tag]];

    [self.tableView reloadData];
}

@end

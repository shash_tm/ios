//
//  TMAppViralityCollectionViewHeader.h
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAppViralityCollectionViewHeader : UICollectionReusableView

-(void)setBottomLayerWithStatus:(BOOL)status;
-(void)setTitleText:(NSString*)text transformArrowForSelectedState:(BOOL)isSelected;

@end

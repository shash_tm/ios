//
//  TMAppViralityCollectionViewCellVoucherData.m
//  TrulyMadly
//
//  Created by Ankit on 13/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityCollectionViewCellVoucherData.h"

@interface TMAppViralityCollectionViewCellVoucherData ()

@property(nonatomic,strong)UILabel *voucherCode;
@property(nonatomic,strong)UILabel *voucherValue;
@property(nonatomic,strong)UILabel *voucherExpiry;

@property (strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation TMAppViralityCollectionViewCellVoucherData

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        //self.backgroundColor = [UIColor grayColor];
        UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(5,
                                                                     CGRectGetHeight(self.frame)-1,
                                                                     CGRectGetWidth(self.frame),
                                                                     1)];
        lineView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        [self addSubview:lineView];
        
        
        self.voucherCode = [[UILabel alloc] initWithFrame:CGRectMake(5,
                                                                   (CGRectGetHeight(self.frame)-40)/2,
                                                                   CGRectGetWidth(self.bounds)-180,
                                                                   40)];
        self.voucherCode.numberOfLines = 0;
        self.voucherCode.textColor = [UIColor blackColor];
        self.voucherCode.font = [UIFont systemFontOfSize:14];
        self.voucherCode.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.voucherCode];
        
        self.voucherValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.voucherCode.frame)+10,
                                                                   (CGRectGetHeight(self.frame)-20)/2,
                                                                   60,
                                                                   20)];
        
        self.voucherValue.textColor = [UIColor blackColor];
        self.voucherValue.font = [UIFont systemFontOfSize:14];
        self.voucherValue.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.voucherValue];
        
        self.voucherExpiry = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.voucherValue.frame)+10,
                                                                      (CGRectGetHeight(self.frame)-20)/2,
                                                                      100,
                                                                      20)];
        
        self.voucherExpiry.textColor = [UIColor blackColor];
        self.voucherExpiry.font = [UIFont systemFontOfSize:14];
        self.voucherExpiry.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.voucherExpiry];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-44)/2,
                                                                                           (CGRectGetHeight(self.frame)-44)/2,
                                                                                           44,
                                                                                           44)];
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        self.activityIndicator.hidesWhenStopped = TRUE;
        [self addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
        
    }
    return self;
}

-(void)setVoucherCode:(NSString*)code voucherValue:(NSString*)value voucherExpiry:(NSString*)expiry isHeader:(BOOL)header
         voucherCount:(NSInteger)voucherCount {
    if(code) {
        if(voucherCount) {
            [self stopActivityLoader];
        }
        if(header) {
            self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        }
        else {
            self.backgroundColor = [UIColor clearColor];
        }
        
        self.voucherCode.text = code;
        self.voucherValue.text = value;
        self.voucherExpiry.text = expiry;
    }
}

-(void)stopActivityLoader {
    [self.activityIndicator stopAnimating];
}

@end

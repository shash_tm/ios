//
//  TMAppViralityCollectionViewCellRewardData.m
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityCollectionViewCellReferralData.h"

@interface TMAppViralityCollectionViewCellReferralData ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *dateLabel;

@property (strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation TMAppViralityCollectionViewCellReferralData

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        //self.backgroundColor = [UIColor clearColor];
        CGFloat width = 40;
        UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(5,
                                                                     CGRectGetHeight(self.frame)-1,
                                                                     CGRectGetWidth(self.frame),
                                                                     1)];
        lineView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        [self addSubview:lineView];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,
                                                                       ((CGRectGetHeight(self.frame) - width)/2),
                                                                       width,
                                                                       width)];
        self.imageView.layer.cornerRadius = CGRectGetWidth(self.imageView.frame)/2;
        self.imageView.layer.masksToBounds = YES;
        [self addSubview:self.imageView];
        
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.imageView.frame)+5,
                                                                   (CGRectGetHeight(self.frame)-40)/2,
                                                                   CGRectGetWidth(self.bounds)-110,
                                                                   40)];
        self.nameLabel.numberOfLines = 0;
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:self.nameLabel];
        
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.frame)-110,
                                                                   (CGRectGetHeight(self.frame)-20)/2,
                                                                   100,
                                                                   20)];

        self.dateLabel.textColor = [UIColor blackColor];
        self.dateLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:self.dateLabel];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-44)/2,
                                                                                           (CGRectGetHeight(self.frame)-44)/2,
                                                                                           44,
                                                                                           44)];
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        self.activityIndicator.hidesWhenStopped = TRUE;
        [self addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
    }
    return self;
}

-(void)setupReferalName:(NSString*)name image:(NSString*)imageURL date:(NSString*)date {
    if(name) {
        [self.activityIndicator stopAnimating];
        self.nameLabel.text = name;
        self.dateLabel.text = date;
        self.imageView.image = [UIImage imageNamed:@"ProfileBlank"];
        [TMAppViralityCollectionViewCellReferralData downloadImageWithURL:[NSURL URLWithString:imageURL] completionBlock:^(BOOL succeeded, UIImage *image) {
            if(succeeded&&image) {
                self.imageView.image = image;
            }
        }];
    }
}

+ (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

@end

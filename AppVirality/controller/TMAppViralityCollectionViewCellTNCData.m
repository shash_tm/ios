//
//  TMAppViralityCollectionViewCellTNCData.m
//  TrulyMadly
//
//  Created by Ankit on 13/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityCollectionViewCellTNCData.h"

@interface TMAppViralityCollectionViewCellTNCData ()//<UIWebViewDelegate>

@property (strong) UIWebView *termsView;
@property (strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation TMAppViralityCollectionViewCellTNCData

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        self.termsView= [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 90)];
        self.termsView.alpha=0.5f;
        [self.termsView setBackgroundColor:[UIColor clearColor]];
        [self.termsView setOpaque:NO];
        //self.termsView.delegate = self;
        [self addSubview:self.termsView];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-44)/2,
                                                                                           (CGRectGetHeight(self.frame)-44)/2,
                                                                                           44,
                                                                                           44)];
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        self.activityIndicator.hidesWhenStopped = TRUE;
        [self addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
    }
    return self;
}

-(void)setupWebViewWithString:(NSString*)htmlString {
    if(htmlString) {
        self.backgroundColor = [UIColor clearColor];
        [self.activityIndicator stopAnimating];
        [self.termsView loadHTMLString:htmlString baseURL:nil];
    }
}

//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//    [self.activityIndicator stopAnimating];
//}
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error {
//    [self.activityIndicator stopAnimating];
//}

@end

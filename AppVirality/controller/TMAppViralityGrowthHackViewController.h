//
//  TMAppViralityGrowthHackViewController.h
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppVirality.h"


@interface TMAppViralityGrowthHackViewController : UIViewController

-(id)initWithCampaignDetails:(NSDictionary *)campaignDetails ForGrowthHack:(GrowthHackType)growthHack;

@end

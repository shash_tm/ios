//
//  TMAppViralityCollectionViewCellRewardData.h
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAppViralityCollectionViewCellReferralData : UICollectionViewCell

-(void)setupReferalName:(NSString*)name image:(NSString*)imageURL date:(NSString*)date;

@end

//
//  TMAppViralityCollectionViewCellShareButton.m
//  TrulyMadly
//
//  Created by Ankit on 12/05/16.
//  Copyright © 2016 trulymadly. All rights reserved.
//

#import "TMAppViralityCollectionViewCellShareButton.h"

@interface TMAppViralityCollectionViewCellShareButton ()

@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UILabel *textLabel;
@end

@implementation TMAppViralityCollectionViewCellShareButton

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        CGFloat width = 90;
        CGFloat iconWidth = 60;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, width)];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake((width-iconWidth)/2, 12, iconWidth, iconWidth)];
        
        [view addSubview:self.imageView];
        
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.imageView.frame)+5, width, 20)];
        self.textLabel.font = [UIFont systemFontOfSize:13];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        [view addSubview:self.textLabel];
        
        [self.contentView addSubview:view];
    }
    return self;
}

-(void)setupShareButtonsWithImage:(NSString*)imageName withText:(NSString*)text {
    imageName = ([text isEqualToString:@"More"]) ? @"more_withborder" : imageName;
    self.imageView.image = [UIImage imageNamed:imageName];
    self.textLabel.text = text;
}

@end

//
//  TMDateSpotTests.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 23/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TMDateSpot.h"

@interface TMDateSpotTests : XCTestCase

@end

@implementation TMDateSpotTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(NSDictionary *)stubDictionary {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"dateSpot" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
    
    return json;
}

-(void)testDateSpotResponse
{
    TMDateSpot *dateSpotObj = [[TMDateSpot alloc] initWithDateData:[self stubDictionary]];
    XCTAssertNotNil(dateSpotObj, @"created obj successfully");
}

@end

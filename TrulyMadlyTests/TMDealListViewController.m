//
//  TMDealListViewController.m
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 28/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import "TMDealListViewController.h"
#import "TMDealDetailViewController.h"
#import "TMDealListCollectionViewCell.h"
#import "TMCuratedDealsManager.h"
#import "TMCuratedDealContext.h"
#import "TMDateSpot.h"
#import "TMAnalytics.h"
#import "TMbadgeView.h"
#import "TMLocationPreferenceViewController.h"
#import "TMDataStore+TMAdAddtions.h"
#import "NSString+TMAdditions.h"
#import "UIViewController+ECSlidingViewController.h"

#define LOCATION_TUTORIAL @"location_tutorial"

@interface TMDealListViewController () <UICollectionViewDataSource, UICollectionViewDelegate, TMLocationPreferenceViewControllerDelegate>

@property(nonatomic, strong) UICollectionView* dealListCollectionView;
@property(nonatomic, strong) UIImageView* loaderBackgroundImageView;
@property(nonatomic, strong) NSArray* dateSpotArray;
@property(nonatomic, strong)TMBadgeView* locationBadgeView;
@property(nonatomic, strong)UIView *locationTutorial;
@property(nonatomic, strong)NSMutableArray *zoneArray;
@property(nonatomic, assign)BOOL isRequestInProgress;
@property(nonatomic, assign)BOOL saveZones;
@property(nonatomic, strong)NSString *cityName;
@property(nonatomic, strong) NSArray* zoneIds;
@property(nonatomic, strong)UIView *overlayView;

//deal manager
@property(nonatomic, strong) TMCuratedDealsManager* dealManager;

//current match ID
@property(nonatomic, strong) NSString* matchID;


@end

@implementation TMDealListViewController

#define DEAL_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER @"DealListCollectionViewCell"
#define DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG 1001
#define DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE 1002
#define DEAL_LIST_EMPTY_DEAL_LIST_IMAGE 1003

#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736.0)

#pragma mark - Initializers

- (instancetype) init {
    //non default constructor
    //require matchID for it to function properly
    return nil;
}


- (instancetype) initWithMatchID:(NSString*) matchID {
    self = [super init];
    if (self) {
        if (matchID && [matchID isKindOfClass:[NSString class]]) {
            self.matchID = matchID;
            self.zoneArray = [[NSMutableArray alloc] init];
            //self.fromPref = false;
            return self;
        }
    }
    return nil;
}

-(UIView*)overlayView {
    if(!_overlayView) {
        _overlayView = [[UIView alloc] initWithFrame:self.view.bounds];
        _overlayView.backgroundColor = [UIColor blackColor];
        _overlayView.alpha = 0.5;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [_overlayView addGestureRecognizer:tapGesture];
    }
    return _overlayView;
}

#pragma mark - View Controller life cycle methods

- (void) viewDidLoad {
  
    //create the navigation title
    UIBarButtonItem* cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed)];
    self.navigationItem.leftBarButtonItem = cancelBarButtonItem;
     self.title = @"Choose Your Datespot";
   
    //add the collection view
    [self createDealListCollectionView];

    //make request for getting deal list
    [self makeDealListRequest];
}

- (void) viewDidLayoutSubviews {
    //update the frame for the deal list collection view
    self.dealListCollectionView.frame = self.view.bounds;
}

#pragma mark - Deal List UI creation methods

- (void) createDealListCollectionView {
    //add the collection view
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = 7.5;
    flowLayout.minimumInteritemSpacing = 0;
    
    self.dealListCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.dealListCollectionView.backgroundColor = [UIColor whiteColor];
    self.dealListCollectionView.dataSource = self;
    self.dealListCollectionView.delegate = self;
    [self.dealListCollectionView registerClass:[TMDealListCollectionViewCell class] forCellWithReuseIdentifier:DEAL_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER];
    [self.view addSubview:self.dealListCollectionView];
}

#pragma mark - Empty Deal List Message methods

/**
 * Shows the empty deal list content to the user
 */
- (void) showEmptyDealListMessage {
    
    [self hideEmptyDealListMessage];
    
    UILabel* emptyDealListMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (IS_IPHONE_6_PLUS)?(self.view.bounds.size.width*4/5):(self.view.bounds.size.width*4.5/5), 80)];
    emptyDealListMessageLabel.backgroundColor = [UIColor clearColor];
    emptyDealListMessageLabel.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    emptyDealListMessageLabel.numberOfLines = 3;
    emptyDealListMessageLabel.tag = DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE;
    emptyDealListMessageLabel.textColor = [UIColor colorWithRed:(112.0/255.0) green:(112.0/255.0) blue:(112.0/255.0) alpha:1.0];
    emptyDealListMessageLabel.font = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?[UIFont systemFontOfSize:15]:[UIFont systemFontOfSize:13];
    emptyDealListMessageLabel.textAlignment = NSTextAlignmentCenter;
    emptyDealListMessageLabel.text = @"Datelicious is not yet available in your area!\nWe are working on it. Please check back later.";
    [self.view addSubview:emptyDealListMessageLabel];
    
    //coffee background image view
    
    CGFloat coffeeLogoSize = (IS_IPHONE_6_PLUS || IS_IPHONE_6)?72:54;
    
    UIImageView* logoLoaderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, coffeeLogoSize, coffeeLogoSize)];
    logoLoaderImageView.image = [UIImage imageNamed:@"date_list_loader_gray_icon"];
    logoLoaderImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoLoaderImageView.backgroundColor = [UIColor clearColor];
    logoLoaderImageView.tag = DEAL_LIST_EMPTY_DEAL_LIST_IMAGE;
    logoLoaderImageView.center = CGPointMake(self.view.bounds.size.width/2, emptyDealListMessageLabel.frame.origin.y - 18);
    [self.view addSubview:logoLoaderImageView];
}


/**
 * Shows the empty deal list content to the user
 */
- (void) hideEmptyDealListMessage {
    if ([self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE]) {
        [[self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_MESSAGE_LABLE] removeFromSuperview];
    }
    if ([self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_IMAGE]) {
        [[self.view viewWithTag:DEAL_LIST_EMPTY_DEAL_LIST_IMAGE] removeFromSuperview];
    }
}

#pragma mark - Deal List Loader View methods

/**
 * Shows the deal list loader view
 */
- (void) showDealListLoaderView {
    
    //hide any deal list loader view if present
    [self hideDealListLoaderView];
    
    UIImageView* dealListLoaderBackgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    dealListLoaderBackgroundImageView.image = [UIImage imageNamed:@"date_list_loader_background"];
    dealListLoaderBackgroundImageView.tag = DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG;
    
    //coffee background image view
    UIImageView* logoLoaderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 72, 72)];
    logoLoaderImageView.image = [UIImage imageNamed:@"date_list_loader_icon"];
    logoLoaderImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoLoaderImageView.backgroundColor = [UIColor clearColor];
    logoLoaderImageView.center = CGPointMake(self.view.bounds.size.width/2, 2*self.view.bounds.size.height/5);
    [dealListLoaderBackgroundImageView addSubview:logoLoaderImageView];
    
    //loader message label
    UILabel* loaderMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x - self.view.bounds.size.width*7/20, logoLoaderImageView.frame.origin.y + logoLoaderImageView.bounds.size.height - 16, self.view.bounds.size.width*7/10, 108)];
    loaderMessageLabel.backgroundColor = [UIColor clearColor];
    loaderMessageLabel.textColor = [UIColor whiteColor];
    loaderMessageLabel.textAlignment = NSTextAlignmentCenter;
    loaderMessageLabel.numberOfLines = 3;
    loaderMessageLabel.font = (IS_IPHONE_6_PLUS)?([UIFont systemFontOfSize:16]):(IS_IPHONE_6?([UIFont systemFontOfSize:15]):(([UIScreen mainScreen].bounds.size.height > 500)?([UIFont systemFontOfSize:14]):([UIFont systemFontOfSize:13])));
    loaderMessageLabel.text = @"PICKING OUT THE BEST SPOTS\nFOR YOUR DATE...";
    [dealListLoaderBackgroundImageView addSubview:loaderMessageLabel];
    
    [self.view addSubview:dealListLoaderBackgroundImageView];
}


/**
 * Hides the deal list loader view
 */
- (void) hideDealListLoaderView {
    if ([self.view viewWithTag:DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG]) {
        [[self.view viewWithTag:DEAL_LIST_LOADER_BACKGROUND_VIEW_TAG] removeFromSuperview];
    }
}

#pragma mark - Deal List network request method

/**
 * Makes a network request for the deal list
 */
- (void) makeDealListRequest {
    
    //show loader screen
    [self showDealListLoaderView];
    
    //remove retry views if present
    [self removeRetryView];
    
    //make the network request
    self.dealManager = [[TMCuratedDealsManager alloc] init];
    if ([self.dealManager isNetworkReachable]) {
        
        self.isRequestInProgress = true;
        NSDictionary *params = @{@"action":@"get_deals", @"match_id":self.matchID};
        if(self.saveZones && self.zoneIds.count>0){
            params = @{@"action":@"save_preference", @"zones":self.zoneIds, @"match_id":self.matchID};
        }
        self.view.userInteractionEnabled = false;
        [self.dealManager getDealsListWithResponseBlock:params responseBlock:^(NSDictionary* dateSpotResponse, TMError* error){
            self.isRequestInProgress = false;
            self.view.userInteractionEnabled = true;
            if (dateSpotResponse) {
                self.saveZones = false;
                self.zoneIds = nil;
                //update the UI with the date spot list
                NSMutableArray* dateSpotMutableArray = [[NSMutableArray alloc] init];
                
                NSArray *dateSpotArray = [[NSArray alloc] initWithArray:[dateSpotResponse objectForKey:@"dealList"]];
                for (int index = 0; index < dateSpotArray.count; index++) {
                    if ([[dateSpotArray objectAtIndex:index] isKindOfClass:[TMDateSpot class]]) {
                        TMDateSpot* dateSpot = [dateSpotArray objectAtIndex:index];
                       [dateSpotMutableArray addObject:dateSpot];
                    }
                }
                self.dateSpotArray = [dateSpotMutableArray copy];
                
                NSArray *zoneList = [dateSpotResponse objectForKey:@"zoneList"];
                int counter = [self processZoneList:zoneList];
                self.cityName = [dateSpotResponse objectForKey:@"cityName"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addRightNavigationItem];
                    [self hideDealListLoaderView];
                    [self hideEmptyDealListMessage];
                    [self setBadgeCounter:counter];
                    if(![self didShownLocationTutorial]) {
                        [self showLocationTutorial];
                    }
                    
                    [self.dealListCollectionView reloadData];
                    
                    if (self.dateSpotArray.count == 0) {
                        //empty list
                        [self showEmptyDealListMessage];
                    }
                });
            }
            else {
                //show retry view
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideDealListLoaderView];
                    [self hideEmptyDealListMessage];
                    
                    //failure block
                    [self showRetryViewWithMessage:@"Oops something went wrong"];
                    [self.retryView.retryButton addTarget:self action:@selector(makeDealListRequest) forControlEvents:UIControlEventTouchUpInside];
                });
            }
        }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideDealListLoaderView];
            [self hideEmptyDealListMessage];
            
            //failure block
            [self showRetryViewWithMessage:@"No Internet Connectivity"];
            [self.retryView.retryButton addTarget:self action:@selector(makeDealListRequest) forControlEvents:UIControlEventTouchUpInside];
        });
    }
}

-(void)setBadgeCounter:(int)counter
{
    NSString *count = [NSString stringWithFormat: @"%d", counter];
    if(self.locationBadgeView){
        [self.locationBadgeView setCounter:count];
    }
}

-(void)addRightNavigationItem
{
    if(!self.locationBadgeView) {
        self.locationBadgeView = [[TMBadgeView alloc] initWithFrame:CGRectMake(0, 0, 54, 44)
                                                          withImage:[UIImage imageNamed:@"location_blue"]
                                                     withImageFrame:CGRectMake(22, 12, 18, 24)];
        [self.locationBadgeView.badgeButton addTarget:self action:@selector(locationPref) forControlEvents:UIControlEventTouchUpInside];
    
        UIBarButtonItem *rightMenuButton1 = [[UIBarButtonItem alloc]
                                         initWithCustomView:self.locationBadgeView];
        ///////
//        UIBarButtonItem *rightMenuButton2 = [[UIBarButtonItem alloc]
//                                         initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//        rightMenuButton2.width = 44;
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -16;
    
        NSArray *itemarrs = [NSArray arrayWithObjects:negativeSpacer,rightMenuButton1, nil];
        [self.navigationItem setRightBarButtonItems:itemarrs];
    }
}

#pragma mark - Cancel button callback method

/**
 * Callback method when cancel button is pressed
 */
- (void) cancelButtonPressed {
    //dismiss the deal list view controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionView methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    int requiredHeight = 170;//(IS_IPHONE_6_PLUS?(242):(IS_IPHONE_6?226:([UIScreen mainScreen].bounds.size.height > 500)?246:216));
    
    int cellSideMargin = 8;
    
    int requredWidth = self.dealListCollectionView.bounds.size.width - 2*cellSideMargin;
    
    return CGSizeMake(requredWidth, requiredHeight);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dateSpotArray.count;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 0, 8, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMDealListCollectionViewCell* dealListCell = [self.dealListCollectionView dequeueReusableCellWithReuseIdentifier:DEAL_LIST_COLLECTION_VIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    dealListCell.dateSpot = [self.dateSpotArray objectAtIndex:indexPath.item];
    return dealListCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TMDateSpot* dateSpot = [self.dateSpotArray objectAtIndex:indexPath.item];
    TMCuratedDealContext *dealContext = [[TMCuratedDealContext alloc] init];
    dealContext.dateSpotId = dateSpot.dateSpotId;
    dealContext.matchId = self.matchID;
    TMDealDetailViewController *dealDetaiView = [[TMDealDetailViewController alloc] initWithDealContext:dealContext];
    dealDetaiView.dealDetailViewControllerDelegate = self.dealListViewControllerDelegate;
    [self.navigationController pushViewController:dealDetaiView animated:YES];
}

#pragma mark - dealloc method

- (void) dealloc {
    //NSLog(@"deal list dealloc called");
    self.dealManager = nil;
}

-(void)locationPref
{
    if(!self.isRequestInProgress) {
        if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered) {
            [self.slidingViewController anchorTopViewToLeftAnimated:YES];
            [self.view addSubview:self.overlayView];
            [self.view bringSubviewToFront:self.overlayView];
            TMLocationPreferenceViewController *locPrefVCont = (TMLocationPreferenceViewController *)self.slidingViewController.underRightViewController;
            locPrefVCont.locationPreferenceDelegate = self;
            [locPrefVCont initWithZoneArray:self.zoneArray cityName:self.cityName];
            
            //track location filter clicked
            [self trackLocation:@"filter_clicked" eventInfo:nil];
        }
        else {
            [self resetLocationView];
        }
    }
}


#pragma mark - tutorial methods

-(void)showLocationTutorial
{
    self.locationTutorial = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.locationTutorial.backgroundColor = [UIColor blackColor];
    self.locationTutorial.alpha = 0.9;
    [self.view addSubview:self.locationTutorial];
    [self.view bringSubviewToFront:self.locationTutorial];
    
    NSString *text = @"Use location \n preference to \n refine your search!";
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:18]};
    CGSize constrintSize = CGSizeMake(self.view.bounds.size.width, NSUIntegerMax);
    CGRect rect = [text boundingRectWithConstraintSize:constrintSize attributeDictionary:attributes];
    
    UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-20-13, self.locationTutorial.frame.origin.y+120, 20, 20)];
    circleView.layer.cornerRadius = circleView.frame.size.width/2;
    circleView.backgroundColor = [UIColor whiteColor];
    [self.locationTutorial addSubview:circleView];
    
    UIView *verView = [[UIView alloc] initWithFrame:CGRectMake(circleView.frame.origin.x+(circleView.frame.size.width-3)/2+1, circleView.frame.origin.y+circleView.frame.size.height, 3, self.view.bounds.size.height/6)];
    verView.backgroundColor = [UIColor whiteColor];
    [self.locationTutorial addSubview:verView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-rect.size.width-10, verView.frame.origin.y +verView.frame.size.height + 15, rect.size.width, rect.size.height)];
    lbl.text = text;
    lbl.font = [UIFont systemFontOfSize:18];
    lbl.textColor = [UIColor whiteColor];
    lbl.numberOfLines = 0;
    lbl.textAlignment = NSTextAlignmentRight;
    [lbl sizeToFit];
    [self.locationTutorial addSubview:lbl];
    
    // animate the whole component
    [UIView animateWithDuration:1.0
                          delay: 0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect newFrame = circleView.frame;
                         newFrame.origin.y -=  56;
                         circleView.frame = newFrame;
                         
                         newFrame = verView.frame;
                         newFrame.origin.y -=  56;
                         verView.frame = newFrame;
                         
                         newFrame = lbl.frame;
                         newFrame.origin.y -=  56;
                         lbl.frame = newFrame;
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tutorialAction)];
    [self.locationTutorial addGestureRecognizer:tapGesture];
    
    [self setLocationTutorialKey];
}

-(void)tutorialAction
{
    if(self.locationTutorial) {
        [self.locationTutorial removeFromSuperview];
        self.locationTutorial = nil;
    }
}

-(BOOL)didShownLocationTutorial
{
    if([TMDataStore containsObjectWithUserIDForKey:LOCATION_TUTORIAL]) {
        BOOL status = [[TMDataStore retrieveObjectWithUserIDForKey:LOCATION_TUTORIAL] boolValue];
        return status;
    }
    return false;
}

-(void)setLocationTutorialKey
{
    [TMDataStore setObjectWithUserIDForObject:[NSNumber numberWithBool:TRUE] key:LOCATION_TUTORIAL];
}

-(int)processZoneList:(NSArray *)zoneList
{
    [self.zoneArray removeAllObjects];
    int counter = 0;
    BOOL isZoneSelected = false;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"-1",@"zone_id",@"Doesn\'t Matter",@"name",[NSNumber numberWithBool:false],@"selected",nil];
    [self.zoneArray addObject:dict];
    for (int i=0; i<zoneList.count; i++) {
        NSMutableDictionary *zoneDict = zoneList[i];
        if([[zoneDict objectForKey:@"selected"] boolValue]){
            isZoneSelected = true;
            counter++;
        }
        [self.zoneArray addObject:zoneDict];
    }
    
    if(!isZoneSelected){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.zoneArray[0]];
        dict[@"selected"] = [NSNumber numberWithBool:true];
        self.zoneArray[0] = dict;
    }
    return counter;
}

#pragma mark TMlocationprefernceViewControllerDelegate Method

-(void)didClickForApply:(NSArray *)zoneIdArr
{
    [self resetLocationView];
    
    if(zoneIdArr.count > 0){
        self.zoneIds = [[NSArray alloc] initWithArray:zoneIdArr];
        self.saveZones = true;
        [self makeDealListRequest];
        
        // track location
        NSString * result = [[zoneIdArr valueForKey:@"description"] componentsJoinedByString:@" "];
        NSDictionary *eventInfoDict = @{@"location":result};
        [self trackLocation:@"save_datespot_preferences" eventInfo:eventInfoDict];
    }
}

-(void)tapAction
{
    [self resetLocationView];
}

-(void)resetLocationView
{
    [self.slidingViewController resetTopViewAnimated:YES];
    self.view.userInteractionEnabled = true;
    [self.overlayView removeFromSuperview];
}

// pragma mark Tracking Function
-(void)trackLocation:(NSString *)eventType eventInfo:(NSDictionary *)eventInfo
{
    NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
    [eventDict setObject:@"TMDealListViewController" forKey:@"screenName"];
    [eventDict setObject:@"datespot_list" forKey:@"eventCategory"];
    [eventDict setObject:eventType forKey:@"eventAction"];
    [eventDict setObject:self.matchID forKey:@"label"];
    if(eventInfo) {
        [eventDict setObject:eventInfo forKey:@"event_info"];
    }
    [[TMAnalytics sharedInstance] trackNetworkEvent:eventDict];
    eventDict = nil;
}

@end

//
//  TMProfileResponseTests.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 03/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "TMProfileResponse.h"
#import "TMUser.h"
#import "TMMatchesViewController.h"

@interface TMProfileResponseTests : XCTestCase

@property(nonatomic)TMMatchesViewController *view;

@end

@interface TMMatchesViewController (Test)
-(void)processMatchResponse:(TMProfileResponse*)matchResponse;
-(void)processUserFlagResponse:(NSDictionary*)response;
-(BOOL)isTopMostViewController;
@property(nonatomic,assign)BOOL isNudgeRequired;
@property(nonatomic,assign)BOOL isShownNudgeScreen;
@property(nonatomic,assign)BOOL isProfileComplete;
@property(nonatomic,assign)BOOL isTrustVerified;
@end

@implementation TMMatchesViewController (Test)

@dynamic isNudgeRequired;
@dynamic isShownNudgeScreen;
-(BOOL)isTopMostViewController {
    return true;
}

@end

@implementation TMProfileResponseTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.view = [[TMMatchesViewController alloc] init];
}

- (void)tearDown {
    self.view = nil;
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(NSDictionary *)stubDictionary {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"matchJSON" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
    
    return json;
}

-(NSDictionary *)stubDictionaryForNudges {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"matchResponseWithNudges" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
    
    return json;
}

-(NSDictionary *)stubDictionaryForMales {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"matchResponseForMale" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
    
    return json;
}

//dictionary for userFlag with no Ad
-(NSDictionary *)stubUserFlagsResponse {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"userFlags_noAd" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
    
    return json;
}

// test case for valid female user's data and matches and with profile visibility and no nudges to shown
-(void)testMatchResponse {
    TMUser *user = [[TMUser alloc] init];
    TMProfileResponse *matchResponse = [[TMProfileResponse alloc] initWithResponse:[self stubDictionary]];
    XCTAssertFalse(matchResponse.isNudgeAvailable, @"nudges are not available");
    XCTAssertNotNil(user.fName, @"fname not nil");
    NSString *age  = [NSString stringWithFormat:@"%d", user.age];
    XCTAssertNotNil(age, @"age not nil");
    XCTAssertNotNil(user.status, @"status not nil");
    XCTAssertNotNil(user.gender, @"gender not nil");
    XCTAssertNotNil(user.userId ,@"user id not nil");
    XCTAssertNotNil(matchResponse.trustScores ,@"user's trust score dictionary not nil");
    
    [self.view processMatchResponse:matchResponse];
    
    XCTAssertNotNil(self.view, @"view is set");
    
    // to check whether user to shown photo visibility nudge
    XCTAssertTrue(self.view.isShownNudgeScreen ,@"show the profile visibility nudge");

}

// test case for valid female user's data and matches and with profile visibility and nudges to shown
-(void)testMatchResponseWithNudges {
    TMUser *user = [[TMUser alloc] init];
    TMProfileResponse *matchResponse = [[TMProfileResponse alloc] initWithResponse:[self stubDictionaryForNudges]];
    XCTAssertTrue(matchResponse.isNudgeAvailable, @"nudges are available");
    XCTAssertNotNil(user.fName, @"fname not nil");
    NSString *age  = [NSString stringWithFormat:@"%d", user.age];
    XCTAssertNotNil(age, @"age not nil");
    XCTAssertNotNil(user.status, @"status not nil");
    XCTAssertNotNil(user.gender, @"gender not nil");
    XCTAssertNotNil(user.userId ,@"user id not nil");
    XCTAssertNotNil(matchResponse.trustScores ,@"user's trust score dictionary not nil");
    
    [self.view processMatchResponse:matchResponse];
    
    XCTAssertNotNil(self.view, @"view is set");
    
    //whether matchViewController will show nudges
    XCTAssertTrue(self.view.isNudgeRequired, @"nudges are available for matches");
    
    // to check whether user to shown photo visibility nudge
    XCTAssertTrue(self.view.isShownNudgeScreen ,@"show the profile visibility nudge");
}

// test case for valid male user's data and matches
-(void)testMatchResponseForMale {
    TMUser *user = [[TMUser alloc] init];
    TMProfileResponse *matchResponse = [[TMProfileResponse alloc] initWithResponse:[self stubDictionaryForMales]];
    XCTAssertFalse(matchResponse.isNudgeAvailable, @"nudges are available");
    XCTAssertNotNil(user.fName, @"fname not nil");
    NSString *age  = [NSString stringWithFormat:@"%d", user.age];
    XCTAssertNotNil(age, @"age not nil");
    XCTAssertNotNil(user.status, @"status not nil");
    XCTAssertNotNil(user.gender, @"gender not nil");
    XCTAssertNotNil(user.userId ,@"user id not nil");
    XCTAssertNotNil(matchResponse.trustScores ,@"user's trust score dictionary not nil");
    
    [self.view processMatchResponse:matchResponse];
    
    XCTAssertNotNil(self.view, @"view is set");
    
    //whether matchViewController will show nudges
    XCTAssertFalse(self.view.isNudgeRequired, @"nudges are available for matches");
    
    // to check whether user to shown photo visibility nudge
    XCTAssertFalse(self.view.isShownNudgeScreen ,@"show the profile visibility nudge");
}

-(void)testUserFlagParserWithNoAd {
    [self.view processUserFlagResponse:[self stubUserFlagsResponse]];
    XCTAssertTrue(self.view.isProfileComplete, @"profile is complete");
    XCTAssertFalse(self.view.isTrustVerified, @"trust nudge to shown");
}

@end

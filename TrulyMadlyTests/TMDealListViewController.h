//
//  TMDealListViewController.h
//  TrulyMadly
//
//  Created by Abhijeet Mishra on 28/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"

@class TMDealMessageContext;

@protocol TMDealListViewControllerDelegate <NSObject>

- (void) didClickForRequestForDateSpot:(TMDealMessageContext*)dealMessageContext;
- (void) didClickLetsGoForDateSpot:(TMDealMessageContext*)dealMessageContext;

@end

@interface TMDealListViewController : TMBaseViewController

@property (nonatomic, weak) id <TMDealListViewControllerDelegate> dealListViewControllerDelegate;

/**
 * Default intializer
 * @param match ID
 * @return deal list view controller
 */
- (instancetype) initWithMatchID:(NSString*) matchID;

@end

//
//  TMCommonUtilTests.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 01/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+TMAdditions.h"
#import "TMCommonUtil.h"

@interface TMCommonUtilTests : XCTestCase

@end

@implementation TMCommonUtilTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testEmailId {
    XCTAssertTrue([@"gm@tm.com" isValidEmail], @"valid email");
    XCTAssertTrue([@"rm@tm123.com" isValidEmail], @"valid email");
    XCTAssertFalse([@"g d@tm.com" isValidEmail], @"not a valid email");
    XCTAssertFalse([@"ab$@scm.co" isValidEmail] , @"not a valid email");
    XCTAssertFalse([@"ab$@scm" isValidEmail] , @"not a valid email");
}

-(void)testNullString {
    NSString *str;
    XCTAssertFalse([str isNULLString], @"null string");
    str = @"null";
    XCTAssertTrue([str isNULLString], @"null string");
    str = @"<null>";
    XCTAssertTrue([str isNULLString], @"null string");
    str = @"(null)";
    XCTAssertTrue([str isNULLString], @"null string");
    str = @"testing";
    XCTAssertFalse([str isNULLString], @"not a null string");
}

-(void)testDeviceIdfromCache {
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"vendorid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *vendorId = [TMCommonUtil vendorId];
    XCTAssertEqualObjects(@"1", vendorId, @"test case passed");
}

@end

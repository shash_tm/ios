//
//  TMAppCacheResposeTests.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 12/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TMAppResponseCachingManager.h"

@interface TMAppCacheResposeTests : XCTestCase

@end

@implementation TMAppCacheResposeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    
    [TMAppResponseCachingManager deleateAllCacheData];
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(NSDictionary *)stubDictionary {
    NSString *file = [[NSBundle bundleForClass:[self class]] pathForResource:@"matchJSON" ofType:@"json"];
    NSData *dta = [NSData dataWithContentsOfFile:file];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dta options:NSJSONReadingMutableLeaves error:&error];
    
    return json;
}

-(void)testsetCachedResponse {
    BOOL isSetSuccessfully = [TMAppResponseCachingManager setCachedResponse:[self stubDictionary] forURL:@"http://dev.trulymadly.com/trulymadly/profile.php?login_mobile=true" hash:@"abc123456" tstamp:@"2015-10-13 00:00:00"];
    XCTAssertTrue(isSetSuccessfully, @"response cached successfully");
    
    BOOL cacheResponse = [TMAppResponseCachingManager containsCachedResponseForURL:@"http://dev.trulymadly.com/trulymadly/profile.php?login_mobile=true"];
    XCTAssertTrue(cacheResponse, @"contain cache response");
    
    NSDictionary *response = [TMAppResponseCachingManager getCachedResponseForURL:@"http://dev.trulymadly.com/trulymadly/profile.php?login_mobile=true"];
    XCTAssertNotNil(response, @"response is not nil");
    
    XCTAssertEqualObjects(response, [self stubDictionary], "Both response are equal");
    
    NSString *hash = [TMAppResponseCachingManager getHashValueForURL:@"http://dev.trulymadly.com/trulymadly/profile.php?login_mobile=true"];
    XCTAssertEqualObjects(hash, @"abc123456", @"Both hash are same");
    
    NSString *tstamp = [TMAppResponseCachingManager getTStampValueForURL:@"http://dev.trulymadly.com/trulymadly/profile.php?login_mobile=true"];
    XCTAssertEqualObjects(tstamp, @"2015-10-13 00:00:00", @"Both tstamp are same");
    
}

-(void)testContainsResponseUrl {
    BOOL cacheResponse = [TMAppResponseCachingManager containsCachedResponseForURL:@"http://dev.trulymadly.com/trulymadly/profile.php?login_mobile=true"];
    XCTAssertFalse(cacheResponse, @"does not contain cache response");
}

@end

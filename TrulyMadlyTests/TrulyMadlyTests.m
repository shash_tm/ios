//
//  TrulyMadlyTests.m
//  TrulyMadly
//
//  Created by Rajesh Singh on 01/12/15.
//  Copyright © 2015 trulymadly. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TMAnalytics.h"

@interface TMAnalytics (Test)
-(NSMutableArray *)sendEvent;
@end

@interface TrulyMadlyTests : XCTestCase

@end

@implementation TrulyMadlyTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
